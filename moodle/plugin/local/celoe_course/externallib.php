<?php

require_once($CFG->libdir . "/externallib.php");
 
class local_celoe_course_external extends external_api {
 
    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_courses_by_creator_parameters() {
        return new external_function_parameters(
            array('userid' => new external_value(PARAM_INT, 'user id of creator'))
        );
    }

    /**
     * Get courses by creator
     * @param int $userid course creator user id
     * @return array of course
     */
    public static function get_courses_by_creator($userid) { //Don't forget to set it as static
        global $CFG, $DB;
        require_once($CFG->dirroot . "/course/externallib.php");

        // Parameter validation
        $params = self::validate_parameters(self::get_courses_by_creator_parameters(), array('userid' => $userid));
 
        $transaction = $DB->start_delegated_transaction(); //If an exception is thrown in the below code, all DB queries in this code will be rollback.
 
        $courses = array();
 
        if (trim($userid) == '') {
            throw new invalid_parameter_exception('Invalid course creator user id');
        }
 
        // get the courses
        $sql = "SELECT mc.id, mc.shortname, mc.category  AS categoryid, mcc.sortorder AS categorysortorder, mc.fullname, mc.fullname AS displayname, mc.idnumber, mc.summary, mc.summaryformat, mc.format, mc.showgrades, mc.newsitems, mc.startdate, mc.enddate, mc.maxbytes, mc.showreports, mc.visible, mc.groupmode, mc.groupmodeforce, mc.defaultgroupingid, mc.timecreated, mc.timemodified, mc.enablecompletion, mc.completionnotify, mc.lang, mc.theme AS forcetheme, mc.cacherev FROM mdl_course mc 
                JOIN mdl_logstore_standard_log mlsl ON mc.id = mlsl.courseid
                JOIN mdl_course_categories mcc ON mc.category = mcc.id
                WHERE mlsl.userid = :userid AND mlsl.action = 'created' AND mlsl.target = 'course'";
        $courses = $DB->get_records_sql($sql, array('userid' => $userid));

        foreach ($courses as &$course) {
            // get the courseformatoptions
            $sql = "SELECT name, value FROM mdl_course_format_options WHERE courseid = :courseid";
            $course->courseformatoptions = $DB->get_records_sql($sql, array('courseid' => $course->id));
        }

        foreach ($courses as &$course) {
            // get the contents
            $contents = core_course_external::get_course_contents($course->id);
            $course->contents = $contents;
        }

        $transaction->allow_commit();

        return $courses;
    }

    public static function get_courses_by_creator_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'course id'),
                    'shortname' => new external_value(PARAM_TEXT, 'course short name'),
                    'categoryid' => new external_value(PARAM_INT, 'category id'),
                    'categorysortorder' => new external_value(PARAM_INT, 'sort order into the category'),
                    'fullname' => new external_value(PARAM_TEXT, 'full name'),
                    'displayname' => new external_value(PARAM_TEXT, 'course display name'),
                    'idnumber' => new external_value(PARAM_TEXT, 'id number'),
                    'summary' => new external_value(PARAM_RAW, 'summary'),
                    'summaryformat' => new external_value(PARAM_INT, 'summary format (1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN)'),
                    'format' => new external_value(PARAM_TEXT, 'course format: weeks, topics, social, site, ..'),
                    'showgrades' => new external_value(PARAM_INT, '1 if grades are shown, otherwise 0'),
                    'newsitems' => new external_value(PARAM_INT, 'number of recent items appearing on the course page'),
                    'startdate' => new external_value(PARAM_INT, 'timestamp when the course start'),
                    'enddate' => new external_value(PARAM_INT, 'timestamp when the course end'),
                    'maxbytes' => new external_value(PARAM_INT, 'largest size of file that can be uploaded into the course'),
                    'showreports' => new external_value(PARAM_INT, 'are activity report shown (yes = 1, no = 0)'),
                    'visible' => new external_value(PARAM_INT, '1: available to student, 0: not available'),
                    'groupmode' => new external_value(PARAM_INT, 'no group, separate, visible'),
                    'groupmodeforce' => new external_value(PARAM_INT, '1: yes, 0: no'),
                    'defaultgroupingid' => new external_value(PARAM_INT, 'default grouping id'),
                    'timecreated' => new external_value(PARAM_INT, 'timestamp when the course have been created'),
                    'timemodified' => new external_value(PARAM_INT, 'timestamp when the course have been modified'),
                    'enablecompletion' => new external_value(PARAM_INT, 'Enabled, control via completion and activity settings, Disabled, not shown in activity settings'),
                    'completionnotify' => new external_value(PARAM_INT, '1: yes, 0: no'),
                    'lang' => new external_value(PARAM_TEXT, 'forced course language'),
                    'forcetheme' => new external_value(PARAM_TEXT, 'name of the force theme'),
                    'courseformatoptions' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'name' => new external_value(PARAM_TEXT, 'course format option name'),
                                'value' => new external_value(PARAM_RAW, 'course format option value'),
                            )
                        )
                    ),
                    'contents' => new external_multiple_structure(
                        new external_single_structure(
                            array(
                                'id' => new external_value(PARAM_INT, 'Section ID'),
                                'name' => new external_value(PARAM_TEXT, 'Section name'),
                                'visible' => new external_value(PARAM_INT, 'is the section visible', VALUE_OPTIONAL),
                                'summary' => new external_value(PARAM_RAW, 'Section description'),
                                'summaryformat' => new external_format_value('summary'),
                                'section' => new external_value(PARAM_INT, 'Section number inside the course', VALUE_OPTIONAL),
                                'hiddenbynumsections' => new external_value(PARAM_INT, 'Whether is a section hidden in the course format', VALUE_OPTIONAL),
                                'uservisible' => new external_value(PARAM_BOOL, 'Is the section visible for the user?', VALUE_OPTIONAL),
                                'availabilityinfo' => new external_value(PARAM_RAW, 'Availability information.', VALUE_OPTIONAL),
                                'modules' => new external_multiple_structure(
                                    new external_single_structure(
                                        array(
                                            'id' => new external_value(PARAM_INT, 'activity id'),
                                            'url' => new external_value(PARAM_URL, 'activity url', VALUE_OPTIONAL),
                                            'name' => new external_value(PARAM_RAW, 'activity module name'),
                                            'instance' => new external_value(PARAM_INT, 'instance id', VALUE_OPTIONAL),
                                            'description' => new external_value(PARAM_RAW, 'activity description', VALUE_OPTIONAL),
                                            'visible' => new external_value(PARAM_INT, 'is the module visible', VALUE_OPTIONAL),
                                            'uservisible' => new external_value(PARAM_BOOL, 'Is the module visible for the user?', VALUE_OPTIONAL),
                                            'availabilityinfo' => new external_value(PARAM_RAW, 'Availability information.', VALUE_OPTIONAL),
                                            'visibleoncoursepage' => new external_value(PARAM_INT, 'is the module visible on course page', VALUE_OPTIONAL),
                                            'modicon' => new external_value(PARAM_URL, 'activity icon url'),
                                            'modname' => new external_value(PARAM_PLUGIN, 'activity module type'),
                                            'modplural' => new external_value(PARAM_TEXT, 'activity module plural name'),
                                            'availability' => new external_value(PARAM_RAW, 'module availability settings', VALUE_OPTIONAL),
                                            'indent' => new external_value(PARAM_INT, 'number of identation in the site'),
                                            'onclick' => new external_value(PARAM_RAW, 'Onclick action.', VALUE_OPTIONAL),
                                            'afterlink' => new external_value(PARAM_RAW, 'After link info to be displayed.', VALUE_OPTIONAL),
                                            'customdata' => new external_value(PARAM_RAW, 'Custom data (JSON encoded).', VALUE_OPTIONAL),
                                            'completion' => new external_value(PARAM_INT, 'Type of completion tracking: 0 means none, 1 manual, 2 automatic.', VALUE_OPTIONAL),
                                            'completiondata' => new external_single_structure(
                                                array(
                                                    'state' => new external_value(PARAM_INT, 'Completion state value: 0 means incomplete, 1 complete, 2 complete pass, 3 complete fail'),
                                                    'timecompleted' => new external_value(PARAM_INT, 'Timestamp for completion status.'),
                                                    'overrideby' => new external_value(PARAM_INT, 'The user id who has overriden the status.'),
                                                ), 'Module completion data.', VALUE_OPTIONAL
                                            ),
                                            'contents' => new external_multiple_structure(
                                                new external_single_structure(
                                                    array(
                                                        // content info
                                                        'type'=> new external_value(PARAM_TEXT, 'a file or a folder or external link'),
                                                        'filename'=> new external_value(PARAM_FILE, 'filename'),
                                                        'filepath'=> new external_value(PARAM_PATH, 'filepath'),
                                                        'filesize'=> new external_value(PARAM_INT, 'filesize'),
                                                        'fileurl' => new external_value(PARAM_URL, 'downloadable file url', VALUE_OPTIONAL),
                                                        'content' => new external_value(PARAM_RAW, 'Raw content, will be used when type is content', VALUE_OPTIONAL),
                                                        'timecreated' => new external_value(PARAM_INT, 'Time created'),
                                                        'timemodified' => new external_value(PARAM_INT, 'Time modified'),
                                                        'sortorder' => new external_value(PARAM_INT, 'Content sort order'),
                                                        'mimetype' => new external_value(PARAM_RAW, 'File mime type.', VALUE_OPTIONAL),
                                                        'isexternalfile' => new external_value(PARAM_BOOL, 'Whether is an external file.', VALUE_OPTIONAL),
                                                        'repositorytype' => new external_value(PARAM_PLUGIN, 'The repository type for external files.', VALUE_OPTIONAL),
            
                                                        // copyright related info
                                                        'userid' => new external_value(PARAM_INT, 'User who added this content to moodle'),
                                                        'author' => new external_value(PARAM_TEXT, 'Content owner'),
                                                        'license' => new external_value(PARAM_TEXT, 'Content license'),
                                                    )
                                                ), VALUE_DEFAULT, array()
                                            )
                                        )
                                    ), 'list of module'
                                )
                            )
                        )
                    )
                )
                
            )
        );
    }   
}