<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_celoe_survey', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_celoe_survey
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['celoesurvey:addinstance'] = 'Add a new Celoe Survey block';
$string['celoesurvey:myaddinstance'] = 'Add a new Celoe Survey block to Dashboard';
$string['celoesurvey'] = '(new Celoe Survey block)';
$string['pluginname'] = 'Celoe Survey';
$string['apikey'] = 'Api Key';
$string['apikeydesc'] = 'Celoe Api Key to connect to CeLOE';
$string['surveyapi'] = 'Survey Api URL';
$string['surveyapidesc'] = 'URL Api to check ongoing survey';
$string['platform'] = 'Platform';
$string['platformdesc'] = 'Get survey from platform';
$string['surveyurl'] = 'Survey URL';
$string['surveyurldesc'] = 'Survey Url for submiting survey';
$string['surveyreject'] = 'Rejected Survey Api URL';
$string['surveyreject'] = 'URL Api to reject survey';
$string['eventsurveystarted'] = 'Survey Started';
