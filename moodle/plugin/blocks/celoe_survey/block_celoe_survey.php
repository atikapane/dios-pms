<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
use block_celoe_survey\event\survey_started;

/**
 * Form for editing HTML block instances.
 *
 * @package   block_celoe_survey
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_celoe_survey extends block_base {

    function init() {
        try {
            $this->title = get_string('pluginname', 'block_celoe_survey');
        } catch (coding_exception $e) {
            debugging($e);
        }
    }

    function has_config()
    {
        return true;
    }

    function get_content() {
        global $USER, $CFG, $PAGE;

        if ($this->content !== null) {
            return $this->content;
        }

        try {
            $celoe_key = get_config('block_celoe_survey', 'apikey');
            $platform = get_config('block_celoe_survey', 'platform');
            $url = get_config('block_celoe_survey', 'surveyapi') . '?user_id=' . $USER->id . '&platform=' . $platform;
        } catch (dml_exception $e) {
            debugging($e);
        }

        if(!empty($celoe_key) && !empty($url) && !empty($platform)){
            try {
                $courses = enrol_get_my_courses();

            } catch (coding_exception $e) {
                debugging($e);
            }

            if(!empty($courses)){
                $coursesId = [];
                foreach ($courses as $course){
                    $coursesId[] = $course->id;
                }
                $url .= '&classes=' . implode(',', $coursesId);
            }

            $event = survey_started::create([
                'objectid' => $PAGE->cm->instance,
                'context' => $PAGE->context,
                'other' => ['url' => $url]
            ]);
            $event->trigger();

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 600,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],
                CURLOPT_HTTPHEADER => [
                    "Accept:application/json",
                    "celoe-api-key:" . $celoe_key
                ],
            ]);

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            $event = survey_started::create([
                'objectid' => $PAGE->cm->instance,
                'context' => $PAGE->context,
                'other' => ['response' => $response]
            ]);
            $event->trigger();

            $event = survey_started::create([
                'objectid' => $PAGE->cm->instance,
                'context' => $PAGE->context,
                'other' => ['error' => $err]
            ]);
            $event->trigger();

            if(!$err){
                $result = json_decode($response, true);
                if($result['status']){
                    $respondent = [
                        'id' => $USER->id,
                        'username' => $USER->username,
                        'name' => $USER->firstname . ' ' . $USER->lastname,
                        'email' => $USER->email,
                        'origin' => $CFG->wwwroot,
                        'survey_id' => $result['survey']['id']
                    ];
                    $args = [
                        'redirect' => get_config('block_celoe_survey', 'surveyurl') . '?' . http_build_query($respondent, '', '&'),
                        'rejectUrl' => get_config('block_celoe_survey', 'surveyreject'),
                        'surveyId' => $result['survey']['id'],
                        'surveyName' => $result['survey']['name'],
                        'userId' => $USER->id,
                        'celoeKey' => $celoe_key,
                    ];

                    $event = survey_started::create([
                        'objectid' => $PAGE->cm->instance,
                        'context' => $PAGE->context,
                        'other' => ['modal' => $args]
                    ]);
                    $event->trigger();

                    $this->page->requires->js_call_amd('block_celoe_survey/script', 'showModal', $args);
                }
            }
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = "";

        return $this->content;
    }
}
