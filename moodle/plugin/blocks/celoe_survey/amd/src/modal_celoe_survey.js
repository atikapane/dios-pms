define(['jquery', 'core/notification', 'core/custom_interaction_events', 'core/modal', 'core/modal_registry'],
    function($, Notification, CustomEvents, Modal, ModalRegistry) {
    var registered = false;
    var SELECTORS = {
        PARTICIPATE_BUTTON: '[data-action="participate"]',
        NO_BUTTON: '[data-action="no"]',
    };

    var ModalCeloeSurvey = function(root) {
        Modal.call(this, root);

        if (!this.getFooter().find(SELECTORS.PARTICIPATE_BUTTON).length) {
            Notification.exception({message: 'No participate button found'});
        }

        if (!this.getFooter().find(SELECTORS.NO_BUTTON).length) {
            Notification.exception({message: 'No no button found'});
        }
    };

    ModalCeloeSurvey.TYPE = 'block_celoe_survey-modal_celoe_survey';
    ModalCeloeSurvey.prototype = Object.create(Modal.prototype);
    ModalCeloeSurvey.prototype.constructor = ModalCeloeSurvey;

    ModalCeloeSurvey.prototype.registerEventListeners = function() {
        Modal.prototype.registerEventListeners.call(this);

        this.getModal().on(CustomEvents.events.activate, SELECTORS.PARTICIPATE_BUTTON, function() {

        });

        this.getModal().on(CustomEvents.events.activate, SELECTORS.NO_BUTTON, function() {

        });
    };

    if (!registered) {
        ModalRegistry.register(ModalCeloeSurvey.TYPE, ModalCeloeSurvey, 'block_celoe_survey/modal_celoe_survey');
        registered = true;
    }

    return ModalCeloeSurvey;
});