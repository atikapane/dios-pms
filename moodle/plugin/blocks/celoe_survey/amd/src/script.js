// eslint-disable-next-line no-unused-vars
define(['jquery', 'core/templates', 'core/modal_factory', 'block_celoe_survey/modal_celoe_survey', 'core/modal_events'],
    function($, Templates, ModalFactory, ModalCeloeSurvey, ModalEvents) {

    return {
        showModal: function(redirect, rejectUrl, surveyId, surveyName, userId, celoeKey) {
            // eslint-disable-next-line promise/catch-or-return
            ModalFactory.create({
                type: ModalCeloeSurvey.TYPE,
                body: `Survey ${surveyName} is ongoing, do you want to participate?`,
                // eslint-disable-next-line promise/always-return
            }).then(function(modal) {
                modal.show();
                var participate = false;

                $('#btn-survey-participate').on('click', function() {
                    if (redirect) {
                        const win = window.open(redirect, '_blank');
                        win.focus();
                        participate = true;
                    }
                    modal.destroy();
                });

                modal.getRoot().on(ModalEvents.hidden, function() {
                    if (!participate) {
                        $.ajax({
                            type: 'POST',
                            url: rejectUrl,
                            // eslint-disable-next-line camelcase
                            data: {
                                // eslint-disable-next-line camelcase
                                user_id: userId,
                                // eslint-disable-next-line camelcase
                                survey_id: surveyId,
                                // eslint-disable-next-line camelcase
                                not_show: $('#not_show:checkbox:checked').length > 0
                            },
                            dataType: 'json',
                            headers: {
                                'celoe-api-key': celoeKey
                            },
                            error: function(jqXHR, textStatus) {
                                // eslint-disable-next-line no-console
                                console.log(textStatus);
                                // eslint-disable-next-line no-console
                                console.log(jqXHR);
                            }
                        });
                    }
                });

                $('#btn-survey-no').on('click', function() {
                    modal.destroy();
                });
            });
        }
    };
});