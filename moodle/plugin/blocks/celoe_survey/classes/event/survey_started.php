<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * block_celoe_survey survey started event.
 *
 * @package    survey_started
 * @copyright  2020 Andrey Budiharja
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_celoe_survey\event;

use core\event\base;
use stdClass;

defined('MOODLE_INTERNAL') || die();

/**
 * block_celoe_survey survey started event.
 *
 * @since     Moodle 3.9
 * @copyright 2020 Andrey Budiharja
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 **/
class survey_started extends base
{
    public static function get_name()
    {
        return get_string('eventsurveystarted', 'block_celoe_survey');
    }

    public static function get_legacy_eventname()
    {
        // Override ONLY if you are migrating events_trigger() call.
        return 'survey_started';
    }

    public function get_description()
    {
        return json_encode($this->other);
    }

    public function get_url()
    {
        return '';
    }

    public function get_legacy_logdata()
    {
        // Override if you are migrating an add_to_log() call.
        return array($this->courseid, 'block_celoe_survey', 'survey_started', '', $this->objectid, $this->contextinstanceid);
    }

    protected function init()
    {
        $this->data['crud'] = 'c'; // c(reate), r(ead), u(pdate), d(elete)
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'survey_started';
    }

    protected function get_legacy_eventdata()
    {
        // Override if you migrating events_trigger() call.
        $data = new stdClass();
        $data->id = $this->objectid;
        $data->userid = $this->relateduserid;
        return $data;
    }
}