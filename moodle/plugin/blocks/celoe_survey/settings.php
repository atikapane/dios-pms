<?php
if($ADMIN->fulltree){
    // setting celoe api key
    $settings->add(new admin_setting_configtext('block_celoe_survey/apikey',
        get_string('apikey', 'block_celoe_survey'),
        get_string('apikeydesc', 'block_celoe_survey'),
        '',
        PARAM_ALPHANUMEXT));

    // setting survey api
    $settings->add(new admin_setting_configtext('block_celoe_survey/surveyapi',
        get_string('surveyapi', 'block_celoe_survey'),
        get_string('surveyapidesc', 'block_celoe_survey'),
        'https://celoe.telkomuniversity.ac.id/api/v1/survey/today_survey',
        PARAM_URL));

    // setting platform
    $settings->add(new admin_setting_configselect('block_celoe_survey/platform',
        get_string('platform', 'block_celoe_survey'),
        get_string('platformdesc', 'block_celoe_survey'), 'MOOC', [
            'MOOC' => 'MOOC',
            'LMS' => 'LMS',
            'CDS' => 'CDS',
        ]));

    // setting survey moodle url
    $settings->add(new admin_setting_configtext('block_celoe_survey/surveyurl',
        get_string('surveyurl', 'block_celoe_survey'),
        get_string('surveyurldesc', 'block_celoe_survey'),
        'https://celoe.telkomuniversity.ac.id/survey',
        PARAM_URL));

    // setting survey not show api
    $settings->add(new admin_setting_configtext('block_celoe_survey/surveyreject',
        get_string('surveyreject', 'block_celoe_survey'),
        get_string('surveyreject', 'block_celoe_survey'),
        'https://celoe.telkomuniversity.ac.id/api/v1/survey/survey_reject',
        PARAM_URL));
}