<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function get_role_group($id)
	{
		$this->db->select('*');		
		$this->db->join('group as b','b.group_id=a.groupid');
		$this->db->join('user as c','c.id=a.userid');	
		$this->db->where('groupid',$id);	
		return $this->db->get('roles as a')->result();
	}

	public function get_group_in_role()
	{
		$this->db->select('group_id,group_name, count(groupid) as role');
		$this->db->from('group');
		$this->db->join('roles', 'group.group_id = roles.groupid','left');
		$this->db->group_by('group.group_id');
		return $query = $this->db->get()->result();		
	}

	public function get_user_active()
	{
		$this->db->select('*');		
		$this->db->where('is_active','1');		
		return $this->db->get('roles')->result();
	}

	public function get_byid($id)
	{
		$this->db->select('*');		
		$this->db->where('roles_id',$id);		
		return $this->db->get('roles')->row();
	}

	public function get_exist_byid($gid,$uid)
	{
		$this->db->select('*');		
		$this->db->where('groupid',$gid);		
		$this->db->where('userid',$uid);		
		return $this->db->get('roles')->num_rows();
	}

	public function get_exist_user($uid)
	{
		$this->db->select('*');		
		$this->db->where('userid',$uid);		
		return $this->db->get('roles')->num_rows();
	}

	public function get_user_byrole()
	{
		$this->db->select('*');		
		$this->db->where('role_id','3');		
		return $this->db->get('roles')->result();
	}

	public function get_count_user()
	{
		$this->db->select('*');		
		return $this->db->get('roles')->num_rows();
	}			
	
}
?>
