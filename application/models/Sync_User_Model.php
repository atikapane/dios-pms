<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sync_User_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}
	public function upsert($data){
            $user = $this->get_single("user","id='".$data["id"]."'","","id");
		if (!empty($user)){
                  $this->update("user",$data,array("id"=>$data["id"]));
            }
            else {
                  $this->create("user",$data);
            }
	}

      public function add_group($user_id,$position){
            if (!empty($user_id) && !empty($position)){
                  $roles = $this->get_single("roles","groupid=".$position." and userid=".$user_id);
                  if (empty($roles)){
                        $data = array(
                              "groupid" => $position,
                              "userid" => $user_id
                        );
                        $this->create("roles",$data);
                  }
            }
      }
	
	public function log_history($executor_id)
	{
		$this->db->insert("sync_user_history",array("user_id"=>$executor_id));
	}
	public function get_last_sync()
	{
		$this->db->select('*');					
		$this->db->join('user','user.id=sync_user_history.user_id');	
		$this->db->order_by('date','desc');	
		return $this->db->get('sync_user_history')->row();
	}
      public function get_token()
      {
            $sql = 'SELECT token FROM user WHERE username IN ("hidayanto","degunk","heriyonolalu") AND expired >= NOW() LIMIT 1';
            $query = $this->db->query($sql);
            $rowCount = $query->num_rows();
            if ($rowCount > 0){
                  $row = $query->row_array();
                  return $row["token"];
            }
      }

      public function get_single($table,$where="",$order="",$select="*")
      {
            $this->db->select($select);
            $this->db->from($table);
            if(!empty($where)){$this->db->where($where);}
            $this->db->limit(1);
            if(!empty($order)){$this->db->order_by($order);}
            $query = $this->db->get();
            return $query->row_array();
      }

      public function create($table,$data)
      {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
      }

      public function update($table,$data,$where)
      {
            $this->db->update($table,$data,$where);
            return $this->db->affected_rows();
      }

      public function get_data_export($table, $kt_query, $search_fields, $extra_where, $extra_where_or = false, $extra_join=false,$custom_select=false,$custom_group_by=false){

            $this->load->model('Datatable_model');
            $this->load->helper('export');
            $this->load->helper('query');
    
            $query = "";

            if (!empty($kt_query)){
                  $query = $kt_query;
            }

            if(is_array($kt_query) && array_key_exists("generalSearch", $kt_query)){
                $query = $kt_query["generalSearch"];
            }
            
            if(is_array($custom_select) && !empty($custom_select)){
                $this->db->select(implode(",", $custom_select));
            }
    
            $this->db->from($table);
             if($extra_join != false){
                foreach ($extra_join as $key => $value) {
    
                    $this->db->join($value->table,$value->condition,$value->join_type);
                }
            }
    
            if ($extra_where != false) {
                foreach ($extra_where as $key => $value) {
                    $this->db->where($value);
    
                }
            }
    
            if($extra_where_or != false){
                foreach ($extra_where_or as $key => $value) {
                    $this->db->or_where($value);
                }
            }
    
            $i = 0;
    
            $query = $this->security->xss_clean($query);
            $query = trim($query);
            $query = escape_allow_symbol($query);
    
            $filtered_column_search = array();
            foreach ($search_fields as $key=>$item){
                if($item == 'created_date'){
                    if(!strtotime($query)){
                        continue;
                    }
                }
                array_push($filtered_column_search,$item);
            }
    
            foreach ($filtered_column_search as $key=>$item) // loop column
            {
                if(!empty($query)) // if datatable send POST for search
                {
                    $adjusted_search_value = $query;
    
                    if($i===0){
                        $this->db->where('('.$item.' LIKE "%'.$adjusted_search_value.'%"');
                    }else if($i == (count($filtered_column_search) - 1) ) {
                        $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%")');
                    }else{
                        $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%"');
                    }
                }
                $i++;
            }
    
            if(!empty($custom_group_by)){
                $this->db->group_by($custom_group_by);
            }
            $dt_query = $this->db->get();
            $data =  $dt_query->result();
    
            $result = new stdClass();
            $result->data = $data;
            return $result;
        }

}
?>
