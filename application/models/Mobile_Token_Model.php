<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mobile_Token_Model extends CI_Model {

	public function generate($user_id)
	{
        $token = base64_encode(microtime());
		$data = array(
            'user_id' => $user_id,
            'token' => $token
         );
        $this->db->insert('mobile_token', $data);
        return $token;
	}

	public function validate($token)
	{       
        $this->db->select("users.*");
        $this->db->from('mobile_token');
        $this->db->join('users', 'users.id = mobile_token.user_id');
        $this->db->where('token', $token);

        $query = $this->db->get();
        
        return $query->row();
		
	}

}