<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function get_menu()
	{
		$this->db->select('*');		
		$this->db->join('icons as b','b.Id=a.menu_icon','left');		
		return $this->db->get('menu as a')->result();
	}

	public function get_menuicons()
	{
		$this->db->select('*');		
		return $this->db->get('icons')->result();
	}

	public function get_submenu($id)
	{
		$this->db->select('*');		
		$this->db->join('menu as b','b.menu_id=a.menuid');		
		$this->db->join('icons as c','c.Id=a.submenu_icon','left');		
		$this->db->where('b.menu_id',$id);		
		return $this->db->get('submenu as a')->result();
	}

	public function get_user_active()
	{
		$this->db->select('*');		
		$this->db->where('is_active','1');		
		return $this->db->get('menu')->result();
	}

	public function get_byid($id)
	{
		$this->db->select('*');		
		$this->db->where('menu_id',$id);		
		return $this->db->get('menu')->row();
	}

	public function get_exist_byid($id)
	{
		$this->db->select('*');		
		$this->db->where('menu_id',$id);		
		return $this->db->get('menu')->num_rows();
	}

	public function get_exist_submneu($menuid,$submenu)
	{
		$this->db->select('*');		
		$this->db->where('menuid',$menuid);		
		$this->db->where('submenu_id',$submenu);		
		return $this->db->get('submenu')->num_rows();
	}

	public function get_user_byrole()
	{
		$this->db->select('*');		
		$this->db->where('role_id','3');		
		return $this->db->get('menu')->result();
	}

	public function get_count_user()
	{
		$this->db->select('*');		
		return $this->db->get('menu')->num_rows();
	}

	public function get_count_operator()
	{
		$this->db->select('*');		
		$this->db->where('role_id','3');		
		$this->db->where('is_active','1');		
		return $this->db->get('menu')->num_rows();
	}			
	
	
	public function get_menu_by_submenu_link($submenu_link)
	{
		$this->db->select('*');		
		$this->db->from('submenu');		
		$this->db->join('menu','menu.menu_id=submenu.menuid');		
		$this->db->where('submenu.submenu_link',$submenu_link);	
		$result =  $this->db->get()->row();
		return $result;
	}

}
?>
