<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datatable_model extends CI_Model{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    private function _get_datatables_query($table, $column_search, $search_query, $column_order, $extra_where, $extra_where_or,$extra_join=false,$custom_select=false,$column_type=false,$group_by=false){

        if(is_array($custom_select) && !empty($custom_select)){
            $this->db->select(implode(",", $custom_select));
        }

        $this->db->from($table);
        if($extra_join != false){
            foreach ($extra_join as $key => $value) {

                $this->db->join($value->table,$value->condition,$value->join_type);
            }
        }


        if ($extra_where != false) {
            foreach ($extra_where as $key => $value) {
                $this->db->where($value);

            }
        }


        if($extra_where_or != false){
            foreach ($extra_where_or as $key => $value) {
                $this->db->or_where($value);
            }
        }

        $i = 0;

        $search_query = $this->security->xss_clean($search_query);
        $search_query = trim($search_query);
        $search_query = $this->escape_allow_symbol($search_query);

        $filtered_column_search = array();
        foreach ($column_search as $key=>$item){
            if($item == 'created_date'){
                if(!strtotime($search_query)){
                    continue;
                }
            }
            array_push($filtered_column_search,$item);
        }

        foreach ($filtered_column_search as $key=>$item) // loop column
        {
            if(!empty($search_query)) // if datatable send POST for search
            {
                $adjusted_search_value = $search_query;

                if($i===0){
                    $this->db->where('('.$item.' LIKE "%'.$adjusted_search_value.'%"');
                }else if($i == (count($filtered_column_search) - 1) ) {
                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%")');
                }else{
                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%"');
                }
            }
            $i++;
        }

        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }

        if(!empty($column_order)){
            $this->db->order_by($this->db->escape_str($column_order['field']), $column_order['direction']);
        }
    }

    public function get_data_export($table, $kt_query, $search_fields, $extra_where, $extra_where_or = false, $extra_join=false,$custom_select=false,$custom_group_by=false){

        $this->load->model('Datatable_model');
        $this->load->helper('export');
        $this->load->helper('query');

        $query = "";

        if (!empty($kt_query)){
              $query = $kt_query;
        }

        if(is_array($kt_query) && array_key_exists("generalSearch", $kt_query)){
            $query = $kt_query["generalSearch"];
        }
        
        if(is_array($custom_select) && !empty($custom_select)){
            $this->db->select(implode(",", $custom_select));
        }

        $this->db->from($table);
         if($extra_join != false){
            foreach ($extra_join as $key => $value) {

                $this->db->join($value->table,$value->condition,$value->join_type);
            }
        }

        if ($extra_where != false) {
            foreach ($extra_where as $key => $value) {
                $this->db->where($value);

            }
        }

        if($extra_where_or != false){
            foreach ($extra_where_or as $key => $value) {
                $this->db->or_where($value);
            }
        }

        $i = 0;

        $query = $this->security->xss_clean($query);
        $query = trim($query);
        $query = escape_allow_symbol($query);

        $filtered_column_search = array();
        foreach ($search_fields as $key=>$item){
            if($item == 'created_date'){
                if(!strtotime($query)){
                    continue;
                }
            }
            array_push($filtered_column_search,$item);
        }

        foreach ($filtered_column_search as $key=>$item) // loop column
        {
            if(!empty($query)) // if datatable send POST for search
            {
                $adjusted_search_value = $query;

                if($i===0){
                    $this->db->where('('.$item.' LIKE "%'.$adjusted_search_value.'%"');
                }else if($i == (count($filtered_column_search) - 1) ) {
                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%")');
                }else{
                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%"');
                }
            }
            $i++;
        }

        if(!empty($custom_group_by)){
            $this->db->group_by($custom_group_by);
        }
        $dt_query = $this->db->get();
        $data =  $dt_query->result();

        $result = new stdClass();
        $result->data = $data;
        return $result;
    }

    private function _get_datatables_count($table, $column_search, $column_order, $extra_where, $extra_where_or,$extra_join=false,$custom_select=false,$column_type=false)
    {
        $search_query = $this->input->post('search',true);

        if(empty($search_query)){
            $search_query = array("value"=>"");
        }
        $this->db->select("COUNT(1) as row_count");
        if ($custom_select != false) {

            $src = $this->security->xss_clean($search_query['value']);
            if(!empty($src))
            {
                if(strpos($this->lang->line('S01_001_9_log_state_1'), $src)!==false)
                {
                    $this->db->where('resv_conf_mail.send_state', 1);
                    $search_query['value']=null;
                }
                else if(strpos($this->lang->line('S01_001_9_log_state_0'), $src)!==false)
                {
                    $this->db->where('resv_conf_mail.send_state', 0);
                    $search_query['value']=null;
                }

            }
        }
        $this->db->from($table);

         if($extra_join != false){

            foreach ($extra_join as $key => $value) {

                $this->db->join($value->table,$value->condition,$value->join_type);
            }
        }


        if ($extra_where != false) {
            foreach ($extra_where as $key => $value) {
                $this->db->where($value);

            }
        }



        if($extra_where_or != false){
            foreach ($extra_where_or as $key => $value) {
                $this->db->or_where($value);
            }
        }

        $i = 0;
        $searchValue = $this->security->xss_clean($search_query['value']);
        
        $searchValue = trim($searchValue);
        $searchValue = $this->escape_allow_symbol($searchValue);
        $filtered_column_search = array();
        foreach ($column_search as $key=>$item){
            if($item == 'created_date'){
                if(!strtotime($searchValue)){

                    continue;
                }
            }
            array_push($filtered_column_search,$item);
        }

        foreach ($filtered_column_search as $key=>$item) // loop column
        {
            if($searchValue) // if datatable send POST for search
            {
                $adjusted_search_value = $searchValue;
                if($column_type){
                    $current_column_type = $column_type[$key];
                    if($current_column_type == "jp_date"){
                         //translate date time from JP standard to mysql standard, change / to -
                        $adjusted_search_value = str_replace('/', '-', $adjusted_search_value);

                    }
                }

                if($i===0){
                    $this->db->where('('.$item.' LIKE "%'.$adjusted_search_value.'%"');
                }else if($i == (count($filtered_column_search) - 1) ) {

                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%")');
                }else{
                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%"');
                }
            }
            $i++;
        }



    }


    public function get_datatables($table, $kt_pagination, $kt_sort, $kt_query, $search_fields, $extra_where, $extra_where_or = false, $extra_join=false,$custom_select=false,$custom_group_by=false, $limit = 10)
    {
        //get data
        $offset = 0;
        $current_page = 1;
        $query = "";

        $column_order = array("field"=>"","direction"=>"");
        if(is_array($kt_pagination) && array_key_exists("page", $kt_pagination) && array_key_exists("perpage", $kt_pagination)){
            $limit = $kt_pagination["perpage"];
            $current_page = $kt_pagination["page"];
            $offset = (($current_page -1) * $limit) ;
        }

        if(is_array($kt_sort) && array_key_exists("field", $kt_sort) && array_key_exists("sort", $kt_sort)){
            $column_order["field"] = $kt_sort["field"];
            $column_order["direction"] = $kt_sort["sort"];
        }

        if(is_array($kt_query) && array_key_exists("generalSearch", $kt_query)){
            $query = $kt_query["generalSearch"];
            $column_order["direction"] = $kt_sort["sort"];
        }

        $this->_get_datatables_query($table, $search_fields, $query, $column_order, $extra_where, $extra_where_or,$extra_join,$custom_select,false,$custom_group_by);
        $this->db->limit($limit, $offset);
        $dt_query = $this->db->get();
        $data =  $dt_query->result();
        //get meta

        $this->_get_datatables_query($table, $search_fields, $query, $column_order, $extra_where, $extra_where_or,$extra_join,$custom_select,false,$custom_group_by);
        $row_count = $this->db->count_all_results();

        $meta = new stdClass();
        $meta->page = $current_page;
        $meta->pages = ceil($row_count/$limit);
        $meta->perpage = $limit;
        $meta->total = $row_count;
        $meta->sort = $column_order["direction"];
        $meta->field = $column_order["field"];

        $result = new stdClass();
        $result->meta = $meta;
        $result->data = $data;
        return $result;
    }

    public function get_datatables_with_query($query, $column_search, $column_order, $is_array = true, $extra_where_col_bind = array(), $extra_where_val_bind = array(), $extra_where_or_col_bind = array(), $extra_where_or_val_bind = array(), $column_type=array())
    {
        $returnData = array();

        $draw_query = $this->input->post('draw',true);
        $length_query = $this->input->post('length',true);
        $start_query = $this->input->post('start',true);
        if(isset($draw_query)){
            $draw_query = $this->security->xss_clean($draw_query);
        }

        $whereClause = array();
        $valueBindWhereClause = array();

        if (!empty($extra_where_col_bind)) {
            array_push($whereClause, implode(" AND ", $extra_where_col_bind));
        }

        if (!empty($extra_where_or_col_bind)) {
            array_push($whereClause, '(' . implode(" OR ", $extra_where_or_col_bind) . ')');
        }

        $valueBindWhereClause = array_merge($extra_where_val_bind, $extra_where_or_val_bind);
        $search_query = $this->input->post('search',true);
        if(empty($search_query)){
            $search_query = array("value"=>"");
        }

        $searchValue = $this->security->xss_clean($search_query['value']);
        
        $searchValue = trim($searchValue);

        if($searchValue) // if datatable send POST for search
        {
            $searchWhere = array();
            foreach ($column_search as $key=>$item) // loop column
            {
                $adjusted_search_value = $searchValue;
                if($column_type){
                    $current_column_type = $column_type[$key];
                    if($current_column_type == "jp_date"){
                         //translate date time from JP standard to mysql standard, change / to -
                        $adjusted_search_value = str_replace('/', '-', $adjusted_search_value);

                    }
                }


                array_push($searchWhere, $item . ' LIKE ?');
                array_push($valueBindWhereClause, '%'.$this->escape_allow_symbol($adjusted_search_value).'%');
            }
            if (!empty($searchWhere)) {
                array_push($whereClause, '(' . implode(" OR ", $searchWhere) . ')');
            }
        }

        $whereClause = implode(" AND ", $whereClause);

        $orderByClause = array();
        if(isset($_POST['order'])){
            $order =  $this->security->xss_clean($_POST['order']);

            foreach ($order as $o) {
                if ($column_order[$o['column']])
                {
                    foreach (explode(",", $column_order[$o['column']]) as $col) {
                        array_push($orderByClause, $col . ' ' . $o['dir']);
                    }
                }

            }
        }

        $orderByClause = implode(", ", $orderByClause);

        if (!empty($whereClause)) {
            $query .= ' WHERE ' . $whereClause;
        }

        $returnData['count_filtered'] = $this->db->query($query, $valueBindWhereClause)->num_rows();

        if (!empty($orderByClause)) {
            $query .= ' ORDER BY ' . $orderByClause;
        }

        if($length_query != -1){
            $query .= ' LIMIT ' . $length_query . ' OFFSET ' . $start_query;
        }

        $result = $this->db->query($query, $valueBindWhereClause);
        if ($is_array)
            $returnData['data'] = $result->result_array();
        else
            $returnData['data'] = $result->result();

        return $returnData;
    }

    public function count_all_with_query($queryTotal, $extra_where_val_bind = array())
    {
        return $this->db->query($queryTotal, $extra_where_val_bind)->row()->total;
    }

    function count_filtered($table, $column_search, $column_order, $extra_where, $extra_where_or = false,$extra_join=false,$custom_select=false,$column_type=false){
        $this->_get_datatables_count($table, $column_search, $column_order, $extra_where, $extra_where_or,$extra_join,$custom_select,$column_type);
        $query = $this->db->get()->row();

        if(!empty($query))
            return $query->row_count;
        else
            return 0;
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function remove_emoji($text){
        $newstr = "";
        if (!empty($text)) {
            $newstr = preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', $text);
            if (empty($newstr)) {
                $newstr = " ";
            }
        }
        return $newstr;
    }

    function get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order = false, $filter = false, $column_search=null, $column_type=false,$start=false,$limit=false, $group_by=false){
        if ($custom_select != false) {
            $this->db->select($custom_select);
        }
        $this->db->from($table);
        if($extra_join != false){
            foreach ($extra_join as $key => $value) {
                $this->db->join($value->table,$value->condition,$value->join_type);
            }
        }

        if ($extra_where != false) {
            foreach ($extra_where as $key => $value) {
                $this->db->where($value);
            }
        }

        if($extra_where_or != false){
            foreach ($extra_where_or as $key => $value) {
                $this->db->or_where($value);
            }
        }


        if ($order != false) {
            $this->db->order_by($order);
        }

        if($group_by != false){
            $this->db->group_by($group_by);
        }

        if ($filter != false) {
            $i = 0;
            $searchValue = $filter;
           
            $searchValue = trim($searchValue);
            $searchValue = $this->escape_allow_symbol($searchValue);
            foreach ($column_search as $key=>$item) // loop column
            {
                if($searchValue) // if datatable send POST for search
                {
                    $adjusted_search_value = $searchValue;
                    if($column_type){
                        $current_column_type = $column_type[$key];
                        if($current_column_type == "jp_date"){
                             //translate date time from JP standard to mysql standard, change / to -
                            $adjusted_search_value = str_replace('/', '-', $adjusted_search_value);

                        }
                    }
                    if($i===0){
                        $this->db->where('('.$item.' LIKE "%'.$adjusted_search_value.'%"');
                    }else if($i == (count($column_search) - 1) ) {
                        $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%")');
                    }else{
                        $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%"');
                    }
                }
                $i++;
            }
        }
        if($start !== false && $limit !== false){

            $this->db->limit($limit,$start);
        }

        $query = $this->db->get();

        return $query->result();

    }
    //custom escape_str to allow symbol search
    private function escape_allow_symbol($str){
        $like_escape_chr = '!';

        if (is_array($str))
        {
            foreach ($str as $key => $val)
            {
                $str[$key] = $this->escape_allow_symbol($val);
            }

            return $str;
        }

        $str = remove_invisible_characters($str, FALSE);
        $str = str_replace("'", "\'", $str);
        $str = str_replace("%", "\%", $str);
        
        return $str;
    }
}
