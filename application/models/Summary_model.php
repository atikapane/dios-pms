<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Summary_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function get_person_statistic($month,$year,$user,$project){
		$this->db->select("report.id, 
							report.user_id, 
							report.project , 
							user.fullname , 
							project.nama_project,
							SUM(TIME_TO_SEC(`hours`)) as totaltime");
		$this->db->join("user", "user.id = report.user_id", "left");
		$this->db->join("project", "project.id = report.project", "left");
        

		if(!empty($month)){
			$this->db->where("MONTH(`date`)",$month);
		}

		if(!empty($year)){
			$this->db->where("YEAR(`date`)",$year);
		}
		
		if(!empty($project)){
			$this->db->where("project",$project);
		}

		if(!empty($user)){
			$this->db->where("user_id",$user);
			$this->db->group_by("report.project, report.user_id");
		} else {
			$this->db->group_by("report.user_id");
		}

		return $this->db->get("report")->result();
	}
	
	public function get_project_statistic($month,$year,$project,$user){
		$this->db->select("report.id as report_id, report.user_id as user_id, 
							report.project as project_id, user.fullname as fullname, 
							project.nama_project as project_name, SUM(TIME_TO_SEC(hours)) as time");
        $this->db->join('project', "project.id = report.project", "left");
        $this->db->join('user', "user.id = user_id", "left");
        
		if(!empty($month)){
			$this->db->where("MONTH(`date`)",$month);
		}
		if(!empty($year)){
			$this->db->where("YEAR(`date`)",$year);
		}
		if(!empty($project)){
            $this->db->where("report.project",$project);
            $this->db->group_by("report.user_id, report.project");
		} else {
            $this->db->group_by("report.project");
		}
		if(!empty($user)){
			$this->db->where("user_id",$user);
		}

        return $this->db->get('report')->result();
	}

}
?>
