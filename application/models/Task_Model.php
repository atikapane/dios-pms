<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Task_Model extends CI_Model {

	/**
	 * @author : Hidayanto
	 * @keterangan : Model untuk Task Report
	 **/

	public function count_data($table,$where="")
    {
        $this->db->from($table);
		if(!empty($where)){$this->db->where($where);}
        return $this->db->count_all_results();
    }

    public function sum_data($table,$field,$alias,$where="")
    {
        $query = $this->db->select_sum($field, $alias);
        $query = $this->db->where($where);
	    $query = $this->db->get($table);
	    $result = $query->result();
	    $value = $result[0]->$alias;
	    return ($value > 0 ? $value : 0);
    }
 
	public function get($table,$where="",$group="",$order="",$limit="")
    {
        $this->db->from($table);
		if(!empty($where)){$this->db->where($where);}
		if(!empty($group)){$this->db->group_by($group);}
		if(!empty($order)){$this->db->order_by($order);}
		if(!empty($limit)){$this->db->limit($limit);}
        $query = $this->db->get();
        return $query->result_array();
    }
	
    public function get_single($table,$where="",$order="")
    {
        $this->db->from($table);
        if(!empty($where)){$this->db->where($where);}
		$this->db->limit(1);
		if(!empty($order)){$this->db->order_by($order);}
        $query = $this->db->get();
        return $query->row_array();
    }
	
	public function get_join($select,$table1,$table2,$join,$param="",$where="",$order="", $limit="")
	{
		$this->db->select($select);
		$this->db->from($table1);
		$this->db->join($table2, $join,$param);
		if(!empty($where)){$this->db->where($where);}
		if(!empty($limit)){$this->db->limit($limit);}
		if(!empty($order)){$this->db->order_by($order);}
		$query = $this->db->get(); 
		return $query->result_array();
	}

	public function create($table,$data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

	public function update($table,$data,$where)
    {
        $this->db->update($table,$data,$where);
        return $this->db->affected_rows();
    }

    public function update_escape($table,$data,$where)
    {
        foreach($data as $key => $value){
        	$this->db->set($key, $value, FALSE);
        }
        $this->db->where($where);
        $this->db->update($table);
        return $this->db->affected_rows();
    }
	
	public function upsert($table,$data,$where)
    {
        $check = $this->get_single($table,$where);
        if (!empty($check)){
			return $this->update($table,$data,array("id"=>$check["id"]));
		}
		else {
			return $this->create($table,$data);
		}
    }
 
    public function delete($table,$where)
    {
        $this->db->where($where);
        $this->db->delete($table);
		return $this->db->affected_rows();
    }

    public function delete_file($path)
    {
		unlink($path);
    }

    public function get_data_course_progress_statistic($date)
    {
    	$subject_list = array();
    	if (!empty($date)){
    		$sql = "select a.subject_id from course_development a join sync_course b on a.subject_id = b.subject_id where b.is_deleted = 0 and a.status_course > 0 and a.status_approve = 0 group by a.subject_id";
    		$query = $this->db->query($sql);
    		$rowCount = $query->num_rows();
			if ($rowCount > 0){
				$subject_list = array();
				$result = $query->result_array();
				foreach($result as $row){
					$subject_list[] = $row["subject_id"];
				}
			}
    	}
    	return $subject_list;
    }

    public function get_data_course_attendance($semester_active)
    {
    	if (!empty($semester_active)){
    		$course_list = array();
    		$sql = "select course_id from sync_lms_course where semester = '".$semester_active."' and last_sync is not null";
    		$query = $this->db->query($sql);
    		$rowCount = $query->num_rows();
			if ($rowCount > 0){
				$course_list = array();
				$result = $query->result_array();
				foreach($result as $row){
					$course_list[] = $row["course_id"];
				}
			}
	    	return $course_list;
    	}
    	
    }

    public function get_sync_course_queue($date,$limit)
    {
        if (!empty($date) && !empty($limit)){
            $sql = 'select history.id as id,course.course_id as course_id,subject.subject_id as subject_id,subject.subject_code as subject_code,curriculum_year from sync_lms_course_teaching_history history join sync_lms_course course on history.course_id = course.course_id join sync_course subject on course.subject_id = subject.subject_id where substr(history.date,1,10) >= "'.$date.'" and history.status = 0 order by history.id asc limit '.$limit;
            $query = $this->db->query($sql);
            $rowCount = $query->num_rows();
            if ($rowCount > 0){
                return $query->result();
            }
        }
        
    }

}