<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	/**
	 * @author : Hidayanto
	 * @keterangan : Model untuk Auth, bisa digunakan juga untuk General Query
	 **/

	public function do_auth($u,$p)
	{
		if (!empty($u) && !empty($p)){
			$user = $this->get_single("user",array("username" => $u));
			if (!empty($user)){
				if (md5($p.$user["salt"]) == $user["password"]){
					$id = $user["id"];
					if (!empty($id)){
						return array(
							"user_data" => array(
								"user_id" => $user["id"],
								"employeeid" => $user["employeeid"],
								"fullname" => $user["fullname"],
								"email" => $user["email"]
							),
							"token" => $user["token"],
							"expired" => $user["expired"],
							"is_manual_insert" => $user['is_manual_insert']
						);
					}
				}
			}
		}
	}

	public function do_register($data)
	{
		if (!empty($data)){
			$data["salt"] = hash('sha256', $this->general->generate_random_string());
			$data["password"] = md5($data["password"].$data["salt"]);
			if ($this->upsert("user",$data)){
				$insert = array(
					"user_id" => $data["id"],
					"log_type" => 0,
					"log_value" => "Sync",
					"log_date" => $data["last_sync"]
				);
				$this->create("user_logging",$insert);
				return array(
					"user_data" => array(
						"user_id" => $data["id"],
						"employeeid" => $data["employeeid"],
						"fullname" => $data["fullname"],
						"email" => $data["email"]
					),
					"token" => $data["token"],
					"expired" => $data["expired"],
				);
			}

		}
	}
	
	public function count_data($table,$where="")
    {
        $this->db->from($table);
		if(!empty($where)){$this->db->where($where);}
        return $this->db->count_all_results();
    }

    public function sum_data($table,$field,$alias,$where="")
    {
        $query = $this->db->select_sum($field, $alias);
        $query = $this->db->where($where);
	    $query = $this->db->get($table);
	    $result = $query->result();
	    $value = $result[0]->$alias;
	    return ($value > 0 ? $value : 0);
    }
 
	public function get($table,$where="",$group="",$order="",$limit="")
    {
        $this->db->from($table);
		if(!empty($where)){$this->db->where($where);}
		if(!empty($group)){$this->db->group_by($group);}
		if(!empty($order)){$this->db->order_by($order);}
		if(!empty($limit)){$this->db->limit($limit);}
        $query = $this->db->get();
        return $query->result_array();
    }
	
    public function get_single($table,$where="",$order="")
    {
        $this->db->from($table);
        if(!empty($where)){$this->db->where($where);}
		$this->db->limit(1);
		if(!empty($order)){$this->db->order_by($order);}
        $query = $this->db->get();
        return $query->row_array();
    }
	
	public function get_join($select,$table1,$table2,$join,$param="",$where="",$order="", $limit="")
	{
		$this->db->select($select);
		$this->db->from($table1);
		$this->db->join($table2, $join,$param);
		if(!empty($where)){$this->db->where($where);}
		if(!empty($limit)){$this->db->limit($limit);}
		if(!empty($order)){$this->db->order_by($order);}
		$query = $this->db->get(); 
		return $query->result_array();
	}

	public function create($table,$data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

	public function update($table,$data,$where)
    {
        $this->db->update($table,$data,$where);
        return $this->db->affected_rows();
    }

    public function update_escape($table,$data,$where)
    {
        foreach($data as $key => $value){
        	$this->db->set($key, $value, FALSE);
        }
        $this->db->where($where);
        $this->db->update($table);
        return $this->db->affected_rows();
    }
	
	public function upsert($table,$data)
    {
        $sql = "select id from user where id = ?";
    	$query = $this->db->query($sql, $data["id"]);
    	$rowCount = $query->num_rows();
		if ($rowCount > 0){
			return $this->update("user",$data,array("id"=>$data["id"]));
		}
		else {
			return $this->create("user",$data);
		}
    }
 
    public function delete($table,$where)
    {
        $this->db->where($where);
        $this->db->delete($table);
		return $this->db->affected_rows();
    }

    public function delete_file($path)
    {
		unlink($path);
    }

}
