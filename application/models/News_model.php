<?php
/**
 * Created by PhpStorm.
 * User: djakapermana
 * Date: 19/11/19
 * Time: 09.21
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends CI_Model
{
    public function __construct()
    {
    	parent::__construct();
    }

    function get_data_news($search, $start, $limit){
        $this->db->select('*');
        $this->db->from('news');
        $this->db->where('deleted_at', null);
        if(empty($search)){
            $this->db->limit($limit,$start);
        }else{
            $this->db->like('title',$search);
        }
        $this->db->order_by('updated_at', 'desc');
        $query = $this->db->get();

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return array();
        }
    }

    function get_all_data_news(){
        $this->db->select('*');
        $this->db->from('news');
        $this->db->where('deleted_at', null);
        $query = $this->db->get()->result();
        return $query;
    }

    function get_thumbnail($thumb_id){
        $this->db->select('thumbnails.*');
        $this->db->from('thumbnails');
        $this->db->where('thumb_id', $thumb_id);
        $query = $this->db->get();

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return array();
        }
    }

    function save_thumbnail($data){
        $this->db->insert('thumbnails', $data);
    }

    function save_data_news($data){
        $this->db->trans_start();
        $this->db->insert('news', $data);
        $this->db->trans_complete();
        $status = $this->db->trans_status();
        return $status;
    }

    function get_data_news_detail($slug){
        $this->db->select('news.*,thumbnails.thumb_name');
        $this->db->from('news');
        $this->db->join('thumbnails', 'news.thumbnail = thumbnails.thumb_id', 'left');
        $this->db->where('slug', $slug);
        $this->db->where('deleted_at', null);
        $query = $this->db->get();

        if($query->num_rows()>0){
            return $query->result();
        }else{
            return array();
        }
    }

}