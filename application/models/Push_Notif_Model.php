<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Push_Notif_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function create($user_token)
    {
        if (!empty($user_token)) {
            $this->db->insert('push_notif_token', $user_token);
            return true;
        }
        return false;
    }

    function delete($token)
    {
       $this->db->delete('push_notif_token', array('token' => $token));
    }

    function get($user_id)
    {
        $this->db->select('*');
        $this->db->from('push_notif_token');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();

        return $query->result();
    }

    function get_count($user_id)
    {
        $this->db->select('count(*)');
        $this->db->from('push_notif_token');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();

        return $query->result();
    }
}
