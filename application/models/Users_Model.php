<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function get_user()
	{
		$this->db->select('*');		
		$this->db->order_by('id','asc');		
		return $this->db->get('user')->result();
	}

	public function get_user_byid($id)
	{
		$this->db->select('*');			
		$this->db->where('id',$id);		
		return $this->db->get('user')->row();
	}

	public function get_exist_user_byid($id)
	{
		$this->db->select('*');		
		$this->db->where('id',$id);		
		return $this->db->get('user')->num_rows();
	}			
	
	public function get_user_roles($id)
	{
		$this->db->select('*');		
		$this->db->join('group',"roles.groupid = group.group_id");
		$this->db->where('userid',$id);		
		return $this->db->get('roles')->result();
	}	

	public function get_all_menus()
	{
		$this->db->select('*');		
		$this->db->join('submenu',"submenu.menuid = menu.menu_id");	
		return $this->db->get('menu')->result();
	}

	public function checkUser($data = array()){
		$this->db->select('id');
		$this->db->from('users');
		$con = array(
			'oauth_provider' => $data['oauth_provider'],
			'oauth_uid' => $data['id']
		);
		$this->db->where($con);
		$query = $this->db->get();

		$check = $query->num_rows();
		if($check > 0){
			$result = $query->row_array();
			$this->db->update('users', $con, array('id' => $result['id']));
			$userID = $result['id'];
		}else{
			return false;
		}
		return $userID?$userID:false;
	}

	public function checkUserAuth($data = array()){
		$this->db->select('*');
		$this->db->from('users');
		$con = array(
			'oauth_provider' => $data['oauth_provider'],
			'oauth_uid' => $data['id'],
			'email'=>$data['email']
		);
		$this->db->where($con);
		$query = $this->db->get();
		$check = $query->num_rows();

		if($check == 0){
			$get_user = $this->checkUserByEmail($data['email']);
			if(!empty($get_user)) {
				return false;
			}
			return true;
		}
		return false;
	}

	public function checkUserByEmail($email){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email',$email);
		$query = $this->db->get();

		return $query->row_array();
	}
}
?>
