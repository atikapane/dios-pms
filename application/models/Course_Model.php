<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_valid_course($course_id)
    {
        $now = date('Y-m-d');
        $this->db->from("ocw_course_joined");
        $this->db->where('id', $course_id);
        $this->db->where('status', 3);
        // $this->db->where('start_date <=', $now);
        $this->db->where('end_date >=', $now);
        $query = $this->db->get();
        return $query->row();
    }

}