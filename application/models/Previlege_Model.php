<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Previlege_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
    }
    
    public function get_menu()
	{
		$this->db->select('*');		
		return $this->db->get('menu')->result();
	}

	public function get_exist_previlege($id)
	{
		$this->db->select('*');		
		$this->db->where('groupid',$id);		
		return $this->db->get('previleges')->num_rows();
	}	
	
	public function get_exist_menuprevilege($uid,$menuid)
	{
		$this->db->select('*');		
		$this->db->where('groupid',$uid);		
		$this->db->where('menuid',$menuid);		
		return $this->db->get('previleges')->num_rows();
	}	

	public function get_privilege($submenu_id,$user_id){
		$this->db->select('create,read,update,delete');		
		$this->db->join('roles',"user.id = roles.userid");		
		$this->db->join('previleges',"previleges.groupid = roles.groupid");
		$this->db->join('group',"roles.groupid = group.group_id");
		$this->db->where('user.id',$user_id);
		$this->db->where('submenu_id',$submenu_id);	
		$result =  $this->db->get('user')->result();
		return $result;
	}

	public function get_clearance_level($user_id){
		$this->db->select('max(group.group_level) as cleareance_level');
		$this->db->join('group','roles.groupid = group.group_id');
		$this->db->where('userid',$user_id);		
		$result =  $this->db->get('roles')->row();
		if(!empty($result)){
			return intval($result->cleareance_level);
		}else{
			return 0;
		}
	}

	public function get_structure_attribute($user_id){
		$this->db->select('c_directorate_id,c_unit_id');
		$this->db->where('id',$user_id);		
		$result =  $this->db->get('user')->row();
		return $result;
	}

	public function get_allowed_menu($user_id){
		$this->db->select('menu.*');		
		$this->db->join('roles',"user.id = roles.userid");		
		$this->db->join('previleges',"previleges.groupid = roles.groupid");		
		$this->db->join('submenu',"submenu.submenu_id = previleges.submenu_id");		
		$this->db->join('menu',"menu.menu_id = submenu.menuid");		
		$this->db->where('user.id',$user_id);		
		$this->db->where('(previleges.read = 1 OR previleges.create = 1 OR previleges.update = 1 OR previleges.delete = 1 )');		
		$this->db->group_by('menu.menu_id');
		$this->db->order_by('menu.priority','asc');
		$result =  $this->db->get('user')->result();
		return $result;
	}
	public function get_allowed_submenu($user_id,$menu_id){
		$this->db->select('submenu.*');		
		$this->db->join('roles',"user.id = roles.userid");		
		$this->db->join('previleges',"previleges.groupid = roles.groupid");		
		$this->db->join('submenu',"submenu.submenu_id = previleges.submenu_id");		
		$this->db->where('user.id',$user_id);		
		$this->db->where('previleges.read',1);		
		$this->db->where('submenu.menuid',$menu_id);
		$this->db->group_by('submenu.submenu_id');
		$this->db->order_by('submenu.priority','asc');		
		$result =  $this->db->get('user')->result();
		return $result;
	}
	
	public function is_super_admin($user_id,$super_admin_group_id){
		$this->db->select('roles_id');		
		$this->db->where('groupid',$super_admin_group_id);
		$this->db->where('userid',$user_id);	
		$this->db->from('roles');	
		$result =  $this->db->count_all_results();
		return $result > 0;
	}

}
?>
