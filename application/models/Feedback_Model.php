<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback_Model extends CI_Model
{

    /**
     * Feedback_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function get_feedback($limit = "", $start = "", $course_id = "")
    {
        $sql = 'SELECT feedbacks.id as feedback_id, user_id, course_id, rating, feedback, created_date, ocw_name, type, start_date, end_date, price, discount, banner_image, lecturer_photo, lecturer_name, lecturer_profile, course_profile, description, certification, status, created_by, created_at, subject_id, subject_code, subject_name, subject_type, subject_ppdu, credit, curriculum_year, sync_by, sync_status, sync_date, flag_status, table_owner, table_id, category_id, studyprogramid, approve_status, approve_date, notes, approve_by, input_by, input_date, last_backup, is_manual_insert, full_name as user_fullname
                FROM feedbacks
                LEFT JOIN ocw_course ON feedbacks.course_id = ocw_course.id
                LEFT JOIN sync_course ON ocw_course.sync_course_id = sync_course.subject_id
                LEFT JOIN users ON feedbacks.user_id = users.id';

//        SELECT feedbacks.id as id, ocw_course.ocw_name as ocw_name, sync_course.subject_name as subject_name, sync_course.subject_code as subject_code, users.full_name as user_fullname, rating, feedback, feedbacks.created_date as created_date

        if (!empty($limit) && !empty($start))
            $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $start;

//        if (!empty($id)) {
//            $sql .= ' WHERE feedbacks.id = ' . $id;
//            $query = $this->db->query($sql);
//            return $query->row_array();
//        }

        if (!empty($course_id)) {
            $sql .= ' WHERE feedbacks.course_id = ' . $course_id;
            $query = $this->db->query($sql);
            return $query->result_array();
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_detail($id)
    {
        $sql = 'SELECT * from feedbacks WHERE id = ' . $id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }
}