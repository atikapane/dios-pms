<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_Model extends CI_Model
{

    /**
     * Feedback_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_export($table, $kt_query, $search_fields, $extra_where, $extra_where_or = false, $extra_join=false,$custom_select=false,$custom_group_by=false){

        $this->load->model('Datatable_model');
        $this->load->helper('export');
        $this->load->helper('query');

        $query = "";
        
        if (!empty($kt_query)){
              $query = $kt_query;
        }

        if(is_array($kt_query) && array_key_exists("generalSearch", $kt_query)){
            $query = $kt_query["generalSearch"];
        }
        
        if(is_array($custom_select) && !empty($custom_select)){
            $this->db->select(implode(",", $custom_select));
        }

        $this->db->from($table);
         if($extra_join != false){
            foreach ($extra_join as $key => $value) {

                $this->db->join($value->table,$value->condition,$value->join_type);
            }
        }

        if ($extra_where != false) {
            foreach ($extra_where as $key => $value) {
                $this->db->where($value);

            }
        }

        if($extra_where_or != false){
            foreach ($extra_where_or as $key => $value) {
                $this->db->or_where($value);
            }
        }

        $i = 0;

        $query = $this->security->xss_clean($query);
        $query = trim($query);
        $query = escape_allow_symbol($query);

        $filtered_column_search = array();
        foreach ($search_fields as $key=>$item){
            if($item == 'created_date'){
                if(!strtotime($query)){
                    continue;
                }
            }
            array_push($filtered_column_search,$item);
        }

        foreach ($filtered_column_search as $key=>$item) // loop column
        {
            if(!empty($query)) // if datatable send POST for search
            {
                $adjusted_search_value = $query;

                if($i===0){
                    $this->db->where('('.$item.' LIKE "%'.$adjusted_search_value.'%"');
                }else if($i == (count($filtered_column_search) - 1) ) {
                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%")');
                }else{
                    $this->db->or_where($item.' LIKE "%'.$adjusted_search_value.'%"');
                }
            }
            $i++;
        }

        if(!empty($custom_group_by)){
            $this->db->group_by($custom_group_by);
        }
        $dt_query = $this->db->get();
        $data =  $dt_query->result();

        $result = new stdClass();
        $result->data = $data;
        return $result;
    }
}