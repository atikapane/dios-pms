<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function get_reviewer($course_id)
	{
		$sql = "select b.fullname as fullname,IFNULL(c.total_score,0) as total_score from enrol_development a join `user` b on a.user_id = b.id left join course_review c on a.course_id = c.course_id and c.reviewer_id = b.id left join course_review d on a.course_id = d.course_id and d.id > c.id where a.type_id = '9' and d.id is null and a.course_id = $course_id order by a.id";
		$query = $this->db->query($sql);
    	if ($query->num_fields() > 0){
    		return $query->result();
    	}	
	}	
	public function get_reviewer_grouped($course_id)
	{
		$this->db->select('user.fullname,course_review.total_score');		
		$this->db->join('course_review','course_review.course_id = enrol_development.course_id and course_review.reviewer_id = enrol_development.user_id','LEFT');		
		$this->db->join('user',' user.id = enrol_development.user_id');		
		$this->db->where('enrol_development.course_id ',$course_id);	
		$this->db->where('type_id',9);//9 = reviewer	
		$this->db->group_by('enrol_development.course_id');
		return $this->db->get('enrol_development')->result();
	}
	public function get_reviewer_grouped_with_score_filter($course_id)
	{
		$this->db->select('user.fullname,course_review.total_score');		
		$this->db->join('course_review','course_review.course_id = enrol_development.course_id and course_review.reviewer_id = enrol_development.user_id');		
		$this->db->join('user',' user.id = enrol_development.user_id');		
		$this->db->where('enrol_development.course_id ',$course_id);	
		$this->db->where('type_id',9);//9 = reviewer	
		$this->db->group_by('enrol_development.course_id');
		return $this->db->get('enrol_development')->result();
	}

	public function get_course_development_by_study_program($study_program_id)
	{
		$this->db->select('sync_course.subject_id');		
		$this->db->join('course_development','course_development.subject_id = sync_course.subject_id');	
		$this->db->where('sync_course.category_id',$study_program_id);
		return $this->db->get('sync_course')->result();
	}	
		
	public function get_course_development_by_faculty($faculty_id)
	{
		$this->db->select('sync_course.subject_id');		
		$this->db->join('course_development','course_development.subject_id = sync_course.subject_id');	
		$this->db->where("sync_course.category_id IN (SELECT category_id FROM sync_category where sync_category.category_parent_id = $faculty_id and sync_category.category_type = \"STUDYPROGRAM\")",NULL,false);
		return $this->db->get('sync_course')->result();
	}	

	public function get_reviewed_course_development_by_study_program($study_program_id)
	{
		$this->db->select('sync_course.subject_id');		
		$this->db->join('course_development','course_development.subject_id = sync_course.subject_id');		
		$this->db->join('(select DISTINCT course_id from course_review) course_review',' course_development.id = course_review.course_id');	
		$this->db->where('sync_course.category_id',$study_program_id);
		return $this->db->get('sync_course')->result();
	}	


	public function get_reviewed_course_development_by_faculty($faculty_id)
	{
		$this->db->select('sync_course.subject_id');		
		$this->db->join('course_development','course_development.subject_id = sync_course.subject_id');		
		$this->db->join('(select DISTINCT course_id from course_review) course_review',' course_development.id = course_review.course_id');	
		$this->db->where("sync_course.category_id IN (SELECT category_id FROM sync_category where sync_category.category_parent_id = $faculty_id and sync_category.category_type = \"STUDYPROGRAM\")",NULL,false);
		return $this->db->get('sync_course')->result();
	}	

	public function get_aproved_course_development_by_study_program($study_program_id)
	{
		$this->db->select('sync_course.subject_id');		
		$this->db->join('course_development','course_development.subject_id = sync_course.subject_id');		
		$this->db->where('sync_course.category_id',$study_program_id);
		$this->db->where('course_development.status_approve',1);
		return $this->db->get('sync_course')->result();
	}	

	public function get_aproved_course_development_by_faculty($faculty_id)
	{
		$this->db->select('sync_course.subject_id');		
		$this->db->join('course_development','course_development.subject_id = sync_course.subject_id');		
		$this->db->where("sync_course.category_id IN (SELECT category_id FROM sync_category where sync_category.category_parent_id = $faculty_id and sync_category.category_type = \"STUDYPROGRAM\")",NULL,false);
		$this->db->where('course_development.status_approve',1);
		return $this->db->get('sync_course')->result();
	}	

	public function get_review_course()
	{
		$this->db->select('course_development.subject_id,subject_name,subject_code');		
		$this->db->join('sync_course','course_development.subject_id = sync_course.subject_id');	
		$this->db->order_by('sync_course.subject_name');
		$this->db->group_by('course_development.subject_id');
		return $this->db->get('course_development')->result();
	}	

	public function get_course_development_subject_statistics($tab,$curriculum,$year,$quarter,$faculty_id,$study_program_id,$subject_id,$approval_status,$user_id)
	{
		$this->db->select("course_development.id as course_development_id,`year`, `quarter`, study_program.`category_name` as category_name, `subject_code`, `subject_name`,`status_approve`, `date_approve`");		
		$this->db->join('sync_course',"`sync_course`.`subject_id` = `course_development`.`subject_id`");	
		$this->db->join('sync_category study_program',"`study_program`.`category_id` = `sync_course`.`category_id` and `study_program`.`category_type` = \"STUDYPROGRAM\" ","LEFT");
		$this->db->join('sync_category faculty',"`faculty`.`category_id` = `study_program`.`category_parent_id` and `faculty`.`category_type` = \"FACULTY\" ","LEFT");
		
		if($user_id != false){
			$this->db->join('enrol_development',"`enrol_development`.`course_id` = `course_development`.`id`");
			$this->db->where(array("enrol_development.user_id"=>$user_id,"user_id"=>2));
		}
		
		if(!empty($curriculum)){
			$this->db->where("sync_course.curriculum_year",$curriculum);
		}
		if(!empty($year)){
			$this->db->where("year",$year);
		}
		if(!empty($quarter)){
			$this->db->where("quarter",$quarter);
		}
		if(!empty($study_program_id)){
			$study_program_id = explode(",",$study_program_id);
			$this->db->where_in("study_program.category_id",$study_program_id);
		}
		if(!empty($faculty_id)){
			$this->db->where("faculty.category_id",$faculty_id);
		}
		if(!empty($subject_id)){
			$this->db->where("sync_course.subject_id",$subject_id);
		}
		if(!empty($approval_status)){
            if($approval_status == 1)
                	$this->db->where("status_approve",1);
            else if($approval_status == 2)  
                	$this->db->where("status_approve",0);
		
		}
		$this->db->group_by("course_development.id");
		if ($tab == "studyprogram"){
			$this->db->group_by("study_program.category_id"); 
		}
		if ($tab == "faculty"){
			$this->db->group_by("faculty.category_id");
		}
		return $this->db->get('course_development')->result();
	}	

	public function get_course_development_study_program_statistics($curriculum,$year,$quarter,$faculty_id,$study_program_id)
	{
		$this->db->select("course_development.id as course_development_id, year,quarter,faculty.category_name as faculty_name,study_program.category_name as study_program_name,study_program.category_id as study_program_id");		
		$this->db->join('course_development',"enrol_development.course_id = course_development.id");	
		$this->db->join('sync_course',"sync_course.subject_id = course_development.subject_id");	
		$this->db->join('sync_category study_program',"study_program.category_id = sync_course.category_id and study_program.category_type = \"STUDYPROGRAM\"","LEFT");	
		$this->db->join('sync_category faculty',"faculty.category_id = study_program.category_parent_id and faculty.category_type = \"FACULTY\"","LEFT");	

		if(!empty($curriculum)){
			$this->db->where("sync_course.curriculum_year",$curriculum);
		}
		if(!empty($year)){
			$this->db->where("year",$year);
		}
		if(!empty($quarter)){
			$this->db->where("quarter",$quarter);
		}
		if(!empty($study_program_id)){
			$this->db->where("study_program.category_id",$study_program_id);
		}
		if(!empty($faculty_id)){
			$this->db->where("faculty.category_id",$faculty_id);
		}
		$this->db->group_by("study_program.category_id");
		return $this->db->get('enrol_development')->result();
	}	

	public function get_course_development_faculty_statistics($curriculum,$year,$quarter,$faculty_id)
	{
		$this->db->select("year,quarter,faculty.category_name as faculty_name, faculty.category_id as faculty_id");		
		$this->db->join('sync_category study_program',"study_program.category_parent_id = faculty.category_id");	
		$this->db->join('sync_course',"sync_course.category_id = study_program.category_id");	
		$this->db->join('course_development',"course_development.subject_id = sync_course.subject_id");	
		$this->db->where("faculty.category_type","FACULTY");

		if(!empty($curriculum)){
			$this->db->where("sync_course.curriculum_year",$curriculum);
		}
		if(!empty($year)){
			$this->db->where("year",$year);
		}
		if(!empty($quarter)){
			$this->db->where("quarter",$quarter);
		}
		if(!empty($faculty_id)){
			$this->db->where("faculty.category_id",$faculty_id);
		}
		$this->db->group_by("faculty.category_id");
		return $this->db->get('sync_category faculty')->result();
	}				
	
	public function get_course_development_subject_progress_statistic($tab,$curriculum,$year,$quarter,$faculty_id,$study_program_id,$subject_id,$course_progress,$user_id)
	{
		
		if ($tab == "subject"){
			$this->db->select("`year`,`quarter`,`category_name`,`subject_code`,`subject_name`,`percent_progress`");	
			if($user_id != false){
				$this->db->join('enrol_development',"(`course_development`.`id` = `enrol_development`.`course_id` and `user_id` = \"$user_id\" and `type_id` = \"2\")");
			}
		}
		$this->db->join('sync_course',"`course_development`.`subject_id` = `sync_course`.`subject_id`");
		$this->db->join('course_development_progress_report course_report',"`course_development`.`subject_id` = `course_report`.`subject_id`");
		$this->db->join('sync_category study_program',"`study_program`.`category_id` = `sync_course`.`category_id` and `study_program`.`category_type` = \"STUDYPROGRAM\" ","LEFT");
		if ($tab == "studyprogram"){
			$this->db->select("`year`,`quarter`,`faculty`.`category_name` as faculty_name,`study_program`.`category_name` as studyprogram_name`,avg(`percent_progress`) as percent_progress");
			$this->db->join('sync_category faculty',"`faculty`.`category_id` = `study_program`.`category_parent_id` and `faculty`.`category_type` = \"FACULTY\" ","LEFT");
		}
		if ($tab == "faculty"){
			$this->db->select("`year`,`quarter`,`faculty`.`category_name` as faculty_name, avg(`percent_progress`) as percent_progress");
			$this->db->join('sync_category faculty',"`faculty`.`category_id` = `study_program`.`category_parent_id` and `faculty`.`category_type` = \"FACULTY\" ","LEFT");
		}
			
		if(!empty($curriculum)){
			$this->db->where("sync_course.curriculum_year",$curriculum);
		}
		if(!empty($year)){
			$this->db->where("year",$year);
		}
		if(!empty($quarter)){
			$this->db->where("quarter",$quarter);
		}
		if(!empty($faculty_id)){
			$this->db->where("study_program.category_parent_id",$faculty_id);
		}
		if(!empty($study_program_id)){
			$study_program_id = explode(',',$study_program_id);
			$this->db->where_in("study_program.category_id",$study_program_id);
		}
		if(!empty($subject_id)){
			$this->db->where("sync_course.subject_id",$subject_id);
		}
		if(!empty($course_progress)){
            if($course_progress == 1){
            	$this->db->where("percent_progress<=",25);
            }
            else if($course_progress == 2){
            	$this->db->where("percent_progress>",25);
            	$this->db->where("percent_progress<=",50); 
            }
            else if($course_progress == 3){ 
            	$this->db->where("percent_progress>",50);
            	$this->db->where("percent_progress<=",75); 
            }
            else if($course_progress == 4){ 
            	$this->db->where("percent_progress>",75);
            	$this->db->where("percent_progress<=",100); 
            }
        }

		if ($tab == "studyprogram"){
			$this->db->group_by("study_program.category_id"); 
		}
		if ($tab == "faculty"){
			$this->db->group_by("faculty.category_id");
		}

		$result = $this->db->get('course_development')->result_array();
		if (!empty($result)){
			return $result;
		}
	}

	public function get_course_teaching_course_attendance_statistic($tab,$semester,$faculty_id,$study_program_id,$course_id,$course_attendance,$user_id)
	{
		if ($tab == "course"){
			$this->db->select('`semester`,`subject_code`,`study_program.category_name` as `studyprogram_name`,`subject_name`,`class`,`attendance` as `percent_attendance`');
		}
		$this->db->join('(SELECT course_id,(SUM(attendance)/COUNT(id)*100) AS attendance FROM course_attendance_report GROUP BY course_id) report',"`sync_lms_course`.`course_id` = `report`.`course_id`");
		$this->db->join('sync_category schoolyear',"`schoolyear`.`category_id` = `sync_lms_course`.`category_id` and `schoolyear`.`category_type` = \"SCHOOLYEAR\" ","LEFT");
		$this->db->join('sync_category study_program',"`study_program`.`category_id` = `schoolyear`.`category_parent_id` and `study_program`.`category_type` = \"STUDYPROGRAM\" ","LEFT");
		$this->db->join('sync_category faculty',"`faculty`.`category_id` = `study_program`.`category_parent_id` and `faculty`.`category_type` = \"FACULTY\" ","LEFT");
		if ($tab == "studyprogram"){
			$this->db->select("`semester`,`faculty`.`category_name` as faculty_name,`study_program`.`category_name` as studyprogram_name`,avg(`attendance`) as percent_attendance");
		}
		if ($tab == "faculty"){
			$this->db->select("`semester`,`faculty`.`category_name` as faculty_name,`study_program`.`category_name` as studyprogram_name`,avg(`attendance`) as percent_attendance");
		}
		if(!empty($semester)){
			$this->db->where("semester",$semester);
		}
		if(!empty($faculty_id)){
			$this->db->where("faculty.category_id",$faculty_id);
		}
		if(!empty($study_program_id)){
			$study_program_id = explode(',',$study_program_id);
			$this->db->where_in("study_program.category_id",$study_program_id);
		}
		if(!empty($subject_id)){
			$this->db->where("sync_course.subject_code",$subject_id);
		}
		if(!empty($course_attendance)){
            if($course_attendance == 1){
            	$this->db->where("attendance<=",25);
            }
            else if($course_attendance == 2){
            	$this->db->where("attendance>",25);
            	$this->db->where("attendance<=",50); 
            }
            else if($course_attendance == 3){ 
            	$this->db->where("attendance>",50);
            	$this->db->where("attendance<=",75); 
            }
            else if($course_attendance == 4){ 
            	$this->db->where("attendance>",75);
            	$this->db->where("attendance<=",100); 
            }
        }
        if ($tab == "course"){
	        if($user_id != false){
				$this->db->where("user_id",$user_id);
			}
		}

        $this->db->where('last_sync is NOT NULL', NULL, FALSE);
		if ($tab == "studyprogram"){
			$this->db->group_by(array("study_program.category_id","semester"));
		}
		if ($tab == "faculty"){
			$this->db->group_by(array("faculty.category_id","semester"));
		}
		
		$result = $this->db->get('sync_lms_course')->result_array();
		if (!empty($result)){
			return $result;
		}
	}

	public function get_lms_course($semester,$faculty_id,$study_program_id,$user_id)
	{
		$this->db->select("sync_lms_course.*");		
		$this->db->join("sync_category as school_year","school_year.category_id = sync_lms_course.category_id","left");	
		$this->db->join("sync_category as study_program","study_program.category_id = school_year.category_parent_id","left");
		$this->db->join("sync_category as faculty","faculty.category_id = study_program.category_parent_id","left");	

		if(!empty($user_id)){
			$this->db->where("sync_lms_course.user_id",$user_id);
		}
		if(!empty($semester)){
			$this->db->where("sync_lms_course.semester",$semester);
		}
		if(!empty($study_program_id)){
			$this->db->where("study_program.category_id",$study_program_id);
		}
		if(!empty($faculty_id)){
			$this->db->where("faculty.category_id",$faculty_id);
		}
		$this->db->where('last_sync is NOT NULL', NULL, FALSE);
		$this->db->group_by("sync_lms_course.subject_code");
		return $this->db->get('sync_lms_course')->result();
	}	

}
?>
