<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();		
	}

	public function get_group()
	{
		$this->db->select('*');		
		$this->db->order_by('group_id','desc');		
		return $this->db->get('group')->result();
	}

	public function get_group_byid($id)
	{
		$this->db->select('*');		
		$this->db->where('group_id',$id);		
		return $this->db->get('group')->row();
	}

	public function get_exist_group_byid($id)
	{
		$this->db->select('*');		
		$this->db->where('group_id',$id);		
		return $this->db->get('group')->num_rows();
	}			
	
}
?>
