<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_Model extends CI_Model
{

    /**
     * Feedback_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function get_comment($limit = "", $start = "", $course_id = "", $parent_id = "")
    {
        $this->db->select('comments.id as comment_id, user_id, course_id, comment, parent_id, created_date, ocw_name, type, start_date, end_date, price, discount, banner_image, lecturer_photo, lecturer_name, lecturer_profile, course_profile, description, certification, status, created_by, created_at, subject_id, subject_code, subject_name, subject_type, subject_ppdu, credit, curriculum_year, sync_by, sync_status, sync_date, flag_status, table_owner, table_id, category_id, studyprogramid, approve_status, approve_date, notes, approve_by, input_by, input_date, last_backup, is_manual_insert, full_name as user_fullname');
        $this->db->from('comments');
        $this->db->join('ocw_course', 'comments.course_id = ocw_course.id');
        $this->db->join('sync_course', 'ocw_course.sync_course_id = sync_course.subject_id');
        $this->db->join('users', 'comments.user_id = users.id');

       

        if (!empty($course_id)) {
            $this->db->where('comments.course_id', $course_id);
        }

        if (!empty($parent_id)) {
            $this->db->where('comments.parent_id', $parent_id);
        }

        if (!empty($limit) && !empty($start)){
            $this->db->limit($limit, $start);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_detail($id)
    {
        $sql = 'SELECT * FROM comments WHERE id = ' . $id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }
}