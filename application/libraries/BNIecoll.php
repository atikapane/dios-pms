<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class BNIecoll
{
    private $tx_expire = 24;
    private $client_id = 0;
    private $secret_key = "";
    private $api_url = "";
    
    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        $this->CI->load->library('BniEnc');
        if(array_key_exists("client_id", $params)){
            $this->client_id = $params["client_id"];
        }
        if(array_key_exists("secret_key", $params)){
            $this->secret_key = $params["secret_key"];
        }
        if(array_key_exists("url", $params)){
            $this->api_url = $params["url"];
        }
    }

    public function generate_va($billing_id,$amount,$customer_name,$customer_email,$customer_phone,$expired_at){
        $param = array(
            'type' => 'createbilling',
            'client_id' => $this->client_id,
            'trx_id' => $billing_id,
            'trx_amount' => $amount,
            'billing_type' => 'c',
            'customer_name' => $customer_name,
            'customer_email' => $customer_email,
            'customer_phone' => $customer_phone,
            'virtual_account' => '',
            'datetime_expired' => $expired_at
        );

        $hashed_string = BniEnc::encrypt(
            $param,
            $this->client_id,
            $this->secret_key
        );

        $data = array(
            'client_id' => $this->client_id,
            'data' => $hashed_string,
        );

        $response = $this->get_content($this->api_url, json_encode($data));
        $response_json = json_decode($response, true);

        $request_result = new stdClass();
        $request_result->success = false;
        $request_result->status = $response_json['status'] ;
        $request_result->data = false;

        if ($response_json['status'] === '000') {
            $request_result->success = true;
            $request_result->data = BniEnc::decrypt($response_json['data'], $this->client_id, $this->secret_key);
        }else{
            $request_result->message = $response_json['message'] ;
        }

        return $request_result;
    }

    public function delete_va($billing_id,$amount,$customer_name,$customer_email,$customer_phone,$expired_at){
        $param = array(
            'type' => 'updatebilling',
            'client_id' => $this->client_id,
            'trx_id' => $billing_id,
            'trx_amount' => $amount,
            'customer_name' => $customer_name,
            'customer_email' => $customer_email,
            'customer_phone' => $customer_phone,
            'datetime_expired' => $expired_at
        );

        $hashed_string = BniEnc::encrypt(
            $param,
            $this->client_id,
            $this->secret_key
        );

        $data = array(
            'client_id' => $this->client_id,
            'data' => $hashed_string,
        );

        $response = $this->get_content($this->api_url, json_encode($data));
        $response_json = json_decode($response, true);

        $request_result = new stdClass();
        $request_result->success = false;
        $request_result->status = $response_json['status'] ;
        $request_result->data = false;

        if ($response_json['status'] === '000') {
            $request_result->success = true;
            $request_result->data = BniEnc::decrypt($response_json['data'], $this->client_id, $this->secret_key);
        }else{
            $request_result->message = $response_json['message'] ;
        }

        return $request_result;
    }

    public function inquiry_billing($billing_id){
        $param = array(
            'type' => 'inquirybilling',
            'client_id' => $this->client_id,
            'trx_id' => $billing_id,
        );

        $hashed_string = BniEnc::encrypt(
            $param,
            $this->client_id,
            $this->secret_key
        );

        $data = array(
            'client_id' => $this->client_id,
            'data' => $hashed_string,
        );

        $response = $this->get_content($this->api_url, json_encode($data));
        $response_json = json_decode($response, true);

        $request_result = new stdClass();
        $request_result->success = false;
        $request_result->status = $response_json['status'] ;
        $request_result->data = false;

        if ($response_json['status'] === '000') {
            $request_result->success = true;
            $request_result->data = BniEnc::decrypt($response_json['data'], $this->client_id, $this->secret_key);
        }else{
            $request_result->message = $response_json['message'] ;
        }

        return $request_result;
    }

    private function get_content($url, $post = '') {
        $usecookie = __DIR__ . "/cookie.txt";
        $header[] = 'Content-Type: application/json';
        $header[] = "Accept-Encoding: gzip, deflate";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        // curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

        if ($post)
        {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $rs = curl_exec($ch);

        if(empty($rs)){
            curl_close($ch);
            return false;
        }
        curl_close($ch);
        return $rs;
    }
    
    public function parse_callback(){
        $response_result = new stdClass();
        $response_result->success = false;
        $response_result->message = "";
        $response_result->data = false;
        $response_result->raw_data = false;

        $data = file_get_contents('php://input');
        $response_result->raw_data = $data;
        
        $data_json = json_decode($data, true);

        if (!$data_json) {
            // handling invalid request
            $response_result->message = '{"status":"999","message":"Invalid request"}';
        }else {
            if ($data_json['client_id'] === $this->client_id) {
                $decrypted_data = BniEnc::decrypt(
                    $data_json['data'],
                    $this->client_id,
                    $this->secret_key
                );

                if (!$decrypted_data) {
                     $response_result->message = '{"status":"999","message":"Invalid request : invalid server time or secret key"}';
                }else {
                    $response_result->success = true;
                    $response_result->message = '{"status":"000"}';
                    $response_result->data = $decrypted_data;
                }
            }
        }

        return $response_result;
    }
}