<?php defined('BASEPATH') OR exit('No direct script access allowed');

class General {

	public function get_date(){
		$dat_server = mktime(date("G"), date("i"), date("s"), date("n"), date("j"), date("Y"));
        $diff_gmt = substr(date("O",$dat_server),1,2);
        $dathif_gmt = 60 * 60 * $diff_gmt;
        if (substr(date("O",$dathif_gmt),0,1) == '+') {
            $dat_gmt = $dat_server - $dathif_gmt;
        } else {
            $dat_gmt = $dat_server + $dathif_gmt;
        }
        $dathif_id = 60 * 60 * 7;
		$dat_id = $dat_gmt + $dathif_id;
        $datetime = date("Y-m-d", $dat_id);
        return $datetime; 
	}

	public function get_datetime($factor="",$value=0){
		$dat_server = mktime(date("G"), date("i"), date("s"), date("n"), date("j"), date("Y"));
        $diff_gmt = substr(date("O",$dat_server),1,2);
        $dathif_gmt = 60 * 60 * $diff_gmt;
        if (substr(date("O",$dathif_gmt),0,1) == '+') {
            $dat_gmt = $dat_server - $dathif_gmt;
        } else {
            $dat_gmt = $dat_server + $dathif_gmt;
        }
        $dathif_id = 60 * 60 * 7;
		if (!empty($factor)){
			if ($factor == "plus"){
				$dat_id = $dat_gmt + $dathif_id + $value;
			}
			if ($factor == "minus"){
				$dat_id = $dat_gmt + $dathif_id - $value;
			}
		}
		else {
			$dat_id = $dat_gmt + $dathif_id;
		}
        $datetime = date("Y-m-d H:i:s", $dat_id);
        return $datetime; 
	}

	public function generate_random_string($length = 16) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	
    public function sanitize_input($input)
    {
    	$str = strip_tags($input); 
	    $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
	    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
	    $str = strtolower($str);
	    $str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
	    $str = htmlentities($str, ENT_QUOTES, "utf-8");
	    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
	    $str = str_replace(' ', '-', $str);
	    $str = rawurlencode($str);
	    $str = str_replace('%', '-', $str);
	    return $str;
    }
    
    public function parse_money($money)
	{
	    $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
	    $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

	    $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

	    $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
	    $removedThousandSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

	    return (float) str_replace(',', '.', $removedThousandSeparator);
	}

	public function split_name($name)
    {
        $exp = explode(' ', $name,2);
		if (count($exp)>1){
			list($firstname, $lastname) = $exp;
		}
		else {
			$firstname = $lastname = $exp[0];
		}
		return array($firstname,$lastname);
    }

}

?>