<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use mikehaertl\wkhtmlto\Pdf;

class WkHtmlToPdf{
    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function single_page_pdf($html_page, $filename){
        $pdf = new Pdf([
            'no-outline',
            'encoding' => 'UTF-8',
            'margin-top' => 0,
            'margin-right' => 0,
            'margin-bottom' => 0,
            'margin-left' => 0,

            'disable-smart-shrinking',

            // command option
            'ignoreWarnings' => true,
            'commandOptions' => ['useExec' => true],
            'binary' => $this->CI->config->item('wkhtmltopdf')
        ]);
        $pdf->addPage($html_page);
        $filepath = $this->CI->config->item('wkhtmltopdf_upload_path') .  $filename . '.pdf';

        if($pdf->saveAs($filepath)){
            return $filename . '.pdf';
        }else{
            return false;
        }
    }
}