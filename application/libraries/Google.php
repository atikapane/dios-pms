<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Google OAuth Library for CodeIgniter 3.x
 *
 * Library for Google+ login. It helps the user to login with their Google account
 * in CodeIgniter application.
 *
 * This library requires the Google API PHP client and it should be placed in third_party folder.
 *
 * It also requires google configuration file and it should be placed in the config directory.
 *
 * @package     CodeIgniter
 * @category    Libraries
 */
class Google{

    public function __construct(){

        $CI =& get_instance();
        $CI->config->load('google');

        require APPPATH .'third_party/vendor/autoload.php';

        $this->client = new Google_Client();
        $this->client->setApplicationName($CI->config->item('application_name', 'google'));
        $this->client->setClientId($CI->config->item('client_id', 'google'));
        $this->client->setClientSecret($CI->config->item('client_secret', 'google'));
        $this->client->setRedirectUri($CI->config->item('redirect_uri', 'google'));
        $this->client->addScope('email');
        $this->client->addScope('openid');
        $this->client->addScope('profile');
        $this->client->addScope(Google_Service_Oauth2::PLUS_ME);
        $this->client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
        $this->oatuh2 = new Google_Service_Oauth2($this->client);
    }

    public function loginURL() {
        return $this->client->createAuthUrl();
    }

    public function getAuthenticate($code) {
        return $this->client->authenticate($code);
    }

    public function getAccessToken() {
        return $this->client->getAccessToken();
    }

    public function setAccessToken($token) {
        return $this->client->setAccessToken($token);
    }

    public function revokeToken() {
        return $this->client->revokeToken();
    }

    public function getUserInfo() {
        return $this->oatuh2->userinfo->get();
    }

    public function setRedirectUri($uri){
        $this->client->setRedirectUri($uri);
    }

}