<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Push_notif
{
	public function __construct()
	{
		$CI =& get_instance();
		$CI->load->model('Push_Notif_Model');
	}

	public function send_notification($user_id, $title, $body)
	{
		$CI =& get_instance();
		$message = "";
		$CI->load->library('fcm');
		$CI->fcm->setTitle('');
		$CI->fcm->setMessage($message);
		$CI->fcm->setIsBackground(true);

		$notification = array();
		$notification['body'] = $body;
		$notification['title'] = $title;

		$payload = array('notification' => '');
		$CI->fcm->setPayload($payload);
		$json = $CI->fcm->getPush();

		//sent notification to all token from each user
		$push_notif_data = $CI->Push_Notif_Model->get($user_id);
		foreach ($push_notif_data as $value) {
			$CI->fcm->send($value->token, $json, $notification);
		}
	}
}
