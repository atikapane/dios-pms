<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class ShoppingCart
{
    public function __construct($params = array())
    {
        $this->CI =& get_instance();
    }
    
    function add($ocw_course_id,$user_id)
    {
        //check if exist
        $existing_item = $this->CI->db->get_where('user_cart', array('user_id' => $user_id,'course_id' => $ocw_course_id))->row();
        if(empty($existing_item)){
            $user_obj = $this->CI->db->get_where('users', array('id' => $user_id))->row();
            $course_obj = $this->CI->db->get_where('ocw_course', array('id' => $ocw_course_id))->row();
            if(!empty($user_obj) && !empty($course_obj)){
                $data = array(
                   'course_id' => $ocw_course_id,
                   'user_id' => $user_id
                );

                $this->CI->db->insert('user_cart', $data);
                return $this->CI->db->insert_id();
            }
        }

        return false;
    }

    function remove($ocw_course_id,$user_id)
    {
       $this->CI->db->delete('user_cart', array('course_id' => $ocw_course_id,'user_id' => $user_id));
    }

    function remove_by_id($id)
    {
       $this->CI->db->delete('user_cart', array('id' => $id));
    }

    function remove_all($user_id)
    {  
       $this->CI->db->delete('user_cart', array('user_id' => $user_id));
    }

    
    function get($user_id)
    {
        $now = date('Y-m-d');
        $this->CI->db->select('*, user_cart.id as cart_id');
        $this->CI->db->select('ocw_course_joined.ocw_name, ocw_course_joined.start_date, ocw_course_joined.end_date');
        $this->CI->db->from('user_cart');
        $this->CI->db->join('ocw_course_joined', 'user_cart.course_id = ocw_course_joined.id');
        $this->CI->db->where('user_id', $user_id);
        $this->CI->db->where('ocw_course_joined.status', 3);
        // $this->CI->db->where('ocw_course_joined.start_date <=', $now);
        $this->CI->db->where('ocw_course_joined.end_date >=', $now);

        $query = $this->CI->db->get();

        return $query->result();
    }
}