<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sync {
	public function __construct()
	{
		$CI =& get_instance();
		$CI->load->helper('log_api_external');
	}

	public function ws_category_sync($sync_data)
	{
		$return_data = array('status'=>true,'message'=>'');
		if (!empty($sync_data)){
			$url = $sync_data["url"];
            $wstoken = $sync_data["wstoken"];
            $action = $sync_data["action"];
            $status_exist = $status_crud = false;

            //start check category if exist
            $category_check_data = array(
        		'wstoken'=>$wstoken,
        		'wsfunction'=>'local_cds_api_get_categories',
        		'moodlewsrestformat'=>'json',
        		'categories'=>array(
					0=>array(
		        		'idnumber'=>$sync_data['category_id']
		        	)
				)
        	);
    		
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url.'?'.http_build_query($category_check_data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
					"Accept: */*",
				),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
			curl_close($curl);
			//end check category if exist
			
			if(!$err){
				$result = json_decode($response,true);
				$status_check = isset($result['status']) ? $result['status'] : '';
				if($status_check == 'success'){
					$status_exist = true;
					insert_log_external('API_EXTERNAL_CDS_CHECK_CATEGORY', $url, json_encode($category_check_data), $response,true);
				}
				else {
					insert_log_external('API_EXTERNAL_CDS_CHECK_CATEGORY', $url, json_encode($category_check_data), $response,false);
				}

				if($action == 'add'){
					if(!$status_exist){
						//start crud category
						$category_data = array(
							'wstoken'=>$wstoken,
			        		'wsfunction'=>'local_cds_api_create_categories',
			        		'moodlewsrestformat'=>'json',
							'categories'=>array(
								0=>array(
					        		'name'=>$sync_data['category_name'],
					        		'parent'=>$sync_data['category_parent_id'],
					        		'idnumber'=>$sync_data['category_id'],
					        		'description'=>$sync_data['category_desc'],
					        	)
				        	)
						);
						$log_key = 'API_EXTERNAL_CDS_CREATE_CATEGORY';
						$status_crud = true;
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Category ".$sync_data['category_name']." is already exist";		
					}
				}
				elseif ($action == 'edit') {
					if($status_exist){
						//start crud category
						$category_data = array(
							'wstoken'=>$wstoken,
			        		'wsfunction'=>'local_cds_api_update_categories',
			        		'moodlewsrestformat'=>'json',
							'categories'=>array(
								0=>array(
					        		'name'=>$sync_data['category_name'],
					        		'parent'=>$sync_data['category_parent_id'],
					        		'idnumber'=>$sync_data['category_id'],
					        		'description'=>$sync_data['category_desc'],
					        	)
				        	)
						);
						$log_key = 'API_EXTERNAL_CDS_UPDATE_CATEGORY';
						$status_crud = true;
					}
					else {

						$return_data["status"] = false;
						$return_data["message"] = "Category ".$sync_data['category_name']." is not exist";
					}
				}
				elseif ($action == 'delete') {
					if($status_exist){
						//start crud category
						$category_data = array(
							'wstoken'=>$wstoken,
			        		'wsfunction'=>'local_cds_api_delete_categories',
			        		'moodlewsrestformat'=>'json',
							'categories'=>array(
								0=>array(
					        		'idnumber'=>$sync_data['category_id'],
					        		'recursive'=>0,
					        	)
				        	)
						);
						$log_key = 'API_EXTERNAL_CDS_DELETE_CATEGORY';
						$status_crud = true;
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Category ".$sync_data['category_name']." is not exist";		
					}
				}

				if($status_crud){
					$curl = curl_init();
		            curl_setopt_array($curl, array(
		                CURLOPT_URL => $url.'?'.http_build_query($category_data),
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_MAXREDIRS => 10,
		                CURLOPT_TIMEOUT => 300,
		                CURLOPT_SSL_VERIFYHOST=> 0,
		                CURLOPT_SSL_VERIFYPEER=> 0,
		                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
		                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
		                CURLOPT_CUSTOMREQUEST => 'GET',
		                CURLOPT_HTTPHEADER => array(
							"Accept: */*",
						),
		            ));
		            $response = curl_exec($curl);
		            $err = curl_error($curl);
					curl_close($curl);

					if (!$err){
						$result = json_decode($response,true);
						$status_action = isset($result['status']) ? $result['status'] : '';
						if($status_action == 'success'){
							//do nothing
							insert_log_external($log_key, $url, json_encode($category_data), $response,true);
						}
						else {
							$return_data["status"] = false;
							if ($status_action == 'failed'){
								$return_data["message"] = !empty($result['exception']) ? $result['exception'] : 'Failed to '.$action.' category';
							}
							else {
								$return_data["message"] = 'Unknown Error : Failed to '.$action.' category';
							}
							insert_log_external($log_key, $url, json_encode($category_data), $response,false);
						}
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Unable to connect to Server";
						insert_log_external($log_key, $url, json_encode($category_data), $err,false);
					}
				}
				else{
					$return_data["status"] = false;
					$return_data["message"] = "Unknown parameter action";
				}
			}
			else {
				$return_data["status"] = false;
				$return_data["message"] = "Unable to connect to Server";
				insert_log_external('API_EXTERNAL_CDS_CHECK_CATEGORY', $url, json_encode($category_check_data), $err,false);
			}
		}
		return $return_data;
	}
	
	public function ws_course_sync($sync_data)
	{
		$return_data = array('status'=>true,'message'=>'');
		if (!empty($sync_data)){
			$url = $sync_data["url"];
            $wstoken = $sync_data["wstoken"];
            $action = $sync_data["action"];
            $status_exist = $status_crud = false;

            //start check course if exist
            $course_check_data = array(
        		'wstoken'=>$wstoken,
        		'wsfunction'=>'local_cds_api_get_courses_by_field',
        		'moodlewsrestformat'=>'json',
        		'field' => 'idnumber',
        		'value' => $sync_data['subject_id']
        	);
    		
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url.'?'.http_build_query($course_check_data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
					"Accept: */*",
				),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
			curl_close($curl);

			//end check course if exist
			if(!$err){
				$result = json_decode($response,true);
				$status_check = isset($result['status']) ? $result['status'] : '';
				if($status_check == 'success'){
					$status_exist = true;
				}
				if ($action == 'check'){
					if (!$status_exist){
						$course_create = isset($sync_data['course_create']) ? $sync_data['course_create'] : false;
						if ($course_create){
							$course_data = array(
								'wstoken'=>$wstoken,
				        		'wsfunction'=>'local_cds_api_create_courses',
				        		'moodlewsrestformat'=>'json',
								'courses'=>array(
									0=>array(
						        		'fullname'=>$sync_data['subject_name'],
						        		'shortname'=>$sync_data['subject_code'],
						        		'idnumber'=>$sync_data['subject_id'],
						        		'categoryid'=>$sync_data['category_id'],
						        		'maxbytes'=>512000,
						        		'format'=>'topics',
						        		'numsections'=>4
						        	)
					        	)
							);
							$log_key = 'API_EXTERNAL_CDS_CREATE_COURSE';
							$status_crud = true;
						}
						else {
							$return_data["status"] = false;
							$return_data["message"] = "Course ".$sync_data['subject_code']." - ".$sync_data['subject_name']." not found";	
						}
					}
					insert_log_external('API_EXTERNAL_CDS_CHECK_COURSE', $url, json_encode($course_check_data), $response,true);
				}
				elseif($action == 'add'){
					if(!$status_exist){
						$course_data = array(
							'wstoken'=>$wstoken,
			        		'wsfunction'=>'local_cds_api_create_courses',
			        		'moodlewsrestformat'=>'json',
							'courses'=>array(
								0=>array(
					        		'fullname'=>$sync_data['subject_name'],
					        		'shortname'=>$sync_data['subject_code'],
					        		'idnumber'=>$sync_data['subject_id'],
					        		'categoryid'=>$sync_data['category_id'],
					        		'maxbytes'=>512000,
					        		'format'=>'topics',
					        		'numsections'=>4
					        	)
				        	)
						);
						$log_key = 'API_EXTERNAL_CDS_CREATE_COURSE';
						$status_crud = true;
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Course ".$sync_data['subject_code']." - ".$sync_data['subject_name']." is already exist";
					}
				}
				elseif($action == 'edit'){
					if($status_exist){
						$course_data = array(
							'wstoken'=>$wstoken,
			        		'wsfunction'=>'local_cds_api_update_courses',
			        		'moodlewsrestformat'=>'json',
							'courses'=>array(
								0=>array(
					        		'fullname'=>$sync_data['subject_name'],
					        		'shortname'=>$sync_data['subject_code'],
					        		'idnumber'=>$sync_data['subject_id'],
					        		'categoryid'=>$sync_data['category_id'],
					        	)
				        	)
						);
						$log_key = 'API_EXTERNAL_CDS_UPDATE_COURSE';
						$status_crud = true;
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Course ".$sync_data['subject_code']." - ".$sync_data['subject_name']." not found";
					}
				}
				elseif($action == 'delete'){
					if($status_exist){
						$course_data = array(
							'wstoken'=>$wstoken,
			        		'wsfunction'=>'local_cds_api_delete_courses',
			        		'moodlewsrestformat'=>'json',
							'courseids'=>array($sync_data['subject_id'])
						);
						$log_key = 'API_EXTERNAL_CDS_DELETE_COURSE';
						$status_crud = true;
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Course ".$sync_data['subject_code']." - ".$sync_data['subject_name']." not found";
					}
				}
				if ($status_crud){
					$curl = curl_init();
		            curl_setopt_array($curl, array(
		                CURLOPT_URL => $url.'?'.http_build_query($course_data),
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_MAXREDIRS => 10,
		                CURLOPT_TIMEOUT => 300,
		                CURLOPT_SSL_VERIFYHOST=> 0,
		                CURLOPT_SSL_VERIFYPEER=> 0,
		                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
		                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
		                CURLOPT_CUSTOMREQUEST => 'GET',
		                CURLOPT_HTTPHEADER => array(
							"Accept: */*",
						),
		            ));
		            $response = curl_exec($curl);
		            $err = curl_error($curl);
					curl_close($curl);

					if (!$err){
						$result = json_decode($response,true);
						$status_action = isset($result['status']) ? $result['status'] : '';
						if($status_action == 'success'){
							//do nothing
							insert_log_external($log_key, $url, json_encode($course_data), $response,true);
						}
						else {
							$return_data["status"] = false;
							if ($status_action == 'failed'){
								$return_data["message"] = !empty($result['exception']) ? $result['exception'] : 'Failed to '.$action.' course';
							}
							else {
								$return_data["message"] = 'Unknown Error : Failed to '.$action.' course';
							}
							insert_log_external($log_key, $url, json_encode($course_data), $response,false);
						}
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Unable to connect to Server";
						insert_log_external($log_key, $url, json_encode($course_data), $err,false);
					}
				}
				else{
					if ($action != "check"){
						$return_data["status"] = false;
						$return_data["message"] = "Unknown parameter action";	
					}
				}
			}
			else {
				insert_log_external('API_EXTERNAL_CDS_COURSE_GET', $url, json_encode($course_check_data), $err,false);
				$return_data["status"] = false;
				$return_data["message"] = "Unable to connect to Server";
			}
		}
		else {
			$return_data["status"] = false;
			$return_data["message"] = "Missing required parameter";
		}
		return $return_data;
	}

	public function ws_course_enrolment($enrol_data)
	{
		$return_data = array('status'=>true,'message'=>'');
		if (!empty($enrol_data)){
			$url = $enrol_data['url'];
            $wstoken = $enrol_data['wstoken'];
            $action = $enrol_data['action'];

            //start check user if exist
            $user_check_data = array(
        		'wstoken'=>$wstoken,
        		'wsfunction'=> 'local_cds_api_get_users_by_field',
        		'moodlewsrestformat'=>'json',
        		'field'=>'username',
        		'values' => array($enrol_data['username'])
        	);
    		
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url.'?'.http_build_query($user_check_data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
					"Accept: */*",
				),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
			curl_close($curl);
			
			//end check user if exist
			if(!$err){
				$status_enrol = false;
				$result = json_decode($response,true);
				$status_check = isset($result['status']) ? $result['status'] : '';
				if($status_check == 'success'){
					$status_enrol = true;
					insert_log_external('API_EXTERNAL_CDS_CHECK_USER', $url, json_encode($user_check_data), $response,true);	
				}
				else {
					insert_log_external('API_EXTERNAL_CDS_CHECK_USER', $url, json_encode($user_check_data), $response,false);	
					//create user if enrol
					if ($action == "enrol"){
						//start create user oidc
			            $user_insert_data = array(
			        		'wstoken'=>$wstoken,
			        		'wsfunction'=> 'local_cds_api_create_users',
			        		'moodlewsrestformat'=>'json',
			        		'users' => array(
			        			0=>array(
			        				'username'=>$enrol_data['username'],
			        				'firstname'=>$enrol_data['firstname'],
			        				'lastname'=>$enrol_data['lastname'],
			        				'email'=>$enrol_data['email'],
			        				'idnumber'=>$enrol_data['employeeid'],
			        				'auth'=>'oidc'
			        			)
			        		)
			        	);
			    		
			        	$curl = curl_init();
			            curl_setopt_array($curl, array(
			                CURLOPT_URL => $url.'?'.http_build_query($user_insert_data),
			                CURLOPT_RETURNTRANSFER => true,
			                CURLOPT_MAXREDIRS => 10,
			                CURLOPT_TIMEOUT => 60,
			                CURLOPT_SSL_VERIFYHOST=> 0,
			                CURLOPT_SSL_VERIFYPEER=> 0,
			                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
			                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
			                CURLOPT_CUSTOMREQUEST => 'GET',
			                CURLOPT_HTTPHEADER => array(
								"Accept: */*",
							),
			            ));
			            $response = curl_exec($curl);
			            $err = curl_error($curl);
						curl_close($curl);

						//end create user oidc
						if(!$err){
							$result = json_decode($response,true);
							$status_insert = isset($result['status']) ? $result['status'] : '';
							if($status_insert == 'success'){
								$status_enrol = true;
								insert_log_external('API_EXTERNAL_CDS_CREATE_USER', $url, json_encode($user_insert_data), $response,true);
							}
							else {
								$return_data["status"] = false;
								if ($status_insert == 'failed'){
									$return_data["message"] = !empty($result['exception']) ? $result['exception'] : 'Failed to insert new user';
								}
								else {
									$return_data["message"] = 'Unknown Error : Failed to insert new user';
								}
								insert_log_external('API_EXTERNAL_CDS_CREATE_USER', $url, json_encode($user_insert_data), $response,false);
							}
						}
						else {
							$return_data["status"] = false;
							$return_data["message"] = "Unable to connect to Server";\
							insert_log_external('API_EXTERNAL_CDS_CREATE_USER', $url, json_encode($user_insert_data), $err,false);
						}
					}
				}
				if ($status_enrol){
					//start enrol or unenrol user
					if ($action == 'enrol'){
						$wsfunction = 'local_cds_api_enrol_users';
						$enrolments = array('userid'=>$enrol_data['username'],'courseid'=>$enrol_data['courseid'],'roleid'=>$enrol_data['roleid']);
						$log_key = 'API_EXTERNAL_CDS_ENROLL_USER';
					}
					else {
						$wsfunction = 'local_cds_api_unenrol_users';
						$enrolments = array('userid'=>$enrol_data['username'],'courseid'=>$enrol_data['courseid']);
						$log_key = 'API_EXTERNAL_CDS_UNENROLL_USER';
					}
					$user_enrolment_data = array(
		        		'wstoken'=>$wstoken,
		        		'wsfunction'=> $wsfunction,
		        		'moodlewsrestformat'=>'json',
		        		'enrolments' => array(0=>$enrolments)
		        	);
		        	$curl = curl_init();
		            curl_setopt_array($curl, array(
		                CURLOPT_URL => $url.'?'.http_build_query($user_enrolment_data),
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_MAXREDIRS => 10,
		                CURLOPT_TIMEOUT => 60,
		                CURLOPT_SSL_VERIFYHOST=> 0,
		                CURLOPT_SSL_VERIFYPEER=> 0,
		                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
		                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
		                CURLOPT_CUSTOMREQUEST => 'GET',
		                CURLOPT_HTTPHEADER => array(
							"Accept: */*",
						),
		            ));
		            $response = curl_exec($curl);
		            $err = curl_error($curl);
					curl_close($curl);

					if(!$err){
						$result = json_decode($response,true);
						$status_insert = isset($result['status']) ? $result['status'] : '';
						if($status_insert == 'success'){
							//do nothing
							insert_log_external($log_key, $url, json_encode($user_enrolment_data), $response,true);
						}
						else {
							$return_data["status"] = false;
							if ($status_insert == 'failed'){
								$return_data["message"] = !empty($result['exception']) ? $result['exception'] : 'Failed to enrol/unenrol user';
							}
							else {
								$return_data["message"] = 'Unknown Error : Failed to enrol/unenrol user';
							}
							insert_log_external($log_key, $url, json_encode($user_enrolment_data), $response,false);
						}
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = "Unable to connect to Server";
						insert_log_external($log_key, $url, json_encode($user_enrolment_data), $err,false);
					}
					//end enrol or unenrol user
				}
			}
			else {
				$return_data["status"] = false;
				$return_data["message"] = "Unable to connect to Server";
				insert_log_external('API_EXTERNAL_CDS_CHECK_USER', $url, json_encode($user_check_data), $err,false);
			}
		}
		else {
			$return_data["status"] = false;
			$return_data["message"] = "Missing required parameter";
		}
		return $return_data;
	}

	public function ws_course_backup($backup_data)
	{
		$return_data = array('status'=>true,'message'=>'');
		if (!empty($backup_data)){
			$url = $backup_data['url'];
            $wstoken = $backup_data['wstoken'];
            $type = $backup_data['type'];
            $subject_id = $backup_data['subject_id'];
            $wsfunction = ($type == 'cds' ? 'local_sync_igracias_approve_course' : 'local_sync_igracias_backup_course_mooc');

            //start backup course
            $course_data = array(
        		'wstoken'=>$wstoken,
        		'wsfunction'=> $wsfunction,
        		'moodlewsrestformat'=>'json',
        		'subject_id'=>$subject_id,
        	);
    		
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url.'?'.http_build_query($course_data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 300,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
					"Accept: */*",
				),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
			curl_close($curl);
			
			if (!$err){
				$result = json_decode($response,true);
				$status_backup = isset($result['status']) ? $result['status'] : '';
				$exception = isset($result['exception']) ? $result['exception'] : '';
				$message = isset($result['message']) ? $result['message'] : '';
				if($status_backup == 'success'){
					if(strpos($message, "Backup completed.") !== false){
						$return_data["message"] = !empty($message) ? $message : 'Backup completed.';
						if($type == 'cds'){
							insert_log_external('API_EXTERNAL_CDS_APPROVE_COURSE', $url, json_encode($course_data), $response, true);	
						}else{
							insert_log_external('API_EXTERNAL_MOOC_BACKUP_COURSE', $url, json_encode($course_data), $response, true);	
						}
					}
					else {
						$return_data["status"] = false;
						$return_data["message"] = !empty($message) ? $message : 'Failed to backup course';
						if($type == 'cds'){
							insert_log_external('API_EXTERNAL_CDS_APPROVE_COURSE', $url, json_encode($course_data), $response);	
						}else{
							insert_log_external('API_EXTERNAL_MOOC_BACKUP_COURSE', $url, json_encode($course_data), $response);	
						}
					}
				}
				else {
					$return_data["status"] = false;
					if ($status_backup == 'failed'){
						$return_data["message"] = !empty($exception) ? $exception : 'Failed to backup course';
					}
					else {
						$return_data["message"] = 'Unknown Error : Failed to backup course';
					}
					if($type == 'cds'){
						insert_log_external('API_EXTERNAL_CDS_APPROVE_COURSE', $url, json_encode($course_data), $response);	
					}else{
						insert_log_external('API_EXTERNAL_MOOC_BACKUP_COURSE', $url, json_encode($course_data), $response);	
					}
				}
            }
            else {
            	$return_data["status"] = false;
				$return_data["message"] = "Unable to connect to Server";
				if($type == 'cds'){
					insert_log_external('API_EXTERNAL_CDS_APPROVE_COURSE', $url, json_encode($course_data), $err);	
				}else{
					insert_log_external('API_EXTERNAL_MOOC_BACKUP_COURSE', $url, json_encode($course_data), $err);	
				}
            }
		}
		else {
			$return_data["status"] = false;
			$return_data["message"] = "Missing required parameter";
		}
		return $return_data;
	}

	public function ws_course_restore_lms($sync_data)
	{
		$return_data = array('status'=>true,'message'=>'');
		if (!empty($sync_data)){
			$url = $sync_data["url"];
            $wstoken = $sync_data["wstoken"];
            $status_exist = $status_crud = false;

            //start check course if exist
            $course_restore_data = array(
        		'wstoken'=>$wstoken,
        		'wsfunction'=>'local_sync_igracias_sync_beta',
        		'moodlewsrestformat'=>'json',
        		'course_id' => $sync_data['course_id'],
        		'cds_shortname' => $sync_data['cds_shortname']
        	);
    		
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url.'?'.http_build_query($course_restore_data),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 300,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
					"Accept: */*",
				),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
			curl_close($curl);

			if(!$err){
				$result = json_decode($response,true);
				$status_sync = isset($result['status']) ? $result['status'] : '';
				$exception = isset($result['exception']) ? $result['exception'] : '';
				$message = isset($result['message']) ? $result['message'] : '';
				if($status_sync == 'success'){
					$return_data["status"] = true;
					$return_data["message"] = $message;
					insert_log_external('API_EXTERNAL_LMS_COURSE_SYNC', $url, json_encode($course_restore_data), $response,true);
				}
				else {
					$return_data["status"] = false;
					$return_data["message"] = $exception." - ".$message;
					insert_log_external('API_EXTERNAL_LMS_COURSE_SYNC', $url, json_encode($course_restore_data), $response,false);
				}
			}
			else {
				$return_data["status"] = false;
				$return_data["message"] = "Unable to connect to Server";
				insert_log_external('API_EXTERNAL_LMS_COURSE_SYNC', $url, json_encode($course_restore_data), $err,false);
			}
		}
		return $return_data;
	}

	//Change to ws_category_sync 
	public function cds_manage_category($action,$sync_data)
	{
		if (!empty($action) && !empty($sync_data)){
			$method = "POST";
			$url = $sync_data["url"];
            $token = $sync_data["token"];
            $category_data = $sync_data["category_data"];
			if ($action == "delete"){
				$url .= "/".$category_data;
				$method = "DELETE";
				$category_data = array();
			}
    		
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 600,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => http_build_query($category_data),
                CURLOPT_HTTPHEADER => array(
					"Accept: application/json",
					"Authorization: Bearer ".$token
				),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
			curl_close($curl);

            if (!$err){
                $result = json_decode($response,true);
                if ($action == "add" && !empty($result["CATEGORY_ID"])){
					insert_log_external('API_EXTERNAL_CDS_CREATE_CATEGORY', $url, json_encode($category_data), $response, true);
			        return $result["CATEGORY_ID"];
			    }
			    elseif ($action == "edit" && !empty($result["UPDATE_STATUS"])){
					insert_log_external('API_EXTERNAL_CDS_UPDATE_CATEGORY', $url, json_encode($category_data), $response, true);
			        return true;
			    }
			    elseif ($action == "delete" && !empty($result["DELETE_STATUS"])){
					insert_log_external('API_EXTERNAL_CDS_DELETE_CATEGORY', $url, json_encode($category_data), $response, true);
			        return true;
			    }
            }else{
				$err = htmlentities($err);
				if($action == 'add'){
					insert_log_external('API_EXTERNAL_CDS_CREATE_CATEGORY', $url, json_encode($category_data), $err);
				}elseif ($action == 'edit') {
					insert_log_external('API_EXTERNAL_CDS_UPDATE_CATEGORY', $url, json_encode($category_data), $err);
				}elseif ($action == 'delete') {
					insert_log_external('API_EXTERNAL_CDS_DELETE_CATEGORY', $url, json_encode($category_data), $err);
				}
			}
        }
	}

	//Change to ws_course_backup 
	public function backup($subject)
	{
		$result = array("status" => 2,"message" => "","log"=>"");
		if (!empty($subject)){
    		$url = $subject["url"];
            $u = $subject["u"];
            $p = $subject["p"];
            $subject_id = $subject["subject_id"];
            if (!empty($url) && !empty($u) && !empty($p) && !empty($subject_id)){
	            $curl = curl_init();
	            $data = array("username" => $u, "password" => $p, "subject_id" => $subject_id);
	            curl_setopt_array($curl, array(
	                CURLOPT_URL => $url,
	                CURLOPT_RETURNTRANSFER => true,
	                CURLOPT_MAXREDIRS => 10,
	                CURLOPT_TIMEOUT => 600,
	                CURLOPT_SSL_VERIFYHOST=> 0,
	                CURLOPT_SSL_VERIFYPEER=> 0,
	                CURLOPT_POST=> 1,
	                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
	                CURLOPT_CUSTOMREQUEST => "POST",
	                CURLOPT_POSTFIELDS => http_build_query($data),
	            ));
	            $curl_response = curl_exec($curl);
	            $err = curl_error($curl);
				curl_close($curl);

	            if (!$err){
	            	$response = htmlentities($curl_response);
	            	if (strlen($response) > 100){
						insert_log_external('API_EXTERNAL_CDS_APPROVE_COURSE', $url, json_encode($data), $curl_response);
	            		$response = "Course backup failed due to CDS error[1]. Please contact Administrator";
	            		$result["message"] = "Course backup failed";
	            	}
	            	else {
						insert_log_external('API_EXTERNAL_CDS_APPROVE_COURSE', $url, json_encode($data), $curl_response, true);
	            		$result["status"] = 1;
                    	$result["message"] = "Course backup success";	
	            	}
                	
                    $result["log"] = $response;
	            }
	            else {
					$error = htmlentities($err);
					insert_log_external('API_EXTERNAL_CDS_APPROVE_COURSE', $url, json_encode($data), $error);
	            	if (strlen($error) > 100){
	            		$error = "Course backup failed due to CDS error[2]. Please contact Administrator";
	            	}
                    $result["message"] = "Course backup failed";
                    $result["log"] = $error;
	            }
	        }
	        else {
	        	$result["message"] = "Data config is not properly setup. Course backup failed";
	        }
        }
        else {
        	$result["message"] = "No Data Course submitted. Course backup failed";
        }
        return $result;
	}

	//Change to ws_course_sync 
	public function cds_sync_course($subject)
    {
    	$result = array("status" => 2,"message" => "","log"=>"");
        if(!empty($subject)){
    		$url = $subject["url"];
            $u = $subject["u"];
            $p = $subject["p"];
            $subject_id = $subject["subject_id"];
            if (!empty($url) && !empty($u) && !empty($p) && !empty($subject_id)){
                $curl = curl_init();
                $data = array("username" => $u, "password" => $p, "subject_id" => $subject_id);
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 600,
                    CURLOPT_SSL_VERIFYHOST=> 0,
                    CURLOPT_SSL_VERIFYPEER=> 0,
                    CURLOPT_POST=> 1,
                    CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => http_build_query($data),
                ));
                $curl_response = curl_exec($curl);
                $err = curl_error($curl);
				curl_close($curl);

                if (!$err){
                	$response = htmlentities($curl_response);
                	if(strpos($response,'created') || strpos($response,'updated')){
						insert_log_external('API_EXTERNAL_CDS_COURSE_SYNC', $url, json_encode($data), $curl_response, true);
                		$result["status"] = 1;
                    	$result["message"] = "Course synchronized to CDS successfully";	
	            	}
	            	else {
						insert_log_external('API_EXTERNAL_CDS_COURSE_SYNC', $url, json_encode($data), $curl_response);
	            		$response = "Course sync failed due to CDS error[1]. Please contact Administrator";
	            		$result["message"] = "Course synchronization to CDS failed";
	            	}
                    $result["log"] = $response;
                }
                else {
					$error = htmlentities($err);
					insert_log_external('API_EXTERNAL_CDS_COURSE_SYNC', $url, json_encode($data), $error);
                	if (strlen($error) > 100){
	            		$error = "Course backup failed due to CDS error[2]. Please contact Administrator";
	            	}
                    $result["message"] = "Course synchronization to CDS failed";
                    $result["log"] = $error;
                }
            }
            else {
                $result["message"] = "Data config is not properly setup. Course synchronization to CDS failed";
            }
        }
        else{
            $result["message"] = "No Data Course submitted. Course synchronization to CDS failed";
        }
        return $result;
    }

    //Change to ws_course_restore_lms
	public function lms_sync_course($course)
	{
		$result = array("status" => 2,"message" => "","log"=>"");
        if(!empty($course)){
    		$url = $course["url"];
            $u = $course["u"];
            $p = $course["p"];
            $course_id = $course["course_id"];
            if (!empty($url) && !empty($u) && !empty($p) && !empty($course_id)){
				$curl = curl_init();
		        $data = array("username" => $u, "password" => $p, "course_id" => $course_id);
		        curl_setopt_array($curl, array(
		            CURLOPT_URL => $url,
		            CURLOPT_RETURNTRANSFER => true,
		            CURLOPT_MAXREDIRS => 10,
		            CURLOPT_TIMEOUT => 600,
		            CURLOPT_SSL_VERIFYHOST=> 0,
		            CURLOPT_SSL_VERIFYPEER=> 0,
		            CURLOPT_POST=> 1,
		            CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
		            CURLOPT_CUSTOMREQUEST => "POST",
		            CURLOPT_POSTFIELDS => http_build_query($data),
		        ));
		        $curl_response = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl);
				
		        if (!$err){
                	$response = htmlentities($curl_response);
                	if(strpos($response,'Course sync success')){
						insert_log_external('API_EXTERNAL_LMS_COURSE_SYNC', $url, json_encode($data), $curl_response, true);
                		$result["status"] = 1;
                    	$result["message"] = "Course synchronized to LMS successfully";
	            	}
	            	elseif ($response == "Course still not approved. Sync failed"){
						insert_log_external('API_EXTERNAL_LMS_COURSE_SYNC', $url, json_encode($data), $curl_response);
	            		$result["message"] = "Course synchronization to LMS failed";
	            	}
	            	else {
						insert_log_external('API_EXTERNAL_LMS_COURSE_SYNC', $url, json_encode($data), $curl_response);
	            		$response = "Course sync failed due to LMS error[1]. Please contact Administrator";
	            		$result["message"] = "Course synchronization to LMS failed";
	            	}
                    $result["log"] = $response;
                }
                else {
					$error = htmlentities($err);
					insert_log_external('API_EXTERNAL_LMS_COURSE_SYNC', $url, json_encode($data), $error);
                	if (strlen($error) > 100){
	            		$error = "Course sync failed due to LMS error[2]. Please contact Administrator";
	            	}
                    $result["message"] = "Course synchronization to LMS failed";
                    $result["log"] = $error;
                }
		    }
		    else {
                $result["message"] = "Data config is not properly setup. Course synchronization to LMS failed";
            }
        }
        else{
            $result["message"] = "No Data Course submitted. Course synchronization to LMS failed";
        }
        return $result;
	}
	public function user_frontend($action,$sync_data, $mobile = false)
	{
		if (!empty($action) && !empty($sync_data)){
			$method = "POST";
			$url = $sync_data["url"];
            $user_data = $sync_data["user_data"];
			if ($action == "delete"){
				$url .= "/".$user_data;
				$method = "DELETE";
				$user_data = array();
			}
			if($action == 'get'){
				$url .= "/".$user_data;
				$method = "GET";
				$user_data = array();
			}

			$header = [];
			if($mobile) {
				$CI =& get_instance();
				$orgid = $CI->config->item('header_x-orgid');
				if($orgid) $header[] = "X-Orgid: ".$orgid;
			}
    		
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 600,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
                CURLOPT_CUSTOMREQUEST => $method,
				CURLOPT_POSTFIELDS => http_build_query($user_data),
				CURLOPT_HTTPHEADER => $header,
            ));
            $response = curl_exec($curl);
			$err = curl_error($curl);
			if(empty($response)){
				$response = "Response empty";
			}

            curl_close($curl);
            if (!$err){
				$result = json_decode($response,true);
				if(isset($result['Status'])){
					$status = strtolower($result['Status']);
					if($status == "success"){
						if($action == 'add'){
							unset($user_data['password']);
							insert_log_external('API_EXTERNAL_IGRACIAS_CREATE_USER', $url, json_encode($user_data), $response, true);
						}elseif ($action == 'edit') {
							insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_USER', $url, json_encode($user_data), $response, true);
						}elseif ($action == 'delete') {
							insert_log_external('API_EXTERNAL_IGRACIAS_DELETE_USER', $url, json_encode($user_data), $response, true);
						}elseif ($action == 'get'){
							insert_log_external('API_EXTERNAL_IGRACIAS_GET_USER', $url, json_encode($user_data), $response, true);
						}elseif ($action == 'update_password'){
							unset($user_data['oldpassword']);
							unset($user_data['newpassword']);
							insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_PASSWORD_USER', $url, json_encode($user_data), $response, true);
						}

						if(isset($result['Data'])) return ['status' => true, 'user' => $result['Data'][0]];
						if(isset($result['Personid'])) return ['status' => true, 'personid' => $result['Personid']];
						if(isset($result['Personid'])) return ['status' => true, 'personid' => $result['Personid']];


						if(isset($result['Message'])){
							$message = strtolower($result['Message']);
							if($message == "no data found"){
								return ['status' => false, 'message' => $message];
							}
							return ['status' => true, 'message' => 'Success Communicate With Igracias'];
						}
					}else{
						if($action == 'add'){
							unset($user_data['password']);
							insert_log_external('API_EXTERNAL_IGRACIAS_CREATE_USER', $url, json_encode($user_data), $response);
						}elseif ($action == 'edit') {
							insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_USER', $url, json_encode($user_data), $response);
						}elseif ($action == 'delete') {
							insert_log_external('API_EXTERNAL_IGRACIAS_DELETE_USER', $url, json_encode($user_data), $response);
						}elseif ($action == 'get'){
							insert_log_external('API_EXTERNAL_IGRACIAS_GET_USER', $url, json_encode($user_data), $response);
						}elseif ($action == 'update_password'){
							unset($user_data['oldpassword']);
							unset($user_data['newpassword']);
							insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_PASSWORD_USER', $url, json_encode($user_data), $response);
						}

						$message = 'Failed to communicate with igracias, please try again later';
						if(isset($result['Message'])){
							$message = $result['Message'];
							if(is_array($message) && count($message) > 0){
								$error_arr = [];
								foreach($message as $error){
									$error_arr[] = implode('<br>', $error);
								}
								$message = implode('<br>', $error_arr);
							}else{
								$message = strtolower($message);
							}
						}
						return ['status' => false, 'message' => $message];
					}
				}else if(isset($result['data'])){
					if($action == 'add'){
						unset($user_data['password']);
						insert_log_external('API_EXTERNAL_IGRACIAS_CREATE_USER', $url, json_encode($user_data), $response, true);
					}elseif ($action == 'edit') {
						insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_USER', $url, json_encode($user_data), $response, true);
					}elseif ($action == 'delete') {
						insert_log_external('API_EXTERNAL_IGRACIAS_DELETE_USER', $url, json_encode($user_data), $response, true);
					}elseif ($action == 'get'){
						insert_log_external('API_EXTERNAL_IGRACIAS_GET_USER', $url, json_encode($user_data), $response, true);
					}elseif ($action == 'update_password'){
						unset($user_data['oldpassword']);
						unset($user_data['newpassword']);
						insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_PASSWORD_USER', $url, json_encode($user_data), $response, true);
					}

					return ['status' => true, 'message' => $result['data']];
				}else{
					if($action == 'add'){
						unset($user_data['password']);
						insert_log_external('API_EXTERNAL_IGRACIAS_CREATE_USER', $url, json_encode($user_data), $response);
					}elseif ($action == 'edit') {
						insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_USER', $url, json_encode($user_data), $response);
					}elseif ($action == 'delete') {
						insert_log_external('API_EXTERNAL_IGRACIAS_DELETE_USER', $url, json_encode($user_data), $response);
					}elseif ($action == 'get'){
						insert_log_external('API_EXTERNAL_IGRACIAS_GET_USER', $url, json_encode($user_data), $response);
					}elseif ($action == 'update_password'){
						unset($user_data['oldpassword']);
						unset($user_data['newpassword']);
						insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_PASSWORD_USER', $url, json_encode($user_data), $response);
					}

					return ['status' => false, 'message' => 'Failed to communicate with igracias, please try again later'];
				}
            }
            else {
				$error = htmlentities($err);
				if($action == 'add'){
					unset($user_data['password']);
					insert_log_external('API_EXTERNAL_IGRACIAS_CREATE_USER', $url, json_encode($user_data), $error);
				}elseif ($action == 'edit') {
					insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_USER', $url, json_encode($user_data), $error);
				}elseif ($action == 'delete') {
					insert_log_external('API_EXTERNAL_IGRACIAS_DELETE_USER', $url, json_encode($user_data), $error);
				}elseif ($action == 'get'){
					insert_log_external('API_EXTERNAL_IGRACIAS_GET_USER', $url, json_encode($user_data), $error);
				}elseif ($action == 'update_password'){
					unset($user_data['oldpassword']);
					unset($user_data['newpassword']);
					insert_log_external('API_EXTERNAL_IGRACIAS_UPDATE_PASSWORD_USER', $url, json_encode($user_data), $error);
				}

            	return ['status' => false, 'message' => $error];
            }
        }
	}

	public function sync_mooc($action, $sync_data)
	{
		if (!empty($sync_data)){
			$method = "POST";
			$url = $sync_data["url"];
			$sync_data = $sync_data["sync_data"];
			$x = 1;
			if($action == 'get' || $action == 'delete'){
				foreach($sync_data as $key => $value){
					if($x == 1) $url .= '?';
					if($x > 1) $url .= '&';
					$url .= $key . '=' . urlencode($value);
					$x++;
				}
				$method = 'GET';
			}
			
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 600,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
				CURLOPT_CUSTOMREQUEST => $method,
				CURLOPT_POSTFIELDS => http_build_query($sync_data),
            ));
            $response = curl_exec($curl);
			$err = curl_error($curl);
			// $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
            if (!$err){
				$result = json_decode($response,true);
				if((isset($result['status']) && $result['status'] == 'failed') || (isset($result['exception']) && !empty($result['exception']))){
					$this->log_sync_mooc($sync_data['wsfunction'], $url, $sync_data, $response);
				}else{
					$this->log_sync_mooc($sync_data['wsfunction'], $url, $sync_data, $response, true);
				}
				
				return $result;
            }
            else {
				$this->log_sync_mooc($sync_data['wsfunction'], $url, $sync_data, $err);

				return $err;
			}
        }
	}

	public function call_mooc($action, $sync_data)
	{
		if (!empty($sync_data)){
			$method = "POST";
			$url = $sync_data["url"];
			$sync_data = $sync_data["sync_data"];
			$x = 1;
			if($action == 'get' || $action == 'delete'){
				foreach($sync_data as $key => $value){
					if($x == 1) $url .= '?';
					if($x > 1) $url .= '&';
					$url .= $key . '=' . urlencode($value);
					$x++;
				}
				$method = 'GET';
			}
			
        	$curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 600,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
				CURLOPT_CUSTOMREQUEST => $method,
				CURLOPT_POSTFIELDS => http_build_query($sync_data),
            ));
            $response = curl_exec($curl);
			$err = curl_error($curl);
			// $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
            if (!$err){
				$result = new MoocResponse($response);
				$this->log_sync_mooc($sync_data['wsfunction'], $url, $sync_data, $response, $result->success);
				return $result;
            }
            else {
				$this->log_sync_mooc($sync_data['wsfunction'], $url, $sync_data, $err);
				return new MoocResponse();
			}
        }
	}

	private function log_sync_mooc($action, $url, $sync_data, $response, $is_success = false){
		if($action == 'core_user_get_users'){
			insert_log_external('API_EXTERNAL_MOOC_CHECK_USER', $url, json_encode($sync_data), $response, $is_success);
		}elseif ($action == 'core_user_create_users') {
			insert_log_external('API_EXTERNAL_MOOC_CREATE_USER', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'core_user_update_users'){
			unset($sync_data['users[0][password]']);
			insert_log_external('API_EXTERNAL_MOOC_UPDATE_USER', $url, json_encode($sync_data), $response, $is_success);
		}elseif ($action == 'core_user_delete_users') {
			insert_log_external('API_EXTERNAL_MOOC_DELETE_USER', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'local_mooc_api_enrol_users'){
			insert_log_external('API_EXTERNAL_MOOC_ENROLL_USER', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'local_mooc_api_unenrol_users'){
			insert_log_external('API_EXTERNAL_MOOC_UNENROLL_USER', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'local_mooc_api_create_courses'){
			insert_log_external('API_EXTERNAL_MOOC_CREATE_COURSES', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'core_course_delete_courses'){
			insert_log_external('API_EXTERNAL_MOOC_DELETE_COURSES', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'local_mooc_api_create_categories'){
			insert_log_external('API_EXTERNAL_MOOC_CREATE_CATEGORIES', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'local_mooc_api_update_categories'){
			insert_log_external('API_EXTERNAL_MOOC_UPDATE_CATEGORIES', $url, json_encode($sync_data), $response, $is_success);
		}elseif($action == 'local_mooc_api_delete_categories'){
			insert_log_external('API_EXTERNAL_MOOC_DELETE_CATEGORIES', $url, json_encode($sync_data), $response, $is_success);				
		}
	}
}
class MoocResponse {
	public $success = false;
	public $data = false;
	public $message = "";

 	public function __construct($response_json)
	{
		$response = json_decode($response_json);
		if(!empty($response)){
			if(property_exists($response,"status")){
				$this->success = strtolower($response->status) == "success";
			}
			if(property_exists($response,"exception")){
				$this->message = $response->exception;
			}
			if(property_exists($response,"data")){
				$this->data = $response->data;
			}
		}
	}
}

?>