<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Graph {

    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->helper('log_api_external');
    }
    
    public function oauth($account)
    {
        if (!empty($account)){
            $url = $account["graph_url_oauth"];
            $body =  array(
                "grant_type" => "client_credentials",
                "client_id" => $account["graph_client_id"],
                "client_secret" => $account["graph_client_secret"],
                "scope" => "https://graph.microsoft.com/.default"
            );
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
                CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($body),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded",
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            $log_resp = $response;
            if(empty($log_resp)){
                $log_resp = $err;
            }

            
            if (!$err){
                $result = json_decode($response,true);
                if(isset($result["access_token"])){
                    insert_log_external('API_EXTERNAL_GRAPH_OAUTH', $url, json_encode($body), $response, true);

                    return $result;
                }else{
                    insert_log_external('API_EXTERNAL_GRAPH_OAUTH', $url, json_encode($body), $response);
                }
            }
            else {
                insert_log_external('API_EXTERNAL_GRAPH_OAUTH', $url, json_encode($body), $err);

                return $err;
            }
        }
    }

    public function get_site_id($subject_id,$data){
        if (!empty($subject_id) && !empty($data)){
            $url = $data["cds_url_course_site_id"]."?subject_id=".$subject_id;
            $api_app_id = $data["api_app_id"];
            $api_app_secret = $data["api_app_secret"];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_SSL_VERIFYHOST=> 0,
                CURLOPT_SSL_VERIFYPEER=> 0,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "api_app_id: ".$api_app_id,
                    "api_app_secret: ".$api_app_secret
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $log_resp = $response;
            if(empty($log_resp)){
                $log_resp = $err;
            }

            if (!$err){
                $result = json_decode($response,true);
                if(!empty($result)){
                    $status = $result["status"];
                    if ($status){
                        insert_log_external('API_EXTERNAL_CDS_COURSE_SITE_ID', $url, json_encode(['subject_id' => $subject_id]), $response, true);

                        return $result["data"];
                    }else{
                        insert_log_external('API_EXTERNAL_CDS_COURSE_SITE_ID', $url, json_encode(['subject_id' => $subject_id]), $response);
                    }
                }else{
                    insert_log_external('API_EXTERNAL_CDS_COURSE_SITE_ID', $url, json_encode(['subject_id' => $subject_id]), $response);
                }
            }else{
                insert_log_external('API_EXTERNAL_CDS_COURSE_SITE_ID', $url, json_encode(['subject_id' => $subject_id]), $err);
            }
        }
    }

    public function get_subsite_id($token,$url){
        if (!empty($token) && !empty($url)){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: Bearer '.$token
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            $log_resp = $response;
            if(empty($log_resp)){
                $log_resp = $err;
            }

            if (!$err){
                $result = json_decode($response,true);
                if(isset($result["value"])){
                    insert_log_external('API_EXTERNAL_GRAPH_SHAREPOINT', $url, json_encode(['url' => $url]), $response, true);

                    $value = $result["value"];
                    foreach($value as $field){
                        if ($field["name"] == "Documents"){
                            return $field["id"];

                        }
                    }
                }else{
                    insert_log_external('API_EXTERNAL_GRAPH_SHAREPOINT', $url, json_encode(['url' => $url]), $response);
                }
            }else{
                insert_log_external('API_EXTERNAL_GRAPH_SHAREPOINT', $url, json_encode(['url' => $url]), $err);
            }
        }
    }

    public function get_subsite_item($token,$url){
        if (!empty($token) && !empty($url)){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: Bearer '.$token
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);

            if (!$err){
                $result = json_decode($response,true);
                if(isset($result["value"])){
                    return $result["value"];
                }
            }
        }
    }

    public function get_public_link($token,$url){
        if (!empty($token) && !empty($url)){
            $body = '{"type": "view","scope": "anonymous"}';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_POST=> 1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $body,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: Bearer '.$token
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $log_resp = $response;
            if(empty($log_resp)){
                $log_resp = $err;
            }

            if (!$err){
                $result = json_decode($response,true);

                if(isset($result["link"])){
                    insert_log_external('API_EXTERNAL_GRAPH_SHAREPOINT', $url, json_encode($body), $response, true);
                    
                    return $result["link"]["webUrl"];
                }else{
                    insert_log_external('API_EXTERNAL_GRAPH_SHAREPOINT', $url, json_encode($body), $response);
                }
            }else{
                insert_log_external('API_EXTERNAL_GRAPH_SHAREPOINT', $url, json_encode($body), $err);
            }
        }
    }

    public function create_public_link($token,$data){
        if (!empty($token) && !empty($data)){
            $result = array("status" => 1,"data" => array(),"log"=>"");
            //get Course Site ID
            $course_data = array(
                "api_app_id" => $data["api_app_id"],
                "api_app_secret" => $data["api_app_secret"],
                "cds_url_course_site_id" => $data["cds_url_course_site_id"]
            );
            $site_id = $this->get_site_id($data["subject_id"],$course_data);
            if (!empty($site_id)){
                //get Subsite ID
                $url_subsite = $data["graph_url_sharepoint"].",".$site_id."/drives";
                $subsite_id = $this->get_subsite_id($token,$url_subsite);
                if (!empty($subsite_id)){
                    //get List Item
                    $url_list = $url_subsite."/".$subsite_id."/root/children";
                    $subsite_list = $this->get_subsite_item($token,$url_list);
                    if (!empty($subsite_list)){
                        $list_item = $data["list_item"];
                        $count_item = count($list_item);
                        $num_public_link = 0;
                        foreach($list_item as $key => $value){
                            $list_item[$key]["err"] = "";
                            $item_name = basename($value["url"]);
                            $item_id = "";
                            // var_dump($subsite_list);
                            foreach($subsite_list as $item){
                                $list_name = $item["name"];
                                // echo "<br>comparing ".$item_name." with ".$list_name."<br>";
                                if (strtolower($item_name) == strtolower($list_name)){
                                    $item_id = $item["id"];
                                    break;
                                }
                            }
                            if(!empty($item_id)){
                                $url_public_link = $url_subsite."/".$subsite_id."/items/".$item_id."/createLink";
                                $public_link = $this->get_public_link($token,$url_public_link);
                                if (!empty($public_link)){
                                    $list_item[$key]["public_url"] = $public_link;
                                    $num_public_link++;
                                }else{
                                    $list_item[$key]["err"] = "no public link available for item : ".$value["name"];
                                }
                            }
                            else {
                                $list_item[$key]["err"] = "no file matches for item : ".$value["url"];
                                $result["log"] = "no file matches for item : ".$value["url"];
                            }
                        }
                        $result["data"] = $list_item;
                    }
                    else {
                        $result["log"] = "no files in subsite";
                    }
                }
                else {
                    $result["log"] = "subsite id is empty";
                }
            }
            else {
                $result["log"] = "course site id is empty";
            }
            return $result;
        }
    }

}

?>