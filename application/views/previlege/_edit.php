<?php
    $this->load->view('previlege/checklist.css');
?>
<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_previlege_id" name="previlege_id"/>
                    <input type="hidden" id="edit_hidden_group_id" name="group_id"/>
                    <div class="form-body">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Submenu Name</label>
                                    <select name="submenu_id" class="form-control form-control" id="edit_submenu_id">   
                                        <?php
                                          foreach ($submenus as $submenu)
                                          {
                                              echo "<option value='$submenu->submenu_id'>$submenu->menu_name > $submenu->submenu_name</option>";
                                          }
                                        ?>
                                    </select>
                    
                                </div>
                            </div>     
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Group Name</label>
                                    <select class="form-control form-control" id="edit_group_id" disabled>           
                                        <?php
                                          foreach ($group as $grp)
                                          {
                                              echo "<option value='$grp->group_id'>$grp->group_name</option>";
                                          }
                                        ?>
                                    </select>
                                </div>
                            </div>      
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                    <h5>Privileges</h5>
                                        <label class="container-inline">Create
                                          <input type="checkbox" id="edit_create" name="create">
                                          <span class="checkmark"></span>
                                        </label>
                                        <label class="container-inline">Update
                                          <input type="checkbox" id="edit_update" name="update">
                                          <span class="checkmark"></span>
                                        </label>
                                        <label class="container-inline">Delete
                                          <input type="checkbox" id="edit_delete" name="delete">
                                          <span class="checkmark"></span>
                                        </label>
                                        <label class="container-inline">Read
                                          <input type="checkbox" id="edit_read" name="read">
                                          <span class="checkmark"></span>
                                        </label>    
                                    </div>
                                </div>   
                            </div>
                            <div class="col-md-12" >     
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                         <div class="alert alert-danger fade show" role="alert" id="failed_alert_edit" style="display: none;">
                                            <div class="alert-text" id="failed_message_edit"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_edit">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>                   
                                    </div>                   
                                </div>            
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->

<script type="text/javascript">
    $('#edit_submenu_name').select2({
        placeholder: "Please select a Submenu Name",
        width: '100%'
    });

    $('#edit_group_name').select2({
        placeholder: "Please select a Group Name",
        width: '100%'
    });
</script>
