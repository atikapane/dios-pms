<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">								
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title">Page</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">#Previlege</span>           
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->					

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Previlege Asignment
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                   		
        </div>
    </div>

    <form name="fm" method="post" action="<?php echo base_url('previlege/save'); ?>">
	<div class="kt-portlet__body">
        <br/>        
		<!--begin::Section-->
        <div class="kt-section">
        <span class="kt-section__info"></span>
        <div class="kt-section__content">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Menu</th>
                        <th colspan="3">Action</th>
                        <th class="text-right"><button type="submit" class="btn btn-primary btn-sm">Submit</button></th>
                        <input type="hidden" name="gid" value="<?php echo $this->uri->segment(3); ?>">						      		
                    </tr>
                </thead>
                <tbody>
                    <?php 
                $no=0;
                $no=0;
                foreach($dt as $row){
                    $no++;
                    echo '<tr>  
                            <td>'.$no.'</td>
                            <td><input type="hidden" name="menuid[]" value="'.$row->menu_id.'">'.$row->menu_name.'</td>
                            <td><input type="checkbox" name="chkcreate[]"';
                            $qry = $this->db->query("SELECT * FROM previleges WHERE groupid=".$this->uri->segment(3)."  AND menuid=".$row->menu_id."")->row();
                            if($qry->create > 0){ echo 'checked';}else{echo '';}
                            echo '> Create</td>                                                    				
                            <td><input type="checkbox" name="chkupdate[]"';
                            if($qry->update > 0){ echo 'checked';}else{echo '';}
                            echo '> Update</td>				
                            <td><input type="checkbox" name="chkdelete[]"';
                            if($qry->delete > 0){ echo 'checked';}else{echo '';}
                            echo '> Delete</td>				
                            <td><input type="checkbox" name="chkread[]"';
                            if($qry->read > 0){ echo 'checked';}else{echo '';}
                            echo '> Read</td>				
                        </tr>';
                }			
			?>						    	
						  	</tbody>
						</table>
					</div>
				</div>
				<!--end::Section-->
	</div>
	

</div>
    </div>
    <!-- end:: Content -->
    <!-- end:: Content -->
    
</div>

<?php
$this->load->view('templates/Footer.php');
?>