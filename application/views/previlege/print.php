<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Id</b></th>
            <th scope="col"><b>Submenu Memu</b></th>
            <th scope="col"><b>Group Name</b></th>
            <th scope="col"><b>Create</b></th>
            <th scope="col"><b>Update</b></th>
            <th scope="col"><b>Delete</b></th>
            <th scope="col"><b>Read</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item['previlege_id'].'</td>
            <td>'.$item['submenu_name'].'</td>
            <td>'.$item['group_name'].'</td>
            <td>'.$item['create'].'</td>
            <td>'.$item['update'].'</td>
            <td>'.$item['delete'].'</td>
            <td>'.$item['read'].'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>