<!DOCTYPE html>
<html lang="en" >
    <meta charset="utf-8"/>

    <title>DIOS PMS | Login</title>
    <meta name="description" content="Login Page">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <link href="<?php echo base_url();?>assets/login/css/login.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/login/css/style.bundle.min.css" rel="stylesheet" type="text/css" />
    
    <link rel="apple-touch-icon" sizes="57x57" href="favico/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favico/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favico/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favico/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favico/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favico/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favico/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favico/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favico/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favico/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favico/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favico/favicon-16x16.png">
    <link rel="manifest" href="favico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favico/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>
<body  style="background-image: url(<?php echo base_url();?>assets/media/demos/demo4/header.jpg); background-position: center top; background-size: 100% 350px;"  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v5 kt-login--signin" id="kt_login">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile" style="background-image: url(<?php echo base_url();?>assets/media//bg/bg-3.jpg);">
			 	<div class="kt-login__left">
					<div class="kt-login__wrapper">
						<div class="kt-login__content">
							<!-- <a class="kt-login__logo" href="#">
								<img src="<?php echo base_url();?>assets/media/img/logo_celoe.png" class="img-fluid">
							</a> -->
							<span class="kt-login__desc">
								
							</span>
						</div>
					</div>
				</div>

				<div class="kt-login__divider">
					<div></div>
				</div>

				<div class="kt-login__right">
					<div class="kt-login__wrapper">
						<div class="kt-login__signin">
							<div class="kt-login__head">
								<h3 class="kt-login__title">Login To Your Account</h3>
							</div>
							<div class="kt-login__form">
								<?php
									$status_message = $this->session->flashdata('status_message');
			                		$value_message = $this->session->flashdata('value_message'); 
			                		if (!empty($status_message)){ ?>
			                			<div class="alert alert-danger fade show" role="alert">
				                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
				                            <div class="alert-text"><?php echo $value_message; ?></div>
				                            <div class="alert-close">
				                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				                                    <span aria-hidden="true"><i class="la la-close"></i></span>
				                                </button>
				                            </div>
				                        </div>
				                <?php } ?>
								<form class="kt-form" action="<?php echo base_url('auth/signin'); ?>" method="post" action="" novalidate="novalidate" id="kt_login_form">
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Username" name="username" autocomplete="off">
									</div>
									<div class="form-group">
										<input class="form-control form-control-last" type="Password" placeholder="Password" name="password">
									</div>
									<div class="row kt-login__extra">
										<div class="col kt-align-left">
											<label class="kt-checkbox">
												<input type="checkbox" name="remember"> Remember me
												<span></span>
											</label>
										</div>
										<div class="col kt-align-right">
											<a href="https://igracias.telkomuniversity.ac.id/activation/forgotsso.php" class="kt-link">Forget Password ?</a>
										</div>
									</div>
									<div class="kt-login__actions">
										<button id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-primary">Sign In</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <script>
        var KTAppOptions = {"colors":{"state":{"brand":"#366cf3","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
    </script>
	<script src="<?php echo base_url();?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

	<script src="<?php echo base_url();?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>

	 <script src="<?php echo base_url();?>assets/login/js/scripts.bundle.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/login/js/login.js" type="text/javascript"></script>
	</body>
</html>
