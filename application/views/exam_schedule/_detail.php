<!--begin::Modal-->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <p id="detail_exam_type"></p>
                <p id="detail_master_course"></p>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Course Paralel</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody id="table_detail">
                    </tbody>
                </table>
                <div class="col-md-12" >     
                    <div class="kt-section">
                        <div class="kt-section__content">
                                <div class="alert alert-danger fade show" role="alert" id="failed_alert_detail" style="display: none;">
                                <div class="alert-text" id="failed_message_detail"></div>
                                <div class="alert-close">
                                    <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_detail">
                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                                </div>
                            </div>                   
                        </div>                   
                    </div>            
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_retry_submit" data-id="0">Retry Task</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->