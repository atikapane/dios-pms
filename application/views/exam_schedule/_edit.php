<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_id" name="edit_id" />
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Exam Type</label>
                                    <input type="text" name="exam_type_edit" id="exam_type_edit" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Period</label>
                                    <select type="text" class="form-control" id="period_edit" name="period_edit">
                                    <option value=""></option>
                                        <option value="Odd">Odd Semester</option>
                                        <option value="Even">Even Semester</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Exam Date</label>
                                    <input type="text" class="form-control" id="exam_date_edit" name="exam_date_edit" placeholder="dd mmm yyyy">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Exam Start Time</label>
                                    <input type="text" class="form-control" id="exam_start_time_edit" name="exam_start_time_edit" placeholder="HH:mm">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Exam End Time</label>
                                    <input type="text" class="form-control" id="exam_end_time_edit" name="exam_end_time_edit" placeholder="HH:mm">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Total Exam Participant</label>
                                    <input type="number" class="form-control" id="total_exam_participant_edit" name="total_exam_participant_edit" placeholder="0">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ID Course Master</label>
                                    <select class="form-control" id="id_course_master_edit" name="id_course_master_edit">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                <label>List ID Course Paralel</label>
                                    <select class="form-control" id="list_id_course_paralel_edit" name="list_id_course_paralel_edit[]" multiple="multiple">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                <label>Coordinator Lecturer</label>
                                    <select class="form-control select2" id="edit_coordinator_lecturer" name="coordinator_lecturer">
                                        <option value=""></option>
                                        <?php foreach($coordinators as $coordinator): ?>
                                            <option value="<?= $coordinator->id ?>"><?= $coordinator->fullname . ' ( ' . $coordinator->employeeid . ' )' ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12" >     
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                         <div class="alert alert-danger fade show" role="alert" id="failed_alert_edit" style="display: none;">
                                            <div class="alert-text" id="failed_message_edit"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_edit">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>                   
                                    </div>                   
                                </div>            
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save Changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->