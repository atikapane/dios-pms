<script>
    //globals
    var datatable;
    var AlertUtil;
    var createForm;
    var editForm;
    var detailForm;

    function initDTEvents() {
        //delete
        $(".btn_delete").on("click", function() {
            var targetId = $(this).data("id");
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it'
            }).then(function(result) {
                if (result.value) {
                    KTApp.blockPage();
                    $.ajax({
                        type: 'GET',
                        url: "<?php echo base_url($controller_full_path . "/delete"); ?>",
                        data: {
                            id: targetId
                        },
                        dataType: "json",
                        success: function(response, status) {
                            KTApp.unblockPage();
                            if (response.status == true) {
                                datatable.reload();
                                AlertUtil.showSuccess(response.message, 5000);
                            } else {
                                AlertUtil.showFailed(response.message);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            KTApp.unblockPage();
                            AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                        }
                    });
                }
            });
        });

        //edit
        $(".btn_edit").on("click", function() {
            var targetId = $(this).data("id");
            KTApp.blockPage();
            $.ajax({
                type: 'GET',
                url: "<?php echo base_url($controller_full_path . "/get_exam_schedule"); ?>",
                data: {
                    id: targetId
                },
                dataType: "json",
                success: function(response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        editForm.populateForm(response.data);
                        $('#modal_edit').modal('show');
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            });
        });
        
        //edit
        $(".btn_view").on("click", function() {
            var targetId = $(this).data("id");
            KTApp.blockPage();
            $.ajax({
                type: 'GET',
                url: "<?php echo base_url($controller_full_path . "/get_detail"); ?>",
                data: {
                    id: targetId
                },
                dataType: "json",
                success: function(response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        detailForm.populateForm(response.data);
                        $('#modal_detail').modal('show');
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            });
        });

        $(".btn_import").on("click", function() {
            $('#modal_import').modal('show')
        })
    }

    function initDataTable() {
        var option = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '<?php echo base_url($controller_full_path . "/datatable"); ?>',
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                },
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            columns: [{
                    field: 'course_master',
                    title: 'Course Master',
                },
                {
                    field: 'exam_type',
                    title: 'Exam Type',
                },
                {
                    field: 'total_exam_participant',
                    title: 'Total Exam Participant',
                },
                {
                    field: 'period',
                    title: 'Period',
                },
                {
                    field: 'exam_date',
                    title: 'Exam Date',
                    template: function(row){
                        return moment(row.exam_date)
                    }
                },
                {
                    field: 'exam_start_time',
                    title: 'Exam Start Time',
                },
                {
                    field: 'exam_end_time',
                    title: 'Exam End Time',
                },
                {
                    field: 'course_paralel',
                    title: 'Course Paralel',
                },
                {
                    field: 'fullname',
                    title: 'Coordinator Lecturer',
                    template: function(row){
                        return row.fullname + ' ( ' + row.employeeid + ' )'
                    }
                },
                {
                    field: 'status',
                    title: 'Status',
                    template: function(row){
                        return row.status == 1 ? 'Queue' : (row.status == 2 ? 'On Progress' : (row.status == 3 ? 'Success' : 'Failed'))
                    }
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false,
                    width: 100,
                    overflow: 'visible',
                    textAlign: 'center',
                    autoHide: false,
                    template: function(row) {
                        var result = "";
                        <?php if ($privilege->can_read) { ?>
                            result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_view" title="View" data-toggle="modal" data-target="#modal_detail"><i class="flaticon-eye" style="cursor:pointer;"></i></span>';
                        <?php } ?>
                        <?php if ($privilege->can_update) { ?>
                            result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" data-toggle="modal" data-target="#modal_edit"><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                        <?php } ?>
                        <?php if ($privilege->can_delete) { ?>
                            result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" style="cursor:pointer;"><i class="flaticon2-trash"></i></span>';
                        <?php } ?>
                        return result;
                    }
                }
            ],
            layout: {
                header: true
            }
        }
        datatable = $('#kt_datatable').KTDatatable(option);
        datatable.on("kt-datatable--on-layout-updated", function() {
            initDTEvents();
        })
    }

    function initCustomRule() {}

    function initAlert() {
        AlertUtil = {
            showSuccess: function(message, timeout) {
                $("#success_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function() {
                        $("#success_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess: function() {
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed: function(message, timeout) {
                $("#failed_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function() {
                        $("#failed_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideFailed: function() {
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd: function(message, timeout) {
                $("#failed_message_add").html(message);
                if (timeout != undefined) {
                    setTimeout(function() {
                        $("#failed_alert_dismiss_add").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_add").show();
            },
            hideFailedDialogAdd: function() {
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit: function(message, timeout) {
                $("#failed_message_edit").html(message);
                if (timeout != undefined) {
                    setTimeout(function() {
                        $("#failed_alert_dismiss_edit").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_edit").show();
            },
            showFailedDialogDetail: function(message, timeout) {
                $("#failed_message_detail").html(message);
                if (timeout != undefined) {
                    setTimeout(function() {
                        $("#failed_alert_dismiss_detail").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_detail").show();
            },
            hideFailedDialogEdit: function() {
                $("#failed_alert_dismiss_edit").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click", function() {
            $("#failed_alert").hide();
        })
        $("#success_alert_dismiss").on("click", function() {
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click", function() {
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click", function() {
            $("#failed_alert_edit").hide();
        })
    }

    function initCreateForm() {
        let totalStudent = 1;
        //validator
        var validator = $("#form_add").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                error.addClass("invalid-feedback")
                element.addClass('is-invalid')
                if (element.hasClass('select2'))
                    error.insertAfter(element.next('span'))
                else
                    error.insertAfter(element)
            },
            rules: {
                // id: {
                //     required: true,
                // },
                exam_type: {
                    required: true
                },
                total_exam_participant: {
                    required: true,
                    number: true,
                    max: function(){
                        return totalStudent
                    },
                    min: 1
                },
                period: {
                    required: true,
                },
                exam_date: {
                    required: true,
                },
                exam_start_time: {
                    required: true,
                },
                exam_end_time: {
                    required: true,
                },
                id_course_master: {
                    required: true,
                },
                list_id_course_paralel: {
                    required: true,
                },
                coordinator_lecturer: {
                    required: true
                }
            },
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();
            }
        });

        //events
        $("#btn_add_submit").on("click", function() {
            var isValid = $("#form_add").valid();
            if (isValid) {
                KTApp.block('#modal_add .modal-content', {});
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url($controller_full_path . "/save"); ?>",
                    data: $('#form_add').serialize(),
                    dataType: "json",
                    success: function(data, status) {
                        KTApp.unblock('#modal_add .modal-content');
                        if (data.status == true) {
                            datatable.reload();
                            $('#modal_add').modal('hide');
                            AlertUtil.showSuccess(data.message, 5000);
                        } else {
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        KTApp.unblock('#modal_add .modal-content');
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        })

        $('#period').select2({
            placeholder: "Please select a period",
            width: '100%'
        });

        $('#id_course_master').select2({
            placeholder: "Code / Class",
            width: '100%',
            ajax: {
                url: '<?php echo base_url($controller_full_path . "/get"); ?>',
                dataType: 'json',
                processResults: function(data){
                    return {
                        results: data.data
                    }
                }
            }
        }).on('change', function(){
            let targetId = $(this).val()
            if(targetId){
                KTApp.block('#modal_add .modal-content', {});
                $.ajax({
                    type: 'GET',
                    url: "<?php echo base_url($controller_full_path . "/get_coordinator"); ?>",
                    data: {id: targetId},
                    dataType: "json",
                    success: function(data, status) {
                        KTApp.unblock('#modal_add .modal-content');
                        if (data.status == true) {
                            $('#add_coordinator_lecturer').val(data.coordinator).trigger('change')
                        } else {
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        KTApp.unblock('#modal_add .modal-content');
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        })

        $('#list_id_course_paralel').select2({
            placeholder: "Please select paralel class",
            width: '100%',
            ajax: {
                url: '<?php echo base_url($controller_full_path . "/get"); ?>',
                dataType: 'json',
                processResults: function(data){
                    courseMaster = $('#id_course_master').val()
                    if(courseMaster){
                        data.data.forEach((option, index, object) => {
                            if(option.id == courseMaster){
                                object.splice(index, 1)
                            }
                        })
                    }
                    return {
                        results: data.data
                    }
                }
            }
        });

        $('#exam_date').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });

        $('#exam_start_time').timepicker({
            defaultTime: '0:00',
            showMeridian: false,
        });

        $('#exam_end_time').timepicker({
            defaultTime: '0:00',
            showMeridian: false,
        });

        $('#add_coordinator_lecturer').select2({
            placeholder: 'Select one of the lecturer',
            width: '100%'
        })

        $('#modal_add').on('hidden.bs.modal', function() {
            validator.resetForm();
            $('#period').val(null).trigger('change');
            $('#exam_start_time').timepicker('setTime', '0:00')
            $('#exam_end_time').timepicker('setTime', '0:00')
            $('#id_course_master').val(null).trigger('change');
            $('#list_id_course_paralel').val(null).trigger('change');
            $('#add_coordinator_lecturer').val(null).trigger('change');
        })

        //disable same course paralel value
        $("list_id_course_paralel").change(function() {

            $("list_id_course_paralel option").attr("disabled", ""); //enable everything
            DisableOptions(); //disable selected values

        });

        let checkMaksStudent = () => {
            let examDate = $('#exam_date').val()
            let examStartTime = $('#exam_start_time').val()
            let examEndTime = $('#exam_end_time').val()

            if(examDate && examStartTime && examEndTime){
                KTApp.block('#modal_add .modal-content', {});
                $.ajax({
                    type: 'GET',
                    url: "<?= base_url($controller_full_path . "/get_total_student"); ?>",
                    data: {
                        exam_date: examDate,
                        exam_start_time: examStartTime,
                        exam_end_time: examEndTime
                    },
                    dataType: "json",
                    success: function(response, status){
                        KTApp.unblock('#modal_add .modal-content');
                        if(response.status == true){
                            totalStudent = response.total_student
                        }else{
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        KTApp.unblock('#modal_add .modal-content');
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                })
            }
        }

        $('#exam_date, #exam_start_time, #exam_end_time').on('change', checkMaksStudent)

        return {
            validator: validator,
        }
    }

    function initEditForm() {
        let totalStudent = 1;
        //validator
        var validator = $("#form_edit").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                error.addClass("invalid-feedback")
                element.addClass('is-invalid')
                if (element.hasClass('select2'))
                    error.insertAfter(element.next('span'))
                else
                    error.insertAfter(element)
            },
            rules: {
                // id: {
                //     required: true,
                // },
                exam_type_edit: {
                    required: true
                },
                total_exam_participant_edit: {
                    required: true,
                    number: true,
                    max: function(){
                        return totalStudent
                    },
                    min: 1
                },
                period_edit: {
                    required: true,
                },
                exam_date_edit: {
                    required: true,
                },
                exam_start_time_edit: {
                    required: true,
                },
                exam_end_time_edit: {
                    required: true,
                },
                id_course_master_edit: {
                    required: true,
                },
                list_id_course_paralel_edit: {
                    required: true,
                },
                coordinator_lecturer: {
                    required: true
                }
            },
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();
            }
        });

        //events
        $("#btn_edit_submit").on("click", function() {
            var isValid = $("#form_edit").valid();
            if (isValid) {
                KTApp.block('#modal_edit .modal-content', {});
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url($controller_full_path . "/edit"); ?>",
                    data: $('#form_edit').serialize(),
                    dataType: "json",
                    success: function(data, status) {
                        KTApp.unblock('#modal_edit .modal-content');
                        if (data.status == true) {
                            datatable.reload();
                            $('#modal_edit').modal('hide');
                            AlertUtil.showSuccess(data.message, 5000);
                        } else {
                            AlertUtil.showFailedDialogEdit(data.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        KTApp.unblock('#modal_edit .modal-content');
                        AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        })

        $('#period_edit').select2({
            placeholder: "Please select a period",
            width: '100%'
        });

        $('#id_course_master_edit').select2({
            placeholder: "Code / Class",
            width: '100%',
            ajax: {
                url: '<?php echo base_url($controller_full_path . "/get"); ?>',
                dataType: 'json',
                processResults: function(data){
                    return {
                        results: data.data
                    }
                }
            }
        }).on('change', function(event, call = true){
            if(call){
                let targetId = $(this).val()
                if(targetId){
                    KTApp.block('#modal_edit .modal-content', {});
                    $.ajax({
                        type: 'GET',
                        url: "<?php echo base_url($controller_full_path . "/get_coordinator"); ?>",
                        data: {id: targetId},
                        dataType: "json",
                        success: function(data, status) {
                            KTApp.unblock('#modal_edit .modal-content');
                            if (data.status == true) {
                                $('#edit_coordinator_lecturer').val(data.coordinator).trigger('change')
                            } else {
                                AlertUtil.showFailedDialogEdit(data.message);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            KTApp.unblock('#modal_edit .modal-content');
                            AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
                        }
                    });
                }
            }
        })

        $('#list_id_course_paralel_edit').select2({
            placeholder: "Please select paralel class",
            width: '100%',
            ajax: {
                url: '<?php echo base_url($controller_full_path . "/get"); ?>',
                dataType: 'json',
                processResults: function(data){
                    courseMaster = $('#id_course_master_edit').val()
                    if(courseMaster){
                        data.data.forEach((option, index, object) => {
                            if(option.id == courseMaster){
                                object.splice(index, 1)
                            }
                        })
                    }
                    return {
                        results: data.data
                    }
                }
            }
        });

        $('#exam_date_edit').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true
        });

        $('#exam_start_time_edit').timepicker({
            defaultTime: '0:00',
            showMeridian: false,
        });

        $('#exam_end_time_edit').timepicker({
            defaultTime: '0:00',
            showMeridian: false,
        });

        $('#edit_coordinator_lecturer').select2({
            placeholder: 'Select one of the lecturer',
            width: '100%'
        })

        $('#modal_edit').on('hidden.bs.modal', function() {
            validator.resetForm();
            $('#exam_type_edit').val(null).trigger('change');
            $('#period_edit').val(null).trigger('change');
            $("#id_course_master_edit").empty().val(null).trigger('change');
            $('#list_id_course_paralel_edit').empty().val(null).trigger('change');
            $('#edit_coordinator_lecturer').val(null).trigger('change');

            $('#exam_start_time_edit').timepicker('setTime', '0:00')
            $('#exam_end_time_edit').timepicker('setTime', '0:00')
        })

        var populateForm = function(examObjects) {
            $("#edit_id").val(examObjects[0].id);
            $("#exam_type_edit").val(examObjects[0].exam_type).trigger('change');
            $("#total_exam_participant_edit").val(examObjects[0].total_exam_participant);
            $("#period_edit").val(examObjects[0].period).trigger('change');
            $("#exam_date_edit").val(examObjects[0].exam_date);
            $("#exam_start_time_edit").val(examObjects[0].exam_start_time);
            $("#exam_end_time_edit").val(examObjects[0].exam_end_time).trigger('change', [examObjects[0].total_exam_participant]);
            var option = new Option(examObjects[0].course_master, examObjects[0].id_course_master, true, true)
            $("#id_course_master_edit").append(option).trigger('change', [false]);

            var pcourse = $.trim(examObjects[0].course_paralel_id);
            var arrayPcourse = pcourse.split(',');
            var pid = $.trim(examObjects[0].pcourse)
            var arrayPid = pid.split(',')

            for(x = 0; x < arrayPid.length; x++){
                var option = new Option(arrayPid[x], arrayPcourse[x], true, true)
                $('#list_id_course_paralel_edit').append(option).trigger('change');
            }

            $('#edit_coordinator_lecturer').val(examObjects[0].coordinator_lecturer).trigger('change')
        }

        //disable same course paralel value
        $("id_course_master_edit").change(function() {

            $("id_course_master_edit option").attr("disabled", ""); //enable everything
            DisableEditOptions(); //disable selected values

        });

        let checkMaksStudent = (event, additional = false) => {
            let examDate = $('#exam_date_edit').val()
            let examStartTime = $('#exam_start_time_edit').val()
            let examEndTime = $('#exam_end_time_edit').val()

            if(examDate && examStartTime && examEndTime){
                KTApp.block('#modal_add .modal-content', {});
                $.ajax({
                    type: 'GET',
                    url: "<?= base_url($controller_full_path . "/get_total_student"); ?>",
                    data: {
                        exam_date: examDate,
                        exam_start_time: examStartTime,
                        exam_end_time: examEndTime
                    },
                    dataType: "json",
                    success: function(response, status){
                        KTApp.unblock('#modal_add .modal-content');
                        if(response.status == true){
                            totalStudent = response.total_student
                            if(additional) totalStudent = totalStudent + parseInt(additional)
                        }else{
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        KTApp.unblock('#modal_add .modal-content');
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                })
            }
        }

        $('#exam_date_edit, #exam_start_time_edit, #exam_end_time_edit').on('change', checkMaksStudent)

        return {
            validator: validator,
            populateForm: populateForm
        }
    }

    function initDetailForm(){
        $('#btn_retry_submit').on('click', function(){
            let targetId = $(this).data('id')
            if(targetId){
                KTApp.block('#modal_detail .modal-content', {});
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url($controller_full_path . "/udpate_status"); ?>",
                    data: {id: targetId},
                    dataType: "json",
                    success: function(data, status) {
                        KTApp.unblock('#modal_detail .modal-content');
                        if (data.status == true) {
                            datatable.reload();
                            $('#modal_detail').modal('hide');
                            AlertUtil.showSuccess(data.message, 5000);
                        } else {
                            AlertUtil.showFailedDialogDetail(data.message);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        KTApp.unblock('#modal_detail .modal-content');
                        AlertUtil.showFailedDialogDetail("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        })

        $('#modal_detail').on('hidden.bs.modal', function() {
            $('#table_detail').empty()
        })

        var populateForm = function(examObjects) {
            $('#detail_exam_type').text(examObjects.exam_type)
            $('#detail_master_course').text(examObjects.subject_code + ' / ' + examObjects.class)

            examObjects.task.forEach(task => {
                status = task.status == 1 ? 'Queue' : (task.status == 2 ? 'On Progress' : (task.status == 3 ? 'Success' : 'Failed'))
                row = `
                    <tr>
                        <td>${task.subject_code + ' / ' + task.class}</td>
                        <td>${status}</td>
                    </tr>
                `
                $('#table_detail').append(row)
            })

            if(examObjects.status == 4){
                $('#btn_retry_submit').show().data('id', examObjects.id)
            }else{
                $('#btn_retry_submit').hide()
            }
        }

        return {
            populateForm: populateForm
        }
    }

    function DisableEditOptions() {
        var arr = [];
        $("list_id_course_paralel_edit option:selected").each(function() {
            arr.push($(this).val());
        });

        $("list_id_course_paralel_edit option").filter(function() {

            return $.inArray($(this).val(), arr) > -1;
        }).attr("disabled", "disabled");
    }

    function DisableAddOptions() {
        var arr = [];
        $("list_id_course_paralel option:selected").each(function() {
            arr.push($(this).val());
        });

        $("list_id_course_paralel option").filter(function() {

            return $.inArray($(this).val(), arr) > -1;
        }).attr("disabled", "disabled");
    }

    function Export() {
        <?php if ($privilege->can_read) { ?>
            var currdate = new Date();
            var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
            var filename = "Exam_Schedule_" + date + ".csv";
            var search = $('#generalSearch').val();
            $.ajax({
                url: "<?php echo base_url($controller_full_path . "/export"); ?>",
                type: 'post',
                dataType: 'html',
                data: {
                    search: search
                },
                success: function(data) {
                    var downloadLink = document.createElement("a");
                    var fileData = ['\ufeff' + data];
                    var blobObject = new Blob(fileData, {
                        type: "text/csv;charset=utf-8;"
                    });
                    var url = URL.createObjectURL(blobObject);
                    downloadLink.href = url;
                    downloadLink.download = filename;
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    document.body.removeChild(downloadLink);
                }
            });
        <?php } else { ?>
            swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
        <?php } ?>
    }

    function Print() {
        <?php if ($privilege->can_read) { ?>
            var search = $('#generalSearch').val();
            KTApp.blockPage();
            $.ajax({
                url: "<?php echo base_url($controller_full_path . "/print"); ?>",
                type: 'post',
                dataType: 'json',
                data: {
                    search: search
                },
                success: function(response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        var win = window.open("", "_blank");
                        win.document.write(response.data);
                        win.document.close();
                        setTimeout(function() {
                            win.print();
                            win.close();
                        }, 250);
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            })
        <?php } else { ?>
            swal.fire({
                title: 'Infomation',
                text: "You cannot Print this data",
                type: 'info',
            });
        <?php } ?>
    }

    jQuery(document).ready(function() {
        initAlert();
        initDataTable();
        initCustomRule();
        createForm = initCreateForm();
        editForm = initEditForm();
        detailForm = initDetailForm();
    });
</script>