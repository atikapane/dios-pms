<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Course Master</b></th>
            <th scope="col"><b>Exam Type</b></th>
            <th scope="col"><b>Total Participant</b></th>
            <th scope="col"><b>Period</b></th>
            <th scope="col"><b>Date</b></th>
            <th scope="col"><b>Start Time</b></th>
            <th scope="col"><b>End Time</b></th>
            <th scope="col"><b>Course Paralel</b></th>
            <th scope="col"><b>Coodinator</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item->course_master.'</td>
            <td>'.$item->exam_type.'</td>
            <td>'.$item->total_exam_participant.'</td>
            <td>'.$item->period.'</td>
            <td>'.$item->exam_date.'</td>
            <td>'.$item->exam_start_time.'</td>
            <td>'.$item->exam_end_time.'</td>
            <td>'.$item->course_paralel.'</td>
            <td>'.$item->fullname . ' ( ' . $item->employeeid . ' )'.'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>