<!--begin::Modal-->
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" id="add_title" name="title">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" id="add_description" name="description"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Type</label>
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio kt-radio--bold kt-radio--success" for="add_type_1">
                                            <input type="radio" class="radio_type" id="add_type_1" name="type" value="file"> File
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger" for="add_type_2">
                                            <input type="radio" class="radio_type" id="add_type_2" name="type" value="external"> External URL
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group if_file">
                                    <label>File :</label>
                                    <div class="dropzone dropzone-default col-12 col-md-12" id="add_file">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="add_file_name" name="file_name"/>
                                </div>
                                <div class="form-group if_external">
                                    <label>External URL</label>
                                    <input type="text" class="form-control" id="add_url" name="url">
                                </div>
                            </div> 
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>Status</label>
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio kt-radio--bold kt-radio--success" for="add_file_status_1">
                                            <input type="radio" class="radio_status" id="add_file_status_1" name="file_status" value="1"> Active
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger" for="add_file_status_2">
                                            <input type="radio" class="radio_status" id="add_file_status_2" name="file_status" value="2"> Inactive
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" >     
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                         <div class="alert alert-danger fade show" role="alert" id="failed_alert_add" style="display: none;">
                                            <div class="alert-text" id="failed_message_add"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_add">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>                   
                                    </div>                   
                                </div>            
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_add_submit">Submit</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->