<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">                               
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title"><?php echo $breadcrumb->parent;?></h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc"><?php echo $breadcrumb->child;?></span>       
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->                 

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand fa <?php echo $breadcrumb->icon;?>"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                       <?php echo $breadcrumb->child;?> List
                    </h3>
                </div>
            </div>

        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="col-md-pull-12" >  
                <!--begin: Alerts -->   
                <div class="kt-section">
                    <div class="kt-section__content">
                        <div class="alert alert-success fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="success_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="success_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="success_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>  
                         <div class="alert alert-danger fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="failed_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="failed_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>                   
                    </div>                   
                </div>        
                <!--end: Alerts -->           
                <!--begin: Search Form -->
                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-l-20 kt-margin-r-20">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="row align-items-center">                
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>   
                                <div class="col-md-8">
                                    <div class="pull-right">
                                        <?php if($privilege->can_create){?>           
                                            <button type="button" id="btn_create" class="btn btn-brand btn-icon-sm" data-toggle="modal" data-target="#modal_add">
                                                <i class="flaticon2-plus"></i> Add New      
                                            </button>   
                                            &nbsp
                                                <button type="button" class="btn btn-success btn-icon-sm" onclick="Export()">
                                                    <i class="flaticon2-file"></i> Export CSV      
                                                </button>
                                            &nbsp
                                                <button type="button" class="btn btn-info btn-icon-sm" onclick="Print()">
                                                    <i class="flaticon2-printer"></i> Print
                                                </button>
                                        <?php }?>       
                                    </div>
                                </div>               
                            </div>
                        </div>      
                    </div>
                </div>      
                <!--end: Search Form -->
            </div>
            <!--begin: Datatable -->        
            <table class="kt-datatable" id="kt_datatable" width="100%">
            </table>
            <!--end: Datatable -->
        </div>
        </div>
    </div>
    <!-- end:: Content -->
    
</div>

<?php
    $this->load->view('templates/Footer.php');
    $this->load->view('tutorial/_add.php');
    $this->load->view('tutorial/_edit.php');
?>


<script>
//globals
var datatable;
var AlertUtil;
var createForm;
var editForm;
function initDTEvents(){
    $(".btn_deactivate").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You are about to deactivate this data",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, deactivate it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/status_active"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailed(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $(".btn_activate").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You are about to activate this data",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, activate it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/status_active"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailed(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $(".btn_delete").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailed(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $("#btn_create").on("click",function(){
        $(".if_file,.if_external").hide();
        $('input[name=type],input[name=file_status]').each(function () { $(this).prop('checked', false); });
    });

    $(".btn_edit").on("click",function(){
        $(".if_file,.if_external").hide();
        var targetId = $(this).data("id");
        KTApp.blockPage();
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {id:targetId},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    editForm.populateForm(response.data);
                    $('#modal_edit').modal('show');
                }else{
                    AlertUtil.showFailed(response.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });  
    });
}
function initDataTable(){
    var option = {
        data: {
            type: 'remote',
            source: {
              read: {
                url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState : {cookie: false,webstorage: false},
          },
          sortable: true,
          pagination: true,
          search: {
            input: $('#generalSearch'),
          },
          columns: [
            {
                field: 'id',
                title: '#',
                width:40,
                sortable: 'asc',
                textAlign: 'center',
            },
            {
                field: 'title',
                title: 'Title',
                textAlign: 'left'
            }, 
            {
                field: 'type',
                title: 'Type',
                textAlign: 'left'
            }, 
            {
                field: 'file_path',
                title: 'Link',
                textAlign: 'center',
            },
            {
                field: 'description',
                title: 'Description',
                textAlign: 'justify',
            },
            {
                field: 'file_status',
                title: 'Status',
                textAlign: 'left',
                template: function (row) {
                    <?php if($privilege->can_update){?>
                        return (row.file_status === "1") ? '<span data-id="' + row.id + '" href="#" class="kt-badge kt-badge--primary kt-badge--inline kt-badge--pill kt-badge--rounded btn_deactivate" style="cursor:pointer;"><i class="fas fa-check mr-1"></i> Active</span>' : '<span data-id="' + row.id + '" href="#" class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded btn_activate" style="cursor:pointer;"><i class="fas fa-ban mr-1"></i> Inactive</span>';
                    <?php }?>
                }
            }, 
            {
                field: 'action',
                title: 'Action',
                sortable: false,
                width: 100,
                overflow: 'visible',
                textAlign: 'left',
                autoHide: false,
                template: function (row) {
                    var result ="";
                    <?php if($privilege->can_update){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" style="cursor:pointer;"><i class="flaticon-edit-1"></i></span>';
                    <?php }?>
                    <?php if($privilege->can_delete){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" style="cursor:pointer;"><i class="flaticon2-trash"></i></span>';
                    <?php }?>
                    return result;
                }                        
            }
          ],
          layout:{
            header:true
          }
    }
    datatable = $('#kt_datatable').KTDatatable(option);
    datatable.on("kt-datatable--on-layout-updated",function(){
        initDTEvents();
    })
}
function initCustomRule(){
    $("input[name=type]").change(function(){
        var val = $(this).val();
        if (val == "file"){
            $(".if_external").hide();
            $(".if_file").show();
        }
        else {
            $(".if_file").hide();
            $(".if_external").show();
        }
    });
}
function initAlert(){
    AlertUtil = {
        showSuccess : function(message,timeout){
            $("#success_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#success_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#success_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#success_alert_dismiss").trigger("click");
        },
        showFailed : function(message,timeout){
            $("#failed_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#failed_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#failed_alert_dismiss").trigger("click");
        },
        showFailedDialogAdd : function(message,timeout){
            $("#failed_message_add").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_add").trigger("click");
                },timeout)
            }
            $("#failed_alert_add").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_add").trigger("click");
        },
        showFailedDialogEdit : function(message,timeout){
            $("#failed_message_edit").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_edit").trigger("click");
                },timeout)
            }
            $("#failed_alert_edit").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_edit").trigger("click");
        }
    }
    $("#failed_alert_dismiss").on("click",function(){
        $("#failed_alert").hide();
    })
    $("#success_alert_dismiss").on("click",function(){
        $("#success_alert").hide();
    })
    $("#failed_alert_dismiss_add").on("click",function(){
        $("#failed_alert_add").hide();
    })
    $("#failed_alert_dismiss_edit").on("click",function(){
        $("#failed_alert_edit").hide();
    })
}
function initCreateForm(){
    //image uploader
    var fileUploader;
    Dropzone.autoDiscover = false;
    var fileUploader = new Dropzone("#add_file",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader.on("removedfile", function (file) {
        $("#add_file_name").val("");
    });

    fileUploader.on("success", function (file,response) {
        $("#add_file_name").val(response);
    });

    //validator
    var validator = $( "#form_add" ).validate({
        ignore:[],
        errorPlacement: function (error, element){
                error.addClass("invalid-feedback")
                element.addClass('is-invalid')
                if (element.hasClass('radio_type') || element.hasClass('radio_status')){
                    error.insertAfter(element.parent().parent())
                }else{
                    error.insertAfter(element)
                }
            },
        rules: {
            title: {
                required: true
            },
            type: {
                required: true
            },
            description: {
                required: true,
            },
            file_status: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_add_submit").on("click",function(){
      var isValid = $( "#form_add" ).valid();
      if(isValid){
        KTApp.block('#modal_add .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/save"); ?>",
            data : $('#form_add').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_add .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_add').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogAdd(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_add .modal-content');
                AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })
    $('#modal_add').on('hidden.bs.modal', function () {
       validator.resetForm();
       fileUploader.removeAllFiles();
    })

    return {
        fileUploader:fileUploader,
        validator:validator
    }
}
function initEditForm(){
    //image uploader
    var fileUploader;
    var fileUploader = new Dropzone("#edit_file",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader.on("success", function (file,response) {
        $("#edit_file_name").val(response);
    });

    fileUploader.on("removedfile", function (file) {
        $("#edit_file_name").val("");
    });

    //validator
    var validator = $( "#form_edit" ).validate({
        ignore:[],
        errorPlacement: function (error, element){
                error.addClass("invalid-feedback")
                element.addClass('is-invalid')
                if (element.hasClass('radio_type') || element.hasClass('radio_status')){
                    error.insertAfter(element.parent().parent())
                }else{
                    error.insertAfter(element)
                }
            },
        rules: {
            title: {
                required: true
            },
            type: {
                required: true
            },
            description: {
                required: true
            },
            file_status: {
                required: true
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_edit_submit").on("click",function(){
      var isValid = $( "#form_edit" ).valid();
      if(isValid){
        KTApp.block('#modal_edit .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/edit"); ?>",
            data : $('#form_edit').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_edit .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_edit').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogEdit(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_edit .modal-content');
                AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#modal_edit').on('hidden.bs.modal', function () {
       validator.resetForm();
       fileUploader.removeAllFiles();
    })

    var populateForm = function(tutorialObject){
        $("#edit_id").val(tutorialObject.id);
        $("#edit_title").val(tutorialObject.title);
        $("#edit_type").val(tutorialObject.type);
        $("#edit_description").val(tutorialObject.description);
        $("input[name=type][value=" + tutorialObject.type + "]").prop("checked",true);
        $("input[name=file_status][value=" + tutorialObject.file_status + "]").prop("checked",true);
        $(".if_file,.if_external").hide();
        if (tutorialObject.type == "file"){
            // re-populate dropzone
            var filename = tutorialObject.file_path.replace(/^.*[\\\/]/, '');
            var mockFile = { name: filename, size: 12345 };
            fileUploader.emit("addedfile", mockFile);
            fileUploader.files.push(mockFile)
            $("#edit_file_name").val(filename);
            $(".if_file").show();
        }
        else {
            $("#edit_url").val(tutorialObject.file_path);
            $(".if_external").show();
        }
    }
    
    return {
        fileUploader:fileUploader,
        validator:validator,
        populateForm:populateForm
    }
}

function Export(){
    <?php if($privilege->can_read){?>
    var currdate = new Date();
    var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
    var filename = "Tutorial_"+date+".csv";
    var search = $('#generalSearch').val();
    $.ajax({
        url: "<?php echo base_url($controller_full_path."/export");?>",
        type: 'post',
        dataType: 'html',
        data: { search: search  },
        success: function(data) {
            var downloadLink = document.createElement("a");
            var fileData = ['\ufeff'+data];
            var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
            var url = URL.createObjectURL(blobObject);
            downloadLink.href = url;
            downloadLink.download = filename;
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
    });
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
}

function Print(){
    <?php if($privilege->can_read){?>
    var search = $('#generalSearch').val();
    KTApp.blockPage();
    $.ajax({
        
        url: "<?php echo base_url($controller_full_path."/print_tutorial");?>",
        type: 'post',
        dataType: 'json',
        data: { search: search  },
        success: function (response, status) {
                KTApp.unblockPage();
                if (response.status == true) {
                    //populate form
                    var win = window.open("", "_blank");                    
                    win.document.write(response.data);
                    win.document.close();  
                    setTimeout(function() {
                        win.print();
                        win.close();
                    }, 250);
                } else {
                    AlertUtil.showFailed(response.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
    })    
<?php }else{ ?>
    swal.fire({
            title: 'Infomation',
            text: "You cannot Print this data",
            type: 'info',
        });
<?php } ?>
}

jQuery(document).ready(function() {
    initDataTable();
    initCustomRule();
    createForm = initCreateForm();
    editForm = initEditForm();
    initAlert();
});
</script>