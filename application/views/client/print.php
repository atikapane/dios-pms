<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Id</b></th>
            <th scope="col"><b>Nama</b></th>
            <th scope="col"><b>Instansi</b></th>
            <th scope="col"><b>Jabatan</b></th>
            <th scope="col"><b>Nomor Kontak</b></th>
            <th scope="col"><b>E-mail</b></th>
            <th scope="col"><b>Consultant</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item->id.'</td>
            <td>'.$item->nama.'</td>
            <td>'.$item->instansi.'</td>
            <td>'.$item->jabatan.'</td>
            <td>'.$item->no_kontak.'</td>
            <td>'.$item->email.'</td>
            <td>'.$item->full_name.'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>