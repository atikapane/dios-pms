<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <form action="#" id="form_edit">
            <input type="hidden" class="form-control" id="id" name="id">

            <div class="row">
                
                <div class="col-md-6 pull-left">
                    <div class="form-group">
						<label>Fullname</label>
						<input type="text" class="form-control" id="fullname-edit" name="fullname" placeholder="" required>
					</div>
                     <div class="form-group">
                        <label>Institution</label>
                        <input type="text" class="form-control" id="institution-edit" name="institution" placeholder="" >
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="email-edit" name="email" placeholder="" >
                    </div> 
                      <div class="form-group">
                        <label>Directorate</label>
                        <input type="text" class="form-control" id="directorate-edit" name="directorate" placeholder="" >
                    </div>                   
                </div>
                <div class="col-md-6 full-right">                  
                    <div class="form-group">
						<label>Unit</label>
						<input type="email" class="form-control" id="unit-edit" name="unit" placeholder="" required>
					</div>
                     <div class="form-group">
                        <label>Structural Functional Position</label>
                        <input type="text" class="form-control" id="structural_functional_position-edit" name="structural_functional_position" placeholder="" required>
                    </div>
                     <div class="form-group">
                        <label>Structural Academic Position</label>
                        <input type="text" class="form-control" id="structural_academic_position-edit" name="structural_academic_position" placeholder="" >
                    </div> 
                    <div class="form-group">
                        <label>Group</label>
                        <!-- <select class="form-control select2" name="group" id="group">
                            <option></option>
                            <?php 
                            //foreach ($group as $row) {
                                //echo '<option value="'.$row->group_id.'">'.$row->group_name.'</option>';
                            //}
                            ?>
                        </select> -->
                        <select class="form-control select2" name="group[]" id="group" multiple="multiple">
                        <option></option>
                        <?php 
                        foreach($group as $row)
                        {
                            echo '<option value="'.$row->group_id.'">'.$row->group_name.'</option>';                        
                        }                        
                        ?>
                        </select>
                    </div>                    
                </div>                             
                
            </div>
            </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn_edit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->