<script> 

    function popAdd(el)
    {
        event.preventDefault();
        $('#form_add')[0].reset(); //reset form on modals 
    }
    
    function popEdit(el)
    {
        var id = $(el).attr('data-id');
        var url_data = '<?php echo base_url(); ?>users/get_user_byid/' + id;
        $.get(url_data, function (data, status){
            var response = JSON.parse(data);
            console.log(response)
            if(status == 'success')
            {
                $('#id').val(response.id);
                $('#fullname-edit').val(response.fullname);               
                $('#email-edit').val(response.email);               
                $('#institution-edit').val(response.institution);               
                $('#directorate-edit').val(response.directorate);               
                $('#structural_functional_position-edit').val(response.structural_functional_position);               
                $('#structural_academic_position-edit').val(response.structural_academic_position);              
                $('#unit-edit').val(response.unit);              
                $('#group').val(response.groupid).trigger('change.select2');              


                setTimeout(function () {               
                $('#modal_edit').modal('show');
                }, 1000);                 
            }else {
                console.log("Error please check your connection");                
            }
        });        
    }

    function popDelete(el)
    {        
       $('.deleteConfirm').attr('href', '#');
       var id = $(el).attr('data-id');
       $('#Id_Delete').val(id);
    }

      //save data
      $('#btn_add').on('click',function(){    
           
           $.ajax({
              type : 'POST',
              url : '<?php echo base_url(); ?>users/save',
              data : $('#form_add').serialize(),
              dataType : "json",
              success : function(data,status)
              {
                  if(status=true)
                  {
                      $('#modal_add').modal('hide');
                       window.datatable.reload();
                      setTimeout(function () 
                      {
                        toastr.success("Data is successfully inputted");
                      }, 400); 
                  }                
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                    console.log(jqXHR,textStatus, errorThrown);
                    setTimeout(function () 
                    {
                        toastr.success("Please check your connection");
                    }, 400); 
              }  
           }); 
           return false; 
      });
  
      //update data
      $('#btn_edit').on('click',function(){
          $.ajax({
              type : "POST",
              url : "<?php echo base_url('users/update'); ?>",
              dataType : "JSON",
              data : $('#form_edit').serialize(),
              success : function(data,status)
              {
                  if(status=true){
                      $('#modal_edit').modal('hide');
                      window.datatable.reload(); 
                      setTimeout(function () 
                      {
                        toastr.success("Data is successfully updated");
                      }, 400);                  
                  } 
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                      console.log(jqXHR,textStatus, errorThrown);
                      setTimeout(function () 
                      {
                        toastr.success("Please check your connection");
                      }, 400);               } 
          });
          return false;
      });
  
      //delete data
      $('#btn_delete').on('click',function(){
          $.ajax({
              type : "POST",
              url : '<?php echo base_url(); ?>users/delete',            
              data : $('#form_delete').serialize(),
              dataType : "JSON",
              success : function(data,status)
              {
                  if(status=true){
                      $('#modal_delete').modal('hide');
                       window.datatable.reload();
                      setTimeout(function () 
                      {
                        toastr.success("Data is successfully deleted");
                      }, 400); 
                  } 
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                    console.log(jqXHR,textStatus, errorThrown);
                    setTimeout(function () 
                    {
                        toastr.success("Please check your connection");
                    }, 400);   
              } 
          });
          return false;
      });

    var KMSIDatatable = function () {
            // Private functions

            var options = {
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo base_url(); ?>users/get_user'
                        },
                    },
                    pageSize: 10,
                    serverSide: true,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,                    
                    timeout: 30000,
                    saveState:
                    {
                        cookie: false,
                        webstorage: false
                    }
                },

                // layout definition
                layout: {
                    scroll: true, // enable/disable datatable scroll both horizontal and
                    // vertical when needed.
                    height: 350, // datatable's body's fixed height
                    footer: false // display/hide footer
                },

                // column sorting
                sortable: true,
                pagination: true,
                search: {
                  input: $('#generalSearch-Education'),
                },

                // columns definition
                columns: [
                    {
                        field: 'fullname',
                        title: 'Fullname',
                        width: 250,
                        sortable: 'asc',                          
                    }, 
                    {
                        field: 'institution',
                        title: 'Institution',
                        width: 250,
                        sortable: 'asc',                          
                    }, 
                    {
                        field: 'email',
                        title: 'Email',
                        width: 150,
                        sortable: 'asc',                          
                    },
                    {
                        field: 'directorate',
                        title: 'Directorate',
                        width: 250,
                        sortable: 'asc',                          
                    }, 
                    {
                        field: 'unit',
                        title: 'Unit',
                        width: 250,
                        sortable: 'asc',                          
                    },
                    {
                        field: 'id',
                        title: 'Action',
                        sortable: 'asc',
                        width: 80,
                        overflow: 'visible',
                        textAlign: 'left',
                        autoHide: false,
                        template: function (row) {
                            var result = '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md editBtn" title="Edit" data-toggle="modal" data-target="#modal_edit"><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                                // result += '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md delBtn" title="Delete" data-toggle="modal" data-target="#modal_delete"><i class="flaticon2-trash" style="cursor:pointer;"></i></span>';
                            return result;
                        }                        
                    }
                ],
            };

            var dataSelector = function () {
                options.search = {
                    input: $('#generalSearch-Education'),
                    delay: 100
                };

                window.datatable = $('#datatable-kmsi').KTDatatable(options);

                $('#kt_form_status').on('change', function () {
                   datatable.search($(this).val(), 'Status');
                });

                $('#kt_form_type').on('change', function () {
                   datatable.search($(this).val().toLowerCase(), 'Type');
                });

                $('#kt_form_status,#kt_form_type').selectpicker();

                datatable.on('kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',function (e) 
                {
                       var checkedNodes = datatable.rows('.kt-datatable__row--active').nodes();
                       var count = checkedNodes.length;
                       $('#kt_datatable_selected_number').html(count);
                       if (count > 0) {
                           $('#kt_datatable_group_action_form').collapse('show');
                       } else {
                           $('#kt_datatable_group_action_form').collapse('hide');
                       }
                 });                
                
                
                $('#kt_modal_fetch_id').on('show.bs.modal', function (e) {
                    var ids = datatable.rows('.kt-datatable__row--active').
                        nodes().
                        find('.kt-checkbox--single > [type="checkbox"]').
                        map(function (i, chk) {
                            return $(chk).val();
                        });
                    var c = document.createDocumentFragment();
                    for (var i = 0; i < ids.length; i++) {
                        var li = document.createElement('li');
                        li.setAttribute('data-id', ids[i]);
                        li.innerHTML = 'Selected record ID: ' + ids[i];
                        c.appendChild(li);
                    }
                    $(e.target).find('.kt-datatable_selected_ids').append(c);
                }).on('hide.bs.modal', function (e) {
                    $(e.target).find('.kt-datatable_selected_ids').empty();
                });
            };

            return {
                init: function () {
                    dataSelector();
                },
            };
    }();
    
    $(document).ready(function (){
        KMSIDatatable.init();

         $('#group').select2({
        placeholder: "Add a group",
        width: '100%',
        tags: true
    });
        //pop up add 
        $(document).on("click", ".addBtn", function () {
                var el = $(this);
                popAdd(el);
        });

        //pop up edit
        $(document).on("click", ".editBtn", function () {
                var el = $(this);
                popEdit(el);
        });

        //pop up delete
        $(document).on("click", ".delBtn", function () {
                var el = $(this);
                popDelete(el);
        });
        
    });

</script>