<!--begin::Modal-->
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <form action="#" id="form_add" class="form-horizontal">
            <div class="row">                
                <div class="col-md-6">
                    <div class="form-group">
						<label>Fullname</label>
						<input type="text" class="form-control" id="fullname" name="fullname" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Telp.</label>
						<input type="text" class="form-control" id="telp" name="telp" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Address</label>
						<input type="text" class="form-control" id="address" name="address" placeholder="" >
					</div>
                </div>                
                
            </div>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn_add">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->