<script>   
var datatable;
var AlertUtil;
var createForm;
var editForm;
function initDTEvents(){
    $(".btn_delete").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailedDialogEdit(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $(".btn_edit").on("click",function(){
        var targetId = $(this).data("id");
        KTApp.blockPage();
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {id:targetId},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    editForm.populateForm(response.data);
                    $('#modal_edit').modal('show');
                }else{
                    AlertUtil.showFailed(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });  
    });
}
function initDataTable(){
    var option = {
        data: {
            type: 'remote',
            source: {
              read: {
                url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState : {cookie: false,webstorage: false},
          },
          sortable: true,
          pagination: true,
          search: {
            input: $('#generalSearch'),
          },
          columns: [
            {
                field: 'group_id',
                title: '#',
                width:40,
                sortable: 'asc',
                textAlign: 'center',
            }, 
            {
                field: 'group_name',
                title: 'Priority',
                width:60,
                textAlign: 'center',
            }, 
            {
                field: 'action',
                title: 'Action',
                sortable: false,
                width: 80,
                overflow: 'visible',
                textAlign: 'left',
                autoHide: false,
                template: function (row) {
                    var result ="";
                    result = result + '<span data-id="' + row.group_id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" ><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                    result = result + '<span data-id="' + row.group_id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" ><i class="flaticon2-trash" style="cursor:pointer;"></i></span>';
                    return result;
                }                        
            }
          ],
          layout:{
            header:true
          }
    }
    datatable = $('#kt_datatable').KTDatatable(option);
    datatable.on("kt-datatable--on-layout-updated",function(){
        initDTEvents();
    })
}
function initCustomRule(){
}
function initAlert(){
    AlertUtil = {
        showSuccess : function(message,timeout){
            $("#success_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#success_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#success_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#success_alert_dismiss").trigger("click");
        },
        showFailed : function(message,timeout){
            $("#failed_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#failed_alert").show();
            KTUtil.scrollTop();
        },
        hideFailed : function(){
            $("#failed_alert_dismiss").trigger("click");
        },
        showFailedDialogAdd : function(message,timeout){
            $("#failed_message_add").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_add").trigger("click");
                },timeout)
            }
            $("#failed_alert_add").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_add").trigger("click");
        },
        showFailedDialogEdit : function(message,timeout){
            $("#failed_message_edit").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_edit").trigger("click");
                },timeout)
            }
            $("#failed_alert_edit").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_edit").trigger("click");
        }
    }
    $("#failed_alert_dismiss").on("click",function(){
        $("#failed_alert").hide();
    })
    $("#success_alert_dismiss").on("click",function(){
        $("#success_alert").hide();
    })
    $("#failed_alert_dismiss_add").on("click",function(){
        $("#failed_alert_add").hide();
    })
    $("#failed_alert_dismiss_edit").on("click",function(){
        $("#failed_alert_edit").hide();
    })
}
function initCreateForm(){
    //image uploader
    var imageUploader;
    Dropzone.autoDiscover = false;
    var imageUploader = new Dropzone("#add_image",{
        url:"<?php echo base_url($controller_full_path."/image_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png"
    });

    imageUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    imageUploader.on("removedfile", function (file) {
        $("#add_image_name").val("");
    });

    imageUploader.on("success", function (file,response) {
        $("#add_image_name").val(response);
    });

    //validator
    var validator = $( "#form_add" ).validate({
        ignore:[],
        rules: {
            url: {
                url: true
            },
            priority: {
                required: true,
                digits: true
            },
            image_name: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_add_submit").on("click",function(){
      var isValid = $( "#form_add" ).valid();
      if(isValid){
        KTApp.block('#modal_add .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/save"); ?>",
            data : $('#form_add').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_add .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_add').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogAdd(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_add .modal-content');
                AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#modal_add').on('hidden.bs.modal', function () {
       validator.resetForm();
       imageUploader.removeAllFiles();
    })

    return {
        imageUploader:imageUploader,
        validator:validator
    }
}
function initEditForm(){
    //image uploader
    var imageUploader;
    var imageUploader = new Dropzone("#edit_image",{
        url:"<?php echo base_url($controller_full_path."/image_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png"
    });

    imageUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    imageUploader.on("success", function (file,response) {
        $("#edit_image_name").val(response);
    });

    imageUploader.on("removedfile", function (file) {
        $("#edit_image_name").val("");
    });

    //validator
    var validator = $( "#form_edit" ).validate({
        ignore:[],
        rules: {
            url: {
                url: true
            },
            priority: {
                required: true,
                digits: true
            },
            image_name: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_edit_submit").on("click",function(){
      var isValid = $( "#form_edit" ).valid();
      if(isValid){
        KTApp.block('#modal_edit .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/edit"); ?>",
            data : $('#form_edit').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_edit .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_edit').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailed(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_edit .modal-content');
                AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#modal_edit').on('hidden.bs.modal', function () {
       validator.resetForm();
       imageUploader.removeAllFiles();
    })

    var populateForm = function(bannerObject){
        $("#edit_id").val(bannerObject.id);
        $("#edit_url").val(bannerObject.url);
        $("#edit_priority").val(bannerObject.priority);

        // re-populate dropzone
        var filename = bannerObject.img_path.replace(/^.*[\\\/]/, '')
        var mockFile = { name: filename, size: 12345 };
        imageUploader.emit("addedfile", mockFile);
        imageUploader.emit("thumbnail", mockFile, bannerObject.img_path);
        imageUploader.files.push(mockFile)
        $("#edit_image_name").val(filename);
    }
    
    return {
        imageUploader:imageUploader,
        validator:validator,
        populateForm:populateForm
    }
}
jQuery(document).ready(function() { 
    initDataTable();
    initCustomRule();
    createForm = initCreateForm();
    editForm = initEditForm();
    initAlert();
});
</script>