<!--begin::Modal-->
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form id="form_add" action="#">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="form-group">
						<label>Group</label>
						<input type="text" class="form-control" id="group_name" name="group_name" placeholder="" required>
					</div>
                </div>                     
                
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn_add">Save changes</button>
            </div>
        </div>
    </div>
</div>
</form>
</div>
<!--end::Modal-->