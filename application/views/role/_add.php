begin::Modal-->
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form id="form_add" action="#">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Enrol User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="form-group">
						<label>User</label>						
                        <select class="form-control select2" name="userid[]" id="userid" multiple="multiple">
                        <option></option>
                        <?php 
                        foreach($users as $row)
                        {
                            echo '<option value="'.$row->id.'">'.$row->fullname.'</option>';                        
                        }                        
                        ?>
                        <!-- <option value="100">foo</option>
                        <option value="101">bar</option>
                        <option value="102">bat</option>
                        <option value="103">baz</option> -->
                        </select>
                        <input type="hidden" class="form-control" id="groupid" name="groupid" value="<?php echo $this->uri->segment(3); ?>">

					</div>
                </div>                     
                
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn_add">Save changes</button>
            </div>
        </div>
    </div>
</div>
</form>
</div>
<!--end::Modal