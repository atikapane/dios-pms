<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">								
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title">Page</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">Roles</span>           
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->					

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Role List
                    </h3>
                </div>                
    </div>

	<div class="kt-portlet__body kt-portlet__body--fit">
        <br/>
        <div class="kt-datatable" id="datatable-group"></div>
		<!--end: Datatable -->
	</div>
</div>
    </div>
    <!-- end:: Content -->
    <!-- end:: Content -->
    
</div>

<?php
$this->load->view('templates/Footer.php');
?>

<script type="text/javascript">

    var KMSIDatatableGroup = function () {
            // Private functions

            var options = {
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo base_url(); ?>/role/get_group'
                        },
                    },
                    pageSize: 10,
                    serverSide: true,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,                    
                    timeout: 30000,
                    saveState:
                    {
                        cookie: false,
                        webstorage: false
                    }
                },

                // layout definition
                layout: {
                    scroll: true, // enable/disable datatable scroll both horizontal and
                    // vertical when needed.
                    height: 350, // datatable's body's fixed height
                    footer: false // display/hide footer
                },

                // column sorting
                sortable: true,
                pagination: true,

                // columns definition
                columns: [
                    {
                        field: 'group_name',
                        title: 'Group',
                        width: 150,
                        sortable: 'desc',                                                    
                    },  {
                        field: 'role',
                        title: 'Role',
                        width: 150,
                        sortable: 'desc',
                        template: function (row) {
                            var   result="";
                            if(row.role > 0){
                                result += '<i class="fa fa-check-circle"></i>';
                            }else{
                                result += '<i class="fa fa-circle-notch"></i>';
                            }
                            return result;
                        }                           
                    },{
                        field: 'user_id',
                        title: 'Action',
                        sortable: 'desc',
                        width: 80,
                        overflow: 'visible',
                        textAlign: 'left',
                        autoHide: false,
                        template: function (row) {
                            var   result = '<a href="<?php echo base_url("role/list/'+row.group_id+'") ?>" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon-eye" style="cursor:pointer;"></i></a>';
                            return result;
                        }                        
                    }
                ],
            };

            var dataSelector = function () {
                options.search = {
                    input: $('#generalSearch-Education'),
                    delay: 100
                };

                window.datatable_group = $('#datatable-group').KTDatatable(options);

                $('#kt_form_status').on('change', function () {
                   datatable_group.search($(this).val(), 'Status');
                });

                $('#kt_form_type').on('change', function () {
                   datatable_group.search($(this).val().toLowerCase(), 'Type');
                });

                $('#kt_form_status,#kt_form_type').selectpicker();

                datatable_group.on('kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',
                
                function (e) {
                       var checkedNodes = datatable_group.rows('.kt-datatable__row--active').nodes();
                       var count = checkedNodes.length;
                       $('#kt_datatable_selected_number').html(count);
                       if (count > 0) {
                           $('#kt_datatable_group_action_form').collapse('show');
                       } else {
                           $('#kt_datatable_group_action_form').collapse('hide');
                       }
                   });

                $('#kt_modal_fetch_id').on('show.bs.modal', function (e) {
                    var ids = datatable_group.rows('.kt-datatable__row--active').
                        nodes().
                        find('.kt-checkbox--single > [type="checkbox"]').
                        map(function (i, chk) {
                            return $(chk).val();
                        });
                    var c = document.createDocumentFragment();
                    for (var i = 0; i < ids.length; i++) {
                        var li = document.createElement('li');
                        li.setAttribute('data-id', ids[i]);
                        li.innerHTML = 'Selected record ID: ' + ids[i];
                        c.appendChild(li);
                    }
                    $(e.target).find('.kt-datatable_selected_ids').append(c);
                }).on('hide.bs.modal', function (e) {
                    $(e.target).find('.kt-datatable_selected_ids').empty();
                });
            };

            return {
                init: function () {
                    dataSelector();
                },
            };
    }();


    $(document).ready(function (){
        KMSIDatatableGroup.init();
    }); 
       
</script>