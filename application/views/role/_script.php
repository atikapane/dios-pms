<script>  

    var id = document.getElementById("role_id").value;


    function popAdd(el)
    {
        event.preventDefault();
        $('#form_add')[0].reset(); //reset form on modals 
    }

    function popDelete(el)
    {        
       $('.deleteConfirm').attr('href', '#');
       var id = $(el).attr('data-id');
       $('#Id_Delete').val(id);
    }

    //save data
      $('#btn_add').on('click',function(){    
           
           $.ajax({
              type : 'POST',
              url : '<?php echo base_url(); ?>role/save',
              data : $('#form_add').serialize(),
              dataType : "json",
              success : function(data,status)
              {
                  if(status=true)
                  {
                      $('#modal_add').modal('hide');
                      window.datatable_role.reload();
                      setTimeout(function () 
                      {
                        toastr.success("Data is successfully inputted");
                      }, 400); 
                  }                
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                    console.log(jqXHR,textStatus, errorThrown);
                    setTimeout(function () 
                    {
                        toastr.success("Please check your connection");
                    }, 400); 
              }  
           }); 
           return false; 
      });

      //delete data
      $('#btn_delete').on('click',function(){
          $.ajax({
              type : "POST",
              url : '<?php echo base_url(); ?>role/delete',            
              data : $('#form_delete').serialize(),
              dataType : "JSON",
              success : function(data,status)
              {
                  if(status=true){
                      $('#modal_delete').modal('hide');
                       window.datatable_role.reload();
                      setTimeout(function () 
                      {
                        toastr.success("Data is successfully deleted");
                      }, 400); 
                  } 
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                    console.log(jqXHR,textStatus, errorThrown);
                    setTimeout(function () 
                    {
                        toastr.success("Please check your connection");
                    }, 400);   
              } 
          });
          return false;
      });

    var KMSIDatatableRole = function () {
            // Private functions            
            var options = {
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '<?php echo base_url(); ?>/role/get_role_group/'+id
                        },
                    },
                    pageSize: 10,
                    serverSide: true,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,                    
                    timeout: 30000,
                    saveState:
                    {
                        cookie: false,
                        webstorage: false
                    }
                },

                // layout definition
                layout: {
                    scroll: true, // enable/disable datatable scroll both horizontal and
                    // vertical when needed.
                    height: 350, // datatable's body's fixed height
                    footer: false // display/hide footer
                },

                // column sorting
                sortable: true,
                pagination: true,

                // columns definition
                columns: [
                    {
                        field: 'group_name',
                        title: 'Group',
                        width: 150,
                        sortable: 'desc',                                                    
                    }, {
                        field: 'fullname',
                        title: 'Fullname',
                        width: 150,
                        sortable: 'desc',                                                    
                    }, {
                        field: 'roles_id',
                        title: 'Action',
                        sortable: 'desc',
                        width: 80,
                        overflow: 'visible',
                        textAlign: 'left',
                        autoHide: false,
                        template: function (row) {
                            var result = '<span data-id="' + row.roles_id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md delBtn" title="Delete" data-toggle="modal" data-target="#modal_delete"><i class="flaticon2-trash" style="cursor:pointer;"></i></span>';
                            return result;
                        }                        
                    }
                ],
            };

            var dataSelector = function () {
                options.search = {
                    input: $('#generalSearch-Education'),
                    delay: 100
                };

                window.datatable_role = $('#datatable-role').KTDatatable(options);

                $('#kt_form_status').on('change', function () {
                   datatable_role.search($(this).val(), 'Status');
                });

                $('#kt_form_type').on('change', function () {
                   datatable_role.search($(this).val().toLowerCase(), 'Type');
                });

                $('#kt_form_status,#kt_form_type').selectpicker();

                datatable_role.on('kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',
                   function (e) {
                       var checkedNodes = datatable_role.rows('.kt-datatable__row--active').nodes();
                       var count = checkedNodes.length;
                       $('#kt_datatable_selected_number').html(count);
                       if (count > 0) {
                           $('#kt_datatable_group_action_form').collapse('show');
                       } else {
                           $('#kt_datatable_group_action_form').collapse('hide');
                       }
                   });

                $('#kt_modal_fetch_id').on('show.bs.modal', function (e) {
                    var ids = datatable_role.rows('.kt-datatable__row--active').
                        nodes().
                        find('.kt-checkbox--single > [type="checkbox"]').
                        map(function (i, chk) {
                            return $(chk).val();
                        });
                    var c = document.createDocumentFragment();
                    for (var i = 0; i < ids.length; i++) {
                        var li = document.createElement('li');
                        li.setAttribute('data-id', ids[i]);
                        li.innerHTML = 'Selected record ID: ' + ids[i];
                        c.appendChild(li);
                    }
                    $(e.target).find('.kt-datatable_selected_ids').append(c);
                }).on('hide.bs.modal', function (e) {
                    $(e.target).find('.kt-datatable_selected_ids').empty();
                });
            };

            return {
                init: function () {
                    dataSelector();
                },
            };
    }();    
    
    $(document).ready(function (){

         //Initialize Select2 Elements
     $('#userid').select2({
        placeholder: "Add a tag",
        width: '100%',
        tags: true
    });

        KMSIDatatableRole.init();

        //pop up add 
        $(document).on("click", ".addBtn", function () {
                var el = $(this);
                popAdd(el);
        });

        //pop up delete
        $(document).on("click", ".delBtn", function () {
                var el = $(this);
                popDelete(el);
        });

    });

</script>