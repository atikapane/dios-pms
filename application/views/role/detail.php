<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">								
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title">Page</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">Role Group</span>           
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->					

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Role group List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">            
                <button type="button" class="btn btn-brand btn-icon-sm addBtn" data-toggle="modal" data-target="#modal_add">
                    <i class="flaticon2-plus"></i> Add New  	
                </button>	
            </div>		
        </div>
    </div>

	<div class="kt-portlet__body kt-portlet__body--fit">
        <br/>
        <input type="hidden" name="role_id" id="role_id" value="<?php echo $this->uri->segment(3); ?>">
        <div class="kt-datatable" id="datatable-role"></div>

		<!--end: Datatable -->
	</div>
</div>
    </div>
    <!-- end:: Content -->
    <!-- end:: Content -->
    
</div>

<?php
$this->load->view('templates/Footer.php');
?>

<?php require '_add.php'; ?>
<?php require '_delete.php'; ?>
<?php require '_script.php'; ?>