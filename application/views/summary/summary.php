<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>
<link href="<?php echo base_url(); ?>assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">	                             
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title"><?php echo $breadcrumb->parent;?></h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc"><?php echo $breadcrumb->child;?></span>       
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->                 		

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
      <!--begin::Portlet-->
      
		<div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-toolbar">
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                    <?php if($clearance_level >= 4){?>
                            <li class="nav-item" id="nav-item-person">
                                <a class="nav-link" data-toggle="tab" href="#person_content" role="tab" data-loaded="true">
                                    <i class="flaticon2-pie-chart-2" aria-hidden="true"></i>PER ORANG
                                </a>
                            </li>      
                        <?php }?>
                        <?php if($clearance_level >= 1){?>
                            <li class="nav-item" id="nav-item-project">
                                <a class="nav-link active" data-toggle="tab" href="#project_content" role="tab" data-loaded="false">
                                     <i class="flaticon2-pie-chart" aria-hidden="true"></i>PER PROJECT
                                </a>
                            </li>      
                        <?php }?>           
                    </ul>                   
                </div>
            </div>

            <div class="kt-portlet__body" style="">                   
                <div class="tab-content">
                    <div class="tab-pane" id="person_content" role="tabpanel"> 
                        <div class="col-md-pull-12 col-xl-pull-12" >                      
                            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                    <div class="row align-items-center" id="person_filter">
                                        <div class="col-xl-2 col-md-2">
                                            <div class="form-group">
                                                <label>Month</label>
                                                <select class="form-control month_filter" id="person_month_filter">
                                                    <option value="0">All</option>
                                                    <option value="1">January</option>
                                                    <option value="2">February</option>
                                                    <option value="3">March</option>
                                                    <option value="4">April</option>
                                                    <option value="5">May</option>
                                                    <option value="6">June</option>
                                                    <option value="7">July</option>
                                                    <option value="8">August</option>
                                                    <option value="9">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-md-2">
                                            <div class="form-group">
                                                <label>Year</label>
                                                <select class="form-control year_filter" id="person_year_filter">
                                                    <option value="0">All</option>
                                                    <?php 
                                                            $start_year = "2018";
                                                            $current_year = intval(date("Y"));
                                                            for ($i=$start_year; $i <= $current_year ; $i++) { 
                                                                echo "<option value='$i'>$i</option>";
                                                            }       
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-md-2">
                                            <div class="form-group">
                                                <label>User</label>
                                                <select class="form-control person_filter" id="person_person_filter">
                                                    <option value="0">All</option>
                                                    <?php foreach($users as $user): ?>
                                                        <option value="<?= $user->id ?>"><?= $user->fullname ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-md-2">
                                            <div class="form-group">
                                                <label>Project</label>
                                                <select class="form-control person_project_filter" id="person_project_filter">
                                                    <option value="0">All</option>
                                                    <?php foreach($projects as $project): ?>
                                                        <option value="<?= $project->id ?>"><?= $project->nama_project ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" id="person_apply_filter" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                            </div> 
                            <div class="kt-form kt-form--label-right">
                                <div class="row align-items-center">
                                        <div class="col-xl-12 order-2 order-xl-1">
                                            <div class="row align-items-center">                   
                                                <div class="col-md-12">
                                                    <div class="pull-right">
                                                        <?php if($privilege->can_read && $clearance_level >= 3){?>
                                                            <button type="button" class="btn btn-success btn-icon-sm" onclick="Export('person')">
                                                                <i class="flaticon2-file"></i> Export CSV      
                                                            </button> 
                                                        <?php } ?>         
                                                    </div>
                                                </div>           
                                            </div>
                                        </div>      
                                </div>
                            </div>   
                        </div>
                        <div class="kt-datatable" id="datatable-person"></div>                      
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Review Summary</h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-section" id="person_statistic">
                                    <h5 class="kt-section__info">Statistics</h5>
                                    <div class="kt-section__content kt-section__content">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Project Name</th>
                                                        <th class="text-center">Hours</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="project_hour_table">
                                                </tbody>
                                            </table>
                                        </div>   
                                    </div>
                                </div>
                                <div class="kt-section">
                                    <div class="kt-section__content kt-section__content">
                                        <h5 class="kt-section__info">Chart</h5>
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <!--begin:: Widgets/Revenue Change-->
                                                <div class="kt-portlet kt-portlet--height-fluid">
                                                    <div class="kt-widget14">
                                                        <div class="kt-widget14__content">
                                                            <div class="kt-widget14__chart col-3">
                                                                <div id="person_hours_pie_chart" style="height: 200px; width: 100%;"></div>
                                                            </div>      
                                                            <div class="kt-widget14__legends legends_person col"></div>            
                                                            <div class="kt-widget14__chart col-5">
                                                                <div id="person_hours_bar_chart"  style="height: 300px; width: 100%; "></div>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>  
                                            </div>  
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="tab-pane active" id="project_content" role="tabpanel"> 
                        <div class="col-md-pull-12 col-xl-pull-12" >                    
                            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                                <div class="row align-items-center" id="project_filter">
                                    <div class="col-xl-2 col-md-2">
                                        <div class="form-group">
                                            <label>Month</label>
                                            <select class="form-control month_filter" id="project_month_filter">
                                                <option value="0">All</option>
                                                <option value="1">January</option>
                                                <option value="2">February</option>
                                                <option value="3">March</option>
                                                <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                                <option value="8">August</option>
                                                <option value="9">September</option>
                                                <option value="10">October</option>
                                                <option value="11">November</option>
                                                <option value="12">December</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-2">
                                        <div class="form-group">
                                            <label>Year</label>
                                            <select class="form-control year_filter" id="project_year_filter">
                                                <option value="0">All</option>
                                                   <?php 
                                                        $start_year = "2018";
                                                        $current_year = intval(date("Y"));
                                                        for ($i=$start_year; $i <= $current_year ; $i++) { 
                                                            echo "<option value='$i'>$i</option>";
                                                        }       
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-2">
                                        <div class="form-group">
                                            <label>Project</label>
                                            <select class="form-control project_filter" id="project_project_filter">
                                                <option value="0">All</option>
                                                <?php foreach($projects as $project): ?>
                                                    <option value="<?= $project->id ?>"><?= $project->nama_project ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-md-2">
                                            <div class="form-group">
                                                <label>User</label>
                                                <select class="form-control person_filter" id="project_person_filter">
                                                    <option value="0">All</option>
                                                    <?php foreach($users as $user): ?>
                                                        <option value="<?= $user->id ?>"><?= $user->fullname ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <button type="button" id="project_apply_filter" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                            </div>
                            <div class="kt-form kt-form--label-right">
                                <div class="row align-items-center">
                                    <div class="col-xl-12 order-2 order-xl-1">
                                        <div class="row align-items-center">                   
                                            <div class="col-md-12">
                                                <div class="pull-right">
                                                    <button type="button" class="btn btn-success btn-icon-sm" onclick="Export('project')">
                                                        <i class="flaticon2-file"></i> Export CSV      
                                                    </button> 
                                                </div>
                                            </div>           
                                        </div>
                                    </div>      
                                </div>
                            </div>      
                        </div>
                        <div class="kt-datatable" id="datatable-project"></div>
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Review Summary</h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-section" id="project_statistic">
                                    <h5 class="kt-section__info">Statistics</h5>
                                    <div class="kt-section__content kt-section__content">
                                        <div class="table-scrollable">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Employee Name</th>
                                                        <th class="text-center">Hours</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="employees_hour_table">
                                                </tbody>
                                            </table>
                                        </div>   
                                    </div>
                                </div>
                                <div class="kt-section">
                                    <div class="kt-section__content kt-section__content">
                                        <h5 class="kt-section__info">Chart</h5>
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <!--begin:: Widgets/Revenue Change-->
                                                <div class="kt-portlet kt-portlet--height-fluid">
                                                    <div class="kt-widget14">
                                                        <div class="kt-widget14__content">
                                                            <div class="kt-widget14__chart col-3">
                                                                <div id="project_hours_pie_chart" style="height: 200px; width: 100%;"></div>
                                                            </div>      
                                                            <div class="kt-widget14__legends legends_project col"></div>            
                                                            <div class="kt-widget14__chart col-5">
                                                                <div id="project_hours_bar_chart"  style="height: 300px; width: 100%; "></div>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>  
                                            </div>  
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>      
            </div>
        </div>
		<!--end::Portlet-->
    </div>
    <!-- end:: Content -->
    <!-- end:: Content -->
    
</div>
<div class="modal fade" id="detail_development" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detail_development_title">Detail Course Development</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body" id="detail_development_body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('templates/Footer.php');
?>
<script src="<?php echo base_url(); ?>assets/vendors/custom/datatables/datatables.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/vendors/custom/datatables/dataTables.rowsGroup.js" type="text/javascript"></script>
<script>
    function initDTEvents(){

        $(".btn_view").on("click",function(){
            var targetId = $(this).data("id");
            if (targetId != ""){
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/get_detail_development"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            $("#detail_development_body").empty();
                            $("#detail_development_body").append(data.body);
                            $("#detail_development").modal("show");
                        }else{
                            swal.fire({
                                title: 'Error',
                                text: data.message,
                                type: 'error',
                            });
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        swal.fire({
                            title: 'Error',
                            text: 'Cannot communicate with server please check your internet connection',
                            type: 'error',
                        });
                    }
                });
            }
        });
    }
    
    function oneDigit(n){
        if(n<10){
            n = "0"+n;
        }
        return n;
    }

    function ConvertSectoDay(n) { 
        var day;
        var hour;
        var minutes;
        var seconds;

        day = Math.floor(n / (24 * 3600)); 
  
        n = (n % (24 * 3600)); 
        hour = Math.floor(n / 3600); 

        n %= 3600; 
        minutes = n / 60 ; 
    
        n %= 60; 
        seconds = n;

        hour = oneDigit(hour);
        minutes = oneDigit(minutes);
        seconds = oneDigit(seconds);

        if(day == 0){
            day = "";
        } else if(day == 1){
            day += " day ";
        } else if(day > 1){
            day += " days "
        }
        
        return (day + hour + ":" + minutes + ":" + seconds); 
            
    } 
    var reportPerson = function () {
        // Private functions
        var reportDt = false;
        var currentFilter = false;
        var personPieChart = false;
        var personBarChart = false;

        var options = {
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: "<?php echo base_url($controller_full_path."/datatable_person"); ?>",
                        params:{
                            month : $("#person_month_filter").val(),
                            year : $("#person_year_filter").val(),
                            user : $("#person_person_filter").val(),
                            project : $("#person_project_filter").val(),
                        }
                    }
                },
                pageSize: 10,
                serverSide: true,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                timeout: 30000,
                saveState:
                    {
                        cookie: false,
                        webstorage: false
                    }
            },

            layout: {
                scroll: true, 
                height: 750, 
                footer: false 
            },
            // column sorting
            sortable: true,
            pagination: true,
            // columns definition
            columns: [{
                    field: 'fullname',
                    title: 'Fullname',
                    sortable: 'desc',
                    width : 200,
                },{
                    field: 'hours',
                    title: 'Hours',
                    sortable: 'desc',
                    width : 200,
                }
            ],
        };
        var refreshParamInput = function(){
            currentFilter = {
                                month : $("#person_month_filter").val(),
                                year : $("#person_year_filter").val(),
                                user : $("#person_person_filter").val(),
                                project : $("#person_project_filter").val(),
                            }
            if(reportDt){
                var currentData = reportDt.getOption("data");
                currentData.source.read.params = currentFilter;
            }
        }
        var initDt = function () {
            options.search = {
                input: $('#generalSearch'),
                delay: 100
            };

            reportDt = $('#datatable-person').KTDatatable(options);
            reportDt.on("kt-datatable--on-layout-updated",function(){
                initDTEvents();
            })
            $("#person_apply_filter").on("click", function () {
                fetchStatistics();
                reportDt.reload();
            });

        };

        var formatY = function(y){
            var hour = Math.floor(y / 3600); 

            y %= 3600; 
            var minutes = Math.floor(y / 60) ; 
        
            y %= 60; 
            var seconds = y;

            hour = oneDigit(hour);
            minutes = oneDigit(minutes);
            seconds = oneDigit(seconds);

            return (hour +":"+ minutes +":"+ seconds);
        }

        var initChart = function() {
            $("#nav-item-person").click(function(){
                fetchStatistics();
            })
            if ($('#person_hours_pie_chart').length == 0) {
                return;
            }

            personPieChart = Morris.Donut({
                element: 'person_hours_pie_chart',
                data: [{}],
                colors: [
                    KTApp.getStateColor('success'),
                    KTApp.getStateColor('danger'),
                    KTApp.getStateColor('primary'),
                    KTApp.getStateColor('warning'),
                    KTApp.getStateColor('info')
                ],
                formatter: function (value) {return (ConvertSectoDay(value))},
            
            });
        

            if ($('#person_hours_bar_chart').length == 0) {
                    return;
            }

            config = {
                  data: [],
                  xkey: 'y',
                  ykeys: ['a'],
                  labels: ['Hours'],
                  fillOpacity: 0.6,
                  hideHover: 'auto',
                  behaveLikeLine: true,
                  resize: true,
                  xLabelMargin:2,
                  padding:60,
                  pointFillColors:['#ffffff'],
                  pointStrokeColors: ['black'],
                  barColors:[KTApp.getStateColor('danger')],
                  yLabelFormat:formatY,
                  axes:"y"
            };
            config.element = 'person_hours_bar_chart';
            personBarChart = Morris.Bar(config);
        }

        var initFilter = function(){
            $("#person_month_filter,#person_year_filter,#person_person_filter,#person_project_filter").on("change",function(){
                refreshParamInput();
            });
        }
        
        var renderStatistics = function(data){
            //clear table body
            $("#project_hour_table tr").remove(); 
            
            var table = document.getElementById("project_hour_table");
            var row;
            var col1;
            var col2;
            var i;
            
            var person = $("#person_person_filter").val();
            
            if(person != 0){ //fill table body with datas
                $("#person_statistic").show();
                for(i = 0; i<data.pie.length; i++){
                    row = table.insertRow(-1);
                    col1 = row.insertCell(0);
                    col2 = row.insertCell(1);
                        
                    col1.innerHTML = data.pie[i].label;
                    col2.innerHTML = formatY(data.pie[i].value);
                }
            } else {
                //hide Statistic section when user did not use filter project
                $("#person_statistic").hide();
            }
            
            var bgColor;
            personPieChart.setData(data.pie);
            $( "div" ).remove( ".legend" );
            for(i = 0; i < personPieChart.segments.length; i++) {
                    bgColor = " style = 'background-color:" + personPieChart.segments[i].color +"'";
                    label_text = data.pie[i].label + " : " +ConvertSectoDay(data.pie[i].value);
                    $(".legends_person").append("<div class='kt-widget14__legend legend'><span class='kt-widget14__bullet' "+bgColor+" ></span><span class='kt-widget14__stats'>"+label_text+"</span></div>");
            }    

            //set bar chart
            personBarChart.setData(data.bar);
            personBarChart.redraw();

            //set pie chart data
            // reviewedChart.setData(data);
            // for(i = 0; i < reviewedChart.segments.length; i++) {
            //     reviewedChart.segments[i].handlers['hover'].push( function(i){
            //         $('#person_hover_label').text(data[i].label);
            //         $('#person_hover_value').text(new Date(data[i].value * 1000).toISOString().substr(11, 8));
            //         $('#person_hover_color').css("background-color", reviewedChart.segments[i].color);
            //     });
            // }    
        
        }


        var fetchStatistics = function(){
            KTApp.blockPage();
            $.ajax({
                type : 'GET',
                url : "<?php echo base_url($controller_full_path."/statistic_person"); ?>",
                dataType : "json",
                data : currentFilter,
                success : function(response,status){
                    KTApp.unblockPage();
                    if(response.status == true){
                        renderStatistics(response.data);
                    }else{
                        renderStatistics(false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    KTApp.unblockPage();
                    renderStatistics(false);
                }
            });  
        }


        return {
            init: function () {
                initDt();
                initDTEvents();
                initChart();
                initFilter();
                refreshParamInput();
                fetchStatistics();
            },

        };
    }();



    var reportProject = function () {
        // Private functions
        var reportDt = false;
        var currentFilter = false;
        var projectPieChart = false;
        var projectBarChart = false;

        var options = {
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: "<?php echo base_url($controller_full_path."/datatable_project"); ?>",
                        params:{
                            month : $("#project_month_filter").val(),
                            year : $("#project_year_filter").val(),
                            project : $("#project_project_filter").val(),
                            user : $("#project_person_filter").val(),
                        }
                    }
                },
                pageSize: 10,
                serverSide: true,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                timeout: 30000,
                saveState:
                    {
                        cookie: false,
                        webstorage: false
                    }
            },

            layout: {
                scroll: true, 
                height: 750, 
                footer: false 
            },
            // column sorting
            sortable: true,
            pagination: true,
            // columns definition
            columns: [{
                    field: 'project_name',
                    title: 'Project Name',
                    sortable: 'desc',
                    width : 200,
                },{
                    field: 'hours',
                    title: 'Hours',
                    sortable: 'desc',
                    width : 200,
                }
            ],
        };
        var refreshParamInput = function(){
            currentFilter = {
                                month : $("#project_month_filter").val(),
                                year : $("#project_year_filter").val(),
                                project : $("#project_project_filter").val(),
                                user : $("#project_person_filter").val(),
                            }
            if(reportDt){
                var currentData = reportDt.getOption("data");
                currentData.source.read.params = currentFilter;
            }
        }
        var initDt = function () {
            options.search = {
                input: $('#generalSearch'),
                delay: 100
            };

            reportDt = $('#datatable-project').KTDatatable(options);
            reportDt.on("kt-datatable--on-layout-updated",function(){
                initDTEvents();
            })

            $("#project_apply_filter").on("click", function () {
                fetchStatistics();
                reportDt.reload();
            });
        };

        //change y axis labels
        var formatY = function(y){
            var hour = Math.floor(y / 3600); 

            y %= 3600; 
            var minutes = Math.floor(y / 60) ; 
        
            y %= 60; 
            var seconds = y;

            hour = oneDigit(hour);
            minutes = oneDigit(minutes);
            seconds = oneDigit(seconds);

            return (hour +":"+ minutes +":"+ seconds);
        }

        var initChart = function() {
            $("#nav-item-project").click(function(){
                fetchStatistics();
            })
            if ($('#project_hours_pie_chart').length == 0) {
                return;
            }

            projectPieChart = Morris.Donut({
                element: 'project_hours_pie_chart',
                data: [{}],
                colors: [
                    KTApp.getStateColor('success'),
                    KTApp.getStateColor('danger'),
                    KTApp.getStateColor('primary'),
                    KTApp.getStateColor('warning'),
                    KTApp.getStateColor('info')
                ],
                formatter: function (value) {return (ConvertSectoDay(value))},
            });

            if ($('#project_hours_bar_chart').length == 0) {
                return;
            }

            config = {
                  data: [],
                  xkey: 'y',
                  ykeys: ['a'],
                  labels: ['Hours'],
                  fillOpacity: 0.6,
                  hideHover: 'auto',
                  behaveLikeLine: true,
                  resize: true,
                  xLabelMargin:2,
                  padding:60,
                  pointFillColors:['#ffffff'],
                  pointStrokeColors: ['black'],
                  barColors:[KTApp.getStateColor('danger')],
                  yLabelFormat:formatY,
                  axes:"y"
            };
            config.element = 'project_hours_bar_chart';
            projectBarChart = Morris.Bar(config);
        }

        var initFilter = function(){
            $("#project_month_filter,#project_year_filter,#project_project_filter,#project_person_filter ").on("change",function(){
                refreshParamInput();
            });
        }

        var renderStatistics = function(data){
            //clear table body
            $("#employees_hour_table tr").remove(); 
            
            var table = document.getElementById("employees_hour_table");
            var row;
            var col1;
            var col2;
            var i;
            
            var project = $("#project_project_filter").val();
            
            if(project != 0){ //fill table body with datas
                $("#project_statistic").show();
                for(i = 0; i<data.pie.length; i++){
                    row = table.insertRow(-1);
                    col1 = row.insertCell(0);
                    col2 = row.insertCell(1);
                        
                    col1.innerHTML = data.pie[i].label;
                    col2.innerHTML = formatY(data.pie[i].value);
                }
            } else {
                //hide Statistic section when user did not use filter project
                $("#project_statistic").hide();
            }
            
            //set pie chart and legend
            var bgColor;
            projectPieChart.setData(data.pie);
            $( "div" ).remove( ".legend_project" );
            for(i = 0; i < projectPieChart.segments.length; i++) {
                    bgColor = " style = 'background-color:" + projectPieChart.segments[i].color +"'";
                    label_text = data.pie[i].label + " : " +ConvertSectoDay(data.pie[i].value);
                    $(".legends_project").append("<div class='kt-widget14__legend legend_project'><span class='kt-widget14__bullet' "+bgColor+" ></span><span class='kt-widget14__stats'>"+label_text+"</span></div>");
            }    

            //set bar chart
            projectBarChart.setData(data.bar);
            projectBarChart.redraw();
        
        }

        var fetchStatistics = function(){
            KTApp.blockPage();
            $.ajax({
                type : 'GET',
                url : "<?php echo base_url($controller_full_path."/statistic_project"); ?>",
                dataType : "json",
                data : currentFilter,
                success : function(response,status){
                    KTApp.unblockPage();
                    if(response.status == true){
                        renderStatistics(response.data);
                    }else{
                        renderStatistics(false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    KTApp.unblockPage();
                    renderStatistics(false);
                }
            });  
        }


        return {
            init: function () {
                initDt();
                initDTEvents();
                initChart();
                initFilter();
                refreshParamInput();
                fetchStatistics();
            },

        };
    }();

    var mySubjects = [];
    var myStudyProgram = [];
    var myFaculty = [];
    var myPerson = [];

    function initFilters(){
        $(".month_filter").select2({
            placeholder: "All",
            width: '100%'
        });
        $(".year_filter").select2({
            placeholder: "All",
            width: '100%'
        });
        $(".project_filter").select2({
            placeholder: "All",
            width: '100%'
        });
        $(".person_filter").select2({
            placeholder: "All",
            width: '100%'
        });
    }

    function Export(type){
        <?php if($privilege->can_read && $clearance_level >= 1){?>
        if (type !== null){
            var prefix = "";
            var list = {"project":"Per_Project","person" : "Per_Employees"};
            prefix = list[type];
            if (prefix !== null){
                KTApp.blockPage();
                var currdate = new Date();
                var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
                var filename = "Summary_"+prefix+"_"+date+".csv";
                var filter = {};
                filter['type'] = type;
                $("#"+type+"_filter").find("select").each(function(){
                    filter[$(this).attr("id")] = $(this).val();
                });
                $.ajax({
                    url: "<?php echo base_url($controller_full_path."/export");?>",
                    type: 'post',
                    dataType: 'html',
                    data: JSON.stringify(filter),
                    success: function(data) {
                        KTApp.unblockPage();
                        var downloadLink = document.createElement("a");
                        var fileData = ['\ufeff'+data];
                        var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                        var url = URL.createObjectURL(blobObject);
                        downloadLink.href = url;
                        downloadLink.download = filename;
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);
                    }
                })
            }
            else {
                swal.fire({
                    title: 'Infomation',
                    text: "Unidentified export parameter",
                    type: 'info',
                });
            }
        <?php }else{ ?>
        swal.fire({
            title: 'Infomation',
            text: "You cannot export this data",
            type: 'info',
        });
        <?php } ?>
        }
    }

    $(document).ready(function(){
        initFilters();
        reportProject.init();
        reportPerson.init();
    });

</script>

