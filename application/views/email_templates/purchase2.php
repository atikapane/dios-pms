<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Enrollment Account</title>
    <style>
        body {
            font-family: "Montserrat";
            font-size:16px;
        }
        .email-area{
            background: rgba(0, 0, 0, .1);
            color: black;
            width: 100%;
            padding: 32px 0;
        }

        @media (min-width: 640px) {
            .card {
                width: 50%;
            }
        }

        @media (max-width: 639px) {
            .card {
                width: 90%;
            }
        }

        .card {
            
            margin: 0 auto;
            background-color: white;
            border: 1px solid rgba(0, 0, 0, .2);
        }

        .card-header{
            background:#f1f4f5;
            padding: 16px;
            border-bottom: 1px solid rgba(0, 0, 0, .2);
            margin:0px;
        }

        .card-header a{
            color:darkred;
            text-decoration: none;
            float:right;
            padding:10px;
            font-weight: normal;
        }

        .card-body{
            width: 100%;
            padding: 16px;
        }

        .card-title{
            font-weight: 700;
            color:darkred;
            width:90%;
        }
        .table {
            border-collapse: collapse;
            width: 100%;
        }
        .table tr td,.table tr th {
            padding:5px 5px;
            margin:0px;
            font-size:10px;
        }

        .table tr th {
            font-weight: bold;
            text-align: left;
        }
    </style>
</head>
<body>
    <div class="email-area">
        <div class="card">
            <h5 class="card-header">
                <img src="cid:<?= $cid ?>" width="100" alt="">
                <a href="https://celoe.telkomuniversity.ac.id/course/catalog">Browse Catalog</a>
            </h5>
            <div class="card-body">
                <h3 class="card-title">Thank you for your purchase on CeLOE MOOC</h3>
                <p class="card-text">Here's your course detail</p>
                <?php $x = 1 ?>
                <table class="table">
                <tr>
                    <th width=10px>#</th>
                    <th>Title</th>
                    <th>Schedule</th>
                </tr>
                <?php foreach($courses as $course): ?>

                    <tr>
                    <?php
                        if(is_array($course)){
                            echo '<td>'.$x++ . '.</td><td>' . $course['course_title'] . '</td><td>' . date('d M Y', strtotime($course['start_date'])) . ' - ' . date('d M Y', strtotime($course['end_date'])) . '</td>';
                        }else if(is_object($course)){
                            echo '<td>'.$x++ . '.</td><td>' . $course->course_title . '</td><td>' . date('d M Y', strtotime($course->start_date)) . ' - ' . date('d M Y', strtotime($course->end_date)) . '</td>';
                        }
                    ?>
                    </tr>
                <?php endforeach; ?>    
                </table>  
                <p class="card-text">Here's your Enrollment Account</p>
                <table class="table">
                    <tr>
                        <th width=40px>Username</th><td><?= $username ?>@365.telkomuniversity.ac.id</td> 
                    </tr>
                    <tr>
                        <th>Password</th><td>the same as your CeLOE account</td>
                    </tr>
                    <tr>
                        <th>Login URL</th><td><a href="https://onlinelearning.telkomuniversity.ac.id/" style="text-decoration:none;color:darkred;">Visit Website</a></td>
                    </tr>
               </table>
              
            </div>
        </div>
    </div>
</body>
</html>