<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Enrollment Account</title>
    <style>
        body {
            font-family: "Montserrat";
            font-size:16px;
        }
        .email-area{
            background: rgba(0, 0, 0, .1);
            color: black;
            width: 100%;
            padding: 32px 0;
        }

        @media (min-width: 640px) {
            .card {
                width: 50%;
            }
        }

        @media (max-width: 639px) {
            .card {
                width: 90%;
            }
        }

        .card {
            
            margin: 0 auto;
            background-color: white;
            border: 1px solid rgba(0, 0, 0, .2);
        }

        .card-header{
            background:#f1f4f5;
            padding: 16px;
            border-bottom: 1px solid rgba(0, 0, 0, .2);
            margin:0px;
        }

        .card-header a{
            color:darkred;
            text-decoration: none;
            float:right;
            padding:10px;
            font-weight: normal;
        }

        .card-body{
            width: 100%;
            padding: 16px;
        }

        .card-title{
            font-weight: 700;
            color:darkred;
            width:90%;
        }
        .table {
            border-collapse: collapse;
            width: 100%;
        }
        .table tr td,.table tr th {
            padding:5px 5px;
            margin:0px;
            font-size:10px;
        }

        .table tr th {
            font-weight: bold;
            text-align: left;
        }
    </style>
</head>
<body>
    <div class="email-area">
        <div class="card">
            <h5 class="card-header">
                <img src="cid:<?= $cid ?>" width="100" alt="">
                <a href="https://celoe.telkomuniversity.ac.id/course/catalog">Browse Catalog</a>
            </h5>
            <div class="card-body">
                <h3 class="card-title">Congratulations, you have finished <?= $course->ocw_name ?></h3>
                <p class="card-text">You can download your certificate <a href="<?= base_url('account/certificate') ?>">here</a></p>
            </div>
        </div>
    </div>
</body>
</html>