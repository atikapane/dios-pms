<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Enrollment Account</title>
    <style>
        .email-area{
            background: rgba(0, 0, 0, .1);
            color: black;
            width: 100%;
            padding: 32px 0;
        }

        .card {
            width: 50%;
            margin: 0 auto;
            background-color: white;
            border: 1px solid rgba(0, 0, 0, .2);
        }

        .card-header{
            padding: 0 16px 16px;
            border-bottom: 1px solid rgba(0, 0, 0, .2);
        }

        .card-body{
            width: 100%;
            padding: 16px;
        }

        .card-title{
            font-weight: 900;
        }
    </style>
</head>
<body>
    <div class="email-area">
        <div class="card">
            <h5 class="card-header"><img src="cid:<?= $cid ?>" width="100" alt=""></h5>
            <div class="card-body">
                <h3 class="card-title">You are now enrolled on CeLOE MOOC</h3>
                <p class="card-text">
                    Here's your course detail : <br>
                    <?php $x = 1 ?>
                    <?php foreach($courses as $course): ?>
                        <?php
                            if(is_array($course)){
                                echo $x++ . '. ' . $course['ocw_name'] . ' (' . date('d M Y', strtotime($course['start_date'])) . ' - ' . date('d M Y', strtotime($course['end_date'])) . ')';
                            }else if(is_object($course)){
                                echo $x++ . '. ' . $course->ocw_name . ' (' . date('d M Y', strtotime($course->start_date)) . ' - ' . date('d M Y', strtotime($course->end_date)) . ')';
                            }
                        ?>
                    <?php endforeach; ?>
                </p>
                <p class="card-text">
                    Here's your Enrollment Account : <br>
                    Username  : <?= $username ?>@365.telkomuniversity.ac.id <br>
                    Password   : the same as your CeLOE account <br>
                    Login URL : https://onlinelearning.telkomuniversity.ac.id/ <br>
                </p>
            </div>
        </div>
    </div>
</body>
</html>