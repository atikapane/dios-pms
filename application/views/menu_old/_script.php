<script>

 jQuery(document).ready(function() {	
    $('#menuicon').select2({
        placeholder: "-- Select a Icons --",
        width: '100%',
    });
    $('#menuicon-edit').select2({
        placeholder: "-- Select a Icons --",
        width: '100%',
    });

    $("#menuicon").change(function () {
        console.log($(this).val());
        var val = $(this).val();
        $("#icon_preview1").removeClass();
        $("#icon_preview1").addClass('fa');
        $("#icon_preview1").addClass(val);
        //alert(val);
    });
    $("#menuicon-edit").change(function () {
        console.log($(this).val());
        var val = $(this).val();
        $("#icon_preview").removeClass();
        $("#icon_preview").addClass('fa');
        $("#icon_preview").addClass(val);
        //alert(val);
    });

});

</script>