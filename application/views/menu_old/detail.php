<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">								
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title">Page</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc">#Sub Menu</span>           
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->					

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        Sub Menu List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">            
                <button type="button" class="btn btn-brand btn-icon-sm" data-toggle="modal" data-target="#add">
                    <i class="flaticon2-plus"></i> Add New  	
                </button>	
            </div>		
        </div>
    </div>

	<div class="kt-portlet__body kt-portlet__body--fit">
        <br/>
		<!--begin: Datatable -->		
		<table class="kt-datatable" id="" width="100%">
			<thead>
			<tr>
            <th title="Field #1">No</th>
				<th title="Field #2">Menu</th>				
				<th title="Field #2">Sub Menu</th>				
				<th title="Field #2">Sub Menu Link</th>				
				<!-- <th title="Field #2">Sub Menu Icon</th>-->
				<th title="Field #5">Action</th>				
				<th title="Field #5"></th>				
			</tr>
			</thead>
			<tbody>
                <?php 
                $no=0;                
                foreach($dt as $row)
                { $no++;
                    echo '<tr>
                    <td>'.$no.'</td>
                    <td>'.$row->menu_name.'</td>
                    <td>'.$row->submenu_name.'</td>
                    <td>'.$row->submenu_link.'</td>                    
                    <td>';
                    echo '<a href="#" data-id="'.$row->submenu_id.'" data-menuid="'.$row->menuid.'" data-menu="'.$row->submenu_name.'" data-link="'.$row->submenu_link.'" class="btn btn-sm btn-clean btn-icon btn-icon-md open-edit" title="Edit" data-toggle="modal" data-target="#edit"><i class="flaticon-edit-1" style="cursor:pointer;"></i></a>';
                    echo '<a href="'.base_url('menu/deletesub/'.$row->menu_id.'/'.$row->submenu_id).'" class="btn btn-sm btn-clean btn-icon btn-icon-md editBtn" title="Edit" onclick="return deletechecked();"><i class="flaticon2-trash" style="cursor:pointer;"></i></a>';
                    echo '</td>					
                    <td></td>					
                </tr>';
                }			
			?>
			</tbody>
		</table>
		<!--end: Datatable -->
	</div>
</div>
    </div>
    <!-- end:: Content -->
    <!-- end:: Content -->
    
</div>

<?php
$this->load->view('templates/Footer.php');
?>

<!--begin::Modal-->
<form name="fmadd" class="form-horizontal" method="post" action="<?php echo base_url('menu/savesubmenu'); ?>">
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group">
						<label>Menu Name</label>
						<input type="text" class="form-control" id="submenu" name="submenu" placeholder="" required>
						<input type="hidden" class="form-control" id="menuid" name="menuid" value="<?php echo $this->uri->segment(3); ?>">
					</div>
                </div>

                 <div class="col-md-6">
                    <div class="form-group">
						<label>Menu Link</label>
						<input type="text" class="form-control" id="submenulink" name="submenulink" placeholder="" required>
					</div>
                </div> 

                <!-- <div class="col-md-6">
                    <div class="form-group">
						<label>Menu icon</label>
						<select class="form-control" name="submenuicon" id="submenuicon">
                        <option><option>
                        <?php 
                        // foreach($icons as $row){
                        //     echo '<option value="'.$row->Id.'">'.$row->Name.'<option>';
                        // }                        
                        ?>
                        </select>
					</div>
                </div>                     -->
                
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<!--end::Modal-->

<!--begin::Modal-->
<form name="fmedit" class="form-horizontal" method="post" action="<?php echo base_url('menu/savesubmenu'); ?>">
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group">
						<label>Menu</label>
						<input type="text" class="form-control" id="submenu" name="submenu" placeholder="" required>
						<input type="hidden" class="form-control" id="submenuid" name="submenuid">
						<input type="hidden" class="form-control" id="menuid" name="menuid">
					</div>
                </div> 

                <div class="col-md-6">
                    <div class="form-group">
						<label>Menu Link</label>
						<input type="text" class="form-control" id="submenulink" name="submenulink" placeholder="" required>
					</div>
                </div> 

                <!-- <div class="col-md-6">
                    <div class="form-group">
						<label>Menu icon</label>
						<select class="form-control" name="submenuicon-edit" id="submenuicon-edit">
                        <option><option>
                        <?php 
                        // foreach($icons as $row){
                        //     echo '<option value="'.$row->Id.'">'.$row->Name.'<option>';
                        // }                        
                        ?>
                        </select>
					</div>
                </div>                       -->
                
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<!--end::Modal-->

<script type="text/javascript">
 function deletechecked()
    {
        if(confirm(" Are you sure delete this data?"))
        {
            return true;
        }
        else
        {
            return false;  
        } 
   }  
</script>

<script type="text/javascript">
  $(document).on("click",".open-edit",function(){
      var id          = $(this).data("id");
      var menuid          = $(this).data("menuid");       
      var menu          = $(this).data("menu");       
      var link          = $(this).data("link");       
      //var icon          = $(this).data("icn");       
      //alert(icon); 
      $(".modal-body #submenuid").val(id);
      $(".modal-body #menuid").val(menuid);                  
      $(".modal-body #submenu").val(menu);                  
      $(".modal-body #submenulink").val(link);                  
      //$(".modal-body #submenuicon-edit").val(icon);                  
  });
</script>

<script>
var KTDatatableHtmlTableDemo = function() {
	var tbl = function() {
		var datatable = $('.kt-datatable').KTDatatable({ });};
	return { init: function() { tbl();},};
}();

jQuery(document).ready(function() {	
    $('#submenuicon').select2({
        placeholder: "-- Select a Icons --",
        width: '100%',
    });
    // $('#submenuicon-edit').select2({
    //     placeholder: "-- Select a Icons --",
    //     width: '100%',
    // });
    KTDatatableHtmlTableDemo.init();});
</script>