<!--begin::Modal-->
<form name="fmadd" class="form-horizontal" method="post" action="<?php echo base_url('menu/save'); ?>">
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group">
						<label>Menu Name</label>
						<input type="text" class="form-control" id="menu" name="menu" placeholder="" required>
					</div>
                </div>

                 <div class="col-md-6">
                    <div class="form-group">
						<label>Menu Link</label>
						<input type="text" class="form-control" id="menulink" name="menulink" placeholder="" required>
					</div>
                </div> 

                <div class="col-md-6">
                    <div class="form-group">
						<label>Menu icon <i id="icon_preview1" class="fa"></i></label>
						<select class="form-control select2" name="menuicon" id="menuicon">
                        <option><option>
                        <?php 
                        foreach($icons as $row){
                            echo '<option value="'.$row->Name.'">'.$row->Name.'<option>';
                        }                        
                        ?>
                        
                        </select>
					</div>
                </div>                    
                
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<!--end::Modal-->