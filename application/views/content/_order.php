<!--begin::Modal-->
<div class="modal fade" id="modal_order" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Re-order <?php echo $breadcrumb->child; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled list-group">
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <p class="my-3">Drag and drop items above to reorder home page content</p>
                    </div>
                    <div class="col-md-12" >     
                        <div class="kt-section">
                            <div class="kt-section__content">
                                    <div class="alert alert-danger fade show" role="alert" id="failed_alert_order" style="display: none;">
                                    <div class="alert-text" id="failed_message_order"></div>
                                    <div class="alert-close">
                                        <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_order">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>                   
                            </div>                   
                        </div>            
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_order_submit">Submit</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->