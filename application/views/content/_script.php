<script src="<?php echo base_url(); ?>assets/vendors/general/jquery/dist/jquery-ui.js" type="text/javascript"></script>
<script>
    //globals
    var datatable;
    var AlertUtil;
    var createForm;
    var editForm;
    var orderList

    function initDTEvents() {
        $(".btn_delete").on("click", function () {
            var targetId = $(this).data("id");
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it'
            }).then(function (result) {
                if (result.value) {
                    KTApp.blockPage();
                    $.ajax({
                        type: 'GET',
                        url: "<?php echo base_url($controller_full_path . "/delete"); ?>",
                        data: {id: targetId},
                        dataType: "json",
                        success: function (data, status) {
                            KTApp.unblockPage();
                            if (data.status == true) {
                                datatable.reload();
                                AlertUtil.showSuccess(data.message, 5000);
                            } else {
                                AlertUtil.showFailed(data.message);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            KTApp.unblockPage();
                            AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                        }
                    });
                }
            });
        });

        $(".btn_edit").on("click", function () {
            var targetId = $(this).data("id");
            KTApp.blockPage();
            $.ajax({
                type: 'GET',
                url: "<?php echo base_url($controller_full_path . "/get"); ?>",
                data: {id: targetId},
                dataType: "json",
                success: function (response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        editForm.populateForm(response.data);
                        response.data.content == null ? $('#modal_edit .content').hide() : $('#modal_edit .content').show()
                        $('#modal_edit').modal('show');
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            });
        });

        $("#btn_order").on('click', function () {
            KTApp.blockPage()
            $.ajax({
                type: 'GET',
                url: "<?php echo base_url($controller_full_path . "/get_order"); ?>",
                dataType: 'json',
                success: function (response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        orderList.populateForm(response.data);
                        $('#modal_order').modal('show');
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            })
        })
    }

function Export(){
<?php if($privilege->can_read){?>
    var currdate = new Date();
    var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
    var filename = "Content_"+date+".csv";
    var search = $('#generalSearch').val();
    $.ajax({
        url: "<?php echo base_url($controller_full_path."/export");?>",
        type: 'post',
        dataType: 'html',
        data: { search: search  },
        success: function(data) {
            var downloadLink = document.createElement("a");
            var fileData = ['\ufeff'+data];
            var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
            var url = URL.createObjectURL(blobObject);
            downloadLink.href = url;
            downloadLink.download = filename;
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
    })    
<?php }else{ ?>
    swal.fire({
            title: 'Infomation',
            text: "You cannot export this data",
            type: 'info',
        });
<?php } ?>
}

function Print(){
    <?php if($privilege->can_read){?>
        var search = $('#generalSearch').val();
        KTApp.blockPage();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/print_content");?>",
            type: 'post',
            dataType: 'json',
            data: { search: search  },
            success: function (response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        var win = window.open("", "_blank");                    
                        win.document.write(response.data);
                        win.document.close();  
                        setTimeout(function() {
                            win.print();
                            win.close();
                        }, 250);
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot Print this data",
                type: 'info',
            });
    <?php } ?>
}

    function initDataTable() {
        var option = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '<?php echo base_url($controller_full_path . "/datatable"); ?>',
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {cookie: false, webstorage: false},
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            columns: [
                {
                    field: 'id',
                    title: '#',
                    width: 40,
                    sortable: 'asc',
                    textAlign: 'center',
                },
                {
                    field: 'title',
                    title: 'Title',
                    textAlign: 'left'
                },
                {
                    field: 'priority',
                    title: 'Priority',
                    textAlign: 'center'
                },
                {
                    field: 'created_date',
                    title: 'Created Date',
                    textAlign: 'left',
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false,
                    width: 80,
                    overflow: 'visible',
                    textAlign: 'left',
                    autoHide: false,
                    template: function (row) {
                        var result = "";
                        <?php if($privilege->can_update){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" ><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                        <?php }?>
                        <?php if($privilege->can_delete){?>
                        if (row.content != null)
                            result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" ><i class="flaticon2-trash" style="cursor:pointer;"></i></span>';
                        <?php }?>
                        return result;
                    }
                }
            ],
            layout: {
                header: true
            }
        }
        datatable = $('#kt_datatable').KTDatatable(option);
        datatable.on("kt-datatable--on-layout-updated", function () {
            initDTEvents();
        })
    }

    var KTSummernoteDemo = function () {
        // Private functions
        var demos = function () {
            $('.summernote').summernote({
                height: 200,
                dialogsInBody: true
            });
        };

        return {
            // public functions
            init: function () {
                demos();
            }
        };
    }();

    function initCustomRule() {
    }

    function initAlert() {
        AlertUtil = {
            showSuccess: function (message, timeout) {
                $("#success_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#success_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess: function () {
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed: function (message, timeout) {
                $("#failed_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideFailed: function () {
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd: function (message, timeout) {
                $("#failed_message_add").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss_add").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_add").show();
            },
            hideSuccessDialogAdd: function () {
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit: function (message, timeout) {
                $("#failed_message_edit").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss_edit").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_edit").show();
            },
            hideSuccessDialogAdd: function () {
                $("#failed_alert_dismiss_edit").trigger("click");
            },
            showFailedDialogOrder : function(message,timeout){
                $("#failed_message_order").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_order").trigger("click");
                    },timeout)
                }
                $("#failed_alert_order").show();
            },
            hideSuccessDialogOrder : function(){
                $("#failed_alert_dismiss_order").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click", function () {
            $("#failed_alert").hide();
        })
        $("#failed_alert_dismiss_order").on("click",function(){
            $("#failed_alert_order").hide();
        })
        $("#success_alert_dismiss").on("click", function () {
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click", function () {
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click", function () {
            $("#failed_alert_edit").hide();
        })
    }

    function initCreateForm() {
        // summernote
        var summernoteElement = $('.summernote');

        //validator
        var validator = $("#form_add").validate({
            ignore: ':hidden:not(.summernote),.note-editable.card-block',
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("invalid-feedback");
                element.addClass("is-invalid");
                if (element.hasClass("summernote")) {
                    error.insertAfter(element.siblings(".note-editor"));
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                title: {
                    required: true
                },
                content: {
                    required: true,
                }
            },
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            }
        });

        summernoteElement.summernote({
            dialogsInBody: true,
            callbacks: {
                onChange: function (contents, $editable) {
                    // Note that at this point, the value of the `textarea` is not the same as the one
                    // you entered into the summernote editor, so you have to set it yourself to make
                    // the validation consistent and in sync with the value.
                    summernoteElement.val(summernoteElement.summernote('isEmpty') ? "" : contents);

                    // You should re-validate your element after change, because the plugin will have
                    // no way to know that the value of your `textarea` has been changed if the change
                    // was done programmatically.
                    validator.element(summernoteElement);
                }
            }
        });

        //events
        $("#btn_add_submit").on("click", function () {
            var isValid = $("#form_add").valid();
            if (isValid) {
                KTApp.block('#modal_add .modal-content', {});
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url($controller_full_path . "/save"); ?>",
                    data: $('#form_add').serialize(),
                    dataType: "json",
                    success: function (data, status) {
                        KTApp.unblock('#modal_add .modal-content');
                        if (data.status == true) {
                            datatable.reload();
                            $('#modal_add').modal('hide');
                            AlertUtil.showSuccess(data.message, 5000);
                        } else {
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        KTApp.unblock('#modal_add .modal-content');
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        })

        $('#modal_add').on('hidden.bs.modal', function () {
            validator.resetForm();
            $("#add_content").summernote('code', '');
        })

        return {
            validator: validator
        }
    }

    function initOrderList() {
        //events
        $("#btn_order_submit").on("click", function () {
            KTApp.block('#modal_order .modal-content', {});
            let orderArr = []
            $(".list-group-item").each(function () {
                orderArr.push($(this).data('id'))
            })
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url($controller_full_path . "/save_order"); ?>",
                data: {order_array: orderArr},
                dataType: "json",
                success: function (data, status) {
                    KTApp.unblock('#modal_order .modal-content');
                    if (data.status == true) {
                        datatable.reload();
                        $('#modal_order').modal('hide');
                        AlertUtil.showSuccess(data.message, 5000);
                    } else {
                        AlertUtil.showFailedDialogOrder(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblock('#modal_order .modal-content');
                    AlertUtil.showFailedDialogOrder("Cannot communicate with server please check your internet connection");
                }
            });
        })

        $('#modal_order').on('hidden.bs.modal', function () {
            $(".list-group-item").remove()
        })

        var populateForm = function (orderObject) {
            list = ''
            $.each(orderObject, function () {
                list = list + `<li class="list-group-item" data-id="${this.id}">${this.title}</li>`
            })
            listOrder = $(".list-group")
            listOrder.html(list)
            listOrder.sortable({
                placeholder: "list-group-item list-group-item-dark",
                axis: 'y',
            })
        }

        return {
            populateForm: populateForm
        }
    }

    function initEditForm() {
        var summernoteElement = $('.summernote');

        //validator
        var validator = $("#form_edit").validate({
            ignore: ':hidden:not(.summernote),.note-editable.card-block',
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("invalid-feedback");
                element.addClass('is-invalid')
                if (element.hasClass("summernote")) {
                    error.insertAfter(element.siblings(".note-editor"));
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                id: {
                    required: true
                },
                title: {
                    required: true
                },
                content: {
                    required: true,
                }
            },
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
            }
        });

        summernoteElement.summernote({
            dialogsInBody: true,
            callbacks: {
                onChange: function (contents, $editable) {
                    // Note that at this point, the value of the `textarea` is not the same as the one
                    // you entered into the summernote editor, so you have to set it yourself to make
                    // the validation consistent and in sync with the value.
                    summernoteElement.val(summernoteElement.summernote('isEmpty') ? "" : contents);

                    // You should re-validate your element after change, because the plugin will have
                    // no way to know that the value of your `textarea` has been changed if the change
                    // was done programmatically.
                    validator.element(summernoteElement);
                }
            }
        });

        //events
        $("#btn_edit_submit").on("click", function () {
            var isValid = $("#form_edit").valid();
            if (isValid) {
                KTApp.block('#modal_edit .modal-content', {});
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url($controller_full_path . "/edit"); ?>",
                    data: $('#form_edit').serialize(),
                    dataType: "json",
                    success: function (data, status) {
                        KTApp.unblock('#modal_edit .modal-content');
                        if (data.status == true) {
                            datatable.reload();
                            $('#modal_edit').modal('hide');
                            AlertUtil.showSuccess(data.message, 5000);
                        } else {
                            AlertUtil.showFailedDialogEdit(data.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        KTApp.unblock('#modal_edit .modal-content');
                        AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        })

        $('#modal_edit').on('hidden.bs.modal', function () {
            validator.resetForm();
            $("#edit_content").summernote('code', '');
        })

        var populateForm = function (contentObject) {
            $("#edit_id").val(contentObject.id);
            $("#edit_title").val(contentObject.title);
            $("#edit_content").summernote('code', contentObject.content);
        }

        return {
            validator: validator,
            populateForm: populateForm
        }
    }

    jQuery(document).ready(function () {
        initDataTable();
        initCustomRule();
        KTSummernoteDemo.init();
        createForm = initCreateForm();
        editForm = initEditForm();
        orderList = initOrderList()
        initAlert();
    });
</script>