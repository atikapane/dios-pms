<!--begin::Modal-->
<div class="modal fade" id="modal_add_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-remove"></span>
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_add">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="email" class="form-control" placeholder="Enter Title" id="add_email" name="add_email" required>
                    </div>
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" class="form-control" placeholder="Enter Name" id="add_full_name" name="add_full_name" required>
                    </div>
                    <div class="form-group">
                        <label>Public Username:</label>
                        <input type="text" class="form-control" placeholder="Enter Title" id="add_username" name="add_username" required>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input type="password" class="form-control" placeholder="Enter Password" id="add_password" name="add_password" required>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label>Country:</label>-->
<!--                        <select class="form-control select2" name="add_country" id="add_country">-->
<!--                            <option></option>-->
<!--                            --><?php
//                            foreach ($countries as $country) {
//                                echo '<option value="' . $country->id . '">' . $country->name . '</option>';
//                            }
//                            ?>
<!--                        </select>-->
<!--                    </div>-->
                    <div class="form-group">
                        <label>City:</label>
                        <select class="form-control select2" name="add_city" id="add_city">
                            <option></option>
                            <?php
                            foreach ($cities as $city) {
                                echo '<option value="' . $city->id . '">' . $city->name . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Gender:</label>
                        <select class="form-control select2" name="add_gender" id="add_gender">
                            <option></option>
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                            <option value="99">Other/Prefer Not To Say</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Date Birth:</label>
                        <input type="text" class="form-control date-picker" placeholder="Enter Birth Date" id="add_date_birth" name="add_date_birth">
                    </div>
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="alert alert-danger fade show" role="alert" id="failed_alert_add" style="display: none;">
                                <div class="alert-text" id="failed_message_add"></div>
                                <div class="alert-close">
                                    <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_add">
                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_add_submit">Submit</button>
            </div>
        </div>
    </div>
</div>

<!--end::Modal-->