<script>
    var datatable;
    var AlertUtil;
    var createForm;
    var editForm;
    function initDTEvents(){
        $(".btn_delete").on("click",function(){
            var targetId = $(this).data("id");
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it'
            }).then(function(result) {
                if (result.value) {
                    KTApp.blockPage();
                    $.ajax({
                        type : 'GET',
                        url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                        data : {
                            id:targetId
                        },
                        dataType : "json",
                        success : function(data,status){
                            KTApp.unblockPage();
                            if(data.status == true){
                                datatable.reload();
                                AlertUtil.showSuccess(data.message,5000);
                            }else{
                                AlertUtil.showFailed(data.message);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            KTApp.unblockPage();
                            AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                        }
                    });
                }
            });
        });

        $(".btn_edit").on("click",function(){
            var targetId = $(this).data("id");
            KTApp.blockPage();
            $.ajax({
                type : 'GET',
                url : "<?php echo base_url($controller_full_path."/get"); ?>",
                data : {
                    id:targetId
                },
                dataType : "json",
                success : function(response){
                    KTApp.unblockPage();
                    if(response.status == true){
                        //populate form
                        editForm.populateForm(response.data);
                        $('#modal_edit_user').modal('show');
                    }else{
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            });
        });
    }
    function initDataTable(){
        var option = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState : {cookie: false,webstorage: false},
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            columns: [
                {
                    field: 'id',
                    title: '#',
                    width:40,
                    sortable: 'asc',
                    textAlign: 'center',
                },
                {
                    field: 'email',
                    title: 'Email',
                    sortable: 'asc',
                },
                {
                    field: 'full_name',
                    title: 'Name',
                    sortable: 'asc',
                },
                {
                    field: 'username',
                    title: 'Username',
                    sortable: 'asc',
                },
                // {
                //     field: 'country_id',
                //     title: 'Country',
                //     sortable: 'asc',
                // },
                {
                    field: 'city_id',
                    title: 'City',
                },
                {
                    field: 'gender',
                    title: 'Gender',
                    sortable: 'asc',
                },
                {
                    field: 'dateBirth',
                    title: 'Date of Birth',
                    sortable: 'asc',
                },
                {
                    field: 'oauth_provider',
                    title: 'Oauth Provider',
                    sortable: 'asc',
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false,
                    width: 150,
                    overflow: 'visible',
                    textAlign: 'left',
                    autoHide: false,
                    template: function (row) {
                        var result ="";
                        <?php if($privilege->can_update){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" ><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                        <?php }?>
                        <?php if($privilege->can_delete){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" ><i class="flaticon2-trash" style="cursor:pointer;"></i></span>';
                        <?php }?>
                        return result;
                    }
                }
            ],
            layout:{
                header:true
            }
        }
        datatable = $('#kt_datatable').KTDatatable(option);
        datatable.on("kt-datatable--on-layout-updated",function(){
            initDTEvents();
        })
    }
    function initCustomRule(){
    }
    function initAlert(){
        AlertUtil = {
            showSuccess : function(message,timeout){
                $("#success_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#success_alert_dismiss").trigger("click");
                    },timeout);
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess : function(){
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed : function(message,timeout){
                $("#failed_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss").trigger("click");
                    },timeout);
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideFailed : function(){
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd : function(message,timeout){
                $("#failed_message_add").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_add").trigger("click");
                    },timeout)
                }
                $("#failed_alert_add").show();
            },
            hideSuccessDialogAdd : function(){
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit : function(message,timeout){
                $("#failed_message_edit").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_edit").trigger("click");
                    },timeout)
                }
                $("#failed_alert_edit").show();
            },
            hideSuccessDialogAdd : function(){
                $("#failed_alert_dismiss_edit").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click",function(){
            $("#failed_alert").hide();
        })
        $("#success_alert_dismiss").on("click",function(){
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click",function(){
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click",function(){
            $("#failed_alert_edit").hide();
        })
    }
    function initCreateForm(){

        $('#add_gender').select2({
            placeholder: "Please select a gender",
            width: '100%'
        });

        // $('#add_country').select2({
        //     placeholder: "Please select a country",
        //     width: '100%'
        // });

        $('#add_city').select2({
            placeholder: "Please select a city",
            width: '100%'
        });

        $("#add_date_birth").datepicker({
            format:"yyyy-mm-dd",
            autoclose:true
        });

        var validator = $("#form_add").validate({
            errorPlacement: function (error, element) {
                error.addClass("invalid-feedback");
                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.siblings("label"));
                } else if (element.hasClass("summernote-add")) {
                    error.insertAfter(element.siblings(".note-editor"));
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                add_email: {
                    required: true
                },
                add_full_name: {
                    required: true
                },
                add_username: {
                    required: true
                },
                add_password: {
                    required: true,
                    minlength:8
                }
            },
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();
            }
        });

        $('#modal_add_user').on('hidden.bs.modal', function () {
            validator.resetForm();

            // $('#add_country').val([]).trigger('change');

            $('#add_city').val([]).trigger('change');

            $('#add_gender').val([]).trigger('change');

            $("#failed_alert_dismiss_add").trigger('click');
        });

        $("#btn_add_submit").on("click", function () {
            var isValid = $( "#form_add" ).valid();
            if(isValid){
                KTApp.block('#modal_add_user .modal-content', {});
                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url($controller_full_path."/save"); ?>",
                    data: $("#form_add").serialize(),
                    dataType:"json",
                    success:function (data) {
                        KTApp.unblock('#modal_add_user .modal-content');
                        if(data.status == true){
                            datatable.reload();
                            $('#modal_add_user').modal('hide');
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },error:function () {
                        KTApp.unblock('#modal_add_user .modal-content');
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        return {
            validator:validator
        }
    }
    function initEditForm(){

        $('#edit_gender').select2({
            placeholder: "Please select a gender",
            width: '100%'
        });

        // $('#edit_country').select2({
        //     placeholder: "Please select a country",
        //     width: '100%'
        // });

        $('#edit_city').select2({
            placeholder: "Please select a city",
            width: '100%'
        });

        $("#edit_date_birth").datepicker({
            format:"yyyy-mm-dd",
            autoclose:true
        });

        var validator = $("#form_edit").validate({
            errorPlacement: function (error, element) {
                error.addClass("invalid-feedback");
                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.siblings("label"));
                } else if (element.hasClass("summernote-add")) {
                    error.insertAfter(element.siblings(".note-editor"));
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                edit_email: {
                    required: true
                },
                edit_full_name: {
                    required: true
                },
                edit_username: {
                    required: true
                },
                edit_password: {
                    minlength:8
                }
            },
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();
            }
        });

        //events
        $("#btn_edit_submit").on("click",function(){
            var isValid = $( "#form_edit" ).valid();
            if(isValid){
                KTApp.block('#modal_edit_user .modal-content', {});
                $.ajax({
                    type : 'POST',
                    url : "<?php echo base_url($controller_full_path."/edit"); ?>",
                    data : $('#form_edit').serialize(),
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblock('#modal_edit_user .modal-content');
                        if(data.status == true){
                            datatable.reload();
                            $('#modal_edit_user').modal('hide');
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailedDialogEdit(data.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblock('#modal_edit_user .modal-content');
                        AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        })

        $('#modal_edit_user').on('hidden.bs.modal', function () {
            validator.resetForm();

            $('#edit_id').val("");

            // $('#edit_country').val([]).trigger('change');

            $('#edit_city').val([]).trigger('change');

            $('#edit_gender').val([]).trigger('change');

            $("#failed_alert_dismiss_edit").trigger('click');
        });

        var populateForm = function(object){
            $("#edit_id").val(object.id);
            $("#edit_full_name").val(object.full_name);
            $("#edit_email").val(object.email);
            $("#edit_username").val(object.username);
            // $('#edit_country').val(object.country_id).trigger('change');
            $('#edit_city').val(object.city_id).trigger('change');
            $('#edit_gender').val(object.gender).trigger('change');
            $("#edit_date_birth").val(object.dateBirth);
        };

        return {
            validator:validator,
            populateForm:populateForm
        }
    }

    function Export(){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "OCW_user_"+date+".csv";
        var search = $('#generalSearch').val();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: {
                query : $("#generalSearch").val()
            },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }

    function Print(){
        <?php if($privilege->can_read){?>
            KTApp.blockPage();
            $.ajax({
                url: "<?php echo base_url($controller_full_path."/print");?>",
                type: 'post',
                dataType: 'json',
                data: {
                    query : $("#generalSearch").val()
                },
                success: function (response, status) {
                        KTApp.unblockPage();
                        if (response.status == true) {
                            //populate form
                            var win = window.open("", "_blank");                    
                            win.document.write(response.data);
                            win.document.close();  
                            win.print();
                        } else {
                            AlertUtil.showFailed(response.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
            })    
        <?php }else{ ?>
            swal.fire({
                    title: 'Infomation',
                    text: "You cannot Print this data",
                    type: 'info',
                });
        <?php } ?>
    }

    jQuery(document).ready(function() {
        initDataTable();
        initCustomRule();
        createForm = initCreateForm();
        editForm = initEditForm();
        initAlert();
    });
</script>