<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Id</b></th>
            <th scope="col"><b>Email</b></th>
            <th scope="col"><b>Name</b></th>
            <th scope="col"><b>Username</b></th>
            <th scope="col"><b>City</b></th>
            <th scope="col"><b>Gender</b></th>
            <th scope="col"><b>Date of Birth</b></th>
            <th scope="col"><b>Oauth Provider</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item['id'].'</td>
            <td>'.$item['email'].'</td>
            <td>'.$item['full_name'].'</td>
            <td>'.$item['username'].'</td>
            <td>'.$item['city'].'</td>
            <td>'.$item['gender'].'</td>
            <td>'.$item['dateBirth'].'</td>
            <td>'.$item['oauth_provider'].'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>