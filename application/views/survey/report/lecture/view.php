<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">                               
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title"><?php echo $breadcrumb->parent;?></h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc"><?php echo $breadcrumb->child;?></span>       
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->                 

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--tabs">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-toolbar">
                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_3_1_tab_content" role="tab">
                                <i class="flaticon2-pie-chart-2" aria-hidden="true"></i>Result
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_2_tab_content" role="tab">
                                <i class="flaticon2-list-1" aria-hidden="true"></i>Content                            
                            </a>
                        </li>                        
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_3_3_tab_content" role="tab">
                                <i class="flaticon2-information" aria-hidden="true"></i>Setting                            
                            </a>
                        </li>                        
                    </ul>
                </div>
            </div>
            <div class="kt-portlet__body">                   
                <div class="tab-content">
                    <!--begin: Alerts -->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="alert alert-success fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20"
                                 role="alert" id="success_alert" style="display: none">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text" id="success_message"></div>
                                <div class="alert-close">
                                    <button type="button" class="close" aria-label="Close" id="success_alert_dismiss">
                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                                </div>
                            </div>
                            <div class="alert alert-danger fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20"
                                 role="alert" id="failed_alert" style="display: none">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text" id="failed_message"></div>
                                <div class="alert-close">
                                    <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss">
                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: Alerts -->
                    <div class="tab-pane active" id="kt_portlet_base_demo_3_1_tab_content" role="tabpanel"> 
                        <div class="kt-portlet">
                            <div class="kt-portlet__head align-items-center">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Lecture Survey Summary</h3>
                                </div>
                                <div class="ml-auto">
                                    <button class="btn btn-success" onclick="Export()">Export</button>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="kt-section">
                                            <h5 class="kt-section__info">Statistics</h5>
                                            <div class="kt-section__content kt-section__content">
                                                <div class="table-scrollable">
                                                    <table class="table table-bordered table-hover">
                                                        <tbody>
                                                            <tr>
                                                                <td>Seen</td>
                                                                <td id="stat_wait"><?= !empty($data) ? $data->seen_count : 0 ?></td>
                                                                <td id="percent_seen">-</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>View</td>
                                                                <td id="stat_exp"><?= !empty($data) ? $data->view_count : 0 ?></td>
                                                                <td id="percent_view">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Reject</td>
                                                                <td id="stat_paid"><?= !empty($data) ? $data->reject_count : 0 ?></td>
                                                                <td id="percent_reject">-</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Reject Permanently</td>
                                                                <td id="stat_cancel"><?= !empty($data) ? $data->reject_permanently_count : 0 ?></td>
                                                                <td id="percent_reject_permanently">-</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="kt-section">
                                            <h5 class="kt-section__info">Pie Chart</h5>
                                            <div class="kt-section__content kt-section__content">
                                                <div class="kt-portlet kt-portlet--height-fluid" style="display: block;">
                                                    <div id="kt_chart_survey_statistics" style="height: 150px; width: 150px; float:left"></div>
                                                    <div id="kt_chart_view_legend" style="float:left;" ></div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="kt-section">
                                            <h5 class="kt-section__info">Total Respondent: <?= $total_respondent ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="kt-section">
                                            <h5 class="kt-section__info">Questions</h5>
                                            <div class="kt-section__content">
                                                <div class="table-scrollable">
                                                    <table class="table table-borderless">
                                                        <tbody>
                                                            <?php if(!empty($data)): ?>
                                                                <?php $x=1 ?>
                                                                <?php foreach($data->questions as $question): ?>
                                                                    <tr>
                                                                        <td><?= $x ?></td>
                                                                        <td><?= $question->question ?></td>
                                                                        <td><button type="button" data-survey="<?= $survey_id ?>" data-question="<?= $question->id ?>" class="btn btn-primary btn-sm btn_detail">Detail</button></td>
                                                                        <!-- <td><a href="<?= base_url($controller_full_path."/answer?survey_id={$survey_id}&question_id={$question->id}"); ?>" target="_blank" class="btn btn-primary btn-sm">Details</a></td> -->
                                                                    </tr>
                                                                    <?php $x++; ?>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="kt_portlet_base_demo_3_2_tab_content" role="tabpanel"> 
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Lecture Survey Content</h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="kt-section">
                                            <h5 class="kt-section__info">Questions</h5>
                                            <div class="kt-section__content kt-section__content">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Question</th>
                                                            <th>Type</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if(!empty($data)): ?>
                                                            <?php $x = 1 ?>
                                                            <?php foreach($data->questions as $question): ?>
                                                                <tr>
                                                                    <td><?= $x ?></td>
                                                                    <td><?= $question->question ?></td>
                                                                    <td><?= ucwords(str_replace('_', ' ', $question->type)) ?></td>
                                                                </tr>
                                                                <?php if($question->type == 'multiple_choice'): ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td colspan="2">
                                                                            <table class="table table-borderless">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Choices</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php $i = 1 ?>
                                                                                    <?php foreach($question->choices as $choice): ?>
                                                                                        <tr>
                                                                                            <td><?= $i . '.&ensp;' . $choice->content ?></td>
                                                                                        </tr>
                                                                                        <?php $i++ ?>
                                                                                    <?php endforeach; ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                <?php endif ?>
                                                                <?php $x++ ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <div class="tab-pane" id="kt_portlet_base_demo_3_3_tab_content" role="tabpanel"> 
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Lecture Survey Information</h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="kt-section">
                                            <h5 class="kt-section__info">Information</h5>
                                            <div class="kt-section__content">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <?php if(!empty($data)): ?>
                                                            <tr>
                                                                <td>Name</td>
                                                                <td>:</td>
                                                                <td><?= $data->name ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Introduction</td>
                                                                <td>:</td>
                                                                <td><?= $data->introduction ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Start Date</td>
                                                                <td>:</td>
                                                                <td><?= $data->start_date ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>End Date</td>
                                                                <td>:</td>
                                                                <td><?= $data->end_date ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Platform</td>
                                                                <td>:</td>
                                                                <td><?= $data->platform ?></td>
                                                            </tr>
                                                            <?php if($data->platform == 'LMS'): ?>
                                                            <tr>
                                                                <td>Semester</td>
                                                                <td>:</td>
                                                                <td><?= $data->semester ?></td>
                                                            </tr>
                                                            <?php endif;  ?>
                                                            <tr>
                                                                <td>Subject</td>
                                                                <td>:</td>
                                                                <td><?= $data->subject_name . ' (' . $data->subject_code . ')' ?></td>
                                                            </tr>
                                                            <?php if($data->platform == 'LMS'): ?>
                                                            <tr>
                                                                <td>Class</td>
                                                                <td>:</td>
                                                                <td><?= implode(', ', $data->classes) ?></td>
                                                            </tr>
                                                            <?php endif; ?>
                                                            <tr>
                                                                <td>Created By</td>
                                                                <td>:</td>
                                                                <td><?= $data->created_by ?></td>
                                                            </tr>
                                                        <?php endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>      
            </div>
        </div>
    </div>
    <!-- end:: Content -->
    
</div>

<?php $this->load->view('templates/Footer.php'); ?>
<?php $this->load->view('survey/report/lecture/_view.php'); ?>
<script>
    var answerDatatable, surveyId, questionId, statisticChart, AlertUtil

    function initAlert() {
        AlertUtil = {
            showSuccess: function (message, timeout) {
                $("#success_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#success_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess: function () {
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed: function (message, timeout) {
                $("#failed_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideFailed: function () {
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd: function (message, timeout) {
                $("#failed_message_add").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss_add").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_add").show();
            },
            hideSuccessDialogAdd: function () {
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit: function (message, timeout) {
                $("#failed_message_edit").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss_edit").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_edit").show();
            },
            hideSuccessDialogAdd: function () {
                $("#failed_alert_dismiss_edit").trigger("click");
            },
            showFailedDialogOrder : function(message,timeout){
                $("#failed_message_order").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_order").trigger("click");
                    },timeout)
                }
                $("#failed_alert_order").show();
            },
            hideSuccessDialogOrder : function(){
                $("#failed_alert_dismiss_order").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click", function () {
            $("#failed_alert").hide();
        })
        $("#failed_alert_dismiss_order").on("click",function(){
            $("#failed_alert_order").hide();
        })
        $("#success_alert_dismiss").on("click", function () {
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click", function () {
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click", function () {
            $("#failed_alert_edit").hide();
        })
    }

    function initDatatable(){
        var option = {
            data: {
                type: 'remote',
                source: {
                read: {
                    url: '<?php echo base_url($controller_full_path."/answer_datatable"); ?>',
                    params: {
                        survey_id: surveyId,
                        question_id: questionId,
                    },
                    map: function(raw) {
                    // sample data mapping
                    var dataSet = raw;
                    if (typeof raw.data !== 'undefined') {
                        dataSet = raw.data;
                    }
                    return dataSet;
                    },
                },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState : {cookie: false,webstorage: false},
            },
            sortable: true,
            pagination: true,
            columns: [
                {
                    field: 'respondent_name',
                    title: 'Name',
                    autoHide: false,
                    width:100
                },{
                    field: 'respondent_email',
                    title: 'Email',
                    autoHide: false,
                    width:100
                },
                {
                    field: 'answer',
                    title: 'Answer',
                    autoHide: false,
                    template: function(row){
                        return row.answer
                    }
                }
            ],
            layout:{
                header:true
            }
        }

        answerDatatable = $('#kt_datatable_answer').KTDatatable(option);
        answerDatatable.on("kt-datatable--on-layout-updated",function(){
            updateStatistic()
        })

        //reset modal every time it hides
        $('#modal_view').on('hidden.bs.modal', function (e) {
            $('#statistic_container').hide()
            $('#statistic_table').empty()
            $("#kt_chart_answer_legend").empty()
        })

    }

    function initBtnEvents(){
        $('.btn_detail').on('click', function(){
            surveyId = $(this).data('survey')
            questionId = $(this).data('question')
            answerDatatable.setDataSourceParam('question_id', questionId)
            answerDatatable.setDataSourceParam('survey_id', surveyId)
            answerDatatable.reload()
            $('#modal_view').modal('show')
        })
    }
    function initPie() {
        if ($('#kt_chart_survey_statistics').length == 0) {
            return;
        }

        const seen = <?= $data->seen_count ?>;
        const view = <?= $data->view_count ?>;
        const reject = <?= $data->reject_count ?>;
        const rejectPermanently = <?= $data->reject_permanently_count ?>;

        var total = seen + view + reject + rejectPermanently
        total = total ? total : 1

        const seenPercent = ((seen / total) * 100).toFixed(2) + '%'
        const viewPercent = ((view / total) * 100).toFixed(2) + '%'
        const rejectPercent = ((reject / total) * 100).toFixed(2) + '%'
        const rejectPermanentlyPercent = ((rejectPermanently / total) * 100).toFixed(2) + '%'

        $('#percent_seen').text(seenPercent)
        $('#percent_view').text(viewPercent)
        $('#percent_reject').text(rejectPercent)
        $('#percent_reject_permanently').text(rejectPermanentlyPercent)
        let viewChart = Morris.Donut({
            element: 'kt_chart_survey_statistics',
            data: [
                {
                    label: seenPercent,
                    value: seen,
                    name: 'Name'
                },
                {
                    label: viewPercent,
                    value: view,
                    name: 'View'
                },
                {
                    label: rejectPercent,
                    value: reject,
                    name: 'Reject'
                },
                {
                    label: rejectPermanentlyPercent,
                    value: rejectPermanently,
                    name: 'Reject Permanently'
                }
            ],
            colors: [
                KTApp.getStateColor('danger'),
                KTApp.getStateColor('warning'),
                KTApp.getStateColor('success'),
                KTApp.getStateColor('primary'),
            ],
        });


        setTimeout(function(){
            viewChart.redraw()
            viewChart.data.forEach(function(label, i) {
                var legendItem = $('<span></span>').text( label['name']  ).prepend('<br><span>&nbsp;</span>');
                legendItem.find('span')
                    .css('backgroundColor', viewChart.options.colors[i])
                    .css('width', '20px')
                    .css('display', 'inline-block')
                    .css('margin', '5px');
                $('#kt_chart_view_legend').append(legendItem)
                });
        },10)
    }

    function initMorris(){
        if ($('#kt_chart_answer_statistics').length == 0) {
            return;
        }

        statisticChart = Morris.Donut({
            element: 'kt_chart_answer_statistics',
            data: [{}]
        });
    }

    function updateStatistic(){
        $('#statistic_container').hide()
        $('#statistic_table').empty()
        $("#kt_chart_answer_legend").empty()

        KTApp.block('#modal_view .modal-content', {});
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get_statistic"); ?>",
            data : {
                survey_id: surveyId,
                question_id: questionId,
            },
            dataType : "json",
            success : function(response,status){
                if(response.status == true){
                    if(response.question.type == 'multiple_choice' || response.question.type == 'rating'){
                        if(!Array.isArray(response.data)){
                            pieChart = []
                            var header = "<tr><th>Answer</th><th>Count</th><th>%</th></tr>"
                            $('#statistic_table').append(header)
                            Object.entries(response.data).forEach(answer => {
                                if(response.question.type == 'rating'){
                                    var prefix = " Stars"
                                    if(answer[0] == 1){
                                        prefix = " Star"
                                    }
                                    answer[0] += prefix
                                }
                                total = response.total_participant ? response.total_participant : 1
                                percentage = ((answer[1]/total) * 100).toFixed(0) + '%'
                                pieChart.push({label: percentage, value: answer[1],name:answer[0]})
                                row = `
                                    <tr>
                                        <td>${answer[0]}</td>
                                        <td>${answer[1]}</td>
                                        <td>${percentage}</td>
                                    </tr>
                                `
                                $('#statistic_table').append(row)
                            })
                            
                            statisticChart.setData(pieChart)
                            setTimeout(function(){
                                statisticChart.redraw()
                                statisticChart.data.forEach(function(label, i) {
                                    var legendItem = $('<span></span>').text( label['name']  ).prepend('<br><span>&nbsp;</span>');
                                    legendItem.find('span')
                                      .css('backgroundColor', statisticChart.options.colors[i])
                                      .css('width', '20px')
                                      .css('display', 'inline-block')
                                      .css('margin', '5px');
                                    $('#kt_chart_answer_legend').append(legendItem)
                                 });
                                KTApp.unblock('#modal_view .modal-content');
                            },10)

                            $('#statistic_container').show()
                        }else{
                            KTApp.unblock('#modal_view .modal-content');
                        }
                    }else{
                        KTApp.unblock('#modal_view .modal-content');
                    }

                }else{
                    KTApp.unblock('#modal_view .modal-content');
                    AlertUtil.showFailed(response.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_view .modal-content');
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        })
    }

    function Export(){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "Lecture_Survey_Answer_Report_"+date+".csv";
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: { id: '<?= $survey_id ?>'  },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }

    $(document).ready(function(){
        initAlert()
        initDatatable()
        initPie()
        initMorris()
        initBtnEvents()
    })
</script>