<script>
    var datatable

    function initDTEvents(){
        $('.btn_view').click(function(){
            url = $(this).data('url')
            window.open(url, '_blank')
        })

        $('#btn_view_export').click(function(){
            targetId = $(this).data('id')
            Export(targetId)
        })
    }

    function initDataTable(){
        var option = {
            data: {
                type: 'remote',
                source: {
                read: {
                    url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                    map: function(raw) {
                    // sample data mapping
                    var dataSet = raw;
                    if (typeof raw.data !== 'undefined') {
                        dataSet = raw.data;
                    }
                    return dataSet;
                    },
                },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState : {cookie: false,webstorage: false},
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            columns: [
                {
                    field: 'id',
                    title: '#',
                    width: 40,
                    sortable: 'asc',
                    textAlign: 'center',
                },
                {
                    field: 'name',
                    title: 'Survey Name',
                },
                {
                    field: 'start_date',
                    title: 'Start',
                },
                {
                    field: 'end_date',
                    title: 'End',
                },
                {
                    field: 'platform',
                    title: 'Platform',
                },
                {
                    field: 'semester',
                    title: 'Semester',
                },
                {
                    field: 'subject',
                    title: 'Subject',
                    template: function(row){
                        return row.subject_name + ' (' + row.subject_code + ')'
                    }
                },
                {
                    field: 'fullname',
                    title: 'Created By'
                },
                {
                    field: 'classes',
                    title: 'Classes',
                    template: function(row){
                        return row.classes
                    }
                },
                {
                    field: 'action',
                    title: 'Action',
                    sortable: false,
                    width: 100,
                    overflow: 'visible',
                    textAlign: 'center',
                    autoHide: false,
                    template: function (row) {
                        var result ="";
                        <?php if($privilege->can_read){?>
                            url = '<?= base_url($controller_full_path."/view") ?>';
                            result = result + `<span data-url="${url}?survey_id=${row.id}" class="btn btn-link btn-sm btn_view">View</span>`;
                        <?php }?>
                        return result;
                    }                        
                }
            ],
            layout:{
                header:true
            }
        }
        datatable = $('#kt_datatable').KTDatatable(option);
        datatable.on("kt-datatable--on-layout-updated",function(){
            initDTEvents();
        })
    }

    function Export(id){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "Lecture_Survey_Answer_Report_"+date+".csv";
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: { id: id  },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }

    jQuery(document).ready(function() { 
        initDataTable();
    });
</script>
