<!--begin::Modal-->
<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Result Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <div class="row" id="statistic_container" style="display: none;">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="kt-section">
                            <h5 class="kt-section__info">Statistics</h5>
                            <div class="kt-section__content kt-section__content">
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <tbody id="statistic_table">
                                        </tbody>
                                    </table>
                                </div>   
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="kt-section">
                            <h5 class="kt-section__info">Pie Chart</h5>
                            <div class="kt-section__content kt-section__content">
                                <div class="kt-portlet kt-portlet--height-fluid" style="display: block;">
                                    <div id="kt_chart_answer_statistics" style="height: 150px; width: 150px; float:left"></div>
                                    <div id="kt_chart_answer_legend" style="float:left;" ></div>
                                </div>  
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!--begin: Datatable -->        
                <table class="kt-datatable" id="kt_datatable_answer" width="100%">
                </table>
                <!--end: Datatable -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->