<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>
<style>
    .btn.btn-link:hover {
        color: #0056b3 !important;
        background-color: transparent !important;
    }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">                               
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title"><?php echo $breadcrumb->parent;?></h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc"><?php echo $breadcrumb->child;?></span>       
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->                 

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand fa <?php echo $breadcrumb->icon;?>"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                       <?php echo $breadcrumb->child;?> Answer List
                    </h3>
                </div>
            </div>

        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="col-md-pull-12" >  
                <!--begin: Alerts -->   
                <div class="kt-section">
                    <div class="kt-section__content">
                        <div class="alert alert-success fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="success_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="success_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="success_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>  
                         <div class="alert alert-danger fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="failed_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="failed_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>                   
                    </div>                   
                </div>        
                <!--end: Alerts -->
                <div class="kt-margin-t-20 kt-margin-l-20 kt-margin-r-20">
                    <div class="row" id="statistic_container" style="display: none;">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="kt-section">
                                <h5 class="kt-section__info">Statistics</h5>
                                <div class="kt-section__content kt-section__content">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <tbody id="statistic_table">
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="kt-section">
                                <h5 class="kt-section__info">Pie Chart</h5>
                                <div class="kt-section__content kt-section__content">
                                    <div class="kt-portlet kt-portlet--height-fluid">
                                        <div id="kt_chart_answer_statistics" style="height: 150px; width: 150px;"></div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin: Search Form -->
                <div class="kt-form kt-form--label-right kt-margin-l-20 kt-margin-r-20  kt-margin-b-10">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="row align-items-center">                
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>                
                            </div>
                        </div>      
                    </div>
                </div>      
                <!--end: Search Form -->
            </div>
            <!--begin: Datatable -->        
            <table class="kt-datatable" id="kt_datatable" width="100%">
            </table>
            <!--end: Datatable -->
        </div>
        </div>
    </div>
    <!-- end:: Content -->
    
</div>

<?php $this->load->view('templates/Footer.php'); ?>
<script>
    var questionType = '<?= $question_type ?>';

    function initAlert(){
        AlertUtil = {
            showSuccess : function(message,timeout){
                $("#success_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#success_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess : function(){
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed : function(message,timeout){
                $("#failed_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideFailed : function(){
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd : function(message,timeout){
                $("#failed_message_add").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_add").trigger("click");
                    },timeout)
                }
                $("#failed_alert_add").show();
            },
            hideFailedDialogAdd : function(){
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit : function(message,timeout){
                $("#failed_message_edit").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_edit").trigger("click");
                    },timeout)
                }
                $("#failed_alert_edit").show();
            },
            hideFailedDialogEdit : function(){
                $("#failed_alert_dismiss_edit").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click",function(){
            $("#failed_alert").hide();
        })
        $("#success_alert_dismiss").on("click",function(){
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click",function(){
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click",function(){
            $("#failed_alert_edit").hide();
        })
    }

    function initDataTable(){
        var option = {
            data: {
                type: 'remote',
                source: {
                read: {
                    url: '<?php echo base_url($controller_full_path."/answer_datatable"); ?>',
                    params: {
                        survey_id: <?= $survey_id ?>,
                        question_id: <?= $question_id ?>,
                    },
                    map: function(raw) {
                    // sample data mapping
                    var dataSet = raw;
                    if (typeof raw.data !== 'undefined') {
                        dataSet = raw.data;
                    }
                    return dataSet;
                    },
                },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState : {cookie: false,webstorage: false},
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            columns: [
                {
                    field: 'respondent_name',
                    title: 'Respondent Name',
                    width: 480
                },
                {
                    field: 'answer',
                    title: 'Answer',
                    width: 480,
                    template: function(row){
                        return row.answer
                    }
                }
            ],
            layout:{
                header:true
            }
        }
        datatable = $('#kt_datatable').KTDatatable(option);
        datatable.on("kt-datatable--on-layout-updated",function(){
            updateStatistic()
        })
    }

    function updateStatistic(){
        if(questionType == 'multiple_choice' || questionType == 'rating'){
            $('#statistic_table').empty()

            KTApp.blockPage();
            $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get_statistic"); ?>",
            data : {
                search: $('#generalSearch').val(),
                survey_id: <?= $survey_id ?>,
                question_id: <?= $question_id ?>,
            },
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    let statisticChart = Morris.Donut({
                        element: 'kt_chart_answer_statistics',
                        data: [{}]
                    });

                    pieChart = []
                    Object.entries(response.data).forEach(answer => {
                        total = response.total_participant ? response.total_participant : 1
                        percentage = ((answer[1]/total) * 100) + '%'
                        pieChart.push({label: percentage, value: answer[1]})
                        row = `
                            <tr>
                                <td>${answer[0]}</td>
                                <td>${answer[1]}</td>
                            </tr>
                        `
                        $('#statistic_table').append(row)
                    })
                    
                    statisticChart.setData(pieChart)
                    $('#statistic_container').show()
                }else{
                    AlertUtil.showFailed(response.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });
        }
    }

    $(document).ready(function(){
        initDataTable()
    })
</script>




