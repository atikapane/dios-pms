<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_submenu_id" name="submenu_id"/>
                    <div class="form-body">
                       <div class="row">
                            <div class="col-md-12">    
                                <div class="form-group">
                                        <label>Menu Name</label>
                                        <select name="menuid" class="form-control form-control select2" id="edit_menu_id">
                                            <?php 
                                                foreach($menus as $menu){
                                                    echo '<option value="'.$menu->menu_id.'">'.$menu->menu_name.'<option>';
                                                }                        
                                            ?>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Submenu Name</label>
                                    <input type="text" class="form-control" id="edit_submenu_name" name="submenu_name">                                      
                                </div>
                            </div>   
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Submenu Link</label>
                                    <input type="text" class="form-control" id="edit_submenu_link" name="submenu_link">                                      
                                </div>
                            </div>   
                            <div class="col-md-12" >     
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                         <div class="alert alert-danger fade show" role="alert" id="failed_alert_add" style="display: none;">
                                            <div class="alert-text" id="failed_message_add"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_add">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>                   
                                    </div>                   
                                </div>            
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->
<script type="text/javascript">
    $('#edit_menu_id').select2({
        placeholder: "Please select a Menu Name",
        width: '100%'
    });
</script>
