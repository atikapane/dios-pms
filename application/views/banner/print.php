<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Id</b></th>
            <th scope="col"><b>Type</b></th>
            <th scope="col"><b>Image</b></th>
            <th scope="col"><b>Url</b></th>
            <th scope="col"><b>Priority</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item->id.'</td>
            <td>'.$item->type.'</td>
            <td><img src="'.base_url($item->img_path).'" width="96" height="96"></td>
            <td>'.$item->url.'</td>
            <td>'.$item->priority.'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>