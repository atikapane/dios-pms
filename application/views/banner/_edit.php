<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_id" name="id"/>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Type</label>
                                    <div class="kt-radio-inline">
                                        <label class="kt-radio kt-radio--bold kt-radio--success" for="edit_type_1">
                                            <input type="radio" class="radio_type" id="edit_type_1" name="type" value="image">Image
                                            <span></span>
                                        </label>
                                        <label class="kt-radio kt-radio--bold kt-radio--danger" for="edit_type_2">
                                            <input type="radio" class="radio_type" id="edit_type_2" name="type" value="video">Video                                            URL
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group if_image">
                                    <label>Image*:</label>
                                    <div class="dropzone dropzone-default col-12 col-md-12" id="edit_image">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-image-file display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop Image to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload image</span>
                                    <input type="hidden" id="edit_image_name" name="image_name"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group if_video">
                                    <label>Link**</label>
                                    <input type="text" class="form-control" id="edit_url" name="url">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Priority</label>
                                    <input type="number" min="0" class="form-control" id="edit_priority"
                                           name="priority">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" id="edit_description" cols="30" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                        <div class="alert alert-danger fade show" role="alert" id="failed_alert_edit"
                                             style="display: none;">
                                            <div class="alert-text" id="failed_message_edit"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close"
                                                        id="failed_alert_dismiss_edit">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h6><p>*Link will be used as video source for video banner</p></h6>
                    <h6><p>**Image will be used as thumbnail for video banner</p></h6>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->