<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_id" name="id"/>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 title">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" id="edit_title" name="title">
                                </div>
                            </div>
                            <div class="col-md-12 group">
                                <div class="form-group">
                                    <label>group</label>
                                    <input type="text" class="form-control" id="edit_group" name="group">
                                </div>
                            </div>
                            <div class="col-md-12 content">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea id="edit_description" class="summernote" data-msg="Please write something" name="description" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Color</label>
                                    <select class="form-control select2" name="color" id="edit_color">
                                        <option value=""></option>
                                        <option value="yellow">Yellow</option>
                                        <option value="blue">Blue</span></option>
                                        <option value="red">Red</option>
                                        <option value="green">Green</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                        <div class="alert alert-danger fade show" role="alert" id="failed_alert_edit"
                                             style="display: none;">
                                            <div class="alert-text" id="failed_message_edit"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close"
                                                        id="failed_alert_dismiss_edit">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->