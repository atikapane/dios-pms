<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_id" name="id"/>
                    <input type="hidden" id="edit_user_id" name="user_id"/>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Task #</label>
                                    <input type="number" min="0" class="form-control" id="edit_task" name="task">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Project</label>
                                    <select class="form-control select2" id="edit_project" name="project">
                                        <option value=""></option>
                                        <?php foreach($projects as $project): ?>
                                            <option value="<?= $project->id ?>"><?= $project->nama_project ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form-control select2" id="edit_type" name="type">
                                        <option value=""></option>
                                        <option value="feature">Feature</option>
                                        <option value="bug">Bug</span></option>
                                        <option value="revision">Revision</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Task Name</label>
                                    <input type="text" class="form-control" id="edit_task_name" name="task_name" placeholder="Please enter task name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Date</label>
                                    <input type="text" class="form-control" id="edit_date" name="date" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group bootstrap-timepicker timepicker">                                    
                                    <label>Start Time</label>
                                    <input type="text" class="form-control" id="edit_start_time" name="start_time" placeholder="Masukkan Waktu Mulai" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group bootstrap-timepicker timepicker">
                                    <label>End Time</label>
                                    <input type="text" class="form-control" id="edit_end_time" name="end_time" placeholder="Masukkan Waktu Selesai" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>PCT of Task Completed</label>
                                    <input type="number" min="0" max="100" class="form-control" id="edit_pct_of_task_completed" name="pct_of_task_completed">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Notes:</label>
                                    <textarea id="edit_content" class="summernote" data-msg="Please write something" name="content" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                        <div class="alert alert-danger fade show" role="alert" id="failed_alert_edit"
                                             style="display: none;">
                                            <div class="alert-text" id="failed_message_edit"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close"
                                                        id="failed_alert_dismiss_edit">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->