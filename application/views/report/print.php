<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Id</b></th>
            <th scope="col"><b>Task</b></th>
            <th scope="col"><b>Nama Project</b></th>
            <th scope="col"><b>Type</b></th>
            <th scope="col"><b>Task name</b></th>
            <th scope="col"><b>Date</b></th>
            <th scope="col"><b>Start time</b></th>
            <th scope="col"><b>End time</b></th>
            <th scope="col"><b>Hours</b></th>
            <th scope="col"><b>Pct of task completed</b></th>
            <th scope="col"><b>Notes</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item['task'].'</td>
            <td>'.$item['nama_project'].'</td>
            <td>'.$item['type'].'</td>
            <td>'.$item['task_name'].'</td>
            <td>'.$item['date'].'</td>
            <td>'.$item['start_time'].'</td>
            <td>'.$item['end_time'].'</td>
            <td>'.$item['hours'].'</td>
            <td>'.$item['pct_of_task_completed'].'</td>
            <td>'.$item['notes'].'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>