<script>
//globals
var datatable;
var AlertUtil;
var createForm;
var editForm;
function initDTEvents(){
    $(".btn_edit").on("click",function(){
        var unitId = $(this).data("unit-id");
        var directorateId = $(this).data("directorate-id");
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    editForm.populateForm(response.data, unitId, directorateId);
                    $('#modal_edit').modal('show');
                }else{
                    AlertUtil.showFailed(response.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });
    })

    $(".btn_import").on("click", function(){
        $('#modal_import').modal('show')
    })
}
function initDataTable(){
    var option = {
        data: {
            type: 'remote',
            source: {
              read: {
                url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState : {cookie: false,webstorage: false},
          },
          sortable: true,
          pagination: true,
          search: {
            input: $('#generalSearch'),
          },
          columns: [
            {
                field: 'directorate_name',
                title: 'Directorate',
                sortable: 'asc',                          
            },
            {
                field: 'unit_name',
                title: 'Unit',
                sortable: 'asc',                          
            },
            {
                field: 'action',
                title: 'Action',
                sortable: false,
                width: 100,
                overflow: 'visible',
                textAlign: 'center',
                autoHide: false,
                template: function (row) {
                    var result ="";
                    <?php if($privilege->can_update){?>
                        var result = '<span data-unit-id="' + row.unit_id + '" data-directorate-id="' + row.directorate_id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" data-toggle="modal" data-target="#modal_edit"><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                    <?php }?>
                    return result;
                }                        
            }
          ],
          layout:{
            header:true
          }
    }
    datatable = $('#kt_datatable').KTDatatable(option);
    datatable.on("kt-datatable--on-layout-updated",function(){
        initDTEvents();
    })
}
function initCustomRule(){
}
function initAlert(){
    AlertUtil = {
        showSuccess : function(message,timeout){
            $("#success_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#success_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#success_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#success_alert_dismiss").trigger("click");
        },
        showFailed : function(message,timeout){
            $("#failed_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#failed_alert").show();
            KTUtil.scrollTop();
        },
        hideFailed : function(){
            $("#failed_alert_dismiss").trigger("click");
        },
        showFailedDialogAdd : function(message,timeout){
            $("#failed_message_add").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_add").trigger("click");
                },timeout)
            }
            $("#failed_alert_add").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_add").trigger("click");
        },
        showFailedDialogEdit : function(message,timeout){
            $("#failed_message_edit").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_edit").trigger("click");
                },timeout)
            }
            $("#failed_alert_edit").show();
        },
        hideSuccessDialogEdit : function(){
            $("#failed_alert_dismiss_edit").trigger("click");
        }
    }
    $("#failed_alert_dismiss").on("click",function(){
        $("#failed_alert").hide();
    })
    $("#success_alert_dismiss").on("click",function(){
        $("#success_alert").hide();
    })
    $("#failed_alert_dismiss_add").on("click",function(){
        $("#failed_alert_add").hide();
    })
    $("#failed_alert_dismiss_edit").on("click",function(){
        $("#failed_alert_edit").hide();
    })
}
function initEditForm(){
    //validator
    var validator = $( "#form_edit" ).validate({
        ignore:[],
        errorPlacement: function (error, element){
            error.addClass("invalid-feedback")
            element.addClass('is-invalid')
            if (element.hasClass('select2'))
                error.insertAfter(element.next('span'))
            else
                error.insertAfter(element)
        },
        rules: {
            // id: {
            //     required: true,
            //},
            unit_id: {
                required: true,
            },
            directorate_id: {
                required: true,
            },
            user: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_edit_submit").on("click",function(){
      var isValid = $( "#form_edit" ).valid();
      if(isValid){
        KTApp.block('#modal_edit .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/edit"); ?>",
            data : $('#form_edit').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_edit .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_edit').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogEdit(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_edit .modal-content');
                AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#edit_user').select2({
        placeholder: "Please select one or more user",
        width: '100%'
    });

    $('#modal_edit').on('hidden.bs.modal', function () {
       validator.resetForm();
       $('#edit_user').empty().val(null).trigger('change');
    })

    var populateForm = function(userObject, unitId, directorateId){
        $("#edit_unit_id").val(unitId);
        $("#edit_directorate_id").val(directorateId);
        userObject.forEach(user => {
            var newOption = new Option(user.fullname, user.id, false, false)
            $('#edit_user').append(newOption)
        });

        $('#edit_user').trigger('change')
    }
    
    return {
        validator:validator,
        populateForm:populateForm
    }
}

function initImportForm(){
    //validator
    var validator = $( "#form_import" ).validate({
        ignore:[],
        rules: {
            file_csv: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    $('#modal_import').on('hidden.bs.modal', function () {
       validator.resetForm();
    })

    return {
        validator:validator
    }
}

function Export(){
    <?php if($privilege->can_read){?>
    var currdate = new Date();
    var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
    var filename = "Unit_Mapping_"+date+".csv";
    var search = $('#generalSearch').val();
    $.ajax({
        url: "<?php echo base_url($controller_full_path."/export");?>",
        type: 'post',
        dataType: 'html',
        data: { search: search  },
        success: function(data) {
            var downloadLink = document.createElement("a");
            var fileData = ['\ufeff'+data];
            var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
            var url = URL.createObjectURL(blobObject);
            downloadLink.href = url;
            downloadLink.download = filename;
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
    });
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
}

function Export(){
<?php if($privilege->can_read){?>
    var currdate = new Date();
    var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
    var filename = "Unit_Mapping_"+date+".csv";
    var search = $('#generalSearch').val();
    $.ajax({
        url: "<?php echo base_url($controller_full_path."/export");?>",
        type: 'post',
        dataType: 'html',
        data: { search: search  },
        success: function(data) {
            var downloadLink = document.createElement("a");
            var fileData = ['\ufeff'+data];
            var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
            var url = URL.createObjectURL(blobObject);
            downloadLink.href = url;
            downloadLink.download = filename;
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
    })    
<?php }else{ ?>
    swal.fire({
            title: 'Infomation',
            text: "You cannot export this data",
            type: 'info',
        });
<?php } ?>
}

function Print(){
    <?php if($privilege->can_read){?>
        var search = $('#generalSearch').val();
        KTApp.blockPage();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/print_unit");?>",
            type: 'post',
            dataType: 'json',
            data: { search: search  },
            success: function (response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        var win = window.open("", "_blank");                    
                        win.document.write(response.data);
                        win.document.close();  
                        setTimeout(function() {
                            win.print();
                            win.close();
                        }, 250);
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot Print this data",
                type: 'info',
            });
    <?php } ?>
}

jQuery(document).ready(function() { 
    initDataTable();
    initCustomRule();
    editForm = initEditForm();
    importForm = initImportForm();
    initAlert();
});

</script>
