<!--begin::Modal-->
<div class="modal fade" id="modal_import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="<?= base_url($controller_full_path."/upload"); ?>" method="post" enctype="multipart/form-data" id="form_import" class="form-horizontal" accept-charset="utf-8">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 full-right">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="import_label">File CSV</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="import_csv" id="import_csv" aria-describedby="import_label" required>
                                        <label class="custom-file-label overflow-hidden text-nowrap" for="import_csv">Choose file</label>
                                    </div>
                                </div>
                                <a href="<?= base_url('public/csv/template/Unit_Mapping.csv') ?>" class="text-decoration-none" style="cursor: pointer" download="Unit_Mapping.csv">Download Template CSV</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" form="form_import" class="btn btn-primary" id="btn_import_submit" value="Import" name="import_submit">
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->
<script type="text/javascript">
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        console.log(fileName)
        console.log($(this).siblings(".custom-file-label"))
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    
    $('#edit_group').select2({
        placeholder: "Please select a Gruop Name",
        width: '100%'
    });
</script>