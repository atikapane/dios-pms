<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">                               
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title"><?php echo $breadcrumb->parent;?></h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc"><?php echo $breadcrumb->child;?></span>       
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->                 

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand fa <?php echo $breadcrumb->icon;?>"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                       <?php echo $breadcrumb->child;?> List
                    </h3>
                </div>
            </div>

        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="col-md-pull-12" >  
                <!--begin: Alerts -->   
                <div class="kt-section">
                    <div class="kt-section__content">
                        <div class="alert alert-success fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="success_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="success_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="success_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>  
                         <div class="alert alert-danger fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="failed_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="failed_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>                   
                    </div>                   
                </div>        
                <!--end: Alerts -->           
                <!--begin: Search Form -->
                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-l-20 kt-margin-r-20">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="row align-items-center">                
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>    
                                <div class="col-md-8">
                                    <div class="pull-right">
                                        <?php if($privilege->can_create){?>           
                                            <button type="button" class="btn btn-brand btn-icon-sm" data-toggle="modal" data-target="#modal_add">
                                                <i class="flaticon2-plus"></i> Add New      
                                            </button>   
                                            &nbsp
                                            <button type="button" class="btn btn-success btn-icon-sm" onclick="Export()">
                                                <i class="flaticon2-file"></i> Export CSV      
                                            </button>
                                            &nbsp
                                            <button type="button" class="btn btn-info btn-icon-sm" onclick="Print()">
                                                <i class="flaticon2-printer"></i> Print
                                            </button>
                                        <?php }?>
                                    </div>
                                </div>              
                            </div>
                        </div>      
                    </div>
                </div>      
                <!--end: Search Form -->
            </div>
            <!--begin: Datatable -->        
            <table class="kt-datatable" id="kt_datatable" width="100%">
            </table>
            <!--end: Datatable -->
        </div>
        </div>
    </div>
    <!-- end:: Content -->
    
</div>

<?php
    $this->load->view('templates/Footer.php');
    $this->load->view('project/_add.php');
    $this->load->view('project/_edit.php');
?>


<script>
//globals
var datatable;
var AlertUtil;
var createForm;
var editForm;
function initDTEvents(){
    $(".btn_delete").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailed(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $(".btn_edit").on("click",function(){
        var targetId = $(this).data("id");
        KTApp.blockPage();
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {id:targetId},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    editForm.populateForm(response.data);
                    $('#modal_edit').modal('show');
                }else{
                    AlertUtil.showFailed(response.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });  
    });
}
function initDataTable(){
    var option = {
        data: {
            type: 'remote',
            source: {
              read: {
                url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState : {cookie: false,webstorage: false},
          },
          sortable: true,
          pagination: true,
          search: {
            input: $('#generalSearch'),
          },
          columns: [
            {
                field: 'id',
                title: '#',
                sortable: 'asc',
                textAlign: 'center',
            },
            {
                field: 'nama_project',
                title: 'Nama Project',
                textAlign: 'left'
            }, 
            {
                field: 'tanggal_mulai',
                title: 'Tanggal Mulai',
                textAlign: 'center',
            },
            {
                field: 'tanggal_selesai',
                title: 'Tanggal Selesai',
                textAlign: 'center',
            },
            {
                field: 'nama_client',
                title: 'Client',
                textAlign: 'left'
            }, 
            {
                field: 'nama_consultant',
                title: 'Consultant',
                textAlign: 'left'
            }, 
            {
                field: 'quotation',
                title: 'Quotation',
                textAlign: 'center',
            }, 
            {
                field: 'spk',
                title: 'SPK',
                textAlign: 'center',
            }, 
            {
                field: 'action',
                title: 'Action',
                sortable: false,
                width: 100,
                overflow: 'visible',
                textAlign: 'left',
                autoHide: false,
                template: function (row) {
                    var result ="";
                    <?php if($privilege->can_update){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" style="cursor:pointer;"><i class="flaticon-edit-1"></i></span>';
                    <?php }?>
                    <?php if($privilege->can_delete){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" style="cursor:pointer;"><i class="flaticon2-trash"></i></span>';
                    <?php }?>
                    return result;
                }                        
            }
          ],
          layout:{
            header:true
          }
    }
    datatable = $('#kt_datatable').KTDatatable(option);
    datatable.on("kt-datatable--on-layout-updated",function(){
        initDTEvents();
    })
}
function initCustomRule(){
}
function initAlert(){
    AlertUtil = {
        showSuccess : function(message,timeout){
            $("#success_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#success_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#success_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#success_alert_dismiss").trigger("click");
        },
        showFailed : function(message,timeout){
            $("#failed_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#failed_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#failed_alert_dismiss").trigger("click");
        },
        showFailedDialogAdd : function(message,timeout){
            $("#failed_message_add").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_add").trigger("click");
                },timeout)
            }
            $("#failed_alert_add").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_add").trigger("click");
        },
        showFailedDialogEdit : function(message,timeout){
            $("#failed_message_edit").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_edit").trigger("click");
                },timeout)
            }
            $("#failed_alert_edit").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_edit").trigger("click");
        }
    }
    $("#failed_alert_dismiss").on("click",function(){
        $("#failed_alert").hide();
    })
    $("#success_alert_dismiss").on("click",function(){
        $("#success_alert").hide();
    })
    $("#failed_alert_dismiss_add").on("click",function(){
        $("#failed_alert_add").hide();
    })
    $("#failed_alert_dismiss_edit").on("click",function(){
        $("#failed_alert_edit").hide();
    })
}
function initCreateForm(){
    //quotation uploader
    var fileUploader;
    Dropzone.autoDiscover = false;
    var fileUploader = new Dropzone("#add_quotation",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader.on("removedfile", function (file) {
        $("#add_file_quotation").val("");
    });

    fileUploader.on("success", function (file,response) {
        $("#add_file_quotation").val(response);
    });

    //spk uploader
    var spkUploader;
    Dropzone.autoDiscover = false;
    var spkUploader = new Dropzone("#add_spk",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    spkUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    spkUploader.on("removedfile", function (file) {
        $("#add_file_spk").val("");
    });

    spkUploader.on("success", function (file,response) {
        $("#add_file_spk").val(response);
    });

    //validator
    var validator = $( "#form_add" ).validate({
        ignore:[],
        rules: {
            nama_project: {
                required: true
            },
            tanggal_mulai: {
                required: true,
            },
            tanggal_selesai: {
                required: true,
            },
            nama_client: {
                required: true,
            },
            nama_consultant: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_add_submit").on("click",function(){
      var isValid = $( "#form_add" ).valid();
      if(isValid){
        KTApp.block('#modal_add .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/save"); ?>",
            data : $('#form_add').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_add .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_add').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogAdd(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_add .modal-content');
                AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#add_nama_consultant').select2({
        placeholder: "Please select a consultant",
        width: '100%'
    });

    $('#add_nama_client').select2({
        placeholder: "Please select a client",
        width: '100%'
    });

    $('#modal_add').on('hidden.bs.modal', function () {
       validator.resetForm();
       fileUploader.removeAllFiles();
       spkUploader.removeAllFiles();
       $('#add_nama_consultant').val(null).trigger('change');
       $('#add_nama_client').val(null).trigger('change');
    })

    var date = new Date();
    $('.date-time-picker').datepicker({
            format: 'yyyy/mm/dd',
            minDate: date,
            startDate: date,
            todayHighlight: true
    });

    return {
        fileUploader:fileUploader,
        spkUploader:spkUploader,
        validator:validator
    }
}
function initEditForm(){
    //quotation uploader
    var fileUploader;
    var fileUploader = new Dropzone("#edit_quotation",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader.on("success", function (file,response) {
        $("#edit_file_quotation").val(response);
    });

    fileUploader.on("removedfile", function (file) {
        $("#edit_file_quotation").val("");
    });

    //spk uploader
    var spkUploader;
    var spkUploader = new Dropzone("#edit_spk",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    spkUploader.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    spkUploader.on("success", function (file,response) {
        $("#edit_file_spk").val(response);
    });

    spkUploader.on("removedfile", function (file) {
        $("#edit_file_spk").val("");
    });

    $('#edit_nama_consultant').select2({
        placeholder: "Please select a consultant",
        width: '100%'
    });

    $('#edit_nama_client').select2({
        placeholder: "Please select a client",
        width: '100%'
    });

    //validator
    var validator = $( "#form_add" ).validate({
        ignore:[],
        rules: {
            nama_project: {
                required: true
            },
            tanggal_mulai: {
                required: true,
            },
            tanggal_selesai: {
                required: true,
            },
            nama_client: {
                required: true,
            },
            nama_consultant: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_edit_submit").on("click",function(){
      var isValid = $( "#form_edit" ).valid();
      if(isValid){
        KTApp.block('#modal_edit .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/edit"); ?>",
            data : $('#form_edit').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_edit .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_edit').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogEdit(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_edit .modal-content');
                AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#modal_edit').on('hidden.bs.modal', function () {
       validator.resetForm();
       fileUploader.removeAllFiles();
       spkUploader.removeAllFiles();
       $('#edit_nama_client').val(null).trigger('change');
       $('#edit_nama_consultant').val(null).trigger('change');
    })

    var populateForm = function(projectObject){
        $("#edit_id").val(projectObject.id);
        $("#edit_nama_project").val(projectObject.nama_project);
        $("#edit_tanggal_mulai").val(projectObject.tanggal_mulai);
        $("#edit_tanggal_selesai").val(projectObject.tanggal_selesai);
        $("#edit_nama_client").val(projectObject.client_id).trigger('change');
        $("#edit_nama_consultant").val(projectObject.consultant).trigger('change');

        // re-populate quotation
        var filename = projectObject.quotation.replace(/^.*[\\\/]/, '')
        var mockFile = { name: filename, size: 12345 };
        fileUploader.emit("addedfile", mockFile);
        fileUploader.files.push(mockFile)
        $("#edit_file_quotation").val(filename);

        // re-populate spk
        var spkname = projectObject.spk.replace(/^.*[\\\/]/, '')
        var spkMockFile = { name: spkname, size: 12345 };
        spkUploader.emit("addedfile", spkMockFile);
        spkUploader.files.push(spkMockFile)
        $("#edit_file_spk").val(spkname);
    }
    
    return {
        fileUploader:fileUploader,
        spkUploader:spkUploader,
        validator:validator,
        populateForm:populateForm
    }
}

    function Export(){
        <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "Project_"+date+".csv";
        var search = $('#generalSearch').val();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: { search: search  },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        });
        <?php }else{ ?>
            swal.fire({
                    title: 'Infomation',
                    text: "You cannot export this data",
                    type: 'info',
                });
        <?php } ?>
    }
    
    function Print(){
        <?php if($privilege->can_read){?>
            var search = $('#generalSearch').val();
            KTApp.blockPage();
            $.ajax({
                url: "<?php echo base_url($controller_full_path."/print_project");?>",
                type: 'post',
                dataType: 'json',
                data: { search: search  },
                success: function (response, status) {
                        KTApp.unblockPage();
                        if (response.status == true) {
                            //populate form
                            var win = window.open("", "_blank");                    
                            win.document.write(response.data);
                            win.document.close();  
                            setTimeout(function() {
                                win.print();
                                win.close();
                            }, 250);
                        } else {
                            AlertUtil.showFailed(response.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
            })    
        <?php }else{ ?>
            swal.fire({
                    title: 'Infomation',
                    text: "You cannot Print this data",
                    type: 'info',
                });
        <?php } ?>
    }

jQuery(document).ready(function() { 
    initDataTable();
    initCustomRule();
    createForm = initCreateForm();
    editForm = initEditForm();
    initAlert();
});
</script>