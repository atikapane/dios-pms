<!--begin::Modal-->
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Project</label>
                                    <input type="text" class="form-control" id="add_nama_project" name="nama_project" placeholder="Nama Project">
                                </div>
                            </div>     
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Mulai</label>
                                    <input type="text" class="form-control date-time-picker" id="add_tanggal_mulai" name="tanggal_mulai" placeholder="Masukkan Tanggal Mulai" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Selesai</label>
                                    <input type="text" class="form-control date-time-picker" id="add_tanggal_selesai" name="tanggal_selesai" placeholder="Masukkan Tanggal Selesai" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Client</label>
                                    <select class="form-control" id="add_nama_client" name="nama_client">
                                        <option value=""></option>
                                        <?php foreach($clients as $client): ?>
                                            <option value="<?= $client->id ?>"><?= $client->nama ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Consultant</label>
                                    <select class="form-control" id="add_nama_consultant" name="nama_consultant">
                                        <option value=""></option>
                                        <?php foreach($consultants as $consultant): ?>
                                            <option value="<?= $consultant->id ?>"><?= $consultant->fullname ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>Quotation :</label>

                                    <div class="dropzone dropzone-default col-12 col-md-12" id="add_quotation">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="add_file_quotation" name="file_quotation"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>SPK :</label>

                                    <div class="dropzone dropzone-default col-12 col-md-12" id="add_spk">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="add_file_spk" name="file_spk"/>
                                </div>
                            </div> 
                            <div class="col-md-12" >     
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                         <div class="alert alert-danger fade show" role="alert" id="failed_alert_add" style="display: none;">
                                            <div class="alert-text" id="failed_message_add"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_add">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>                   
                                    </div>                   
                                </div>            
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_add_submit">Submit</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->