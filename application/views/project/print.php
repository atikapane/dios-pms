<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Id</b></th>
            <th scope="col"><b>Nama Project</b></th>
            <th scope="col"><b>Tanggal Mulai</b></th>
            <th scope="col"><b>Tanggal Selesai</b></th>
            <th scope="col"><b>Client</b></th>
            <th scope="col"><b>Consultant</b></th>
            <th scope="col"><b>Quotation</b></th>
            <th scope="col"><b>SPK</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item['id'].'</td>
            <td>'.$item['nama_project'].'</td>
            <td>'.$item['tanggal_mulai'].'</td>
            <td>'.$item['tanggal_selesai'].'</td>
            <td>'.$item['nama_client'].'</td>
            <td>'.$item['nama_consultant'].'</td>
            <td>'.$item['quotation'].'</td>
            <td>'.$item['spk'].'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>