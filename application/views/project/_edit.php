<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_id" name="id"/>
                    <div class="form-body">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Project</label>
                                    <input type="text" class="form-control" id="edit_nama_project" name="nama_project">
                                </div>
                            </div>     
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Mulai</label>
                                    <input type="text" class="form-control date-time-picker" id="edit_tanggal_mulai" name="tanggal_mulai" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Selesai</label>
                                    <input type="text" class="form-control date-time-picker" id="edit_tanggal_selesai" name="tanggal_selesai" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Client</label>
                                    <select class="form-control select2" id="edit_nama_client" name="nama_client">
                                        <option value=""></option>
                                        <?php foreach($clients as $client): ?>
                                            <option value="<?= $client->id ?>"><?= $client->nama ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Consultant</label>
                                    <select class="form-control select2" id="edit_nama_consultant" name="nama_consultant">
                                        <option value=""></option>
                                        <?php foreach($consultants as $consultant): ?>
                                            <option value="<?= $consultant->id ?>"><?= $consultant->fullname ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>Quotation :</label>

                                    <div class="dropzone dropzone-default col-12 col-md-12" id="edit_quotation">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="edit_file_quotation" name="file_quotation"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>SPK :</label>

                                    <div class="dropzone dropzone-default col-12 col-md-12" id="edit_spk">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="edit_file_spk" name="file_spk"/>
                                </div>
                            </div>
                            <div class="col-md-12" >     
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                         <div class="alert alert-danger fade show" role="alert" id="failed_alert_edit" style="display: none;">
                                            <div class="alert-text" id="failed_message_edit"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_edit">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>                   
                                    </div>                   
                                </div>            
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->