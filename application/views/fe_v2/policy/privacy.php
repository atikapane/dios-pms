<?php $this->load->view('fe/templates/header') ?>
<style>
    #scroll-to-top {
        position:fixed;
        right:20px;
        bottom:10px;
        cursor:pointer;
        width:70px;
        height:70px;
        background-color: #e74c3c;
        display:none;
        -webkit-border-radius: 50%;
        -moz-border-radius:50%;
        border-radius:50%;
        z-index: 9999;
    }

    #scroll-to-top span{
        position:absolute;
        top:50%;
        left:50%;
        transform: translate(-50%,-50%);
    }

    #scroll-to-top:hover {
        background-color: #3498db;
        opacity:1;filter: "alpha(opacity=100)";
        -ms-filter: "alpha(opacity=100)";
    }
</style>
<a href="" id="scroll-to-top"><span class="fas fa-arrow-up fa-2x text-secondary"></span></a>
<section class="pt-5 pb-4 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Privacy Policy</h1>

                <?= $agreement->content ?>
            </div>
        </div>
    </div>
</section>
<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->
<?php $this->load->view('fe/templates/footer') ?>
