<?php $this->load->view('fe/templates/header') ?>
<style>
    #scroll-to-top {
        position:fixed;
        right:20px;
        bottom:10px;
        cursor:pointer;
        width:70px;
        height:70px;
        background-color: #e74c3c;
        display:none;
        -webkit-border-radius: 50%;
        -moz-border-radius:50%;
        border-radius:50%;
        z-index: 9999;
    }

    #scroll-to-top span{
        position:absolute;
        top:50%;
        left:50%;
        transform: translate(-50%,-50%);
    }

    #scroll-to-top:hover {
        background-color: #3498db;
        opacity:1;filter: "alpha(opacity=100)";
        -ms-filter: "alpha(opacity=100)";
    }

    .about-title {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;         
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
    }
</style>
<a href="" id="scroll-to-top"><span class="fas fa-arrow-up fa-2x text-secondary"></span></a>
<section class="pt-5 pb-4 bg-white">
    <div class="container">
    <div class="row">

        <div class="col-lg-3 pr-0">
            <div class="book-box blue-diagonal">
                <div class="book-container">
                    <span class="book-accent"></span>
                    <div class="book-content">
                        <h5 class="about_link" data-id="<?= $dashboard->id ?>" style="cursor: pointer;"><?= $dashboard->title ?></h5>
                        <p>Dashboard & Administration</p>
                    </div>
                </div>
            </div>
            <div class="book-box-title blue-diagonal-title">
                &nbsp
            </div>
        </div>
        <?php $x = 1 ?>
        <?php foreach($groups as $group => $abouts): ?>
            <?php 
                switch($x % 4){
                    case 0:
                        $color = 'blue';
                        break;
                    case 1:
                        $color = 'green';
                        break;
                    case 2:
                        $color = 'yellow';
                        break;
                    case 3:
                        $color = 'red';
                        break;
                }
                ?>
            <div class="col-lg-3 pr-0 pl-0">
                <div class="book-box <?= $color ?>-diagonal">
                    <div class="book-container">
                        <span class="book-accent"></span>
                        <?php foreach($abouts as $about): ?>
                        <a class="book-arrow book-arrow-<?= $about->color ?> about_link" data-id="<?= $about->id ?>" style="cursor: pointer">
                            <div class="about-title"><?= $about->title ?></div>
                        </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="book-box-title <?= $color ?>-diagonal-title">
                    <?= $group ?>
                </div>
            </div>
            <?php $x++ ?>
        <?php endforeach; ?>
    </div>
    </div>
</section>
<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->

<div class="container mt-5" id="about_description">
    <div class="caret mt-5 mb-3">
        <div class="caret-title">About Us</div>
    </div>
    <div class="jumbotron bg-white terms p-3 fs-12">
        <div class="font-weight-bold fs-15 mb-3" id="about_title"><?= $dashboard->title ?></div>
        <?= $dashboard->description ?>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>
<script>
    function blockUI(){
        $("#loading_overlay").show()
    }
    function unblockUI(){
        $("#loading_overlay").hide()
    }
    $(document).ready(function(){
        $('.about_link').each(function(){
            let self = this
            $(self).click(function(){
                var id = $(this).data('id')
                blockUI()
                $.ajax({
                    type : 'GET',
                    url : "<?= base_url('about/get_description'); ?>",
                    data : {
                        id: id
                    },
                    dataType : "json",
                    success : function(data,status){
                        $('#about_title').empty();
                        $('#about_title').text(data.data.title)
                        $('#about_title').get(0).nextSibling.remove()
                        $('#about_title').siblings().remove()
                        $('#about_title').after(data.data.description)
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $("#about_description").offset().top
                        }, 600);
                        unblockUI()
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        unblockUI()
                    }
                });
            })
        })

        $(window).scroll(function(){ 
            if ($(this).scrollTop() > 300) { 
                $('#scroll-to-top').fadeIn(); 
            } else { 
                $('#scroll-to-top').fadeOut(); 
            } 
        }); 
        $('#scroll-to-top').click(function(){ 
            $("html, body").animate({ scrollTop: 0 }, 600); 
            return false; 
        });
    })
</script>