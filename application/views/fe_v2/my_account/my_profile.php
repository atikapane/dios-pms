<?php $this->load->view('fe/templates/header') ?>
<style>
    .form-group input, .form-group select{
        border: 0;
        border-bottom: 1px solid darkred !important;
    }

    .form-group input:focus, .form-group select:focus{
        outline: 0 !important;
        -webkit-appearance: none;
        box-shadow: none !important;
    }

    .form-control{
        color:black;
    }

    .title-selected{
        color:darkred !important;
        background-color:white;
    }
</style>
<div class="container pt-3 pb-5 py-md-5">
    <div class="row">
        <div class="col-12 col-md-3 mb-3 mt-md-5">
            <div class="d-flex flex-column pt-5">
                <h5 class="font-weight-bold" style="word-wrap: break-word;"><?= $user->username . '@365.telkomuniversity.ac.id' ?></h5>
                <span class="wash-out">Member since <time><?= date('d M Y', strtotime($user->created_on)) ?></time></span>
                <div class="border my-3"></div>

                <div class="text-primary font-weight-bolder">Course</div>
                <?php foreach($courses_status as $status => $count): ?>
                <div class="d-flex mt-3 fs-18"><?= $status ?> Course <span class="ml-auto"><?= empty($count) ? 0 : $count ?></span></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-12 col-md-9 d-flex flex-column">
            <div class="border pb-3">
                <div class="bg-danger d-flex flex-row">
                    <a href="<?= base_url('account') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Course</a>
                    <a href="<?= base_url('account/transaction') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">Transaction</a>
                    <a href="<?= base_url('account/profile') ?>" class="text-decoration-none title-selected px-4  py-3 fs-17 font-weight-bold">My Profile</a>
                    <a href="<?= base_url('account/certificate') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Certificate</a>
                </div>
                
                <div class="container-fluid ph-25">
                    <h5 class="font-weight-bold mb-5 mt-5">Personal Information</h5>
                    <form action="#" id="form_edit" class="form-horizontal">
                        <input type="hidden" value="<?= $user->id ?>" id="edit_id" name="id"/>
                        <div class="form-group">
                            <label for="username" class="text-primary">Username</label>
                            <input type="text" name="username" class="form-control fs-20" id="username" placeholder="Username" value="<?= $user->username ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="fullname" class="text-primary">Full Name</label>
                            <input type="text" name="full_name" class="form-control fs-20" id="fullname" placeholder="Fullname" value="<?= $user->full_name ?>">
                        </div>
                        <div class="form-group">
                            <label for="phone_number" class="text-primary">Phone Number</label>
                            <input type="number" name="phone_number" class="form-control fs-20" id="phone_number" placeholder="Phone Number" value="<?= $user->phone_number ?>">
                        </div>
                        <div class="form-group">
                            <label for="birth_date" class="text-primary">Birthdate</label>
                            <input type="text" name="dateBirth" class="form-control date-picker fs-20" id="birth_date" placeholder="Birthdate" value="<?= $user->dateBirth ?>" onkeypress="javascript:void(0)">
                        </div>
                        <div class="form-group">
                            <label for="email" class="text-primary">Email</label>
                            <input type="email" name="email" class="form-control fs-20" id="email" placeholder="Email" value="<?= $user->email ?>">
                        </div>
                        <div class="form-group">
                            <label for="city" class="text-primary">City</label>
                            <select class="form-control fs-20" name="city_id" id="profile_city">
                                <?php foreach($cities as $city): ?>
                                    <option value="<?= $city->id ?>" <?= $user->city_id == $city->id ? 'selected' : '' ?>><?= $city->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="gender" class="text-primary">Gender</label>
                            <select class="form-control fs-20" name="gender" id="profile_gender">
                                <option value="99" <?= $user->gender == 99 ? 'selected' : '' ?>>Other/Prefer not to say</option>
                                <option value="1" <?= $user->gender == 1 ? 'selected' : '' ?>>Male</option>
                                <option value="2" <?= $user->gender == 2 ? 'selected' : '' ?>>Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="old_password" class="text-primary">Old Password</label>
                            <input type="password" name="old_password" class="form-control fs-20" id="old_password" placeholder="Old Password">
                        </div>
                        <div class="form-group">
                            <label for="new_password" class="text-primary">New Password</label>
                            <input type="password" name="new_password" class="form-control fs-20" id="new_password" placeholder="New Password">
                        </div>
                    </form>
                    <div class="alert alert-danger fade show mt-3" role="alert" id="failed_alert" style="display: none;">
                        <div class="alert-text" id="failed_message"></div>
                        <div class="alert-close">
                            <button type="button" class="close" aria-label="Close"
                                    id="failed_alert_dismiss">
                                <span aria-hidden="true"><i class="la la-close"></i></span>
                            </button>
                        </div>
                    </div>
                    <div class="alert alert-success fade show mt-3" role="alert" id="success_alert" style="display: none;">
                        <div class="alert-text" id="success_message"></div>
                        <div class="alert-close">
                            <button type="button" class="close" aria-label="Close"
                                    id="success_alert_dismiss">
                                <span aria-hidden="true"><i class="la la-close"></i></span>
                            </button>
                        </div>
                    </div>
                    <div class="d-flex">
                        <button type="button" class="btn btn-danger my-3 px-5 ml-auto rounded-0 fs-17" id="btn_update_profile"><b>Update Profile</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>
<script>
    var AlertUtil, updateForm

    function blockUI(){
        $("#loading_overlay").show()
    }

    function unblockUI(){
        $("#loading_overlay").hide()
    }

    function initFilters(){
        var date = new Date();
        var datePicker = $('.date-picker')

        datePicker.datepicker({
            format: 'yyyy/mm/dd',
            endDate: '0d',
            todayHighlight: true,
            zIndexOffset: 9990,
            autoclose: true,
            clearBtn: true,
            todayBtn: true
        });
    }

    function initAlert(){
        AlertUtil = {
            showSuccess : function(message,timeout){
                $("#success_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#success_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#success_alert").show();
            },
            hideSuccess : function(){
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed : function(message,timeout){
                $("#failed_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#failed_alert").show();
            },
            hideSuccess : function(){
                $("#failed_alert_dismiss").trigger("click");
            },
        }
        $("#failed_alert_dismiss").on("click",function(){
            $("#failed_alert").hide();
        })
        $("#success_alert_dismiss").on("click",function(){
            $("#success_alert").hide();
        })
    }

    function initCustomRule(){
        $.validator.addMethod("edit_password_contain_username", function(value, element) {
            username = '<?= $user->username ?>'
            return !(username != "" && value.includes(username))
        }, "Password cannot contain username");

        $.validator.addMethod("edit_new_password_not_empty", function(value, element){
            old_password = $("#old_password").val()
            return !(value == "" && old_password != "")
        }, "New password cannot empty when update password")
        
        $.validator.addMethod("edit_old_password_not_empty", function(value, element){
            new_password = $("#new_password").val()
            return !(value == "" && new_password != "")
        }, "Old password cannot empty when update password")
    }

    function initUpdateForm(){
        //validator
        var validator = $( "#form_edit" ).validate({
            ignore:[],
            errorPlacement: function (error, element){
                element.addClass('is-invalid')
                error.addClass('invalid-feedback')
                if (element.hasClass('select2'))
                    error.insertAfter(element.next('span'))
                else
                    error.insertAfter(element)
            },
            rules: {
                id: {
                    required: true,
                },
                old_password: {
                    minlength: 8,
                    maxlength: 12,
                    edit_old_password_not_empty: true,
                    pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/,
                },
                new_password: {
                    minlength: 8,
                    maxlength: 12,
                    edit_new_password_not_empty: true,
                    edit_password_contain_username: true,
                    pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/,
                },
                email: {
                    required: true,
                },
                full_name: {
                    required: true,
                },
                city_id: {
                    required: true,
                },
                gender: {
                    required: true,
                },
                dateBirth: {
                    required: true,
                },
                phone_number: {
                    required: true,
                    number: true,
                }
            },
            messages: {
                old_password: {
                    pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character and 1 symbol (!@$*{},.\":)"
                },
                new_password: {
                    pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character and 1 symbol (!@$*{},.\":)"
                }
            },
            invalidHandler: function(event, validator) {   
                scrollTop()
            }
        }); 

        $("#btn_update_profile").on("click", function () {
            var isValid = $( "#form_edit" ).valid();
            if(isValid){
                blockUI()
                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url().'account/update_profile/'; ?>",
                    data: $("#form_edit").serialize(),
                    dataType:"json",
                    success:function (data, status) {
                        if(data.status == true){
                            AlertUtil.showSuccess(data.message + ', please refresh the page')
                            unblockUI()
                            location.reload()
                        }else{
                            AlertUtil.showFailed(data.message);
                            unblockUI()
                        }
                    },error:function () {
                        unblockUI()
                        swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                    }
                });
            }
        });

        return {
            validator:validator
        }
    }

    function scrollTop(){
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }

    $(document).ready(function(){
        initFilters()
        initAlert()
        initCustomRule()
        updateForm = initUpdateForm()
    })
</script>