<?php $this->load->view('fe/templates/header') ?>
<style>
    .form-group span, .form-group span{
        position: absolute;
        right: 25px;
        top: 10px;
    }
    .card {
        border-radius:0px !important;
        margin-top:-1px;
    }
    a:not([href]):not([tabindex]){
        cursor:pointer;
    }

    .title-selected{
        color:darkred !important;
        background-color:white;
    }
</style>
<div class="container py-3 py-md-5">
    <div class="row">
        <div class="col-12 col-md-3 mb-3 mt-md-5">
            <div class="d-flex flex-column pt-5">
                <h5 class="font-weight-bold" style="word-wrap: break-word;"><?= $user->username . '@365.telkomuniversity.ac.id' ?></h5>
                <span class="wash-out">Member since <time><?= date('d M Y', strtotime($user->created_on)) ?></time></span>
                <div class="border my-3"></div>

                <div class="text-primary font-weight-bolder">Course</div>
                <?php foreach($courses_status as $status => $count): ?>
                <div class="d-flex mt-3 fs-18"><?= $status ?> Course <span class="ml-auto"><?= empty($count) ? 0 : $count ?></span></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-12 col-md-9 d-flex flex-column">
            <div class="border pb-3">
                <div class="bg-danger d-flex flex-row">
                    <a href="<?= base_url('account') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Course</a>
                    <a href="<?= base_url('account/transaction') ?>" class="text-decoration-none text-light py-3 px-4 font-weight-bold fs-17 title-selected">Transaction</a>
                    <a href="<?= base_url('account/profile') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Profile</a>
                    <a href="<?= base_url('account/certificate') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Certificate</a>
                </div>

                <div class="container-fluid ph-25">
                    <div class="row pt-4 pb-md-2">
                        <div class="col-12 col-md-4 d-flex mb-2 mb-md-0">
                            <div class="form-group w-100">
                                <input type="text" placeholder="Search" name="No. Inv" class="form-control" id="search_filter">
                                <span class="fas fa-search"></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 d-flex mb-2 mb-md-0">
                            <div class="form-group w-100">
                                <input placeholder="Select Date" type="text" name="select_date" id="select_date_filter" class="form-control date-picker">
                                <span class="fas fa-calendar-alt"></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 d-flex mb-2 mb-md-0">
                            <div class="form-group w-100">
                                <select name="status" id="status_filter" class="form-control">
                                    <option value="">All</option>
                                    <option value="0">Waiting Payment</option>
                                    <option value="1">Expired</option>
                                    <option value="2">Paid</option>
                                    <option value="3">Cancelled</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="border mb-4"></div>
                    <div id="transaction_container">
                    </div>
                </div>
            </div>

            <div class="ml-auto py-3">
                <nav aria-label="Page navigation">
                    <ul class="pagination" style="cursor: pointer;">
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>
<script>
    function blockUI(){
        $("#loading_overlay").show()
    }

    function unblockUI(){
        $("#loading_overlay").hide()
    }

    var transactionPerPage = 6
    var totalPage = 1
    var currentPage= 1;

    function initFilters(){
        var date = new Date();
        var datePicker = $('.date-picker')

        datePicker.datepicker({
            format: 'yyyy/mm/dd',
            startDate: '0d',
            todayHighlight: true,
            zIndexOffset: 9990,
            autoclose: true,
            clearBtn: true,
            todayBtn: true
        });
    }

    function load_transactions(){
        search = $('#search_filter').val()
        select_date = $('#select_date_filter').val()
        status = $('#status_filter').val()

        offset = (currentPage*transactionPerPage) - transactionPerPage

        blockUI()
        $.ajax({
            type: 'GET',
            url: '<?= base_url('account/get_transactions') ?>',
            data: {
                search: search,
                select_date: select_date,
                status: status,
                limit: transactionPerPage,
                start: offset
            },
            dataType: 'json',
            success: function (response, data){
                if(response.status){
                    transactionContainer = $('#transaction_container')
                    transactionContainer.empty()

                    $.each(response.transactions, function(){
                        expired_at = moment(this.expired_at).format('DD MMM YYYY HH:mm')
                        number = parseInt(this.total_price)
                        number = number.toLocaleString('id-ID', {currency: 'IDR', style: 'currency', minimumFractionDigits: 0,maximumFractionDigits: 0, currencyDisplay: 'code'})
                        if(this.hasOwnProperty('banner_image')){
                            image = '<?= base_url() ?>' + this.banner_image
                        }else{
                            image = '<?= $this->config->item('ASSET_PATH') . 'fe_v2/assets/images/photo-of-person-reading-1928151.jpg' ?>'
                        }
                        coursesCard = `
                            <div class="card shadow-none">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-3 align-self-center">
                                            <img src="${image}" alt="Transaction Thumbnail">
                                        </div>
                                        <div class="col-12 col-md-9" style="min-height: 100px;">
                                            <div class="row h-100">
                                                <div class="col-6 col-md-8 d-flex flex-column mt-3 mt-md-0">
                                                    <h5 class="text-primary font-weight-bold align-items-start m-0 fs-15">${this.invoice_number}</h5>
                                                    <small class="fs-10 fs-md-12 text-gray">Time Limit <time>${expired_at}</time></small>
                                                    <div class="mt-auto font-weight-bold text-black">Total</div>
                                                </div>
                                                <div class="col-6 col-md-4 d-flex flex-column align-items-start mt-3 mt-md-0">
                                                    <span class="btn ${this.status == 0 ? 'btn-primary' : (this.status == 1 ? 'btn-danger' : (this.status == 2 ?  'btn-success' : 'btn-secondary'))} btn-sm ml-auto"><i>${this.status == 0 ? 'Waiting Payment' : (this.status == 1 ? 'Expired' : (this.status == 2 ? 'Paid' : 'Cancelled'))}</i></span>
                                                    <div class="mt-auto ml-auto font-weight-bold text-black">${number}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="border-top-1 my-3"></div>
                                    <div class="d-flex">
                                        <a class="text-decoration-none text-secondary" data-invoice-number="${this.invoice_number}" data-state="close" id="lihat_detail" style="cursor: pointer"><span class="fas fa-eye"></span>  Lihat Detail</a>
                                        <a class="ml-auto text-secondary text-decoration-none" data-id="${this.id}" id="download_transaction"><span class="fas fa-download"></span></a>
                                    </div>
                                </div>
                            </div>
                        `
                        transactionContainer.append(coursesCard)
                    })

                    totalPage = Math.ceil(response.count / transactionPerPage)
                    generate_pagination()
                }else{
                    totalPage = response.count
                    $('#transaction_container').empty().html(response.message)
                    generate_pagination()
                }
                unblockUI()
            },
            error: function (jqXHR, textStatus, errorThrown){
                unblockUI()
                swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
            }
        })
    }

    $('#transaction_container').on('click', '#download_transaction', function(){
        var id = $(this).data('id')
        blockUI()
        $.ajax({
            type: 'GET',
            url: '<?= base_url('account/download_transaction') ?>',
            data: {id: id},
            dataType: 'json',
            success: function(response, status){
                if(response.status){
                    window.open('<?= base_url('account/download') ?>' + '?filename=' + response.data)
                }else{
                    swal.fire({text:response.message,confirmButtonColor:"darkred"})
                }
                unblockUI()
            },
            error: function(jqXHR, textStatus, errorThrown){
                unblockUI()
                swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
            }
        })
    })

    $('#transaction_container').on('click', '#lihat_detail', function(){
        var self = this
        var invoice_number = $(self).data('invoice-number')
        var state = $(self).data('state')
        parentContainer = $(self).parent().parent().parent()
        if(state == 'close'){
            parentContainer.after('<div style="display: none"></div>')
            detailContainer = parentContainer.next()

            blockUI()
            $.ajax({
                type: 'GET',
                url: '<?= base_url('account/get_detail_transactions') ?>',
                data: {invoice_number: invoice_number},
                dataType: 'json',
                success: function(response, status){
                    if(response.status){
                        console.log(response.detail_transactions)
                        $.each(response.detail_transactions, function(){
                            image = '<?= base_url() ?>' + this.banner_image
                            number = parseInt(this.price)
                            number = number.toLocaleString('id-ID', {currency: 'IDR', style: 'currency', minimumFractionDigits: 0,maximumFractionDigits: 0, currencyDisplay: 'code'})
                            detailCard = `
                                <div class="card shadow-none">
                                    <div class="card-body bg-light-gray">
                                        <div class="row flex-column flex-md-row">
                                            <div class="col col-md-2 align-self-center">
                                                <img src="${image}" alt="Course Banner">
                                            </div>
                                            <div class="col col-md-8">
                                                <h1 class="card-title fs-15 my-3 m-md-0 text-black">${this.course_title}</h1>
                                                <p class="m-0 elipsis" style="font-size: 13px;color:black">${this.course_description}</p>
                                            </div>
                                            <div class="col col-md-2 text-right mt-md-0 text-black">
                                                <span class="font-weight-bold fs-15">${number}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `
                            console.log(detailCard)
                            detailContainer.append(detailCard)
                            console.log(detailContainer)
                        })
                    }else{
                        detailContainer.append(response.message)
                    }
                    unblockUI()
                    detailContainer.slideDown(600)
                },
                error: function (jqXHR, textStatus, errorThrown){
                    unblockUI()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            })

            $(self).data('state', 'open')
            $(self).find('span').removeClass('fa-eye')
            $(self).find('span').addClass('fa-eye-slash')
            $(self).get(0).lastChild.nodeValue= '  Sembunyikan Detail'
        }else{
            $(self).data('state', 'close')
            $(self).find('span').addClass('fa-eye')
            $(self).find('span').removeClass('fa-eye-slash')
            $(self).get(0).lastChild.nodeValue= '  Lihat Detail'
            parentContainer.next().slideUp(600, function(){
                this.remove()
            })
        }
    })

    function generate_pagination(){
        paginationContainer = $('ul.pagination')
        appendPagination = '';
        if(totalPage > 0){
            if((+currentPage - 3) > 1){
                appendPagination += `<li class="page-item"><a class="page-link ${currentPage == 1 ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="1">1</a></li>`
                if(currentPage-3 != 2) appendPagination += `<li class="page-item"><a class="page-link text-dark bg-transparent border-0" data-page="">...</a></li>`
            }
    
            for(var i = +currentPage - 3; i <= +currentPage; i++){
                if(i >= 1){
                    appendPagination += `<li class="page-item"><a class="page-link ${currentPage == i ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${i}">${i}</a></li>`
                }
            }
            for(var i = +currentPage + 1; i <= +currentPage + 3; i++){
                if(i <= totalPage){
                    appendPagination += `<li class="page-item"><a class="page-link ${currentPage == i ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${i}">${i}</a></li>`
                }
            }
    
            if((+currentPage + 3) < totalPage){
                if(currentPage + 3 != totalPage - 1) appendPagination += `<li class="page-item"><a class="page-link text-dark bg-transparent border-0"data-page="">...</a></li>`
                appendPagination += `<li class="page-item"><a class="page-link ${currentPage == totalPage ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${totalPage}">${totalPage}</a></li>`
            }
        }

        paginationContainer.html(appendPagination)
    }

    $('ul.pagination').on('click', '.page-item .page-link', function(e){
        currentPage = $(this).data('page')
        if(currentPage != '') load_transactions()
    })

    $('#search_filter, #select_date_filter, #status_filter').change(function(){
        load_transactions()
    })

    $(document).ready(function(){
        initFilters()
        load_transactions()
    })
</script>