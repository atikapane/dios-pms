<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta  http-equiv='Content-Type' content='text/html; charset=UTF-8' ></meta>
    <title>Transaction Detail</title>
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/bootstrap/css/bootstrap.min.css">
    <style>
        body {
            margin: 0;
            padding: 0;
            width: 21cm;
            height: 29.7cm;
        }

        #print-area {
            position: relative;
            top: 4cm;
            left: 4cm;
            width: 14cm;
            height: 22.6cm;

            font-size: 10px;
            font-family: arial;
        }
    </style>
</head>
<body>
    <div id="print-area">
        <div class="pb-3">
            <img src="<?= $this->config->item('ASSET_PATH') . 'fe_v2/assets/images/logo-telkom.png' ?>" alt="Telkom University Logo" width="50">
            <img class="float-right" style="margin-top: 8px;" src="<?= $this->config->item('ASSET_PATH') . 'fe_v2/assets/images/logo-celoe-small.png' ?>" alt="Celoe Logo" width="100">
        </div>
        <table class="border-0 w-100">
            <colgroup>
                <col class="w-75">
                <col class="w-25">
            </colgroup>
            <tbody>
                <tr>
                    <td>
                        <table>
                            <colgroup>
                                <col class="w-25">
                                <col class="w-75">
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><strong>Invoice Number</strong></td>
                                    <td>: <?= $transaction->invoice_number ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Purchase Date</strong></td>
                                    <td>: <?= $transaction->created_date ?></td>
                                </tr>
                                <?php if($transaction->status != 3): ?>
                                    <tr>
                                        <?php if($transaction->status == 0 || $transaction->status == 1): ?>
                                            <td><strong>Payment Time Limit</strong></td>
                                            <td>: <?= $transaction->expired_at ?></td>
                                        <?php elseif($transaction->status == 2): ?>
                                            <td><strong>Payment Date</strong></td>
                                            <td>: <?= $transaction->paid_date ?></td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endif; ?>
                                <tr>
                                    <td><strong>BNI VA Number</strong></td>
                                    <td>: <?= $transaction->bni_va_number ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Status</strong></td>
                                    <td>: <?= $transaction->status == 0 ? 'Waiting for payment' : ($transaction->status == 1 ? 'Expired' : ($transaction->status == 2 ? 'Paid' : 'Cancelled')) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: text-top;">
                        <table>
                            <tbody>
                                <tr><td><strong><?= $transaction->user_full_name ?></strong></td></tr>
                                <tr><td><?= $transaction->user_city ?></td></tr>
                                <tr><td><?= $transaction->user_email ?></td></tr>
                                <tr><td>+62<?= $transaction->user_phone ?></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="border my-3"></div>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">Course</th>
                <th scope="col" class="text-right">Price</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($transaction->transaction_items as $item): ?>
                    <tr>
                        <td><?= $item->course_title ?></td>
                        <td class="text-right">IDR <?= number_format($item->price, 0, ',', '.') ?></td>
                    </tr>
                <?php endforeach ?>
                <tr>
                    <td class="text-right"><strong>Total Price</strong></td>
                    <td class="text-right">IDR <?= number_format($transaction->total_price, 0, ',', '.') ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/bootstrap/js/bootstrap.min.js"></script>
</html>