<?php $this->load->view('fe/templates/header') ?>
<style>
    .form-group span, .form-group span{
        position: absolute;
        right: 25px;
        top: 10px;
    }
    a:not([href]):not([tabindex]){
        cursor:pointer;
    }

    .title-selected{
        color:darkred !important;
        background-color:white;
    }

    #image_container{
        position: relative;
        width: 100%;
        height: 100%;
    }

    #image_overlay{
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(0, 0, 0, .5);
        color: white;
        display: none;
    }

    #image_container:hover #image_overlay{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
</style>
<div class="container py-3 py-md-5">
    <div class="row">
        <div class="col-12 col-md-3 mb-3 mt-md-5">
            <div class="d-flex flex-column pt-5">
                <h5 class="font-weight-bold" style="word-wrap: break-word;"><?= $user->username . '@365.telkomuniversity.ac.id' ?></h5>
                <span class="wash-out">Member since <time><?= date('d M Y', strtotime($user->created_on)) ?></time></span>
                <div class="border my-3"></div>

                <div class="text-primary font-weight-bolder">Course</div>
                <?php foreach($courses_status as $status => $count): ?>
                <div class="d-flex mt-3 fs-18"><?= $status ?> Course <span class="ml-auto"><?= empty($count) ? 0 : $count ?></span></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-12 col-md-9 d-flex flex-column">
            <div class="border pb-3">
                <div class="bg-danger d-flex flex-row">
                    <a href="<?= base_url('account') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Course</a>
                    <a href="<?= base_url('account/transaction') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">Transaction</a>
                    <a href="<?= base_url('account/profile') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Profile</a>
                    <a href="<?= base_url('account/certificate') ?>" class="text-decoration-none text-light py-3 px-4 font-weight-bold fs-17 title-selected">My Certificate</a>
                </div>

                <div class="container-fluid p-4">
                    <div class="row pb-md-2">
                        <div class="col-12 d-flex mb-2 mb-md-0">
                            <div class="form-group w-100">
                                <input type="text" placeholder="Search Course" name="No. Inv" class="form-control" id="search_filter">
                                <span class="fas fa-search"></span>
                            </div>
                        </div>
                    </div>
                    <div class="border mb-4"></div>
                    <div id="certificate_container">
                    </div>
                </div>
            </div>

            <div class="ml-auto py-3">
                <nav aria-label="Page navigation">
                    <ul class="pagination" style="cursor: pointer;">
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>
<script>
    function blockUI(){
        $("#loading_overlay").show()
    }

    function unblockUI(){
        $("#loading_overlay").hide()
    }

    var certificatePerPage = 6
    var totalPage = 1
    var currentPage= 1;

    function load_certificates(){
        search = $('#search_filter').val()

        offset = (currentPage*certificatePerPage) - certificatePerPage

        blockUI()
        $.ajax({
            type: 'GET',
            url: '<?= base_url('account/get_certificates') ?>',
            data: {
                search: search,
                limit: certificatePerPage,
                start: offset
            },
            dataType: 'json',
            success: function (response, data){
                if(response.status){
                    certificateContainer = $('#certificate_container')
                    certificateContainer.empty()

                    $.each(response.certificates, function(){
                        start_date = moment(this.start_date).format('DD MMM YYYY')
                        end_date = moment(this.end_Date).format('DD MMM YYYY')
                        if(this.hasOwnProperty('banner_image')){
                            image = '<?= base_url() ?>' + this.banner_image
                        }else{
                            image = '<?= $this->config->item('ASSET_PATH') . 'fe_v2/assets/images/photo-of-person-reading-1928151.jpg' ?>'
                        }
                        certificateCard = `
                            <div class="card mt-2 shadow-sm">
                                <div class="card-body p-0">
                                    <div class="row">
                                        <div class="col-12 col-md-3" style="max-height: 100px;">
                                            <div id="image_container">
                                                <img class="w-100 h-100" src="${image}" alt="Course Thumbnail" style="object-fit: cover; object-position: center;">
                                                <div id="image_overlay">
                                                    <div style="cursor: pointer;" class="text-center" id="download_certificate" data-id="${this.ocw_course_id}">
                                                        <img style="width: 32px; min-height: initial;" src="<?= $this->config->item("ASSET_PATH") . 'fe/images/eyes-icon-white.png' ?>" />
                                                        <p class="m-0">View Detail</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-9" style="max-height: 100px;">
                                            <div class="row h-100 py-2 py-md-0">
                                                <div class="col-9 col-md-10 d-flex flex-column justify-content-center px-4 px-md-0">
                                                    <h5 class="text-truncate font-weight-bold m-0 fs-20">${this.ocw_name}</h5>
                                                    <small class="fs-15 font-weight-bold text-primary">${this.course.faculty_store_name}</small>
                                                    <div class="card-subtitle mt-1 d-flex">
                                                        <i class="fa fa-calendar-alt"></i>
                                                        <div class="fs-12 text-gray ml-2"><time>${start_date}</time> - <time>${end_date}</time></div>
                                                    </div>
                                                </div>
                                                <div class="col-3 col-md-2 d-flex flex-row justify-content-center align-items-center">
                                                    <span>Grade <span class="bg-danger rounded p-2 d-block text-white text-center">${this.grade}</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `
                        certificateContainer.append(certificateCard)
                    })

                    totalPage = Math.ceil(response.count / certificatePerPage)
                    generate_pagination()
                }else{
                    totalPage = response.count
                    $('#certificate_container').empty().html(response.message)
                    generate_pagination()
                }
                unblockUI()
            },
            error: function (jqXHR, textStatus, errorThrown){
                unblockUI()
                swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
            }
        })
    }

    $('#certificate_container').on('click', '#download_certificate', function(){
        var id = $(this).data('id')
        blockUI()
        $.ajax({
            type: 'GET',
            url: '<?= base_url('certificate/create_certificate') ?>',
            data: {id: id},
            dataType: 'json',
            success: function(response, status){
                if(response.status){
                    response.path.forEach(path => {
                        var downloadLink = document.createElement("a");
                        var url = '<?= base_url() ?>' + path;
                        downloadLink.href = url
                        filename = path.split('/')
                        filename = filename[filename.length - 1]
                        console.log(filename)
                        downloadLink.download = filename;
                        console.log(downloadLink)
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                        document.body.removeChild(downloadLink);
                    })
                }else{
                    swal.fire({text:response.message,confirmButtonColor:"darkred"})
                }
                unblockUI()
            },
            error: function(jqXHR, textStatus, errorThrown){
                unblockUI()
                swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
            }
        })
    })

    function generate_pagination(){
        paginationContainer = $('ul.pagination')
        appendPagination = '';
        if(totalPage > 0){
            if((+currentPage - 3) > 1){
                appendPagination += `<li class="page-item"><a class="page-link ${currentPage == 1 ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="1">1</a></li>`
                if(currentPage-3 != 2) appendPagination += `<li class="page-item"><a class="page-link text-dark bg-transparent border-0" data-page="">...</a></li>`
            }
    
            for(var i = +currentPage - 3; i <= +currentPage; i++){
                if(i >= 1){
                    appendPagination += `<li class="page-item"><a class="page-link ${currentPage == i ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${i}">${i}</a></li>`
                }
            }
            for(var i = +currentPage + 1; i <= +currentPage + 3; i++){
                if(i <= totalPage){
                    appendPagination += `<li class="page-item"><a class="page-link ${currentPage == i ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${i}">${i}</a></li>`
                }
            }
    
            if((+currentPage + 3) < totalPage){
                if(currentPage + 3 != totalPage - 1) appendPagination += `<li class="page-item"><a class="page-link text-dark bg-transparent border-0"data-page="">...</a></li>`
                appendPagination += `<li class="page-item"><a class="page-link ${currentPage == totalPage ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${totalPage}">${totalPage}</a></li>`
            }
        }

        paginationContainer.html(appendPagination)
    }

    $('ul.pagination').on('click', '.page-item .page-link', function(e){
        currentPage = $(this).data('page')
        if(currentPage != '') load_certificates()
    })

    $('#search_filter').change(function(){
        load_certificates()
    })

    $(document).ready(function(){
        load_certificates()
    })
</script>