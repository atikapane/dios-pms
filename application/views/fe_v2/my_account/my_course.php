<?php $this->load->view('fe/templates/header') ?>
<style>
    .form-group span, .form-group span{
        position: absolute;
        right: 25px;
        top: 10px;
    }
    .card-body{
        padding:1rem;
    }
    #courses_container > p{
        margin-left:1rem;
        margin-right:1rem;
    }
    .title-selected{
        color:darkred !important;
        background-color:white;
    }
    .btn-warning{
        background-color:#C55A11;
        border-color:#C55A11;
    }
</style>
<div class="container py-3 py-md-5">
    <div class="row">
        <div class="col-12 col-md-3 mb-3 mt-md-5">
            <div class="d-flex flex-column pt-5">
                <h5 class="font-weight-bold" style="word-wrap: break-word;"><?= $user->username . '@365.telkomuniversity.ac.id' ?></h5>
                <span class="wash-out">Member since <time><?= date('d M Y', strtotime($user->created_on)) ?></time></span>
                <div class="border my-3"></div>

                <div class="text-primary font-weight-bolder">Course</div>
                <?php foreach($courses_status as $status => $count): ?>
                <div class="d-flex mt-3 fs-18"><?= $status ?> Course <span class="ml-auto"><?= empty($count) ? 0 : $count ?></span></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-12 col-md-9 d-flex flex-column">
            <div class="border">
                <div class="bg-danger d-flex flex-row">
                    <a href="<?= base_url('account') ?>" class="text-decoration-none text-light py-3 px-4 font-weight-bold fs-17 title-selected">My Course</a>
                    <a href="<?= base_url('account/transaction') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">Transaction</a>
                    <a href="<?= base_url('account/profile') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Profile</a>
                    <a href="<?= base_url('account/certificate') ?>" class="text-decoration-none text-light py-3 px-4 fs-17 text-black">My Certificate</a>
                </div>
                
                <div class="container-fluid ph-25">
                    <div class="row pt-4 pb-md-2">
                        <div class="col-12 col-md-4 d-flex mb-2 mb-md-0">
                            <div class="form-group w-100">
                                <input type="text" placeholder="Search" name="search" id="search_filter" class="form-control">
                                <span class="fas fa-search"></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 d-flex mb-2 mb-md-0">
                            <div class="form-group w-100">
                                <input placeholder="Select Date" type="text" name="select_date" id="select_date_filter" class="form-control date-picker">
                                <span class="fas fa-calendar-alt"></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 d-flex mb-2 mb-md-0">
                            <div class="form-group w-100">
                                <select name="status" id="status_filter" class="form-control">
                                    <option value="">All</option>
                                    <option value="upcoming">Upcoming</option>
                                    <option value="in_progress">In Progress</option>
                                    <option value="completed">Completed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="border mb-4"></div>
                    <div class="row" id="courses_container">
                    </div>
                </div>
            </div>

            <div class="ml-auto py-3">
                <nav aria-label="Page navigation">
                    <ul class="pagination" style="cursor: pointer;">
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>
<script>
    function blockUI(){
        $("#loading_overlay").show()
    }

    function unblockUI(){
        $("#loading_overlay").hide()
    }

    function initFilters(){
        var date = new Date();
        var datePicker = $('.date-picker')

        datePicker.datepicker({
            format: 'yyyy/mm/dd',
            startDate: '0d',
            todayHighlight: true,
            zIndexOffset: 9990,
            autoclose: true,
            clearBtn: true,
            todayBtn: true
        });
    }

    var coursePerPage = 6
    var totalPage = 1
    var currentPage= 1;


    function load_paid_courses(){
        search = $('#search_filter').val()
        select_date = $('#select_date_filter').val()
        status = $('#status_filter').val()

        offset = (currentPage*coursePerPage) - coursePerPage

        blockUI()
        $.ajax({
            type: 'GET',
            url: '<?= base_url('account/get_paid_courses') ?>',
            data: {
                search: search,
                select_date: select_date,
                status: status,
                limit: coursePerPage,
                start: offset
            },
            dataType: 'json',
            success: function (response, data){
                if(response.status){
                    coursesContainer = $('#courses_container')
                    coursesContainer.empty()

                    $.each(response.courses, function(){
                        if(this.is_multiple == 0){
                            startDate = moment(this.start_date)
                            endDate = moment(this.end_date)
                            if(moment().isBefore(startDate)){
                                button = 'upcoming'
                            }else if(moment().isAfter(endDate)){
                                button = 'finished'
                            }else{
                                button = 'in progress'
                            }
                            bannerImage = '<?= base_url() ?>' + this.banner_image
                            coursesCard = `
                                <div class="col-md-4 mb-3 mb-md-0">
                                    <div class="card h-100 mb-4">
                                        <img id="course_image" src="${bannerImage}" style="width: 100%; object-fit: cover; min-height: initial; height: 150px">
                                        <div class="card-body d-flex flex-column">
                                            <div class="card-subtitle mb-2 d-flex">
                                                <i class="fa fa-calendar-alt"></i>
                                                <div class="fs-12 color-gray ml-2" id="course_date"><time>${startDate.format('DD MMM YYYY')}</time> - <time>${endDate.format('DD MMM YYYY')}</time></div>
                                            </div>
                                            <h1 class="card-title fs-15" id="course_title">${this.ocw_name}</h1>
                                            <p class="fs-12 elipsis lc-4" id="course_desc">${this.description}</p>

                                            <div class="d-flex mt-auto">
                                                <a href="https://onlinelearning.telkomuniversity.ac.id/" class="text-decoration-none text-primary fs-12 my-auto"><i class="fas fa-external-link-alt"></i>  Open MOOC</a>
                                                <span class="btn ${button == 'upcoming' ? 'btn-primary' : (button == 'finished' ? 'btn-completed' : 'btn-warning')} btn-xsm ml-auto"><i class="text-white">${button}</i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `
                            coursesContainer.append(coursesCard)
                        }else{
                            let self = this
                            $.each(self.courses, function(){
                                startDate = moment(this.start_date)
                                endDate = moment(this.end_date)
                                if(moment().isBefore(startDate)){
                                    button = 'upcoming'
                                }else if(moment().isAfter(endDate)){
                                    button = 'finished'
                                }else{
                                    button = 'in progress'
                                }
                                bannerImage = '<?= base_url() ?>' + this.banner_image
                                coursesCard = `
                                    <div class="col-md-4 mb-3 mb-md-0">
                                        <div class="card h-100 mb-4">
                                            <img id="course_image" src="${bannerImage}" style="width: 100%; object-fit: cover; min-height: initial; max-height: 200px">
                                            <div class="card-body d-flex flex-column">
                                                <div class="card-subtitle mb-2 d-flex">
                                                    <i class="fa fa-calendar-alt"></i>
                                                    <div class="fs-12 color-gray ml-2" id="course_date"><time>${startDate.format('DD MMM YYYY')}</time> - <time>${endDate.format('DD MMM YYYY')}</time></div>
                                                </div>
                                                <h1 class="card-title fs-15" id="course_title">${this.ocw_name}</h1>
                                                <p class="fs-12 elipsis lc-4" id="course_desc">${this.description}</p>
                                                <div class="d-flex mt-auto">
                                                    <a href="https://onlinelearning.telkomuniversity.ac.id/" class="text-decoration-none text-primary fs-12 my-auto"><i class="fas fa-external-link-alt"></i>  Open MOOC</a>
                                                    <span class="btn ${button == 'upcoming' ? 'btn-primary' : (button == 'finished' ? 'btn-completed' : 'btn-warning')} btn-xsm ml-auto"><i class="text-white">${button}</i></span>
                                                </div>
                                                <span class="btn btn-danger btn-sm w-100 mt-2 text-white">Multi Course</span>
                                            </div>
                                        </div>
                                    </div>
                                `
                                console.log(coursesCard)
                                coursesContainer.append(coursesCard)
                            })
                        }
                    })

                    totalPage = Math.ceil(response.count / coursePerPage)
                    generate_pagination()
                }else{
                    totalPage = response.count
                    $('#courses_container').empty().html(response.message)
                    generate_pagination()
                }
                unblockUI()
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                unblockUI()
            }
        })
    }

    function generate_pagination(){
        paginationContainer = $('ul.pagination')
        appendPagination = '';
        if(totalPage > 0){
            if((+currentPage - 3) > 1){
                appendPagination += `<li class="page-item"><a class="page-link ${currentPage == 1 ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="1">1</a></li>`
                if(currentPage-3 != 2) appendPagination += `<li class="page-item"><a class="page-link text-dark bg-transparent border-0" data-page="">...</a></li>`
            }
    
            for(var i = +currentPage - 3; i <= +currentPage; i++){
                if(i >= 1){
                    appendPagination += `<li class="page-item"><a class="page-link ${currentPage == i ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${i}">${i}</a></li>`
                }
            }
            for(var i = +currentPage + 1; i <= +currentPage + 3; i++){
                if(i <= totalPage){
                    appendPagination += `<li class="page-item"><a class="page-link ${currentPage == i ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${i}">${i}</a></li>`
                }
            }
    
            if((+currentPage + 3) < totalPage){
                if(currentPage + 3 != totalPage - 1) appendPagination += `<li class="page-item"><a class="page-link text-dark bg-transparent border-0"data-page="">...</a></li>`
                appendPagination += `<li class="page-item"><a class="page-link ${currentPage == totalPage ? 'bg-danger text-light' : 'bg-transparent text-dark'} border-0" data-page="${totalPage}">${totalPage}</a></li>`
            }
        }

        paginationContainer.html(appendPagination)
    }

    $('ul.pagination').on('click', '.page-item .page-link', function(e){
        currentPage = $(this).data('page')
        if(currentPage != '') load_paid_courses()
    })

    $('#search_filter, #select_date_filter, #status_filter').change(function(){
        load_paid_courses()
    })

    $(document).ready(function(){
        initFilters()
        load_paid_courses()
    })
</script>