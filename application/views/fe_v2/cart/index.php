<?php $this->load->view('fe/templates/header') ?>
<style>
    .card {
        box-shadow: initial;
        border-radius: initial;
        margin-top:-1;
    }
    .card-body{
        padding:.5rem;
    }
    .cart-course-image{
       width: 100%;
    }
    .cart-course-image img{
        min-height: auto !important;
    }
    .item_desc p, .item_desc span {
        overflow: hidden;
        display: -webkit-box;
        line-clamp: 4;
        -webkit-line-clamp: 4;
        -webkit-box-orient: vertical;
    }
    .item_desc p:nth-child(n+2),.item_desc span:nth-child(n+2){
        display: none;
    }
    #btn_checkout{
        color:darkred;
    }
    #btn_checkout:hover{
        color:white;
    }
</style>
<div class="bg-header-cart"></div>
<div class="container my-5">
    <div class="caret mt-5 mb-3">
        <div class="caret-title">Cart</div>
    </div>
    <div class="row mt-4">
        <div class="col-md-9 text-black mb-3" id="item_container">
              
        </div>
        <div class="col-md-3 text-black" id="checkout_container">
            <div class="fs-14 text-bold">Total</div>
            <div class="d-flex align-items-center fs-30 text-bold">
                <i class="fas fa-tags flip fs-30 mr-3"></i>
                <div id="total_price">IDR</div>
            </div>
            <a class="btn btn-outline-red w-100 mt-3" style="cursor: pointer;" id="btn_checkout">CHECKOUT</a>
        </div>
    </div>
</div>
<!-- templates -->
<div class="row no_item_template" style="display: none">
    <div class="col-md-12 no_item_text">
        <h5>Your cart is empty, click <a href="<?php echo base_url('Course/catalog')?>">here</a> to browse our courses</h5>
    </div>
</div>
<div class="row item_template" style="display: none">
    <div class="col-md-12 cart_item">
        <div class="card pt-3">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="cart-course-image col-12 mb-3">
                                <img src=""
                                    class="item_image">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                            <div class="col-md-8">
                                <div class="d-grid ml-3 mb-auto">
                                    <h5 class="item_title fs-20">
                                    </h5>
                                    <p class="fs-12 item_desc  item_desc elipsis lc-4">
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="d-flex flex-column h-100 px-3 pb-3">
                                    <button class="btn-link-gray ml-auto hover-red fs-18 btn_remove_from_cart text-black"><i
                                            class="fas fa-trash-alt"></i></button>
                                    <div class="fs-20 font-weight-bold text-right mt-auto"><div class="item_price"></div></div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>

<script type="text/javascript">
    $(document).ready(function(){
        var baseUrl = "<?php echo base_url();?>";
        function renderCart(cartData){
            $("#item_container").empty();
            if(cartData.length == 0){
                var element = $(".no_item_template").clone()
                element.removeClass("no_item_template")
                element.css("display","block")
                $("#checkout_container").css("display","none")
                $("#item_container").append(element)
            }else{
                var totalPrice = 0
                for (var i = 0; i < cartData.length; i++) {
                    var item = cartData[i]
                    var element = $(".item_template").clone()
                    element.removeClass("item_template")
                    element.find(".item_title").html(item.ocw_name)
                    element.find(".item_desc").html(item.description.replace(/(<([^>]+)>)/ig,""))
                    var price = parseInt(item.price).toLocaleString('id-ID', { style: 'currency', currency: 'IDR',minimumFractionDigits: 0, maximumFractionDigits: 0 })
                    var discount_price = 100*item.price/(100-item.discount)
                    discount_price = parseInt(discount_price).toLocaleString('id-ID', { style: 'currency', currency: 'IDR',minimumFractionDigits: 0, maximumFractionDigits: 0 })
                    element.find(".item_price").html(`<strike class="fs-15">${discount_price}</strike> ${price}`)
                    element.find(".item_image").attr("src",baseUrl+item.banner_image)
                    element.find(".btn_remove_from_cart").data("id",item.id)
                    element.find(".btn_remove_from_cart").data("name",item.ocw_name)
                    element.css("display","block")
                    totalPrice += parseInt(item.price)
                    $("#item_container").append(element)
                }
                $("#total_price").html(totalPrice.toLocaleString('id-ID', { style: 'currency', currency: 'IDR',minimumFractionDigits: 0, maximumFractionDigits: 0 }))
                $("#checkout_container").css("display","block")
            }

            $(".btn_remove_from_cart").on("click",function(){
                 Swal.fire({
                  text: "Are you sure you want to delete course "+$(this).data('name')+"?",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: 'darkred',
                  cancelButtonColor: 'gray',
                  confirmButtonText: 'Yes',
                  cancelButtonText: 'No'
                }).then((result) => {
                  if (result.value) {
                      deleteFromCart($(this).data('id'));
                  }
                })
            })
        }
        function fetchCart(){
            $("#loading_overlay").show()
            $.ajax({
                type: 'GET',
                url: "<?php echo base_url("/Cart/get"); ?>",
                dataType: "json",
                success: function (data, status) {
                    $("#loading_overlay").hide()
                    if (data.status == true) {
                        renderCart(data.data)
                    } else {
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            });
        }

        function deleteFromCart(itemId){
            $("#loading_overlay").show()
            $.ajax({
                type: 'GET',
                url: "<?= base_url("/Cart/remove"); ?>",
                data: {
                    course_id: itemId
                },
                dataType: "json",
                success: function (data, status) {
                    $("#loading_overlay").hide()
                    if (data.status == true) {
                        fetchCart()
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                    } else {
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            });
        }
        fetchCart()

        $('#btn_checkout').on('click', function(){
            $("#loading_overlay").show()
            $.ajax({
                type: 'GET',
                url: "<?= base_url("/billing/make_tx"); ?>",
                data: {},
                dataType: 'json',
                success: function(response, status){
                    $("#loading_overlay").hide()
                    if(response.error_code == 407){
                        swal.fire({
                            text:"Do you want to override previous transaction?",
                            confirmButtonColor:"darkred",
                            showCancelButton:true,
                            showLoaderOnConfirm:true,
                            preConfirm: () => {
                                return fetch('<?= base_url('/billing/make_tx?override=1') ?>')
                                .then(response => {
                                    if(!response.ok) throw new Error(response.statusText)
                                    return response.json()
                                })
                                .catch(error => {
                                    swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                })
                            }
                        }).then((result) => {
                            if(result.value.status){
                                window.location.href = '<?= base_url("/cart/checkout") ?>' + '?invoice=' + result.value.data
                            }
                        })
                    }else{
                        window.location.href = '<?= base_url("/cart/checkout") ?>' + '?invoice=' + response.data
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            })
        })
    })
</script>