<?php $this->load->view('fe/templates/header') ?>
<style>
    .card {
        box-shadow: initial;
        border-radius: initial;
    }
    #countdown{
        font-size:70px;
        font-weight: bold;
    }

    .minutes-space{
        margin-left:70px;
        margin-right:70px;
    }
</style>
<div class="bg-header-checkout"></div>
<div class="container my-5 text-black">
    <div class="caret mt-5 mb-3">
        <div class="caret-title">Checkout</div>
    </div>
    <div class="jumbotron no-radius text-white text-center bg-red mt-3">
        <div class="fs-20 mb-2">Payment Time Limit</div>
        <div id="countdown"></div>
        <div class="d-flex justify-content-center text-center">
            <div class="fs-18 mr-3">Hour</div>
            <div class="fs-18 minutes-space">Minute</div>
            <div class="fs-18 ml-3">Second</div>
        </div>
    </div>
    <div class="terms p-3">
        <div class="font-weight-bold fs-15 mb-3">Payment Account Number</div>
        <img class="img-responsive" style="width: 100px !important;" src="<?= $this->config->item('ASSET_PATH') ?>fe_v2/assets/images/bni.png">
        <div class="fs-20 mt-3 text-bold" id="norek"></div>
        <div class="btn-link-red fs-15 mt-4 text-bold" id="salin_norek" style="cursor: pointer" data-toggle="tooltip" title="Copied to clipboard">Copy Account Number</div>
    </div>
    <div class="terms p-3 mt-3">
        <div class="fs-15 font-weight-bold">Amount</div>
        <div class="d-flex align-items-center fs-30 mt-1" id="jumlah">
            <i class="fas fa-tags flip fs-30 mr-3"></i>
        </div>
        <div class="btn-link-red fs-15 mt-4 text-bold" id="salin_jumlah" style="cursor: pointer" data-toggle="tooltip" title="Copied to clipboard">Copy Amount</div>
    </div>


    <div class="terms p-3 mt-3 mb-3">
        <div class="font-weight-bold fs-15 mb-3 text-bold">Payment Guide</div>
        <div class="accordion" id="accordionExample">
            <?php foreach($billing_content as $single_content): ?>
                
                    <div id="heading<?= $single_content->priority ?>">
                        <h2 class="m-0 p-0">
                            <button id="checkout_accordion" class="btn btn-link-gray text-primary fs-15 text-bold w-100 text-left <?= $single_content->priority == 1 ? '' : 'collapsed' ?>" type="button" data-toggle="collapse" data-target="#collapse<?= $single_content->priority ?>" aria-expanded="<?= $single_content->priority == 1 ? 'true' : 'false' ?>" aria-controls="collapse<?= $single_content->priority ?>">
                                <?= $single_content->title;?>
                            </button>
                        </h2>
                    </div>

                    <div id="collapse<?= $single_content->priority ?>" class="collapse <?= $single_content->priority == 1 ? 'show' : '' ?>" aria-labelledby="heading<?= $single_content->priority ?>" data-parent="#accordionExample">
                        <div class="card-body px-2">
                            <?= $single_content->content;?>
                        </div>
                    </div>
        
            <?php endforeach; ?>
        </div>
    </div>
    <div class="d-flex align-items-center">
        <a href="<?= base_url() ?>" class="btn btn-outline-red w-100 py-3">CONTINUE BROWSING</a>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>
<script>
    $(document).ready(function(){
        $('#norek').text('<?= $transaction->bni_va_number ?>')
        $('#salin_norek').click(function(){
            let norek = $.trim($('#norek').text())
            let textArea = document.createElement("textarea")
            textArea.textContent = norek
            document.body.appendChild(textArea)
            textArea.select()
            copy = document.execCommand("Copy")
            textArea.remove()
        })

        number = parseInt('<?= $transaction->total_price ?>')
        number = number.toLocaleString('id-ID', {currency: 'IDR', style: 'currency', minimumFractionDigits: 0,maximumFractionDigits: 0, currencyDisplay: 'code'})
        $('#jumlah').get(0).lastChild.nodeValue = number

        $('#salin_jumlah').click(function(){
            let jumlah = $.trim($('#jumlah').text())
            let textArea = document.createElement("textarea")
            textArea.textContent = jumlah
            document.body.appendChild(textArea)
            textArea.select()
            copy = document.execCommand("Copy")
            textArea.remove()
        })

        currentTime = moment()
        expiredAt = moment('<?= $transaction->expired_at ?>')
        different = expiredAt.diff(currentTime)
        duration = moment.duration(different)

        let hours = different > 0 ? parseInt(duration.asHours()) : 0
        let minutes = different > 0 ? parseInt(duration.asMinutes())%60 : 0
        let seconds = different > 0 ? parseInt(duration.asSeconds())%60%60 : 0
        var interval = setInterval(() => setCountdown(), 1000)



        function setCountdown(){
            $('#countdown').text(("0" + hours).slice(-2) + ' : ' + ("0" + minutes).slice(-2) + ' : ' + ("0" + seconds).slice(-2))

            if(hours > 0 || minutes > 0 || seconds > 0){
                seconds--
                if(seconds < 0){
                    minutes--
                    seconds = 59
                    if(minutes < 0){
                        hours--
                        minutes = 59
                    }
                }
            }else{
                clearInterval(interval)
            }
        }
    })
</script>