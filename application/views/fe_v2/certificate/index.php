<?php $this->load->view('fe/templates/header') ?>
<style>
  .body {
    background:white;
  }
  .hr-title{
      border-top: 6px solid darkred;
      border-radius: 10px;
      left: 0;
      width: 25%;
      margin-left: 0;
      margin-top: 0;
    }
    .spacer {
      height:100px;
    }

    table tr td {
      background-color:white;
    }

    .pentagon-shape {
      position: absolute;
      right: 0;
      z-index: -1;
      width: 30%;
      height: 50%;
      box-sizing: content-box;
      border-width: 550px 0px 0 400px;
      border-style: solid;
      border-color: #DF2B2C transparent;
    }
</style>
<div class="pentagon-shape"></div>
<div class="container">
  <div class="row">
      <div class="col-12">
        <div class="spacer"></div>
        <h1 class="h1">Certificate Verification</h1>
        <hr class="hr-title">
        <div class="table-responsive">
          <?php if(!empty($certificate)): ?>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                      <td>Certificate Number</td>
                      <td><?= $certificate->certificate_number ?></td>
                    </tr>  
                    <tr>
                      <td>Name</td>
                      <td><?= ucwords($certificate->full_name) ?></td>
                    </tr>  
                    <tr>
                      <td>Course</td>
                      <td><?= ucwords($certificate->ocw_name) . (isset($certificate->courses) ? ' ( '. implode(', ', $certificate->courses) . ' )' : '' ) ?></td>
                    </tr>  
                    <tr>
                      <td>Issued Date</td>
                      <td><?= date('d M Y', strtotime($certificate->issued_date)) ?></td>
                    </tr>  
                </tbody>
            </table>
          <?php else: ?>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                      <td><center>Certificate Number Not Found</center></td>
                    </tr>  
                </tbody>
            </table>
          <?php endif; ?>

          <div class="spacer"></div>
        </div>
      </div>
  </div>
</div>

<?php $this->load->view('fe/templates/footer') ?>
<script>
   
</script>