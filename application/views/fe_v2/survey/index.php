<?php $this->load->view('fe/templates/header') ?>
<style>
    #form_survey{
        width:100%;
    }
    .question li{
        float:left;
        clear:both;
        margin-bottom:25px !important;
        width: 100%;
    }
    .question input[type="text"]{
        border: none;
        border-bottom:1px solid darkred;
        margin-top:10px;
        background:none;
    }
    .question input[type="text"].middle:focus {
        outline-width: 0;
    }
   .checked{
    color:darkred;
   }
   .radio {
      position: relative;
      cursor: pointer;
      line-height: 25px;
      font-size: 15px;
      float:left;
      clear:both;
      margin-top:10px;
    }
    .radio .label {
      position: relative;
      display: block;
      float: left;
      margin-right: 10px;
      width: 25px;
      height: 25px;
      border: 3px solid darkred;
      border-radius: 100%;
      -webkit-tap-highlight-color: transparent;
    }
    .radio .label:after {
      content: '';
      position: absolute;
      top: 3px;
      left: 2px;
      width: 14px;
      height: 14px;
      border-radius: 100%;
      background: darkred;
      transform: scale(0);
      transition: all 0.2s ease;
      opacity: 0.08;
      pointer-events: none;
    }
    .radio:hover .label:after {
      transform: scale(3.6);
    }
    input[type="radio"]:checked + .label {
      border-color: darkred;
    }
    input[type="radio"]:checked + .label:after {
      transform: scale(1);
      transition: all 0.2s cubic-bezier(0.35, 0.9, 0.4, 0.9);
      opacity: 1;
    }
    .hidden {
      display: none;
    }
    .triangle {
        width: 0; 
        height: 0; 
        border-top: 200px solid transparent;
        border-bottom: 200px solid transparent; 
        border-right:300px solid #DF2B2C;
        position: absolute;
        right: 0;
        top: 50px;
        z-index: -1;
    }
    .ques-nav {
        display: flex;
        flex-grow: 1;
        margin-bottom:50px;
    }
    .ques-nav .info{
        display: flex;
        flex-grow: 1;
        font-size:15px;
        font-weight: bold;
        color:darkred;
        justify-content: center;
        align-items: flex-end;
    }
    .btn-text {
        font-size:25px;
        font-weight: bold;
    }
    .ph-5 {
        padding-left:3rem;
        padding-right: 3rem;
    }
    .pull-right {
        float:right;
    }
</style>

<!-- Shape !-->
<div class="triangle"></div>
<!-- End Shape !-->

<!-- Container !-->
<div class="container mt-3">
    <!-- 3 Grid Columns !-->
    <div class="caret mt-5">
        <div class="caret-title">Survey</div>
    </div>
    <?php if(isset($error)): ?>
        <?= $error; ?>
    <?php else: ?>
        <div class="row mt-4" id="main_container">
            <form id="form_survey">
                <ol class="question m-0" id="question_container">
                    <li id="question_text_template" style="display: none;">
                        <b id="question_text">
                            question 1
                        </b>
                        <input type="text" placeholder="type answer here" class="form-control" name="answer[]">
                    </li>
                    <li id="question_choice_template" style="display: none;">
                        <b id="question_text">
                            question 2
                        </b>
                        <br>
                        <label class="radio" id="choice_template">
                            <input type="radio" name="answer[]" class="hidden" />
                            <span class="label"></span><span id="choice_label">choice 1-1</span>
                        </label>
                    </li>
                    <li id="question_rating_template" style="display: none;">
                        <b id="question_text">
                            question 3
                        </b>
                        <div class="star-rating mr-3">
                            <span class="fas fa-star" data-rating="1" data-target="#rating1"></span>
                            <span class="fas fa-star" data-rating="2" data-target="#rating1"></span>
                            <span class="fas fa-star" data-rating="3" data-target="#rating1"></span>
                            <span class="fas fa-star" data-rating="4" data-target="#rating1"></span>
                            <span class="fas fa-star" data-rating="5" data-target="#rating1"></span>
                            <input type="hidden" name="rating" class="rating-value" id="rating1">
                        </div>
                    </li>
                </ol>
            </form>
            <div class="ques-nav">
                <button type="button" class="btn btn-text" disabled id="btn_prev">Prev</button>           
                <div class="info" id="question_count"><p>1 of 1</p></div>
                 <button type="button" class="btn btn-primary-red ph-5 hidden" id="btn_submit">Submit</button>
                <button type="button" class="btn btn-text" id="btn_next">Next</button>
            </div>
        </div>
    <?php endif; ?>
    <!-- End Grid !-->
</div>


<?php $this->load->view('fe/templates/footer') ?>
<script>
    function blockUI(){
        $("#loading_overlay").show()
    }

    function unblockUI(){
        $("#loading_overlay").hide()
    }

    $('#question_container').on('click', '.fa-star', function(){
        let self = this
        let rating = $(self).data('rating')
        let target = $(self).data('target')
        $(target).val(rating)
        stars = $(self).closest('.star-rating').find('.fa-star')
        stars.each(function(){
            let tempRating = $(this).data('rating')
            if(tempRating <= rating){
                $(this).css('color', 'orange')
            }else{
                $(this).css('color', '#212529')
            }
        })
    })

    var survey = <?= $survey ?>;
    var surveyId = survey.id
    var questions = []
    var surveyQuestionIndex = 1
    var questionTextTemplate = $('#question_text_template').clone()
    var choiceTemplate = $('#choice_template').clone()
    $('#choice_template').remove()
    var questionChoiceTemplate = $('#question_choice_template').clone()
    var questionRatingTemplate = $('#question_rating_template').clone()
    page = 1
    totalPage = 1

    $('#btn_next').on('click', function(){
        if(surveyQuestionIndex + 5 <= questions.length){
            surveyQuestionIndex += 5
            page++
        }
        loadQuestion()
    })

    $('#btn_prev').on('click', function(){
        if(surveyQuestionIndex > 1){
            surveyQuestionIndex -= 5
            page--
        }
        loadQuestion()
    })

    $('#btn_submit').click(function(){
        var respondent = <?= json_encode($respondent) ?>;
        isValid = true
        for(i = 1; i <= questions.length; i++){
            answer = $(`[name=answer${i}]`).val()
            if(!answer) isValid = false
        }
        if(isValid){
            if(respondent && surveyId){
                blockUI()
                $.ajax({
                    type:"POST",
                    url : "<?= base_url('survey/save'); ?>",
                    data: $('#form_survey').serialize() + '&survey_id='+ surveyId +'&respondent=' + encodeURI(JSON.stringify(respondent)),
                    dataType:"json",
                    success:function (data, status) {
                        unblockUI()
                        if(data.status == true){
                            swal.fire({text:'Thank you for filling this survey, you can close this tab now',confirmButtonColor:"darkred"})
                            $('#question_container input').attr('disabled', true)
                            $('#question_container').off('click', '.fa-star')
                            $('#btn_submit').attr('disabled', true)
                        }else{
                            swal.fire({text:data.message,confirmButtonColor:"darkred"})
                        }
                    },error:function () {
                        unblockUI()
                        swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                    }
                });
            }else{
                swal.fire({text:"Information not completed, please try again",confirmButtonColor:"darkred"})    
            }
        }else{
            swal.fire({text:"Please answer all questions",confirmButtonColor:"darkred"})
        }
    })

    function getQuestion(){
        if(surveyId){
            blockUI()
            $.ajax({
                type:"GET",
                url : "<?= base_url('survey/get_question'); ?>",
                data: {id:surveyId},
                dataType:"json",
                success:function (data, status) {
                    unblockUI()
                    if(data.status == true){
                        $('#question_container').append(`<p>${survey.introduction}</p>`)

                        surveyQuestionIndex = 1
                        questions = data.data
                        page = 1
                        totalPage = Math.ceil(questions.length / 5)
                        for(i = 1; i <= questions.length; i++){
                            questionObj = questions[i - 1]
                            if(questionObj.type == 'freetext'){
                                question = questionTextTemplate.clone()
                                question.find('input').attr('name', 'answer'+i)
                            }else if(questionObj.type == 'multiple_choice'){
                                question = questionChoiceTemplate.clone()
                                let x = 1
                                questionObj.choices.forEach(choiceObj => {
                                    choice = choiceTemplate.clone()
                                    choice.find('input').attr('name', 'answer'+i).attr('value', choiceObj.content)
                                    choice.find('#choice_label').text(choiceObj.content)
                                    if(x == 1){
                                        choice.find('input').attr('checked', true)
                                    }
                                    question.append(choice)
                                    x++
                                })
                            }else if(questionObj.type == 'rating'){
                                question = questionRatingTemplate.clone()
                                question.find('.fa-star').data('target', '#rating'+i)
                                question.find('input').attr('id', 'rating'+i).attr('name', 'answer'+i)
                            }

                            question.css('display', 'list-item')
                            question.addClass('survey_question')
                            question.attr('id', 'question'+i)
                            question.find('#question_text').text(questionObj.question)
                            $('#question_container').append(question)
                        }

                        loadQuestion()
                    }else{
                        $('#main_container').empty().html(data.message)
                    }
                },error:function () {
                    unblockUI()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            });
        }
    }

    function loadQuestion(){
        $('.survey_question').attr('style', 'margin-bottom: 0 !important')
        $('.survey_question').css('visibility', 'hidden')
        $('.survey_question').css('height', '0')
        for(i = surveyQuestionIndex; i < surveyQuestionIndex + 5; i++){
            $('#question'+i).css('visibility', 'visible')
            $('#question'+i).css('height', 'initial')
            $('#question'+i).css('margin-bottom', 'initial')
        }

        $('#question_count').html(page + ' of ' + totalPage)
        if(page == 1 && totalPage == 1){
            $('#btn_prev').attr('disabled', true)
            $('#btn_next').hide()
            $('#btn_submit').show()
        }else if(page == 1 && totalPage > 1){
            $('#btn_prev').attr('disabled', true)
            $('#btn_next').show()
            $('#btn_submit').hide()
        }else if(page == totalPage){
            $('#btn_prev').attr('disabled', false)
            $('#btn_next').hide()
            $('#btn_submit').show()
        }else{
            $('#btn_prev').attr('disabled', false)
            $('#btn_next').show()
            $('#btn_submit').hide()
        }
    }

    $(document).ready(function(){
        $('#question_container').empty()
        error = '<?= isset($error) ? $error : '' ?>';
        if(!error) getQuestion()
    })
</script>