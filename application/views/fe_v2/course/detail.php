<?php $this->load->view('fe/templates/header') ?>
<!-- Container !-->
<div class="position-relative header-wrapper">
    <div class="bg-header-v3"></div>
    <div class="container-absolute">
        <div class="container">
            <div class="row">
                <div class="col-md-8 d-flex container-mt-80">
                    <div style="width:100%">
                        <div class="card size-mobile">
                            <div class="img-gradient" style="background: linear-gradient(0deg, rgba(0, 0, 0, 0.66), rgba(0, 0, 0, 0.3)), url('<?php echo base_url().$course->banner_image?>'); background-size: cover;background-repeat: no-repeat; background-position: center;">
                                <div class="fs-18 text-white ml-3 pt-3 elipsis-title">
                                    <?php echo $course->ocw_name?>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="avatar-wrapper d-flex">
                                    <div class="avatar"style="background: url('<?php echo base_url().$course->lecturer_photo?>');background-size: cover;background-repeat: no-repeat; background-position: center;"></div>
                                    <div class="d-grid ml-2">
                                        <div class="text-white fs-15 font-weight-bold">Lecturer</div>
                                        <div class="fs-12 mt-3"><?php echo $course->lecturer_name?></div>
                                    </div>
                                </div>
                                <?php if($course->type == 2){?>
                                    <div class="d-flex align-items-center fs-15 mt-3">
                                        <i class="fas fa-tags flip fs-30 mr-3 hide"></i>
                                        <strike><?php echo $course->discount==0?"":"IDR ".number_format((100*$course->price/(100-$course->discount)),0,",",".") ?></strike>
                                    </div>
                                    <div class="d-flex align-items-center fs-30 mt-3">
                                        <i class="fas fa-tags flip fs-30 mr-3"></i>
                                        IDR <?php echo number_format($course->price,0,",",".");?>
                                    </div>
                                    <button class="btn btn-outline-red w-100 mt-3 btn_buy_now">BUY NOW</button>
                                    <button class="btn btn-primary-red w-100 mt-2 btn_add_to_cart"data-id="<?php echo $course->id?>">ADD TO CART</button>
                                <?php }?>
                            </div>
                        </div>
                        <div class="fs-30 text-white title-desktop">
                            <span><?php echo $course->ocw_name?></span>
                        </div>
                        <div class="divider mt-2 mtm-50"></div>
                        <div class="d-flex mt-3 sub-card">
                            <i class="fas fa-calendar-alt mr-2"></i>
                            <span class="fs-12"><?php echo date("d M y",strtotime($course->start_date))?> - <?php echo date("d M y",strtotime($course->end_date))?></span>
                            <i class="fas fa-book-open ml-3 mr-2"></i>
                            <span class="fs-12"><?php echo $course->faculty_store_name?></span>
                        </div>
                        <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active no-radius" id="course-profile-tab" data-toggle="tab"
                                    href="#course-profile" role="tab" aria-controls="course-profile"
                                    aria-selected="true">Course Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link no-radius" id="lecture-notes-tab" data-toggle="tab"
                                    href="#lecture-notes" role="tab" aria-controls="lecture-notes"
                                    aria-selected="false">Lecture Notes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link no-radius" id="course-video-tab" data-toggle="tab"
                                    href="#course-video" role="tab" aria-controls="course-video"
                                    aria-selected="false">Course Video</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link no-radius" id="assignment-tab" data-toggle="tab" href="#assignment"
                                    role="tab" aria-controls="assignment" aria-selected="false">Assignment</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link no-radius" id="exam-tab" data-toggle="tab" href="#exam" role="tab"
                                    aria-controls="exam" aria-selected="false">Exam</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link no-radius" id="related-tab" data-toggle="tab" href="#related"
                                    role="tab" aria-controls="related" aria-selected="false">Related Source</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link no-radius" target="_balank"  href="https://openlibrary.telkomuniversity.ac.id/" aria-selected="false">
                                    <i class="fas fa-external-link-alt"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="course-profile" role="tabpanel"
                                aria-labelledby="course-profile-tab">
                                <div class="mb-5">
                                    <img src="https://telkomuniversity.ac.id/wp-content/uploads/2019/09/logo3-e1511767184374.png" style="width:90px;margin-top:20px" />
                                    <div class="fs-12 mt-4 mb-5 ">
                                         <?php echo $course->course_profile?>
                                    </div>
                                    <!-- Course Description !-->
                                    <div class="fade-container">
                                        <div class="fade-content fs-12">
                                            <div class="fs-20 font-weight-bold mb-3">Course Description</div>
                                             <?php echo $course->description?>
                                            <div class="fade-anchor"><span class="fade-anchor-text">Show more</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caret mt-5 mb-3" style="margin-top:80px !important">
                                        <div class="caret-title">Syllabus</div>
                                    </div>
                                    <?php if(!empty($syllabus)): ?>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <th>#</th>
                                                    <th>Topic</th>
                                                    <th>Duration ( Day )</th>
                                                </thead>
                                                <tbody>
                                                    <?php $n = 1 ?>
                                                    <?php foreach ($syllabus as $section): ?>
                                                        <tr>
                                                            <th scope="row"><?= $n++ ?></th>
                                                            <td><?= $section->section_name ?></td>
                                                            <td><?= $section->duration ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php else: ?>
                                        This course doesn't have syllabus
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="lecture-notes" role="tabpanel"
                                aria-labelledby="lecture-notes-tab">
                                <div class="caret mt-3 mb-3">
                                    <div class="caret-title">Lecture Notes</div>
                                </div>
                                <?php if(!empty($notes)): ?>
                                    <div class="accordion mb-3" id="collapseLectureNotes">
                                        <?php $n = 1 ?>
                                        <?php foreach ($notes as $key => $note): ?>
                                        <div class="card">
                                            <div class="card-header" id="headingLecture<?php echo $n?>">
                                                <h2 class="mb-0">
                                                    <button class="btn-link-gray collapse-item" type="button" data-toggle="collapse" data-target="#collapseLecture<?php echo $n ?>"
                                                        aria-expanded="false" aria-controls="collapseLecture<?php echo $n ?>">
                                                        <span class="fas fa-plus"></span>
                                                        <p class="col"><?= $key ?></p>
                                                    </button>
                                                </h2>
                                            </div>

                                            <div id="collapseLecture<?php echo $n?>" class="collapse multi-collapse" aria-labelledby="headingLecture<?php echo $n?>">
                                                <div class="card-body">
                                                    <?php foreach ($note as $note): ?>
                                                        <div class="d-flex align-items-center" style="font-size: 12px;margin-bottom:15px">
                                                            <?php echo $note->content_name; ?>
                                                            <div class="ml-auto">
                                                                <?php if($note->content_status){?>
                                                                    <a target="_blank" href="<?= $note->content_status ? $note->public_url : '#' ?>">
                                                                        <button class="btn-link-gray"><i class="fas fa-external-link-alt fs-15"></i></button>
                                                                    </a>
                                                                <?php }else{ ?>
                                                                    <button class="btn-link-gray"><i class="fas fa-lock fs-15"></i></button>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $n++ ?>
                                        <?php endforeach; ?>
                                    </div>
                                <?php else: ?>
                                    This course doesn't have notes
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane fade" id="course-video" role="tabpanel"
                                aria-labelledby="course-video-tab">
                                <div class="d-flex align-items-center">
                                    <div class="caret mt-3 mb-3">
                                        <div class="caret-title">Course Video</div>
                                    </div>
                                    <?php if(!empty($videos)): ?>
                                        <div class="ml-auto">
                                            <select class="form-control fs-12" id="course_video_filter">
                                                <?php 
                                                $default_section_id = 0;
                                                foreach ($videos as $key => $section): ?>
                                                <?php foreach ($section as $video): ?>
                                                        <option value="<?php echo $video->section_id?>"><?php echo $key?></option>
                                                    <?php
                                                        if($default_section_id == 0)
                                                            $default_section_id = $video->section_id;
                                                        break;
                                                    endforeach; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php if(!empty($videos)): ?>
                                    <div class="mt-3 mb-5">
                                        <div class="row">
                                            <?php $n = 1 ?>
                                            <?php foreach ($videos as $key => $section): ?>
                                            <?php foreach ($section as $video): ?>
                                                    <div class="col-md-6 video_pane" style="display: <?php echo $video->section_id == $default_section_id ? 'block' : 'none';?>" data-section="<?php echo $video->section_id?>">
                                                        <div class="video-wrapper">
                                                            <img src="<?php echo base_url() . ($video->thumbnail ?: 'assets/fe_v2/assets/images/telkom_thumbnail.jpeg')?>" class="img-responsive">
                                                            <?php if($video->content_status){?>
                                                                <button class="video-wrapper-btn video_play_btn" data-url="<?php echo $video->public_url?>">
                                                                    <i class="fas fa-play"></i>
                                                                </button>
                                                            <?php }else{ ?>
                                                                <button class="video-wrapper-btn">
                                                                    <i class="fas fa-lock"></i>
                                                                </button>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="fs-20 mt-3 elipsis-title mb-3"><?php echo $video->content_name ?></div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php $n++ ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade bd-example-modal-lg" id="video_modal" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                            <div class="modal-content">

                                                <div class="modal-body">

                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <!-- 16:9 aspect ratio -->
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <video id="video_container" controls>
                                                            <source src="">
                                                        </video>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                This course doesn't have videos
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane fade" id="assignment" role="tabpanel" aria-labelledby="assignment-tab">
                                <div class="caret mt-3 mb-3">
                                    <div class="caret-title">Assignment</div>
                                </div>
                                <?php if(!empty($assignments)): ?>
                                    <div class="accordion mb-3" id="collapseAssignment">
                                        <?php $n = 1 ?>
                                        <?php foreach ($assignments as $key => $section ): ?>
                                            <div class="card">
                                                <div class="card-header" id="headingAssignment<?php echo $n?>">
                                                    <h2 class="mb-0">
                                                        <?php $content_unlocked = is_array($section) && count($section) > 0 && $section[0]->content_status;?>
                                                        <button class="btn-link-gray assignment-list collapse-item row" type="button" data-toggle="collapse" data-target="#collapseAssignment<?php echo $content_unlocked ? $n : '0'?>"
                                                            aria-expanded="false" aria-controls="collapseAssignment<?php echo $n?>">
                                                            <?php if($content_unlocked){?>
                                                                <span class="fas fa-plus"></span>
                                                            <?php }else{ ?>
                                                                <span class="fas fa-lock"></span>
                                                            <?php } ?>
                                                                <p class="col"><?php echo trim($key)?></p>
                                                        </button>
                                                    </h2>
                                                </div>

                                                <div id="collapseAssignment<?php echo $content_unlocked ? $n : '9999'?>" class="collapse multi-collapse" aria-labelledby="headingAssignment<?php echo $n?>"> 
                                                    <?php $x = 1 ?>
                                                    <?php foreach ($section as $task): ?>
                                                            <div class="card-body" style="font-size: 12px;">
                                                                <?php if($content_unlocked):?>
                                                                    <?php echo $task->content_name?><br>
                                                                    <?php echo $task->content_desc?>
                                                                    <?php if(!empty($task->public_url)): ?>
                                                                        <a href="<?php echo $task->public_url?>" target="_blank">Download</a><br>
                                                                    <?php endif; ?>
                                                                <?php endif;?>
                                                            </div>
                                                    <?php $x++ ?>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        <?php $n++ ?>
                                        <?php endforeach; ?>
                                    </div>
                                <?php else: ?>
                                    This course doesn't have assignment
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane fade" id="exam" role="tabpanel" aria-labelledby="exam-tab">
                                <div class="caret mt-3 mb-3">
                                    <div class="caret-title">Exam</div>
                                </div>
                                <?php if(!empty($exams)): ?>
                                    <div class="filter-exam">
                                        <div class="row">
                                            <div class="col-md-6 text-white">
                                                Select Topic
                                            </div>
                                            <div class="col-md-6">
                                                <select class="form-control fs-12" id="exam_topic">
                                                    <?php 
                                                    $default_section_id = 0;
                                                    $n=1;
                                                    foreach ($videos as $key => $section): ?>
                                                    <?php foreach ($section as $video): ?>
                                                            <option value="<?php echo $n?>"><?php echo $key?></option>
                                                        <?php
                                                            if($default_section_id == 0)
                                                                $default_section_id = $n;
                                                            break;
                                                        endforeach; ?>
                                                    <?php $n++;endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6 text-white mt-3">
                                                Select Quiz
                                            </div>
                                            <div class="col-md-6 mt-3">
                                                <?php $n=1; foreach ($exams as $key => $section): ?>
                                                    <select class="form-control fs-12 exam_quiz" id="exam_quiz_<?php echo $n?>" style="display: <?php echo $n == $default_section_id ? 'block' : 'none';?>">
                                                        <?php foreach ($section as $exam): ?>
                                                            <option value="<?php echo $exam->id?>"><?php echo $exam->content_name?> <?php echo $exam->content_status == 0 ? "(Locked)": ""?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                <?php $n++;endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caret mt-3 mb-3 quiz_component dn-important">
                                        <div class="caret-title" id="quiz_title">Quiz 1</div>
                                    </div>
                                    <div class="d-flex mb-3 quiz_component dn-important">
                                        <div class="ml-auto font-weight-bold" id="quiz_count">
                                        </div>
                                    </div>
                                    <div class="jumbotron p-3 quiz_component dn-important">
                                        <div class="fs-20" id="quiz_question"></div>
                                        <div class="row mt-3" id="quiz_answer_container">
                        
                                        </div>
                                    </div>

                                    <div class="d-flex mb-3 quiz_lock dn-important">
                                        <div class="ml-auto font-weight-bold">
                                        </div>
                                    </div>
                                    <div class="jumbotron p-3 quiz_lock dn-important">
                                        <div class="fs-20" >
                                            <p>
                                            Sorry this content is locked and unavailable for viewing
                                            </p>
                                        </div>
                                        <div class="row mt-3">

                                        </div>
                                    </div>
                                    <div class="col-md-6 quiz_answer_1_template mb-3" style="display: none">
                                        <button class="btn-answer quiz_answer">4,6</button>
                                    </div>
                                    <div class="col-md-6 quiz_answer_2_template mb-3" style="display: none;">
                                        <button class="btn-answer quiz_answer">0,046</button>
                                    </div>
                                    <div class="d-flex align-items-center mb-3 quiz_component">
                                        <button class="btn-link-black font-weight-bold" id="prev_question">Prev</button>
                                        <div class="ml-auto">
                                            <button class="btn-link-black font-weight-bold" id="next_question">Next</button>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    This course doesn't have exam
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane fade" id="related" role="tabpanel" aria-labelledby="related-tab">
                                <div class="caret mt-3 mb-3">
                                    <div class="caret-title">Related Source</div>
                                </div>
                                <?php if(!empty($resources)): ?>
                                    <div class="accordion mb-3" id="collapseRelated">
                                        <?php $n = 1 ?>
                                        <?php foreach ($resources as $key => $section ): ?>
                                            <div class="card">
                                                <div class="card-header" id="heading<?php echo $n?>">
                                                    <h2 class="mb-0">
                                                        <a class="btn-link-gray collapse-item" type="button" data-toggle="collapse" href="#collapseRelated<?php echo $n?>"
                                                            aria-expanded="false" aria-controls="collapse<?php echo $n?>">
                                                            <p><i class="fas fa-plus"></i> <?php echo $key?></p>
                                                        </a>
                                                    </h2>
                                                </div>

                                                <div id="collapseRelated<?php echo $n?>" class="collapse multi-collapse" aria-labelledby="collapseRelated<?php echo $n?>" style="padding-bottom:1.25rem">
                                                    <?php $x = 1 ?>
                                                    <?php foreach ($section as $resource): ?>
                                                        <div class="card-body" style="font-size: 12px;padding-bottom:0px;">
                                                            <div class="d-flex align-items-center">
                                                                <div class="col-md-10">
                                                                    <?php echo $resource->content_name?>
                                                                </div>
                                                                <div class="col-md-2" style="text-align:right">
                                                                        <?php if($resource->content_status){?>
                                                                        <a target="_blank" href="<?php echo $resource->content_status ? $resource->content_url : '#' ?>">
                                                                            <button class="btn-link-gray"><i class="fas fa-external-link-alt fs-15"></i></button>
                                                                        </a>
                                                                        <?php }else{ ?>
                                                                        <button class="btn-link-gray"><i class="fas fa-lock fs-15"></i></button>
                                                                        <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php $x++ ?>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        <?php $n++ ?>
                                        <?php endforeach; ?>
                                    </div>
                                <?php else: ?>
                                    This course doesn't have resources
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5 mt-5">
                    <div class="card size-desktop">
                        <div class="img-gradient" style="background: linear-gradient(0deg, rgba(0, 0, 0, 0.66), rgba(0, 0, 0, 0.3)), url('<?php echo base_url().$course->banner_image?>'); background-size: cover;background-repeat: no-repeat; background-position: center;"></div>
                        <div class="card-body">
                            <div class="avatar-wrapper d-flex">
                                <div class="avatar"style="background: url('<?php echo base_url().$course->lecturer_photo?>');background-size: cover;background-repeat: no-repeat; background-position: center;"></div>
                                <div class="d-grid ml-2">
                                    <div class="text-white fs-15 font-weight-bold">Lecturer</div>
                                    <div class="fs-12 mt-3"><?php echo $course->lecturer_name?></div>
                                </div>
                            </div>
                            <?php if($course->type == 2){?>
                                <div class="d-flex align-items-center fs-15 mt-3">
                                    <i class="fas fa-tags flip fs-30 mr-3 hide"></i>
                                    <strike><?php echo $course->discount==0?"":"IDR ".number_format((100*$course->price/(100-$course->discount)),0,",",".") ?></strike>
                                </div>
                                <div class="d-flex align-items-center fs-30">
                                    <i class="fas fa-tags flip fs-30 mr-3"></i>
                                    <?php echo $course->price==0?"FREE":"IDR ".number_format($course->price,0,",",".") ?>
                                </div>
                                <button class="btn btn-outline-red w-100 mt-3 btn_buy_now">BUY
                                    NOW</button>
                                <button class="btn btn-primary-red w-100 mt-2 btn_add_to_cart">ADD TO
                                    CART</button>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="d-grid text-center mt-5" id="course-rating">
                        <div class="fs-40" id="course_overall">0.0</div>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <div class="fs-12">Course Rating</div>
                        <div class="d-flex align-items-center mt-5 w-100" id="5-star">
                            <div class="progress w-100 mr-3">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 0%"
                                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <div class="fs-12 ml-2" id="progress_text">(0)</div>
                        </div>
                        <div class="d-flex align-items-center mt-2 w-100" id="4-star">
                            <div class="progress w-100 mr-3">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 0%"
                                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <div class="fs-12 ml-2" id="progress_text">(0)</div>
                        </div>
                        <div class="d-flex align-items-center mt-2 w-100" id="3-star">
                            <div class="progress w-100 mr-3">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 0%"
                                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <div class="fs-12 ml-2" id="progress_text">(0)</div>
                        </div>
                        <div class="d-flex align-items-center mt-2 w-100" id="2-star">
                            <div class="progress w-100 mr-3">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 0%"
                                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <div class="fs-12 ml-2" id="progress_text">(0)</div>
                        </div>
                        <div class="d-flex align-items-center mt-2 w-100" id="1-star">
                            <div class="progress w-100 mr-3">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 0%"
                                    aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <div class="fs-12 ml-2" id="progress_text">(0)</div>
                        </div>
                    </div>
                    <ul class="nav nav-pills mb-3 mt-5 fs-12 justify-content-center tabs-rating" id="pills-tab"
                        role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="student-feedback-tab" data-toggle="pill"
                                href="#student-feedback" role="tab" aria-controls="student-feedback"
                                aria-selected="true">Student Feedback</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="student-comment-tab" data-toggle="pill" href="#student-comment"
                                role="tab" aria-controls="student-comment" aria-selected="false">Student Comment</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="student-feedback" role="tabpanel"
                            aria-labelledby="student-feedback-tab">
                            <div>
                                <div class="mt-3">
                                    <?php if($logged_in): ?>
                                        <input type="hidden" name="feedback_id" id="rating_feedback_id">
                                        <input type="text" class="form-control" placeholder="Write Feedback" id="rating_feedback">
                                        <div class=" d-flex align-items-center mt-3 justify-content-center">
                                            <div class="star-rating mr-3">
                                                <span class="fas fa-star" data-rating="1"></span>
                                                <span class="fas fa-star" data-rating="2"></span>
                                                <span class="fas fa-star" data-rating="3"></span>
                                                <span class="fas fa-star" data-rating="4"></span>
                                                <span class="fas fa-star" data-rating="5"></span>
                                                <input type="hidden" name="rating" class="rating-value">
                                            </div>
                                            <button type="button" id="btn_rating_submit" class="btn btn-primary-red" disabled>Rate Course</button>
                                        </div>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-primary-red w-100" id="btn_login_first">Login To Give Feedback</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="student-comment" role="tabpanel"
                            aria-labelledby="student-comment-tab">
                            <div>
                                <?php if($logged_in): ?>
                                    <input type="text" class="form-control mb-3" placeholder="Write Comment" id="comment_input" data-parent-id>
                                <?php else: ?>
                                    <button type="button" class="btn btn-primary-red w-100" id="btn_login_first">Login To Give Comment</button>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('fe/templates/footer')?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        //exam
        var examQuestionIndex = 0;
        var questions = [];
        $("#prev_question").on('click',function(){
            if(examQuestionIndex > 0){
                examQuestionIndex -= 1
            }
            loadQuestion()
        })

        $("#next_question").on('click',function(){
            if(examQuestionIndex + 2 <= questions.length){
                examQuestionIndex += 1
            }
            loadQuestion()
        })

        function loadExam(exam){
            examQuestionIndex = 0;
            $("#quiz_title").html(exam.name)
            questions = exam.questions
            loadQuestion()
        }

        function loadQuestion(){
            if(questions.length == 0 || questions.length <= examQuestionIndex){
                return
            }
            var question = questions[examQuestionIndex]
            $("#quiz_count").html("Question "+(examQuestionIndex+1)+" of "+questions.length)
            $("#quiz_question").html(question.question_text)
            $("#quiz_answer_container").html("")
            $(".quiz_answer").removeClass('active-answer')
            for (var i = 0; i < question.answer.length; i++) {
                var answer = question.answer[i];
                var templateClassName = "quiz_answer_1_template"

                if(i%2 == 0){
                    templateClassName = "quiz_answer_2_template"
                }

                var template = $("."+templateClassName).clone()
                template.removeClass(templateClassName)
                template.css("display","block")
                template.find(".quiz_answer").html(answer.answer)
                template.find(".quiz_answer").off('click').on('click',function(){
                    $(".quiz_answer").removeClass('active-answer')
                    $(this).addClass('active-answer')
                })
                $("#quiz_answer_container").append(template)
            }

        }

        function getExam(contentId){
            $("#loading_overlay").show()
            $(".quiz_component").addClass("dn-important")
            $(".quiz_lock").addClass("dn-important")
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url("Course/get_content");?>',
                data: {content_id:contentId},
                dataType: "json",
                success: function (response, status) {
                    if(response.status && response.data != false){
                       var exam = JSON.parse(response.data.content_json);
                       loadExam(exam)
                       $(".quiz_component").removeClass("dn-important")
                    }else if(response.code == 403){
                      $(".quiz_lock").removeClass("dn-important")
                    }else{
                        swal.fire({text:"Cannot get content at this moment, please try again later",confirmButtonColor:"darkred"})
                    }
                    $("#loading_overlay").hide()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                }
            });
        }

        $( "#exam_topic" ).change(function() {
            var selectedSectionId = $(this).children("option:selected").val();
            $(".exam_quiz").css("display","none");
            $("#exam_quiz_"+selectedSectionId).css("display","block");
            var selectedId = $("#exam_quiz_"+selectedSectionId).children("option:selected").val();
            getExam(selectedId);
        });
        $( ".exam_quiz" ).change(function() {
            var selectedId = $(this).children("option:selected").val();
            getExam(selectedId);
        });

        examVar = '<?= json_encode($exams) ?>';
        if(examVar) $( "#exam_topic" ).trigger("change")
        //videos
        $( "#course_video_filter" ).change(function() {
            var selectedSectionId = $(this).children("option:selected").val();
            $(".video_pane").css("display","none");
            $(".video_pane[data-section='"+selectedSectionId+"']").css("display","block");
        });
        
        $('#video_modal').on('hidden.bs.modal', function () {
            $("#video_container").get(0).pause();
            $('#video_container source').attr('src', "");
        })

        $('.video_play_btn').click(function () {
            var videoSrc = $(this).data("url");
            $('#video_container source').attr('src', videoSrc);
            $("#video_container").get(0).load();
            $("#video_container").get(0).play();

            $("#video_modal").modal('show');
        });

        userId = "<?php echo !empty($user)?$user->id :0; ?>";
        courseId = "<?php echo $course->id; ?>"

        // feedback
        $('#btn_rating_submit').on('click', () => {
            feedback_id = $('#rating_feedback_id').val()
            feedback = $('#rating_feedback').val()
            star = $('.rating-value').val()
            $("#loading_overlay").show()
            $.ajax({
                type: 'POST',
                url: "<?= base_url("/Course/save_feedback"); ?>",
                data: {
                    feedback_id: feedback_id,
                    feedback: feedback,
                    rating: star,
                    user_id: userId,
                    course_id: courseId
                },
                dataType: "json",
                success: function (data, status) {
                    $("#loading_overlay").hide()
                    if (data.status == true) {
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                        loadFeedback()
                    } else {
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            });
        })

        var loadOverall = overall => {
            $('#course-rating>#course_overall').text(overall)
            
            stars = $('#course-rating>.fa-star')
            for (let index = 0; index < stars.length; index++) {
                index + 1 > Math.floor(overall) ? $(stars[index]).removeClass('checked') : ''
            }
        }

        var loadStarProgress = progresses => {
            total = 0
            $.each(progresses, function(){
                total += this
            })
            
            for (let index = 1; index <= Object.keys(progresses).length; index++) {
                let progressContainer = $(`#${index}-star`)
                
                let progress = (progresses[index] / total) * 100;
                // progress = isFinite(progress) ? progress : 0
                progressContainer.find('.progress-bar').css('width', `${progress}%`)

                progressContainer.children('#progress_text').text(`(${progresses[index]})`)
            }
        }

        var loadStudentFeedback = feedbacks => {
            var studentFeedback = $('#student-feedback')
            studentFeedback.children('#feedback_container').remove()
            $.each(feedbacks, function(){
                if(this.user_id == userId){
                    $('#rating_feedback_id').val(this.id)
                    $('#rating_feedback').val(this.feedback).trigger('keyup')
                    $('.rating-value').val(this.rating).trigger('change')

                    var star_rating = $('.star-rating .fa-star');
                    star_rating.each(function () {
                        if (parseInt(star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                            return $(this).removeClass('fas-star').addClass('checked');
                        } else {
                            return $(this).removeClass('checked');
                        }
                    })
                }

                let container = $('<div class="mt-4" id="feedback_container"><div class="d-flex"></div></div>')
                let profileContainer = $(`<div class="feedback-avatar"></div>`)
                let feedbackContainer = $(`<div class="d-grid ml-3"><div class="f-15">${this.full_name}</div></div>`)

                let starsContainer = $('<div class="d-flex align-items-center"></div>')
                for (let index = 1; index <= 5; index++) {
                    checked = index <= this.rating ? 'checked' : ''
                    starsContainer.append(`<span class="fa fa-star fs-12 ${checked}"></span>`)
                }
                starsContainer.append(`<div class="fs-12 ml-2 color-gray">${formatDate(this.created_date)}</div>`)

                let feedback = $(`<div class="fs-12 mt-2">${this.feedback}</div>`)

                feedbackContainer.append(starsContainer).append(feedback)
                container.children().append(profileContainer).append(feedbackContainer)
                studentFeedback.append(container)
            })
        }

        var loadFeedback = () => {
            $("#loading_overlay").show()
            $.ajax({
                type: 'GET',
                url: "<?= base_url("/Course/load_feedback"); ?>",
                data: {
                    course_id: courseId
                },
                dataType: "json",
                success: function (data, status) {
                    $("#loading_overlay").hide()
                    if (data.status == true) {
                        loadOverall(data.overall)
                        loadStarProgress(data.starsCount)
                        loadStudentFeedback(data.feedbacks)
                    } else {
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            });
        }

        // comment
        $('#student-comment').delegate('#comment_input', 'keypress', function(e){
            if(e.which == 13){
                parent_id = $(this).data('parent-id')
                comment = $(this).val()
                $("#loading_overlay").show()
                $.ajax({
                    type: 'POST',
                    url: "<?= base_url("/Course/save_comment"); ?>",
                    data: {
                        parent_id: parent_id,
                        comment: comment,
                        user_id: userId,
                        course_id: courseId
                    },
                    dataType: "json",
                    success: function (data, status) {
                        $("#loading_overlay").hide()
                        if (data.status == true) {
                            loadComment()
                            swal.fire({text:data.message,confirmButtonColor:"darkred"})
                            $('#comment_input').val(null)
                        } else {
                            swal.fire({text:data.message,confirmButtonColor:"darkred"})
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#loading_overlay").hide()
                        swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                    }
                });
            }
        })

        $('#student-comment').delegate('button#btn_comment_reply', 'click', function(){
            if(userId){
                reply = $(this).data('reply')
                if(!reply){
                    $(this).data('reply', 'reply')
                    parentId = $(this).data('parent-id')
                    $(this).after(`<input type="text" class="form-control mb-3" placeholder="Write Comment" id="comment_input" data-parent-id="${parentId}">`)
                }else{
                    $(this).data('reply', '')
                    $(this).next('#comment_input').remove()
                }
            }
        })

        var loadStudentComment = comments => {
            var studentComment = $('#student-comment')
            studentComment.children('#comment_container').remove()

            $.each(comments, function(){
                let self = this
                let container = $('<div class="mt-4" id="comment_container"><div class="d-flex"></div></div>')
                let profileContainer = $(`<div class="feedback-avatar"</div>`)
                let commentContainer = $(`<div class="d-grid ml-3 w-100"></div>`)
                commentContainer.append(`<div class="f-15">${self.full_name}</div>`)
                commentContainer.append(`<div class="fs-12 color-gray">${ formatDate(self.created_date) }</div>`)
                commentContainer.append(`<div class="fs-12 mt-2">${self.comment}</div>`)
                commentContainer.append(`<button class="btn-link-gray mt-2" id="btn_comment_reply" data-parent-id="${self.id}" data-reply="false"><i class="fas fa-reply mr-2"></i>Reply</button>`)
    
                container.children().append(profileContainer).append(commentContainer)
                studentComment.append(container)

                $.each(self.replies, function(){
                    let container = $('<div class="mt-4 ml-5" id="comment_container"><div class="d-flex"></div></div>')
                    let profileContainer = $(`<div class="feedback-avatar"></div>`)
                    let replyContainer = $(`<div class="d-grid ml-3 w-100"></div>`)
                    replyContainer.append(`<div class="f-15">${this.full_name}</div>`)
                    replyContainer.append(`<div class="fs-12 color-gray">${formatDate(this.created_date)}</div>`)
                    replyContainer.append(`<div class="fs-12 mt-2">${this.comment}</div>`)
        
                    container.children().append(profileContainer).append(replyContainer)
                    studentComment.append(container)
                })
            })
        }

        var loadComment = () => {
            $("#loading_overlay").show()
            $.ajax({
                type: 'GET',
                url: "<?= base_url("/Course/load_comments"); ?>",
                data: {
                    course_id: courseId
                },
                dataType: "json",
                success: function (data, status) {
                    $("#loading_overlay").hide()
                    if (data.status == true) {
                        loadStudentComment(data.comments)
                    } else {
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            });
        }

         // format date to human readable
        function formatDate(date) {
            const bulan = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
            let newDate = new Date(date)
            let formattedDate = newDate.getDate() + ' ' + bulan[newDate.getMonth()] + ' ' + newDate.getFullYear()
            return formattedDate
        }

        loadFeedback()
        loadComment()
        function addToCart(isBuyNow){
            $("#loading_overlay").show()
            $.ajax({
                type: 'GET',
                url: "<?= base_url("/Cart/add"); ?>",
                data: {
                    course_id: courseId
                },
                dataType: "json",
                success: function (data, status) {
                    $("#loading_overlay").hide()
                    if (data.status == true) {
                        if(isBuyNow){
                            window.location.href = "<?php echo base_url('cart');?>"
                        }else{
                            swal.fire({text:data.message,confirmButtonColor:"darkred"})
                        }
                    } else {
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                        // if(isBuyNow){
                        //     window.location.href = "<?php echo base_url('cart');?>"
                        // }else{
                        //     swal.fire({text:data.message,confirmButtonColor:"darkred"})
                        // }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            });
        }
        var isLoggedIn = <?php echo $logged_in ? 'true' : 'false' ?>;
        $(".btn_add_to_cart").on("click",function(){
            if(isLoggedIn){
                addToCart(false);
            }else{
                window.location.href = "<?php echo current_url().'?redir='.urlencode(base_url(uri_string()));?>"
            }
        }) 
        $(".btn_buy_now").on("click",function(){
            if(isLoggedIn){
                addToCart(true);
            }else{
                window.location.href = "<?php echo current_url().'?redir='.urlencode(base_url(uri_string()));?>"
            }
        })

        $('button#btn_login_first').click(function(){
            window.location.href = "<?php echo current_url().'?redir='.urlencode(base_url(uri_string()));?>"
        })
    })
</script>
<!-- End Container !-->