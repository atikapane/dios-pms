<?php $this->load->view('fe/templates/header') ?>
<style>
  .body {
    background:white;
  }
  .header-package{
    background: rgb(159,21,34);
    background: linear-gradient(90deg, rgba(159,21,34,1) 0%, rgba(197,90,17,1) 100%);
    color:white;
  }
  .course-package-title {
    border-bottom:1px solid white;
    padding:0px 0px 20px 0px;
    margin-bottom:20px;
    font-weight: bold;
  }
  .package-navigation {
    width:100%;
    display: block;
  }
  .package-navigation a{
    float:left;
    clear: both;
    color:darkred;
    padding:10px 10px 10px 0px;
    text-decoration: none;
    font-size:20px;
    cursor:pointer;
  }
  .package-navigation a:hover{
    font-weight: bold;
  }
  .btn-outline-white:hover{
    background-color:white;
    color:darkred;
  }
  .btn-white {
    background-color:white;
    color:darkred;
    border-color: #fff;
    border-radius: 0;
    height: 30px;
    padding: 0;
    font-size: 12px;
    padding-left: 10px;
    padding-right: 10px;
  }
  .btn-white:hover{
    background-color:darkred;
    border-color:darkred;
    color:white;
  }
  .course-item-package{
    float:left;
  }
  .course-container {
    width:100%;
    overflow-y:scroll;
    overflow-y:hidden;
  }
  .card-lecture-content{
    text-align:center;
    position: relative;
  }

  .card-lecture-content .avatar{
    margin:auto;
    margin-bottom:80px;
  }

  .card-lecture-content p {
    text-align:center;
    margin:0px;
  }

  .card-lecture-content .arrow-right {
    width: 0; 
    height: 0; 
    border-top: 20px solid transparent;
    border-bottom: 20px solid transparent;    
    border-left: 20px solid darkred;
    position: absolute;
    left:0;
    top:10px;
  }

  .card-lecture-content .arrow-left {
    width: 0; 
    height: 0; 
    border-top: 50px solid transparent;
    border-bottom: 50px solid transparent;     
    border-right:50px solid darkred; 
    position: absolute;
    right:0;
    top:70px;
  }
  .faq-box{
    border:1px solid #ccc;
    padding:20px;
  }
  .faq-item .question{
    font-weight: bold;
  }
  .action-button {
    display:flex;
  justify-content:space-around;
  }

  .action-button button {
    width: 100%;
    margin: 5px; /* or whatever you like */
  }
</style>

<div class="position-relative">
  <div class="header-package pt-4 pb-4">
    <div class="container pt-4">
        <div class="row">
            <div class="col col-8 col-sm-12 col-md-8">
              <h1 class="course-package-title">
                <?= isset($package) ? $package->ocw_name : '-' ?>
              </h1>
              <p>
              <?= isset($package) ? $package->description : '-' ?>
              </p>
                     
            </div>
            <div class="col col-4 col-sm-12 col-md-4 align-self-start">
              <img src="<?php echo $this->config->item("ASSET_PATH");?>media/img/logo-telkom.png" style="width:180px;float:right">
            </div>
        </div>     
        <div class="row pt-2 pb-2">
            <div class="col col-8 col-sm-12 col-md-8">
              <div class="d-flex align-items-center fs-30">
                  <i class="fas fa-tags flip fs-30 mr-3"></i>
                  <?php $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY) ?>
                  <?= isset($package) ? $fmt->format($package->price) : '-' ?>
              </div>       
            </div>
            <div class="col col-4 col-sm-12 col-md-4">
              <div class="action-button">
                <button class="btn btn-outline-white" id="btn_buy_now">BUY NOW</button>
                <button class="btn btn-white" id="btn_add_to_cart">ADD TO CART</button>
              </div>
            </div>
        </div>      
    </div>
  </div>
</div>

<div class="container mt-4 mb-4">
  <div class="row">
    <div class="col col-4 col-sm-12 col-md-4">
      <div class="package-navigation">
        <a href="#">About</a>
        <a href="#">How It Works</a>
        <a href="#">Course</a>
        <a href="#">Instructor</a>
        <a href="#">Enrollment Option</a>
        <a href="#">F.A.Q</a>
      </div>
    </div>
    <div class="col col-8 col-sm-12 col-md-8">
      <h3>About</h3>
      <?= isset($package) ? $package->about : '-' ?>
      <h3>How It Works</h3>
      <?= isset($package) ? $package->how_it_works : '-' ?>
      <h3>Course</h3>
      <div class="row">
          <?php if(isset($package)): ?>
            <?php foreach($package->courses as $course): ?>
              <div class="course-item-package col col-4">
                <div class="card mb-5">
                  <img id="course_image" class="img-responsive h-img-200" src="<?= base_url($course->banner_image) ?>">
                  <div class="card-body">
                    <div class="card-subtitle mb-2 d-flex">
                      <i class="fa fa-calendar-alt"></i>
                      <div class="fs-12 color-gray ml-2" id="course_date"><?= $course->start_date ?></div>
                    </div>
                    <h5 class="card-title" id="course_title"><?= $course->ocw_name ?></h5>
                    <p class="fs-12 elipsis lc-5" id="course_desc"><?= strip_tags($course->description) ?></p>
                    <?php $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY); ?>
                    <p class="fs-10 text-right mb-0 secret"><del id="course_real_price"><?= $fmt->format((100*$course->price/(100-$course->discount))) ?></del></p>
                    <h5 class="card-title text-right secret"><i class="fa fa-tags mr-1"></i><span id="course_price"><?= $fmt->format($course->price) ?></span></h5>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
      </div>
      <h3>Instructor</h3>
        <div class="row">
          <?php if(isset($package)): ?>
            <?php foreach($package->courses as $course): ?>
              <div class="course-item-package col col-3">
                <div class="card mb-5">
                  <div class="card-body card-lecture-content">
                    <div class="arrow-right"></div>
                    <div class="avatar" style="background: url(<?= base_url($course->lecturer_photo) ?>);background-size: cover;background-repeat: no-repeat; background-position: center;"></div>      
                    <div class="arrow-left"></div>  
                    <p><b>Lecturer</b></p>
                    <p><?= $course->lecturer_name ?></p>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
        </div>
      <h3>Enrollment Option</h3>
      <?= isset($package) ? $package->enrollment_options : '-' ?>
      <h3>F.A.Q</h3>
      <?php if(isset($package)): ?>
        <div class="faq-box">
          <?php foreach($package->faq as $faq): ?>
            <div class="faq-item">
              <p class="question"><?= $faq->question ?></p>
              <p class="answer"><?= $faq->answer ?></p>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php $this->load->view('fe/templates/footer') ?>
<script>
    var isLoggedIn = '<?= $logged_in ? 'true' : 'false' ?>';
    $("#btn_add_to_cart").on("click",function(){
        if(isLoggedIn){
            addToCart(false);
        }else{
            window.location.href = "<?php echo current_url().'?redir='.urlencode(base_url(uri_string()));?>"
        }
    }) 
    $("#btn_buy_now").on("click",function(){
        if(isLoggedIn){
            addToCart(true);
        }else{
            window.location.href = "<?php echo current_url().'?redir='.urlencode(base_url(uri_string()));?>"
        }
    })
    packageId = "<?= isset($package) ? $package->id : ''; ?>";
    function addToCart(isBuyNow){
      if(packageId){
        $("#loading_overlay").show()
        $.ajax({
            type: 'GET',
            url: "<?= base_url("/Cart/add"); ?>",
            data: {
                course_id: packageId
            },
            dataType: "json",
            success: function (data, status) {
                $("#loading_overlay").hide()
                if (data.status == true) {
                    if(isBuyNow){
                        window.location.href = "<?php echo base_url('cart');?>"
                    }else{
                        swal.fire({text:data.message,confirmButtonColor:"darkred"})
                    }
                } else {
                    swal.fire({text:data.message,confirmButtonColor:"darkred"})
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#loading_overlay").hide()
                swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
            }
        });
      }
    }
</script>