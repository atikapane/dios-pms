<?php $this->load->view('fe/templates/header') ?>
<style>
  .card {
      box-shadow: 0 6px 20px rgba(17, 26, 104, .2);
  }

  img {
    min-height: initial !important;
  }

  

</style>
<div class="bg-header-course"></div>

<!-- Shape !-->
<div class="pentagon-shape"></div>
<!-- End Shape !-->

<!-- Search Course !-->
<div class="search-course-wrapper">
  <div class="search-course-input-wrapper container">
    <input class="search-course-input" placeholder="Search Course" id="search_query">
    <div class="search-icon">
      <i class="fas fa-search" id="search_btn"></i>
    </div>
  </div>
</div>
<!-- End Search Course !-->

<!-- Container !-->
<div class="container mt-3">
  <!-- 3 Grid Column !-->
  <div class="owl-carousel owl-theme">
     <div class="card mb-3 h-170 category_card" data-id="0">
      <img src="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png" width="135" class="img-responsive pt-4 d-block m-auto">
      <div class="card-body p-0 pt-2 pb-2">
        <h5 class="card-title text-center">All Category</h5>
      </div>
    </div>
    <?php foreach($category_list as $category){?>
	    <div class="card mb-3 h-170 category_card" data-id="<?php echo $category->faculty_id?>">
	      <div class="card-body p-0 pt-2 pb-2">
	        <div class="fs-12 ml-3 pl-3 pr-3 elipsis">
	          <div class="d-grid mt-3">
	            <h5 class="card-title  color-pale font-weight-bold"><?php echo $category->name?></h5>
	          </div>
	        </div>
	        <div class="fs-12 ml-3 p-3 elipsis">
	          <?php echo $category->description?>
	        </div>
	      </div>
	    </div>
	<?php } ?>
  </div>
  <!-- End Grid !-->

  <!-- 3 Grid Columns !-->
  <div class="caret mt-5">
    <div class="caret-title" id="filter_title">All Course</div>
    <div class="ml-auto">
      <select class="form-control" id="program_course_filter">
        <option value="0">All Program</option>
      </select>
    </div>
  </div>
  <div class="row mt-4" id="main_container">
   

  </div>
  <!-- End Grid !-->
</div>

<div class="col-md-4 template" style="display: none">
	<div class="card mb-5">
		<img id="course_image" class="img-responsive h-img-200">
		<div class="card-body">
			<div class="card-subtitle mb-2 d-flex">
				<i class="fa fa-calendar-alt"></i>
				<div class="fs-12 color-gray ml-2" id="course_date"></div>
			</div>
			<h5 class="card-title" id="course_title"></h5>
			<p class="fs-12 elipsis lc-5" id="course_desc"></p>
      <p class="fs-10 text-right mb-0 secret"><del id="course_real_price"></del></p>
      <h5 class="card-title text-right secret"><i class="fa fa-tags mr-1"></i><span id="course_price"><span></h5>
		</div>
	</div>
</div>
<script type="text/javascript">
    var getProgramURL = "<?php echo base_url().'Course/get_program_study';?>"
    var getCourseURL = "<?php echo base_url().'Course/load_course';?>"
    var baseURL = "<?php echo base_url()?>"
    var viewURL = "<?php echo base_url('Course/view')?>/"
    var packageURL = "<?php echo base_url('Course/package')?>/"
    var catId = <?php echo !empty($selected_category) ? $selected_category->faculty_id : 0;?>;
    var catName = "<?php echo !empty($selected_category) ? $selected_category->name : '';?>"
</script>

<!-- End Container !-->
<?php $this->load->view('fe/templates/footer') ?>
<script>
    $(document).ready(function(){
      $('#search_query').val('<?= $search ?>')
    })
</script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/js/course_list/script.js"></script>