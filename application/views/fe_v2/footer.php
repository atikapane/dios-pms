</body>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
            container: 'body',
            html: true,
            placement: 'bottom',
            sanitize: false,
            content: `
            <div id="PopoverContent">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari disini..."
                        aria-label="Cari disini..." aria-describedby="button-addon1">
                    <div class="input-group-append" id="button-addon1">
                    <button class="btn btn-outline-red" type="button" data-toggle="popover" data-placement="bottom"
                        data-html="true" data-title="Search">
                        <i class="fas fa-search"></i>
                    </button>
                    </div>
                </div>
            </div>`
        });
    });
</script>

</html>