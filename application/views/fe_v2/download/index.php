<?php $this->load->view('fe/templates/header') ?>
<style>
    .card {
        box-shadow: 0 6px 20px rgba(17, 26, 104, .2);
    }
    .card-body {
        padding:0px;
    }
    .download_btn{
        background-color: darkred;
        border-top-right-radius:0 !important;
        border-bottom-right-radius:0 !important;
        padding:1rem;
    }
</style>
<div class="bg-header-download"></div>
<!-- Search Course !-->
<div class="container">
    <div class="search-course-wrapper-white">
        <div class="search-course-input-wrapper">
            <input class="search-course-input-white" placeholder="Search Download" id="search_query">
            <div class="search-icon-white" id="search_btn">
                <i class="fas fa-search"></i>
            </div>
        </div>
    </div>
</div>
<!-- End Search Course !-->

<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->

<!-- Container !-->
<div class="container mt-3">
    <!-- 3 Grid Columns !-->
    <div class="caret mt-5">
        <div class="caret-title">Download</div>
    </div>
    <div class="row mt-4" id="main_container">
        
    </div>
    <!-- End Grid !-->
</div>

<div class="col-md-12 download_template" style="display: none">
    <div class="card mb-5">
        <div class="card-body">
            <div class="d-flex align-items-center">
                <div class="card-download">
                    <button class="btn-link btn w-100 d-grid download_btn">
                        <i class="fas fa-download color-gray"></i><br>
                        <span>Download</span>
                    </button>
                </div>
                <div class="card-download-border ml-3"></div>
                <div class="d-grid p-10 ml-3">
                    <h5 class="card-download-title title">
                    </h5>
                    <h6 class="card-download-title description">
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Container !-->
<script type="text/javascript">
    var downloadURL = "<?php echo base_url('Download/get'); ?>";
</script>
<?php $this->load->view('fe/templates/footer') ?>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/js/download/script.js"></script>