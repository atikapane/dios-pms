<?php $this->load->view('fe/templates/header') ?>
<style>
    .card {
        box-shadow: 0 6px 20px rgba(17, 26, 104, .2);
    }
    .news_tag{
        display: table;
    }
    .news_tag a {
        float:left;
    }
</style>
<div class="bg-header-news"></div>
<!-- Search Course !-->
<div class="container">
    <div class="search-course-wrapper-white">
        <div class="search-course-input-wrapper">
            <input class="search-course-input-white" placeholder="Search News" id="search_query">
            <div class="search-icon-white" id="search_btn">
                <i class="fas fa-search"></i>
            </div>
        </div>
    </div>
</div>
<!-- End Search Course !-->

<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->

<!-- Container !-->
<div class="container mt-3">
    <!-- 3 Grid Columns !-->
    <div class="caret mt-5">
        <div class="caret-title">Recent Event</div>
    </div>
    <div class="row mt-4" id="main_container">
    </div>
    <!-- End Grid !-->
</div>
<div class="col-md-12 template" style="display: none">
    <div class="card mb-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-3 news_thumbnail" style="background-repeat:no-repeat;background-size:cover;background-position:center;">
                </div>
                <div class="col-md-9">
                    <div class="d-grid p-10 ml-3 mb-auto mt-4  ">
                        <div class="card-subtitle mb-2 d-flex">
                            <i class="fas fa-calendar-alt"></i>
                            <div class="fs-12 color-gray ml-2 news_date"></div>
                        </div>
                        <h5 class="card-title news_title">
                        </h5>
                        <p class="fs-12 pr-3 news_content">
                           

                        </p>
                        <div class="mt-auto fs-12 font-italic mb-3 news_tag">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="#" class="color-gray mr-3 tag_template" style="display: none"></a>
<!-- End Container !-->
<script type="text/javascript">
    var getURL = "<?php echo base_url('News/get'); ?>";
</script>
<?php $this->load->view('fe/templates/footer') ?>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/js/news/script.js"></script>