<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
	
	<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">		
		
		<ul class="kt-menu__nav ">
			<li class="kt-menu__item  " aria-haspopup="true" ><a  href="<?php echo base_url('dashboard'); ?>" class="kt-menu__link "><span class="kt-menu__link-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"/>
    </g>
</svg></span>
<span class="kt-menu__link-text">Dashboard</span></a></li>

<?php 
$user_id = get_logged_in_id();
$menu = $this->Previlege_Model->get_allowed_menu($user_id);

$current_menu_id = 0;
$current_submenu_id = 0;
if(!empty($breadcrumb)){
    $current_menu_id = $breadcrumb->parent_id;
    $current_submenu_id = $breadcrumb->child_id;
}
foreach($menu as $row)
{
    $additional_class = "";
    if($row->menu_id == $current_menu_id){
        $additional_class = "kt-menu__item--open";
    }
    echo '<li class="kt-menu__item  kt-menu__item--submenu '.$additional_class.'" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
            <a href="javascript:;" class="kt-menu__link kt-menu__toggle ">
                <span class="kt-menu__link-icon">
                    <i class="fa '.$row->menu_icon.'"></i>
                </span>
                <span class="kt-menu__link-text">'.$row->menu_name.'</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>';
    $submenu = $this->Previlege_Model->get_allowed_submenu($user_id,$row->menu_id);
    if($submenu !=null)
    {
        echo '<div class="kt-menu__submenu ">
              <span class="kt-menu__arrow"></span>
              <ul class="kt-menu__subnav">';
        foreach( $submenu as $rowdata)
        {
            $additional_class = "";
            if($rowdata->submenu_id == $current_submenu_id){
                $additional_class = "kt-menu__item--active";
            }
            echo '<li class="kt-menu__item '.$additional_class.'" aria-haspopup="true" >
                        <a  href="'.base_url($rowdata->submenu_link).'" class="kt-menu__link ">
                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                        <span class="kt-menu__link-text">'.$rowdata->submenu_name.'</span></a>
                  </li>';
        }
        echo '</ul></div>';
    }
    echo '</li>';
}
?>

</ul>
</div>
</div>
<!-- end:: Aside Menu -->				
</div>
<!-- end:: Aside -->
			
<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " >
			
<!-- begin:: Header Menu -->
<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
	
	<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default "  >
		<ul class="kt-menu__nav ">
			<li class="kt-menu__item  kt-menu__item--open kt-menu__item--here kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open kt-menu__item--here kt-menu__item--active"  data-ktmenu-submenu-toggle="click" aria-haspopup="true">
            <!-- <a  href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <span class="kt-menu__link-text">Pages</span>
            </a> -->
            </li>		
        </ul>

	</div>
</div>
<!-- end:: Header Menu -->

<!-- begin:: Header Topbar -->
<div class="kt-header__topbar">


<!--begin: User Bar -->
<?php $identity = get_login_info(); ?>
<div class="kt-header__topbar-item kt-header__topbar-item--user">    
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
            <div class="kt-header__topbar-user">
            <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
            <span class="kt-header__topbar-username kt-hidden-mobile"><?php echo $identity["fullname"]; ?></span>
            <img class="kt-hidden" alt="Pic" src="<?php echo base_url(); ?>assets/media/users/300_25.jpg" />
            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?php echo substr($identity["fullname"],0,1); ?></span>
        </div>
    </div>

        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
        <!--begin: Head -->
    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?php echo base_url(); ?>assets/media/misc/bg-1.jpg)">
        <div class="kt-user-card__avatar">
            <img class="kt-hidden" alt="Pic" src="<?php echo base_url(); ?>assets/media/users/300_25.jpg" />
            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
            <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?php echo substr($identity["fullname"],0,1); ?></span>
        </div>
        <div class="kt-user-card__name">
            <?php echo $identity["employeeid"]. "<br>" .$identity["fullname"]; ?>
        </div>        
    </div>
<!--end: Head -->

<!--begin: Navigation -->
<div class="kt-notification">
    <a href="<?php echo base_url('dashboard'); ?>" class="kt-notification__item">
        <div class="kt-notification__item-icon">
            <i class="flaticon2-calendar-3 kt-font-success"></i>
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                My Profile
            </div>
            <div class="kt-notification__item-time">
                Account settings and more
            </div>
        </div>
    </a>
    <div class="kt-notification__custom kt-space-between">
        <a href="<?php echo base_url('auth\signout'); ?>" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
    </div>
</div>
<!--end: Navigation -->
</div>
</div>
<!--end: User Bar -->	
</div>
<!-- end:: Header Topbar -->
</div>
<!-- end:: Header -->