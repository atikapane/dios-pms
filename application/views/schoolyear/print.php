<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b>Id</b></th>
            <th scope="col"><b>Schoolyear</b></th>
            <th scope="col"><b>Semester</b></th>
            <th scope="col"><b>Description</b></th>
            <th scope="col"><b>Status</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            echo '<tr>
            <td>'.$item->id.'</td>
            <td>'.$item->year.'</td>
            <td>'.($item->semester == 1 ? "Odd" : "Even").'</td>
            <td>'.$item->description.'</td>
            <td>'.($item->status == 1 ? "Active" : "Inactive").'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>