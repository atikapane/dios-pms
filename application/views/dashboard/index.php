<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">								
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title"><?php echo $breadcrumb->parent;?></h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc"><?php echo $breadcrumb->child;?></span>           
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                    <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                    </span>
                </div>
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->					

     <!-- begin:: Content -->
     <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--Begin::Dashboard 1-->

        <?php $identity = get_login_info(); ?>
        <h3>WELCOME <?= $identity['fullname'] ?></h3>
        <p>PLease use menu to the left to navigate</p>

        <!--End::Dashboard 1-->	
    </div>
    <!-- end:: Content -->
    
</div>

<?php
$this->load->view('templates/Footer.php');
?>

<!--begin::Modal-->
<form name="fmadd" class="form-horizontal" method="post" action="<?php echo base_url('users/save'); ?>">
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group">
						<label>Fullname</label>
						<input type="text" class="form-control" id="fullname" name="fullname" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Telp.</label>
						<input type="text" class="form-control" id="telp" name="telp" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Address</label>
						<input type="text" class="form-control" id="address" name="address" placeholder="" >
					</div>
                </div>                
                
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<!--end::Modal-->

<!--begin::Modal-->
<form name="fmedit" class="form-horizontal" method="post" action="<?php echo base_url('users/save'); ?>">
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                
                <div class="col-md-6">
                    <div class="form-group">
						<label>Fullname</label>
						<input type="text" class="form-control" id="fullname" name="fullname" placeholder="" required>
						<input type="hidden" class="form-control" id="uid" name="uid" placeholder="">
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Telp.</label>
						<input type="text" class="form-control" id="telp" name="telp" placeholder="" required>
					</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
						<label>Address</label>
						<input type="text" class="form-control" id="address" name="address" placeholder="" >
					</div>
                </div>                
                
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<!--end::Modal-->

<script type="text/javascript">
 function deletechecked()
    {
        if(confirm(" Are you sure delete this data?"))
        {
            return true;
        }
        else
        {
            return false;  
        } 
   }  
</script>

<script type="text/javascript">
  $(document).on("click",".open-edit",function(){
      var uid          = $(this).data("uid");
      var fullname          = $(this).data("fullname");       
      var email          = $(this).data("email");       
      var telp          = $(this).data("telp");       
      var address          = $(this).data("address");       
            
      //alert(id); 
      $(".modal-body #uid").val(uid);
      $(".modal-body #fullname").val(fullname);                  
      $(".modal-body #email").val(email);                  
      $(".modal-body #telp").val(telp);                  
      $(".modal-body #address").val(address);                  
  });
</script>

<script>
var KTDatatableHtmlTableDemo = function() {
	var tbl = function() {
		var datatable = $('.kt-datatable').KTDatatable({ });};
	return { init: function() { tbl();},};
}();

jQuery(document).ready(function() {	KTDatatableHtmlTableDemo.init();});
</script>