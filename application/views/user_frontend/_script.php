<script>
//globals
var datatable;
var AlertUtil;
var createForm;
var editForm;
function initDTEvents(){
    $(".btn_edit").on("click",function(){
        var targetId = $(this).data("id");
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {id:targetId},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    editForm.populateForm(response.data);
                    $('#modal_edit').modal('show');
                }else{
                    AlertUtil.showFailed(response.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });
    })

    $(".btn_delete").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailed(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $(".btn_import").on("click", function(){
        $('#modal_import').modal('show')
    })
}
function initDataTable(){
    var option = {
        data: {
            type: 'remote',
            source: {
              read: {
                url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState : {cookie: false,webstorage: false},
          },
          sortable: true,
          pagination: true,
          search: {
            input: $('#generalSearch'),
          },
          columns: [
            {
                field: 'id',
                title: 'Id',
                sortable: 'asc',                          
            },
            {
                field: 'username',
                title: 'Username',
            },
            {
                field: 'email',
                title: 'Email',
            },
            {
                field: 'full_name',
                title: 'Fullname',
            },
            {
                field: 'country',
                title: 'Country',
            },
            {
                field: 'city',
                title: 'City',
            },
            {
                field: 'gender',
                title: 'Gender',
                template: function(row){
                    return row.gender == 1 ? 'Male' : row.gender == 2 ? 'Female' : 'Other/Prefer Not To Say'
                }
            },
            {
                field: 'dateBirth',
                title: 'Date Birth',
            },
            {
                field: 'phone_number',
                title: 'Phone Number',
            },
            {
                field: 'action',
                title: 'Action',
                sortable: false,
                width: 100,
                overflow: 'visible',
                textAlign: 'center',
                autoHide: false,
                template: function (row) {
                    var result ="";
                    <?php if($privilege->can_update){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" data-toggle="modal" data-target="#modal_edit"><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                    <?php }?>
                    <?php if($privilege->can_delete){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" style="cursor:pointer;"><i class="flaticon2-trash"></i></span>';
                    <?php }?>
                    return result;
                }                        
            }
          ],
          layout:{
            header:true
          }
    }
    datatable = $('#kt_datatable').KTDatatable(option);
    datatable.on("kt-datatable--on-layout-updated",function(){
        initDTEvents();
    })
}
function initCustomRule(){
    $.validator.addMethod("add_password_contain_username", function(value, element) {
        username = $("#add_username").val()
        return !(username != "" && value.includes(username))
    }, "Password cannot contain username");

    $.validator.addMethod("edit_password_contain_username", function(value, element) {
        username = $("#edit_new_username").val()
        return !(username != "" && value.includes(username))
    }, "Password cannot contain username");

    $.validator.addMethod("edit_new_password_not_empty", function(value, element){
        old_password = $("#edit_old_password").val()
        return !(value == "" && old_password != "")
    }, "New password cannot empty when update password")
    
    $.validator.addMethod("edit_old_password_not_empty", function(value, element){
        new_password = $("#edit_new_password").val()
        return !(value == "" && new_password != "")
    }, "Old password cannot empty when update password")
}
function initAlert(){
    AlertUtil = {
        showSuccess : function(message,timeout){
            $("#success_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#success_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#success_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#success_alert_dismiss").trigger("click");
        },
        showFailed : function(message,timeout){
            $("#failed_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#failed_alert").show();
            KTUtil.scrollTop();
        },
        hideFailed : function(){
            $("#failed_alert_dismiss").trigger("click");
        },
        showFailedDialogAdd : function(message,timeout){
            $("#failed_message_add").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_add").trigger("click");
                },timeout)
            }
            $("#failed_alert_add").show();
        },
        hideFailedDialogAdd : function(){
            $("#failed_alert_dismiss_add").trigger("click");
        },
        showFailedDialogEdit : function(message,timeout){
            $("#failed_message_edit").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_edit").trigger("click");
                },timeout)
            }
            $("#failed_alert_edit").show();
        },
        hideFailedDialogEdit : function(){
            $("#failed_alert_dismiss_edit").trigger("click");
        }
    }
    $("#failed_alert_dismiss").on("click",function(){
        $("#failed_alert").hide();
    })
    $("#success_alert_dismiss").on("click",function(){
        $("#success_alert").hide();
    })
    $("#failed_alert_dismiss_add").on("click",function(){
        $("#failed_alert_add").hide();
    })
    $("#failed_alert_dismiss_edit").on("click",function(){
        $("#failed_alert_edit").hide();
    })
}
function initCreateForm(){
    //validator
    var validator = $( "#form_add" ).validate({
        ignore:[],
        errorPlacement: function (error, element){
            error.addClass("invalid-feedback")
            element.addClass('is-invalid')
            if (element.hasClass('select2'))
                error.insertAfter(element.next('span'))
            else
                error.insertAfter(element)
        },
        rules: {
            // id: {
            //     required: true,
            // },
            username: {
                required: true,
                minlength:6,
                maxlength:20,
                lettersonly: true,
                pattern: /^[a-z]*$/
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 12,
                add_password_contain_username: true,
                pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/,
            },
            email: {
                required: true,
            },
            fullname: {
                required: true,
            },
            city: {
                required: true,
            },
            gender: {
                required: true,
            },
            date_birth: {
                required: true,
            },
            phone_number: {
                required: true,
                number: true,
            }
        },
        messages: {
            username: {
                pattern: "Must be lowercase"
            },
            password: {
                pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character, 1 numeric character and 1 symbol (!@$*{},.\":)"
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_add_submit").on("click",function(){
      var isValid = $( "#form_add" ).valid();
      if(isValid){
        KTApp.block('#modal_add .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/save"); ?>",
            data : $('#form_add').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_add .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_add').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogAdd(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_add .modal-content');
                AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#add_city').select2({
        placeholder: "Please select a city",
        width: '100%'
    });

    $('#add_gender').select2({
        placeholder: "Please select a gender",
        width: '100%'
    });

    $('.date-picker').datepicker({
        format: 'yyyy/mm/dd',
        endDate: '0d',
        todayHighlight: true,
        zIndexOffset: 9999,
        autoclose: true,
        clearBtn: true,
        todayBtn: true
    });

    $('#modal_add').on('hidden.bs.modal', function () {
       validator.resetForm();
       $('#add_city').val(null).trigger('change');
       $('#add_gender').val(null).trigger('change');
    })

    return {
        validator:validator
    }
}
function initEditForm(){
    //validator
    var validator = $( "#form_edit" ).validate({
        ignore:[],
        errorPlacement: function (error, element){
            error.addClass("invalid-feedback")
            element.addClass('is-invalid')
            if (element.hasClass('select2'))
                error.insertAfter(element.next('span'))
            else
                error.insertAfter(element)
        },
        rules: {
            id: {
                required: true,
            },
            username: {
                required: true,
                minlength:6,
                maxlength:20,
                lettersonly: true,
                pattern: /^[a-z]*$/
            },
            old_password: {
                minlength: 8,
                maxlength: 12,
                edit_old_password_not_empty: true,
                pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/,
            },
            new_password: {
                minlength: 8,
                maxlength: 12,
                edit_new_password_not_empty: true,
                edit_password_contain_username: true,
                pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/,
            },
            email: {
                required: true,
            },
            fullname: {
                required: true,
            },
            city: {
                required: true,
            },
            gender: {
                required: true,
            },
            date_birth: {
                required: true,
            },
            phone_number: {
                required: true,
                number: true,
            }
        },
        messages: {
            username: {
                pattern: "Must be lowercase"
            },
            old_password: {
                pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character, 1 numeric character and 1 symbol (!@$*{},.\":)"
            },
            new_password: {
                pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character, 1 numeric character and 1 symbol (!@$*{},.\":)"
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_edit_submit").on("click",function(){
      var isValid = $( "#form_edit" ).valid();
      if(isValid){
        KTApp.block('#modal_edit .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/edit"); ?>",
            data : $('#form_edit').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_edit .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_edit').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogEdit(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_edit .modal-content');
                AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#edit_city').select2({
        placeholder: "Please select a city",
        width: '100%'
    });

    $('#edit_gender').select2({
        placeholder: "Please select a gender",
        width: '100%'
    });

    $('.date-picker').datepicker({
        format: 'yyyy/mm/dd',
        endDate: '0d',
        todayHighlight: true,
        zIndexOffset: 9999,
        autoclose: true,
        clearBtn: true,
        todayBtn: true
    });

    $('#modal_edit').on('hidden.bs.modal', function () {
       validator.resetForm();
       $('#edit_city').val(null).trigger('change');
       $('#edit_gender').val(null).trigger('change');
    })

    var populateForm = function(userObject){
        console.log(userObject)
        $("#edit_id").val(userObject.id);
        $("#edit_username").val(userObject.username);
        $("#edit_email").val(userObject.email);
        $("#edit_fullname").val(userObject.full_name);
        $("#edit_fullname").val(userObject.full_name);
        $("#edit_city").val(userObject.city_id).trigger('change');
        $("#edit_gender").val(userObject.gender).trigger('change');
        $("#edit_date_birth").val(userObject.dateBirth);
        $("#edit_phone_number").val(userObject.phone_number);
    }
    
    return {
        validator:validator,
        populateForm:populateForm
    }
}

function initImportForm(){
    //validator
    var validator = $( "#form_import" ).validate({
        ignore:[],
        rules: {
            file_csv: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    $('#modal_import').on('hidden.bs.modal', function () {
       validator.resetForm();
    })

    return {
        validator:validator
    }
}

function Export(){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "User_Frontend_"+date+".csv";
        var search = $('#generalSearch').val();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: { search: search  },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }
    
    function Print(){
        <?php if($privilege->can_read){?>
            var search = $('#generalSearch').val();
            KTApp.blockPage();
            $.ajax({
                url: "<?php echo base_url($controller_full_path."/print_frontend");?>",
                type: 'post',
                dataType: 'json',
                data: { search: search  },
                success: function (response, status) {
                        KTApp.unblockPage();
                        if (response.status == true) {
                            //populate form
                            var win = window.open("", "_blank");                    
                            win.document.write(response.data);
                            win.document.close();  
                            setTimeout(function() {
                                win.print();
                                win.close();
                            }, 250);
                        } else {
                            AlertUtil.showFailed(response.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
            })    
        <?php }else{ ?>
            swal.fire({
                    title: 'Infomation',
                    text: "You cannot Print this data",
                    type: 'info',
                });
        <?php } ?>
    }

jQuery(document).ready(function() { 
    initDataTable();
    initCustomRule();
    createForm = initCreateForm();
    editForm = initEditForm();
    importForm = initImportForm();
    initAlert();
    importErr = '<?= isset($import_error) ? $import_error : '' ?>'
    if(importErr) AlertUtil.showFailed(importErr)
});

</script>
