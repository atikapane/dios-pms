<?php 
$this->load->view('templates/HeadTop.php');
$this->load->view('templates/HeadMobile.php');
$this->load->view('templates/HeadBottom.php');
$this->load->view('templates/SideBar.php');
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">                               
     <!-- begin:: Content Head -->
     <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">                
                <h3 class="kt-subheader__title"><?php echo $breadcrumb->parent;?></h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc"><?php echo $breadcrumb->child;?></span>       
            </div>
        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
            </div>
            </div>
        </div>
    </div>
    <!-- end:: Content Head -->                 

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand fa <?php echo $breadcrumb->icon;?>"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                       <?php echo $breadcrumb->child;?> Import
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">            
                        <?php if ($privilege->can_read) { ?>                           
                            <button type="button" class="btn btn-success btn-icon-sm mr-2 btn_import" disabled>
                                <i class="fas fa-file-csv"></i> Import Data      
                            </button> 
                        <?php } ?>
                    </div>      
                </div>
            </div>

        <div class="kt-portlet__body">
            <div class="col-md-pull-12" >  
                <!--begin: Alerts -->   
                <div class="kt-section">
                    <div class="kt-section__content">
                        <div class="alert alert-success fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="success_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="success_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="success_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>
                         <div class="alert alert-danger fade show kt-margin-r-20 kt-margin-l-20 kt-margin-t-20" role="alert" id="failed_alert" style="display: none">
                            <div class="alert-icon"><i class="flaticon-warning"></i></div>
                            <div class="alert-text" id="failed_message"></div>
                            <div class="alert-close">
                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>                   
                </div>        
                <!--end: Alerts -->           
            </div>
            <!--begin: Datatable -->       
            <div class="col-md-pull-12 table-responsive" >  
                <table  id="result_table" class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Username</th>
                            <th>Old Password</th>
                            <th>Password</th>
                            <th>Email</th>
                            <th>Fullname</th>
                            <th>Gender</th>
                            <th>Date Birth</th>
                            <th>Phone Number</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="table_body">
                        <tr>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--end: Datatable -->
        </div>
        </div>
    </div>
    <!-- end:: Content -->
    
</div>

<?php
    $this->load->view('templates/Footer.php');
?>
<script>
    validationErr = '<?= implode("<br>", $this->form_validation->error_array()) ?>';
    errMessage = ''
    currentData = false

    function loadTable(){
        let successCount = 0
        let errCount = 0

        $("#table_body").empty();
        var data = <?= json_encode($table) ?>;
        $.each(data, function(){
            let row = $("<tr></tr>");

            bg_color = this.error.includes('id') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.id+"</td>");

            bg_color = this.error.includes('username') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.username+"</td>");
            
            bg_color = this.error.includes('old_password') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.old_password+"</td>");

            bg_color = this.error.includes('password') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.password+"</td>");
            
            bg_color = this.error.includes('email') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.email+"</td>");

            bg_color = this.error.includes('full_name') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.full_name+"</td>");

            bg_color = this.error.includes('gender') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.gender+"</td>");

            bg_color = this.error.includes('date_birth') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.date_birth+"</td>");

            bg_color = this.error.includes('phone_number') ? 'bg-danger text-white' : ''
            row.append(`<td class="${bg_color}">`+this.phone_number+"</td>");

            if(this.error.length == 0){
                successCount++
                row.append("<td class='text-success'>OK</td>");
            }else{
                errCount++
                row.append("<td class='text-danger'>FAILED</td>");
            }

            $("#table_body").append(row);
        })

        if(data.length > 0){
            if(!errCount){
                currentData = data
                $('.btn_import').removeAttr('disabled')
                $('#success_alert').show()
                $('#success_message').html(data.length+" data read, "+successCount+" valid data, "+errCount+" invalid data.")
            }else{
                $('#failed_alert').show()
                $('#failed_message').html(data.length+" data read, "+successCount+" valid data, "+errCount+" invalid data.")
            }
        }else{
            $('#failed_alert').show()
            $('#failed_message').html('No Data In CSV')
        }
    }

    $(document).ready(function(){
        if(validationErr) {
            $('#failed_alert').show()
            $('#failed_message').html(validationErr)
        }
        loadTable()
        $('.btn_import').click(function(){
            if(currentData){
                KTApp.blockPage();
                $.ajax({
                    url : "<?php echo base_url($controller_full_path."/import"); ?>",
                    type: "post",
                    dataType: 'json',
                    data: {data:JSON.stringify(currentData)},
                    success: function(response) {
                        KTApp.unblockPage();
                        if(response.status){
                            $('#success_alert').show()
                            $('#success_message').html(response.message)
                            window.location.href = '<?= base_url($controller_full_path) ?>'
                        }else{
                            $('#failed_alert').show()
                            $('#failed_message').html(response.message)
                        }
                    },
                    error: function(e) {
                        KTApp.unblockPage();
                        $('#failed_alert').show()
                        $('#failed_message').html("Cannot reach server")
                    }
                });
            }
        })
    })
</script>




