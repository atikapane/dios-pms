<script src="<?php echo base_url(); ?>assets/vendors/general/jquery/dist/jquery-ui.js" type="text/javascript"></script>
<script>
    //globals
    var datatable;
    var AlertUtil;
    var editForm;
    var orderList

    function initDTEvents() {
        $(".edit_log").on("click", function () {
            var targetId = $(this).data("id");
            if($(this).prop('checked') == true){
                var is_log = 1;
            }else{
                var is_log = 0;
            }
            console.log(targetId)
            console.log(is_log)
            KTApp.blockPage();
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url($controller_full_path . "/edit"); ?>",
                data: {
                    id: targetId,
                    is_log: is_log
                },
                dataType: "json",
                success: function (response, status) {
                    KTApp.unblockPage();
                    if(response.status == true){
                        datatable.reload();
                        AlertUtil.showSuccess(response.message,5000);
                    }else{
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            });
        });
    }

    function initDataTable() {
        var option = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '<?php echo base_url($controller_full_path . "/datatable"); ?>',
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {cookie: false, webstorage: false},
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
            },
            columns: [
                {
                    field: 'key',
                    title: 'Log Action',
                    textAlign: 'left'
                },
                {
                    field: 'log',
                    title: 'Log',
                    sortable: false,
                    width: 80,
                    overflow: 'visible',
                    textAlign: 'center',
                    autoHide: false,
                    template: function (row) {
                        var result = "";
                        <?php if($privilege->can_update){?>
                        result = result + `<div class="custom-control custom-switch"><input type="checkbox" data-id="${row.id}" class="custom-control-input edit_log" id="edit_log${row.id}" ${ row.is_log == 1 ? 'checked' : '' }><label class="custom-control-label" for="edit_log${row.id}"></label></div>`;
                        <?php }?>
                        return result;
                    }
                }
            ],
            layout: {
                header: true
            }
        }
        datatable = $('#kt_datatable').KTDatatable(option);
        datatable.on("kt-datatable--on-layout-updated", function () {
            initDTEvents();
        })
    }

    function initCustomRule() {
    }

    function initAlert() {
        AlertUtil = {
            showSuccess: function (message, timeout) {
                $("#success_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#success_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess: function () {
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed: function (message, timeout) {
                $("#failed_message").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss").trigger("click");
                    }, timeout)
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideFailed: function () {
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd: function (message, timeout) {
                $("#failed_message_add").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss_add").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_add").show();
            },
            hideSuccessDialogAdd: function () {
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit: function (message, timeout) {
                $("#failed_message_edit").html(message);
                if (timeout != undefined) {
                    setTimeout(function () {
                        $("#failed_alert_dismiss_edit").trigger("click");
                    }, timeout)
                }
                $("#failed_alert_edit").show();
            },
            hideSuccessDialogAdd: function () {
                $("#failed_alert_dismiss_edit").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click", function () {
            $("#failed_alert").hide();
        })
        $("#success_alert_dismiss").on("click", function () {
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click", function () {
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click", function () {
            $("#failed_alert_edit").hide();
        })
    }

    function Export(){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "Log_setting_"+date+".csv";
        var search = $('#generalSearch').val();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: {
                query : $("#generalSearch").val()
            },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }

    function Print(){
        <?php if($privilege->can_read){?>
            KTApp.blockPage();
            $.ajax({
                url: "<?php echo base_url($controller_full_path."/print_log");?>",
                type: 'post',
                dataType: 'json',
                data: {
                    query : $("#generalSearch").val()
                },
                success: function (response, status) {
                        KTApp.unblockPage();
                        if (response.status == true) {
                            //populate form
                            var win = window.open("", "_blank");                    
                            win.document.write(response.data);
                            win.document.close();  
                            setTimeout(function() {
                                win.print();
                                win.close();
                            }, 250);
                        } else {
                            AlertUtil.showFailed(response.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
            })    
        <?php }else{ ?>
            swal.fire({
                    title: 'Infomation',
                    text: "You cannot Print this data",
                    type: 'info',
                });
        <?php } ?>
    }

    jQuery(document).ready(function () {
        initDataTable();
        initCustomRule();
        initAlert();
    });
</script>