 <!--begin::Modal-->
 <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form id="form_delete" action="#">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <input type="hidden" id="Id_Delete" name="group_id" />
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Area you sure want to delete this?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-danger deleteConfirm" value="Delete" id="btn_delete"/>
                </div>
            </div>
        </div>
    </form>
</div>
<!--end::Modal-->