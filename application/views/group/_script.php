<script>
//globals
var datatable;
var AlertUtil;
var createForm;
var editForm;
function initDTEvents(){
    $(".btn_delete").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailed(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $(".btn_edit").on("click",function(){
        var targetId = $(this).data("id");
        KTApp.blockPage();
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {id:targetId},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    editForm.populateForm(response.data);
                    $('#modal_edit').modal('show');
                }else{
                    AlertUtil.showFailed(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });  
    });
}
function initDataTable(){
    var option = {
        data: {
            type: 'remote',
            source: {
              read: {
                url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState : {cookie: false,webstorage: false},
          },
          sortable: true,
          pagination: true,
          search: {
            input: $('#generalSearch'),
          },
          columns: [
            {
                field: 'group_id',
                title: 'Group ID',
                sortable: 'asc',
                width:60,
                textAlign: 'center',
            }, 
            {
                field: 'group_name',
                title: 'Group Name',
                sortable: 'asc',
                textAlign: 'left',
            }, 
            {
                field: 'group_level',
                title: 'Level',
                sortable: 'asc',
                textAlign: 'left',
            }, 
            {
                field: 'action',
                title: 'Action',
                sortable: false,
                width: 100,
                overflow: 'visible',
                textAlign: 'center',
                autoHide: false,
                template: function (row) {
                    var result ="";
                    <?php if($privilege->can_update){?>
                        result = result + '<span data-id="' + row.group_id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" ><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                    <?php }?>
                    <?php if($privilege->can_delete){?>
                        result = result + '<span data-id="' + row.group_id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" ><i class="flaticon2-trash" style="cursor:pointer;"></i></span>';
                    <?php }?>
                    return result;
                }                        
            }
          ],
          layout:{
            header:true
          }
    }
    datatable = $('#kt_datatable').KTDatatable(option);
    datatable.on("kt-datatable--on-layout-updated",function(){
        initDTEvents();
    })
}
function initCustomRule(){
}
function initAlert(){
    AlertUtil = {
        showSuccess : function(message,timeout){
            $("#success_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#success_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#success_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#success_alert_dismiss").trigger("click");
        },
        showFailed : function(message,timeout){
            $("#failed_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#failed_alert").show();
            KTUtil.scrollTop();
        },
        hideFailed : function(){
            $("#failed_alert_dismiss").trigger("click");
        },
        showFailedDialogAdd : function(message,timeout){
            $("#failed_message_add").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_add").trigger("click");
                },timeout)
            }
            $("#failed_alert_add").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_add").trigger("click");
        },
        showFailedDialogEdit : function(message,timeout){
            $("#failed_message_edit").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_edit").trigger("click");
                },timeout)
            }
            $("#failed_alert_edit").show();
        },
        hideSuccessDialogAdd : function(){
            $("#failed_alert_dismiss_edit").trigger("click");
        }
    }
    $("#failed_alert_dismiss").on("click",function(){
        $("#failed_alert").hide();
    })
    $("#success_alert_dismiss").on("click",function(){
        $("#success_alert").hide();
    })
    $("#failed_alert_dismiss_add").on("click",function(){
        $("#failed_alert_add").hide();
    })
    $("#failed_alert_dismiss_edit").on("click",function(){
        $("#failed_alert_edit").hide();
    })
}
function initCreateForm(){
    //validator
    var validator = $( "#form_add" ).validate({
        ignore:[],
        rules: {
            group_name: {
                required: true,
            },
            group_level: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   
    $('#add_group_level').select2({
        placeholder: "Please select a Level",
        width: '100%'
    });
    //events
    $("#btn_add_submit").on("click",function(){
      var isValid = $( "#form_add" ).valid();
      if(isValid){
        KTApp.block('#modal_add .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/save"); ?>",
            data : $('#form_add').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_add .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_add').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogAdd(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_add .modal-content');
                AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#modal_add').on('hidden.bs.modal', function () {
       validator.resetForm();
    })

    return {
        validator:validator
    }
}
function initEditForm(){
    //validator
    var validator = $( "#form_edit" ).validate({
        ignore:[],
        rules: {
            group_name: {
                required: true,
            },
            group_level: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    $('#edit_group_level').select2({
        placeholder: "Please select a Level",
        width: '100%'
    });
    //events
    $("#btn_edit_submit").on("click",function(){
      var isValid = $( "#form_edit" ).valid();
      if(isValid){
        KTApp.block('#modal_edit .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/edit"); ?>",
            data : $('#form_edit').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_edit .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_edit').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailed(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_edit .modal-content');
                AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#modal_edit').on('hidden.bs.modal', function () {
       validator.resetForm();
    
    })

    var populateForm = function(groupObject){
        $("#edit_group_id").val(groupObject.group_id);
        $("#edit_group_name").val(groupObject.group_name);
        $("#edit_group_level").val(groupObject.group_level);
        $("#edit_group_level").trigger('change');
    }
    
    return {
        validator:validator,
        populateForm:populateForm
    }
}

function Print(){
    <?php if($privilege->can_read){?>
        var search = $('#generalSearch').val();
        KTApp.blockPage();
        $.ajax({
            
            url: "<?php echo base_url($controller_full_path."/print_group");?>",
            type: 'post',
            dataType: 'json',
            data: { search: search  },
            success: function (response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        var win = window.open("", "_blank");                    
                        win.document.write(response.data);
                        win.document.close();  
                        setTimeout(function() {
                            win.print();
                            win.close();
                        }, 250);
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot Print this data",
                type: 'info',
            });
    <?php } ?>
    }

function Export(){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "Group_"+date+".csv";
        var search = $('#generalSearch').val();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: { search: search  },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }

jQuery(document).ready(function() { 
    initDataTable();
    initCustomRule();
    createForm = initCreateForm();
    editForm = initEditForm();
    initAlert();
});

</script>
