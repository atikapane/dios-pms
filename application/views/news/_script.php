<script>
    var token_file="";
    var dropThumbnail;
    var datatable;
    var AlertUtil;
    var createForm;
    var editForm;
    var imageUploader;

    function initDTEvents(){
        $(".btn_delete").on("click",function(){
            var targetId = $(this).data("id");
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it'
            }).then(function(result) {
                if (result.value) {
                    KTApp.blockPage();
                    $.ajax({
                        type : 'GET',
                        url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                        data : {
                            id:targetId
                        },
                        dataType : "json",
                        success : function(data,status){
                            KTApp.unblockPage();
                            if(data.status == true){
                                datatable.reload();
                                AlertUtil.showSuccess(data.message,5000);
                            }else{
                                AlertUtil.showFailed(data.message);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            KTApp.unblockPage();
                            AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                        }
                    });
                }
            });
        });

        $(".btn_edit").on("click",function(){
            var targetId = $(this).data("id");
            KTApp.blockPage();
            $("#description").summernote('code','');
            imageUploader.removeAllFiles();
            $.ajax({
                type : 'GET',
                url : "<?php echo base_url($controller_full_path."/get"); ?>",
                data : {id:targetId},
                dataType : "json",
                success : function(response,status){
                    KTApp.unblockPage();
                    if(response.status == true){
                        //populate form
                        console.log(response.data.title);
                        editForm.populateForm(response.data);
                        $('#modal_edit_news').modal('show');
                    }else{
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
            });
        });
    };

    function Export(){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "News_"+date+".csv";
        var search = $('#generalSearch').val();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: { search: search  },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }

    function Print(){
        <?php if($privilege->can_read){?>
            var search = $('#generalSearch').val();
            KTApp.blockPage();
            $.ajax({
                url: "<?php echo base_url($controller_full_path."/print_news");?>",
                type: 'post',
                dataType: 'json',
                data: { search: search  },
                success: function (response, status) {
                        KTApp.unblockPage();
                        if (response.status == true) {
                            //populate form
                            var win = window.open("", "_blank");                    
                            win.document.write(response.data);
                            win.document.close();  
                            setTimeout(function() {
                                win.print();
                                win.close();
                            }, 250);
                        } else {
                            AlertUtil.showFailed(response.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
            })    
        <?php }else{ ?>
            swal.fire({
                    title: 'Infomation',
                    text: "You cannot Print this data",
                    type: 'info',
                });
        <?php } ?>
    }

    function initDataTable(){
        var option = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '<?php echo base_url(); ?>be/landing/News/datatable',
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState : {cookie: false,webstorage: false},
            },
            sortable: true,
            pagination: true,
            search: {
                input: $("#generalSearch"),
            },
            columns: [
                {
                    field: 'title',
                    title: 'Title',
                    width: 200,
                    sortable: 'desc',
                }, {
                    field: 'news_date',
                    title: 'News Date',
                    width: 150,
                    sortable: 'desc',
                }, {
                    field: 'updated_at',
                    title: 'Updated Date',
                    width: 150,
                    sortable: 'desc',
                }, {
                    field: 'action',
                    title: 'Action',
                    sortable: false,
                    width: 80,
                    overflow: 'visible',
                    textAlign: 'left',
                    autoHide: false,
                    template: function (row) {
                        var result ="";
                        <?php if($privilege->can_update){?>
                            result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Edit" ><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                        <?php }?>
                        <?php if($privilege->can_delete){?>
                            result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" ><i class="flaticon2-trash" style="cursor:pointer;"></i></span>';
                        <?php }?>
                        return result;
                    }
                }
            ],
            layout:{
                header:true
            }
        }
        datatable = $('#news_datatable').KTDatatable(option);
        datatable.on("kt-datatable--on-layout-updated",function(){
            initDTEvents();
        });
    };

    var KTSummernoteDemo = function () {
        // Private functions
        var demos = function () {
            $('.summernote').summernote({
                height: 200,
                dialogsInBody: true
            });
        };

        return {
            // public functions
            init: function() {
                demos();
            }
        };
    }();

    function initCustomRule(){
    }

    function initAlert(){
        AlertUtil = {
            showSuccess : function(message,timeout){
                $("#success_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#success_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess : function(){
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed : function(message,timeout){
                $("#failed_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess : function(){
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd : function(message,timeout){
                $("#failed_message_add").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_add").trigger("click");
                    },timeout)
                }
                $("#failed_alert_add").show();
            },
            hideSuccessDialogAdd : function(){
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit : function(message,timeout){
                $("#failed_message_edit").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_edit").trigger("click");
                    },timeout)
                }
                $("#failed_alert_edit").show();
            },
            hideSuccessDialogAdd : function(){
                $("#failed_alert_dismiss_edit").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click",function(){
            $("#failed_alert").hide();
        })
        $("#success_alert_dismiss").on("click",function(){
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click",function(){
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click",function(){
            $("#failed_alert_edit").hide();
        })
    }

    function initCreateForm(){

        var input = document.getElementById('tag_related_add');
        let tagify = new Tagify(input);

        document.getElementById('tag_add_related_remove').addEventListener('click', tagify.removeAllTags.bind(tagify));

        tagify.on('add', onAddTag)
            .on('remove', onRemoveTag)
            .on('input', onInput)
            .on('edit', onTagEdit)
            .on('invalid', onInvalidTag)
            .on('click', onTagClick)
            .on('dropdown:show', onDropdownShow)
            .on('dropdown:hide', onDropdownHide);

        function onAddTag(e) {
            tagify.off('add', onAddTag);
        }

        function onRemoveTag(e) {
        }

        function onInput(e) {
        }

        function onTagEdit(e) {
        }

        function onInvalidTag(e) {
        }

        // invalid tag added callback
        function onTagClick(e) {
        }

        function onDropdownShow(e) {
        }

        function onDropdownHide(e) {
        }

        Dropzone.autoDiscover = false;
        imageUploader = new Dropzone("#add_image",{
            url:"<?php echo base_url($controller_full_path."/image_upload"); ?>",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: true,
            method:"post",
            acceptedFiles: "image/jpeg,image/png"
        });

        imageUploader.on("sending", function (file, xhr, formData) {
            file.token = new Date().getTime();
            formData.append("token",file.token);
            token_file = file.token;
        });

        imageUploader.on("removedfile", function (file) {
            $("#add_image_name").val("");
        });

        imageUploader.on("success", function (file,response) {
            $("#add_image_name").val(response);
        });

        var summernoteElement = $('.summernote');

        var validator = $('#form_add').validate({
            ignore: ':hidden:not(.summernote),.note-editable.card-block',
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("invalid-feedback");
                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.siblings("label"));
                } else if (element.hasClass("summernote")) {
                    error.insertAfter(element.siblings(".note-editor"));
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                title: {
                    required: true,
                },
                description:{
                    required:true
                },
                image_name: {
                    required: true,
                },
                tag_related:{
                    required: true,
                }
            },
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();
            }
        });

        summernoteElement.summernote({
            dialogsInBody: true,
            callbacks: {
                onChange: function (contents, $editable) {
                    // Note that at this point, the value of the `textarea` is not the same as the one
                    // you entered into the summernote editor, so you have to set it yourself to make
                    // the validation consistent and in sync with the value.
                    summernoteElement.val(summernoteElement.summernote('isEmpty') ? "" : contents);

                    // You should re-validate your element after change, because the plugin will have
                    // no way to know that the value of your `textarea` has been changed if the change
                    // was done programmatically.
                    validator.element(summernoteElement);
                }
            }
        });


        $("#btn_add_submit").on("click", function () {
            var isValid = $( "#form_add" ).valid();
            if($("#add_image_name").val() == ""){
                $("#image-error").show();
                return;
            };

            if(isValid){
                KTApp.block('#modal_add_news .modal-content', {});

                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url($controller_full_path."/save"); ?>",
                    data: $("#form_add").serialize(),
                    dataType:"json",
                    success:function (data, status) {
                        KTApp.unblock('#modal_add_news .modal-content');
                        if(data.status == true){
                            datatable.reload();
                            $('#modal_add_news').modal('hide');
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },error:function () {
                        KTApp.unblock('#modal_add_news .modal-content');
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        var date = new Date();

        $('.date-time-picker').datetimepicker({
            format: 'yyyy/mm/dd hh:ii',
            minDate: date,
            startDate: date,
            todayHighlight: true,
            use24hours: true
        });

        return {
            imageUploader:imageUploader,
            validator:validator,
            tag:tagify
        }
    }

    function initEditForm(){
        imageUploader = new Dropzone("#edit_image",{
            url:"<?php echo base_url($controller_full_path."/image_upload"); ?>",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: true,
            method:"post",
            acceptedFiles: "image/jpeg,image/png"
        });

        imageUploader.on("sending", function (file, xhr, formData) {
            file.token = new Date().getTime();
            formData.append("token",file.token);
            token_file = file.token;
        });

        imageUploader.on("success", function (file,response) {
            $("#edit_image_name").val(response);
        });

        imageUploader.on("removedfile", function (file) {
            $("#edit_image_name").val("");
        });

        //validator
        var summernoteElement = $('.summernote');

        var validator = $('#form_edit').validate({
            ignore: ':hidden:not(.summernote),.note-editable.card-block',
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("invalid-feedback");
                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.siblings("label"));
                } else if (element.hasClass("summernote")) {
                    error.insertAfter(element.siblings(".note-editor"));
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                title: {
                    required: true,
                },
                description:{
                    required:true
                },
                image_name: {
                    required: true,
                },
                tag_related:{
                    required: true,
                }
            },
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();
            }
        });

        summernoteElement.summernote({
            dialogsInBody: true,
            callbacks: {
                onChange: function (contents, $editable) {
                    // Note that at this point, the value of the `textarea` is not the same as the one
                    // you entered into the summernote editor, so you have to set it yourself to make
                    // the validation consistent and in sync with the value.
                    summernoteElement.val(summernoteElement.summernote('isEmpty') ? "" : contents);

                    // You should re-validate your element after change, because the plugin will have
                    // no way to know that the value of your `textarea` has been changed if the change
                    // was done programmatically.
                    validator.element(summernoteElement);
                }
            }
        });

        //events
        $("#btn_edit_submit").on("click",function(){
            console.log($("#edit_image_name").val());
            var isValid = $( "#form_edit" ).valid();
            if($("#edit_image_name").val() == ""){
                $("#image-error-edit").show();
                return;
            };
            if(isValid){
                KTApp.block('#modal_edit_news .modal-content', {});
                $.ajax({
                    type : 'POST',
                    url : "<?php echo base_url($controller_full_path."/edit"); ?>",
                    data : $('#form_edit').serialize(),
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblock('#modal_edit_news .modal-content');
                        if(data.status == true){
                            datatable.reload();
                            $('#modal_edit_news').modal('hide');
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailedDialogEdit(data.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblock('#modal_edit_news .modal-content');
                        AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        let edit_tag = document.getElementById('tag_related_edit');
        let tagify = new Tagify(edit_tag, {
                whitelist: [],
                blacklist: [],
            });

        var related_field = function (val) {
            tagify.addTags(val);

            document.getElementById('tag_edit_related_remove').addEventListener('click', tagify.removeAllTags.bind(tagify));

            tagify.on('add', onAddTag)
                .on('remove', onRemoveTag)
                .on('input', onInput)
                .on('edit', onTagEdit)
                .on('invalid', onInvalidTag)
                .on('click', onTagClick)
                .on('dropdown:show', onDropdownShow)
                .on('dropdown:hide', onDropdownHide);

            function onAddTag(e) {
                tagify.off('add', onAddTag) // exmaple of removing a custom Tagify event
            }

            function onRemoveTag(e) {
            }

            function onInput(e) {
            }

            function onTagEdit(e) {
            }

            function onInvalidTag(e) {
            }

            function onTagClick(e) {
            }

            function onDropdownShow(e) {
            }

            function onDropdownHide(e) {
            }
        };

        var populateForm = function(newsObject){
            // console.log(newsObject.related);
            $("#edit_id").val(newsObject.id);
            $("#edit_title").val(newsObject.title);
            $("#edit_description").summernote('code',newsObject.description);

            // re-populate dropzone
            var filename = newsObject.img_path.replace(/^.*[\\\/]/, '');
            var mockFile = { name: filename, size: 12345 };
            imageUploader.emit("addedfile", mockFile);
            imageUploader.emit("thumbnail", mockFile, newsObject.img_path);
            imageUploader.files.push(mockFile);
            $("#edit_image_name").val(filename);

            tagify.removeAllTags();
            related_field(newsObject.related);

            $("#edit_news_date").val(newsObject.news_date);
        };

        return {
            imageUploader:imageUploader,
            validator:validator,
            populateForm:populateForm,
            tag:tagify
        }
    }

    $(document).ready(function (){
        //image uploader
        initDataTable();
        initCustomRule();
        KTSummernoteDemo.init();
        createForm = initCreateForm();
        editForm = initEditForm();
        initAlert();

    });

    $(document).on("show.bs.modal", '.modal', function (event) {
        var zIndex = 100000 + (10 * $(".modal:visible").length);
        $(this).css("z-index", zIndex);
        setTimeout(function () {
            $(".modal-backdrop").not(".modal-stack").first().css("z-index", zIndex - 1).addClass("modal-stack");
        }, 0);
    }).on("hidden.bs.modal", '.modal', function (event) {
        $(".modal:visible").length && $("body").addClass("modal-open");

        createForm.validator.resetForm();
        createForm.imageUploader.removeAllFiles();
        createForm.tag.removeAllTags();
        $("#description").summernote('code','');

        editForm.validator.resetForm();
        editForm.imageUploader.removeAllFiles();
        editForm.tag.removeAllTags();
        $("#edit_description").summernote('code','');
    });
    $(document).on('inserted.bs.tooltip', function (event) {
        var zIndex = 100000 + (10 * $(".modal:visible").length);
        var tooltipId = $(event.target).attr("aria-describedby");
        $("#" + tooltipId).css("z-index", zIndex);
    });
    $(document).on('inserted.bs.popover', function (event) {
        var zIndex = 100000 + (10 * $(".modal:visible").length);
        var popoverId = $(event.target).attr("aria-describedby");
        $("#" + popoverId).css("z-index", zIndex);
    });

</script>