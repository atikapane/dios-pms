<!--begin::Modal-->
<div class="modal fade" id="modal_edit_news" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_id" name="id"/>
                    <div class="form-group">
                        <label>Title:</label>
                        <input type="text" class="form-control" placeholder="Enter Title" id="edit_title" name="title" required>
                    </div>
                    <div class="form-group">
                        <label>Descriptions:</label>
                        <textarea id="edit_description" class="summernote" data-msg="Please write something" name="description" required></textarea>
                    </div>
                    <div class="form-group form-group-last">
                        <label>Thumbnail:</label>

                        <div class="dropzone dropzone-default col-12" id="edit_image">
                            <div class="dropzone-msg dz-message needsclick">
                                <h3 class="dropzone-msg-title">
                                    <i class="flaticon2-image-file display-3"></i>
                                </h3>
                                <span class="dropzone-msg-desc">Drop Image to Here</span>
                            </div>
                        </div>
                        <input type="hidden" id="edit_image_name" name="image_name"/>
                        <div id="image-error-edit" style="display: none; width: 100%; margin-top: 0.25rem; font-size: 80%; color: #fd397a;">This field is required.</div>
                    </div>
                    <div class="col-md-12" >
                        <div class="kt-section">
                            <div class="kt-section__content">
                                <div class="alert alert-danger fade show" role="alert" id="failed_alert_edit" style="display: none;">
                                    <div class="alert-text" id="failed_message_edit"></div>
                                    <div class="alert-close">
                                        <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_edit">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Related:</label>
                        <div class="col-12 p-0" id="related">
                            <input id="tag_related_edit" name="tag_related_edit" placeholder="type..." autofocus >
                            <div class="kt-margin-t-10">
                                <a href="javascript:;" id="tag_edit_related_remove" class="btn btn-label-brand btn-bold">Remove tags</a>
                            </div>
                            <div class="kt-margin-t-10">
                                The last tag has the same value as the first tag, and will be removed,
                                because the duplicates setting is set to true.
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-last">
                        <label>News Date:</label>
                        <input type="text" class="form-control date-time-picker" id="edit_news_date" name="news_date" placeholder="Enter News Date" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->