<!--begin::Modal-->
<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_add" class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" class="form-control" id="add_username" name="username" placeholder="Username">
                                </div>
                            </div>   
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" id="add_password" name="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input type="text" class="form-control" id="add_employee_id" name="employee_id" placeholder="NIP">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Pegawai</label>
                                    <input type="text" class="form-control" id="add_fullname" name="fullname" placeholder="Nama lengkap pegawai">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="add_email" name="email" placeholder="example@email.com">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status Pegawai</label>
                                    <select class="form-control" name="status_pegawai" id="add_status_pegawai">
                                        <option value=""></option>
                                        <option value = "1">Tetap</option>
                                        <option value = "2">Kontrak</option>
                                        <option value = "3">Freelance/Magang</option>
                                        <option value = "99">Keluar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select class="form-control" name="gender" id="add_gender">
                                        <option value=""></option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="99">Other/Prefer Not To Say</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <select class="form-control" name="tempat_lahir" id="add_tempat_lahir">
                                        <option value=""></option>
                                        <?php foreach($cities as $city): ?>
                                            <option value="<?= $city->id ?>"><?= $city->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input placeholder="yyyy-mm-dd" type="text" name="tgl_lahir" id="add_tgl_lahir" class="form-control date-picker" onkeydown="return false">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tanggal Bergabung</label>
                                    <input placeholder="yyyy-mm-dd" type="text" name="tgl_bergabung" id="add_tgl_bergabung" class="form-control date-picker" onkeydown="return false">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Departemen</label>
                                    <select class="form-control" name="departemen" id="add_departemen">
                                        <option value=""></option>
                                        <?php foreach($departemen as $dep): ?>
                                            <option value="<?= $dep->departemen_id ?>"><?= $dep->departemen_name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Posisi</label>
                                    <input type="text" class="form-control" id="add_posisi" name="posisi" placeholder="Posisi">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>NPWP</label>
                                    <input type="text" class="form-control" id="add_npwp" name="npwp" placeholder="NPWP">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. KTP</label>
                                    <input type="text" class="form-control" id="add_no_ktp" name="no_ktp" placeholder="Nomor Kartu Tanda Penduduk">
                                </div>
                            </div>
                            <!--BPJS-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. BPJS Kesehatan</label>
                                    <input type="text" class="form-control" id="add_no_bpjs_kesehatan" name="no_bpjs_kesehatan" placeholder="Nomor BPJS Kesehatan">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Faskes</label>
                                    <input type="text" class="form-control" id="add_nama_faskes" name="nama_faskes" placeholder="Nama Faskes">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. BPJS Ketenagakerjaan</label>
                                    <input type="text" class="form-control" id="add_no_bpjs_ketenagakerjaan" name="no_bpjs_ketenagakerjaan" placeholder="Nomor BPJS Ketenagakerjaan">
                                </div>
                            </div>
                            <!--Pendidikan-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pendidikan Terakhir</label>
                                    <input type="text" class="form-control" id="add_pendidikan_terakhir" name="pendidikan_terakhir" placeholder="Pendidikan terakhir">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Sekolah/Universitas</label>
                                    <input type="text" class="form-control" id="add_nama_sekolah" name="nama_sekolah" placeholder="Nama sekolah">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Jurusan</label>
                                    <input type="text" class="form-control" id="add_jurusan" name="jurusan" placeholder="Nama jurusan">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>IPK</label>
                                    <input type="text" class="form-control" id="add_ipk" name="ipk" placeholder="IPK">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tahun Lulus</label>
                                    <input type="text" class="form-control" id="add_tahun_lulus" name="tahun_lulus" placeholder="Tahun lulus">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. Seri Ijazah</label>
                                    <input type="text" class="form-control" id="add_no_seri_ijazah" name="no_seri_ijazah" placeholder="No. Seri Ijazah">
                                </div>
                            </div>
                            <!--kontak-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No Handphone</label>
                                    <input type="text" class="form-control" id="add_no_hp" name="no_hp" placeholder="No Handphone">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No. Telepon Rumah</label>
                                    <input type="text" class="form-control" id="add_no_tlp_rumah" name="no_tlp_rumah" placeholder="No Telepon Rumah">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No Telepon Darurat</label>
                                    <input type="text" class="form-control" id="add_no_darurat" name="no_darurat" placeholder="No yang dapat dihubungi saat darurat">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Keluarga</label>
                                    <input type="text" class="form-control" id="add_nama_keluarga" name="nama_keluarga" placeholder="Nama keluarga">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Hubungan Keluarga</label>
                                    <input type="text" class="form-control" id="add_hubungan_keluarga" name="hubungan_keluarga" placeholder="Hubungan dalam keluarga">
                                </div>
                            </div>
                            <!--Alamat-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Alamat KTP</label>
                                    <input type="text" class="form-control" id="add_alamat_ktp" name="alamat_ktp" placeholder="Alamat KTP">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kode POS</label>
                                    <input type="text" class="form-control" id="add_kode_pos" name="kode_pos" placeholder="Kode POS">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Alamat Domisili</label>
                                    <input type="text" class="form-control" id="add_alamat_domisili" name="alamat_domisili" placeholder="Alamat domisili">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Keahlian khusus</label>
                                    <input type="text" class="form-control" id="add_keahlian_khusus" name="keahlian_khusus" placeholder="Keahlian khusus">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pengalaman proyek</label>
                                    <input type="text" class="form-control" id="add_pengalaman_proyek" name="pengalaman_proyek" placeholder="Pengalaman proyek">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Riwayat Training</label>
                                    <input type="text" class="form-control" id="add_riwayat_training" name="riwayat_training" placeholder="Training internal/eksternal">
                                </div>
                            </div>
                            <!-- Bank -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>No Rekening</label>
                                    <input type="text" class="form-control" id="add_account_no" name="account_no" placeholder="No Rekening">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Owner</label>
                                    <input type="text" class="form-control" id="add_owner" name="owner" placeholder="Nama pemilik rekening">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Bank</label>
                                    <input type="text" class="form-control" id="add_bank" name="bank" placeholder="Nama Bank">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Cabang</label>
                                    <input type="text" class="form-control" id="add_bank_branch" name="bank_branch" placeholder="Nama Cabang">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Agama</label>
                                    <input type="text" class="form-control" id="add_agama" name="agama" placeholder="agama">
                                </div>
                            </div>
                             <div class="col-md-12">
                                <div class="form-group">
                                    <label>Status Keluarga</label>
                                    <select class="form-control" name="status_keluarga" id="add_status_keluarga">
                                        <option value=""></option>
                                        <option value="1">Belum Menikah</option>
                                        <option value="2">Menikah</option>
                                        <option value="3">Berpisah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tenaga kerja</label>
                                    <input type="text" class="form-control" id="add_tenaga_kerja" name="tenaga_kerja" placeholder="jumlah tenaga kerja di keluarga">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Istri/Suami</label>
                                    <input type="text" class="form-control" id="add_istri_suami" name="istri_suami" placeholder="jumlah istri/suami">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Anak</label>
                                    <input type="text" class="form-control" id="add_anak" name="anak" placeholder="jumlah anak">
                                </div>
                            </div>

                            <!-- Upload Data -->
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>Copy Ijazah</label>
                                    <div class="dropzone dropzone-default col-12 col-md-12" id="add_copy_ijazah">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="add_copy_ijazah_name" name="copy_ijazah_name"/>
                                    <br>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>Copy Transkrip Nilai</label>
                                    <div class="dropzone dropzone-default col-12 col-md-12" id="add_copy_transkrip_nilai">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="add_copy_transkrip_nilai_name" name="copy_transkrip_nilai_name"/>
                                    <br>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>Copy KTP</label>
                                    <div class="dropzone dropzone-default col-12 col-md-12" id="add_copy_ktp">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="add_copy_ktp_name" name="copy_ktp_name"/>
                                </div>
                                <br>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group form-group-last">
                                <label>Copy NPWP</label>
                                <div class="dropzone dropzone-default col-12 col-md-12" id="add_copy_npwp">
                                    <div class="dropzone-msg dz-message needsclick">
                                        <h3 class="dropzone-msg-title">
                                            <i class="flaticon2-document display-3"></i>
                                        </h3>
                                        <span class="dropzone-msg-desc">Drop File to Here</span>
                                    </div>
                                </div>
                                <span class="form-text text-muted">Please upload File</span>
                                <input type="hidden" id="add_copy_npwp_name" name="copy_npwp_name"/>
                            </div>
                            <br>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group form-group-last">
                                    <label>CV</label>
                                    <div class="dropzone dropzone-default col-12 col-md-12" id="add_cv">
                                        <div class="dropzone-msg dz-message needsclick">
                                            <h3 class="dropzone-msg-title">
                                                <i class="flaticon2-document display-3"></i>
                                            </h3>
                                            <span class="dropzone-msg-desc">Drop File to Here</span>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Please upload File</span>
                                    <input type="hidden" id="add_cv_name" name="cv_name"/>
                                </div>
                            </div> 
                            <div class="col-md-12" >     
                                <div class="kt-section">
                                    <div class="kt-section__content">
                                         <div class="alert alert-danger fade show" role="alert" id="failed_alert_add" style="display: none;">
                                            <div class="alert-text" id="failed_message_add"></div>
                                            <div class="alert-close">
                                                <button type="button" class="close" aria-label="Close" id="failed_alert_dismiss_add">
                                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                                </button>
                                            </div>
                                        </div>                   
                                    </div>                   
                                </div>            
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_add_submit">Submit</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->