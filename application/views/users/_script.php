<script>
//globals
var datatable;
var AlertUtil;
var createForm;
var editForm;
var modifyForm;
function initDTEvents(){
    $(".btn_delete").on("click",function(){
        var targetId = $(this).data("id");
        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it'
        }).then(function(result) {
            if (result.value) {
                KTApp.blockPage();
                $.ajax({
                    type : 'GET',
                    url : "<?php echo base_url($controller_full_path."/delete"); ?>",
                    data : {id:targetId},
                    dataType : "json",
                    success : function(data,status){
                        KTApp.unblockPage();
                        if(data.status == true){
                            datatable.reload();
                            AlertUtil.showSuccess(data.message,5000);
                        }else{
                            AlertUtil.showFailedDialogEdit(data.message);
                        }                
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        KTApp.unblockPage();
                        AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                    }
                });  
            }
        });
    });

    $(".btn_edit").on("click",function(){
        var targetId = $(this).data("id");
        KTApp.blockPage();
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {id:targetId},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    editForm.populateForm(response.data);
                    $('#modal_edit').modal('show');
                }else{
                    AlertUtil.showFailed(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });  
    });

    $(".btn_modify").on("click",function(){
        var targetId = $(this).data("id");
        KTApp.blockPage();
        $.ajax({
            type : 'GET',
            url : "<?php echo base_url($controller_full_path."/get"); ?>",
            data : {id:targetId},
            dataType : "json",
            success : function(response,status){
                KTApp.unblockPage();
                if(response.status == true){
                    //populate form
                    modifyForm.populateForm(response.data);
                    $('#modal_modify').modal('show');
                }else{
                    AlertUtil.showFailed(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblockPage();
                AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
            }
        });  
    });

    $(".btn_import").on("click", function(){
        $('#modal_import').modal('show')
    })
}
function initDataTable(){
    var option = {
        data: {
            type: 'remote',
            source: {
              read: {
                url: '<?php echo base_url($controller_full_path."/datatable"); ?>',
                params:{
                    group_id : $("#group_id").val(),
                },
                map: function(raw) {
                  // sample data mapping
                  var dataSet = raw;
                  if (typeof raw.data !== 'undefined') {
                    dataSet = raw.data;
                  }
                  return dataSet;
                },
              },
            },
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            saveState : {cookie: false,webstorage: false},
          },
          sortable: true,
          pagination: true,
          search: {
            input: $('#generalSearch'),
          },
          columns: [
            {
                field: 'employeeid',
                title: 'Employee ID',
                sortable: 'asc',                          
            }, 
            {
                field: 'fullname',
                title: 'Fullname',
                sortable: 'asc',                          
            }, 
            {
                field: 'departemen_name',
                title: 'Departemen',
                sortable: 'asc',                          
            }, 
            {
                field: 'status_pegawai',
                title: 'Status',
                sortable: 'asc',                          
            },
            {
                field: 'email',
                title: 'Email',
                sortable: 'asc',                          
            },
            {
                field: 'no_hp',
                title: 'No HP',
                sortable: 'asc',                          
            },
            {
                field: 'gender',
                title: 'Gender',
                sortable: 'asc',                          
            },   
            {
                field: 'tempat_lahir',
                title: 'Tempat lahir',
                sortable: 'asc',                          
            },
            {
                field: 'tgl_lahir',
                title: 'Tanggal lahir',
                sortable: 'asc',                          
            }, 
            {
                field: 'tgl_bergabung',
                title: 'Tanggal bergabung',
                sortable: 'asc',                          
            }, 
            {
                field: 'npwp',
                title: 'NPWP',
                sortable: 'asc',                          
            }, 
            {
                field: 'no_ktp',
                title: 'No KTP',
                sortable: 'asc',                          
            },
            {
                field: 'no_bpjs_kesehatan',
                title: 'No BPJS kesehatan',
                sortable: 'asc',                          
            },   
            {
                field: 'nama_faskes',
                title: 'Nama Faskes',
                sortable: 'asc',                          
            }, 
            {
                field: 'no_bpjs_ketenagakerjaan',
                title: 'No BPJS ketenagakerjaan',
                sortable: 'asc',                          
            }, 
            {
                field: 'pendidikan_terakhir',
                title: 'Pendidikan terakhir',
                sortable: 'asc',                          
            }, 
            {
                field: 'nama_sekolah',
                title: 'Nama sekolah',
                sortable: 'asc',                          
            },
            {
                field: 'jurusan',
                title: 'Jurusan',
                sortable: 'asc',                          
            },   
            {
                field: 'ipk',
                title: 'IPK',
                sortable: 'asc',                          
            }, 
            {
                field: 'tahun_lulus',
                title: 'Tahun lulus',
                sortable: 'asc',                          
            }, 
            {
                field: 'no_seri_ijazah',
                title: 'No seri ijazah',
                sortable: 'asc',                          
            }, 
            
            {
                field: 'no_tlp_rumah',
                title: 'No telepon rumah',
                sortable: 'asc',                          
            }, 
             
            {
                field: 'alamat_ktp',
                title: 'Alamat KTP',
                sortable: 'asc',                          
            }, 
            {
                field: 'kode_pos',
                title: 'Kode POS',
                sortable: 'asc',                          
            }, 
            {
                field: 'alamat_domisili',
                title: 'Alamat domisili',
                sortable: 'asc',                          
            },
            {
                field: 'no_darurat',
                title: 'No darurat',
                sortable: 'asc',                          
            },   
            {
                field: 'nama_keluarga',
                title: 'Nama keluarga',
                sortable: 'asc',                          
            }, 
            {
                field: 'hubungan_keluarga',
                title: 'Hubungan keluarga',
                sortable: 'asc',                          
            }, 
            {
                field: 'keahlian_khusus',
                title: 'Keahlian khusus',
                sortable: 'asc',                          
            }, 
            {
                field: 'pengalaman_project',
                title: 'Pengalaman project',
                sortable: 'asc',                          
            },
            {
                field: 'riwayat_training',
                title: 'Riwayat training',
                sortable: 'asc',                          
            },   
            {
                field: 'account_no',
                title: 'No Rekening',
                sortable: 'asc',                          
            }, 
            {
                field: 'owner',
                title: 'Owner',
                sortable: 'asc',                          
            }, 
            {
                field: 'bank',
                title: 'Bank',
                sortable: 'asc',                          
            }, 
            {
                field: 'bank_branch',
                title: 'Bank branch',
                sortable: 'asc',                          
            },
            {
                field: 'agama',
                title: 'Agama',
                sortable: 'asc',                          
            },   
            {
                field: 'status_keluarga',
                title: 'Status keluarga',
                sortable: 'asc',                          
            }, 
            {
                field: 'tenaga_kerja',
                title: 'Tenaga kerja',
                sortable: 'asc',                          
            }, 
            {
                field: 'istri_suami',
                title: 'Istri suami',
                sortable: 'asc',                          
            }, 
            {
                field: 'anak',
                title: 'Anak',
                sortable: 'asc',                          
            },
            {
                field: 'copy_ijazah',
                title: 'Copy ijazah',
                sortable: 'asc',                          
            }, 
            {
                field: 'copy_transkrip_nilai',
                title: 'Copy transkrip nilai',
                sortable: 'asc',                          
            },
            {
                field: 'copy_ktp',
                title: 'Copy KTP',
                sortable: 'asc',                          
            },
            {
                field: 'copy_npwp',
                title: 'Copy NPWP',
                sortable: 'asc',                          
            },
            {
                field: 'cv',
                title: 'CV',
                sortable: 'asc',                          
            },
            {
                field: 'action',
                title: 'Action',
                sortable: false,
                width: 100,
                overflow: 'visible',
                textAlign: 'center',
                autoHide: false,
                template: function (row) {
                    var result ="";
                    <?php if($privilege->can_update){?>
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_edit" title="Group" data-toggle="modal" data-target="#modal_edit"><i class="fas fa-users" style="cursor:pointer;"></i></span>';
                        result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_modify" title="Edit" data-toggle="modal" data-target="#modal_modify"><i class="flaticon-edit-1" style="cursor:pointer;"></i></span>';
                    <?php }?>
                    <?php if($privilege->can_delete){?>
                        console.log(row)
                        if(row.is_manual_insert == 1)
                            result = result + '<span data-id="' + row.id + '" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md btn_delete" title="Delete" style="cursor:pointer;"><i class="flaticon2-trash"></i></span>';
                    <?php }?>
                    return result;
                }                        
            }
          ],
          layout:{
            header:true
          }
    }
    datatable = $('#kt_datatable').KTDatatable(option);
    datatable.on("kt-datatable--on-layout-updated",function(){
        initDTEvents();
    })
}
function initCustomRule(){
    $.validator.addMethod("add_password_contain_username", function(value, element) {
        username = $("#add_username").val()
        return !(username != "" && value.includes(username))
    }, "Password cannot contain username");

    $.validator.addMethod("edit_password_contain_username", function(value, element) {
        username = $("#edit_username").val()
        return !(username != "" && value.includes(username))
    }, "Password cannot contain username");
}

function refreshParamInput(){
    currentFilter = {
        group_id : $("#group_id").val()
    }
    if(datatable){
        var currentData = datatable.getOption("data");
        currentData.source.read.params = currentFilter;
    }
}

function initFilter(){
    $("#group_id").select2({
        placeholder: "All",
        width: '100%',
    });

    $("#group_id").on("change",function(){
        refreshParamInput();
        datatable.reload();
    });
}

function initAlert(){
    AlertUtil = {
        showSuccess : function(message,timeout){
            $("#success_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#success_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#success_alert").show();
            KTUtil.scrollTop();
        },
        hideSuccess : function(){
            $("#success_alert_dismiss").trigger("click");
        },
        showFailed : function(message,timeout){
            $("#failed_message").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss").trigger("click");
                },timeout)
            }
            $("#failed_alert").show();
            KTUtil.scrollTop();
        },
        hideFailed : function(){
            $("#failed_alert_dismiss").trigger("click");
        },
        showFailedDialogAdd : function(message,timeout){
            $("#failed_message_add").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_add").trigger("click");
                },timeout)
            }
            $("#failed_alert_add").show();
        },
        hideFailedDialogAdd : function(){
            $("#failed_alert_dismiss_add").trigger("click");
        },
        showFailedDialogEdit : function(message,timeout){
            $("#failed_message_edit").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_edit").trigger("click");
                },timeout)
            }
            $("#failed_alert_edit").show();
        },
        hideFailedDialogEdit : function(){
            $("#failed_alert_dismiss_edit").trigger("click");
        },
        showFailedDialogModify : function(message,timeout){
            $("#failed_message_modify").html(message);
            if(timeout != undefined){
                setTimeout(function(){
                    $("#failed_alert_dismiss_modify").trigger("click");
                },timeout)
            }
            $("#failed_alert_modify").show();
        },
        hideFailedDialogModify : function(){
            $("#failed_alert_dismiss_modify").trigger("click");
        }
    }
    $("#failed_alert_dismiss").on("click",function(){
        $("#failed_alert").hide();
    })
    $("#success_alert_dismiss").on("click",function(){
        $("#success_alert").hide();
    })
    $("#failed_alert_dismiss_add").on("click",function(){
        $("#failed_alert_add").hide();
    })
    $("#failed_alert_dismiss_edit").on("click",function(){
        $("#failed_alert_edit").hide();
    })
    $("#failed_alert_dismiss_modify").on("click",function(){
        $("#failed_alert_modify").hide();
    })
}
function initCreateForm(){
    var fileUploader1,fileUploader2,fileUploader3,fileUploader4,fileUploader5;
    Dropzone.autoDiscover = false;
    var fileUploader1 = new Dropzone("#add_copy_ijazah",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader1.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader1.on("removedfile", function (file) {
        $("#add_copy_ijazah_name").val("");
    });

    fileUploader1.on("success", function (file,response) {
        $("#add_copy_ijazah_name").val(response);
    });
    
    var fileUploader2 = new Dropzone("#add_copy_transkrip_nilai",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader2.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader2.on("removedfile", function (file) {
        $("#add_copy_transkrip_nilai_name").val("");
    });

    fileUploader2.on("success", function (file,response) {
        $("#add_copy_transkrip_nilai_name").val(response);
    });

    var fileUploader3 = new Dropzone("#add_copy_ktp",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader3.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader3.on("removedfile", function (file) {
        $("#add_copy_ktp_name").val("");
    });

    fileUploader3.on("success", function (file,response) {
        $("#add_copy_ktp_name").val(response);
    });
    var fileUploader4 = new Dropzone("#add_copy_npwp",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader4.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader4.on("removedfile", function (file) {
        $("#add_copy_npwp_name").val("");
    });

    fileUploader4.on("success", function (file,response) {
        $("#add_copy_npwp_name").val(response);
    });
 

    var fileUploader5 = new Dropzone("#add_cv",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader5.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader5.on("removedfile", function (file) {
        $("#add_cv_name").val("");
    });

    fileUploader5.on("success", function (file,response) {
        $("#add_cv_name").val(response);
    }); 
    //validator
    var validator = $( "#form_add" ).validate({
        ignore:[],
        errorPlacement: function (error, element){
            error.addClass("invalid-feedback")
            element.addClass('is-invalid')
            if (element.hasClass('select2'))
                error.insertAfter(element.next('span'))
            else
                error.insertAfter(element)
        },
        rules: {
            username: {
                required: true,
                minlength:6,
                maxlength:20,
                lettersonly: true,
                pattern: /^[a-z]*$/
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 12,
                add_password_contain_username: true,
                pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/,
            },
            employee_id: {
                required: true,
            },
            fullname: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            status_pegawai:{
                required: true,
            },
            gender:{
                required: true,
            },
            tempat_lahir:{
                required: true,
            },
            tgl_lahir:{
                required: true,
            },
            tgl_bergabung:{
                required: true,
            },
            departemen:{
                required: true,
            },
            posisi:{
                required: true,
            },  
            no_ktp:{
                required: true,
            },
            no_bpjs_ketenagakerjaan:{
                required: true,
            },
            pendidikan_terakhir:{
                required: true,
            },
            nama_sekolah:{
                required: true,
            },
            jurusan:{
                required: true,
            },
            tahun_lulus:{
                required: true,
            },
            no_seri_ijazah:{
                required: true,
            },
            no_hp:{
                required: true,
            },
            alamat_ktp:{
                required: true,
            },
            kode_pos:{
                required: true,
            },
            alamat_domisili:{
                required: true,
            },
            no_darurat:{
                required: true,
            },
            nama_keluarga:{
                required: true,
            },
            hubungan_keluarga:{
                required: true,
            },
            account_no:{
                required: true,
            },
            owner:{
                required: true,
            },
            bank:{
                required: true,
            },
            bank_branch:{
                required: true,
            },
            agama:{
                required: true,
            },
            status_keluarga:{
                required: true,
            },
            tenaga_kerja:{
                required: true,
            },
            istri_suami:{
                required: true,
            },
            anak:{
                required: true,
            },
            copy_ijazah_name:{
                required: true,
            },
            copy_transkrip_nilai_name:{
                required: true,
            },
            copy_ktp_name:{
                required: true,
            },
            cv_name:{
                required: true,
            }
        },
        messages: {
            username: {
                pattern: "Must be lowercase"
            },
            password: {
                pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character, 1 numeric character and 1 symbol (!@$*{},.\":)"
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    $('#add_tempat_lahir').select2({
        placeholder: "Please select a city",
        width: '100%'
    });

    $('#add_gender').select2({
        placeholder: "Please select a gender",
        width: '100%'
    });

    $('#add_departemen').select2({
        placeholder: "Please select a departemen",
        width: '100%'
    });

    $('#add_status_pegawai').select2({
        placeholder: "Please select a status",
        width: '100%'
    });

    $('#add_status_keluarga').select2({
        placeholder: "Please select a status",
        width: '100%'
    });

    $('.date-picker').datepicker({
        format: 'yyyy/mm/dd',
        endDate: '0d',
        todayHighlight: true,
        zIndexOffset: 9999,
        autoclose: true,
        clearBtn: true,
        todayBtn: true
    });
    //events
    $("#btn_add_submit").on("click",function(){
      var isValid = $( "#form_add" ).valid();
      if(isValid){
        KTApp.block('#modify .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/save"); ?>",
            data : $('#form_add').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modify .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_add').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogAdd(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_add .modal-content');
                AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#modal_add').on('hidden.bs.modal', function () {
       fileUploader1.removeAllFiles();
       fileUploader2.removeAllFiles();
       fileUploader3.removeAllFiles();
       fileUploader4.removeAllFiles();
       fileUploader5.removeAllFiles();
       validator.resetForm();
       $('#add_tempat_lahir').val(null).trigger('change');
       $('#add_tgl_lahir').val(null).trigger('change');
       $('#add_tgl_bergabung').val(null).trigger('change');
       $('#add_gender').val(null).trigger('change'); 
       $('#add_departemen').val(null).trigger('change'); 
       $('#add_status_pegawai').val(null).trigger('change'); 
       $('#add_status_keluarga').val(null).trigger('change');

    })

    return {
        fileUploader1:fileUploader1,
        fileUploader2:fileUploader2,
        fileUploader3:fileUploader3,
        fileUploader4:fileUploader4,
        fileUploader5:fileUploader5,
        validator:validator
    }
}
function initEditForm(){
    //validator
    var validator = $( "#form_edit" ).validate({
        ignore:[],
        rules: {
            // id: {
            //     required: true,
            //},
            fullname: {
                required: true,
            },
            institution: {
                required: true,
            },
            email: {
                required: true,
            },
            directorate: {
                required: true,
            },
            unit: {
                required: true,
            },
            structural_functional_position: {
                required: true,
            },
            structural_academic_position: {
                required: true,
            },
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_edit_submit").on("click",function(){
      var isValid = $( "#form_edit" ).valid();
      if(isValid){
        KTApp.block('#modal_edit .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/edit"); ?>",
            data : $('#form_edit').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_edit .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_edit').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailed(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_edit .modal-content');
                AlertUtil.showFailedDialogEdit("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })

    $('#edit_group').select2({
        placeholder: "Please select one or more group",
        width: '100%'
    });

    $('#modal_edit').on('hidden.bs.modal', function () {
       validator.resetForm();
       $('#edit_group').val(null).trigger('change');
    
    })

    var populateForm = function(userObject){
        $("#edit_id").val(userObject.id);

        var selectedValArr = [];
        for (var i = 0; i < userObject.roles.length; i++) {
            selectedValArr.push(userObject.roles[i].groupid);
        }
        $('#edit_group').val(selectedValArr);
        $('#edit_group').trigger('change');
    }
    
    return {
        validator:validator,
        populateForm:populateForm
    }
}
function initModifyForm(){
    var fileUploader1,fileUploader2,fileUploader3,fileUploader4,fileUploader5;
    Dropzone.autoDiscover = false;
    var fileUploader1 = new Dropzone("#modify_copy_ijazah",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader1.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader1.on("removedfile", function (file) {
        $("#modify_copy_ijazah_name").val("");
    });

    fileUploader1.on("success", function (file,response) {
        $("#modify_copy_ijazah_name").val(response);
    });
    
    var fileUploader2 = new Dropzone("#modify_copy_transkrip_nilai",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader2.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader2.on("removedfile", function (file) {
        $("#modify_copy_transkrip_nilai_name").val("");
    });

    fileUploader2.on("success", function (file,response) {
        $("#modify_copy_transkrip_nilai_name").val(response);
    });

    var fileUploader3 = new Dropzone("#modify_copy_ktp",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader3.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader3.on("removedfile", function (file) {
        $("#modify_copy_ktp_name").val("");
    });

    fileUploader3.on("success", function (file,response) {
        $("#modify_copy_ktp_name").val(response);
    });
    var fileUploader4 = new Dropzone("#modify_copy_npwp",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader4.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader4.on("removedfile", function (file) {
        $("#modify_copy_npwp_name").val("");
    });

    fileUploader4.on("success", function (file,response) {
        $("#modify_copy_npwp_name").val(response);
    });
 

    var fileUploader5 = new Dropzone("#modify_cv",{
        url:"<?php echo base_url($controller_full_path."/file_upload"); ?>",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: <?php echo $max_upload; ?>,
        addRemoveLinks: true,
        method:"post",
        acceptedFiles: "image/jpeg,image/png,application/pdf"
    });

    fileUploader5.on("sending", function (file, xhr, formData) {
        file.token = new Date().getTime();
        formData.append("token",file.token);
        token_file = file.token;
    });

    fileUploader5.on("removedfile", function (file) {
        $("#modify_cv_name").val("");
    });

    fileUploader5.on("success", function (file,response) {
        $("#modify_cv_name").val(response);
    }); 
    
    //validator
    var validator = $( "#form_modify" ).validate({
        ignore:[],
        errorPlacement: function (error, element){
            error.addClass("invalid-feedback")
            element.addClass('is-invalid')
            if (element.hasClass('select2'))
                error.insertAfter(element.next('span'))
            else
                error.insertAfter(element)
        },
        rules: {
            id: {
                required: true,
                number: true,
            },
            username: {
                required: true,
                minlength:6,
                maxlength:20,
                lettersonly: true,
                pattern: /^[a-z]*$/
            },
            employee_id: {
                required: true,
            },
            fullname: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            status_pegawai:{
                required: true,
            },
            gender:{
                required: true,
            },
            tempat_lahir:{
                required: true,
            },
            tgl_lahir:{
                required: true,
            },
            tgl_bergabung:{
                required: true,
            },
            departemen:{
                required: true,
            },
            posisi:{
                required: true,
            },  
            no_ktp:{
                required: true,
            },
            no_bpjs_ketenagakerjaan:{
                required: true,
            },
            pendidikan_terakhir:{
                required: true,
            },
            nama_sekolah:{
                required: true,
            },
            jurusan:{
                required: true,
            },
            tahun_lulus:{
                required: true,
            },
            no_seri_ijazah:{
                required: true,
            },
            no_hp:{
                required: true,
            },
            alamat_ktp:{
                required: true,
            },
            kode_pos:{
                required: true,
            },
            alamat_domisili:{
                required: true,
            },
            no_darurat:{
                required: true,
            },
            nama_keluarga:{
                required: true,
            },
            hubungan_keluarga:{
                required: true,
            },
            account_no:{
                required: true,
            },
            owner:{
                required: true,
            },
            bank:{
                required: true,
            },
            bank_branch:{
                required: true,
            },
            agama:{
                required: true,
            },
            status_keluarga:{
                required: true,
            },
            tenaga_kerja:{
                required: true,
            },
            istri_suami:{
                required: true,
            },
            anak:{
                required: true,
            },
            copy_ijazah_name:{
                required: true,
            },
            copy_transkrip_nilai_name:{
                required: true,
            },
            copy_ktp_name:{
                required: true,
            },
            cv_name:{
                required: true,
            }
        },
        messages: {
            username: {
                pattern: "Must be lowercase"
            },
            password: {
                pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character, 1 numeric character and 1 symbol (!@$*{},.\":)"
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    //events
    $("#btn_modify_submit").on("click",function(){
      var isValid = $( "#form_modify" ).valid();
      if(isValid){
        KTApp.block('#modal_modify .modal-content', {});
        $.ajax({
            type : 'POST',
            url : "<?php echo base_url($controller_full_path."/modify"); ?>",
            data : $('#form_modify').serialize(),
            dataType : "json",
            success : function(data,status){
                KTApp.unblock('#modal_modify .modal-content');
                if(data.status == true){
                    datatable.reload();
                    $('#modal_modify').modal('hide');
                    AlertUtil.showSuccess(data.message,5000);
                }else{
                    AlertUtil.showFailedDialogModify(data.message);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown){
                KTApp.unblock('#modal_modify .modal-content');
                AlertUtil.showFailedDialogModify("Cannot communicate with server please check your internet connection");
            }
        });  
      }
    })
    $('#modify_tempat_lahir').select2({
        placeholder: "Please select a city",
        width: '100%'
    });

    $('#modify_gender').select2({
        placeholder: "Please select a gender",
        width: '100%'
    });

    $('#modify_departemen').select2({
        placeholder: "Please select a departemen",
        width: '100%'
    });

    $('#modify_status_pegawai').select2({
        placeholder: "Please select a status",
        width: '100%'
    });

    $('#modify_status_keluarga').select2({
        placeholder: "Please select a status",
        width: '100%'
    });

    $('.date-picker').datepicker({
        format: 'yyyy/mm/dd',
        endDate: '0d',
        todayHighlight: true,
        zIndexOffset: 9999,
        autoclose: true,
        clearBtn: true,
        todayBtn: true
    });


    $('#modal_modify').on('hidden.bs.modal', function () {
       fileUploader1.removeAllFiles();
       fileUploader2.removeAllFiles();
       fileUploader3.removeAllFiles();
       fileUploader4.removeAllFiles();
       fileUploader5.removeAllFiles();
       validator.resetForm();
       $('#modify_tempat_lahir').val(null).trigger('change');
       $('#modify_tgl_lahir').val(null).trigger('change');
       $('#modify_tgl_bergabung').val(null).trigger('change');
       $('#modify_gender').val(null).trigger('change'); 
       $('#modify_departemen').val(null).trigger('change'); 
       $('#modify_status_pegawai').val(null).trigger('change'); 
       $('#modify_status_keluarga').val(null).trigger('change');

    })

    var populateForm = function(userObject){
        $("#modify_id").val(userObject.id)
        $("#modify_username").val(userObject.username)
        $("#modify_employee_id").val(userObject.employeeid)
        $("#modify_fullname").val(userObject.fullname)
        $("#modify_email").val(userObject.email)
        $("#modify_status_pegawai").val(userObject.status_pegawai).trigger('change');
        $("#modify_gender").val(userObject.gender).trigger('change');
        $("#modify_tempat_lahir").val(userObject.tempat_lahir).trigger('change');
        $("#modify_tgl_lahir").val(userObject.tgl_lahir);
        $("#modify_tgl_bergabung").val(userObject.tgl_bergabung);
        $("#modify_departemen").val(userObject.departemen);
        $("#modify_posisi").val(userObject.posisi);
        $("#modify_npwp").val(userObject.npwp);
        $("#modify_no_ktp").val(userObject.no_ktp);
        $("#modify_no_bpjs_kesehatan").val(userObject.no_bpjs_kesehatan);
        $("#modify_nama_faskes").val(userObject.nama_faskes);
        $("#modify_no_bpjs_ketenagakerjaan").val(userObject.no_bpjs_ketenagakerjaan);
        $("#modify_pendidikan_terakhir").val(userObject.pendidikan_terakhir);
        $("#modify_nama_sekolah").val(userObject.nama_sekolah);
        $("#modify_jurusan").val(userObject.jurusan);
        $("#modify_ipk").val(userObject.ipk);
        $("#modify_tahun_lulus").val(userObject.tahun_lulus);
        $("#modify_no_seri_ijazah").val(userObject.no_seri_ijazah);
        $("#modify_no_hp").val(userObject.no_hp);
        $("#modify_no_tlp_rumah").val(userObject.no_tlp_rumah);
        $("#modify_alamat_ktp").val(userObject.alamat_ktp);
        $("#modify_kode_pos").val(userObject.kode_pos);
        $("#modify_alamat_domisili").val(userObject.alamat_domisili);
        $("#modify_no_darurat").val(userObject.no_darurat);
        $("#modify_nama_keluarga").val(userObject.nama_keluarga);
        $("#modify_hubungan_keluarga").val(userObject.hubungan_keluarga);
        $("#modify_keahlian_khusus").val(userObject.keahlian_khusus);
        $("#modify_pengalaman_project").val(userObject.pengalaman_project);
        $("#modify_riwayat_training").val(userObject.riwayat_training);
        $("#modify_account_no").val(userObject.account_no);
        $("#modify_owner").val(userObject.owner);
        $("#modify_bank").val(userObject.bank);
        $("#modify_bank_branch").val(userObject.bank_branch);
        $("#modify_agama").val(userObject.agama);
        $("#modify_status_keluarga").val(userObject.status_keluarga).trigger('change');
        $("#modify_tenaga_kerja").val(userObject.tenaga_kerja);
        $("#modify_istri_suami").val(userObject.istri_suami);
        $("#modify_anak").val(userObject.anak);
       
        var copy_ijazah = userObject.copy_ijazah.replace(/^.*[\\\/]/, '')
        var mockFile1 = { name: copy_ijazah, size: 12345 };
        fileUploader1.emit("addedfile", mockFile1);
        fileUploader1.files.push(mockFile1);
        $("#modify_copy_ijazah").val(copy_ijazah);
        
        var copy_transkrip_nilai = userObject.copy_transkrip_nilai.replace(/^.*[\\\/]/, '')
        var mockFile2 = { name: copy_transkrip_nilai, size: 12345 };
        fileUploader2.emit("addedfile", mockFile2);
        fileUploader2.files.push(mockFile2);
        $("#modify_copy_transkrip_nilai").val(copy_transkrip_nilai);

        var copy_ktp = userObject.copy_ktp.replace(/^.*[\\\/]/, '')
        var mockFile3 = { name: copy_ktp, size: 12345 };
        fileUploader3.emit("addedfile", mockFile3);
        fileUploader3.files.push(mockFile3);
        $("#modify_copy_ktp").val(copy_ktp);

        var copy_npwp = userObject.copy_npwp.replace(/^.*[\\\/]/, '')
        var mockFile4 = { name: copy_npwp, size: 12345 };
        fileUploader4.emit("addedfile", mockFile4);
        fileUploader4.files.push(mockFile4);
        $("#modify_copy_npwp").val(copy_npwp);
        
        var cv = userObject.cv.replace(/^.*[\\\/]/, '')
        var mockFile5 = { name: cv, size: 12345 };
        fileUploader5.emit("addedfile", mockFile5);
        fileUploader5.files.push(mockFile5);
        $("#modify_cv").val(cv);
    }

    return {
        fileUploader1:fileUploader1,
        fileUploader2:fileUploader2,
        fileUploader3:fileUploader3,
        fileUploader4:fileUploader4,
        fileUploader5:fileUploader5,
        validator:validator,
        populateForm:populateForm
    }
    
}
function initImportForm(){
    //validator
    var validator = $( "#form_import" ).validate({
        ignore:[],
        rules: {
            file_csv: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {   
            KTUtil.scrollTop();
        }
    });   

    $('#modal_import').on('hidden.bs.modal', function () {
       validator.resetForm();
    })

    return {
        validator:validator
    }
}

function Export(){
    <?php if($privilege->can_read){?>
        var currdate = new Date();
        var date = moment(currdate).format('YYYY-MM-DD H:mm:s');
        var filename = "User_"+date+".csv";
        var search = $('#generalSearch').val();
        $.ajax({
            url: "<?php echo base_url($controller_full_path."/export");?>",
            type: 'post',
            dataType: 'html',
            data: { search: search  },
            success: function(data) {
                var downloadLink = document.createElement("a");
                var fileData = ['\ufeff'+data];
                var blobObject = new Blob(fileData,{type: "text/csv;charset=utf-8;"});
                var url = URL.createObjectURL(blobObject);
                downloadLink.href = url;
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot export this data",
                type: 'info',
            });
    <?php } ?>
    }

    function Print(){
    <?php if($privilege->can_read){?>
        var search = $('#generalSearch').val();
        KTApp.blockPage();
        $.ajax({
            
            url: "<?php echo base_url($controller_full_path."/print_users");?>",
            type: 'post',
            dataType: 'json',
            data: { search: search  },
            success: function (response, status) {
                    KTApp.unblockPage();
                    if (response.status == true) {
                        //populate form
                        var win = window.open("", "_blank");                    
                        win.document.write(response.data);
                        win.document.close();  
                        setTimeout(function() {
                            win.print();
                            win.close();
                        }, 250);
                    } else {
                        AlertUtil.showFailed(response.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    KTApp.unblockPage();
                    AlertUtil.showFailed("Cannot communicate with server please check your internet connection");
                }
        })    
    <?php }else{ ?>
        swal.fire({
                title: 'Infomation',
                text: "You cannot Print this data",
                type: 'info',
            });
    <?php } ?>
    }

jQuery(document).ready(function() { 
    initDataTable();
    initCustomRule();
    editForm = initEditForm();
    createForm = initCreateForm()
    modifyForm = initModifyForm()
    initAlert();
    initFilter();
    importForm = initImportForm();
    importErr = '<?= isset($import_error) ? $import_error : '' ?>'
    if(importErr) AlertUtil.showSuccess(importErr)
});

</script>
