<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>        
    
<div id="container" class="border">
  <table class="table table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col"><b> NIP </b></th>
            <th scope="col"><b> Nama Pegawai  </b></th>
            <th scope="col"><b> Status Pegawai  </b></th>
            <th scope="col"><b> Departemen  </b></th>
            <th scope="col"><b> Posisi  </b></th>
            <th scope="col"><b> No Handphone  </b></th>
            <th scope="col"><b> Email </b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $item) {
            $group = isset($item->groups) ? $item->groups : '';
            echo '<tr>
                    <td>'.$item->employeeid.'</td>
                    <td>'.$item->fullname .'</td>
                    <td>'.$item->status_pegawai .'</td>
                    <td>'.$item->departemen_name .'</td>
                    <td>'.$item->posisi .'</td>
                    <td>'.$item->no_hp.'</td>
                    <td>'.$item->email.'</td>
            </tr>';
        }
        ?>
    </tbody>
  </table>
</div>
</body>
</html>