<!--begin::Modal-->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Group <?php echo $breadcrumb->child;?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_edit" class="form-horizontal">
                    <input type="hidden" id="edit_id" name="id" />
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 full-right">
                                <div class="form-group">
                                    <label>Group</label>
                                    <select class="form-control" name="group[]" id="edit_group" multiple="multiple">
                                        <option></option>
                                        <?php 
                                            foreach($groups as $group)
                                            {
                                                echo '<option value="'.$group->group_id.'">'.$group->group_name.'</option>';                        
                                            }                        
                                            ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_edit_submit">Save changes</button>
            </div>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->
<script type="text/javascript">
    $('#edit_group').select2({
        placeholder: "Please select a Gruop Name",
        width: '100%'
    });
</script>