        <header class="bg-secondary text-white p-5 clearfix">
            <div class="container text-left">
                <h1 class="title"><?php echo $title; ?></h1>
                <p class="lead"><?php echo $sub_title; ?></p>
            </div>
        </header>
        <section class="p-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col col-md-12">
                        <table class="table table-bordered table-striped dataTable" style="width:100%">
                            <thead>
                                <tr>
                                    <th><h5>Berkas</h5></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($data)) {
                            foreach($data as $row){ ?>
                                <tr>
                                    <td><?php echo "<h6>".$row->title."</h6>".(!empty($row->description) ? "<p>".$row->description."</p>" : ""); ?></td>
                                    <td class="text-right"><?php echo ($row->file_status=="1" ? (!empty($row->file_path) ? "<a href=".base_url($row->file_path)." target='_blank' class='text-danger'>Download</a>" : "") : "not available"); ?></td>
                                </tr>
                            <?php }}?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>