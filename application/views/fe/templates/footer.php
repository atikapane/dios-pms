        <section>
            <footer class="font-small footer-dark text-light">
                <div class="container mb-5">
                    <div class="row">      
                        <?php $footers = $this->web_app_model->get_data('landing_footer',[],'','priority'); ?>
                        <?php foreach($footers as $footer): ?>
                            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-5 text-link-footer">
                                <h6 class="text-uppercase mb-4 font-weight-bold"><?= strtoupper($footer->title) ?></h6>
                                <?= $footer->content ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="container-fluid">
                    <hr/>
                    <div class="text-center pb-2 mt-2">© Copyright Telkom University <?php echo date('Y');?>. Design & Development by Telkom University</a>
                    </div>
                </div>

            </footer>
        </section>
    </body>
</html>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/jquery/jquery-3.4.1.min.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/popper.js/dist/umd/popper.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/slick/slick.min.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/js/owl.carousel.min.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/js/clamp.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/js/main.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/bootstrap-datepicker/dist/locales/bootstrap-datepicker.en-IE.min.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script>
    $(function () {
        $('select:not(#program_course_filter, #course_video_filter,#exam_topic,.exam_quiz, #status_filter, #profile_gender, #profile_city)').each(function () {
            $(this).select2({
                theme: 'bootstrap4',
                width: 'style',
                placeholder: $(this).attr('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        });

        $('#search_icon').click(function(){
            $('#search_form').submit()
        })
    });
    var AlertUtil;
    var loginForm;
    <?php $redir = $this->input->get('redir'); ?>
    var redir = "<?php echo $redir; ?>";

    let url_google = "<?php echo base_url("UsersAuth/login_google");?>";
    let url_facebook = "<?php echo base_url("UsersAuth/login_facebook");?>";

    $("#btn_login_google_popup").on('click',function () {
        window.location.href = url_google;
    });

    $("#btn_login_facebook_popup").on('click',function () {
        window.location.href = url_facebook;
    });



    function initAlert(){
        AlertUtil = {
            showFailedDialog : function(message,timeout){
                $("#failed_message_popup").text(message);
                $("#failed_alert_popup").show();
            }
        };
    }

    function initLoginForm(){
        // validator.resetForm();
        var validator = $('#form_ocw_user_login_popup').validate({
            errorClass:'error-field',
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true
                }
            }
        });
        
        $("#btn_login_popup").on("click", function () {
            $("#failed_alert_popup").hide();
            $("#failed_alert_server_popup").hide();
            var isValid = $( "#form_ocw_user_login_popup" ).valid();
            if(isValid){
                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url().'UsersAuth/processLogin'; ?>",
                    data: $("#form_ocw_user_login_popup").serialize(),
                    dataType:"json",
                    success:function (data) {
                        if(data.status == true){
                            if(redir != ""){
                                window.location.href = redir;
                            }else{
                                window.location.href = "<?php echo site_url(); ?>";
                            }
                        }else{
                            AlertUtil.showFailedDialog(data.message);
                        }
                    },error:function () {
                        AlertUtil.showFailedDialog("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        return {
            validator:validator
        }
    }

    $(document).ready(function (){
        $(".prevent-default").click(function(e){
            e.preventDefault()
        });

        loginForm = initLoginForm();
        initAlert();
        errMessage = "<?= $this->session->flashdata('message_error') ?>";
        if(errMessage) swal.fire({text:errMessage,confirmButtonColor:"darkred"})
    });
</script>