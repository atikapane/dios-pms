<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/datatables/jquery.dataTables.js"></script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    if ($(".dataTable").length){
        $('.dataTable').DataTable({
            "order": [],
            "lengthChange": false,
            "columnDefs": [ 
            {
              "targets": '_all',
              "searchable": true,
              "orderable": false,
            } ],
            "scrollX": true,
        });
    }
</script>