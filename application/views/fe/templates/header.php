<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $this->config->item("title")." | ".$title;?></title>
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/slick/slick.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/libs/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/css/slider.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/select2/dist/css/select2.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>fe/css/select2-bootstrap4.css">
    <link rel="stylesheet" href="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/sweetalert2/dist/sweetalert2.css">

    <link rel="shortcut icon" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <link rel="manifest" href="favico/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $this->config->item("ASSET_PATH");?>fe_v2/assets/images/logo-telkom.png">
    <!-- <meta name="msapplication-TileImage" content="favico/ms-icon-144x144.png"> -->
    <meta name="theme-color" content="#ffffff">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>

    <style>
        @media(max-width: 1023px) {
            #sm-res {
                display: block!important;
            }

            #big-res{
                display:none!important;
            }
        }

        @media(min-width: 1024px) {
            #sm-res {
                display: none!important;
            }

            #big-res{
                display:block!important;
            }
        }

        #sm-res{
            display:none;
            background-color: #f1f4f5!important;
        }

        #big-res{
            display:none;
        }

        .nav-bar-padding{
            padding-top: 100px;
        }
        .nav-icon{
            padding-top: 0px;
            padding-bottom: 0px;
            cursor: pointer;
            color:#fff!important;
        }
        .bg-light{
            background-color: #f1f4f5!important;
        }

        .content-top{
            background-color:darkred;
        }

        .sosmed-list{
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            display:flex;
            align-items: end;
            min-width: 220px;
        }

        .sosmed-list li{
            margin:5px;
            font-size: 20px;

        }

        .sosmed-item{
            list-style: none;
        }

        .sosmed-item a{
            color: rgba(255,255,255,255)!important;
        }

        .sosmed-item a:hover{
            color: rgba(255,255,255,255)!important;
        }

        .sosmed-item-sub a{
            color: darkred!important;
        }

        .sosmed-item-sub a:hover{
            color: #a33e00 !important;
        }

        .navbar-toggler{
            border:0px!important;
        }

        .search-content input{
            border-right: 0;
            outline: none;
            border-radius: 50px 0 0 50px;
            padding-right: 0px;
           
        }

        .search-content input[type=search]:focus{
            box-shadow:none!important;
            outline: none!important;
            border-bottom: 1px solid #d2d4d5;
            border-top: 1px solid #d2d4d5;
            border-left:1px solid #d2d4d5;
        }

        .search-content div{
            border-radius: 0 50px 50px 0;
        }

        .search-content div span{
            background-color: #FFF;
            border-left: 0;
            border-radius: 0 50px 50px 0;
            color:darkred;
            cursor:pointer;
        }

        @media (min-width: 320px) {
            .navbar-menu-search{
                width: 100px!important;
            }

            .nav-link{
                padding: .5rem .5rem;
            }
        }

        @media (min-width: 360px) {
            /*.navbar-menu-search{
                width: 150px!important;
            }*/
        }

        @media (min-width: 540px) {
            .navbar-menu-search{
                width: 250px!important;
            }
        }

        .navbar-menu-expand{
            margin: auto!important;
        }

        .modal-content-login{
            height: 100%;
            width: 100%;
            position: fixed;
        }
        #login-modal{
            top: 50% !important;
            z-index: 99999 !important;
            transform: translateY(-50%) !important;
            background-color: rgba(0,0,0,0.6);
        }
        #login-modal .modal-close{
            float:right;
            cursor:pointer;
            color:darkred;
            text-decoration: none
        }
        #login-modal h3{
            color:darkred;

        }
        .navbar {
            padding:.5rem 0rem;
        }
        .input-group {
            min-width: 120px;
            margin-top:5px;
        }
    </style>
</head>
<body class="nav-bar-padding">
<?php $redir = $this->input->get('redir'); ?>
<div id="login-modal" class="modal fixed-top" <?= !empty($redir) ? 'style="display: block"' : '' ?>>
  <div class="modal-content-login">

    <div class="login-wrapper mb-5 py-3 px-3">
    <a href="#" class="modal-close pull-right" id="close-login-modal">Close (X)</a>
    <h3 class="mb-3">Login</h3>
    <form action="#" id="form_ocw_user_login_popup">
        <div class="form-group">
            <label for="inputEmailPopup">Email*</label>
            <input type="email" placeholder="Email" class="form-control" id="inputEmailPopup" name="email">
            <?php if(!empty($redir)): ?>
            <input type="hidden" name="redir" value="<?php echo $redir?>"/>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label for="inputPasswordPopup">Password*</label>
            <input type="password" placeholder="Password" class="form-control" name="password" id="inputPasswordPopup">
        </div>
        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="rememberMePopup" name="remember" value="1">
                <label class="form-check-label" for="rememberMePopup">
                    Remember me
                </label>
            </div>
        </div>
        <div class="alert alert-danger fade show" role="alert" id="failed_alert_popup" style="display: none;">
            <div class="alert-text" id="failed_message_popup"></div>
        </div>
        <?php $message_error = $this->session->flashdata('message_error');?>
        <div class="alert alert-danger fade show" role="alert" id="failed_alert_server_popup" style="<?php if($message_error){echo '';}else{echo 'display: none';}?>">
            <div class="alert-text" id="failed_message_server_popup"><?php echo $message_error;?></div>
        </div>
        <button type="button" id="btn_login_popup" class="btn btn-primary-red w-100">Log In</button>
    </form>
    <div class="mt-3 d-flex">
        <button class="btn btn-primary-google w-100 mr-3" id="btn_login_google_popup">Log In With Google</button>
        <button class="btn btn-primary-fb w-100" id="btn_login_facebook_popup">Log In With Facebook</button>
    </div>
    <div class="mt-3">
        <div class="d-flex">
            <div class="ml-auto">
                <div class="d-flex flex-column">
                    <button class="btn-link-red text-right" onClick="window.location = '<?= base_url('ForgotPassword') ?>';">Forgot
                        password?</button>
                    <button class="btn-link-red text-right" onClick="window.location = '<?= base_url('Register') ?>';">New To Celoe?
                        Create an Account</button>
                    <button class="btn-link-red text-right" onClick="window.location = '<?= base_url('auth') ?>';">CeLOE Dashboard</button>
                </div>
            </div>
        </div>
    </div>
</div>
  </div>
</div>
<div id="loading_overlay" style="display:none;"> 
    <div class="loadingio-spinner-pulse-rwldebfbb3d"><div class="ldio-4wzjjylhqlv">
        <div></div><div></div><div></div>
        </div></div>
        <style type="text/css">
        @keyframes ldio-4wzjjylhqlv-1 {
          0% { top: 36px; height: 128px }
          50% { top: 60px; height: 80px }
          100% { top: 60px; height: 80px }
        }
        @keyframes ldio-4wzjjylhqlv-2 {
          0% { top: 41.99999999999999px; height: 116.00000000000001px }
          50% { top: 60px; height: 80px }
          100% { top: 60px; height: 80px }
        }
        @keyframes ldio-4wzjjylhqlv-3 {
          0% { top: 48px; height: 104px }
          50% { top: 60px; height: 80px }
          100% { top: 60px; height: 80px }
        }
        .ldio-4wzjjylhqlv div { position: absolute; width: 30px }.ldio-4wzjjylhqlv div:nth-child(1) {
          left: 35px;
          background: #b83128;
          animation: ldio-4wzjjylhqlv-1 1s cubic-bezier(0,0.5,0.5,1) infinite;
          animation-delay: -0.2s
        }
        .ldio-4wzjjylhqlv div:nth-child(2) {
          left: 85px;
          background: #ce6260;
          animation: ldio-4wzjjylhqlv-2 1s cubic-bezier(0,0.5,0.5,1) infinite;
          animation-delay: -0.1s
        }
        .ldio-4wzjjylhqlv div:nth-child(3) {
          left: 135px;
          background: #e9b6ae;
          animation: ldio-4wzjjylhqlv-3 1s cubic-bezier(0,0.5,0.5,1) infinite;
          animation-delay: undefineds
        }

        .loadingio-spinner-pulse-rwldebfbb3d {
          width: 200px;
          height: 200px;
          display: inline-block;
          overflow: hidden;
          top: 50%;
          left: 50%;
          position: absolute;
          transform: translate(-50%, -50%);
        }
        .ldio-4wzjjylhqlv {
          width: 100%;
          height: 100%;
          position: relative;
          transform: translateZ(0) scale(1);
          backface-visibility: hidden;
          transform-origin: 0 0; /* see note above */
        }
        .ldio-4wzjjylhqlv div { box-sizing: content-box; }
        /* generated by https://loading.io/ */
        </style>
    </div> 
    <div class="fixed-top" id="big-res">
        <div class="row">
            <div class="col col-12">
                <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: darkred;">
                    <div class="container">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link nav-icon" href="https://www.facebook.com/telkomuniversity" target="_blank">
                                    <i class="fab fa-facebook mr-2"> </i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-icon" href="https://www.youtube.com/channel/UCCEr8He96NhAxVe_YL32Rww" target="_blank">
                                    <i class="fab fa-youtube mr-2"> </i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-icon" href="https://www.instagram.com/telkomuniversity" target="_blank">
                                    <i class="fab fa-instagram mr-2"> </i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-icon" href="https://plus.google.com/u/0/102341623351522188224" target="_blank">
                                    <i class="fab fa-google-plus mr-2"> </i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-icon" href="https://id.linkedin.com/company/telkom-university" target="_blank">
                                    <i class="fab fa-linkedin mr-2"> </i>
                                </a>
                            </li>
                        </ul>
                        <?php $session =  $this->ion_auth->user()->row();
                            if(empty($session)){?>
                                <ul class="navbar-nav navbar-right">
                                    <li class="nav-item <?php echo $active_page == "register"? 'active':'';?>">
                                        <a class="nav-link" style="padding: 0px 0.5rem;" href="<?php echo base_url().'register'?>">Register</a>
                                    </li>
                                    <li class="nav-item secret <?php echo $active_page == "login"? 'active':'';?>">
                                        <a class="nav-link" style="padding: 0px 0.5rem;" href="<?php echo base_url().'UsersAuth'?>">Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link modal-trigger open-login-modal" href="#" style="padding: 0 .5rem;">Login</a>
                                    </li>
                                </ul>
                        <?php }else{?>
                                <ul class="navbar-nav navbar-right">
                                    <li class="nav-item <?php echo $active_page == "account"? 'active':'';?>">
                                        <a class="nav-link" href="<?php echo base_url('account');?>">My Account</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('UsersAuth/processLogout');?>">Logout</a>
                                    </li>
                                </ul>
                        <?php }?>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row shadow-sm">
            <div class="col col-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container">
                        <a class="navbar-brand" href="<?php echo base_url().'home'?>">
                            <img src="<?php echo $this->config->item("ASSET_PATH");?>media/img/celoe_small.png" alt="" srcset="" style="width: 90px; height: 32px;">
                        </a>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="nav-item <?php echo $active_page == "about"? 'active':'';?>">
                                <a class="nav-link" href="<?php echo base_url().'about'?>">About Us</a>
                            </li>
                            <li class="nav-item <?php echo $active_page == "listCourse"? 'active':'';?>">
                                <a class="nav-link" href="<?php echo base_url().'course/catalog';?>">Course</a>
                            </li>
                            <li class="nav-item <?php echo $active_page == "news"? 'active':'';?>">
                                <a class="nav-link" href="<?php echo base_url().'news'?>">News</a>
                            </li>
                            <li class="nav-item <?php echo $active_page == "download"? 'active':'';?>">
                                <a class="nav-link" href="<?php echo base_url().'download'?>">Download</a>
                            </li>
                            <li class="nav-item <?php echo $active_page == "tutorial"? 'active':'';?>">
                                <a class="nav-link" href="<?php echo base_url().'tutorial'?>">Tutorial</a>
                            </li>
                            <li class="nav-item <?php echo $active_page == "cart"? 'active':'';?>">
                                <a class="nav-link" href="<?= empty($session) ? current_url().'?redir='.urlencode(base_url('cart')) : base_url('cart')?>"><i class="fas fa-shopping-cart fa-flip-horizontal"></i></a>
                            </li>
                            <li class="nav-item">
                                <form id="form_search" method="post" action="<?= base_url('course/catalog') ?>">
                                    <div class="input-group ml-3 search-content">
                                        <input type="search" class="form-control" placeholder="Search Course" name="s">
                                        <div class="input-group-append" id="search_icon">
                                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="fixed-top" id="sm-res">
        <div class="row">
            <div class="col col-12 content-top">
                <div class="container">
                    <div class="col-12">
                        <ul class="sosmed-list">
                            <li class="sosmed-item">
                                <a class="nav-link nav-icon" href="https://www.facebook.com/telkomuniversity" target="_blank">
                                    <i class="fab fa-facebook mr-2"> </i>
                                </a>
                            </li>
                            <li class="sosmed-item">
                                <a class="nav-link nav-icon" href="https://www.youtube.com/channel/UCCEr8He96NhAxVe_YL32Rww" target="_blank">
                                    <i class="fab fa-youtube mr-2"> </i>
                                </a>
                            </li>
                            <li class="sosmed-item">
                                <a class="nav-link nav-icon" href="https://www.instagram.com/telkomuniversity" target="_blank">
                                    <i class="fab fa-instagram mr-2"> </i>
                                </a>
                            </li>
                            <li class="sosmed-item">
                                <a class="nav-link nav-icon" href="https://plus.google.com/u/0/102341623351522188224" target="_blank">
                                    <i class="fab fa-google-plus mr-2"> </i>
                                </a>
                            </li>
                            <li class="sosmed-item">
                                <a class="nav-link nav-icon" href="https://id.linkedin.com/company/telkom-university" target="_blank">
                                    <i class="fab fa-linkedin mr-2"> </i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row shadow-sm">
            <div class="col col-12">
                <div class="container bg-light">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="<?php echo base_url().'home'?>">
                            <img src="<?php echo $this->config->item("ASSET_PATH");?>media/img/celoe_small.png" alt="" srcset="" style="width: 90px; height: 32px;">
                        </a>
                        <ul class="sosmed-list">
                            <li class="sosmed-item-sub">
                            <a class="nav-link" href="<?= empty($session) ? current_url().'?redir='.urlencode(base_url('cart')) : base_url('cart')?>"><i class="fas fa-shopping-cart fa-flip-horizontal"></i></a>
                            </li>
                            <li class="sosmed-item-sub navbar-menu-search">
                                <form id="form_search" method="post" action="<?= base_url('course/catalog') ?>">
                                    <div class="input-group search-content">
                                        <input type="search" class="form-control" placeholder="Search Course" name="s">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </form>
                            </li>
                            <li class="sosmed-item-sub navbar-menu-expand">
                                <a href="#!" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="fa fa-ellipsis-v"></span>
                                </a>
                            </li>
                        </ul>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item <?php echo $active_page == "about"? 'active':'';?>">
                                    <a class="nav-link" href="<?php echo base_url().'about'?>">About Us</a>
                                </li>
                                <li class="nav-item <?php echo $active_page == "listCourse"? 'active':'';?>">
                                    <a class="nav-link" href="<?php echo base_url().'course/catalog';?>">Course</a>
                                </li>
                                <li class="nav-item <?php echo $active_page == "news"? 'active':'';?>">
                                    <a class="nav-link" href="<?php echo base_url().'news'?>">News</a>
                                </li>
                                <li class="nav-item <?php echo $active_page == "download"? 'active':'';?>">
                                    <a class="nav-link" href="<?php echo base_url().'download'?>">Download</a>
                                </li>
                                <li class="nav-item <?php echo $active_page == "tutorial"? 'active':'';?>">
                                    <a class="nav-link" href="<?php echo base_url().'tutorial'?>">Tutorial</a>
                                </li>
                            </ul>
                            <?php $session = $this->session->userdata('user_data');
                            if(!$session){?>
                                <ul class="navbar-nav navbar-right">
                                    <li class="nav-item <?php echo $active_page == "login"? 'active':'';?>">
                                        <a class="nav-link modal-trigger open-login-modal" href="#">Login</a>
                                    </li>
                                    <li class="nav-item <?php echo $active_page == "register"? 'active':'';?>">
                                        <a class="nav-link" href="<?php echo base_url().'register'?>">Register</a>
                                    </li>
                                </ul>
                            <?php }else{?>
                                <ul class="navbar-nav navbar-right">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('account');?>">My Account</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('UsersAuth/processLogout');?>">Logout</a>
                                    </li>
                                </ul>
                            <?php }?>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    
