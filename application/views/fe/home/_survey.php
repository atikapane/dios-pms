<div class="modal" tabindex="-1" role="dialog" id="modal_survey">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Survey</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>There's survey <span id="survey_name"></span> ongoing, do you want to participate?</p>
      </div>
      <div class="modal-footer">
        <div class="custom-control custom-checkbox mr-auto">
            <input type="checkbox" class="custom-control-input" id="not_show">
            <label class="custom-control-label" for="not_show">Don't show this again</label>
        </div>

        <button type="button" class="btn btn-primary" id="btn_survey_participate" style="background: darkred;">Participate</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>