<?php $this->load->view('fe/templates/header'); ?>
<header>
    <div id="carouselIndicators" class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="false">
        <ol class="carousel-indicators">

            <?php
            $class = "active";
            $iterator = 0;
            foreach ($banner as $item) {
            ?>
                <li data-target="#carouselIndicators" data-slide-to="<?php echo $iterator; ?> " class="<?php echo $class; ?> "></li>
            <?php
                //only first class needed to be active
                $class = "";
            };
            ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php
            $class = "active";
            foreach ($banner as $item) {
                $img_name = basename($item->img_path);
                $item->img_path = str_replace($img_name, urlencode($img_name), $item->img_path);
            ?>
                <div class="carousel-item <?php echo $class; ?> image-link" style="background-image: url(<?php echo base_url() . $item->img_path; ?>)">
                    <?php if (!empty($item->url)) { ?>
                        <a href="<?php echo $item->url; ?>"></a>
                    <?php } ?>
                    <div class="carousel-caption d-none d-md-block">
                        <h2 class="display-4"></h2>
                        <p class="lead"></p>
                    </div>
                </div>
            <?php
                //only first class needed to be active
                $class = "";
            };
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

<?php if (count($news) > 0) { ?>
<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col col-md-12 col-sm-12 col-12">
                <h1 class="display-4">Headline News</h1>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                <div class="row">
                    <div class="col col-12 text-right mb-3">
                        <a href="<?php echo base_url() . 'News' ?>" class="btn btn-danger">View All</a>
                    </div>
                </div>
                <div class="row">
                    <?php foreach ($news as $news_item) {  ?>
                        <div class="col col-xs-12 col-sm-12 col-md-6 col-lg-3 mb-5 col-12">
                            <div class="card border-1 border rounded-0">
                                <img src="<?php echo $news_item->thumbnail; ?>" class="img-thumbnail-custom">
                                <div class="card-body text-slide">
                                    <h6><?php echo $news_item->title; ?></h6>
                                    <a href="<?php echo base_url() . 'News/news_detail/' . $news_item->slug; ?>" class="btn btn-outline-danger">Read More</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php } ?>

<section class="pb-5">
    <div class="container mb-3">
        <h1 class="display-4">Category Course</h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <div class="col col-12 text-right">
            <a href="<?php echo base_url('Category'); ?>" class="btn btn-danger">View All</a>
        </div>
    </div>

    <div class="regular slider" style="object-fit: none;">
        <?php foreach ($category_list as $category) { ?>
            <a href="<?php echo base_url('Course/list') . "?category_id=" . $category->ocw_category_id; ?>">
                <div class="content-list border" style="transform: none;" data-id="<?php echo "$category->img_path"; ?>">
                    <div style="width:100%; height: 100px;">
                        <!-- <img src="<?php //echo base_url(); ?>assets/media/slider/1.jpg" class="card-img-top p-3">  -->
                        <img src="<?php echo base_url() . $category->img_path; ?>" class="card-img-top p-3">
                    </div>
                    <div class="card-body text-slide">
                        <div style="padding: 0px !important;">
                            <h6><?php echo $category->name; ?></h6>
                        </div>
                        <p><?php echo substr($category->description, 0, 50);
                            if (strlen($category->description) > 50) echo "..."; ?></p>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
</section>

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col col-md-6 col-sm-12 col-12 mb-3 order-2 order-md-1">
                <h1 class="display-4">New Course</h1>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            </div>
            <div class="col offset-md-2 col-md-4 col-sm-12 col-12 mb-3 order-1 order-md-2">
                <div class="input-group input-group-lg">
                    <input class="form-control border border-right-0" type="search" placeholder="Find Anything" id="search_course">
                    <span class="input-group-append">
                        <button class="btn btn-danger border border-left-0" type="button" id="btn_search_course">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <?php
        if(count($course_list) > 0) {?>
            <div class="row">
                <div class="col col-12 text-right mb-3">
                    <a href="<?php echo base_url('ListCourse'); ?>" class="btn btn-danger">View All</a>
                </div>
            </div>
            <?php
            $i = 1;
            foreach ($course_list as $course_item) {
                $i++;
                if ($i % 2 == 0) {
            ?>
                    <div class="row">
                        <div class="col-md-6 mb-4">

                            <div class="event row align-items-center align-items-stretch border mr-0 ml-0">
                                <div class="col-lg-12 p-0">
                                    <div class="image">
                                        <!-- <img src="<?php //echo base_url(); ?>assets/media/slider/1.jpg" alt=""> -->
                                        <img src="<?php echo base_url().$course_item->banner_image; ?>" alt="">
                                        <div class="overlay d-flex align-items-end">
                                            <div class="date">
                                                <?php $date_start = date_create($course_item->start_date);
                                                $date_end = date_create($course_item->end_date); ?>
                                                <strong><?php echo date_format($date_start, "d"); ?></strong>&nbsp;<span><?php echo date_format($date_start, "F yy"); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-0">
                                    <div class="text bg-white d-flex align-items-center">
                                        <div class="text-inner">
                                            <h4><?php echo $course_item->ocw_name; ?></h4>
                                            <p><?php echo substr(strip_tags($course_item->description), 0, 500);
                                                if (strlen(strip_tags($course_item->description)) > 500) echo "..."; ?></p>
                                            <!-- <a href="<?php //echo base_url("Course/view/$course_item->slug"); ?>" class="btn btn-outline-danger">Read more</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 mb-4">                        
                        
                            <div class="event row align-items-center align-items-stretch border mr-0 ml-0">
                                <div class="col-lg-6 p-0">
                                    <div class="image h-100">
                                        <!-- <img src="<?php //echo base_url(); ?>assets/media/slider/1.jpg" alt="">                                        -->
                                        <img src="<?php echo base_url().$course_item->banner_image; ?>" alt="">
                                        <div class="overlay d-flex align-items-end">
                                            <div class="date">
                                                <?php $date_start = date_create($course_item->start_date);
                                                $date_end = date_create($course_item->end_date); ?>
                                                <strong><?php echo date_format($date_start, "d"); ?></strong>&nbsp;<span><?php echo date_format($date_start, "F yy"); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-0">
                                    <div class="text bg-white d-flex align-items-center">
                                        <div class="text-inner">
                                            <h4><?php echo $course_item->ocw_name; ?></h4>
                                            <p><?php echo substr(strip_tags($course_item->description), 0, 100);
                                                if (strlen(strip_tags($course_item->description)) > 500) echo "..."; ?></p>
                                            <!-- <a href="<?php //echo base_url("Course/view/$course_item->slug"); ?>" class="btn btn-outline-danger">Read more</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                           
                            <div class="event row align-items-center align-items-stretch border mr-0 ml-0">
                                <div class="col-lg-6 p-0">
                                    <div class="image h-100">
                                        <!-- <img src="<?php echo base_url(); ?>assets/media/slider/2.jpg" alt=""> -->
                                        <img src="<?php echo base_url().$course_item->banner_image; ?>" alt="">
                                        <div class="overlay d-flex align-items-end">
                                            <div class="date">
                                                <?php $date_start = date_create($course_item->start_date);
                                                $date_end = date_create($course_item->end_date); ?>
                                                <strong><?php echo date_format($date_start, "d"); ?></strong>&nbsp;<span><?php echo date_format($date_start, "F yy"); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-0">
                                    <div class="text bg-white d-flex align-items-center">
                                        <div class="text-inner">
                                            <h4><?php echo $course_item->ocw_name; ?></h4>
                                            <p><?php echo substr(strip_tags($course_item->description), 0, 100);
                                                if (strlen(strip_tags($course_item->description)) > 500) echo "..."; ?></p>
                                            <!-- <a href="<?php //echo base_url("Course/view/$course_item->slug"); ?>" class="btn btn-outline-danger">Read more</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                          

                            <div class="event row align-items-center align-items-stretch border mr-0 ml-0">
                                <div class="col-lg-6 p-0">
                                    <div class="image h-100">
                                        <!-- <img src="<?php echo base_url(); ?>assets/media/slider/1.jpg" alt=""> -->
                                        <img src="<?php echo base_url().$course_item->banner_image; ?>" alt="">
                                        <div class="overlay d-flex align-items-end">
                                            <div class="date">
                                                <?php $date_start = date_create($course_item->start_date);
                                                $date_end = date_create($course_item->end_date); ?>
                                                <strong><?php echo date_format($date_start, "d"); ?></strong>&nbsp;<span><?php echo date_format($date_start, "F yy"); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-0">
                                    <div class="text bg-white d-flex align-items-center">
                                        <div class="text-inner">
                                            <h4><?php echo $course_item->ocw_name; ?></h4>
                                            <p><?php echo substr(strip_tags($course_item->description), 0, 100);
                                                if (strlen(strip_tags($course_item->description)) > 500) echo "..."; ?></p>
                                            <!-- <a href="<?php //echo base_url("Course/view/$course_item->slug"); ?>" class="btn btn-outline-danger">Read more</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php } else {
                ?>
                    <div class="row">
                        <div class="col col-md-12 mb-4">

                            <div class="event row align-items-center align-items-stretch border mr-0 ml-0">
                                <div class="col-lg-6 p-0 order-2 order-lg-1">
                                    <div class="text bg-white d-flex align-items-center">
                                        <div class="text-inner">
                                            <h4><?php echo $course_item->ocw_name; ?></h4>
                                            <p><?php echo substr(strip_tags($course_item->description), 0, 500);
                                                if (strlen(strip_tags($course_item->description)) > 500) echo "..."; ?></p>
                                            <a href="<?php echo base_url() . 'CourseDetail/index'; ?>" class="btn btn-outline-danger">Read more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-0 order-1 order-lg-2">
                                    <div class="image">
                                        <img src="<?php echo base_url().$course_item->banner_image; ?>" alt="">
                                        <div class="overlay d-flex align-items-end">
                                            <div class="date">
                                                <?php $date_start = date_create($course_item->start_date);
                                                $date_end = date_create($course_item->end_date); ?>
                                                <strong><?php echo date_format($date_start, "d"); ?></strong>&nbsp;<span><?php echo date_format($date_start, "F yy"); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php };
            }
        } else {; ?>
            <div class="col col-12">
                Course Empty
            </div>
        <?php }
        ?>
    </div>
</section>

<section class="pb-5">
    <div class="container mb-3">
        <h1 class="display-4">Recent Event</h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <div class="col col-12 text-right">
            <a href="<?php //echo base_url('Category'); ?>" class="btn btn-danger">View All</a>
        </div>
    </div>

    <div class="regular slider" style="object-fit: none;">
        <?php foreach ($category_list as $category) { ?>
            <a href="<?php echo base_url('Course/list') . "?category_id=" . $category->ocw_category_id; ?>">
                <div class="content-list border" style="transform: none;" data-id="<?php echo "$category->img_path"; ?>">
                    <div style="width:100%; height: 100px;">
                        <img src="<?php echo base_url() . $category->img_path; ?>" class="card-img-top p-3">
                    </div>
                    <div class="card-body text-slide">
                        <div style="padding: 0px !important;">
                            <h6><?php echo $category->name; ?></h6>
                        </div>
                        <p><?php echo substr($category->description, 0, 50);
                            if (strlen($category->description) > 50) echo "..."; ?></p>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>

</section>
<?php
 if (!$this->ion_auth->logged_in()){
?>
<section class="py-5">
    <div class="jumbotron bg-red">
        <div class="container">
            <div class="row align-items-center">
                <div class="col col-md-10">
                    <h5 class="text-light">Join Our Programme</h5>
                    <h3 class="text-light">Telkom University Open Courseware Registration. </h3>
                </div>
                <div class="col col-md-2 text-right">
                    <a href="<?php echo base_url('register')?>" class="btn btn-light">Join Now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php }?>
<?php
$this->load->view('fe/templates/footer');
require "_script.php";
?>
</body>

</html>