<?php $this->load->view('fe/templates/header'); ?>
<style>
    @media (max-width: 1023px) {
        header{
            position:relative!important;
            width:100%!important;
        }

        .content-header{
            position:relative!important;
            background: #df2b2b;
        }

        .box{
            display:none;
        }

        .text-header{
            font-size:2rem!important;
            text-align:left!important;
        }

        .btn-view-all{
            display:none;
        }

        .btn-load-more{
            display:block;
        }
        .banner-image {
            height :400px !important;
        }
    }
    
    @media (min-width: 1024px) {
        header{
            position:relative!important;
            width:100%!important;
            height:86vh!important;
        }
        .btn-view-all{
            display:block;
        }
        .btn-load-more{
            display:none;
        }

         .course-image-list-container{
            height:140px;
            overflow:hidden;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .course-image-list-container img{
            width: 100% !important;
        }
        .banner-image {
            height :400px !important;
        }
    }

    @media (min-width: 1200px) {
        header{
            position:relative!important;
            width:100%!important;
            height:76vh!important;
        }
        .btn-view-all{
            display:block;
        }
        .btn-load-more{
            display:none;
        }
        .banner-image {
            height :400px !important;
        }
    }

    @media (min-width: 1366px) {
        header{
            position:relative!important;
            width:100%!important;
            height:90vh!important;
        }
        .content-header{
            margin-top: 3rem!important;
        }
        .btn-view-all{
            display:block;
        }
        .btn-load-more{
            display:none;
        }
        .banner-image {
            height :400px !important;
        }
    }

    @media (min-width: 1440px) {
        header{
            position:relative!important;
            width:100%!important;
            height:86vh!important;
        }
        .btn-view-all{
            display:block;
        }
        .btn-load-more{
            display:none;
        }
        .banner-image {
            height :400px !important;
        }
    }

    @media (min-width: 1920px) {
        header{
            position:relative!important;
            width:100%!important;
            height:76vh!important;
        }
        .content-header{
            margin-top: 3rem!important;
        }
        .btn-view-all{
            display:block;
        }
        .btn-load-more{
            display:none;
        }
        .banner-image {
            height :400px !important;
        }
    }

    

    .box{
        width: 100%;
        height: 100%;
        background: #df2b2b;
        -webkit-clip-path: polygon(0 0, 75% 0, 20% 100%, 0% 100%);
        clip-path: polygon(0 0, 75% 0, 20% 100%, 0% 100%);
        position:relative;
    }

    .content-slider{
        z-index: 1000;
    }

    .content-header{
        position: absolute;
        width: 100%;
        z-index: 999;
    }

    .text-header{
        color:#fff;
        text-align: left;
    }

    .sld-category{
        position: relative;
        color: #fff;
        height:25vh;
    }
    .sld-category img{
        object-fit: cover;
        object-position: center;
        width:100%;
        height:100%;
    }
    .sld-txt-img-category{
        position: absolute;
        bottom: 0;
        padding: 10px;
        width: 100%;
        background: black;
        opacity: 0.7;
    }

    .content-box-white{
        color:#fff!important;
    }

    .content-box a{
        color:#fff!important;
    }

    .content-box a:hover{
        color:#fff!important;
        text-decoration: none!important;
    }

    .content-box-default{
        color:#000!important;
    }

    .content-box-default a{
        color:#000!important;
    }

    .content-box-default a:hover{
        color:#000!important;
        text-decoration: none!important;
    }

    .hr-title{
        border-top: 6px solid darkred;
        border-radius: 10px;
        left: 0;
        width: 25%;
        margin-left: 0;
        margin-top: 0;
    }

    .bg-news{
        background-color:#e9e9ea;
    }

    h5,h6{
        font-weight: bold;
    }

    .course_content{
        padding-bottom: 15px;
        margin-bottom: 15px;
        border-bottom: 1px solid #c3c3c3;
    }

    .crop-1{
        overflow: hidden;
        display: -webkit-box;
        line-clamp:1;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
    }

    .crop-4{
        overflow: hidden;
        display: -webkit-box;
        line-clamp:4;
        -webkit-line-clamp: 4;
        -webkit-box-orient: vertical;
    }

    .slider-nav .slick-next:before,.slider-nav .slick-prev:before {
        border-radius:0px;
        border:1px solid black;
    }

    .slider-nav .item {
        height:70px;
        overflow:hidden;
    }

    .home-link{
        border:1px solid darkred;
        position:relative;
        margin-bottom:15px;
    }
    
    .home-link a{
        position: absolute;
        bottom:0px;
        left:0px;
        background-color:rgba(255,255,255,0.5);
        width:100%;
        padding:10px;
        color:darkred;
        font-weight: bold;
        text-decoration:none;
    }

    .home-link h5{
        font-size:20px;
        line-height: 20px;
    }

    .home-link p{
        color:black;
        font-size: 10px;
        margin:0px;
    }

</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63888721-61"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-63888721-61');
</script>
<?php foreach($landing_content as $single_content){?>

    <?php if($single_content->content != null){?>
        <section class="py-5 mt-5">
            <div class="container">
                <div class="row">
                    <div class="col col-9">
                        <h1 class="h1"><?php echo $single_content->title;?></h1>
                        <hr class="hr-title">
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12">
                        <?php echo $single_content->content;?>
                    </div>
                </div>
            </div>
        </section>
    <?php 
        }else{
            if($single_content->id == 1){//banner
    ?>
                <!--BANNER SECTION-->
                <header>
                    <div class="box" style="position: absolute;top: 0;"></div>
                    <div class="content-header">
                        <div class="container py-5">
                                <div class="row">
                                    <div class="col col-12 col-md-4 align-self-center">
                                        <h1 class="text-header" id="banner_desc">
                                            <?php
                                                if(!empty($banner))
                                                    echo $banner[0]->description;
                                            ?>
                                        </h1>
                                    </div>
                                    <div class="col col-12 col-md-8">
                                        <div class="content-slider">
                                            <div class="slider-for">
                                                <?php foreach($banner as $single_banner){?>
                                                    <?php if($single_banner->type == "image"){?>
                                                    <div class="item slick-content-header" data-caption="<?php echo htmlspecialchars($single_banner->description)?>">
                                                        <a target="_blank" href="<?php echo $single_banner->url;?>">
                                                            <!--<img src="<?php echo base_url($single_banner->img_path)?>" alt="<?php echo base_url($single_banner->description)?>"  draggable="false"/>-->
                                                            <div class="banner-image" style="background:url(<?php echo base_url($single_banner->img_path)?>);background-size:contain;background-repeat:no-repeat;background-position:center;">
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if($single_banner->type == "video"){?>
                                                    <div class="item slick-content-header banner-image" data-caption="<?php echo htmlspecialchars($single_banner->description)?>">
                                                        <div class="embed-responsive embed-responsive-16by9 banner-image" draggable="false">
                                                            <iframe class="embed-responsive-item banner-image" src="<?php echo $single_banner->url;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <div class="slider-nav">
                                                <?php foreach($banner as $single_banner){?>
                                                    <div class="item slick-thumbnail-header">
                                                        <img src="<?php echo base_url($single_banner->img_path)?>" alt="<?php echo base_url($single_banner->description)?>"  draggable="false"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </header>

                <section class="mt-5">
                    <div class="container">
                        <div class="row">
                            <div class="col col-12 col-md-6 col-lg-3 p-1">
                                <div class="home-link">
                                    <img src="<?php echo $this->config->item("ASSET_PATH");?>media/img/dashboard.jpeg" width="100">
                                 
                                     <a href="<?php echo base_url("auth")?>">
                                        <h5>CeLOE<br>Dashboard </h5>
                                        <p>Aplikasi Pengelolaan Course dan Program Daring</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col col-12 col-md-6 col-lg-3 p-1">
                                <div class="home-link">
                                    <img src="<?php echo $this->config->item("ASSET_PATH");?>media/img/LMS.JPG" width="100">
                                 
                                     <a href="https://lms.telkomuniversity.ac.id/login/index.php">
                                        <h5>Learning Management System (LMS)</h5>
                                        <p>Aplikasi Belajar untuk Interaksi Dosen dan Mahasiswa Telkom University</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col col-12 col-md-6 col-lg-3 p-1">
                                
                                <div class="home-link">
                                    <img src="<?php echo $this->config->item("ASSET_PATH");?>media/img/CDS.JPG" width="100">
                                    <a href="https://cds.telkomuniversity.ac.id/login/index.php">
                                        <h5>Course Development System (CDS)</h5>
                                        <p>Aplikasi Pengembangan Course untuk Pengajar Telkom University</p>
                                    </a>

                                </div>
                            </div>
                            <div class="col col-12 col-md-6 col-lg-3 p-1">
                                 <div class="home-link">
                                     <img src="<?php echo $this->config->item("ASSET_PATH");?>media/img/OOC.jpg" width="100">
                                    
                                    <a href="https://onlinelearning.telkomuniversity.ac.id/login/index.php">
                                        <h5>Open Online Course<br>(MOOC)</h5>
                                        <p>Aplikasi Belajar dan Sertifikasi Daring untuk Umum</p>
                                    </a>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </section>

    <?php
            }else if($single_content->id == 2){//category
    ?>
                <!--CATEGORY SECTION-->
                <section class="py-5 mt-5">
                    <div class="container">
                        <div class="row">
                            <div class="col col-9 col-md-9">
                                <h1 class="h1"><?php echo $single_content->title;?></h1>
                                <hr class="hr-title">
                            </div>
                            <div class="col col-3 col-md-3 text-right">
                                <a href="<?php echo base_url('Course/catalog'); ?>" class="btn btn-danger rounded-0 shadow-sm btn-container">View All</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-12">
                                <div class="regular slider">
                                    <div class="slider-category">
                                        <?php foreach($category_list as $key=>$category){?>
                                            <div class="item shadow-sm">
                                               <a href="<?php echo base_url('Course/catalog?c='.$category->ocw_category_id);?>">
                                                    <div class="sld-category">
                                                        <img src="<?php echo base_url($category->img_path)?>" alt="image"  draggable="false"/>
                                                        <div class="sld-txt-img-category">
                                                            <h5 class="crop-1" id="clamp-title-cat-<?php echo $key;?>" style="margin:0;font-size:1rem;"><?php echo strip_tags($category->name);?></h5>
                                                            <span class="crop-1" id="clamp-desc-cat-<?php echo $key;?>" style="font-size:0.7rem;"><?php echo strip_tags($category->description);?></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
    <?php          
            }else if($single_content->id == 3){//course
    ?>
                <!--NEW COURSE SECTION-->
                <section class="py-5">
                    <div class="container bg-news">
                        <div class="row">
                            <div class="col col-12 col-md-9">
                                <h1 class="h1 mt-3"><?php echo $single_content->title;?></h1>
                                <hr class="hr-title">
                            </div>
                            <div class="col col-md-3 text-right mt-3 btn-view-all">
                                <a href="<?php echo base_url('Course/catalog'); ?>" class="btn btn-danger rounded-0 shadow-sm">View All</a>
                            </div>
                        </div>
                        <div class="row">
                            <?php if(!empty($course_list)){?>
                                    <div class="col col-12 col-sm-12 col-md-12 col-lg-6">
                                        <div class="row">
                                            <div class="col col-12 pb-2">
                                                <a href="<?php echo $course_list[0]->is_multiple == '0' ? base_url('Course/view/'.$course_list[0]->slug) : base_url('Course/package/'.$course_list[0]->slug);?>">
                                                    <img src="<?php echo base_url( $course_list[0]->banner_image)?>" alt="" class="img-fluid">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row content-box-default">
                                            <div class="col col-12">
                                                <h6 class="crop-1 mt-3" id="clamp-title-course-main"><a href="<?php echo $course_list[0]->is_multiple == '0' ? base_url('Course/view/'.$course_list[0]->slug) : base_url('Course/package/'.$course_list[0]->slug);?>"><?php echo strip_tags($course_list[0]->ocw_name);?></a></h6>
                                                <p id="clamp-desc-course-main" class="my-3 crop-4"><a href="<?php echo $course_list[0]->is_multiple == '0' ? base_url('Course/view/'.$course_list[0]->slug) : base_url('Course/package/'.$course_list[0]->slug);?>"><?php echo strip_tags($course_list[0]->description);?></a></p>
                                            </div>
                                        </div>
                                    </div>
                            <?php }?>
                            <div class="col col-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="col col-12">
                                <?php for($i=1; $i<count($course_list); $i++){?>
                                    <div class="row content-box-default course_content">
                                        <div class="col col-12 col-sm-12 col-md-12 col-lg-6 pl-0 course-image-list-container">
                                            <a href="<?php echo $course_list[$i]->is_multiple == '0' ? base_url('Course/view/'.$course_list[$i]->slug) : base_url('Course/package/'.$course_list[$i]->slug);?>">
                                                <img src="<?php echo base_url( $course_list[$i]->banner_image)?>" alt="" class="img-fluid">
                                            </a>
                                        </div>
                                        <div class="col col-12 col-sm-12 col-md-12 col-lg-6 p-1">
                                            <h6 class="crop-1" id="clamp-title-course-<?php echo $i;?>"><a href="<?php echo $course_list[$i]->is_multiple == '0' ? base_url('Course/view/'.$course_list[$i]->slug) : base_url('Course/package/'.$course_list[$i]->slug);?>"><?php echo strip_tags($course_list[$i]->ocw_name);?></a></h6>
                                            <p id="clamp-desc-course-<?php echo $i;?>" class="my-3 crop-4"><a href="<?php echo $course_list[$i]->is_multiple == '0' ? base_url('Course/view/'.$course_list[$i]->slug) : base_url('Course/package/'.$course_list[$i]->slug);?>"><?php echo strip_tags($course_list[$i]->description);?></a></p>
                                        </div>
                                    </div>
                                <?php }?>
                                </div>
                            </div>
                        </div>
                        <div class="row btn-load-more py-3">
                            <div class="col col-12">
                                <button class="btn btn-danger btn-block rounded-0 shadow-sm">View All</button>
                            </div>
                        </div>
                    </div>
                </section>
    <?php
            }else if($single_content->id == 4){//event
    ?>
                <!--NEWS SECTION-->
                <section class="py-5">
                    <div class="container">
                        <div class="row">
                            <div class="col col-md-9 col-12">
                                <h1 class="h1"><?php echo $single_content->title;?></h1>
                                <hr class="hr-title">
                            </div>
                            <div class="col col-md-3 text-right btn-view-all">
                                <a href="<?php echo base_url('news')?>" class="btn btn-danger rounded-0 shadow-sm rounded-0 shadow-sm">View All</a>
                            </div>
                        </div>
                        <div class="row">
                            <?php foreach($news as $key=>$single_news){?>
                                    <div class="col col-xs-12 col-sm-12 col-md-4 col-lg-4 mb-4 col-12 content-box-default news_content">
                                        <a href="<?php echo base_url('News/view/'.$single_news->slug);?>">
                                            <div class="card border-1 border rounded-0">
                                                <img src="<?php echo base_url($single_news->thumbnail)?>" class="img-thumbnail-custom">
                                                <div class="card-body text-slide">
                                                    <div class="row">
                                                        <div class="col col-12 mt-2">
                                                            <i class="fa fa-calendar"></i> <?php echo date("d M Y", strtotime($single_news->updated_at))?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col col-12">
                                                            <h5 class="crop-1 my-3" id="clamp-title-<?php echo $key;?>"><?php echo strip_tags($single_news->title);?></h5>
                                                            <p class="crop-4 desc_news" id="clamp-desc-<?php echo $key;?>"><?php echo strip_tags($single_news->description);?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                            <?php } ?>
                        </div>
                        <div class="row btn-load-more">
                            <div class="col col-12">
                                <button class="btn btn-danger btn-block rounded-0 shadow-sm">View All</button>
                            </div>
                        </div>
                    </div>
                </section>
    <?php      
            }
        }

    ?>
<?php }?>



<?php
$this->load->view('fe/home/_survey');
$this->load->view('fe/templates/footer');
require "_script.php";
?>
</body>

</html>