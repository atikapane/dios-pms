<script>
    function initSurvey(){
        var participate = false
        var survey_id

        var user = '<?= json_encode($user) ?>';
        user = JSON.parse(user)
        if(user){
            $("#loading_overlay").show()
            $.ajax({
                type: 'GET',
                url: '<?= base_url('home/today_survey') ?>',
                data: {
                    user_id: user.mooc_id,
                },
                dataType: 'json',
                success: function (response, data){
                    if(response.status){
                        survey_id = response.survey.id
                        $('#survey_name').text(response.survey.name)
                        $('#modal_survey').modal('show')
                    }
                    $("#loading_overlay").hide()
                },
                error: function (jqXHR, textStatus, errorThrown){
                    $("#loading_overlay").hide()
                    swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                }
            })
        }

        $('#btn_survey_participate').on('click', function(){
            respondent = {
                'id': user.mooc_id,
                'username': user.username,
                'name': user.full_name,
                'email': user.email,
                'origin': '<?= base_url() ?>',
                'survey_id': survey_id
            }
            let url = "<?= base_url('survey') ?>" + "?" + $.param(respondent)

            const win = window.open(url, '_blank')
            win.focus()
            participate = true
        })

        $('#modal_survey').on('hidden.bs.modal', function(){
            if(user && !participate){
                $("#loading_overlay").show()
                $.ajax({
                    type:"POST",
                    url : "<?= base_url().'home/survey_reject'; ?>",
                    data: {
                        user_id: user.mooc_id,
                        survey_id: survey_id,
                        not_show: $('#not_show:checkbox:checked').length > 0
                    },
                    dataType:"json",
                    success:function (data, status) {
                        $("#loading_overlay").hide()
                    },error:function () {
                        $("#loading_overlay").hide()
                        swal.fire({text:"Cannot communicate with server please check your internet connection",confirmButtonColor:"darkred"})
                    }
                });
            }
        })
    }

    $(document).ready(function(){
        initSurvey()
    })

    $(".news_content").each(function (index, item) {
        var clmp_title_index = "clamp-title-"+index.toString();
        var clmp_desc_index = "clamp-desc-"+index.toString();

        $clamp($("#"+clmp_title_index).get(0), {clamp: 2});
        $clamp($("#"+clmp_desc_index).get(0), {clamp: 4});
    });

    $clamp($("#clamp-title-course-main").get(0), {clamp: 1});
    $clamp($("#clamp-desc-course-main").get(0), {clamp: 4});

    $(".course_content").each(function (index, item) {
        index=index+1;
        var clmp_title_course = "clamp-title-course-"+index.toString();
        var clmp_desc_course = "clamp-desc-course-"+index.toString();

        $clamp($("#"+clmp_title_course).get(0), {clamp: 1});
        $clamp($("#"+clmp_desc_course).get(0), {clamp: 4});
    });

    $(".sld-category").each(function (index, item) {
        var clmp_title_category = "clamp-title-cat-"+index.toString();
        var clmp_desc_category = "clamp-desc-cat-"+index.toString();

        $clamp($("#"+clmp_title_category).get(0), {clamp: 1});
        $clamp($("#"+clmp_desc_category).get(0), {clamp: 1});
    });

    // instalasi adc

    $(".image-link").click(function() {
        var url =  $(this).find("a").attr("href");
        if(url != "" && url != undefined){
            window.location = url
        }
        return false;
    });

    $("#btn_search_course").on("click",function () {
        if($.trim($("#search_course").val()).length>0){
            getDataCourse($.trim($("#search_course").val()));
        }
    });

    $("#search_course").on("keypress", function (e) {
        var code = e.keyCode || e.which;
        if(code===13){
            // Enter pressed... do anything here...
            if($.trim($(this).val()).length>0){
                getDataCourse($.trim($("#search_course").val()));
            }
        }
    });

    function getDataCourse(value){
        console.log(value);
        $.ajax({
            type:"POST",
            url : "<?php echo base_url().'Home/search_course'; ?>",
            data: {
                search_content: value
            },
            dataType:"json",
            success:function (res) {
                if(res.status){
                    window.location.href = "<?php echo base_url()."ListCourse";?>";
                }
            },error:function (e) {
                console.log(e);
            }
        });
    }

</script>