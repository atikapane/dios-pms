<?php $this->load->view('fe/templates/header'); ?>
    <section class="my-5">
        <div class="container">
            <div class="col col-sm-12 col-md-8 col-lg-7 mx-auto">
                <div class="card rounded-1">
                    <div class="card-body">
                        <div class="my-3 col-md-12">
                            <div class="row">
                                <div class="col col-md-12 text-center">
                                    <h4>Forgot Password</h4>
                                </div>
                            </div>
                            <form action="#" id="form_send_email" class="py-3">
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input type="email" class="form-control" id="inputEmail" name="email" placeholder="username@domain.com">
                                </div>
                                <div class="alert alert-danger fade show" role="alert" id="success_alert" style="display: none;">
                                    <div class="alert-text" id="success_message"></div>
                                </div>
                                <div class="alert alert-danger fade show" role="alert" id="failed_alert" style="display: none;">
                                    <div class="alert-text" id="failed_message"></div>
                                </div>
                                <button type="button" class="btn btn-block btn-primary py-2" id="btn_send_email">Send Email</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('fe/templates/footer'); ?>
<?php require '_script.php'; ?>