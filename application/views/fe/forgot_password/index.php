<?php $this->load->view('fe/templates/header'); ?>
<div class="bg-header-v2"></div>
<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->
<div class="login-wrapper mb-5 p-3">
    <form action="#" id="form_send_email">
        <div class="form-group">
            <label for="inputEmail">Email*</label>
            <input type="email" placeholder="Email" class="form-control" id="inputEmail" name="email">
        </div>
        <div class="alert alert-success fade show" role="alert" id="success_alert" style="display: none;">
            <div class="alert-text" id="success_message"></div>
        </div>
        <div class="alert alert-danger fade show" role="alert" id="failed_alert" style="display: none;">
            <div class="alert-text" id="failed_message"></div>
        </div>
        <button type="button" id="btn_send_email" class="btn btn-primary-red w-100">Send Email </button>
    </form>
</div>
<?php $this->load->view('fe/templates/footer'); ?>
<?php $this->load->view('fe/forgot_password/_script'); ?>