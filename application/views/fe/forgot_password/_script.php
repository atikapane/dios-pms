<script>
    var AlertUtil;
    var loginForm;

    function initAlert(){
        AlertUtil = {
            showFailedDialog : function(message,timeout){
                $("#failed_message").text(message);
                $("#failed_alert").show();
            },
            showSuccessDialog : function(message,timeout){
                $("#success_message").text(message);
                $("#success_alert").show();
            }
        };
    }


    function initLoginForm(){
        // validator.resetForm();
        var validator = $('#form_send_email').validate({
            errorClass:'error-field',
            rules: {
                email: {
                    required: true
                }
            }
        });

        $("#btn_send_email").on("click", function () {
            $("#failed_alert").hide();
            $("#success_alert").hide();
            var isValid = $( "#form_send_email" ).valid();
            if(isValid){
                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url().'ForgotPassword/send_email'; ?>",
                    data: $("#form_send_email").serialize(),
                    dataType:"json",
                    success:function (res) {
                        if(res.status){
                            AlertUtil.showSuccessDialog(res.message);
                            //setTimeout(function () {
                            //    window.location.href = "<?php //echo site_url('UsersAuth'); ?>//";
                            //},3000)
                        }else{
                            AlertUtil.showFailedDialog(res.message);
                        }
                    },error:function () {
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        return {
            validator:validator
        }
    }

    $(document).ready(function (){
        loginForm = initLoginForm();
        initAlert();
    });

</script>