<?php $this->load->view('fe/templates/header'); ?>
<div class="bg-header-v2"></div>
<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->
<div class="login-wrapper mb-5 p-3">
    <form action="#" id="form_reset_password">
        <div class="form-group">
            <label for="inputNewPassword">New Password</label>
            <input type="password" placeholder="••••••••" class="form-control" id="inputNewPassword" name="new_password">
        </div>
        <div class="form-group">
            <label for="inputConfirmNewPassword">Confirm New Password</label>
            <input type="password" placeholder="••••••••" class="form-control" id="inputConfirmNewPassword" name="confirm_new_password">
        </div>
        <div class="alert alert-success fade show" role="alert" id="success_alert" style="display: none;">
            <div class="alert-text" id="success_message"></div>
        </div>
        <?php $message = $this->session->flashdata('message');?>
        <div class="alert alert-danger fade show" role="alert" id="failed_alert_server" style="<?php if($message){echo '';}else{echo 'display: none';}?>">
            <div class="alert-text" id="failed_message_server"><?php echo $message;?></div>
        </div>
        <div class="alert alert-success fade show" role="alert" id="success_alert_server" style="<?php if($message){echo '';}else{echo 'display: none';}?>">
            <div class="alert-text" id="success_message_server"><?php echo $message;?></div>
        </div>
        <button type="button" id="btn_reset_password" class="btn btn-primary-red w-100">Reset Password </button>
    </form>
</div>
<?php $this->load->view('fe/templates/footer'); ?>
<?php $this->load->view('fe/forgot_password_reset/_script'); ?>