<?php $this->load->view('fe/templates/header'); ?>
    <section class="my-5">
        <div class="container">
            <div class="col col-sm-12 col-md-8 col-lg-7 mx-auto">
                <div class="card rounded-1">
                    <div class="card-body">
                        <div class="my-3 col-md-12">
                            <div class="row">
                                <div class="col col-md-12 text-center">
                                    <h4>Reset Password</h4>
                                </div>
                            </div>
                            <form action="#" id="form_reset_password" class="py-3">
                                <div class="form-group">
                                    <label for="inputNewPassword">New Password</label>
                                    <input type="password" class="form-control" id="inputNewPassword" name="new_password" placeholder="••••••••">
                                </div>
                                <div class="form-group">
                                    <label for="inputConfirmNewPassword">Confirm New Password</label>
                                    <input type="password" class="form-control" id="inputConfirmNewPassword" name="confirm_new_password" placeholder="••••••••">
                                </div>
                                <div class="alert alert-danger fade show" role="alert" id="success_alert" style="display: none;">
                                    <div class="alert-text" id="success_message"></div>
                                </div>
                                <?php $message = $this->session->flashdata('message');?>
                                <div class="alert alert-danger fade show" role="alert" id="failed_alert_server" style="<?php if($message){echo '';}else{echo 'display: none';}?>">
                                    <div class="alert-text" id="failed_message_server"><?php echo $message;?></div>
                                </div>
                                <div class="alert alert-success fade show" role="alert" id="success_alert_server" style="<?php if($message){echo '';}else{echo 'display: none';}?>">
                                    <div class="alert-text" id="success_message_server"><?php echo $message;?></div>
                                </div>
                                <button type="button" class="btn btn-block btn-primary py-2" id="btn_reset_password">Reset Password</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('fe/templates/footer'); ?>
<?php require '_script.php'; ?>