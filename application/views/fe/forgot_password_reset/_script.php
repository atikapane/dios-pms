<script>
    var AlertUtil;
    var resetPasswordForm;

    function initAlert(){
        AlertUtil = {
            showFailedDialog : function(message,timeout){
                $("#failed_message").text(message);
                $("#failed_alert").show();
            },
            showSuccessDialog : function(message,timeout){
                $("#success_message").text(message);
                $("#success_alert").show();
            }
        };
    }


    function initResetPasswordForm(){
        // validator.resetForm();
        var validator = $('#form_reset_password').validate({
            errorClass:'error-field',
            rules: {
                new_password: {
                    required: true,
                    minlength:8
                },
                confirm_new_password:{
                    required:true,
                    equalTo: "#inputNewPassword",
                    minlength:8
                }
            }
        });

        $("#btn_reset_password").on("click", function () {
            $("#failed_alert").hide();
            $("#failed_alert_server").hide();
            var isValid = $( "#form_reset_password" ).valid();
            if(isValid){
                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url().'ForgotPassword/process_reset_password'; ?>",
                    data: $("#form_reset_password").serialize(),
                    dataType:"json",
                    success:function (res) {
                        if(res.status == true){
                            AlertUtil.showSuccessDialog(res.message);
                            setTimeout(function () {
                                window.location.href = "<?php echo site_url(); ?>";
                            },3000);
                        }else{
                            AlertUtil.showFailedDialog(data.message);
                        }
                    },error:function () {
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        return {
            validator:validator
        }
    }

    $(document).ready(function (){
        resetPasswordForm = initResetPasswordForm();
        initAlert();
    });

</script>