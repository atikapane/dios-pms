<?php $this->load->view('fe/templates/header'); ?>
<style>
    .card{
        cursor:pointer;
    }

    .card-body {
        min-height: 100px;
    }
</style>


<header class="bg-image-full py-5 mb-5" style="background-image: url('http://placehold.jp/1024x300.png');">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <h1 class="display-4 text-white mt-5 mb-2">All Category</h1>
                <p class="lead mb-5 text-white-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non
                    possimus ab labore provident mollitia.</p>
            </div>
        </div>
    </div>
</header>

<section class="py-3">
    <div class="container">
        <div class="col-md-10 mx-auto">
            <div class="row">
                <div class="col col-md-4 col-sm-12 col-12 mb-3 order-1 order-md-2">
                    <div class="input-group">
                        <input class="form-control border border-right-0" type="search" placeholder="Search Category" id="txt_search">
                        <span class="input-group-append">
                            <button class="btn btn-danger border border-left-0" type="button" id="btn_search">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10 mx-auto py-5">
            <div class="row" id="categories">
            </div>
            <div id="loader" class="text-center">
                <div class="loadingio-spinner-pulse-ea1zitzrksm">
                    <div class="ldio-nr4fjypgnsg">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-5 mx-auto py-5">
            <button class="btn btn-danger btn-block" id="btn-load-more" style="display: none">LOAD MORE</button>
        </div>
    </div>
</section>
<?php $this->load->view('fe/templates/footer'); ?>
<?php require '_script.php'; ?>

</body>
</html>
