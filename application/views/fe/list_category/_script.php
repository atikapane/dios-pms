<script>

    var data = {
        "search":"",
        "limit":<?php echo $this->config->item('limit_category_page');?>,
        "start":0
    };

    var search = false;
    var all_record = 0;

    function loadDataCategory(){
        var ajax = $.ajax({
            type:"POST",
            url:"<?php echo base_url().'Category/load_category';?>",
            data:{
                search:data.search,
                limit:data.limit,
                start:data.start
            },
            cache:false
        });

        ajax.done(function (res) {

            all_record = res.num_of_rows;

            $("#loader").hide();

            if(res.data.length>0){
                append_element(res);

                if(!search){
                    if(data.limit<res.num_of_rows){
                        $("#btn-load-more").show();
                    }else {
                        $("#btn-load-more").hide();
                    }
                }else {
                    data.limit = res.num_of_rows;
                    $("#btn-load-more").hide();
                }
            }else {
                $("#categories").append('<div class="col col-12"><p>No Category Found</p></div>');
            }

        });
    }

    function append_element(res){
        $.each(res.data, function (i, val) {
            console.log(val);
            var path = "<?php echo base_url();?>";
            var thumbnail = val.img_path == null ? "http://placehold.it/300x200":path+val.img_path;
            var ocw_category_id = val.ocw_category_id;
            var category_name = val.name;
            var description = val.description;
            var faculty_id = val.faculty_id;

            $("#categories").append('<div class="col col-md-4 col-sm-6 col-12 mb-5"><div class="content-list border list-faculty" data-id="'+faculty_id+'"><img src="'+ thumbnail +'" class="card-img-top p-3 rounded-0"><div class="card-body text-slide"><h6>'+ category_name +'</h6><p>'+ description +'</p></div></div></div>');
        });

        $(".list-faculty").each(function (a) {
            var id = $(this).data('id');
            $(this).on('click', function () {
                var base_url = "<?php echo base_url('Course/list');?>";
                window.location.href =  base_url+"?faculty_id="+id;
            });
        });
    }

    $(document).ready(function () {

        setTimeout(function () {
            loadDataCategory();
        },3000);

        $("#btn_search").on("click", function () {
            var text = $.trim($("#txt_search").val());

            $("#categories").empty();
            $('#loader').show();

            // if(text.length>0){
                search = true;

                data.limit = all_record;
                data.search = text;

                setTimeout(function () {
                    loadDataCategory();
                },3000);

                $("#btn-load-more").hide();
            // }
        });

        $("#txt_search").on('keyup', function () {
            if (event.keyCode === 13) {
                event.preventDefault();
                $("#btn_search").click();
            }
        });

        $("#btn-load-more").on("click",function () {
            search = false;

            data.limit = data.limit + data.limit;

            $('#loader').show();
            setTimeout(function () {
                loadDataCategory();
            }, 3000);
        });

    });
</script>