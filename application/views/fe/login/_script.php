<script>
    var AlertUtil;
    var loginForm;

    let url_google = "<?php echo base_url("UsersAuth/login_google");?>";
    let url_facebook = "<?php echo base_url("UsersAuth/login_facebook");?>";

    $("#btn_login_google").on('click',function () {
        window.location.href = url_google;
    });

    $("#btn_login_facebook").on('click',function () {
        window.location.href = url_facebook;
    });

    function initAlert(){
        AlertUtil = {
            showFailedDialog : function(message,timeout){
                $("#failed_message").text(message);
                $("#failed_alert").show();
            }
        };
    }


    function initLoginForm(){
        // validator.resetForm();
        var validator = $('#form_ocw_user_login').validate({
            errorClass:'error-field',
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true
                }
            }
        });

        $("#btn_login_non_modal").on("click", function () {
            $("#failed_alert").hide();
            $("#failed_alert_server").hide();
            var isValid = $( "#form_ocw_user_login_no_modal" ).valid();
            if(isValid){
                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url().'UsersAuth/processLogin'; ?>",
                    data: $("#form_ocw_user_login_no_modal").serialize(),
                    dataType:"json",
                    success:function (data) {
                        if(data.status == true){
                            if(redir != ""){
                                window.location.href = redir;
                            }else{
                                window.location.href = "<?php echo site_url(); ?>";
                            }
                        }else{
                            AlertUtil.showFailedDialog(data.message);
                        }
                    },error:function () {
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        return {
            validator:validator
        }
    }

    $(document).ready(function (){
        loginForm = initLoginForm();
        initAlert();
    });

</script>