<?php $this->load->view('fe/templates/header'); ?>
<section class="my-5">
    <div class="container">
        <div class="col col-sm-12 col-md-8 col-lg-7 mx-auto">
            <div class="card rounded-1">
                <div class="card-body">
                    <div class="my-3 col-md-12">
                        <div class="row">
                            <div class="col col-md-12 text-center">
                                <h4>Welcome Back!</h4>
                            </div>
                        </div>
                        <form action="#" id="form_ocw_user_login" class="py-3">
                            <div class="form-group">
                                <label for="inputEmail">Email *</label>
                                <input type="email" class="form-control" id="inputEmail" name="email" placeholder="username@domain.com">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Password *</label>
                                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="••••••••">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="rememberMe" name="remember" value="1">
                                    <label class="form-check-label" for="rememberMe">
                                        Remember me
                                    </label>
                                </div>
                            </div>
                            <div class="alert alert-danger fade show" role="alert" id="failed_alert" style="display: none;">
                                <div class="alert-text" id="failed_message"></div>
                            </div>
                            <?php $message_error = $this->session->flashdata('message_error');?>
                            <div class="alert alert-danger fade show" role="alert" id="failed_alert_server" style="<?php if($message_error){echo '';}else{echo 'display: none';}?>">
                                <div class="alert-text" id="failed_message_server"><?php echo $message_error;?></div>
                            </div>
                            <button type="button" class="btn btn-block btn-primary py-2" id="btn_login">Login</button>
                        </form>
                        <hr>
                        <div class="row py-2">
                            <div class="col col-md-6 col-12 my-2">
                                <button type="button" class="btn btn-block border rounded-1 btn-google py-2" id="btn_login_google"><i class="fab fa-google"></i> Login with Google</button>
                            </div>
                            <div class="col col-md-6 col-12 my-2">
                                <button type="button" class="btn btn-block border rounded-1 btn-facebook py-2" id="btn_login_facebook"><i class="fab fa-facebook"></i> Login with Facebook</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row py-2">
                            <div class="col col-12 text-center">
                                <small><a href="<?php echo base_url('ForgotPassword');?>">Forgot Password?</a></small>
                            </div>
                            <div class="col col-12 text-center">
                                <small><a href="<?php echo base_url('Register');?>">New to Celoe? Create an Account</a></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('fe/templates/footer'); ?>
<?php require '_script.php'; ?>