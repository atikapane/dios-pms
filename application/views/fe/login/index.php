<?php $this->load->view('fe/templates/header') ?>
<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->
<div class="login-wrapper mb-5 py-3 px-3">
    <form action="#" id="form_ocw_user_login_no_modal">
        <div class="form-group">
            <label for="inputEmail">Email*</label>
            <input type="email" placeholder="Email" class="form-control" id="inputEmail" name="email">
            <input type="hidden" name="redir" value="<?php echo $redir?>"/>
        </div>
        <div class="form-group">
            <label for="inputPassword">Password*</label>
            <input type="password" placeholder="Password" class="form-control" name="password" id="inputPassword">
        </div>
        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="rememberMe" name="remember" value="1">
                <label class="form-check-label" for="rememberMe">
                    Remember me
                </label>
            </div>
        </div>
        <div class="alert alert-danger fade show" role="alert" id="failed_alert" style="display: none;">
            <div class="alert-text" id="failed_message"></div>
        </div>
        <?php $message_error = $this->session->flashdata('message_error');?>
        <div class="alert alert-danger fade show" role="alert" id="failed_alert_server" style="<?php if($message_error){echo '';}else{echo 'display: none';}?>">
            <div class="alert-text" id="failed_message_server"><?php echo $message_error;?></div>
        </div>
        <button type="button" id="btn_login_non_modal" class="btn btn-primary-red w-100">Log In</button>
    </form>
    <div class="mt-3 d-flex">
        <button class="btn btn-primary-google w-100 mr-3" id="btn_login_google">Log In With Google</button>
        <button class="btn btn-primary-fb w-100" id="btn_login_facebook">Log In With Facebook</button>
    </div>
    <div class="mt-3">
        <div class="d-flex">
            <div class="ml-auto">
                <div class="d-flex flex-column">
                    <button class="btn-link-red text-right" onClick="window.location = '<?= base_url('ForgotPassword') ?>';">Forgot
                        password?</button>
                    <button class="btn-link-red text-right" onClick="window.location = '<?= base_url('Register') ?>';">New To Celoe?
                        Create an Account</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var redir = "<?php echo $redir?>";
</script>
<?php $this->load->view('fe/templates/footer') ?>
<?php $this->load->view('fe/login/_script') ?>