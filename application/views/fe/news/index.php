<?php $this->load->view('fe/templates/header');?>
<header class="bg-image-full py-5 mb-5" style="background-image: url('http://placehold.jp/1024x300.png');">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <h1 class="display-4 text-white mt-5 mb-2">News</h1>
                    <p class="lead mb-5 text-white-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non possimus ab labore provident mollitia. Id assumenda voluptate earum corporis facere quibusdam quisquam iste ipsa cumque unde nisi, totam quas ipsam.</p>
                </div>
            </div>
        </div>
    </header>

    <section class="py-3">
        <div class="container">
            <div class="row">
                <div class="col com-10 mb-5">
                    <div class="input-group input-group-lg">
                        <input class="form-control border rounded-pill-left border-right-0" type="text" placeholder="Search" id="txt_search">
                        <span class="input-group-append">
                            <button class="btn btn-danger border rounded-pill-right border-left-0 btn-search" type="button" id="btn_search">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-md-10 mx-auto">
                <div class="row" id="news">
                </div>
            </div>
            <div class="col col-md-5 mx-auto py-5">
                <button class="btn btn-danger btn-block" id="btn-load-more" style="display: none">LOAD MORE</button>
            </div>
        </div>
    </section>
    <?php $this->load->view('fe/templates/footer');?>
    <?php require '_script.php';?>

    </body>
</html>
