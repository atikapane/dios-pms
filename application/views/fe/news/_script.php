<script>

    var data = {
        "search":"",
        "limit":<?php echo $this->config->item('limit_news_page');?>,
        "start":0
    };

    var search = false;
    var all_record = 0;

    function loadDataNews(){
        var ajax = $.ajax({
            type:"POST",
            url:"<?php echo base_url().'News/load_news';?>",
            data:{
                search:data.search,
                limit:data.limit,
                start:data.start
            },
            cache:false
        });

        ajax.done(function (res) {

            all_record = res.num_of_rows;

            $("#news").empty();

            if(res.data.length>0){
                append_element(res);

                if(!search){
                    if(data.limit<res.num_of_rows){
                        $("#btn-load-more").show();
                    }else {
                        $("#btn-load-more").hide();
                    }
                }else {
                    data.limit = res.num_of_rows;
                    $("#btn-load-more").hide();
                }
            }else {
                $("#news").append('<div class="col col-12"><p>Data Not Available</p></div>');
            }

        });
    }

    function append_element(res){
        $.each(res.data, function (i, val) {
            var path = "<?php echo base_url();?>";
            var thumbnail = val.thumbnail == null ? "http://placehold.it/300x200":path+val.thumbnail;
            var title = val.title;
            var link = "<?php echo base_url().'News/news_detail/';?>"+val.slug;

            $("#news").append('<div class="col-md-4 col-sm-6 mb-5"><div class="card border rounded-0 border-1"><div class="featured-article"><img src="'+thumbnail+'" alt="" class="img-thumbnail-custom"> </div> <div class="card-body"> <p class="card-text">'+title+'</p> <a href="'+link+'" class="btn btn-outline-danger btn-block">Read More</a></div></div></div>');
        });
    }

    $(document).ready(function () {

        loadDataNews();

        $("#btn_search").on("click", function () {
            var text = $.trim($("#txt_search").val());

            if(text.length>0){
                search = true;

                data.limit = all_record;
                data.search = text;

                loadDataNews();
                $("#btn-load-more").hide();
            }
        });

        // $("#txt_search").on("keyup", function () {
        //     var text = $.trim($(this).val());
        //     search = true;
        //
        //     data.search = text;
        //
        //     loadDataNews();
        //     $("#btn-load-more").hide();
        // });

        $("#btn-load-more").on("click",function () {
            search = false;

            data.limit = data.limit + data.limit;

            loadDataNews();
        });

    });
</script>