        <header class="bg-secondary text-white p-5 clearfix">
            <div class="container text-left">
                <h1 class="title"><?php echo $title; ?></h1>
                <p class="lead"><?php echo $sub_title; ?></p>
            </div>
        </header>
        <section class="p-5">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                          <strong>Informasi</strong> Alamat tautan yang merujuk ke Google Drive hanya dapat diakses dengan Akun Email GAFE Telkom University.
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <table class="table table-bordered table-striped dataTable" style="width:100%">
                            <thead>
                                <tr>
                                    <th><h5>Konten</h5></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($data)) {
                            foreach($data as $row){ ?>
                                <tr>
                                    <td><?php echo "<h6>".$row->title."</h6>".(!empty($row->description) ? "<p>".$row->description."</p>" : ""); ?></td>
                                    <td class="text-right"><a href="<?php echo $row->file_path; ?>" target='_blank' class='text-danger'>Download</a></td>
                                </tr>
                            <?php }}?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </section>