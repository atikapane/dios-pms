<script>
    let category_id = "<?php echo $category_id;?>";
    var data = {
        "search": "<?= $search ?>",
        "filter_category": $("#category_filter option:selected").val(),
        "filter_program": $("#program_filter option:selected").val(),
        "limit":<?php echo $this->config->item('limit_course_page');?>,
        "start":0,
        "limit_default":<?php echo $this->config->item('limit_course_page');?>
    };

    var search = false;
    var all_record = 0;

    function loadDataCourse(){
        var ajax = $.ajax({
            type:"POST",
            url:"<?php echo base_url().'Course/load_course';?>",
            data:{
                faculty_id:data.filter_category,
                program_id:data.filter_program,
                search:data.search,
                limit:data.limit,
                start:data.start
            },
            cache:false
        });

        ajax.done(function (res) {

            all_record = res.num_of_rows;

            $("#loader").hide();
            $("#courses").empty()
            if(res.data.length>0){
                append_element(res);

                if(!search){
                    if(data.limit<res.num_of_rows){
                        $("#btn-load-more").show();
                    }else {
                        $("#btn-load-more").hide();
                    }
                }else {
                    data.limit = res.num_of_rows;
                    $("#btn-load-more").hide();
                }
            }else {
                $("#courses").append('<div class="col col-12"><p>No Course Available </p></div>');
            }

        });
    }

    function append_element(res){
        $.each(res.data, function (i, val) {
            var path = "<?php echo base_url();?>";
            var course_id = val.id;
            var slug = val.slug;
            var banner_course = val.banner_image == null ? "http://placehold.it/300x200":path+val.banner_image;
            var ocw_name = val.ocw_name;
            var desc = val.description.replace(/(<([^>]+)>)/ig,"");
            var description = desc.length>50 ? desc.substr(1,200)+'...':desc;
            var formattedStartTime =  moment(val.start_date, 'YYYY-MM-DD').format('DD-MMM-YY');
            var formattedEndTime =  moment(val.end_date, 'YYYY-MM-DD').format('DD-MMM-YY');
            var date = formattedStartTime +' to '+ formattedEndTime;
            var faculty_name = val.faculty_store_name;
            var study_program = val.study_program_store_name;

            $("#courses").append('<div class="col-md-4 col-sm-6 mb-5"><div class="content-list rounded-0 border list-course" data-slug="'+slug+'"><img src="'+banner_course+'" class="card-img-top rounded-0"> <div class="card-body text-slide"><h6>'+ocw_name+'</h6><p><div><i class="fa fa-tags"></i> '+faculty_name+'</div><div><i class="fa fa-graduation-cap"></i> '+study_program+'</div></p><p>'+description+'</p><p><i class="fa fa-calendar"></i> '+date+'</p><hr> <button type="button" class="btn btn-danger btn-block" id="btn_view_course'+i+'"><i class="fa fa-eye"></i> View</button></div></div></div>');
        });

        $(".list-course").each(function (a) {
            var slug = $(this).data('slug');
            $("#btn_view_course"+a).on('click', function (e) {
                e.preventDefault();
                var base_url = "<?php echo base_url('Course/view');?>";
                window.location.href =  base_url+"/"+slug;
            });
        });
    }

    $(document).ready(function () {
        var content_search = "<?php echo $this->session->userdata('course');?>";
        if(content_search.length>0){
            $("#txt_search").val(content_search);
            <?php $search_course = array('course' => "");
            $this->session->set_userdata($search_course);?>
            data.search = $("#txt_search").val();
            loadDataCourse();
        }else {
            loadDataCourse();
        }

        $("#category_filter").on('change', function (e) {
            data.limit = data.limit_default;
            data.filter_category = $(this).children("option:selected").val();
            // loadDataCourse()
            var ajax = $.ajax({
                url:"<?php echo base_url().'Course/get_program_study';?>",
                type:"POST",
                data:{
                    faculty_id:data.filter_category
                },
                cache:false
            });

            ajax.done(function (res) {
                populateProgram(res.data);
            });
        });

        if (data.filter_category)
            $("#category_filter").trigger('change')

        $("#program_filter").on('change', function (e) {
            data.limit = data.limit_default;
            data.filter_program = $(this).children("option:selected").val();
            loadDataCourse()
        });

        $("#btn_search").on("click", function () {
            var text = $.trim($("#txt_search").val());

            $("#courses").empty();
            $('#loader').show();

            // if(text.length>0){
            search = true;

            // data.limit = all_record;
            data.search = text;

            loadDataCourse();

            $("#btn-load-more").hide();
            // }
        });

        $("#txt_search").on('keyup', function () {
            if (event.keyCode === 13) {
                event.preventDefault();
                $("#btn_search").click();
            }
        });

        $("#btn-load-more").on("click",function () {
            search = false;

            data.limit = data.limit + data.limit;

            $('#loader').show();
            loadDataCourse();
        });

        function populateProgram(program){
            data.filter_program = "";
            $("#program_filter").removeAttr('disabled')
            $(".program").remove();
            $.each(program, function (e, program) {
                $("#program_filter").append('<option class="program" value="'+program.program_id+'">'+program.study_program_name+'</option>');
            });
        }

    });
</script>
<script src="<?php echo $this->config->item("ASSET_PATH");?>vendors/general/moment/moment.js"></script>