<?php $this->load->view('fe/templates/header'); ?>
<section class="my-5">
    <div class="container">
        <div class="col col-sm-12 col-md-8 col-lg-7 mx-auto">
            <div class="card rounded-1">
                <div class="card-body">
                    <div class="my-3 col-md-12">
                        <div class="title_register" <?php if($data_user){echo "style='display:none;'";};?>>
                            <div class="row">
                                <div class="col col-md-12 text-center">
                                    <h3>Create an account using</h3>
                                </div>
                            </div>
                            <div class="row py-3">
                                <div class="col col-md-6 col-12 py-2">
                                    <button type="button" class="btn btn-block border rounded-1 btn-google" id="btn_register_google"><i class="fab fa-google"></i> Google</button>
                                </div>
                                <div class="col col-md-6 col-12 py-2">
                                    <button type="button" class="btn btn-block border rounded-1 btn-facebook" id="btn_register_facebook"><i class="fab fa-facebook"></i> Facebook</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md-12 text-center">
                                    <h3>or create a new one here</h3>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-success mb-0 my-2" role="alert" <?php if(!$data_user){echo "style='display:none;'";};?>>
                            Successful Integrated with your <?php echo $data_user["oauth_provider"];?> account. Please enter other information needed here !
                        </div>
                        <form action="#" id="form_ocw_user_register" class="py-3">
                            <div class="form-group">
                                <label for="inputEmail">Email *</label>
                                <input type="email" class="form-control" id="inputEmail" name="email"
                                       placeholder="username@domain.com" value="<?php echo $data_user["email"];?>" <?php if($data_user){echo "readonly";};?>>
                            </div>
                            <div class="form-group">
                                <label for="inputName">Full name *</label>
                                <input type="text" class="form-control" id="inputName" name="fullname"
                                       placeholder="Full Name" value="<?php echo $data_user["full_name"];?>">
                            </div>
                            <div class="form-group">
                                <label for="inputUsername">Public username *</label>
                                <input type="text" class="form-control" id="inputUsername" name="username"
                                       placeholder="EdwardCullen">
                            </div>
                            <?php if(!$data_user){;?>
                                <div class="form-group">
                                    <label for="inputPassword">Password *</label>
                                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="••••••••">
                                </div>
                            <?php };?>
<!--                            <div class="form-group">-->
<!--                                <label>Country *</label>-->
<!--                                <select placeholder="Choose one Country" id="inputCountry" name="country" class="select2">-->
<!--                                    <option></option>-->
<!--                                    --><?php
//                                    foreach ($countries as $country) {
//                                        echo '<option value="' . $country->id . '">' . $country->name . '</option>';
//                                    }
//                                    ?>
<!--                                </select>-->
<!--                            </div>-->
                            <div class="form-group">
                                <label for="inputCity">City *</label>
                                <select placeholder="Choose one City" id="inputCity" name="city" class="select2">
                                    <option></option>
                                    <?php
                                    foreach ($cities as $city) {
                                        echo '<option value="' . $city->id . '">' . $city->name . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputGender">Gender *</label>
                                <select placeholder="Choose Gender" class="select2" id="inputGender" name="gender">
                                    <option></option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="99">Other/Prefer Not To Say</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputDateBirth">Date of birth *</label>
                                <input type="text" class="form-control datepicker px-2" id="inputDateBirth" name="dateBirth"
                                       placeholder="yyyy-mm-dd" onkeydown="return false">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck" name="gridCheck">
                                    <label class="form-check-label" for="gridCheck">
                                        I agree to the CELOE Terms of Service
                                    </label>
                                </div>
                            </div>
                            <div class="alert alert-danger fade show" role="alert" id="failed_alert_add"
                                 style="display: none;">
                                <div class="alert-text" id="failed_message_add"></div>
                                <div class="alert-close">
                                    <button type="button" class="close" aria-label="Close"
                                            id="failed_alert_dismiss_add">
                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                    </button>
                                </div>
                            </div>
                            <button type="button" class="btn btn-block btn-primary py-2" id="btn_register">Create your account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('fe/templates/footer'); ?>
<?php require '_script.php'; ?>
</body>
</html>
