<?php $this->load->view('fe/templates/header'); ?>
<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->
<div class="container mt-5">
    <div class="caret mt-5 mb-3">
        <div class="caret-title">Term of Service</div>
    </div>
    <div class="jumbotron bg-white terms p-3 fs-12">
        <div class="font-weight-bold fs-15 mb-3">Term of Service</div>
        <?= $agreement->content ?>
    </div>
</div>
<?php $this->load->view('fe/templates/footer') ?>