<?php $this->load->view('fe/templates/header'); ?>
<!-- Shape !-->
<div class="box"></div>
<!-- End Shape !-->
<div class="login-wrapper mb-5 px-3 py-3">
    <div class="text-center mb-3">Create An Account Using</div>
    <div class="mt-3 d-flex">
        <button type="button" class="btn btn-primary-google w-100 mr-3" id="btn_register_google">Google</button>
        <button type="button" class="btn btn-primary-fb w-100" id="btn_register_facebook">Facebook</button>
    </div>
    <hr>
    <div class="text-center mb-3">Or Create a New One Here</div>
    <div class="alert alert-success mb-0 my-2" role="alert" <?php if(!$data_user){echo "style='display:none;'";};?>>
        Successfully Integrated with your <?php echo $data_user["oauth_provider"];?> account. Please enter other information needed here!
    </div>
    <form action="#" id="form_ocw_user_register">
        <div class="form-group">
            <label for="inputEmail">Email*</label>
            <input type="email" placeholder="Email" class="form-control" id="inputEmail" name="email" value="<?php echo !empty($data_user)?$data_user["email"]:'';?>">
        </div>
        <div class="form-group">
            <label for="inputName">Full Name*</label>
            <input type="text" placeholder="Full Name" class="form-control" id="inputName" name="fullname" value="<?php echo !empty($data_user)?$data_user["full_name"]:'';?>">
        </div>
        <div class="form-group">
            <label for="inputUsername">Public Username*</label>
            <input type="text" placeholder="Public Username" class="form-control" id="inputUsername" name="username">
        </div>
        <div class="form-group">
            <label for="inputPassword">Password*</label>
            <input type="password" placeholder="Password" class="form-control" id="inputPassword" name="password">
        </div>
        <div class="form-group">
            <label for="inputRePassword">Re-enter Password*</label>
            <input type="password" placeholder="Password" class="form-control" id="inputRePassword" name="repassword">
        </div>
        <div class="form-group">
            <label for="inputCity">City *</label>
            <select placeholder="Choose one City" id="inputCity" name="city" class="select2">
                <option></option>
                <?php
                foreach ($cities as $city) {
                    echo '<option value="' . $city->id . '">' . $city->name . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="inputGender">Gender *</label>
            <select placeholder="Choose Gender" class="select2" id="inputGender" name="gender">
                <option></option>
                <option value="1">Male</option>
                <option value="2">Female</option>
                <option value="99">Other/Prefer Not To Say</option>
            </select>
        </div>
        <div class="form-group">
            <label for="inputDateBirth">Date of birth *</label>
            <input type="text" class="form-control datepicker px-2" id="inputDateBirth" name="dateBirth"
                placeholder="yyyy-mm-dd" onkeydown="return false">
        </div>
        <div class="form-group">
            <label for="inputPhone">Phone Number*</label>
            <input type="tel" placeholder="Phone Number" class="form-control" id="inputPhone" name="phone_number">
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck" name="gridCheck">
            <label class="form-check-label" for="gridCheck">I Agree to the Celoe <a href="<?= base_url('register/terms') ?>" target="_blank" class="btn-link-red fs-15">Terms of Service</a></label>
        </div>
        <div class="alert alert-danger fade show mt-3" role="alert" id="failed_alert_add" style="display: none;">
            <div class="alert-text" id="failed_message_add"></div>
            <div class="alert-close">
                <button type="button" class="close" aria-label="Close"
                        id="failed_alert_dismiss_add">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
        <button type="button" class="btn btn-primary-red w-100 mt-3" id="btn_register">Register</button>
    </form>
</div>
<?php $this->load->view('fe/templates/footer'); ?>
<script src="<?php echo base_url(); ?>assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<?php $this->load->view('fe/register/_script'); ?>