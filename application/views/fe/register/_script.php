<script>
    var token_file="";
    var AlertUtil;
    var createForm;

    let url_google_register = "<?php echo base_url("register/register_google");?>";
    let url_facebook_register = "<?php echo base_url("register/register_facebook");?>";
    $("#btn_register_google").on('click',function () {
        window.location.href = url_google_register;
    });

    $("#btn_register_facebook").on('click',function () {
        window.location.href = url_facebook_register;
    });

    function initAlert(){
        AlertUtil = {
            showSuccess : function(message,timeout){
                $("#success_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#success_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#success_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess : function(){
                $("#success_alert_dismiss").trigger("click");
            },
            showFailed : function(message,timeout){
                $("#failed_message").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss").trigger("click");
                    },timeout)
                }
                $("#failed_alert").show();
                KTUtil.scrollTop();
            },
            hideSuccess : function(){
                $("#failed_alert_dismiss").trigger("click");
            },
            showFailedDialogAdd : function(message,timeout){
                $("#failed_message_add").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_add").trigger("click");
                    },timeout)
                }
                $("#failed_alert_add").show();
            },
            hideSuccessDialogAdd : function(){
                $("#failed_alert_dismiss_add").trigger("click");
            },
            showFailedDialogEdit : function(message,timeout){
                $("#failed_message_edit").html(message);
                if(timeout != undefined){
                    setTimeout(function(){
                        $("#failed_alert_dismiss_edit").trigger("click");
                    },timeout)
                }
                $("#failed_alert_edit").show();
            },
            hideSuccessDialogAdd : function(){
                $("#failed_alert_dismiss_edit").trigger("click");
            }
        }
        $("#failed_alert_dismiss").on("click",function(){
            $("#failed_alert").hide();
        })
        $("#success_alert_dismiss").on("click",function(){
            $("#success_alert").hide();
        })
        $("#failed_alert_dismiss_add").on("click",function(){
            $("#failed_alert_add").hide();
        })
        $("#failed_alert_dismiss_edit").on("click",function(){
            $("#failed_alert_edit").hide();
        })
    }
    function initCustomRule(){
        $.validator.addMethod("register_password_contain_username", function(value, element) {
            username = $("#inputUsername").val()
            return !(username != "" && value.includes(username))
        }, "Password cannot contain username");
    }

    function initCreateForm(){
        // validator.resetForm();

        // $('#inputCountry').select2({
        //     theme: 'bootstrap4',
        //     width: 'style',
        //     placeholder: $("#inputCountry").attr('placeholder')
        // });
        $('#inputCity').select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $("#inputCity").attr('placeholder')
        });
        $('#inputEducation').select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $("#inputEducation").attr('placeholder')
        });
        $('#inputGender').select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $("#inputGender").attr('placeholder')
        });

        $("#inputDateBirth").datepicker({
            format:"yyyy-mm-dd",
            autoclose:true
        });

        var validator = $('#form_ocw_user_register').validate({
            rules: {
                email: {
                    required: true
                },
                fullname: {
                    required: true
                },
                username: {
                    required: true,
                    minlength:6,
                    maxlength:20,
                    lettersonly: true,
                    pattern: /^[a-z]*$/
                },
                password: {
                    required: true,
                    maxlength:12,
                    minlength:8,
                    register_password_contain_username: true,
                    pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/,
                },
                repassword: {
                    required: true,
                    equalTo: '#inputPassword'
                },
                city: {
                    required: true
                },
                gender: {
                    required: true
                },
                dateBirth: {
                    required: true
                },
                gridCheck: {
                    required: true
                },
                phone_number: {
                    required: true,
                    number: true
                }
            },
            messages: {
                username: {
                    pattern: "Must be lowercase"
                },
                password: {
                    pattern: "Please enter password with at least 1 uppercase character, 1 lowercase character, 1 numeric character and 1 symbol (!@$*{},.\":)"
                }
            },
            errorClass:'error-field',
            errorPlacement: function(error, element) {
                if(element.hasClass('select2') && element.next('.select2-container').length) {
                    error.insertAfter(element.next('.select2-container'));
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                }
                else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                    error.insertAfter(element.parent().parent());
                }
                else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.insertAfter(element.parent());
                }
                else {
                    error.insertAfter(element);
                }
            }
        });

        $("#btn_register").on("click", function () {
            var isValid = $( "#form_ocw_user_register" ).valid();
            if(isValid){
                $("#loading_overlay").show()
                $.ajax({
                    type:"POST",
                    url : "<?php echo base_url().'Register/register/'; ?>",
                    data: $("#form_ocw_user_register").serialize(),
                    dataType:"json",
                    success:function (data, status) {
                        if(data.status == true){
                            window.location.href = "<?php echo site_url(); ?>";
                        }else{
                            $("#loading_overlay").hide()
                            AlertUtil.showFailedDialogAdd(data.message);
                        }
                    },error:function () {
                        $("#loading_overlay").hide()
                        AlertUtil.showFailedDialogAdd("Cannot communicate with server please check your internet connection");
                    }
                });
            }
        });

        return {
            validator:validator
        }
    }

    $(document).ready(function (){
        createForm = initCreateForm();
        initAlert();
        initCustomRule()
    });

</script>