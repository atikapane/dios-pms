<?php $this->load->view('fe/templates/header');?>
<section class="my-5 news-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <div class="col">
                    <div class="release">
                        <span style="color:red">Update</span>
                        <br>
                        <span id="updatedat"><?php echo $news_detail->data_detail->news_date;?></span>
                    </div>
                    <div class="news-title mt-4 mb-4">
                        <h1><span id="title"><?php echo $news_detail->data_detail->title;?></span></h1>
                    </div>
                    <div class="col-md-6 col-lg-7 hyperlink">
                        <a class="fb-ic" href="http://www.facebook.com/share.php?u=<?php echo base_url()."news/view/".$news_detail->data_detail->slug;?>&amp;title=<?php echo $news_detail->data_detail->title;?>" target="_blank">
                            <i class="fab fa-facebook white-text mr-2"> </i>
                        </a>
                        <a class="tw-ic" href="https://twitter.com/share?url=<?php echo base_url()."news/view/".$news_detail->data_detail->slug;?>&amp;text=<?php echo $news_detail->data_detail->title;?>&amp;hashtags=telkomuniversity" target="_blank">
                            <i class="fab fa-twitter white-text mr-2"> </i>
                        </a>
                        <a class="li-ic" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url()."news/view/".$news_detail->data_detail->id;?>" target="_blank">
                            <i class="fab fa-linkedin white-text mr-2"> </i>
                        </a>
                        <a class="li-ic" id="salin-link" data-url="<?php echo base_url()."news/view/".$news_detail->data_detail->slug;?>">
                            <i class="fa fa-link white-text mr-2"></i>
                        </a>
                    </div>
                    <hr>
                </div>
                <div class="col news-content" id="news_detail">
                    <?php echo $news_detail->data_detail->description;?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="news-related p-5 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <h4>Related Articles</h4>
                <div class="mt-5 mb-5" id="related_list">
                    <?php if(count($news_detail->data_related)>0){
                        foreach ($news_detail->data_related as $list_related){?>
                            <div class="row mb-5">
                                <div class="col col-12 col-sm-12 col-md-4 col-lg-4">
                                    <a href="#">
                                        <img src="<?php echo base_url().$list_related->thumbnail;?>" alt="" srcset="" style="object-fit: cover; object-position: center;">
                                    </a>
                                </div>
                                <div class="col col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="mb-3">
                                        <span id="rel_update_at"></span><?php echo $list_related->news_date;?>&nbsp;<span style="color: red;" id="rel_title"><?php echo $list_related->fullname;?></span>
                                    </div>
                                    <h5><?php echo $list_related->title;?></h5>
                                    <p><?php echo substr(strip_tags($list_related->description), 0, 150); ?></p>
                                    <p><a href="<?php echo base_url().'News/view/'.$list_related->slug;?>" class="btn btn-outline-danger">Read More</a> </p>
                                </div>
                            </div>

                    <?php }}else{?>
                        <div>Article Not Related</div>
                    <?php }?>
                </div>
                <div>
                    <div><a class="text-danger" href="<?php echo base_url('News')?>">Go to News List >></a></div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view("fe/templates/footer");?>
<script>
    $(document).ready(function(){
        $('#salin-link').click(function(){
            let url = $(this).data('url')
            let textArea = document.createElement("textarea")
            textArea.textContent = url
            document.body.appendChild(textArea)
            textArea.select()
            copy = document.execCommand("Copy")
            textArea.remove()
        })

        var title = '<?= $news_detail->data_detail->title;?>';
        $('head').append(`<meta property="og:title" content="${title}" />`);

        var image = '<?= $this->config->item("ASSET_PATH") . 'images/girl-holding-pencil-while-looking-at-imac-4144223.jpg'?>';
        $('head').append(`<meta property="og:image" content="${image}" />`);

        var description = `<?= strip_tags($news_detail->data_detail->description) ?>`;
        if(description.length > 100){
            description = description.slice(0, 100)
            description += '...'
        }
        $('head').append(`<meta property="og:description" content="${description}" />`);

        var url = '<?= base_url().'news/news_detail/'.$list_related->slug; ?>';
        $('head').append(`<meta property="og:url" content="${url}" />`);

        $('head').append(`<meta property="og:type" content="article" />`);
        $('head').append(`<meta property="og:site_name" content="CELOE" />`);
    })
</script>

</body>
</html>
