<div class="course-content">
    <div class="col col-12">
        <h3>Course Profile</h3>
    </div>
    <hr>
    <div class="col col-12 py-3">
        <p><?= $course->course_profile ?>
        </p>

        <?php $image = is_null($course) ? 'media/img/course/course2.png' : $course->banner_image ?>
    </div>
</div>