<style>
    #accordionExample .fa{
        margin-right: 0.5rem;
    }

    .open-content{
        color: #ffffff !important;
    }

    .open-content:hover{
        text-decoration: none;
    }

    .fa-minus-circle{
        color: red;
    }

    .fa-plus-circle{
        color: red;
    }

    span{
        color: #000000;
    }

</style>
<div class="course-content">
    <div class="col col-12">
        <h3>Lecture Notes</h3>
    </div>
    <hr>
    <div class="col col-12 col-md-12 py-3">
        <div id="accordionExample">
            <?php $n = 1 ?>
            <?php foreach ($notes as $key => $note): ?>
                <div class="my-3">
                    <a class="open-content" data-toggle="collapse" data-target="#collapse<?= $n ?>" aria-expanded="<?= $n == 1 ? 'true' : 'false' ?>" aria-controls="collapse<?= $n ?>" href="#">
                        <h5><span><i class="fa fa-<?= $n == 1 ? 'minus' : 'plus' ?>-circle"></i><?= $key ?></span></h5>
                    </a>
                    <div id="collapse<?= $n ?>" class="collapse <?= $n == 1 ? 'show' : '' ?> m-2">
                        <div class="card rounded-0">
                            <div class="card-body">
                                <div class="col col-12 col-md-12 py-3">
                                    <?php foreach ($note as $note): ?>
                                        <div class="row py-2 border-bottom">
                                            <div class="col col-8 col-sm-8 col-md-10"><?= $note->content_name ?></div>
                                            <div class="col col-4 col-sm-4 col-md-2 text-center"><a href="<?= $note->content_status ? $note->content_url : '#' ?>"><?= $note->content_status ? 'Open' : '<i class="fa fa-lock" aria-hidden="true"></i>' ?></a></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $n++ ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>