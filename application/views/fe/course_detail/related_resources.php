<style>
    .thead-dark th {
        background-color: #ffffff !important;
        color: #000000 !important;
        font-weight: bold;
        border: 1px solid #dee2e6 !important;
    }
</style>
<div class="course-content">
    <div class="col col-12">
        <h3>Related Resources</h3>
    </div>
    <hr>
    <div class="col col-12 py-3">
        <table class="table table-responsive table-bordered table-striped">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Pokok Bahasan</th>
                <th>Sumber Belajar Terkait</th>
                <th>Website</th>
            </tr>
            </thead>
            <tbody>
            <?php $n = 1 ?>
            <?php foreach ($resources as $url): ?>
                <tr>
                    <th scope="row"><?= $n++ ?></th>
                    <td><?= $url->section_name ?></td>
                    <td><?= $url->content_name ?></td>
                    <td>
                        <a href="<?= $url->content_status ? $url->content_url : '#' ?>"><?= $url->content_status ? 'Open' : '<i class="fa fa-lock" aria-hidden="true"></i>' ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>