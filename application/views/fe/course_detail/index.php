<?php $this->load->view('fe/templates/header'); ?>
<style>
    #side-menu {
        font-weight: bold;
        color: #0b2e13;
    }

    #side-menu li a {
        font-weight: bold;
        color: #0b2e13;
    }

    #side-menu li a:hover {
        color: #df2b2b;
        text-decoration: none;
    }

    .course-content img {
        width: 100%;
        height: auto;
    }

    #side-menu .active-link{
        background-color: #df2b2b;
    }

    #side-menu .active-link a{
        color: #FFFFFF;
    }

    #side-menu .active-link a:hover{
        color: #FFFFFF;
        text-decoration: none;
    }

    #side-menu-lite {
        font-weight: bold;
        color: #0b2e13;
    }

    #side-menu-lite a {
        /*font-weight: bold;*/
        color: #0b2e13;
    }

    #side-menu-lite a:hover {
        color: #df2b2b;
        text-decoration: none;
    }

    #side-menu-lite .active-link{
        background-color: #df2b2b;
    }

    #side-menu-lite a .active-link{
        color: #FFFFFF;
    }


    @media only screen and (max-width: 768px), only screen and (max-device-width: 768px) {
        #side-menu{
            display: none;
            border: none;
            margin-bottom: 1.5rem !important;
        }

        .list-group{
            padding-left: 20px;
            padding-right: 20px;
        }

        .list-group-item{
            border: none!important;
            padding: .50rem 1rem;
        }
    }

    @media(min-width: 768px) {
        #side-menu {
            display: block!important;
            border: none;
            border-right: 1px solid rgba(0,0,0,.1);
        }
        #side-menu-lite{
            display: none;
        }

    }

    @media only screen and (max-width: 320px), only screen and (max-device-width: 320px) {
        .list-group-item{
            border: none!important;
            padding: .50rem 1rem;
        }

        body{
            font-size: 90% !important;
        }
    }

    div .sticky-top{
        top:70px!important;
        margin-bottom: 20px !important;
    }

    #btnScrollTop{
        position:fixed;
        right: 10px;
        bottom: 10px;
        border-radius:5%;
        background: #df2b2b;
        box-shadow: 0 0 10px rgba(0,0,0,0.25);
        color: #FFFFFF;
        outline: none;
        cursor: pointer;
        border: none;
        width: 32px;
        height: 32px;
        z-index: 10000;
        display: none;
    }

    .list-group-item:first-child{
        border-top-left-radius:0px!important;;
        border-top-right-radius:0px!important;;
    }

    .list-group-item:last-child{
        border-bottom-right-radius:0px!important;
        border-bottom-left-radius:0px!important;;
    }

</style>
<?php
    $active = "active-link";
?>
<div class="py-4">
    <div class="container">
        <div class="row">
            <div class="col col-lg-3 col-md-4 col-sm-12 col-12 list-group list-group-flush" id="side-menu">
                <ul class="nav flex-column sticky-top">
                    <li class="nav-item list-group-item <?php echo $page == "course_home"? $active:'';?>"">
                        <a href="<?php echo base_url("Course/view/$course->slug");?>"><i class="fa fa-home"></i> COURSE HOME</a>
                    </li>
                    <li class="nav-item list-group-item <?php echo $page == "course_profile"?$active:'';?>">
                        <a href="<?php echo base_url("Course/profile/$course->slug");?>"><i class="fa fa-atom"></i> COURSE PROFILE</a>
                    </li>
                    <li class="nav-item list-group-item <?php echo $page == "syllabus"? $active:'';?>">
                        <a href="<?php echo base_url("Course/syllabus/$course->slug");?>"><i class="fa fa-book"></i> SYLLABUS</a>
                    </li>
                    <li class="nav-item list-group-item <?php echo $page == "lecture_notes"?$active:'';?>">
                        <a href="<?php echo base_url("Course/notes/$course->slug");?>"><i class="fa fa-pen"></i> LECTURE NOTES</a>
                    </li>
                    <li class="nav-item list-group-item <?php echo $page == "course_video"?$active:'';?>">
                        <a href="<?php echo base_url("Course/videos/$course->slug");?>"><i class="fa fa-play-circle"></i> COURSE VIDEO</a>
                    </li>
                    <li class="nav-item list-group-item <?php echo $page == "assignments"?$active:'';?>">
                        <a href="<?php echo base_url("Course/assignments/$course->slug");?>"><i class="fa fa-address-card"></i> ASSIGNMENTS</a>
                    </li>
                    <li class="nav-item list-group-item <?php echo $page == "exam"?$active:'';?>">
                        <a href="<?php echo base_url("Course/exam/$course->slug");?>"><i class="fa fa-file"></i> EXAM</a>
                    </li>
                    <li class="nav-item list-group-item <?php echo $page == "related_resources"?$active:'';?>">
                        <a href="<?php echo base_url("Course/resources/$course->slug");?>"><i class="fa fa-link"></i> RELATED RESOURCES</a>
                    </li>
                    <li class="nav-item list-group-item">
                        <a href="http://openlibrary.telkomuniversity.ac.id/"><i class="fa fa-book-open"></i> REFERENCES</a>
                    </li>
                </ul>
            </div>
            <div id="side-menu-lite">
                <ul class="nav flex-column sticky-top">
                    <a href="<?php echo base_url('CourseDetail/index')?>">
                        <li class="list-group-item <?php echo $page == "course_home"? $active:'';?>" data-toggle="tooltip" data-placement="right" title="Course Home">
                            <i class="fa fa-home"></i>
                        </li>
                    </a>
                    <a href="<?php echo base_url('CourseDetail/courseProfile')?>">
                        <li class="list-group-item <?php echo $page == "course_profile"?$active:'';?>" data-toggle="tooltip" data-placement="right" title="Course Profile">
                            <i class="fa fa-atom"></i>
                        </li>
                    </a>
                    <a href="<?php echo base_url('CourseDetail/sylabus')?>">
                        <li class="list-group-item <?php echo $page == "syllabus"? $active:'';?>" data-toggle="tooltip" data-placement="right" title="Syllabus">
                            <i class="fa fa-book"></i>
                        </li>
                    </a>
                    <a href="<?php echo base_url('CourseDetail/lectureNotes')?>">
                        <li class="list-group-item <?php echo $page == "lecture_notes"?$active:'';?>" data-toggle="tooltip" data-placement="right" title="Lecture Notes">
                            <i class="fa fa-pen"></i>
                        </li>
                    </a>
                    <a href="<?php echo base_url('CourseDetail/courseVideo')?>">
                        <li class="list-group-item <?php echo $page == "course_video"?$active:'';?>" data-toggle="tooltip" data-placement="right" title="Course Video">
                            <i class="fa fa-play-circle"></i>
                        </li>
                    </a>
                    <a href="<?php echo base_url('CourseDetail/assignments')?>">
                        <li class="list-group-item <?php echo $page == "assignments"?$active:'';?>" data-toggle="tooltip" data-placement="right" title="Assignments">
                            <i class="fa fa-address-card"></i>
                        </li>
                    </a>
                    <a href="<?php echo base_url('CourseDetail/exam')?>">
                        <li class="list-group-item <?php echo $page == "exam"?$active:'';?>" data-toggle="tooltip" data-placement="right" title="Exam">
                            <i class="fa fa-file"></i>
                        </li>
                    </a>
                    <a href="<?php echo base_url('CourseDetail/relatedResources')?>">
                        <li class="list-group-item <?php echo $page == "related_resources"?$active:'';?>" data-toggle="tooltip" data-placement="right" title="Related Course">
                            <i class="fa fa-link"></i>
                        </li>
                    </a>
                    <a href="http://openlibrary.telkomuniversity.ac.id/">
                        <li class="nav-item list-group-item" data-toggle="tooltip" data-placement="right" title="References">
                            <i class="fa fa-book-open"></i>
                        </li>
                    </a>
                </ul>
            </div>
            <div class="col col-lg-9 col-md-8 col-10">
                <?php $this->load->view($content);?>
                <button id="btnScrollTop">
                    <i class="fa fa-arrow-up"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('fe/templates/footer');
require "_script.php";
?>

