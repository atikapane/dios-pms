<style>
    #accordion .fa {
        margin-right: 0.5rem;
    }

    .open-content {
        color: #ffffff !important;
    }

    .open-content:hover {
        text-decoration: none;
    }

    .open-sub-content {
        color: #ffffff !important;
    }

    .open-sub-content:hover {
        text-decoration: none;
        cursor: pointer;
    }

    .fa-minus-circle {
        color: red;
    }

    .fa-plus-circle {
        color: red;
    }

    span {
        color: #000000;
    }

    .hidden {
        display: none;
    }
</style>
<div class="course-content">
    <div class="col col-12">
        <h3>Assignments</h3>
    </div>
    <hr>
    <div class="col col-12 col-md-12 py-3">
        <div id="accordion">
            <?php $n = 1 ?>
            <?php foreach ($assignments as $key => $section ): ?>
                <div class="my-3">
                    <a class="open-content <?= $n == 1 ? 'collapsed' : '' ?>" data-toggle="collapse" data-target="#collapse<?= $n ?>" aria-expanded="<?= $n == 1 ? 'true' : 'false'?>"
                       aria-controls="collapse<?= $n ?>" href="#">
                        <h5><span><i class="fa fa-<?= $n == 1 ? 'minus' : 'plus' ?>-circle"></i><?= $key ?></span></h5>
                    </a>
                    <div id="collapse<?= $n ?>" class="collapse <?= $n == 1 ? 'show' : '' ?>">
                        <div class="col col-12">

                            <!-- <div>Accordion 2 </div>-->
                            <div id="accordionSub<?= $n ?>">
                                <?php $x = 1 ?>
                                <?php foreach ($section as $task): ?>
                                    <div class="rounded-0 my-2 border-0">
                                    <span role="button" class="open-sub-content" id="openSubContent<?= $n . '-' . $x ?>">
                                        <h5><span><i class="fa fa-<?= $n == 1 && $x == 1 ? 'minus' : 'plus' ?>-circle"></i><?= $task->content_name ?></span></h5>
                                    </span>

                                        <div id="collapseSub<?= $n . '-' . $x ?>" class="<?= $n == 1 && $x == 1 ? '' : 'hidden' ?>">
                                            <div class="card rounded-0">
                                                <div class="card-body">
                                                    <div class="col col-12">
                                                        <div class="row py-2">
                                                            <div class="col col-12"><?= $task->content_desc ?>
                                                            </div>
                                                        </div>
                                                        <div class="row py-2">
                                                            <div class="col col-12">
                                                                <?php if ($task->content_status): ?>
                                                                    <a href="<?= $task->content_url ?>"><?= $task->content_url ?></a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $x++ ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $n++ ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>