<style>
    .thead-dark th {
        background-color: #ffffff !important;
        color: #000000 !important;
        font-weight: bold;
        border:1px solid #dee2e6 !important;
    }
</style>
<div class="course-content">
    <div class="col col-12">
        <h3>Syllabus</h3>
    </div>
    <hr>
    <div class="col col-12 py-3 table-responsive ">
        <table class="table table-bordered table-striped">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Topic</th>
                <th>Duration (Day)</th>
            </tr>
            </thead>
            <tbody>
            <?php $n = 1 ?>
            <?php foreach ($syllabus as $section): ?>
                <tr>
                    <th scope="row"><?= $n++ ?></th>
                    <td><?= $section->section_name ?></td>
                    <td><?= $section->duration ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>