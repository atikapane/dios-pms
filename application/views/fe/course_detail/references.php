<style>
    #accordionExample .fa{
        margin-right: 0.5rem;
    }

    .card-header{
        padding:5px !important;
        background-color: #df2b2b !important;
        color: #ffffff;
    }

    .btn-link{
        text-decoration: none !important;
        color: #ffffff !important;
    }
    .btn-link:hover{
        text-decoration: none !important;
        color: #ffffff !important;
    }
</style>
<div class="course-content">
    <div class="col col-12">
        <h3>References</h3>
    </div>
    <hr>
    <div class="col col-12 py-3">
        <div class="card rounded-0 my-2 border-danger">
            <div class="card-header rounded-0" id="headingOne">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSubOne" aria-expanded="true" aria-controls="collapseSubOne">
                    Title Resources 1
                </button>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <div class="col col-12 py-3">
                        <div class="row py-2">
                            <div class="col col-10"><a href="#">https://www.telkomuniversity.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>