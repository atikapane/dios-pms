<style>
    #accordionExample .fa{
        margin-right: 0.5rem;
    }
    .modal-dialog {
        max-width: 800px;
        margin: 50px auto;
    }


    .modal-body {
        position: relative;
        padding: 0px;
    }

    .close {
        position: absolute;
        right: -30px;
        top: 0;
        z-index: 999;
        font-size: 2rem;
        font-weight: normal;
        color: #fff;
        opacity: 1;
    }

    .open-content{
        color: #ffffff !important;
    }

    .open-content:hover{
        text-decoration: none;
    }

    .fa-minus-circle{
        color: red;
    }

    .fa-plus-circle{
        color: red;
    }

    span{
        color: #000000;
    }
</style>

<div class="course-content">
    <div class="col col-12">
        <h3>Course Video</h3>
    </div>
    <hr>
    <div class="col col-12 col-md-12 py-3">
        <div id="accordionExample">
            <?php $n = 1 ?>
            <?php foreach ($videos as $key => $section): ?>
                <div class="my-3">
                    <a class="open-content" data-toggle="collapse" data-target="#collapse<?= $n ?>" aria-expanded="<?= $n == 1 ? 'true' : 'false' ?>" aria-controls="collapse<?= $n ?>" href="#">
                        <h5><span><i class="fa fa-<?= $n == 1 ? 'minus' : 'plus' ?>-circle"></i><?= $key ?></span></h5>
                    </a>
                    <div id="collapse<?= $n ?>" class="collapse <?= $n == 1 ? 'show' : '' ?> m-2">
                        <div class="card rounded-0">
                            <div class="card-body">
                                <div class="col col-12 col-md-12 py-3">
                                    <?php foreach ($section as $video): ?>
                                        <div class="row py-3 border-bottom">
                                            <div class="col col-12 col-sm-12 col-md-4 col-lg-4">
                                                <a href="#">
                                                    <img class="img-fluid"
                                                         src="<?php echo $this->config->item("ASSET_PATH"); ?>media/img/course/course2.png"
                                                         alt="Most part fantastic faculty members for the most students">
                                                </a>
                                            </div>
                                            <div class="col col-12 col-sm-12 col-md-5 col-lg-5">
                                                <?= $video->content_name ?>
                                            </div>
                                            <div class="col col-12 col-sm-12 col-md-3 col-lg-3 text-center">
                                                <a href="#" class="video-btn" data-toggle="modal"
                                                   data-src="<?= $video->content_status ? $video->content_url : '#' ?>" data-target="<?= $video->content_status ? '#myModal' : '' ?>"><?= $video->content_status ? 'Open' : '<i class="fa fa-lock" aria-hidden="true"></i>' ?></a>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $n++ ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                            allow="autoplay"></iframe>
                </div>

            </div>

        </div>
    </div>
</div>