<script>
    var questions = []
    var currentQuestion = [];
    var selectedAnswer = [[]];

    var question = [];
    var numChoices = [];

    var questionElm = [];
    var choiceElm = [];
    var totalExam = [];
    var choice = []

    $(document).ready(function () {

        // Sidebar
        sidebar_page();

        // Course Video
        course_video_page();

        // Accordion Content
        accordion();

        // Exam
        exam();

        $("#btnScrollTop").on("click", function () {
            $('html,body').animate({scrollTop: 0}, 'slow');
            return false;
        });

        $(document).scroll(function () {
            var y = $(this).scrollTop();
            if (y > 500) {
                $("#btnScrollTop").fadeIn()
            } else {
                $("#btnScrollTop").fadeOut()
            }
        });

        $('[data-toggle="tooltip"]').tooltip();

    });

    function sidebar_page() {
        $('#toggler-sidebar').on('click', function () {
            $('#side-menu').slideToggle(250);
            $(".fa", this).toggleClass("fa-minus-circle fa-plus-circle");
        });
    }

    function course_video_page() {
        var videoSrc;
        $('.video-btn').click(function () {
            videoSrc = $(this).data("src");
        });

        $('#myModal').on('shown.bs.modal', function (e) {
            $("#video").attr('src', videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
        });

        $('#myModal').on('hide.bs.modal', function (e) {
            $("#video").attr('src', videoSrc);
        });
    }

    function accordion() {
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).prev(".open-content").find(".fa").removeClass("fa-plus-circle").addClass("fa-minus-circle");
        }).on('hide.bs.collapse', function () {
            $(this).prev(".open-content").find(".fa").removeClass("fa-minus-circle").addClass("fa-plus-circle");
        });

        $("#accordion").find(".open-content").each(function (j) {
            j++;
            $("#collapse" + j).find(".open-sub-content").each(function (i) {
                i++;
                $("#openSubContent" + j + '-' + i).on('click', function () {
                    $("#collapseSub" + j + '-' + i).slideToggle(250);
                    $(".fa", this).toggleClass("fa-minus-circle fa-plus-circle");
                });
            });
        });

    }

    function exam() {
        $("[id=btn_next]").removeAttr('disabled')

        var course = <?= json_encode(isset($exams) ? $exams : ''); ?>;
        let i = 0
        $.each(course, function () {
            let self = this;

            questions.push([])
            currentQuestion.push([])
            question.push([])
            numChoices.push([])
            questionElm.push([])
            choiceElm.push([])
            totalExam.push([])
            choice.push([])

            let x = 0
            $.each(self, function () {
                try {
                    questions[i].push(JSON.parse(this.content_json).questions)
                } catch (e) {
                    // arrErrKey.push(i)
                    questions[i].push('')
                    console.log(e)
                }
                questionElm[i].push($(`#question${i + 1}-${x + 1}`))
                choiceElm[i].push($(`#choice${i + 1}-${x + 1}`))
                totalExam[i].push($(`#total_exam${i + 1}-${x + 1}`))

                x++
            })
            i++
        })

        $.each(questions, function (i) {
            section = this

            $.each(section, function (x) {
                currentQuestion[i][x] = 0
                question[i][x] = this[currentQuestion[i][x]] ? this[currentQuestion[i][x]].question_text : ''
                numChoices[i][x] = this[currentQuestion[i][x]] ? this[currentQuestion[i][x]].answer.length : 0

                totalExam[i][x].text(currentQuestion[i][x] + 1 + ' of ' + questions[i][x].length);
                questionElm[i][x].html(question[i][x]);

                for (let a = 0; a < numChoices[i][x]; a++) {
                    choice[i][x] = questions[i][x][currentQuestion[i][x]].answer[a].answer
                    $('<div class="form-check"><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="customRadioInline' + i + '-' + x + '" name="customRadioInline' + i + '-' + x + '" class="custom-control-input"> <label class="custom-control-label" for="customRadioInline' + i + '-' + x + '">' + choice[i][x] + '</label></div></div>').appendTo(choiceElm[i][x]);
                }
                $(`[id="btn_next"][data-section=${i + 1}-${x + 1}]`).on('click', function () {
                    currentQuestion[i][x]++;

                    searchQuestion(i, x, currentQuestion[i][x])
                });

                $(`[id="btn_prev"][data-section=${i + 1}-${x + 1}]`).on('click', function () {
                    currentQuestion[i][x]--;

                    searchQuestion(i, x, currentQuestion[i][x])
                });

                $(`[id="button_search"][data-section=${i + 1}-${x + 1}]`).on('click', function () {
                    let input = $(`[id="input_search"][data-section=${i + 1}-${x + 1}]`)
                    let currentQuestion = parseInt(input.val()) - 1
                    searchQuestion(i, x, currentQuestion)
                    input.val('')
                });

                $(`[id="input_search"][data-section=${i + 1}-${x + 1}]`).keypress(function (event) {
                    if (event.keyCode === 13) {
                        searchQuestion(i, x, parseInt(this.value) - 1)
                        this.value = ''
                    }
                });
            })
        })

        function searchQuestion(i, x, current) {
            currentQuestion[i][x] = current

            if (currentQuestion[i][x] == 0) {
                $(`[id="btn_next"][data-section=${i + 1}-${x + 1}]`).removeAttr('disabled');
                $(`[id="btn_prev"][data-section=${i + 1}-${x + 1}]`).attr('disabled', 'disabled');
            }

            if (currentQuestion[i][x] > 0 || currentQuestion < questions[i][x].length - 1) {
                $(`[id="btn_next"][data-section=${i + 1}-${x + 1}]`).removeAttr('disabled');
                $(`[id="btn_prev"][data-section=${i + 1}-${x + 1}]`).removeAttr('disabled');
            }

            if (currentQuestion[i][x] == questions[i][x].length - 1) {
                $(`[id="btn_next"][data-section=${i + 1}-${x + 1}]`).attr('disabled', 'disable');
                $(`[id="btn_prev"][data-section=${i + 1}-${x + 1}]`).removeAttr('disabled');
            }

            question[i][x] = questions[i][x][currentQuestion[i][x]].question_text;
            numChoices[i][x] = questions[i][x][currentQuestion[i][x]].answer.length;

            totalExam[i][x].text(currentQuestion[i][x] + 1 + ' of ' + questions[i][x].length);

            choiceElm[i][x].empty();
            questionElm[i][x].html(question[i][x]);

            for (let a = 0; a < numChoices[i][x]; a++) {
                choice[i][x] = questions[i][x][currentQuestion[i][x]].answer[a].answer
                $('<div class="form-check"><div class="custom-control custom-radio custom-control-inline"><input type="radio" id="customRadioInline' + i + '-' + x + '" name="customRadioInline' + i + '-' + x + '" class="custom-control-input"> <label class="custom-control-label" for="customRadioInline' + i + '-' + x + '">' + choice[i][x] + '</label></div></div>').appendTo(choiceElm[i][x]);
            }
        }
    }
</script>