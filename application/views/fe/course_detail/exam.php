<style>
    #accordion .fa{
        margin-right: 0.5rem;
    }

    .open-content{
        color: #ffffff !important;
    }

    .open-content:hover{
        text-decoration: none;
    }

    .open-sub-content{
        color: #ffffff !important;
    }

    .open-sub-content:hover{
        text-decoration: none;
        cursor: pointer;
    }

    .fa-minus-circle{
        color: red;
    }

    .fa-plus-circle{
        color: red;
    }

    span{
        color: #000000;
    }

    .hidden{
        display: none;
    }
</style>
<div class="course-content">
    <div class="col col-12">
        <h3>Exam</h3>
    </div>
    <hr>
    <div class="col col-12 col-md-12 py-3">
        <div id="accordion">
            <?php $n = 1 ?>
            <?php foreach ($exams as $key => $section): ?>
                <div class="my-3">
                    <a class="open-content" data-toggle="collapse" data-target="#collapse<?= $n ?>" aria-expanded="<?= $n == 1 ? 'true' : 'false' ?>" aria-controls="collapse<?= $n ?>" href="#">
                        <h5><span><i class="fa fa-<?= $n == 1 ? 'minus' : 'plus' ?>-circle"></i><?= $key ?></span></h5>
                    </a>
                    <div id="collapse<?= $n ?>" class="collapse <?= $n == 1 ? 'show' : '' ?>">
                        <div class="col col-12">

                            <!-- <div>Accordion 2 </div>-->
                            <div id="accordionSub1">
                                <?php $x = 1 ?>
                                <?php foreach ($section as $quiz): ?>
                                    <div class="rounded-0 border-0">
                                        <span role="button" class="open-sub-content" id="openSubContent<?= $n . '-' . $x ?>">
                                            <h5><span><i class="fa fa-<?= $n == 1 && $x == 1 ? 'minus' : 'plus' ?>-circle"></i><?= $quiz->content_name ?></span></h5>
                                        </span>

                                        <div id="collapseSub<?= $n . '-' . $x ?>" class="<?= $n == 1 && $x == 1 ? '' : 'hidden' ?>">
                                            <div class="card rounded-0">
                                                <div class="card-body">
                                                    <div class="col col-12 py-3">
                                                        <div class="row py-2">
                                                            <div class="col col-12 py-3">
                                                                <div class="row py-2">
                                                                    <div class="col offset-md-8 offset-xs-5 col-12 col-md-4">
                                                                        <div class="input-group">
                                                                            <input type="number" min="1" class="form-control" placeholder="Exam No" aria-label="Recipient's username" aria-describedby="button-addon2" id="input_search" data-section="<?= $n . '-' . $x ?>">
                                                                            <div class="input-group-append">
                                                                                <button class="btn btn-secondary" type="button" id="button_search" data-section="<?= $n . '-' . $x ?>">
                                                                                    <i class="fa fa-search"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-12">
                                                                        <label id="question<?= $n . '-' . $x ?>"></label>
                                                                        <div class="form-group" id="choice<?= $n . '-' . $x ?>"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col col-6">
                                                                        <button class="btn btn-outline-secondary" id="btn_prev" data-section="<?= $n . '-' . $x ?>" disabled>Prev
                                                                        </button>
                                                                        <button class="btn btn-outline-primary" id="btn_next" data-section="<?= $n . '-' . $x ?>" disabled>Next
                                                                        </button>
                                                                    </div>
                                                                    <div class="col col-6">
                                                                        <span class="float-right" id="total_exam<?= $n . '-' . $x ?>"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $x++ ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php $n++ ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>