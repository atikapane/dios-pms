<style>
    /* read more */
    .course-content {
        max-height: 700px;
        position: relative;
        overflow: hidden;
    }

    .content-fade {
        position: absolute;
        bottom: 0;
        padding: 80px 0;
        background: linear-gradient(to top, rgba(241, 244, 245, 1) 0%, rgba(241, 244, 245, 0) 100%);
    }

    .read-more {
        text-align: center;
        margin: 0;
        padding: 16px 0;
    }

    .read-more__link {
        text-decoration: none;
        color: black;
        font-weight: bold;
    }

    .read-more__link:hover {
        text-decoration: none;
        color: #DF2B2B;
    }

    /* ./read more */

    /* tabs */
    .tabs__item {
        height: 15%;
        display: flex;
        border-bottom: 3px solid #8A8A8C;
    }

    .tabs__link {
        background: transparent;
        width: 50%;
        border: none;
        outline: none;
        cursor: pointer;
        color: #bdbdbd;
        margin: 0;
        padding: 10px 16px;
        font-weight: bold;
        font-size: 1.2em;
    }

    .tabs__link:focus {
        outline: 0;
    }

    .tabs__link:first-child {
        border-right: 1px solid rgba(0, 0, 0, .1);
    }

    .tabs__link--active {
        color: black;
        border-bottom: 3px solid #8A8A8C;
    }

    .tabs__content {
        height: 85%;
        padding: 32px 0;
        box-sizing: border-box;
        display: none;
    }

    .tabs__content--active {
        display: block;
    }

    /* ./tabs */

    /* rating */
    .rating {
        display: flex;
        flex-wrap: wrap;
    }

    .center-content {
        position: absolute;
        width: 100%;
        text-align: center;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .rating__number {
        width: 25%;
        position: relative;
    }

    .rating__number__title {
        font-size: 6.8em;
        font-weight: bold;
        line-height: .8;
    }

    .rating__number__description {
        font-size: 1.5em;
        line-height: 1;
    }

    .rating__star {
        display: flex;
        flex-direction: column;
        width: 25%;
    }

    .star .fa-star:last-child {
        margin-right: 10px;
    }

    .fa-star.active {
        color: orange;
    }

    .feedback__submit {
        width: 50%;
        position: relative;
    }

    .feedback__submit .fa-star {
        font-size: 3em;
        display: block;
        margin-bottom: 16px;
    }

    .submit__overlay {
        position: fixed;
        z-index: 100000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0, 0, 0, .3);
        display: none;
    }

    .card.middle {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 500px;
    }

    .rating__submit .star .fa-star {
        margin-right: 12px;
        font-size: 1.8rem;
        cursor: pointer;
    }

    .rating__submit .row {
        height: 60px;
    }

    .row .input {
        align-self: center;
    }

    .feedback__input {
        width: 100%;
        border: none;
        outline: none;
        border-bottom: 1px solid #CCCED0;
        padding: 6px;
    }

    .feedback__input:focus {
        outline: none;
    }

    .feedback__input::placeholder {
        color: #bdbdbd;
    }

    .feedback__cancel,
    .feedback__button {
        height: 60%;
        width: 44%;
        box-shadow: 0 0 10px rgba(0, 0, 0, .4);
    }

    /* ./rating */

    /* feedback */
    .profile {
        position: absolute;
        border-radius: 50%;
        left: 50%;
        transform: translateX(-50%);
        width: 60px;
        height: 60px;
    }

    .star.mini-star {
        font-size: .8em;
    }

    .tgl-rating {
        color: #CCCED0;
        font-size: .8em;
    }

    /* ./feedback */

    /* comment */
    .comment-input {
        height: 60px;
    }

    .comment__input {
        background: white;
        width: 100%;
        padding: 6px;
        border: none;
        outline: none;
        border-radius: 5px;
    }

    .comment__input:focus {
        outline: none;
    }

    .comment__input::placeholder {
        color: #bdbdbd;
    }

    ul.nested-comment {
        padding: 0;
        list-style: none;
    }

    ul.nested-comment li {
        margin-left: 0;
        list-style: none;
    }

    .comment__reply,
    .comment__view {
        margin-bottom: 0;
    }

    /* ./comment */

    ul ul {
        padding-left: 0;
        margin-left: 80px;
    }

    a {
        text-decoration: none;
        color: black;
    }

    a:hover {
        text-decoration: none;
        color: #DF2B2B;
    }

    @media (max-width: 991px) {

        /* rating */
        .rating {
            flex-wrap: wrap;
        }

        .rating__number {
            width: 50%;

        }

        .rating__star {
            width: 50%;
        }

        .feedback__submit {
            width: 100%;
            min-height: 130px;
        }

        .card.middle {
            width: 300px;
        }

        /* rating */
    }
</style>
<?php $session = $this->session->userdata('user_data'); ?>
<div class="course-content">
    <div class="col col-12">
        <h3>FEH2B4 Rangkaian Listrik</h3>
    </div>
    <hr>
    <div class="col col-12 py-3">
        <?php $image = $course === null ? $this->config->item("ASSET_PATH") . 'media/img/course/course2.png' : base_url() . $course->banner_image ?>
        <img class="img-fluid" src="<?= $image ?>"
             alt="Course Banner">
    </div>
    <div class="col col-12 py-3">
        <div class="card rounded-0">
            <div class="card-body">
                <h5>Course Description</h5>
                <p><?= $course->description ?></p>
            </div>
        </div>
    </div>
    <div class="col col-12 py-3">
        <div class="card rounded-0">
            <div class="card-body">
                <h5>Course Certification</h5>
                <p><?= $course->certification ?></p>
            </div>
        </div>
    </div>
    <div class="col col-12 py-3">
        <div class="card rounded-0">
            <div class="card-body">
                <h5>Lecturer Profile</h5>
                <p><?= $course->lecturer_profile ?></p>
            </div>
        </div>
    </div>
    <div class="col col-12 py-3">
        <h5>Summary</h5>
        <div class="row py-2">
            <div class="col col-3 col-sm-12 col-md-3 col-lg-3 border-right">
                <div>
                    Course Name
                </div>
                <div>
                    <strong><?= $course->ocw_name ?></strong>
                </div>
            </div>
            <div class="col col-3 col-sm-12 col-md-3 col-lg-3 border-right">
                <div>
                    Lecturer
                </div>
                <div>
                    <strong><?= $course->lecturer_name ?></strong>
                </div>
            </div>
            <div class="col col-3 col-sm-12 col-md-3 col-lg-3 border-right">
                <div>
                    Course Start
                </div>
                <div>
                    <strong><?= DateTime::createFromFormat('Y-m-d', $course->start_date)->format('j F Y') ?></strong>
                </div>
            </div>
            <div class="col col-3 col-sm-12 col-md-3 col-lg-3">
                <div>
                    Course End
                </div>
                <div>
                    <strong><?= DateTime::createFromFormat('Y-m-d', $course->end_date)->format('j F Y') ?></strong>
                </div>
            </div>
        </div>
    </div>
    <div class="col col-12 content-fade"></div>
</div>

<!--  read more -->
<div class="col col-12 read-more">
    <a href="#" class="read-more__link">See More About This Course</a>
</div>
<!-- ./read more -->

<hr>

<!-- tabs -->
<div class="tabs">
    <!-- tab button -->
    <div class="tabs__item">
        <button class="tabs__link tabs__link--active" data-tab-target="#rating">Rating & Feedback (0)</button>
        <button class="tabs__link" data-tab-target="#comment">Comment (0)</button>
    </div>

    <!-- tab rating -->
    <div class="tabs__content tabs__content--active" id="rating">
        <!-- rating -->
        <div class="rating">
            <div class="rating__number">
                <div class="center-content">
                    <div class="rating__number__title">0.0</div>
                    <div class="rating__number__description">Average Rating</div>
                </div>
            </div>
            <div class="rating__star">
                <div class="row mx-0">
                    <div class="star">
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                    </div>
                    <div class="right-side" id="star-5">(0)</div>
                </div>
                <div class="row mx-0">
                    <div class="star">
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star"></span>
                    </div>
                    <div class="right-side" id="star-4">(0)</div>
                </div>
                <div class="row mx-0">
                    <div class="star">
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star"></span>
                        <span class="fas fa-star"></span>
                    </div>
                    <div class="right-side" id="star-3">(0)</div>
                </div>
                <div class="row mx-0">
                    <div class="star">
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star"></span>
                        <span class="fas fa-star"></span>
                        <span class="fas fa-star"></span>
                    </div>
                    <div class="right-side" id="star-2">(0)</div>
                </div>
                <div class="row mx-0">
                    <div class="star">
                        <span class="fas fa-star active"></span>
                        <span class="fas fa-star"></span>
                        <span class="fas fa-star"></span>
                        <span class="fas fa-star"></span>
                        <span class="fas fa-star"></span>
                    </div>
                    <div class="right-side" id="star-1">(0)</div>
                </div>
            </div>
            <div class="feedback__submit">
                <div class="center-content">
                    <span class="fas fa-star active"></span>
                    <button class="btn btn-danger shadow"><?= $session['id'] ? 'Give Rating & Feedback' : 'Login To Give Rating' ?></button>
                </div>
            </div>
            <div class="submit__overlay">
                <div class="card middle">
                    <div class="card-body">
                        <button type="button" class="close"><span class="fas fa-times"></span></button>
                        <div class="rating__submit">
                            <div class="rating__number__title">0.0</div>
                            <div class="star my-3">
                                <span class="fas fa-star" data-star="1"></span>
                                <span class="fas fa-star" data-star="2"></span>
                                <span class="fas fa-star" data-star="3"></span>
                                <span class="fas fa-star" data-star="4"></span>
                                <span class="fas fa-star" data-star="5"></span>
                            </div>
                            <div class="row mx-0 my-4">
                                <div class="col col-3 col-md-2 px-0">
                                    <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Profile Image"
                                         class="profile">
                                </div>
                                <div class="col col-9 col-md-10 input">
                                    <input type="text" class="feedback__input" placeholder="Add Your Feedback">
                                </div>
                            </div>
                            <div class="row mx-0 align-items-center justify-content-around">
                                <button class="btn btn-secondary feedback__cancel">Cancel</button>
                                <button class="btn btn-danger feedback__button">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./ rating -->
    </div>
    <!-- ./tab rating -->

    <div class="tabs__content" id="comment">
        <div class="row mx-0 my-3 comment-input">
            <?php if ($session['id']): ?>
                <div class="col col-2 col-md-1 px-0">
                    <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Profile Image" class="profile">
                </div>
                <div class="col col-10 col-md-11 input">
                    <input type="text" class="comment__input" placeholder="Add Your Comment" data-parent-id>
                </div>
            <?php else: ?>
                <a href="<?= base_url('/UsersAuth') ?>" class="btn btn-danger shadow m-auto">Login To Give
                    Comment</a>
            <?php endif; ?>
        </div>
        <ul class="nested-comment">
        </ul>
        <script src="<?php echo $this->config->item("ASSET_PATH"); ?>fe/libs/jquery/jquery-3.4.1.min.js"></script>
        <script>
            // load Tab Content
            userId = "<?= $session['id']; ?>"
            courseId = "<?= $course->id; ?>"
            defaultId = 0
            loadFeedbackTab()
            loadCommentTab()

            if ($(".course-content").outerHeight() < 700) {
                $(".read-more__link").parent().hide()
                $(".content-fade").hide();
            }

            // read more function
            var el = $(".read-more__link");
            el.click(() => {
                event.preventDefault();

                //hide button see more
                el.parent().hide();

                // hide content fade
                $(".content-fade").hide();


                // get total height of course content
                var totalHeight = 0;
                $(".course-content > *:not(.content-fade)").each(() => {
                    totalHeight += $(this).outerHeight();
                });

                // animating the height of course content
                $(".course-content").animate({
                    maxHeight: totalHeight + "px"
                }, 2000);
            });

            // tabs function
            var myTabs = $(".tabs__link");
            var myContent = $(".tabs__content");
            myTabs.each(function () {
                let self = this;
                $(self).click(() => {
                    let self = this;
                    const target = $($(self).data("tabTarget"));

                    myContent.each(function () {
                        $(this).removeClass('tabs__content--active');
                    });

                    myTabs.each(function () {
                        $(this).removeClass('tabs__link--active');
                    });

                    $(self).addClass('tabs__link--active');
                    target.addClass('tabs__content--active');
                });
            });

            // Feedback Modal
            modal = $(".submit__overlay").get(0);
            $(window).click(e => e.target === modal ? closeModal() : '');
            $("button.close").click(closeModal);
            $(".feedback__cancel").click(closeModal);
            $(".center-content .btn").click(userId ? showModal : redirectLogin);

            $(".feedback__button").click(sendFeedback);

            var defaultRating = 0
            var defaultFeedback = ""

            function showModal() {
                // return modal to default set
                n = 1;
                stars.each(function () {
                    n++ > defaultRating ? $(this).removeClass('active') : $(this).addClass('active')
                });
                $(".rating__submit .rating__number__title").text(defaultRating + ".0");
                $(".feedback__input").val(defaultFeedback);
                $(".submit__overlay").fadeIn();
            }

            function redirectLogin() {
                alert('Login To Give Rating')
                window.location.href = '<?= base_url('/UsersAuth') ?>'
            }

            function closeModal() {
                $(".submit__overlay").fadeOut();
            }

            // load stars in modal
            var stars = $("[data-star]");
            var currentStar = 0;
            var rating = $(".rating__submit .rating__number__title");

            function loadStar(hover = 0) {
                stars.each(function () {
                    let x = $(this).data("star");
                    if (!hover) {
                        if (x <= currentStar) {
                            $(this).addClass("active");
                            rating.text(currentStar + ".0");
                            return;
                        }
                    } else {
                        if (x <= hover) {
                            rating.text(hover + ".0");
                            $(this).addClass("active");
                            return;
                        }
                    }
                    $(this).removeClass("active")
                });
            }

            stars.each(function () {
                $(this).hover(() => {
                    let x = $(this).data("star")
                    loadStar(x)
                }, () => {
                    loadStar()
                    rating.text(currentStar + '.0')
                });

                $(this).click(() => {
                    currentStar = $(this).data("star")
                    loadStar()
                });
            });
            // ./feedback modal

            // load feedback tab content
            function loadFeedbackTab() {
                $.ajax({
                    type: "get",
                    url: "<?php echo base_url("/course/load_feedback_tab"); ?>",
                    data: {
                        course_id: courseId,
                    },
                    dataType: "json",
                    success: data => {
                        if (data.status) {
                            $("[data-tab-target='#rating']").text(`Rating & Feedback (${data.feedbacks.length})`)
                            loadRating(data.overall, data.starsCount)
                            loadFeedbacks(data.feedbacks)
                        } else
                            alert(data.message)
                    },
                    error: err => {
                        alert('Cannot communicate with server please check your internet connection')
                    }
                })
            }

            function loadRating(overall, starsCount) {
                $(".rating__number .rating__number__title").text(overall)
                $.each(starsCount, function (key) {
                    $(`#star-${key}`).text(`(${this})`)
                })
            }

            function loadFeedbacks(feedbacks) {
                var arrEle = []
                $(".tabs__content .rating").nextAll().remove()
                $.each(feedbacks, function () {
                    if (this.user_id === userId && this.course_id === courseId) {
                        defaultRating = this.rating
                        defaultFeedback = this.feedback
                        currentStar = this.rating
                        defaultId = this.id
                    }

                    e = $('<div class="row mx-0 mt-4"></div>')
                    inner = `
                <div class="col col-2 col-md-1 px-0">
                    <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Profile Image" class="profile">
                </div>
                <div class="col col-10 col-md-11 d-flex flex-column">
                    <div class="font-weight-bold">${this.full_name}</div>
                    <div class="row mx-0">
                        <div class="star mini-star">`

                    for (x = 1; x <= 5; x++) {
                        active = x <= this.rating ? ' active' : ''
                        inner += `  <span class="fas fa-star${active}"></span>`
                    }

                    inner += `  <em class="tgl-rating">${formatDate(this.created_date)}</em>
                        </div>
                    </div>
                    <p class="feedback">
                        ${this.feedback}
                    </p>
                </div>
            `
                    e.html(inner)
                    arrEle.push(e)
                })
                $(".tabs__content .rating").after(arrEle)
            }

            // send new Feedback to Server
            function sendFeedback() {
                console.log('test')
                feedback = $(".feedback__input").val()
                if (userId) {
                    if (feedback && currentStar) {
                        $.ajax({
                            type: "post",
                            url: "<?php echo base_url("/Course/save_feedback"); ?>",
                            data: {
                                user_id: userId,
                                course_id: courseId,
                                rating: currentStar,
                                feedback: feedback,
                                feedback_id: defaultId
                            },
                            dataType: "json",
                            success: data => {
                                if (data.status) {
                                    alert(data.message)
                                    closeModal()
                                    loadFeedbackTab();
                                } else
                                    alert(data.message)
                            },
                            error: err => {
                                alert('Cannot communicate with server please check your internet connection')
                            }
                        })
                    } else {
                        alert('Please Give Your Rating & Feedback')
                    }
                } else {
                    window.location.href = '<?= base_url('/UsersAuth') ?>'
                }
            }

            // format date to human readable
            function formatDate(date) {
                const bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
                let newDate = new Date(date)
                let formattedDate = newDate.getDate() + ' ' + bulan[newDate.getMonth()] + ' ' + newDate.getFullYear()
                return formattedDate
            }

            // load Comment Tab Content
            function loadCommentTab() {
                $.ajax({
                    type: "get",
                    url: "<?php echo base_url("/Course/load_comment_tab"); ?>",
                    data: {
                        course_id: courseId,
                    },
                    dataType: "json",
                    success: data => {
                        if (data.status) {
                            $("[data-tab-target='#comment']").text(`Comments (${data.count})`)
                            loadComments(data.comments)
                        } else
                            alert(data.message)
                    },
                    error: err => {
                        alert('Cannot communicate with server please check your internet connection')
                    }
                })
            }

            function loadComments(comments) {
                $(".nested-comment").empty()
                $.each(comments, function () {
                    let e = $("<li></li>")

                    let inner = `<div class="comment mt-2">
                                    <div class="row mx-0">
                                        <div class="col col-2 col-md-1 px-0">
                                            <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Profile Image" class="profile">
                                        </div>
                                        <div class="col col-10 col-md-11 d-flex flex-column">
                                            <div class="font-weight-bold">${this.full_name}</div>
                                            <div class="row mx-0">
                                                <em class="tgl-rating">${formatDate(this.created_date)}</em>
                                            </div>
                                            <p class="feedback mb-0">
                                                ${this.comment}
                                            </p>
                                            <div class="row mx-0">
                                                <span class="comment__reply mr-2" data-id="${this.id}"><i class="fas fa-reply-all"></i><a href="javascript:(0)"> Reply</a></span>`
                    if (this.replies)
                        inner += `<span class="comment__view" data-id="${this.id}"><i class="fas fa-angle-down"></i><a href="javascript:(0)"> View All Reply</a></span>`
                    inner += `</div>
                                        </div>
                                    </div>
                                 </div>
                                 <ul class="reply"></ul>
                                `
                    e.html(inner)
                    $(".nested-comment").append(e)
                })
            }

            function loadReplies(replies) {
                childArr = []
                $.each(replies, function () {
                    let e = $("<li></li>")
                    let inner = `<div class="comment mt-2">
                                    <div class="row mx-0">
                                        <div class="col col-2 col-md-1 px-0">
                                            <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Profile Image" class="profile">
                                        </div>
                                        <div class="col col-10 col-md-11 d-flex flex-column">
                                            <div class="font-weight-bold">${this.full_name}</div>
                                            <div class="row mx-0">
                                                <em class="tgl-rating">${formatDate(this.created_date)}</em>
                                            </div>
                                            <p class="feedback mb-0">
                                                ${this.comment}
                                            </p>
                                        </div>
                                    </div>
                                 </div>
                                 <ul class="reply"></ul>
                                `
                    e.html(inner)
                    childArr.push(e)
                })
                return childArr;
            }

            $(document).delegate('.comment__input', 'keypress', function (e) {
                if (e.which === 13) {
                    e.preventDefault()
                    let comment = $(this).val()
                    let parentId = $(this).data('parent-id')
                    $.ajax({
                        type: "post",
                        url: "<?php echo base_url("/course/save_comment"); ?>",
                        data: {
                            user_id: userId,
                            course_id: courseId,
                            comment: comment,
                            parent_id: parentId
                        },
                        dataType: "json",
                        success: data => {
                            if (data.status) {
                                alert(data.message)
                                $(this).val(``)
                                loadCommentTab();
                            } else
                                alert(data.message)
                        },
                        error: err => {
                            alert('Cannot communicate with server please check your internet connection')
                        }
                    })
                }
            })

            //  reply
            $(".nested-comment").delegate('.comment__reply', 'click', function (e) {
                if (userId) {
                    e.preventDefault()
                    let id = $(this).data('id')
                    let list = $(this).closest("li").children(".reply")
                    let listInput = list.children(".comment-input")
                    if (!listInput.length) {
                        let e = $(document.createElement("div"))
                        e.addClass("row mx-0 my-3 comment-input")
                        e.html(`<div class="col col-2 col-md-1 px-0">
                                    <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Foto Profile" class="profile">
                                </div>
                                <div class="col col-10 col-md-11 input">
                                    <input type="text" class="comment__input" placeholder="Add Your Comment" data-parent-id="${id}">
                                </div>`)
                        e.prependTo(list)
                    } else
                        listInput.get(0).remove()
                }
            })

            // view all comment
            $(".nested-comment").delegate('.comment__view', 'click', function (e) {
                e.preventDefault();
                parentId = $(this).data('id')
                icon = $(this).children("i")

                if (icon.hasClass("fa-angle-down")) {
                    icon.removeClass("fa-angle-down");
                    icon.addClass("fa-angle-up");

                    $.ajax({
                        type: "get",
                        url: "<?php echo base_url("/Course/load_replies"); ?>",
                        data: {
                            parent_id: parentId,
                        },
                        dataType: "json",
                        success: data => {
                            if (data.status) {
                                replies = loadReplies(data.replies)
                                replyEle = $(this).parent().parent().parent().parent().next()
                                replyEle.append(replies)
                            } else
                                alert(data.message)
                        },
                        error: err => {
                            alert('Cannot communicate with server please check your internet connection')
                        }
                    })
                } else {
                    icon.addClass("fa-angle-down");
                    icon.removeClass("fa-angle-up");
                    replyEle = $(this).parent().parent().parent().parent().next()
                    replyEle.children('li').remove()
                }
            })
        </script>
    </div>
</div>