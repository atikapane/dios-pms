<?php $this->load->view('fe/templates/header'); ?>
<style>
    .timer {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .timer li {
        list-style-type: none;
        padding: 0 .5em;
        font-size: 1em;
    }

    .timer li span {
        display: block;
        font-size: 4em;
        line-height: 1;
    }
</style>
<div class="container">
    <h4 class="font-weight-bold my-4">Selesaikan Pembayaran</h4>
    <div class="card text-white bg-dark mb-4 text-center">
        <div class="card-body">
            <h5 class="card-title my-3">Sisa Waktu Pembayaran Anda</h5>
            <ul class="timer p-0 mb-5">
                <li><span id="hours">23</span>Jam</li>
                <li>:</li>
                <li><span id="minutes">54</span>Menit</li>
                <li>:</li>
                <li><span id="secods">22</span>Detik</li>
            </ul>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-body">
            <h5 class="card-title my-3 font-weight-bold">Transfer Pembayaran Ke Nomor Rekening</h5>
            <div class="row mx-0 align-items-center">
                <img src="https://www.freepnglogos.com/uploads/logo-bca-png/bank-central-asia-logo-bank-central-asia-bca-format-cdr-png-gudril-1.png" width="200" alt="Bank Central Asia" />
                <span style="font-size: 1.5em; margin-left: 8px;">505019284</span>
            </div>
            <p class="mb-5" style="font-size: 1.5em;">a.N <span>Pemilik Rekening</span></p>
            <a href="#" class="card-link text-danger">Salin no. rekening</a>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-body">
            <h5 class="card-title my-3 font-weight-bold">Jumlah Yang Harus Dibayar</h5>
            <span class="ml-1 mb-5 d-block" style="font-size: 1.5em;">Rp 99.000.000</span>
            <a href="#" class="card-link text-danger">Salin no. rekening</a>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-body">
            <h5 class="card-title my-3 font-weight-bold">Panduang Pembayaran</h5>
            <div class="accordion" id="accordionExample">
                <h5 class="mb-0">
                    <a href="#" class="text-decoration-none text-danger"" type=" button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        ATM BCA <i class="fas fa-angle-down"></i>
                    </a>
                </h5>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>

                <h5 class="mb-0">
                    <a href="#" class="text-decoration-none text-danger" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        m-BCA (BCA MOBILE) <i class="fas fa-angle-down"></i>
                    </a>
                </h5>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>

                <h5 class="mb-0">
                    <a href="#" class="text-decoration-none text-danger" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Internet Banking BCA <i class="fas fa-angle-down"></i>
                    </a>
                </h5>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>

                <h5 class="mb-0">
                    <a href="#" class="text-decoration-none text-danger" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Kantor Bank BCA <i class="fas fa-angle-down"></i>
                    </a>
                </h5>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mx-0 mb-4 justify-content-around">
        <button class="btn btn-danger" style="width: 45%">Cek Status Pembayaran</button>
        <button class="btn btn-outline-danger bg-white" style="width: 45%">Kembali Belanja</button>
    </div>
</div>

<?php $this->load->view('fe/templates/footer'); ?>