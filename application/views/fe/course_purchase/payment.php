<?php $this->load->view('fe/templates/header'); ?>
<style>
    .payment-total {
        font-weight: bold;
        font-size: 1.5em;
        line-height: 1;
        margin: 0;
    }

    .payment-method {
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #bdbdbd;
    }

    .payment-method:last-child {
        border: none;
    }
</style>

<div class="container">
    <h4 class="font-weight-bold my-4">Selesaikan Pembayaran</h4>
    <div class="card mb-4">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">Rheo Habibie Ramadhan</h5>
            <hr>
            <div class="d-flex justify-content-between">
                <h5 class="align-self-center">Total Tagihan</h5>
                <div class="text-right">
                    <p class="payment-total">99.000.000</p>
                    <a href="#" class="text-decoration-none text-danger">Detail Tagihan</a>
                </div>
            </div>
            <h5 class="card-title font-weight-bold mt-5">Pilih Metode Pembayaran</h5>
            <div class="payment-method">
                <img src="https://www.freepnglogos.com/uploads/logo-bca-png/bank-central-asia-logo-bank-central-asia-bca-format-cdr-png-gudril-1.png" width="200" alt="Bank Central Asia" />
                <a href="#" class="btn btn-danger w-25 h-25 shadow">Bayar</a>
            </div>
            <div class="payment-method">
                <img src="https://1.bp.blogspot.com/-pBKWWv4H0YY/V15O0ZSZO9I/AAAAAAAAAJE/Xps9eUzp6q8hLNuvYTCtm9CaiyAdiaoCACKgB/s1600/Bank-Mandiri-Logo-Vector-Image.png" width="200" alt="Bank Mandiri" />
                <a href="#" class="btn btn-danger w-25 h-25 shadow">Bayar</a>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('fe/templates/footer'); ?>