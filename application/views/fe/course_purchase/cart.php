<?php $this->load->view('fe/templates/header'); ?>
<style>
    .checkbox-container {
        display: block;
        position: relative;
        user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        -moz-user-select: none;
        cursor: pointer;
        padding-left: 32px;
    }

    .checkbox-container .cart__checkbox {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    .checkbox-container .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        border: 2px solid gray;
        border-radius: 5px;
    }

    .checkbox-container .checkmark::after {
        content: '';
        position: absolute;
        display: none;
        bottom: 7px;
        left: 2px;
        border-left: 4px solid gray;
        border-bottom: 4px solid gray;
        height: 10px;
        width: 17px;
        border-bottom-left-radius: 2px;
        transform: rotate(-58deg);
    }

    .checkbox-container .cart__checkbox:checked+.checkmark::after {
        display: block;
    }

    .cart-item {
        border-bottom: 2px solid #CCCED0;
        height: 120px;
    }


    .cart-item .cart__checkbox+.checkmark {
        transform: translateY(-8.5px);
    }

    .course-image {
        width: 80px;
        height: 80px;
        border-radius: 5px;
        box-shadow: 0px 0px 8px rgba(0, 0, 0, .5);
    }

    .course-title,
    .course-price {
        margin: 0;
        font-weight: bold;
    }

    .course-title {
        color: red;
        font-size: 1em;
    }

    .course-price {
        font-size: 1.6em;
        line-height: 1;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col col-12 col-md-8 p-3">
            <div class="card rounded-0">
                <div class="card-body">
                    <label class="checkbox-container">
                        <h5 class="card-title">Pilih Semua Course (5)</h5>
                        <input type="checkbox" name="" id="" class="cart__checkbox">
                        <span class="checkmark"></span>
                    </label>

                    <div class="row mx-0 align-items-center cart-item">
                        <div class="px-0 flex-shrink-1">
                            <label class="checkbox-container align-self-center">
                                <input type="checkbox" name="" id="" class="cart__checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col col-10">
                            <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Course Image" class="course-image float-left">
                            <div class="px-3 py-2 float-left">
                                <p class="course-title">Judul Course</p>
                                <p class="course-price">250.000</p>
                            </div>
                        </div>
                        <div class="col col-1 align-self-end mb-3">
                            <i class="fas fa-trash-alt fa-2x"></i>
                        </div>
                    </div>
                    <div class="row mx-0 align-items-center cart-item">
                        <div class="px-0 flex-shrink-1">
                            <label class="checkbox-container align-self-center">
                                <input type="checkbox" name="" id="" class="cart__checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col col-10">
                            <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Course Image" class="course-image float-left">
                            <div class="px-3 py-2 float-left">
                                <p class="course-title">Judul Course</p>
                                <p class="course-price">250.000</p>
                            </div>
                        </div>
                        <div class="col col-1 align-self-end mb-3">
                            <i class="fas fa-trash-alt fa-2x"></i>
                        </div>
                    </div>
                    <div class="row mx-0 align-items-center cart-item">
                        <div class="px-0 flex-shrink-1">
                            <label class="checkbox-container align-self-center">
                                <input type="checkbox" name="" id="" class="cart__checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col col-10">
                            <img src="https://source.unsplash.com/1600x900/?nature,water" alt="Course Image" class="course-image float-left">
                            <div class="px-3 py-2 float-left">
                                <p class="course-title">Judul Course</p>
                                <p class="course-price">250.000</p>
                            </div>
                        </div>
                        <div class="col col-1 align-self-end mb-3">
                            <i class="fas fa-trash-alt fa-2x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-12 col-md-4 p-3">
            <div class="card rounded-0">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">Ringkasan</h5>
                    <hr>
                    <div class="d-flex justify-content-between">
                        Total Tagihan <strong class="font-weight-bold">99.000.000</strong>
                    </div>
                    <button class="btn btn-danger w-100 mt-5 shadow">Beli Course</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('fe/templates/footer'); ?>