<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");


class Menu extends BaseBEController {

	function __construct() 
	{
		parent::__construct();		
		$this->load->model('Menu_Model','m_menu');						
	}
	
	public function index()
	{		
		$data['dt']		= $this->m_menu->get_menu();
		$data['icons']	= $this->m_menu->get_menuicons();		
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;	
		$this->load->view('menu/index.php',$data);
	}

	public function list($id)
	{		
		$data['dt']		= $this->m_menu->get_submenu($id);
		$data['icons']	= $this->m_menu->get_menuicons();
		$this->load->view('menu/detail.php',$data);
	}

	public function save()
	{	
		$menuid 			= $this->input->post('menuid');
		$data['menu_name'] 	= $this->input->post('menu');
		$data['menu_link'] 	= $this->input->post('menulink');		

		$db=false;
		if($this->m_menu->get_exist_byid($menuid)>0){
			$data['menu_icon'] 		= $this->input->post('menuicon-edit');
			$db = $this->web_app_model->update_data('menu',$data,$menuid,'menu_id');
		}else{
			$data['menu_icon'] 		= $this->input->post('menuicon');
			$db = $this->web_app_model->insert_data('menu',$data);
		}	
		redirect('menu');
	}

	public function savesubmenu()
	{	
		$submenuid 					= $this->input->post('submenuid');
		$data['menuid'] 			= $this->input->post('menuid');
		$data['submenu_name'] 		= $this->input->post('submenu');
		$data['submenu_link'] 		= $this->input->post('submenulink');

		$db=false;
		if($this->m_menu->get_exist_submneu($data['menuid'],$submenuid)>0){
			$db = $this->web_app_model->update_data('submenu',$data,$submenuid,'submenu_id');
		}else{
			$db = $this->web_app_model->insert_data('submenu',$data);
		}	
		redirect('menu/list/'.$data['menuid']);
	}

	public function delete($id)
	{
		$data['menu_id'] = $id;
		$db=false;
		$db = $this->web_app_model->delete_data('menu',$data);
		if($db=true){
		$this->session->set_flashdata('message','success');			
		}else{
			$this->session->set_flashdata('message','failed');				
		}
		redirect('menu');
	}

	public function deletesub($idmenu,$id)
	{
		$data['submenu_id'] = $id;
		$db=false;
		$db = $this->web_app_model->delete_data('submenu',$data);
		if($db=true){
			$this->session->set_flashdata('message','success');			
		}else{
			$this->session->set_flashdata('message','failed');				
		}
		redirect('menu/list/'.$idmenu);
	}
	
}
