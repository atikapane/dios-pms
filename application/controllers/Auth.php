<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Auth_model','m_auth');
		$this->load->model('Sync_User_Model','m_sync');
		$this->load->helper('log');
		$this->load->helper('log_api_external');
	}

	private function check_session()
	{
		if(is_logged_in()){
			redirect('dashboard', 'auto');
		}
	}

	private function set_session($data)
	{
		$this->session->set_userdata('identity',$data["user_data"]);
		$this->session->set_userdata('token_data',array("token"=>$data["token"],"expired"=>$data["expired"]));
	}

	private function user_log($type)
	{
		if (!empty($type)){
			$this->load->library('general');
			$user_session = $this->session->userdata('identity');
			$insert = array(
				"user_id" => $user_session["user_id"],
				"log_type" => $type,
				"log_value" => ($type == "1" ? "Login" : "Logout"),
				"log_date" => $this->general->get_datetime()
			);
			$this->m_auth->create("user_logging",$insert);
		}
	}
	
	public function index()
	{	
		$this->check_session();
		$this->load->view('login');
	}

	public function signin()
	{
		$u = $this->input->post("username");
		$p = $this->input->post("password");

		$input = ['username'=> $u];

		$log = format_log_data($input);

		insert_log('USER_LOGIN', $log);
		//LOG HERE : USER_LOGIN, data : {data:{username:username}}
		if (!empty($u) && !empty($p)){
			$check = $this->m_auth->do_auth($u,$p);
			if (!empty($check)){
				$is_manual_insert = $check['is_manual_insert'];
				if ($is_manual_insert == 0){
					$this->load->library('general');
					$datetime = $this->general->get_datetime();
					$expired = $check["expired"];
					if (strtotime($datetime) >= strtotime($expired)){
						$result = $this->sync_token($u,$p,2);
						if(isset($result['status_message']) && $result['status_message'] == 1){
							$this->session->set_flashdata($result);
							redirect('auth', 'auto');
						}

						$check["token"] = $result["token"];
						$check["expired"] = $result["expired"];
					}
				}
				
				$this->set_session($check);
				$this->user_log(1);
				insert_log('USER_LOGIN_SUCCESS', $log);
				//LOG HERE : USER_LOGIN_SUCCESS, data : {data:{username:username}}
				redirect('dashboard', 'auto');
			}
		}
		else {
		    $log = format_log_data($input, "Username or Password Empty");
		    insert_log('USER_LOGIN_FAILED', $log);
			//LOG HERE : USER_LOGIN_FAILED, data : {message:"username or pssword empty",data:{username:username}}
			$data = array(
				"status_message" => 1,
				"value_message" => "Username and Password must be filled"
			);
			$this->session->set_flashdata($data);
			redirect('auth', 'auto');
		}
	}

	private function sync_token($u,$p,$t)
	{
	    insert_log('USER_TOKEN_SYNC');
		//LOG HERE : USER_TOKEN_SYNC, data : -
		$url_auth = $this->config->item('dev_token');
		if (!empty($url_auth)){
			//Get Token
			$curl = curl_init();
			$data = array("username" => $u, "password" => $p);
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url_auth,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_SSL_VERIFYHOST=> 0,
				CURLOPT_SSL_VERIFYPEER=> 0,
				CURLOPT_POST=> 1,
				CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
				CURLOPT_USERAGENT=> $_SERVER['HTTP_USER_AGENT'],
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => http_build_query($data),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			$log = $data;
			unset($log['password']);
			if (!$err){
				$result = json_decode($response,true);
				if (!isset($result['status'])){
					if (count($result) == 2){
						insert_log_external('API_EXTERNAL_DEV_TOKEN', $url_auth, json_encode($log), $response, true);
						unset($data["username"]);
						unset($data["password"]);
						$data["token"] = $result["token"];
						$data["expired"] = $this->general->get_datetime("plus",7200);
						if ($t=="2"){
							$this->m_auth->update("user",$data,"username ='".$u."'");
						}
                        insert_log('USER_TOKEN_SYNC_SUCCESS');
						//LOG HERE : USER_TOKEN_SYNC_SUCCESS, data : -
						return array(
							"status_message" => 0,
							"token" => $data["token"],
							"expired" => $data["expired"]
						);
					}
					else {
						insert_log_external('API_EXTERNAL_DEV_TOKEN', $url_auth, json_encode($log), $response);

					    $input = [
					        'username' => $u,
                            'url_auth' => $url_auth
                        ];

					    $log = format_log_data($input, "Sync User Data Failed. Please try again Later [Err.002]");
                        insert_log('USER_TOKEN_SYNC_FAILED', $log);
						//LOG HERE : USER_TOKEN_SYNC_FAILED, data : {message:"Sync User Data Failed. Please try again Later [Err.002]",data:{username:$username,url_auth:$url_auth}}
						return array(
							"status_message" => 1,
							"value_message" => "Sync User Data Failed. Please try again Later [Err.002]"
						);
					}
				}
				else {
					insert_log_external('API_EXTERNAL_DEV_TOKEN', $url_auth, json_encode($log), $response);

                    $input = [
                        'username' => $u,
                        'url_auth' => $url_auth
                    ];

                    $log = format_log_data($input, $err);
                    insert_log('USER_TOKEN_SYNC_FAILED', $log);
					//LOG HERE : USER_TOKEN_SYNC_FAILED, data : {message:"Username or Password is incorrect",data:{username:$username,url_auth:$url_auth}}
					return array(
						"status_message" => 1,
						"value_message" => $result['message']
					);
				}
			}
			else {
				insert_log_external('API_EXTERNAL_DEV_TOKEN', $url_auth, json_encode($log), $err);
			    $input = [
                    'username' => $u,
                    'url_auth' => $url_auth,
                    'err' => json_encode($err)
				];

			    $log = format_log_data($input, "Sync User Data Failed. Please try again Later [Err.001]");
			    insert_log('USER_TOKEN_SYNC_FAILED', $log);
				//LOG HERE : USER_TOKEN_SYNC_FAILED, data : {message:"Sync User Data Failed. Please try again Later [Err.001]",data:{username:$username,url_auth:$url_auth,err:json_encode($err)}}
				return array(
					"status_message" => 1,
					"value_message" => $err
				);
			}
		}
	}

	public function signout()
    {
    	if ($user = $this->session->userdata('identity')) {
    	    $log = format_log_data($user);
    	    insert_log('USER_LOGOUT', $log, $user['user_id']);
    		$this->user_log(2);
	        $this->session->sess_destroy();
	        $this->session->__construct();
	    }
	    redirect('auth', 'auto');
	}
}
