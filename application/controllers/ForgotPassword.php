<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ForgotPassword extends CI_Controller {

    public function __construct()
    {
    	parent::__construct();
    }

	public function index()
	{
        $data["title"] = "Forgot Password";
        $data["sub_menu"] = false;
        $data['active_page'] = "forgot_password";
        $this->load->view('fe/forgot_password/index', $data);
	}

    function send_email()
    {
        $response = init_response();

        $email = $this->input->post('email',true);
        $this->form_validation->set_rules('email', 'Email Address', 'required');

        if ($this->form_validation->run() == false) {
            $response->message = validation_errors();
            $response->status = false;
        }
        else {
            $forgotten = $this->ion_auth->forgotten_password($email);
            if ($forgotten) {
                $response->message = strip_tags($this->ion_auth->messages());
                $response->status = true;
                $response->data = $forgotten;
            }
            else {
                $response->message = strip_tags($this->ion_auth->errors());
                $response->status = false;
            }
        }

        json_response($response);
    }

    public function reset_password(){
        $token = $this->input->get('token');
        $user = $this->ion_auth->forgotten_password_check($token);
        if ($user)
        {
            $data["title"] = "Reset Password";
            $data["sub_menu"] = false;
            $data['active_page'] = "reset_password";
            $reset_password = array(
                'token' => $token
            );
            $this->session->set_userdata($reset_password);

            $this->load->view('fe/forgot_password_reset/index', $data);
        }else{
            redirect('','refresh');
        }
    }

    function process_reset_password(){
        $response = init_response();

        $new_password = $this->input->post("new_password");
        $token = $this->session->userdata('token');
        $user = $this->ion_auth->forgotten_password_check($token);
        if ($user)
        {
            $id = $user->id;
            $data = array(
                "oauth_provider"=>null,
                "oauth_uid"=>null,
                "password" => $new_password,
                "forgotten_password_selector"=>null,
                "forgotten_password_code"=>null,
                "forgotten_password_time"=>null
            );
            if($this->ion_auth->update_user($id, $data)){
                $response->message = strip_tags($this->ion_auth->messages());
                $response->status = true;
            }else{
                $response->message = strip_tags($this->ion_auth->errors());
                $response->status = false;
            }
        }else{
            $response->message = $this->ion_auth->errors();
            $response->status = false;
        }

        json_response($response);
    }

}

/* End of file Controllername.php */