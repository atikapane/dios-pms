<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Web_app_model');
    }

	public function index()
	{
        $data["sub_menu"] = false;
        $data["title"] = "List Category";
        $data['active_page'] = "home";
        $this->load->view('fe/list_category/index', $data);
	}

    public function load_category(){
        $response = init_response();
        $search = $this->input->post('search',true);
        $start = $this->input->post('start',true);
        $limit = $this->input->post('limit',true);

        $extra_where = array("deleted_at"=>null);
        $data_news = $this->Web_app_model->get_data_list("vw_ocw_category", $search, "name", $start, $limit, "updated_at", $extra_where);
        $count_data_news = $this->Web_app_model->get_data("vw_ocw_category",$extra_where);

        if($data_news){
            $response->status = true;
            $response->data = $data_news;
            $response->num_of_rows = count($count_data_news);
        }else{
            $response->status = false;
            $response->data = array();
            $response->num_of_rows = count($count_data_news);
        }
        json_response($response);
    }

}

/* End of file Controllername.php */