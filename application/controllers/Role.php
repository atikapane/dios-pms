<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	function __construct() 
	{
		parent::__construct();		
		$this->load->model('Role_Model','m_role');						
		$this->load->model('Group_Model','m_group');						
		$this->load->model('Users_Model','m_user');						
	}
	
	public function index()
	{		
		$this->load->view('role/index.php');
    }
    
    public function list($id)
	{	
		$data['users'] = $this->m_user->get_user(); 	
		$this->load->view('role/detail.php',$data);
	}

	public function save()
	{	
		$group 			= $this->input->post('groupid');
		$user 			= $this->input->post('userid');

		$db=false;
		for ($i=0; $i <count($user) ; $i++) { 
			$data['groupid'] 	= $group;
			$data['userid'] 	= $userid[$i];

			if($this->m_role->get_exist_byid($group,$$userid[$i])>0){
				$db = $this->web_app_model->update_data_multifield('roles',$data,$data);
			}else{
				$db = $this->web_app_model->insert_data('roles',$data);
			}	
			//print_r($data);
		}		
		
		if($db=true){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
		
	}

	public function delete()
	{
		$data['roles_id'] = $this->input->post('role_id');
		$db=false;
		$db = $this->web_app_model->delete_data('roles',$data);
		if($db=true){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	//json data
	public function get_group()
	{
		$data = $this->m_role->get_group_in_role();
		echo json_encode($data);
	}

	public function get_role_group($id)
	{
		$data = $this->m_role->get_role_group($id);
		echo json_encode($data);
	}
	
}
