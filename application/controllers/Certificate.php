<?php

use Endroid\QrCode\QrCode;

defined('BASEPATH') OR exit('No direct script access allowed');

class Certificate extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Web_app_model');
    }

    public function create_certificate()
    {
        $response = get_ajax_response();
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Course Id', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->status = false;
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $course_id = $this->input->get('id', true);
            $user_object = $this->ion_auth->user()->row();

            $student_object = $this->web_app_model->get_data_join('users', 'user_grade','', 'users.id = user_grade.user_id', '', ['users.id' => $user_object->id, 'user_grade.ocw_course_id' => $course_id], 'users.*, user_grade.id as grade_id, user_grade.grade, user_grade.certificate_number, user_grade.certificate_path');
            $course_object = $this->web_app_model->get_data_join('ocw_course_joined', 'certificate', '', 'ocw_course_joined.template_id = certificate.id', '', ['ocw_course_joined.id' => $course_id]);
    
            if(!empty($student_object) && !empty($course_object)){
                $always_generate = $this->config->item('always_generate_certificate');
                if($always_generate || empty($student_object[0]->certificate_path)){
                    $certificate_arr = [];
                    $grade_id = $student_object[0]->grade_id;
                    $certificate_number = $course_object[0]->certificate_prefix;
                    $certificate_number = !empty($certificate_number) ? $certificate_number : 'cert_';
                    $update_data['certificate_number'] = empty($student_object[0]->certificate_number) ? uniqid($certificate_number) : $student_object[0]->certificate_number;

                    $publish_path = $this->config->item('certificate_publish_path');
                    $upload_path = $publish_path.$course_object[0]->slug.$course_object[0]->id;
                    $upload_folder = FCPATH.$upload_path;
                    if(!file_exists($upload_folder)){
                        mkdir($upload_folder,0777,true);
                    }

                    $this->load->library('image_lib');
                    if($course_object[0]->is_multiple == 0){
                        if(file_exists(FCPATH.$course_object[0]->template_image)){
                            $update_data['certificate_path'] = $upload_path.'/'. $student_object[0]->username . '.jpeg';
                            $student_certificate = $upload_folder.'/'. $student_object[0]->username . '.jpeg';
                            copy(FCPATH.$course_object[0]->template_image, $student_certificate);
                        }else{
                            $response->status = false;
                            $response->error_code = 400;
                            $response->message = "There is no template image";
            
                            echo json_encode($response);
                            return;
                        }
                
                        if(isset($student_certificate)){
                            $template_objects = json_decode($course_object[0]->data);
                            foreach($template_objects as $object){
                                $config = [];
                                $config['image_library'] = 'GD2';
                                $config['source_image'] = $student_certificate;
                                $config['quality'] = '100%';
                    
                                $object = json_decode($object);
                                if(property_exists($object, 'objType') && $object->objType == 'fixedElement'){
                                    if($object->type == 'i-text'){
                                        if($object->id == '(Course Name)'){
                                            $config['wm_text'] = $course_object[0]->ocw_name;
                                        }else if($object->id == '(Lecturer Name)'){
                                            $config['wm_text'] =  empty($course_object[0]->lecturer_name) ? '-' : $course_object[0]->lecturer_name;
                                        }else if ($object->id == '(Learning Date)'){
                                            $config['wm_text'] = $course_object[0]->start_date . ' - ' . $course_object[0]->end_date;
                                        }else if($object->id == '(Student Name)'){
                                            $config['wm_text'] = $student_object[0]->full_name;
                                        }else if($object->id == '(Grade)'){
                                            $config['wm_text'] = $student_object[0]->grade;
                                        }else if($object->id == '(Certificate Number)'){
                                            $config['wm_text'] = $update_data['certificate_number'];
                                        }
    
                                        if($object->underline){
                                            $config['wm_underline'] = true;
                                        }
                
                                        $is_bold = $object->fontWeight == 'bold';
                                        $is_italic = $object->fontStyle == 'italic';
                                        if($object->fontFamily == 'Times New Roman'){
                                            $fontname = 'times';
                                            if($is_bold && $is_italic){
                                                $fontname = 'timesbi';
                                            }else if($is_bold){
                                                $fontname = 'timesbd';
                                            }else if($is_italic){
                                                $fontname = 'timesi';
                                            }
                                        }else if($object->fontFamily == 'Verdana'){
                                            $fontname = 'verdana';
                                            if($is_bold && $is_italic){
                                                $fontname = 'verdanaz';
                                            }else if($is_bold){
                                                $fontname = 'verdanab';
                                            }else if($is_italic){
                                                $fontname = 'verdanai';
                                            }
                                        }else if($object->fontFamily == 'Arial'){
                                            $fontname = 'arial';
                                            if($is_bold && $is_italic){
                                                $fontname = 'arialbi';
                                            }else if($is_bold){
                                                $fontname = 'arialbd';
                                            }else if($is_italic){
                                                $fontname = 'ariali';
                                            }
                                        }else if($object->fontFamily == 'Kunstler'){
                                            $fontname = 'kunstler';
                                            if($is_bold){
                                                $fontname = 'kunstlerbold';
                                            }
                                        }
                    
                                        $config['wm_type'] = 'text';
                                        $config['wm_font_path'] = FCPATH.$this->config->item('certificate_fonts_path'). $fontname . '.ttf';
                                        $config['wm_font_size'] = $object->fontSize * $object->scaleX;
                                        $config['wm_font_color'] = $object->fill;
    
                                        $temp = imagettfbbox($config['wm_font_size'], 0, $config['wm_font_path'], $config['wm_text']);
                                        $temp = abs($temp[2] - $temp[0]);
    
                                        $y = $object->top;
                                        $scale_response = $this->scaleWidth($config, $object, $temp, $y);
                                        $config = $scale_response[0];
                                        $temp = $scale_response[1];
                                        $y = $scale_response[2];
    
                                        if($object->originX == 'right'){
                                            $x = $object->left - $temp;
                                        }else if($object->originX == 'center'){
                                            $x = $object->left - ($temp / 2);
                                        }else{
                                            $x = $object->left;
                                        }
    
                                        $config['wm_hor_alignment'] = 'left';
                                        $config['wm_hor_offset'] = $x;
                                        $config['wm_vrt_alignment'] = 'top';
                                        $config['wm_vrt_offset'] = $y;
                                    }else if($object->type = 'image'){
                                        $config['wm_hor_alignment'] = 'left';
                                        $config['wm_hor_offset'] = $object->left;
                                        $config['wm_vrt_alignment'] = 'top';
                                        $config['wm_vrt_offset'] = $object->top;
                                        
                                        $md5 = md5($student_object[0]->grade_id);
                                        $update_data['md5_hash'] = $md5;
                                        $qrcode = new QrCode(base_url('certificate/view/') . $md5);
                                        $qrcode->setSize($object->width * $object->scaleX);
                
                                        // save qrcode to temp directory
                                        $tmpfname = tempnam(sys_get_temp_dir(), 'cert_');
                                        $base64_img = explode(',', $qrcode->writeDataUri());
                                        $image = base64_decode($base64_img[1]);
                                        file_put_contents($tmpfname, $image);
                
                                        $config['wm_type'] = 'overlay';
                                        $config['wm_overlay_path'] = $tmpfname;
                                        $config['wm_opacity'] = 100;
                                    }
                    
                                    $this->image_lib->initialize($config);
                                    if(!$this->image_lib->watermark()){
                                        echo $this->image_lib->display_errors();
                                    }
                                }
                            }
                            $this->web_app_model->update_data("user_grade",$update_data,$grade_id,"id");
                            array_push($certificate_arr, $update_data['certificate_path']);
            
                            $response->status = true;
                            $response->error_code = 200;
                            $response->path = $certificate_arr;
                            $response->message = "Successfully to generate the certificate";
                        }else{
                            $response->status = false;
                            $response->error_code = 400;
                            $response->message = "Failed to generate the certificate";
                        }
                    }else{
                        $package_object = $this->web_app_model->get_single_data('packet_ocw_course', ['course_id' => $course_id]);
                        $courses = $this->web_app_model->get_data_join('reference_multiple_course as reference', 'ocw_course_joined as course', 'certificate', 'reference.course_target_id = course.id', 'course.template_id = certificate.id', ['reference.package_ocw_course_id' => $package_object->id], 'course.*, certificate.*');

                        array_push($courses, $course_object[0]);

                        $count = count($courses);
                        foreach($courses as $course){
                            if(file_exists(FCPATH.$course->template_image)){
                                $update_data['certificate_path'] = $upload_path.'/'. $student_object[0]->username;
                                $student_certificate = $upload_folder.'/'. $student_object[0]->username. '_'.$count . '.jpeg';
                                copy(FCPATH.$course_object[0]->template_image, $student_certificate);
                            }else{
                                $count--;
                                continue;
                            }

                            if(isset($student_certificate)){
                                $template_objects = json_decode($course->data);
                                foreach($template_objects as $object){
                                    $config = [];
                                    $config['image_library'] = 'GD2';
                                    $config['source_image'] = $student_certificate;
                                    $config['quality'] = '100%';
                        
                                    $object = json_decode($object);
                                    if(property_exists($object, 'objType') && $object->objType == 'fixedElement'){
                                        if($object->type == 'i-text'){
                                            if($object->id == '(Course Name)'){
                                                $config['wm_text'] = $course->ocw_name;
                                            }else if($object->id == '(Lecturer Name)'){
                                                $config['wm_text'] =  empty($course->lecturer_name) ? '-' : $course->lecturer_name;
                                            }else if ($object->id == '(Learning Date)'){
                                                $config['wm_text'] = $course->start_date . ' - ' . $course->end_date;
                                            }else if($object->id == '(Student Name)'){
                                                $config['wm_text'] = $student_object[0]->full_name;
                                            }else if($object->id == '(Grade)'){
                                                $config['wm_text'] = $student_object[0]->grade;
                                            }else if($object->id == '(Certificate Number)'){
                                                $config['wm_text'] = $update_data['certificate_number'];
                                            }
        
                                            if($object->underline){
                                                $config['wm_underline'] = true;
                                            }
                    
                                            $is_bold = $object->fontWeight == 'bold';
                                            $is_italic = $object->fontStyle == 'italic';
                                            if($object->fontFamily == 'Times New Roman'){
                                                $fontname = 'times';
                                                if($is_bold && $is_italic){
                                                    $fontname = 'timesbi';
                                                }else if($is_bold){
                                                    $fontname = 'timesbd';
                                                }else if($is_italic){
                                                    $fontname = 'timesi';
                                                }
                                            }else if($object->fontFamily == 'Verdana'){
                                                $fontname = 'verdana';
                                                if($is_bold && $is_italic){
                                                    $fontname = 'verdanaz';
                                                }else if($is_bold){
                                                    $fontname = 'verdanab';
                                                }else if($is_italic){
                                                    $fontname = 'verdanai';
                                                }
                                            }else if($object->fontFamily == 'Arial'){
                                                $fontname = 'arial';
                                                if($is_bold && $is_italic){
                                                    $fontname = 'arialbi';
                                                }else if($is_bold){
                                                    $fontname = 'arialbd';
                                                }else if($is_italic){
                                                    $fontname = 'ariali';
                                                }
                                            }else if($object->fontFamily == 'Kunstler'){
                                                $fontname = 'kunstler';
                                                if($is_bold){
                                                    $fontname = 'kunstlerbold';
                                                }
                                            }
                        
                                            $config['wm_type'] = 'text';
                                            $config['wm_font_path'] = FCPATH.$this->config->item('certificate_fonts_path'). $fontname . '.ttf';
                                            $config['wm_font_size'] = $object->fontSize * $object->scaleX;
                                            $config['wm_font_color'] = $object->fill;
        
                                            $temp = imagettfbbox($config['wm_font_size'], 0, $config['wm_font_path'], $config['wm_text']);
                                            $temp = abs($temp[2] - $temp[0]);
        
                                            $y = $object->top;
                                            $scale_response = $this->scaleWidth($config, $object, $temp, $y);
                                            $config = $scale_response[0];
                                            $temp = $scale_response[1];
                                            $y = $scale_response[2];
        
                                            if($object->originX == 'right'){
                                                $x = $object->left - $temp;
                                            }else if($object->originX == 'center'){
                                                $x = $object->left - ($temp / 2);
                                            }else{
                                                $x = $object->left;
                                            }
        
                                            $config['wm_hor_alignment'] = 'left';
                                            $config['wm_hor_offset'] = $x;
                                            $config['wm_vrt_alignment'] = 'top';
                                            $config['wm_vrt_offset'] = $y;
                                        }else if($object->type = 'image'){
                                            $config['wm_hor_alignment'] = 'left';
                                            $config['wm_hor_offset'] = $object->left;
                                            $config['wm_vrt_alignment'] = 'top';
                                            $config['wm_vrt_offset'] = $object->top;
                                            
                                            $md5 = md5($student_object[0]->grade_id);
                                            $update_data['md5_hash'] = $md5;
                                            $qrcode = new QrCode(base_url('certificate/view/') . $md5);
                                            $qrcode->setSize($object->width * $object->scaleX);
                    
                                            // save qrcode to temp directory
                                            $tmpfname = tempnam(sys_get_temp_dir(), 'cert_');
                                            $base64_img = explode(',', $qrcode->writeDataUri());
                                            $image = base64_decode($base64_img[1]);
                                            file_put_contents($tmpfname, $image);
                    
                                            $config['wm_type'] = 'overlay';
                                            $config['wm_overlay_path'] = $tmpfname;
                                            $config['wm_opacity'] = 100;
                                        }
                        
                                        $this->image_lib->initialize($config);
                                        if(!$this->image_lib->watermark()){
                                            echo $this->image_lib->display_errors();
                                        }
                                    }
                                }
                                array_push($certificate_arr, $update_data['certificate_path'].'_'.$count--.'.jpeg');
                            }else{
                                $count--;
                                continue;
                            }
                        }

                        $this->web_app_model->update_data("user_grade",$update_data,$grade_id,"id");
                
                        $response->status = true;
                        $response->error_code = 200;
                        $response->path = $certificate_arr;
                        $response->message = "Successfully to generate the certificate";
                    }
                }else{
                    if($course_object[0]->is_multiple == 0){
                        $response->path = [$student_object[0]->certificate_path];
                    }else{
                        $package_object = $this->web_app_model->get_single_data('packet_ocw_course', ['course_id' => $course_id]);
                        $courses = $this->web_app_model->get_data_join('reference_multiple_course as reference', 'ocw_course_joined as course', 'certificate', 'reference.course_target_id = course.id', 'course.template_id = certificate.id', ['reference.package_ocw_course_id' => $package_object->id], 'course.*, certificate.*');

                        array_push($courses, $course_object[0]);
                        $response->path = [];

                        for($x = count($courses); $x >= 1; $x--){
                            if(file_exists(FCPATH.$student_object[0]->certificate_path.'_'.$x.'.jpeg')){
                                array_push($response->path, $student_object[0]->certificate_path.'_'.$x.'.jpeg');
                            }
                        }
                    }

                    $response->status = true;
                    $response->error_code = 200;
                    $response->message = "Successfully to generate the certificate";
                }
            }else{
                $response->status = false;
                $response->error_code = 400;
                $response->message = "Something wrong with the information that sended";
            }
        }

        echo json_encode($response);
    }

    private function string_to_underline($letter){
        return $letter . '&#x0332;';
    }

    public function view(){
        $data["sub_menu"] = false;
        $data["title"] = "Certificate";
        $data['active_page'] = "certificate";

        $md5 = $this->uri->segment(3);
        if(!empty($md5)){
            $course_object = $this->web_app_model->get_data_join('user_grade as grade', 'users', 'ocw_course_joined as course', 'grade.user_id = users.id', 'grade.ocw_course_id = course.id', ['grade.md5_hash' => $md5], 'grade.created_date as issued_date, course.id as course_id, grade.*, users.*, course.*');
            if($course_object[0]->is_multiple == 1){
                $package_object = $this->web_app_model->get_single_data('packet_ocw_course', ['course_id' => $course_object[0]->course_id]);
                $courses = $this->web_app_model->get_data_join('reference_multiple_course as reference', 'ocw_course_joined as course', '', 'reference.course_target_id = course.id', '', ['reference.package_ocw_course_id' => $package_object->id], 'course.*');
                $course_name = [];
                foreach($courses as $course){
                    array_push($course_name, $course->ocw_name);
                }
                $course_object[0]->courses = $course_name;
            }
            $data['certificate'] = $course_object[0];
            $this->load->view('fe_v2/certificate/index',$data);
        }else{
            redirect('/');
        }
    }

    private function scaleWidth($config, $object, $temp, $y){
        $origin_width = $object->width * $object->scaleX;
        if($origin_width < $temp){
            $scale = $origin_width / $temp;
            $config['wm_font_size'] = floor($config['wm_font_size'] * $scale);

            $temp = imagettfbbox($config['wm_font_size'], 0, $config['wm_font_path'], $config['wm_text']);
            $new_height = abs($temp[7] - $temp[1]);
            $temp = abs($temp[2] - $temp[0]);

            $y = $object->top + (($object->height * $object->scaleY) / 2) - ($new_height / 2);

            return $this->scaleWidth($config, $object, $temp, $y);
        }else{
            return [$config,$temp,$y];
        }
    }
}

/* End of file Controllername.php */