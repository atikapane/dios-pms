<?php
/**
 * Created by PhpStorm.
 * User: djakapermana
 * Date: 13/11/19
 * Time: 21.13
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->model('web_app_model');
    }

	public function index()
	{
        $data["sub_menu"] = false;
        $data["title"] = "Download";
        $data['active_page'] = "download";
        $this->load->view('fe_v2/download/index', $data);

	}

    public function get()
    {
        $query = $this->input->get("q");
        $limit = $this->input->get("l");
        $offset = $this->input->get("o");

        $data = $this->web_app_model->get_data_list("landing_download", $query, "title", $offset, $limit, "id", array("file_status"=>1));
        foreach ($data as $single_data) {
            $single_data->file_path = base_url().$single_data->file_path;
        }
        echo json_encode($data);
    }

}

/* End of file Controllername.php */