<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_copy_exam extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
        if (!$this->input->is_cli_request())
        {
            show_error("You don't have permission for this action");
        }
		$this->load->helper('check_and_copy_exam');
    }

    function execute(){
        // copy section
        $task_objects = $this->web_app_model->get_data('lms_exam_schedule_task', ['status' => 1]);
        foreach($task_objects as $task){
            $this->web_app_model->update_data("lms_exam_schedule_task", ['status' => 2], $task->id, "id");
            $this->web_app_model->update_data("lms_exam_schedule", ['status' => 2], $task->lms_exam_schedule_id, "id");

            $success = check_section($task->paralel_course_id,$task->exam_type);
            if(isset($success->status)){
                if($success->status){
                    $this->web_app_model->update_data("lms_exam_schedule_task", ['status' => 3], $task->id, "id");
                }else{
                    $success = copy_exam($task->master_course_id,$task->exam_type,"[{$task->paralel_course_id}]");
                    if(isset($success->status)){
                        if($success->status && !empty($success->data->successful_idnumber_list)){
                            $this->web_app_model->update_data("lms_exam_schedule_task", ['status' => 3], $task->id, "id");
                        }else{
                            $this->web_app_model->update_data("lms_exam_schedule_task", ['status' => 4], $task->id, "id");
                        }
                    }else{
                        $this->web_app_model->update_data("lms_exam_schedule_task", ['status' => 4], $task->id, "id");
                    }
                }
            }else{
                $this->web_app_model->update_data("lms_exam_schedule_task", ['status' => 4], $task->id, "id");
            }
        }

        // update exam status
        $exam_objects = $this->web_app_model->get_data('lms_exam_schedule', ['status' => 2]);
        foreach($exam_objects as $exam){
            $task_objects = $this->web_app_model->get_data('lms_exam_schedule_task', ['status >' => 2]);

            $success = true;
            foreach($task_objects as $task){
                $success = $success && ($task->status == 3);
            }
            if($success){
                $this->web_app_model->update_data("lms_exam_schedule", ['status' => 3], $exam->id, "id");
            }else{
                $this->web_app_model->update_data("lms_exam_schedule", ['status' => 4], $exam->id, "id");
            }
        }
    }
}