<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Task_billing extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Transaction_Model');
		$this->load->helper('log_api');
		$this->load->model('Web_App_Model');
        $this->load->library('Push_notif');
	}

	public function invalidate_expired()
	{
		$expired_tx = $this->Transaction_Model->invalidate_expired();

		//sent notification to user based on expired tx
		foreach ($expired_tx as $tx){
			$invoice_number = $tx->invoice_number;
			$user_id = $tx->user_id;
			$title = "Transaction Expired";
			$body = "Your transaction with invoice number $invoice_number has been canceled";
			$this->push_notif->send_notification($user_id, $title, $body);
		}
	}
}
