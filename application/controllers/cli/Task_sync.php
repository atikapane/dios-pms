<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_sync extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
        if (!$this->input->is_cli_request())
        {
            show_error("You don't have permission for this action");
        }
		$this->load->library('general');
		$this->load->library('sync');
		$this->load->model("Task_Model");
	}

	public function course_backup()
	{
		$date = $this->general->get_date();
		$filter_range = $this->config->item('backup_date_diff');
		$filter_date = date('Y-m-d',strtotime($date . "-" .$filter_range." days"));
		$datetime = $this->general->get_datetime();
		$limit = $this->config->item('backup_limit');
		$subjects = $this->web_app_model->get_data("sync_course_backup_history",array("substr(date,1,10)>="=>$filter_date,"status"=>0),"","id asc",$limit);
		if (!empty($subjects)){
			$backup_data = array(
	            'url'=>$this->config->item('cds_api'),
	            'wstoken'=>$this->config->item('cds_token'),
	            'type'=>'cds'
	        );
            foreach($subjects as $row){
            	if ($this->web_app_model->update_data_where("sync_course_backup_history",array("status"=>2),"id = ".$row->id." and status = 0")){
            		$backup_data['subject_id'] = $row->subject_id;
            		$do_backup = $this->sync->ws_course_backup($backup_data);
            		if (!empty($do_backup)){
            			$status = ($do_backup["status"] ? 1 : 3);
	                }
	                else {
	                	$status = 3;
	                }
	                $this->web_app_model->update_data("sync_course_backup_history",array("status"=>$status,"log"=>json_encode($do_backup)),$row->id,"id");
	                if ($status == 1){
	                	$this->web_app_model->update_data("sync_course",array("last_backup"=>$datetime),$row->subject_id,"subject_id");
	                }
            	}

            }
		}
		else {
			$this->web_app_model->update_data_where("sync_course_backup_history",array("status"=>3,"log"=>"date expired"),"status = 0 and date < '".$filter_date."'");
		}
	}

	public function course_lms_sync()
	{
		$date = $this->general->get_date();
		$filter_range = $this->config->item('sync_date_diff');
		$filter_date = date('Y-m-d',strtotime($date . "-" .$filter_range." days"));
		$datetime = $this->general->get_datetime();
		$limit = $this->config->item('sync_limit');
		$courses = $this->Task_Model->get_sync_course_queue($filter_date,$limit);
		if (!empty($courses)){
			$restore_data = array(
	            'url'=>$this->config->item('lms_api'),
	            'wstoken'=>$this->config->item('lms_token'),
	        );
            foreach($courses as $row){
            	if ($this->web_app_model->update_data_where("sync_lms_course_teaching_history",array("status"=>2),"id = ".$row->id." and status = 0")){
            		$cds_shortname = $row->subject_code;
            		$curriculum_year = $row->curriculum_year;
            		if ($curriculum_year == 2020){
            			$cds_shortname .= "-".$row->subject_id;
            		}
            		$restore_data['course_id'] = $row->course_id;
            		$restore_data['cds_shortname'] = $cds_shortname;
            		$do_restore = $this->sync->ws_course_restore_lms($restore_data);
            		if (!empty($do_restore)){
            			$status = ($do_restore["status"] ? 1 : 3);
            			$message = $do_restore["message"];
	                }
	                else {
	                	$status = 3;
	                }
	                $this->web_app_model->update_data("sync_lms_course_teaching_history",array("status"=>$status,"log"=>json_encode($do_restore)),$row->id,"id");
	                if ($status == 1){
	                	$this->web_app_model->update_data("sync_lms_course",array("last_sync"=>$datetime, ),$row->course_id,"course_id");	
	                }
            	}

            }
		}
		else {
			$this->web_app_model->update_data_where("sync_lms_course_teaching_history",array("status"=>3,"log"=>"date expired"),"status = 0 and date < '".$filter_date."'");
		}
	}
}