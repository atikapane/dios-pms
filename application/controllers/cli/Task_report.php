<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_report extends CI_Controller {
	protected $semester_active = "";
	
	function __construct() 
	{
		parent::__construct();
        // if (!$this->input->is_cli_request())
        // {
        //     show_error("You don't have permission for this action");
        // }
		$this->load->library('general');
		$this->load->model("Task_Model");
		$this->get_semester_active();
		$this->load->helper('log_api_external');
	}

	protected function get_semester_active(){
		$data = $this->Task_Model->get_single("config_semester","status_active = 1");
		if (!empty($data)){
			$this->semester_active = $data["semester"];
		}
	}

	public function course_development()
	{
		$date = $this->general->get_date();
		$subjects = $this->Task_Model->get_data_course_progress_statistic($date);
		if (!empty($subjects)){
			$api_app_id = $this->config->item('api_app_id');
			$api_app_secret = $this->config->item('api_app_secret');
			$url = $this->config->item('cds_url_course_development');
			$stat_config = $this->config->item('stat_config');
			if (!empty($api_app_id) && !empty($api_app_secret) && !empty($url) && !empty($stat_config)){
				$curl_var_limit = $this->config->item('curl_var_limit');
				if (count($subjects) > $curl_var_limit){
					$chunk = array_chunk($subjects, $curl_var_limit);
				}
				else {
					$chunk = $subjects;
				}
				foreach($chunk as $subjects){
					$curl = curl_init();
		            curl_setopt_array($curl, array(
		                CURLOPT_URL => $url,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_MAXREDIRS => 10,
		                CURLOPT_TIMEOUT => 600,
		                CURLOPT_SSL_VERIFYHOST=> 0,
		                CURLOPT_SSL_VERIFYPEER=> 0,
		                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
		                CURLOPT_CUSTOMREQUEST => "POST",
		                CURLOPT_POSTFIELDS => http_build_query(array("subjects"=>$subjects,"stat_config"=>$stat_config)),
		                CURLOPT_HTTPHEADER => array(
							"api_app_id: ".$api_app_id,
							"api_app_secret: ".$api_app_secret
						),
		            ));
		            $response = curl_exec($curl);
		            $err = curl_error($curl);
		            
					curl_close($curl);
					
					insert_log_external('API_EXTERNAL_CDS_COURSE_DEVELOPMENT', $url, json_encode(array("subjects"=>$subjects,"stat_config"=>$stat_config)), $response);

		            if (!$err){
		                $result = json_decode($response,true);
		                $status = $result["status"];
		                if ($status){
		                	$data = $result["data"];
		                	if (!empty($data)){
		                		foreach($data as $row){
		                			unset($row["id"]);
		                			unset($row["subject_code"]);
		                			unset($row["subject_name"]);
		                			$this->Task_Model->upsert("course_development_progress_report",$row,array("subject_id"=>$row["subject_id"]));
		                			$row["statistic_date"] = $date;
		                			$this->Task_Model->upsert("course_development_progress_statistic",$row,array("subject_id"=>$row["subject_id"],"statistic_date"=>$date));
		                		}
		                	}
		                }
		            }
				}
			}
		}
	}

	public function course_change()
	{
		$api_app_id = $this->config->item('api_app_id');
		$api_app_secret = $this->config->item('api_app_secret');
		$url = $this->config->item('lms_url_course_change');

		//get synced course
		if (!empty($this->semester_active)){
			$synced_course = $this->web_app_model->get_data_and_where("sync_lms_course",array("semester"=>$this->semester_active,"last_sync !="=> NULL));
			if (!empty($api_app_id) && !empty($api_app_secret) && !empty($url)){
				$curl_var_limit = $this->config->item('curl_var_limit');
				if (count($synced_course) > $curl_var_limit){
					$chunk = array_chunk($synced_course, $curl_var_limit);
				}
				else {
					$chunk = $synced_course;
				}
				foreach($chunk as $synced_course){
					$synced_course_ids = array();
					foreach ($synced_course as $single_course) {
						$synced_course_ids[] = $single_course->course_id;
					}
					$url .= "?course_id=".implode(",", $synced_course_ids);
					$curl = curl_init();
		            curl_setopt_array($curl, array(
		                CURLOPT_URL => $url,
		                CURLOPT_RETURNTRANSFER => true,
		                CURLOPT_MAXREDIRS => 10,
		                CURLOPT_TIMEOUT => 300,
		                CURLOPT_SSL_VERIFYHOST=> 0,
		                CURLOPT_SSL_VERIFYPEER=> 0,
		                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
		                CURLOPT_CUSTOMREQUEST => "GET",
		                CURLOPT_HTTPHEADER => array(
							"api_app_id: ".$api_app_id,
							"api_app_secret: ".$api_app_secret
						),
		            ));
		            $response = curl_exec($curl);
		            $err = curl_error($curl);
		            
					curl_close($curl);
					
		            if (!$err){
		                $result = json_decode($response,true);
		                if(!empty($result)){
			  				$status = $result["status"];
			                if ($status){
								insert_log_external('API_EXTERNAL_LMS_COURSE_CHANGE', $url, json_encode(['course_id' => implode(",", $synced_course_ids)]), $response, true);

			                	$data = $result["data"];
		            			$this->web_app_model->delete_data("course_change_log"," 1=1 ");
		                		foreach($data as $row){

		                			$single_data = array();
		                			$single_data["id"] = $row["id"];
		                			$single_data["name"] = $row["fullname"];
		                			$single_data["course_id"] = $row["idnumber"];

		                			$changes = $row["changes"];
		                			$single_data["quiz"] = $changes["quiz"];
		                			$single_data["resource"] = $changes["resource"];
		                			$single_data["hvp"] = $changes["hvp"];
		                			$single_data["label"] = $changes["label"];
		                			$single_data["page"] = $changes["page"];
		                			$single_data["url"] = $changes["url"];
		                			$single_data["forum"] = $changes["forum"];
		                			$single_data["assignment"] = $changes["assignment"];

		                			$this->web_app_model->insert_data("course_change_log",$single_data);
		                		}
			                }else{
								insert_log_external('API_EXTERNAL_LMS_COURSE_CHANGE', $url, json_encode(['course_id' => implode(",", $synced_course_ids)]), $response);
							}
		                }else{
							insert_log_external('API_EXTERNAL_LMS_COURSE_CHANGE', $url, json_encode(['course_id' => implode(",", $synced_course_ids)]), $response);
						}
		            }else{
						insert_log_external('API_EXTERNAL_LMS_COURSE_CHANGE', $url, json_encode(['course_id' => implode(",", $synced_course_ids)]), $err);
					}
				}
			}
		}
	}

	public function course_attendance()
	{
		$statistic_date = "";
		$data_sync = $this->Task_Model->get_single("course_attendance_sync","","date DESC");
		if (!empty($data_sync)){
			$statistic_date = $data_sync["date"];
		}
		if (!empty($this->semester_active)){
			$courses = $this->Task_Model->get_data_course_attendance($this->semester_active);
			if (!empty($courses)){
				$api_app_id = $this->config->item('api_app_id');
				$api_app_secret = $this->config->item('api_app_secret');
				$url = $this->config->item('lms_url_course_attendance');
				if (!empty($api_app_id) && !empty($api_app_secret) && !empty($url)){
					$curl_var_limit = $this->config->item('curl_var_limit');
					if (count($courses) > $curl_var_limit){
						$chunk = array_chunk($courses, $curl_var_limit);
					}
					else {
						$chunk = $courses;
					}
					foreach($chunk as $courses){
						$curl = curl_init();
			            curl_setopt_array($curl, array(
			                CURLOPT_URL => $url,
			                CURLOPT_RETURNTRANSFER => true,
			                CURLOPT_MAXREDIRS => 10,
			                CURLOPT_TIMEOUT => 600,
			                CURLOPT_SSL_VERIFYHOST=> 0,
			                CURLOPT_SSL_VERIFYPEER=> 0,
			                CURLOPT_HTTPAUTH=> CURLAUTH_BASIC,
			                CURLOPT_CUSTOMREQUEST => "POST",
			                CURLOPT_POSTFIELDS => http_build_query(array("courses"=>$courses,"statistic_date"=>$statistic_date)),
			                CURLOPT_HTTPHEADER => array(
								"api_app_id: ".$api_app_id,
								"api_app_secret: ".$api_app_secret
							),
			            ));
			            $response = curl_exec($curl);
			            $err = curl_error($curl);
			            
						curl_close($curl);

			            if (!$err){
			                $result = json_decode($response,true);
			                $status = $result["status"];
			                if ($status){
								insert_log_external('API_EXTERNAL_LMS_COURSE_ATTENDANCE', $url, json_encode(array("courses"=>$courses,"statistic_date"=>$statistic_date)), $response, true);

			                	$data = $result["data"];
			                	$num_course = 0;
			                	foreach($data as $course_id => $course_data){
			                		if (!empty($course_data)){
			                			foreach($course_data as $section_data){
			                				$upsert_report = array(
				                				"course_id" => $course_id,
				                				"section_id" =>$section_data["section_id"],
				                				"section_name" => $section_data["name"],
				                				"num_login" => 0,
				                				"num_forum" => 0,
				                				"num_grading" => 0,
				                				"attendance" => 0,
				                			);
			                				if (!empty($section_data["attendance"])){
					                			$attendance = $section_data["attendance"];
					                			$num_login = $num_forum = $num_grading = $attendance_status = 0;
					                			if (!empty($attendance)){
					                				$dates = array_keys($attendance);
													$upsert_report["date_start"] = reset($dates);
													$upsert_report["date_end"] = end($dates);
													foreach($attendance as $date => $stats){
														$login = isset($stats["login"]) ? $stats["login"] : 0;
														$forum = isset($stats["created"]) ? $stats["created"] : 0;
														$grading = isset($stats["graded"]) ? $stats["graded"] : 0; 

														$upsert_statistic = array(
															"course_id" => $course_id,
															"section_id" =>$section_data["section_id"],
															"date" => $date,
															"num_login" => $login,
															"num_forum" => $forum,
															"num_grading" => $grading,
														);

														$num_login += $login;
														$num_forum += $forum;
														$num_grading += $grading;
														$this->Task_Model->upsert("course_attendance_statistic",$upsert_statistic,array("course_id"=>$course_id,"section_id"=>$section_data["section_id"],"date"=>$date));
													}
													$upsert_report["num_login"] = $num_login;
													$upsert_report["num_forum"] = $num_forum;
													$upsert_report["num_grading"] = $num_grading;
													if ($num_login > 0 && $num_forum > 0 && $num_grading > 0){
														$upsert_report["attendance"] = 1;
													}
					                			}
			                				}
			                				$this->Task_Model->upsert("course_attendance_report",$upsert_report,array("course_id"=>$course_id,"section_id"=>$section_data["section_id"]));
			                			}
			                			$num_course++;
			                		}
			                	}
			                	$insert_data = array(
			                		"date" => $this->general->get_date(),
			                		"num_course" => $num_course
			                	);
			                	$this->Task_Model->create("course_attendance_sync",$insert_data);
			                }else{
								insert_log_external('API_EXTERNAL_LMS_COURSE_ATTENDANCE', $url, json_encode(array("courses"=>$courses,"statistic_date"=>$statistic_date)), $response);
							}
			            }else{
							insert_log_external('API_EXTERNAL_LMS_COURSE_ATTENDANCE', $url, json_encode(array("courses"=>$courses,"statistic_date"=>$statistic_date)), $err);
						}
					}
				}
			}
		}
	}
}