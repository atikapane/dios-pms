<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gateway extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Transaction_Model');
        $this->load->library('Push_notif');
        $this->load->helper('sync_mooc');
        $this->load->helper('sync_igracias');
        $this->load->helper('log_user_frontend');
    }

    
    public function bni()
    {
        $bni_conf['client_id'] = $this->config->item("bni_ecoll_client_id");
        $bni_conf['secret_key'] = $this->config->item("bni_ecoll_secret_key");
        $bni_conf['url'] = $this->config->item("bni_ecoll_url");
        $this->load->library('BNIecoll',$bni_conf);

        $log = format_log_data([], 'Check BNI Callback');
        insert_log('USER_FRONTEND_PAYMENT', $log);
        $bni_callback_response = $this->bniecoll->parse_callback();
        if($bni_callback_response->success){
        	if(is_array($bni_callback_response->data)){
        		$bni_callback_response->data = (object) $bni_callback_response->data;
        	}

        	if(!empty($bni_callback_response->data)){
        		$invoice_number = $bni_callback_response->data->trx_id;
                $payment_date = $bni_callback_response->data->datetime_payment;
                $log = format_log_data(['invoice_number' => $invoice_number, 'status' => 2, 'payment_date' => $payment_date], 'Set Transaction Paid');
                insert_log('USER_FRONTEND_PAYMENT', $log);
                $this->Transaction_Model->set_tx_status($invoice_number,Transaction_Model::STATUS_PAID,$payment_date);

                // enroll user after paid
                $user_id = $this->Transaction_Model->get_user($invoice_number);
                $user_id = $user_id[0]->user_id;
                $user_object = $this->ion_auth->user($user_id)->row();
                $transaction_items = $this->Transaction_Model->get_transaction_item($invoice_number);

                // check user in mooc
                $log = format_log_data(['email' => $user_object->username], 'Cek User in Mooc');
                insert_log('USER_FRONTEND_PAYMENT', $log);
                $sync_response = check_user_mooc($user_object->username);
                if(empty($sync_response['users']) && !isset($sync_response['exception'])){
                    // if no user then create it
                    $sync_data = new stdClass();
                    $sync_data->username = $user_object->username;
                    $sync_data->fullname = $user_object->full_name;
                    $sync_data->email = $user_object->email;
                    $log = format_log_data($sync_data, 'Create User in Mooc');
                    insert_log('USER_FRONTEND_PAYMENT', $log);
                    $sync_response = create_user_mooc($sync_data);

                    // user berhasil di create
                    if(isset($sync_response[0]['id'])){
                        $primary_key['id'] = $user_id;
                        $update_data['mooc_id'] = $sync_response[0]['id'];
                        $log = format_log_data(['mooc_id' => $sync_response[0]['id']], 'Update User Mooc Id');
                        insert_log('USER_FRONTEND_PAYMENT', $log);
                        $this->web_app_model->update_data("users", $update_data, $primary_key['id'], "id");
                    }else{
                        $log = format_log_data($sync_data, 'Failed Create User In Mooc');
                        insert_log('USER_FRONTEND_PAYMENT', $log);
                    }
                }elseif(isset($sync_response['exception']) && !empty($sync_response['exception'])){
                    $err_message = isset($sync_response['debuginfo']) ? $sync_response['debuginfo'] : ((isset($sync_response['message'])) ? $sync_response['message'] : ' Failed To Enroll User');
                    
                    $log = format_log_data(['email' => $user_object->username], $err_message);
                    insert_log('USER_FRONTEND_PAYMENT_FAILED', $log);

                    return;
                }
                $insert_data['user_id'] = $user_id;
                $insert_data['enroll_type_id'] = 1;

                $email_param = [];
                foreach($transaction_items as $item){
                    $where = [
                        'ocw_course_id' => $item->course_id,
                        'user_id' => $user_id,
                        'enroll_type_id' => 1
                    ];
                    $log = format_log_data($where, 'Check User Has Enroll Or Not');
                    insert_log('USER_FRONTEND_PAYMENT', $log);
                    $enroll_objects = $this->web_app_model->get_data_and_where("ocw_course_enroll", $where);

                    if(empty($enroll_objects)){
                        $course_object = $this->web_app_model->get_single_data('ocw_course_joined', ['id' => $item->course_id]);
    
                        if($course_object->is_multiple == 0){
                            // enroll user in mooc
                            $sync_data = new stdClass();
                            $sync_data->username = $user_object->username;
                            $sync_data->enroll_type_id = 1;
                            $sync_data->subject_code = $course_object->subject_code;
                            $sync_data->ocw_id = $course_object->id;
                            $sync_data->start_date = strtotime($item->start_date);
                            $sync_data->end_date = strtotime($item->end_date);
                            $log = format_log_data($sync_data, 'Enroll User In Mooc');
                            insert_log('USER_FRONTEND_PAYMENT', $log);
                            $sync_response = enroll_user_mooc($sync_data);
                            $log = format_log_data(['response' => $sync_response], 'Enroll User In Mooc');
                            insert_log('USER_FRONTEND_PAYMENT', $log);


                            if(isset($sync_response['status']) && $sync_response['status'] == 'success'){
                                $insert_data['ocw_course_id'] = $item->course_id;
                                $log = format_log_data($insert_data, 'Enroll User');
                                insert_log('USER_FRONTEND_PAYMENT', $log);
                                $this->web_app_model->insert_data("ocw_course_enroll", $insert_data);

                                $email_param[] = $item;
                            }else{
                                $log = format_log_data($sync_data, 'Failed Enroll User In Mooc');
                                insert_log('USER_FRONTEND_PAYMENT', $log);
                            }
                        }else{
                            $package_object = $this->web_app_model->get_single_data('packet_ocw_course', ['course_id' => $course_object->id]);
                            $course_reference_objects = $this->web_app_model->get_data('reference_multiple_course', ['package_ocw_course_id' => $package_object->id]);
                            foreach($course_reference_objects as $reference){
                                $course_object = $this->web_app_model->get_single_data('ocw_course_joined', ['id' => $reference->course_target_id]);
                                
                                // enroll user in mooc
                                $sync_data = new stdClass();
                                $sync_data->username = $user_object->username;
                                $sync_data->enroll_type_id = 1;
                                $sync_data->subject_code = $course_object->subject_code;
                                $sync_data->ocw_id = $course_object->id;
                                $sync_data->start_date = strtotime($course_object->start_date);
                                $sync_data->end_date = strtotime($course_object->end_date);
                                $log = format_log_data($sync_data, 'Enroll User In Mooc');
                                insert_log('USER_FRONTEND_PAYMENT', $log);
                                $sync_response = enroll_user_mooc($sync_data);
                                $log = format_log_data(['response' => $sync_response], 'Enroll User In Mooc');
                                insert_log('USER_FRONTEND_PAYMENT', $log);
    
    
                                if(isset($sync_response['status']) && $sync_response['status'] == 'success'){
                                    $insert_data['ocw_course_id'] = $course_object->id;
                                    $log = format_log_data($insert_data, 'Enroll User');
                                    insert_log('USER_FRONTEND_PAYMENT', $log);
                                }else{
                                    $log = format_log_data($sync_data, 'Failed Enroll User In Mooc');
                                    insert_log('USER_FRONTEND_PAYMENT', $log);
                                }
                            }
                            
                            $insert_data['ocw_course_id'] = $item->course_id;
                            $this->web_app_model->insert_data("ocw_course_enroll", $insert_data);
    
                            $email_param[] = $item;
                        }
                    }else{
                        $log = format_log_data($sync_data, 'User Has Enrolled');
                        insert_log('USER_FRONTEND_PAYMENT', $log);
                    }
                    
                }
                if(!empty($email_param)){
                    $this->load->library('email');
                    $this->email->from('celoe@telkomuniversity.ac.id', 'CeLOE');
                    $this->email->to($user_object->email);
                    $this->email->subject('Your Enrollment Account');
                    $filename = $this->config->item('ASSET_PATH') . 'fe_v2/assets/images/logo-celoe-small.png';
                    $this->email->attach($filename);
                    $data['cid'] = $this->email->attachment_cid($filename);

                    $data['username'] = $user_object->username;
                    $data['courses'] = $email_param;
                    $email_body = $this->load->view('email_templates/purchase2', $data, true);
                    $this->email->message($email_body);

                    $this->email->send();
                }


				//sent notification to user based on invoice number
                $user_id = $this->Transaction_Model->get_user($invoice_number);
                $user_id = $user_id[0]->user_id;
				$title = "Transaction Success";
                $body = "Your transaction with invoice number $invoice_number has been confirmed";
                $log = format_log_data(['user_id' => $user_id, 'title' => $title, 'body' => $body], 'Send Notif');
                insert_log('USER_FRONTEND_PAYMENT', $log);
                $this->push_notif->send_notification($user_id, $title, $body);
                
                $log = format_log_data([], 'Payment Success');
                insert_log('USER_FRONTEND_PAYMENT_SUCCESS', $log);
        	}
        }else{
            $log = format_log_data([], $bni_callback_response->message);
            insert_log('USER_FRONTEND_PAYMENT_FAILED', $log);
        }
        
     	echo $bni_callback_response->message;  
    }
}

/* End of file Controllername.php */