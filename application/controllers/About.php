<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Web_app_model');
    }

    public function index()
    {
        // $header["title"] = $body["title"] = "News";
        // $header["sub_menu"] = false;
        // $header['active_page'] = "news";
        // $this->load->view('fe_v2/nav_bar',$header);
        // $this->load->view('fe_v2/news/index');
        // $this->load->view('fe_v2/footer');

        $data['dashboard'] = $this->web_app_model->get_single_data('landing_about', ['id' => 1]);
        $data['groups'] = $this->get_abouts();
        $data["sub_menu"] = false;
        $data["title"] = "About Us";
        $data['active_page'] = "about";
        $this->load->view('fe_v2/about/about', $data);
    }

    private function get_abouts(){
        $abouts_object = $this->web_app_model->get_data('landing_about');
        array_shift($abouts_object);

        $arr_about = [];
        foreach($abouts_object as $about){
            $arr_about[$about->group][] = $about;
        }

        return $arr_about;
    }

    public function get_description(){
        $response = init_response();
        
        $id = $this->input->get('id');

        $about = $this->web_app_model->get_single_data('landing_about', ['id' => $id]);

        $response->status = true;
        $response->data = $about;
        json_response($response);
    }

    public function test()
    {
        $data['cid'] = "http://localhost/celoe/assets/media/img/celoe_small.png";
        $data['username'] = "firumanusia";
        $c = new stdClass;
        $c->course_title = "Course Title";
        $c->start_date = "22/02/2020";
        $c->end_date = "22/04/2020";
        $data['courses'] = array($c,$c,$c,$c);
        $this->load->view('email_templates/purchase2', $data);
    }
}

/* End of file Controllername.php */