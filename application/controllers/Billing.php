<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Transaction_Model');
        $this->load->library('ShoppingCart');
        $this->load->helper('log_user_frontend');
    }

    public function make_tx()
    {
        $response = init_response();

        $log = format_log_data([], 'Check Login User');
        insert_log('USER_FRONTEND_MAKE_TX', $log);
        $logged_in_user = $this->ion_auth->user()->row();

        $override_tx = $this->input->get('override',true);
        $override_tx = $override_tx == 1;//convert to boolean

        if(!empty($logged_in_user)){
            //get cart info
            $log = format_log_data(['user_id' => $logged_in_user->id], 'Get Cart Info');
            insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
            $cart_items = $this->shoppingcart->get($logged_in_user->id);

            if(!empty($cart_items)){
                $should_make_tx = true;
                $log = format_log_data(['user_id' => $logged_in_user->id, 'status' => 0], 'Get Existing Tx');
                insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                $existing_tx =  $this->Transaction_Model->get_by_user($logged_in_user->id,Transaction_Model::STATUS_WAITING);

                if(!empty($existing_tx) && !$override_tx){
                    $log = format_log_data(['user_id' => $logged_in_user->id, 'status' => 0], 'Pending transaction exists');
                    insert_log('USER_FRONTEND_MAKE_TX_FAILED', $log, $logged_in_user->id);

                    //ask user to override
                    $response->error_code = 407;
                    $response->data = $existing_tx[0]->invoice_number;
                    $response->message = "Pending transaction exists";

                    $should_make_tx = false;
                }else if(!empty($existing_tx)){
                    // expiring bni va
                    $bni_ecoll_active = $this->config->item("bni_ecoll_active");
                    $log = format_log_data(['bni_ecoll_active' => $bni_ecoll_active], 'Check BNI Ecoll');
                    insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                    if($bni_ecoll_active === true){
                        //use real api
                        $bni_conf['client_id'] = $this->config->item("bni_ecoll_client_id");
                        $bni_conf['secret_key'] = $this->config->item("bni_ecoll_secret_key");
                        $bni_conf['url'] = $this->config->item("bni_ecoll_url");
                        $this->load->library('BNIecoll',$bni_conf);

                        $log = format_log_data(['invoice_number' => $existing_tx[0]->invoice_number, 'last_va' => $existing_tx[0]->bni_va_number], 'Expiring Last VA');
                        insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                        $va_response = $this->bniecoll->delete_va($existing_tx[0]->invoice_number,$existing_tx[0]->total_price,$existing_tx[0]->user_full_name,$existing_tx[0]->user_email,$existing_tx[0]->user_phone,date('Y-m-d H:i:s', strtotime("now")));
                        
                        $va_expiring_success = $va_response->success;
                        if($va_expiring_success && is_array($va_response->data) && array_key_exists("virtual_account", $va_response->data)){
                            $generated_va = $va_response->data["virtual_account"];
                        }else{
                            $log = format_log_data(['invoice_number' => $existing_tx[0]->invoice_number, 'last_va' => $existing_tx[0]->bni_va_number], 'Failed Expiring Last VA');
                            insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                            //TODO : Log external api
                        }
                    }
                    //override tx
                    $log = format_log_data(['invoice_number' => $existing_tx[0]->invoice_number, 'status' => 3], 'Cancelling Transaction');
                    insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                    $this->Transaction_Model->set_tx_status($existing_tx[0]->invoice_number,Transaction_Model::STATUS_CANCELLED);
                }

                //make tx
                if($should_make_tx){
                    //prepare data
                    $transaction_items = array();
                    $invoice_number = $this->generate_invoice_number($logged_in_user->id);
                    $invoice_expiry = date('Y-m-d H:i:s', strtotime("+1 days"));
                    $total_price = 0;

                    //build transaction item
                    foreach($cart_items as $cart_item){
                        $transaction_item = array();
                        $transaction_item['invoice_number'] = $invoice_number;
                        $transaction_item['course_id'] = $cart_item->course_id;
                        $transaction_item['course_title'] = $cart_item->ocw_name;
                        $transaction_item['price'] = $cart_item->price;
                        $transaction_item['course_description'] = $cart_item->description;
                        $transaction_item['start_date'] = $cart_item->start_date;
                        $transaction_item['end_date'] = $cart_item->end_date;

                        $total_price +=  $cart_item->price;
                        $transaction_items[] = $transaction_item;
                    }

                    //build transaction data
                    $transaction_data = array();
                    $transaction_data['user_id'] = $logged_in_user->id;
                    $transaction_data['username'] = $logged_in_user->username;
                    $transaction_data['user_full_name'] = $logged_in_user->full_name;
                    $transaction_data['user_email'] = $logged_in_user->email;
                    $transaction_data['user_gender'] = $logged_in_user->gender;
                    $transaction_data['user_birth_date'] = $logged_in_user->dateBirth;
                    $transaction_data['user_phone'] = $logged_in_user->phone_number;
                    $transaction_data['expired_at'] = $invoice_expiry;
                    $transaction_data['total_price'] = $total_price;
                    $transaction_data['invoice_number'] = $invoice_number;

                        
                    $city = $this->web_app_model->get_single_data("city",array("id"=>$logged_in_user->city_id));
                    if(!empty($city)){
                        $transaction_data['user_city'] = $city->name;
                    }else{
                        $transaction_data['user_city'] = "";
                    }

                    $country = $this->web_app_model->get_single_data("country",array("id"=>$logged_in_user->country_id));
                    if(!empty($country)){
                        $transaction_data['user_country'] = $country->name;
                    }else{
                        $transaction_data['user_country'] = "";
                    }

                    //generate va number from BNI
                    $generated_va = "";
                    $va_generation_success = false;

                    $bni_ecoll_active = $this->config->item("bni_ecoll_active");
                    $log = format_log_data(['bni_ecoll_active' => $bni_ecoll_active], 'Check BNI Ecoll');
                    insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                    if($bni_ecoll_active === true){
                        //use real api
                        $bni_conf['client_id'] = $this->config->item("bni_ecoll_client_id");
                        $bni_conf['secret_key'] = $this->config->item("bni_ecoll_secret_key");
                        $bni_conf['url'] = $this->config->item("bni_ecoll_url");
                        $this->load->library('BNIecoll',$bni_conf);
                        
                        $input = [
                            'invoice_number' => $invoice_number,
                            'total_price' => $total_price,
                            'user_full_name' => $logged_in_user->full_name,
                            'user_email' => $logged_in_user->email,
                            'user_phone' => $logged_in_user->phone_number,
                            'expired_at' => $invoice_expiry
                        ];
                        $log = format_log_data($input, 'Generate BNI VA');
                        insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                        $va_response = $this->bniecoll->generate_va($invoice_number,$total_price,$logged_in_user->full_name,$logged_in_user->email,$logged_in_user->phone_number,$invoice_expiry);
                        
                        $va_generation_success = $va_response->success;
                        if($va_generation_success && is_array($va_response->data) && array_key_exists("virtual_account", $va_response->data)){
                            $generated_va = $va_response->data["virtual_account"];
                        }else{
                            $log = format_log_data($input, 'Failed Generate BNI VA');
                            insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                            //TODO : Log external api
                        }
                    }else{
                        //use dummy va
                        $va_generation_success = true;
                        $generated_va = "DUMMY_".time();
                    }
                    
                    if($va_generation_success){
                        $transaction_data['bni_va_number'] = $generated_va;
                        $log = format_log_data(['transaction_data' => $transaction_data, 'transaction_items' => $transaction_items], 'Create trasnaction');
                        insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                        $this->Transaction_Model->create($transaction_data,$transaction_items);

                        //delete cart
                        $log = format_log_data(['user_id' => $logged_in_user->id], 'Remove All Cart From User');
                        insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                        $this->shoppingcart->remove_all($logged_in_user->id);

                        $log = format_log_data(['invoice_number' => $invoice_number], 'Transaction successfully created');
                        insert_log('USER_FRONTEND_MAKE_TX_SUCCESS', $log, $logged_in_user->id);

                        $response->status = true;
                        $response->data = $invoice_number;
                        $response->message = "Transaction successfully created";
                    }else{
                        $log = format_log_data(['va_generation' => $va_generation_success], 'We cannot process your transaction request right now, please try again later');
                        insert_log('USER_FRONTEND_MAKE_TX_FAILED', $log, $logged_in_user->id);

                        $response->error_code = 500;
                        $response->message = "We cannot process your transaction request right now, please try again later";
                        $response->data = array();
                    }
                }

            }else{
                $log = format_log_data(['user_id' => $logged_in_user->id], "You don't have any item in your cart");
                insert_log('USER_FRONTEND_MAKE_TX', $log, $logged_in_user->id);
                
                $response->error_code = 400;
                $response->message = "You don't have any item in your cart";
                $response->data = array();
            }
        }else{
            $log = format_log_data($logged_in_user, 'Please login first');
            insert_log('USER_FRONTEND_MAKE_TX_FAILED', $log);

            $response->error_code = 403;
            $response->message = "Please login first";
            $response->data = array();
        }
        json_response($response);
    }

    private function generate_invoice_number($user_id)
    {
        $year = date('y');
        $month = date('m');
        $roman_year = $this->roman_number(20);
        $roman_month = $this->roman_number($month);

        //$invoice_number = $this->Transaction_Model->get_max_id() + 1;//uncomment this to use max id instead of timestamp
        $invoice_number = time();

        return "INV/$roman_year/$roman_month/$user_id/$invoice_number";
    }

    private function roman_number($number) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
}

/* End of file Controllername.php */