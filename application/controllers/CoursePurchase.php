<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CoursePurchase extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function cart()
    {
        $data["title"] = "Course Cart";
        $data["sub_menu"] = false;
        $data['active_page'] = "home";
        $this->load->view('fe/course_purchase/cart', $data);
    }

    public function payment()
    {
        $data["title"] = "Course Payment";
        $data["sub_menu"] = false;
        $data['active_page'] = "home";
        $this->load->view('fe/course_purchase/payment', $data);
    }

    public function guide()
    {
        $data["title"] = "Payment Guide";
        $data["sub_menu"] = false;
        $data['active_page'] = "home";
        $this->load->view('fe/course_purchase/guide', $data);
    }
}
