<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Dashboard extends BaseBEController {

	function __construct() 
	{
		parent::__construct();							
	}
	
	public function index()
	{			
		if(!is_logged_in()){
			redirect("/auth");
		}
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
		$this->load->view('dashboard/index.php',$data);
	}

//	public function new()
//	{		
//		$this->load->view('dashboard/dashboard1.php');
//	}

	public function signin()
	{
		
	}

	public function signout()
	{
		
	}

	
}
