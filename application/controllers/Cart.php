<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->model('web_app_model');
        $this->load->library('ShoppingCart');
        $this->load->helper('log_user_frontend');
    }

	public function index()
	{
        $logged_in_user = $this->ion_auth->user()->row();
        if(empty($logged_in_user)){
            redirect(base_url('UsersAuth').'?redir='.urlencode(base_url(uri_string())));
        }else{
            $cart_items = $this->shoppingcart->get($logged_in_user->id);
            $data["cart_items"] = $cart_items;
            $data["sub_menu"] = false;
            $data["title"] = "Cart";
            $data['active_page'] = "cart";
            $this->load->view('fe_v2/cart/index', $data);
        }
    }

	public function checkout()
	{
        $invoice_number = $this->input->get('invoice');
        if(!$invoice_number){
            echo 'An Error Has Occured, Please Try Again Later';
            return;
        }

        $data["sub_menu"] = false;
        $data["title"] = "Checkout";
        $data['active_page'] = "checkout";
        
        $data['transaction'] = $this->web_app_model->get_single_data('transaction', ['invoice_number' => $invoice_number]);
        $data["billing_content"] = $this->load_billing_content();
        $this->load->view('fe_v2/cart/checkout', $data);
    }

    public function get()
    { 
        $response = init_response();
        $logged_in_user = $this->ion_auth->user()->row();
        if(!empty($logged_in_user)){
            $response->status = true;
            $response->data = $this->shoppingcart->get($logged_in_user->id);
        }else{
            $response->error_code = 403;
            $response->message = "Please login first";
            $response->data = array();
        }
        json_response($response);
    }

    public function add()
    {
        $this->load->model('Course_Model');
        $response = init_response();
        $course_id = $this->input->get('course_id',true);

        $log = format_log_data([], 'Check Login User');
        insert_log('USER_FRONTEND_ADD_CART', $log);
        $logged_in_user = $this->ion_auth->user()->row();

        if(!empty($logged_in_user)){
            $log = format_log_data(['course_id' => $course_id], 'Check Valid Course');
            insert_log('USER_FRONTEND_ADD_CART', $log, $logged_in_user->id);
            $course = $this->Course_Model->get_valid_course($course_id);

            if(!empty($course)){
                $log = format_log_data(['transaction.user_id' => $logged_in_user->id, 'transaction.status' => 2, 'transaction_item.course_id' => $course_id], 'Check Transaction');
                insert_log('USER_FRONTEND_ADD_CART', $log, $logged_in_user->id);
                $transaction_objects = $this->web_app_model->get_data_join('transaction', 'transaction_item', '', 'transaction.invoice_number = transaction_item.invoice_number', '', ['transaction.user_id' => $logged_in_user->id, 'transaction.status' => 2, 'transaction_item.course_id' => $course_id]);

                if(empty($transaction_objects)){
                    $log = format_log_data(['course_id' => $course_id, 'user_id' => $logged_in_user->id], 'Add Course To Cart');
                    insert_log('USER_FRONTEND_ADD_CART', $log, $logged_in_user->id);
                    $insert_result = @$this->shoppingcart->add($course_id,$logged_in_user->id);

                    if($insert_result){
                        $log = format_log_data(['transaction.user_id' => $logged_in_user->id, 'transaction.status' => 2, 'transaction_item.course_id' => $course_id], 'Course successfully added to your cart');
                        insert_log('USER_FRONTEND_ADD_CART_SUCCESS', $log, $logged_in_user->id);

                        $response->status = true;
                        $response->message = "Course successfully added to your cart";
                    }else{
                        $log = format_log_data(['transaction.user_id' => $logged_in_user->id, 'transaction.status' => 2, 'transaction_item.course_id' => $course_id], 'Course has been added to your cart');
                        insert_log('USER_FRONTEND_ADD_CART_FAILED', $log, $logged_in_user->id);

                        $response->error_code = 500;
                        $response->message = "Course has been added to your cart";
                    }
                }else{
                    $log = format_log_data(['transaction.user_id' => $logged_in_user->id, 'transaction.status' => 2, 'transaction_item.course_id' => $course_id], 'You already bought this course');
                    insert_log('USER_FRONTEND_ADD_CART_FAILED', $log, $logged_in_user->id);

                    $response->error_code = 500;
                    $response->message = "You already bought this course";
                }
            }else{
                $log = format_log_data(['course_id' => $course_id], 'This course is not available for purchase now');
                insert_log('USER_FRONTEND_ADD_CART_FAILED', $log, $logged_in_user->id);

                $response->error_code = 404;
                $response->message = "This course is not available for purchase now";
                $response->ww = $this->db->last_query();
            }
        }else{
            $log = format_log_data($logged_in_user, 'Please login first');
            insert_log('USER_FRONTEND_ADD_CART_FAILED', $log);

            $response->error_code = 403;
            $response->message = "Please login first";
        }
        json_response($response);
    }

    public function remove()
    {
        $this->load->model('Course_Model');
        $response = init_response();
        $course_id = $this->input->get('course_id',true);

        $log = format_log_data([], 'Check Login User');
        insert_log('USER_FRONTEND_REMOVE_CART', $log);
        $logged_in_user = $this->ion_auth->user()->row();

        if(!empty($logged_in_user)){
            $log = format_log_data(['course_id' => $course_id], 'Check Valid Course');
            insert_log('USER_FRONTEND_REMOVE_CART', $log, $logged_in_user->id);
            $course = $this->Course_Model->get_valid_course($course_id);

            if(!empty($course)){
                $log = format_log_data(['course_id' => $course_id, 'user_id' => $logged_in_user->id], 'Remove Course In Cart');
                insert_log('USER_FRONTEND_REMOVE_CART', $log, $logged_in_user->id);
                @$this->shoppingcart->remove($course_id,$logged_in_user->id);
                
                $log = format_log_data(['course_id' => $course_id, 'user_id' => $logged_in_user->id], 'Course successfully removed from your cart');
                insert_log('USER_FRONTEND_REMOVE_CART_SUCCESS', $log, $logged_in_user->id);
                $response->status = true;
                $response->message = "Course successfully removed from your cart";
            }else{
                $log = format_log_data(['course_id' => $course_id, 'user_id' => $logged_in_user->id], 'Course has been removed');
                insert_log('USER_FRONTEND_REMOVE_CART_FAILED', $log, $logged_in_user->id);

                $response->error_code = 404;
                $response->message = "Course has been removed";
                $response->ww = $this->db->last_query();
            }
        }else{
            $log = format_log_data($logged_in_user, 'Please login first');
            insert_log('USER_FRONTEND_REMOVE_CART_FAILED', $log);

            $response->error_code = 403;
            $response->message = "Please login first";
        }
        json_response($response);
    }

    public function load_billing_content(){
        $content = $this->web_app_model->get_data_list("landing_billing","","",0,1000,"priority","","asc");
        return $content;
    }

}

/* End of file Controllername.php */