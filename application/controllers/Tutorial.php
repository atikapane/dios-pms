<?php
/**
 * Created by PhpStorm.
 * User: djakapermana
 * Date: 13/11/19
 * Time: 21.13
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Tutorial extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->model('web_app_model');
    }

    public function index()
    {
        // $header["title"] = $body["title"] = "Tutorial";
        // $header["sub_menu"] = false;
        // $header['active_page'] = "tutorial";
        // $this->load->view('fe_v2/nav_bar',$header);
        // $this->load->view('fe_v2/tutorial/index');
        // $this->load->view('fe_v2/footer');

        $data["sub_menu"] = false;
        $data["title"] = "Tutorial";
        $data["sub_title"] = "Subtitle";
        $data['active_page'] = "tutorial";
        $this->load->view('fe_v2/tutorial/index', $data);
    }

    public function get()
    {
        $query = $this->input->get("q");
        $limit = $this->input->get("l");
        $offset = $this->input->get("o");

        $data = $this->web_app_model->get_data_list("landing_tutorial", $query, "title", $offset, $limit, "id", false);
        foreach ($data as $single_data) {
            if($single_data->type == "file"){
                $single_data->file_path = $single_data->file_path;
            }
        }
        echo json_encode($data);
    }

}

/* End of file Controllername.php */