<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersAuth extends CI_Controller {

    public function __construct()
    {
    	parent::__construct();
        $this->load->model('Users_Model');
    }

	public function index()
	{
        $oauth_provider = $this->session->flashdata('oauth_provider');
        $data["data_user"] = null;
        $data["redir"] = $this->input->get('redir');

        if(!empty($oauth_provider)){
            if($oauth_provider == 'google'){
                $this->google->setRedirectUri(base_url('UsersAuth'));
                if (isset($_GET['code'])) {
                    $token = $this->google->getAuthenticate($_GET['code']);
                    $this->google->setAccessToken($token['access_token']);
                    $gpInfo = $this->google->getUserInfo();

                    $userData["oauth_provider"] = $oauth_provider;
                    $userData["id"] = $gpInfo["id"];
                    $userData["email"] = $gpInfo["email"];
                }
            } else if($oauth_provider == "facebook"){
                $this->facebook->set_redirect_uri('UsersAuth');
                if($this->facebook->is_authenticated()){
                    $fb_user = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,picture');

                    $userData["oauth_provider"] = $oauth_provider;
                    $userData["id"] = $fb_user["id"];
                    $userData["email"] = $fb_user["email"];
                }
            }
            $user_object = $this->web_app_model->get_single_data('users', ['email' => $userData['email']]);
            if(!empty($user_object)){
                if($user_object->oauth_provider == $userData['oauth_provider'] && $user_object->oauth_uid == $userData['id']){
                    if($this->ion_auth->login_oauth($user_object->email)){
                        $get_user_data = $this->Users_Model->checkUserByEmail($user_object->email);
                        if($get_user_data){
                            $this->session->set_userdata('user_data',$get_user_data);
                        } 
                        redirect('', 'auto');
                    }else{
                        $this->session->set_flashdata('message_error', strip_tags("Message error: ".$this->ion_auth->errors()));
                        redirect('', 'auto');
                    }
                }else{
                    $this->session->set_flashdata('message_error', 'this ' . $userData['oauth_provider'] . ' account is not associated with CeLOE account, please try to login manually by using your email and password');
                    redirect('', 'auto');
                }
            }else{
                $this->session->set_flashdata('message_error', 'This account is not registered, please register first in the registration menu');
                redirect('', 'auto');
            }

        }

        $data["title"] = "Login";
        $data["sub_menu"] = false;
        $data['active_page'] = "login";
        $this->load->view('fe/login/index', $data);
	}

    public function processLogin(){
        $response = init_response();

        $identity = $this->input->post('email',true);
        $password = $this->input->post('password',true);
        $remember = (bool)$this->input->post('remember',true);

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('remember', 'Remember me', 'integer');

        if ($this->form_validation->run()==TRUE) {
            if($this->ion_auth->login($identity, $password, $remember)){
                $get_user_data = $this->Users_Model->checkUserByEmail($identity);
                if(empty($get_user_data)){
                    $response->status = false;
                    $response->message = 'User Not Found';
                    $response->error_code = 400;

                    echo json_encode($response);
                    return;
                }
                
                $this->session->set_userdata('user_data',$get_user_data);
                $response->status = true;
                $response->message = $this->ion_auth->messages();
                $response->error_code = 200;
            }else{
                $response->status = false;
                $response->message = strip_tags("Message error: ".$this->ion_auth->errors());
                $response->error_code = 400;
            }
        }else{
            $response->status = false;
            $response->message = "Message error: ".validation_errors();
            $response->error_code = 400;
        }
        json_response($response);
    }

    public function processLogout(){
        if($this->ion_auth->logout()){
            redirect('','auto');
        };
    }

    public function login_google(){
        $this->google->setRedirectUri(base_url('UsersAuth'));
        $this->session->set_flashdata('oauth_provider', 'google');
        header("Location:".$this->google->loginURL());
    }

    public function login_facebook(){
        $this->facebook->set_redirect_uri('UsersAuth');
        $this->session->set_flashdata('oauth_provider', 'facebook');
        header("Location:".$this->facebook->login_url());
    }

}

/* End of file Controllername.php */