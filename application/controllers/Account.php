<?php

use mikehaertl\tmp\File;

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(empty($this->ion_auth->user()->row())) redirect('?redir=' . urlencode(current_url()), 'auto');
        $this->load->model('Web_app_model');
        $this->load->model('Transaction_Model');
        $this->load->helper('sync_mooc');
        $this->load->helper('sync_igracias');
    }

    public function index()
    {
        $user = $this->ion_auth->user()->row();
        $data['courses_status'] = $this->load_courses_status($user->id);
        $data['user'] = $user;
        $data["sub_menu"] = false;
        $data["title"] = "My Course";
        $data['active_page'] = "account";
        $this->load->view('fe_v2/my_account/my_course', $data);
    }

    public function transaction(){
        $user = $this->ion_auth->user()->row();
        $data['courses_status'] = $this->load_courses_status($user->id);
        $data['user'] = $user;
        $data["sub_menu"] = false;
        $data["title"] = "Transaction";
        $data['active_page'] = "account";
        $this->load->view('fe_v2/my_account/transaction', $data);
    }

    public function profile(){
        $user = $this->ion_auth->user()->row();
        $data['courses_status'] = $this->load_courses_status($user->id);
        $data['user'] = $user;
        $data['cities'] = $this->web_app_model->get_data('city', ['country_id' => 1]);
        $data["sub_menu"] = false;
        $data["title"] = "My Profile";
        $data['active_page'] = "account";
        $this->load->view('fe_v2/my_account/my_profile', $data);
    }

    public function certificate(){
        $user = $this->ion_auth->user()->row();
        $data['courses_status'] = $this->load_courses_status($user->id);
        $data['user'] = $user;
        $data["sub_menu"] = false;
        $data["title"] = "Certificate";
        $data['active_page'] = "account";
        $this->load->view('fe_v2/my_account/my_certificate', $data);
    }

    private function load_courses_status($user_id){
        $courses = [];
        $courses['Upcoming'] = [];
        $courses['Active'] = [];
        $courses['Finished'] = [];
        
        $enroll_objects = $this->web_app_model->get_data_join('ocw_course_enroll', 'ocw_course', '','ocw_course_enroll.ocw_course_id = ocw_course.id', '', ['ocw_course_enroll.user_id' => $user_id]);
        if(empty($enroll_objects)) return $courses;

        foreach($enroll_objects as $course){
            if(time() < strtotime($course->start_date)) array_push($courses['Upcoming'], $course);
            if(time() > strtotime($course->end_date)) array_push($courses['Finished'], $course);
            if(time() >= strtotime($course->start_date) && time() <= strtotime($course->end_date)) array_push($courses['Active'], $course);
        }

        $courses['Upcoming'] = count($courses['Upcoming']);
        $courses['Finished'] = count($courses['Finished']);
        $courses['Active'] = count($courses['Active']);

        return $courses;
    }

    public function get_transactions(){
        $response = get_ajax_response();
        $search = $this->input->get('search');
        $select_date = $this->input->get('select_date');
        $status = $this->input->get('status');
        $start = $this->input->get('start');
        $limit = $this->input->get('limit');

        $user = $this->ion_auth->user()->row();
        $where = [
            'user_id' => $user->id
        ];
        if($status != "") $where['status'] = $status;
        if(!empty($select_date)){
            $start_date = new DateTime($select_date);
            $end_date = new DateTime($select_date);
    
            $end_date->modify('+1 day')->modify('-1 second');
            if ($start_date < $end_date) $where["expired_at BETWEEN '" . $start_date->format('Y-m-d H:i:s') . "' AND '" . $end_date->format('Y-m-d H:i:s') . "'"] = '';
        }
        
        if(!empty($search)) $where['(transaction.invoice_number LIKE "%' . $search . '%" OR ocw_course.ocw_name LIKE "%' . $search . '%")'] = null;
        // $transaction_objects = $this->web_app_model->get_data_list('transaction', $search, 'invoice_number', $start, $limit, 'created_date', $where);
        $transaction_objects = $this->web_app_model->get_data_join_list('transaction', 'transaction_item', 'ocw_course', 'transaction.invoice_number = transaction_item.invoice_number', 'transaction_item.course_id = ocw_course.id', $where, $start, $limit, 'transaction.*', 'transaction.invoice_number', 'transaction.created_date desc');
        foreach($transaction_objects as &$transaction_object){
            $banner_image_objects = $this->web_app_model->get_data_join('transaction_item', 'ocw_course', '', 'transaction_item.course_id = ocw_course.id', '', ['transaction_item.invoice_number' => $transaction_object->invoice_number], 'ocw_course.banner_image');
            if(!empty($banner_image_objects)){
                $transaction_object->banner_image = $banner_image_objects[0]->banner_image;
            }
        }
        // if(!empty($search)) $where['transaction.invoice_number LIKE'] = '%' . $search . '%';
        // $count = $this->web_app_model->get_data_and_where('transaction', $where, '', '', 'COUNT(id) as total_item');
        $count = $this->web_app_model->get_data_join('transaction', 'transaction_item', 'ocw_course', 'transaction.invoice_number = transaction_item.invoice_number', 'transaction_item.course_id = ocw_course.id', $where, 'COUNT(transaction.id) as total_item', 'transaction.invoice_number');

        if(!empty($transaction_objects)){
            $response->code = 200;
            $response->status = true;
            $response->transactions = $transaction_objects;
            $response->count = $count[0]->total_item;
        }else{
            $response->code = 400;
            $response->status = false;
            $response->count = 0;
            $response->message = "You Don't Have Appropriate Transactions";
        }
        echo json_encode($response);
    }

    public function get_detail_transactions(){
        $response = get_ajax_response();
        $invoice_number = $this->input->get('invoice_number');

        $transaction_item_objects = $this->web_app_model->get_data_join('transaction_item', 'ocw_course', '', 'transaction_item.course_id = ocw_course.id', '', ['transaction_item.invoice_number' => $invoice_number]);
        foreach($transaction_item_objects as &$transaction_item_object){
            $transaction_item_object->course_description = strip_tags($transaction_item_object->course_description);
        }

        if(!empty($transaction_item_objects)){
            $response->code = 200;
            $response->status = true;
            $response->detail_transactions = $transaction_item_objects;
        }else{
            $response->code = 400;
            $response->status = false;
            $response->message = "You Don't Have Appropriate Transaction Items";
        }
        echo json_encode($response);
    }

    public function get_paid_courses(){
        $response = get_ajax_response();
        $search = $this->input->get('search');
        $select_date = $this->input->get('select_date');
        $status = $this->input->get('status');
        $start = $this->input->get('start');
        $limit = $this->input->get('limit');

        $user = $this->ion_auth->user()->row();
        $where = [
            'ocw_course_enroll.user_id' => $user->id,
            'ocw_course_enroll.enroll_type_id' => 1
        ];

        if(!empty($search)){
            $where['ocw_course.ocw_name LIKE'] = '%' . $search . '%';
        }

        if(!empty($select_date)){
            $where['ocw_course.start_date <='] = $select_date;
            $where['ocw_course.end_date >='] = $select_date;
        }
        
        if(!empty($status)){
            if($status == 'upcoming') {
                $where['ocw_course.start_date >'] = date('Y-m-d');
            }else if($status == 'completed'){
                $where['ocw_course.end_date <'] = date('Y-m-d');
            }else{
                $where['ocw_course.start_date <='] = date('Y-m-d');
                $where['ocw_course.end_date >='] = date('Y-m-d');
            }
        }

        $courses = $this->web_app_model->get_data_join_list('ocw_course_enroll', 'ocw_course', '','ocw_course_enroll.ocw_course_id = ocw_course.id', '', $where, $start, $limit, 'ocw_course.*', '', 'ocw_course.created_at');
        foreach($courses as &$course){
            $course->description = strip_tags($course->description);

            if($course->is_multiple == 1){
                $course->courses = $this->web_app_model->get_data_join('packet_ocw_course as packet', 'reference_multiple_course as reference', 'ocw_course_joined as course', 'packet.id = reference.package_ocw_course_id', 'reference.course_target_id = course.id', ['packet.course_id' => $course->id]);

                foreach($course->courses as &$course){
                    $course->description = strip_tags($course->description);
                }
            }
        }
        $count = $this->web_app_model->get_data_join('ocw_course_enroll', 'ocw_course', '','ocw_course_enroll.ocw_course_id = ocw_course.id', '', $where, 'COUNT(ocw_course.id) as total_item');
        if(!empty($courses)){
            $response->code = 200;
            $response->status = true;
            $response->courses = $courses;
            $response->count = $count[0]->total_item;
        }else{
            $response->code = 400;
            $response->status = false;
            $response->count = 0;
            $response->message = "<p>You don't have any courses yet</p>";
        }

        echo json_encode($response);
    }

    public function download_transaction(){
        $this->load->library('WkHtmlToPdf');
        $response = get_ajax_response();
        $id = $this->input->get('id');

        $transaction_object = $this->web_app_model->get_single_data('transaction', ['id' => $id]);
        $transaction_item_objects = $this->web_app_model->get_data('transaction_item', ['invoice_number' => $transaction_object->invoice_number]);
        $transaction_object->transaction_items = $transaction_item_objects;
        
        $data['transaction'] = $transaction_object;
        $html_page = $this->load->view('fe_v2/my_account/_download_transaction', $data, true);

        $filename = $this->wkhtmltopdf->single_page_pdf($html_page, str_replace('/', '_', $transaction_object->invoice_number));
        if($filename){
            $response->code = 200;
            $response->status = true;
            $response->data = $filename;
        }else{
            $response->code = 400;
            $response->status = false;
            $response->message = 'Cannot Download Invoice At This Momment, Please Try Again Later';
        }
        echo json_encode($response);
    }

    public function download(){
        $filename = $this->input->get('filename');
        if(!$filename){
            echo 'An Error Has Occured, Please Try Again Later';
            return;
        }

        $filepath = $this->config->item('wkhtmltopdf_upload_path') . $filename;
        if(file_exists($filepath)){
            $this->load->helper('download');
            force_download($filepath, NULL);
        }else{
            echo 'An Error Has Occured, Please Try Again Later';
        }
    }

    public function update_profile(){
        $response = get_ajax_response();

        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('new_password', 'New Password', 'min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_password_check|callback_new_password_check');
        $this->form_validation->set_rules('old_password', 'Old Password', 'min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_old_password_check');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('full_name', 'Fullname', 'required');
        $this->form_validation->set_rules('city_id', 'City', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('dateBirth', 'Date Birth', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->status = false;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $primary_key['id'] = $this->input->post("id",true);

            $user_object = $this->ion_auth->user($primary_key['id'])->row();
            if(!empty($user_object)){
                $update_data['ip_address'] = $this->input->ip_address();
                $update_data['username'] = $user_object->username;
                $password = $this->input->post('new_password',true);
                if($password) $update_data['password'] = $password;
                $update_data['email'] = $this->input->post('email',true);
                $update_data['full_name'] = $this->input->post('full_name',true);
                $update_data['country_id'] = 1;
                $update_data['city_id'] = $this->input->post('city_id',true);
                $update_data['gender'] = $this->input->post('gender',true);
                $update_data['dateBirth'] = $this->input->post('dateBirth',true);
                $update_data['phone_number'] = $this->input->post('phone_number',true);

                $email_object = $this->web_app_model->get_single_data('users', ['email' => $update_data['email']]);
                if(empty($email_object) || $email_object->id == $primary_key['id']){
                    // check user in mooc
                    $sync_response = check_user_mooc($user_object->username);
                    // $sync_response['users'][0]['id'] = 31847;

                    if(!empty($sync_response['users'])){
                        $update_data['mooc_id'] = $sync_response['users'][0]['id'];
                        // update user mooc
                        $sync_data = new stdClass();
                        $sync_data->id = $update_data['mooc_id'];
                        $sync_data->username = $update_data['username'];
                        $sync_data->fullname = $update_data['full_name'];
                        $sync_data->email = $update_data['email'];
                        $sync_data->password = $password;
                        $sync_response = update_user_mooc($sync_data);
                        // var_dump($sync_response);
                        // die();

                        if(is_null($sync_response)){
                        }else{
                            $err_message = isset($sync_response['message']) ? strip_tags($sync_response['message']) : 'Failed To Update User In MOOC';

                            $response->code = 400;
                            $response->status = false;
                            $response->message = $err_message;

                            echo json_encode($response);
                            return false;
                        }
                    }elseif(isset($sync_response['exception']) && !empty($sync_response['exception'])){
                        $err_message = isset($sync_response['debuginfo']) ? $sync_response['debuginfo'] : ((isset($sync_response['message'])) ? $sync_response['message'] : ' Failed To Enroll User');
                        
                        $response->code = 400;
                        $response->status = false;
                        $response->message = $err_message;

                        echo json_encode($response);
                        return;
                    }

                    // check user igracias
                    $sync_response = get_user_igracias($user_object->username);

                    $city = $this->web_app_model->get_single_data('city', ['id'=> $update_data['city_id']]);
                    if($sync_response['status']){
                        // update user igracias
                        $sync_data = new stdClass();
                        $sync_data->username = $update_data['username'];
                        $sync_data->phone_number = $update_data['phone_number'];
                        $sync_data->city = $city->name;
                        $sync_data->email = $update_data['email'];
                        $sync_data->fullname = $update_data['full_name'];
                        $sync_data->birth_date = $update_data['dateBirth'];
                        $sync_data->gender = $update_data['gender'];
                        $sync_response = update_user_igracias($sync_data);

                        if($sync_response['status']){
                            // udpate password igracias
                            $old_password = $this->input->post('old_password', true);
                            if($password && $old_password){
                                $sync_data = new stdClass();
                                $sync_data->username = $user_object->username;
                                $sync_data->old_password = $old_password;
                                $sync_data->new_password = $password;
                                $sync_response = update_password_igracias($sync_data);

                                if($sync_response['status']){
                                }else{
                                    $response->code = 400;
                                    $response->status = false;
                                    $response->message = $sync_response['message'];

                                    echo json_encode($response);
                                    return false;
                                }
                            }
                        }else{
                            $response->code = 400;
                            $response->status = false;
                            $response->message = $sync_response['message'];

                            echo json_encode($response);
                            return;
                        }
                    }else{
                        // create user igracias
                        $sync_data = new stdClass();
                        $sync_data->username = $update_data['username'];
                        $sync_data->password = $password ?: 'Qwerty1@';
                        $sync_data->phone_number = $update_data['phone_number'];
                        $sync_data->city = $city->name;
                        $sync_data->email = $update_data['email'];
                        $sync_data->fullname = $update_data['full_name'];
                        $sync_data->birth_date = $update_data['dateBirth'];
                        $sync_data->gender = $update_data['gender'];
                        $sync_response = create_user_igracias($sync_data);

                        if($sync_response['status']){
                            $update_data['personid'] = $sync_response['personid'];
                        }else{
                            $response->code = 400;
                            $response->status = false;
                            $response->message = $sync_response['message'];

                            echo json_encode($response);
                            return false;
                        }
                    }

                    // update user database
                    if($this->ion_auth->update($primary_key['id'], $update_data)) {
                        $response->message = "Data updated successfully";
                        $response->code = 200;
                        $response->status = true;
                    }else{

                        $response->message = "Failed to update data"; 
                        $response->code = 400;
                        $response->status = false;
                    }
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Email has been used";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function password_check($str){
        $username = $this->ion_auth->user()->row();
        $username = $username->username;
        if(strpos($str, $username) === false) return true;
        $this->form_validation->set_message('password_check', '{field} cannot contain username');
        return false;
    }

    public function new_password_check($str){
        $new_password = $this->input->post('new_password', true);
        $old_password = $this->input->post('old_password', true);
        if($new_password == '' && $old_password != ''){
            $this->form_validation->set_message('new_password_check', '{field} cannot empty when update password');
            return false;
        }
        return true;
    }

    public function old_password_check($str){
        $new_password = $this->input->post('new_password', true);
        $old_password = $this->input->post('old_password', true);
        if($new_password != '' && $old_password == ''){
            $this->form_validation->set_message('old_password_check', '{field} cannot empty when update password');
            return false;
        }
        return true;
    }

    public function get_certificates(){
        $response = get_ajax_response();
        $search = $this->input->get('search');
        $start = $this->input->get('start');
        $limit = $this->input->get('limit');

        $user = $this->ion_auth->user()->row();
        $where = [
            'user_id' => $user->id
        ];
        if(!empty($search)) $where['ocw_course.ocw_name LIKE'] = '%' . $search . '%';

        $grade_objects = $this->web_app_model->get_data_join_list('user_grade', 'ocw_course', '', 'user_grade.ocw_course_id = ocw_course.id', '', $where, $start, $limit, '*', '', 'created_date');
        foreach($grade_objects as &$grade_object){
            $grade_object->course = $this->web_app_model->get_single_data('ocw_course_joined', ['id' => $grade_object->ocw_course_id]);
        }
        $count = $this->web_app_model->get_data_join('user_grade', 'ocw_course', '', 'user_grade.ocw_course_id = ocw_course.id', '', $where, 'COUNT(user_grade.id) as total_item');
        if(!empty($grade_objects)){
            $response->code = 200;
            $response->status = true;
            $response->certificates = $grade_objects;
            $response->count = $count[0]->total_item;
        }else{
            $response->code = 400;
            $response->status = false;
            $response->count = 0;
            $response->message = "You Don't Have Any Certificate";
        }
        echo json_encode($response);
    }
}

/* End of file Controllername.php */