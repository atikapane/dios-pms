<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct() 
	{
		parent::__construct();	
		$this->load->model('Users_Model','m_users');	
		$this->load->model('Group_Model','m_group');						
		$this->load->model('Role_Model','m_role');						
	}
	
	public function index()
	{	
		$data["group"] = $this->m_group->get_group();	
		$this->load->view('users/index.php',$data);
	}

	public function save()
	{
		$data['user_id'] 	= date('mydHis');
		$data['fullname'] 	= $this->input->post('fullname');
		$data['email'] 		= $this->input->post('email');
		$data['username'] 	= "";
		$data['password'] 	= "";
		$data['telp'] 		= $this->input->post('telp');
		$data['address'] 	= $this->input->post('address');

		$db=false;		
		$db = $this->web_app_model->insert_data('users',$data);
		if($db=true){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function update()
	{
		$uid 									= $this->input->post('id');
		$data['fullname'] 						= $this->input->post('fullname');
		$data['email'] 							= $this->input->post('email');
		$data['institution'] 					= $this->input->post('institution');
		$data['directorate'] 					= $this->input->post('directorate');
		$data['structural_functional_position'] = $this->input->post('structural_functional_position');
		$data['structural_academic_position'] 	= $this->input->post('structural_academic_position');
		$data['unit'] 							= $this->input->post('unit');

		$role ['userid']  = $uid;
		$group  = $this->input->post('group');

		$db=false;
		$del['userid'] = $uid;//haous dengan id dirole
		$db = $this->web_app_model->delete_data('roles',$del);//insert to role
		for ($i=0; $i < count($group); $i++) { 
			$role ['userid']  = $uid;
			$role['groupid'] = $group[$i];
			$db = $this->web_app_model->insert_data('roles',$role);
		}
		
		$db = $this->web_app_model->update_data('user',$data,$uid,'id');
		if($db=true){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function delete()
	{
		$data['user_id']= $this->input->post('user_id');
		$db=false;
		$db = $this->web_app_model->delete_data('users',$data);
		if($db=true){
			echo json_encode(array("status" => TRUE));		
		}else{
			echo json_encode(array("status" => TRUE));
		}
	}

	//json data
	public function get_user(){
		$data = $this->m_users->get_user();
		echo json_encode($data);
	}

	public function get_user_byid($id){
		$data = $this->m_users->get_user_byid($id);
		echo json_encode($data);
	}
	
}
