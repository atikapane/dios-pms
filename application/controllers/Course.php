<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Course extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Web_App_Model');
    }

    private function get_slug()
    {
        //last segment is expected to be the slug
        $segment_array = $this->uri->segment_array();
        return end($segment_array);
    }

    private function get_default_page_data()
    {
        $data = array();
        $data["title"] = "Course";
        $data["sub_menu"] = false;
        $data["content"] = 'fe/course_detail/home';
        $data["page"] = "course_home";
        $data['active_page'] = "listCourse";
        $data["course"] = $this->get_course_data($this->get_slug());

        return $data;
    }

    private function load_view($data)
    {
        if (empty($data["course"])) {
            redirect("/"); //TODO : Implement 404 page
        } else {
            $this->load->view('fe/course_detail/index', $data);
        }
    }

    public function view()
    {
        $data["sub_menu"] = false;
        $data["title"] = "Course";
        $data['active_page'] = "Course";
        $current_course = $this->get_course_data($this->get_slug());
        if(!empty($current_course)){
            $data["course"] = $current_course;
            $data["logged_in"] = $this->ion_auth->logged_in();
            $data["user"] = $this->ion_auth->user()->row();
            $data["syllabus"] = $this->Web_App_Model->get_data("ocw_course_duration", "ocw_course_id = $current_course->id", "", "section_id");
            $data['notes'] = $this->get_content_by_type($current_course->id, 'resource');
            $data['videos'] = $this->get_content_by_type($current_course->id, 'hvp');
            $data['assignments'] = $this->get_content_by_type($current_course->id, 'assignment');
            $data['resources'] = $this->get_content_by_type($current_course->id, 'url');
            $data['exams'] = $this->get_content_by_type($current_course->id, 'quiz');
             $this->load->view('fe_v2/course/detail', $data);
        }else{
            redirect("/"); //TODO : Implement 404 page
        }
    }

    public function profile()
    {
        $data = $this->get_default_page_data();
        $data["title"] = "Course Profile";
        $data["content"] = 'fe/course_detail/course_profile';
        $data["page"] = "course_profile";
        $this->load_view($data);
    }

    public function syllabus()
    {
        $data = $this->get_default_page_data();
        $data["title"] = "Course Syllabus";
        $data["content"] = 'fe/course_detail/sylabus';
        $data["page"] = "syllabus";
        //get syllabus data
        $current_course = $data["course"];
        if (!empty($current_course)) {
            $data["syllabus"] = $this->Web_App_Model->get_data("ocw_course_duration", "ocw_course_id = $current_course->id", "", "section_id");
        }
        $this->load_view($data);
    }

    public function notes()
    {
        $data = $this->get_default_page_data();
        $data['title'] = "Course Lecture Notes";
        $data['content'] = 'fe/course_detail/lecture_notes';
        $data['page'] = 'lecture_notes';
        $current_course = $data['course'];
        if (!empty($current_course)) {
            $data['notes'] = $this->get_content_by_type($current_course->id, 'resource');
        }
        $this->load_view($data);
    }

    public function videos()
    {
        $data = $this->get_default_page_data();
        $data['title'] = "Course Video";
        $data['content'] = 'fe/course_detail/course_video';
        $data['page'] = 'course_video';
        $current_course = $data['course'];
        if (!empty($current_course)) {
            $data['videos'] = $this->get_content_by_type($current_course->id, 'hvp');
        }
        $this->load_view($data);
    }

    public function assignments()
    {
        $data = $this->get_default_page_data();
        $data['title'] = "Course Assignments";
        $data['content'] = 'fe/course_detail/assignments';
        $data['page'] = 'assignments';
        $current_course = $data['course'];
        if (!empty($current_course)) {
            $data['assignments'] = $this->get_content_by_type($current_course->id, 'assignment');
        }
        $this->load_view($data);
    }

    public function exam()
    {
        $data = $this->get_default_page_data();
        $data['title'] = "Course Exams";
        $data['content'] = 'fe/course_detail/exam';
        $data['page'] = 'exam';
        $current_course = $data['course'];
        if (!empty($current_course)) {
            $data['exams'] = $this->get_content_by_type($current_course->id, 'quiz');
        }
        $this->load_view($data);
    }
    public function get_content()
    {
        $response = get_ajax_response();
        $content_id = $this->input->get('content_id', true);
        $data = $this->get_content_by_id($content_id);
        if(!empty($data) && $data->content_status == 1){
            $response->code = 200;
            $response->status = true;
            $response->data = $data;
        }else{
            $response->code = 403;
            $response->status = false;
            $response->data = false;
        }
        echo json_encode($response);
    }

    public function resources()
    {
        $data = $this->get_default_page_data();
        $data['title'] = "Course Resources";
        $data['content'] = 'fe/course_detail/related_resources';
        $data['page'] = 'related_resources';
        $current_course = $data['course'];
        if (!empty($current_course)) {
            //get data content
            $where = array(
                'ocw_course_id' => $current_course->id,
                'content_type' => 'url'
            );
            $data['resources'] = $this->Web_App_Model->get_data_and_where('ocw_course_content', $where, 'section_id');
        }
        $this->load_view($data);
    }

    private function get_content_by_type($course_id, $content_type)
    {
        //get data content
        $where = array(
            'ocw_course_id' => $course_id,
            'content_type' => $content_type
        );
        $contents = $this->Web_App_Model->get_data_and_where('ocw_course_content', $where);

        // group content by section name
        $course_content = [];
        foreach ($contents as $content) {
            $course_content[$content->section_name][] = $content;
        }

        return $course_content;
    }


    private function get_content_by_id($content_id)
    {
        //get data content
        $where = array(
            'id' => $content_id
        );
        $contents = $this->Web_App_Model->get_data_and_where('ocw_course_content', $where);
        if(!empty($contents)){
            return $contents[0];
        }
        return false;
    }

    private function get_course_data($slug)
    {
        $where = array('slug' => $slug);
        $course_data = $this->Web_App_Model->get_single_data("ocw_course_joined", $where);
        if (!empty($course_data) && $course_data->status == 3) {
            return $course_data;
        } else {
            return false;
        }
    }

    public function save_feedback()
    {
        $response = get_ajax_response();
        $feedback_id = $this->input->post('feedback_id', true);

        if (!$feedback_id) {
            $data = array(
                'user_id' => $this->input->post('user_id', true),
                'course_id' => $this->input->post('course_id', true),
                'rating' => $this->input->post('rating', true),
                'feedback' => $this->input->post('feedback', true)
            );
            $id = $this->Web_App_Model->insert_data('feedbacks', $data);
        } else {
            $data = array(
                'rating' => $this->input->post('rating', true),
                'feedback' => $this->input->post('feedback', true),
                'created_date' => date("Y-m-d h:i:s"),
            );

            $id = $this->Web_App_Model->update_data('feedbacks', $data, $feedback_id, 'id');
        }

        if (!empty($id)) {
            $response->code = 200;
            $response->status = true;
            $response->message = "Thanks For Your Feedback";
        } else {
            $response->code = 400;
            $response->status = false;
            $response->message = "Failed To Submit Feedback, Please Check Your Feedback.";
        }

        echo json_encode($response);
    }

    public function load_feedback()
    {
        $response = get_ajax_response();

        $id = $this->input->get('course_id', true);
        $clause = "feedbacks.user_id = users.id";
        $where = array('course_id' => $id);
        $order_by = "created_date DESC";
        $feedbacks = $this->Web_App_Model->get_data_join("feedbacks", "users", '', $clause, '', $where, "feedbacks.id, feedbacks.user_id, feedbacks.course_id, feedbacks.rating, feedbacks.feedback, feedbacks.feedback, feedbacks.created_date, users.full_name", '', $order_by);

        for ($x = 1; $x <= 5; $x++) {
            $rating_arr[$x] = 0;
        }
        $overall = 0;

        foreach ($feedbacks as $rating) {
            $rating_arr[$rating->rating]++;
            $overall += $rating->rating;
        }
        $overall = $overall / (count($feedbacks) ?: 1);

        $response->code = 200;
        $response->status = true;
        $response->feedbacks = $feedbacks;
        $response->starsCount = $rating_arr;
        $response->overall = number_format($overall, 1);

        echo json_encode($response);
    }

    public function load_comments(){
        $response = get_ajax_response();

        $id = $this->input->get('course_id', true);
        $clause = 'comments.user_id = users.id';
        $where = array(
            'course_id' => $id,
            'parent_id' => 0
        );
        $order_by = 'created_date DESC';
        $comments = $this->Web_App_Model->get_data_join("comments", "users", '', $clause, '', $where, "comments.id, comments.user_id, comments.course_id, comments.comment, comments.parent_id, comments.created_date, users.full_name", '', $order_by);

        $comments_with_replies = [];
        foreach ($comments as $comment) {
            $where = array(
                'course_id' => $id,
                'parent_id' => $comment->id
            );
            $order_by = 'created_date DESC';
            $comment->replies = $this->Web_App_Model->get_data_join("comments", "users", '', $clause, '', $where, "comments.id, comments.user_id, comments.course_id, comments.comment, comments.parent_id, comments.created_date, users.full_name", '', $order_by);
            $comments_with_replies[] = $comment;
        }

        $response->code = 200;
        $response->status = true;
        $response->comments = $comments_with_replies;

        echo json_encode($response);
    }

    public function save_comment()
    {
        $response = get_ajax_response();
        $data = array(
            'user_id' => $this->input->post('user_id', true),
            'course_id' => $this->input->post('course_id', true),
            'comment' => $this->input->post('comment', true),
            'parent_id' => $this->input->post('parent_id', true)
        );

        $id = $this->Web_App_Model->insert_data('comments', $data);
        if (!empty($id)) {
            $response->code = 200;
            $response->status = true;
            $response->message = "Thanks For Your Comment";
        } else {
            $response->code = 400;
            $response->status = false;
            $response->message = "Failed To Submit Comment, Please Try Again later.";
        }

        echo json_encode($response);
    }

    public function catalog()
    {
        $cat_id = $this->input->get('c');
        if(empty($cat_id)){
            $cat_id = 0;
        }

        // $header["title"] = $body["title"] = "Courses";
        // $header["sub_menu"] = false;
        // $header['active_page'] = "course";
        // $data['category_list'] = $this->Web_App_Model->get_data("vw_ocw_category");
        // $this->load->view('fe_v2/nav_bar',$header);
        // $this->load->view('fe_v2/course/list',$data);
        // $this->load->view('fe_v2/footer');

        $data["sub_menu"] = false;
        $data["title"] = "Course List";
        $data["sub_title"] = "Subtitle";
        $data['active_page'] = "listCourse";
        $data['search'] = $this->input->post('s');
        $data['category_list'] = $this->Web_App_Model->get_data("vw_ocw_category");
        $data['selected_category'] = $this->Web_App_Model->get_single_data("vw_ocw_category",array("ocw_category_id"=>$cat_id));
        $this->load->view('fe_v2/course/list', $data);
    }

    public function get_program_study()
    {
        $response = get_ajax_response();

        $faculty_id = $this->input->post("faculty_id", true);

        if(!empty($faculty_id)){
            $query_data['faculty_id'] = $faculty_id;
            $program_study_list = $this->Web_App_Model->get_data("vw_ocw_category",$query_data);
        }else{
            $program_study_list = $this->Web_App_Model->get_data("vw_ocw_category");
        }

        $response->code = 200;
        $response->data = $program_study_list;
        $response->status = true;
        $response->message = "Get data success";

        json_response($response);
    }

    public function load_course(){
        $response = init_response();
        $search = $this->input->post('search',true);
        $start = $this->input->post('start',true);
        $limit = $this->input->post('limit',true);
        $faculty = $this->input->post('faculty_id',true);
        $program = $this->input->post('program_id',true);

        $extra_where = array('status' => 3);
        if(!empty($faculty)){
            $extra_where["faculty_id"]=$faculty;
        }

        if(!empty($program)){
            $extra_where["study_program_id"]=$program;
        }

        $data_news = $this->Web_App_Model->get_data_list("ocw_course_joined", $search, "ocw_name", $start, $limit, "created_at", $extra_where);

        $count_data_news = $this->Web_App_Model->get_data("ocw_course_joined",$extra_where);

        if($data_news){
            $response->status = true;
            $response->data = $data_news;
            $response->num_of_rows = count($count_data_news);
        }else{
            $response->status = false;
            $response->data = array();
            $response->num_of_rows = count($count_data_news);
        }
        json_response($response);
    }

    public function package(){
        $course_slug = $this->uri->segment(3);
        $package_objects = $this->web_app_model->get_data_join('packet_ocw_course as packet', 'ocw_course as course', '', 'packet.course_id = course.id', '', ['course.slug' => $course_slug], 'packet.id as packet_id, packet.*, course.*');

        if(!empty($package_objects)){
            $package_objects[0]->courses = $this->web_app_model->get_data_join('reference_multiple_course as reference', 'ocw_course_joined as course','','reference.course_target_id = course.id', '', ['reference.package_ocw_course_id' => $package_objects[0]->packet_id]);
            $package_objects[0]->faq = $this->web_app_model->get_data('packet_ocw_course_faq', ['packet_ocw_course_id' => $package_objects[0]->packet_id]);
        }
        $data['package'] = !empty($package_objects) ? $package_objects[0] : '';
        $data["logged_in"] = $this->ion_auth->logged_in();
        $data["sub_menu"] = false;
        $data["title"] = "Course Package";
        $data["sub_title"] = "";
        $data['active_page'] = "";
        $this->load->view('fe_v2/course/package', $data);
    }

    public function certificate(){
        $data["logged_in"] = $this->ion_auth->logged_in();
        $data["sub_menu"] = false;
        $data["title"] = "Certificate Verification";
        $data["sub_title"] = "";
        $data['active_page'] = "";
        $this->load->view('fe_v2/course/certificate', $data);
    }

}
