<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
    }

    public function index()
    {
        $data['agreement'] = $this->web_app_model->get_single_data('landing_agreements', ['name' => 'privacy']);
        $data["sub_menu"] = false;
        $data["title"] = "Privacy Policy";
        $data['active_page'] = "privacy";
        $this->load->view('fe_v2/policy/privacy', $data);
    }

}
/* End of file Controllername.php */
