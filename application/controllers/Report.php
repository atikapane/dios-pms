<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	function __construct() 
	{
		parent::__construct();							
	}


	public function index()
	{		
		$this->load->view('dashboard/index.php');
	}


	public function course_development(){
		$this->load->view('report/course_development');
	}

	public function course_development_data(){
        $response = init_response();
        $response->status = true;
        $data = array();
        $data[] = array("no"=>1,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Informatika","status_review"=>"OK");
        $data[] = array("no"=>2,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Elektronika","status_review"=>"Tidak OK");
        $response->data = $data;
        json_response($response);
    }

    public function course_development_data_prodi(){
        $response = init_response();
        $response->status = true;
        $data = array();
        $data[] = array("no"=>1,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Elektronika","prodi"=>"Teknik Elektronika","status_review"=>"OK");
        $data[] = array("no"=>2,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Elektronika","prodi"=>"Teknik Telekomunikasi","status_review"=>"Tidak OK");
        $response->data = $data;
        json_response($response);
    }

    public function course_development_data_mk(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"jajang",8=>"rahmat",9=>"OK",10=>"OK",11=>"12 Oktober 2019");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"herman",8=>"neneng",9=>"Tidak OK",10=>"OK",11=>"12 Oktober 2019");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Elektronika",4=>"Teknik Telekomunikasi",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"herman",8=>"neneng",9=>"Tidak OK",10=>"OK",11=>"12 Oktober 2019");
        $data[] = array(0=>2,1=>2019,2=>1,3=>"Fakultas Teknik Elektronika",4=>"Teknik Telekomunikasi",5=>"TT1234",6=>"Jaringan Komputer",7=>"made",8=>"siti",9=>"Tidak OK",10=>"OK",11=>"13 Oktober 2019");
        $data[] = array(0=>2,1=>2019,2=>1,3=>"Fakultas Teknik Elektronika",4=>"Teknik Telekomunikasi",5=>"TT1234",6=>"Jaringan Komputer",7=>"bayu",8=>"asep",9=>"Tidak OK",10=>"OK",11=>"13 Oktober 2019");
        $response['data'] = $data;
        json_response($response);
    }

	public function monitoring_course_development(){
		$this->load->view('report/monitoring_course_development');
	}

    public function monitoring_development_data_mk(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>7,8=>"PERSAMAAN DASAR AKUTANSI (100%)",9=>"70%",10=>"50%",11=>14,12=>7,13=>49,14=>7,15=>7,16=>7,17=>"90%");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>7,8=>"JURNAL UMUM (80%)",9=>"70%",10=>"50%",11=>14,12=>7,13=>49,14=>7,15=>7,16=>7,17=>"90%");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>7,8=>"SIKLUS AKUTANSI (80%)",9=>"70%",10=>"50%",11=>14,12=>7,13=>49,14=>7,15=>7,16=>7,17=>"90%");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>7,8=>"JURNAL PENYESUAIAN (70%)",9=>"70%",10=>"50%",11=>14,12=>7,13=>49,14=>7,15=>7,16=>7,17=>"90%");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>7,8=>"SIKLUS AKUTANSI PERUSAHAAN DAGANG (20%)",9=>"70%",10=>"50%",11=>14,12=>7,13=>49,14=>7,15=>7,16=>7,17=>"90%");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>7,8=>"JURNAL KHUSUS (0%)",9=>"70%",10=>"50%",11=>14,12=>7,13=>49,14=>7,15=>7,16=>7,17=>"90%");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>7,8=>"PERSEDIAAN BARANG DAGANG (0%)",9=>"70%",10=>"50%",11=>14,12=>7,13=>49,14=>7,15=>7,16=>7,17=>"90%");
        $response['data'] = $data;
        json_response($response);
    }


    public function monitoring_development_data_faculty(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Prodi A (9%)",5=>"Prodi C (90%)",6=>"65%");
        $response['data'] = $data;
        json_response($response);
    }

     public function monitoring_development_data_prodi(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();
        $data = array();
         $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"MK A (9%)",6=>"MK B (90%)",7=>"65%");
       
        $response['data'] = $data;
        json_response($response);
    }

	public function course_change(){
		$this->load->view('report/course_change');
	}

    public function course_change_mk_data(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();

        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"IF-32-01",8=>20,9=>"Samsudin",10=>"6",11=>"File(2)",12=>"Quiz(1)",13=>20);
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"IF-32-01",8=>20,9=>"Samsudin",10=>"6",11=>"label(4)",12=>"Forum(1)",13=>20);
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"IF-32-01",8=>20,9=>"Samsudin",10=>"6",11=>"Page(6)",12=>"Assignment(2)",13=>20);
       
        $response['data'] = $data;
        json_response($response);
    }

	public function instructor_attendance(){
		$this->load->view('report/instructor_attendance');
	}

    public function instructor_attendance_data(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();

        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"IF-32-01",8=>20,9=>"Samsudin");
        $response['data'] = $data;
        json_response($response);
    }


    public function instructor_attendance_detail_data(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();

        $data[] = array(0=>1,1=>"PERSAMAAN DASAR AKUTANSI",2=>"",3=>"1 Okt - 6 Okt 2019",4=>20,5=>5,6=>20,7=>"Hadir");
        $data[] = array(0=>1,1=>"JURNAL UMUM",2=>"",3=>"1 Okt - 6 Okt 2019",4=>30,5=>3,6=>20,7=>"Hadir");
        $data[] = array(0=>1,1=>"SIKLUS AKUNTANSI",2=>"",3=>"1 Okt - 6 Okt 2019",4=>10,5=>0,6=>20,7=>"Hadir");
        $data[] = array(0=>1,1=>"JURNAL PENYESUAIAN",2=>"",3=>"1 Okt - 6 Okt 2019",4=>15,5=>4,6=>20,7=>"Hadir");
        $data[] = array(0=>1,1=>"SIKLUS AKUNTANSI PERUSAHAAN DAGANG",2=>"",3=>"1 Okt - 6 Okt 2019",4=>16,5=>0,6=>0,7=>"Tidak Hadir");
        $data[] = array(0=>1,1=>"JURNAL KHUSUS",2=>"",3=>"1 Okt - 6 Okt 2019",4=>33,5=>10,6=>0,7=>"Hadir");
        $data[] = array(0=>1,1=>"PERSEDIAAN DAGANG BARANG",2=>"",3=>"1 Okt - 6 Okt 2019",4=>40,5=>15,6=>20,7=>"Hadir");
       
       
        $response['data'] = $data;
        json_response($response);
    }


	public function student_attendance(){
		$this->load->view('report/student_attendance');
	}

}