<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class BaseAPIController extends REST_Controller {

	function __construct() 
	{
		parent::__construct();	
	}

    protected function get_response(){
        $template = new stdClass();
        $template->status = false;
        $template->data = null;
        $template->message = "";
        $template->error_code = "";

        return $template;
    }

    protected function has_valid_auth(){
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTHORIZATION::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                return $decodedToken;
            }
        }
        return false;
    }

    protected function has_valid_key(){
        $headers = $this->input->request_headers();
        if (array_key_exists('celoe-api-key', $headers) && !empty($headers['celoe-api-key'])) {
            if($headers['celoe-api-key'] == $this->config->item('celoe-api-key')){
                return true;
            }
        }
        return false;
    }

    public function auth_check()
    {
        $cek_valid_key = $this->has_valid_key();
        $cek_valid = $this->has_valid_auth();
        if(!$cek_valid || !$cek_valid_key){
            $response = $this->get_response();
            $response->status = false;
            $response->message = "Unauthorized";
            $response->error_code = "401";

            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            json_response($response);
            die();
        }
    }

    public function credentials_check()
    {
        $cek_valid_key = $this->has_valid_key();
        if(!$cek_valid_key){
            $response = $this->get_response();
            $response->status = false;
            $response->message = "Unauthorized";
            $response->error_code = "401";

            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            json_response($response);
            die();
        }
    }

      protected function get_mobile_token_request(){
        $headers = $this->input->request_headers();
        if (array_key_exists('X-Mobile-Token', $headers) && !empty($headers['X-Mobile-Token'])) {
           return $headers['X-Mobile-Token'];
        }
        return false;
    }

    public function generate_token($user_id)
    {
        $this->load->model("Mobile_Token_Model");
        $new_token = $this->Mobile_Token_Model->generate($user_id);
        return $new_token;
    }

    public function validate_token($token)
    {
        $this->load->model("Mobile_Token_Model");
        $result = $this->Mobile_Token_Model->validate($token);
        return $result;
    }
	
}
