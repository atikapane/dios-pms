<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");


class Billing extends BaseAPIController {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ShoppingCart');
        $this->load->model('Web_App_Model');
        $this->load->model('Transaction_Model');
        $this->load->helper('log_api');
        $this->load->helper('billing');
    }

    public function transaction_get(){
        $this->credentials_check();
        $response = init_response();
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        $override_tx = $this->input->get('override',true);
        $override_tx = $override_tx == 1;//convert to boolean

        if(!empty($token_user)){
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $cart_data = $this->shoppingcart->get($user_id);
                if (!empty($cart_data)) {
                    
                    $should_make_tx = true;
                    $existing_tx =  $this->Transaction_Model->get_by_user($user_id,Transaction_Model::STATUS_WAITING);

                    if(!empty($existing_tx) && !$override_tx){
                        //ask user to override
                        $response->error_code = 407;
                        $response->data = $existing_tx[0]->invoice_number;
                        $response->message = "Pending transaction exists";
    
                        log_api("API_PENDING_TX_EXISTS",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);

                        $should_make_tx = false;
                    }else if(!empty($existing_tx)){
                        // expiring bni va
                        $bni_ecoll_active = $this->config->item("bni_ecoll_active");
                        if($bni_ecoll_active === true){
                            //use real api
                            $bni_conf['client_id'] = $this->config->item("bni_ecoll_client_id");
                            $bni_conf['secret_key'] = $this->config->item("bni_ecoll_secret_key");
                            $bni_conf['url'] = $this->config->item("bni_ecoll_url");
                            $this->load->library('BNIecoll',$bni_conf);
                            
                            $va_response = $this->bniecoll->delete_va($existing_tx[0]->invoice_number,$existing_tx[0]->total_price,$existing_tx[0]->user_full_name,$existing_tx[0]->user_email,$existing_tx[0]->user_phone,date('Y-m-d H:i:s', strtotime("now")));
                            
                            $va_generation_success = $va_response->success;
                            if($va_generation_success && is_array($va_response->data) && array_key_exists("virtual_account", $va_response->data)){
                                $generated_va = $va_response->data["virtual_account"];
                            }else{
                                //TODO : Log external api
                            }
                        }
                        //override tx
                        $this->Transaction_Model->set_tx_status($existing_tx[0]->invoice_number,Transaction_Model::STATUS_CANCELLED);
                    }

                    //make tx
                if($should_make_tx){
                    //prepare data
                    $transaction_items = array();
                    $invoice_number = generate_invoice_number($user_id);
                    $invoice_expiry = date('Y-m-d H:i:s', strtotime("+1 days"));
                    $total_price = 0;

                    //build transaction item
                    foreach($cart_data as $cart_item){
                        $transaction_item = array();
                        $transaction_item['invoice_number'] = $invoice_number;
                        $transaction_item['course_id'] = $cart_item->course_id;
                        $transaction_item['course_title'] = $cart_item->ocw_name;
                        $transaction_item['price'] = $cart_item->price;
                        $transaction_item['course_description'] = $cart_item->description;
                        $transaction_item['start_date'] = $cart_item->start_date;
                        $transaction_item['end_date'] = $cart_item->end_date;

                        $total_price +=  $cart_item->price;
                        $transaction_items[] = $transaction_item;
                    }

                    //build transaction data
                    $transaction_data = array();
                    $transaction_data['user_id'] = $user_id;
                    $transaction_data['username'] = $token_user->username;
                    $transaction_data['user_full_name'] = $token_user->full_name;
                    $transaction_data['user_email'] = $token_user->email;
                    $transaction_data['user_gender'] = $token_user->gender;
                    $transaction_data['user_birth_date'] = $token_user->dateBirth;
                    $transaction_data['user_phone'] = $token_user->phone_number;
                    $transaction_data['expired_at'] = $invoice_expiry;
                    $transaction_data['total_price'] = $total_price;
                    $transaction_data['invoice_number'] = $invoice_number;

                        
                    $city = $this->web_app_model->get_single_data("city",array("id"=>$token_user->city_id));
                    if(!empty($city)){
                        $transaction_data['user_city'] = $city->name;
                    }else{
                        $transaction_data['user_city'] = "";
                    }

                    $country = $this->web_app_model->get_single_data("country",array("id"=>$token_user->country_id));
                    if(!empty($country)){
                        $transaction_data['user_country'] = $country->name;
                    }else{
                        $transaction_data['user_country'] = "";
                    }

                    //generate va number from BNI
                    $generated_va = "";
                    $va_generation_success = false;

                    $bni_ecoll_active = $this->config->item("bni_ecoll_active");
                    if($bni_ecoll_active === true){
                        //use real api
                        $bni_conf['client_id'] = $this->config->item("bni_ecoll_client_id");
                        $bni_conf['secret_key'] = $this->config->item("bni_ecoll_secret_key");
                        $bni_conf['url'] = $this->config->item("bni_ecoll_url");
                        $this->load->library('BNIecoll',$bni_conf);
                        
                        $va_response = $this->bniecoll->generate_va($invoice_number,$total_price,$token_user->full_name,$token_user->email,$token_user->phone_number,$invoice_expiry);
                        
                        $va_generation_success = $va_response->success;
                        if($va_generation_success && is_array($va_response->data) && array_key_exists("virtual_account", $va_response->data)){
                            $generated_va = $va_response->data["virtual_account"];
                        }else{
                            //TODO : Log external api
                        }
                    }else{
                        //use dummy va
                        $va_generation_success = true;
                        $generated_va = "DUMMY_".time();
                    }
                    
                    if($va_generation_success){
                        $transaction_data['bni_va_number'] = $generated_va;
                        $this->Transaction_Model->create($transaction_data,$transaction_items);

                        //delete cart
                        $this->shoppingcart->remove_all($user_id);

                        $response->status = true;
                        $response->data = $invoice_number;
                        $response->message = "Transaction successfully created";
    
                        log_api("API_TX_CREATE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }else{
                        $response->error_code = 500;
                        $response->message = "We cannot process your transaction request right now, please try again later";
                        $response->data = array();
    
                        log_api("API_TX_CREATE_FAILED",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                }

                } else {
                    $response->status = false;
                    $response->error_code = 400;
                    $response->message = "You don't have any item in your cart";
                    log_api("API_TX_CREATE_FAILED",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_TX_CREATE_FAILED",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }else{
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_TX_CREATE_FAILED",$response,$token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }

    }
}
