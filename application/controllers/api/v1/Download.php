<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class Download extends BaseAPIController{

    function __construct() 
    {
        parent::__construct();
        $this->load->helper('log_api');
    }

    public function index_get()
    {
        $response = get_ajax_response();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");

        if(empty($start)){
            $start = 0;
        }

        if(empty($limit)){
            $limit = 10000;
        }

        $response->status = true;
        $response->data = $this->web_app_model->get_data_list("landing_download", "", "", $start, $limit, "id", "file_status = 1");
        foreach ($response->data as $value) {
            $value->file_path = base_url().$value->file_path;
        }
     
        log_api("API_DOWNLOAD_LIST",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }
    
}
