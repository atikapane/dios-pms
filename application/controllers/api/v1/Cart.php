<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class Cart extends BaseAPIController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('ShoppingCart');
        $this->load->model('Web_App_Model');
        $this->load->helper('log_api');
        $this->load->model('Course_Model');
    }

    public function detail_get()
    {
        $this->credentials_check();
        $response = init_response();
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        if (!empty($token_user)) {
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $cart_data = $this->shoppingcart->get($user_id);
                if (!empty($cart_data)) {
                    foreach($cart_data as &$course){
                        $course->banner_image = base_url() . $course->banner_image;
                        $course->lecturer_photo = base_url() . $course->lecturer_photo;
                    }

                    $response->data = $cart_data;
                    $response->status = true;
                    $response->error_code = 0;
                    $response->message = "Get Cart data success";

                    log_api("API_CART_LIST", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->status = false;
                    $response->error_code = 400;
                    $response->message = "Cart data not found";
                    log_api("API_CART_LIST", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_CART_LIST", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_CART_LIST", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }

    public function create_post()
    {
        $this->credentials_check();

        $response = init_response();

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        $this->form_validation->set_rules('course_id', 'Course Id', 'required');

        if ($this->form_validation->run() == FALSE) {

            $response->status = false;
            $response->message = "Message error: " . validation_errors();
            $response->error_code = 400;

            log_api("API_CART_CREATE", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            if (!empty($token_user)) {
                $user_id = $token_user->id;

                $course_id = $this->input->post('course_id', true);
                $course = $this->Course_Model->get_valid_course($course_id);

                if (!empty($course)) {
                    $transaction_objects = $this->web_app_model->get_data_join('transaction', 'transaction_item', '', 'transaction.invoice_number = transaction_item.invoice_number', '', ['transaction.user_id' => $user_id, 'transaction.status' => 2, 'transaction_item.course_id' => $course_id]);
                    if(empty($transaction_objects)) {
                        $where = array(
                            'user_id' => $user_id,
                            'course_id' => $this->input->post('course_id', true)
                        );
                        $check_cart = $this->Web_App_Model->get_data_and_where('user_cart', $where);
                        if (!$check_cart) {
                            $data = array(
                                'user_id' => $user_id,
                                'course_id' => $this->input->post('course_id', true)
                            );
                            $insert = $this->shoppingcart->add($this->input->post('course_id', true), $user_id);
                            if ($insert) {
                                $response->status = true;
                                $response->message = "Course has been added to your cart";
                                $response->error_code = 200;
                            } else {
                                $response->status = false;
                                $response->message = "Fail to add this course to your cart";
                                $response->error_code = 500;
                            }

                            log_api("API_CART_CREATE", $response, $token_user);
                            $this->response($response, REST_Controller::HTTP_OK);
                        } else {
                            $response->status = false;
                            $response->message = "This course already exist in your cart";
                            $response->error_code = 400;

                            log_api("API_CART_CREATE", $response, $token_user);
                            $this->response($response, REST_Controller::HTTP_OK);
                        }
                    } else {
                        $response->status = false;
                        $response->message = "You already bought this course";
                        $response->error_code = 500;

                        log_api("API_CART_CREATE", $response, $token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                } else {
                    $response->error_code = 404;
                    $response->message = "This course is not available for purchase now";
                    // $response->ww = $this->db->last_query();
                    $response->status = false;

                    log_api("API_CART_CREATE", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "Invalid token";
                $response->error_code = 403;

                log_api("API_CART_CREATE", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function delete_get()
    {
        $this->credentials_check();
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        $response = init_response();
        if (!empty($token_user)) {
            // $user_id = $token_user->id;

            $get_params = $this->input->get();
            if (empty($get_params)) { //set default param so the form validation triggered
                $get_params = array("id" => "");
            }
            $this->form_validation->set_data($get_params);
            $this->form_validation->set_rules('id', 'Id', 'required');

            if ($this->form_validation->run() == FALSE) {
                $response->status = false;
                $response->message = "Message error: " . validation_errors();
                $response->error_code = 400;

                log_api("API_CART_DELETE", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            } else {
                $cart_id = $this->input->get("id", true);
                if (!empty($cart_id)) {
                    $query_data['id'] = $cart_id;

                    $check_cart = $this->web_app_model->get_single_data("user_cart", $query_data);
                    if (!empty($check_cart)) {

                        $delete = $this->shoppingcart->remove_by_id($cart_id);

                        $response->status = true;
                        $response->message = "Course has been removed from your cart";
                        $response->error_code = 200;

                        log_api("API_CART_DELETE", $response, $token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    } else {
                        $response->status = false;
                        $response->message = "Unable to remove course from cart";
                        $response->error_code = 400;

                        log_api("API_CART_DELETE", $response, $token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                } else {
                    $response->status = false;
                    $response->message = "Id is required";
                    $response->error_code = 400;

                    log_api("API_CART_DELETE", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_CART_DELETE", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }
}
