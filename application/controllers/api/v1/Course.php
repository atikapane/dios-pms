<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class Course extends BaseAPIController
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('log_api');
    }

    public function index_get()
    {
        $response = get_ajax_response();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");
        $category_id = $this->input->get("category_id");
        $search = $this->input->get("search");

        $extra_where = Array('status' => 3);

        if (!empty($category_id)) {
            $extra_where["faculty_id"] = $category_id;
        }

        if (empty($start)) {
            $start = 0;
        }

        if (empty($limit)) {
            $limit = 10;
        }

        $response->status = true;
        $response->data = $this->web_app_model->get_data_list("ocw_course_joined", $search, "ocw_name", $start, $limit, "id", $extra_where);
        foreach ($response->data as $value) {
            $value->banner_image = base_url() . $value->banner_image;
            $value->lecturer_photo = base_url() . $value->lecturer_photo;
        }

        log_api("API_COURSE_LIST",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function lms_get()
    {
        $response = get_ajax_response();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");
        $category_id = $this->input->get("category_id");
        $search = $this->input->get("search");

        $extra_where = Array('status' => 3);

        if (!empty($category_id)) {
            $extra_where["faculty_id"] = $category_id;
        }

        if (empty($start)) {
            $start = 0;
        }

        if (empty($limit)) {
            $limit = 10;
        }

        $response->status = true;
        $response->data = $this->web_app_model->get_data_list("ocw_course_joined", $search, "ocw_name", $start, $limit, "id", $extra_where);
        foreach ($response->data as $value) {
            $value->banner_image = base_url() . $value->banner_image;
            $value->lecturer_photo = base_url() . $value->lecturer_photo;
        }

        log_api("API_COURSE_LMS",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }
    
    private function get_content_by_type($course_id, $content_type)
    {
        //get data content
        $where = array(
            'ocw_course_id' => $course_id,
            'content_type' => $content_type
        );
        $contents = $this->web_app_model->get_data_and_where('ocw_course_content', $where);

        //build section array
        $sections = [];
        foreach ($contents as $content) {
            $section = new stdClass();
            $section->section_id = $content->section_id;
            $section->section_name = $content->section_name;
            $section->contents = [];

            $is_section_added = false;
            foreach ($sections as $single_section) {
                if($single_section->section_id == $section->section_id){
                    $is_section_added = true;
                    break;
                }
            }

            if(!$is_section_added){
                $sections[] = $section;
            }
        }
        //build section content
        foreach($sections as $section){
            foreach($contents as $content){
                if($content->section_id == $section->section_id){
                    $section->contents[] = $content;
                }
            }
        }

        return $sections;
    }

    public function detail_get()
    {
        $response = init_response();

        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $response->status = false;
            $response->error_code = 400;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));

            log_api("API_COURSE_DETAIL",$response);
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $id = $this->input->get("id", true);
            if (!empty($id)) {
                $query_data['id'] = $id;
                $course_detail = $this->web_app_model->get_single_data('ocw_course_joined', $query_data);
                if(!empty($course_detail)){
                    if($course_detail->is_multiple == 0){
                        $course_content["resource"] = $this->get_content_by_type($id,"resource");
                        $course_content["hvp"] = $this->get_content_by_type($id,"hvp");
                        $course_content["assignment"] = $this->get_content_by_type($id,"assignment");
                        $course_content["quiz"] = $this->get_content_by_type($id,"quiz");
                        $course_content["url"] = $this->get_content_by_type($id,"url");
                       
        
                        //get syllabus 
                        $syllabus = $this->web_app_model->get_data("ocw_course_duration","ocw_course_id = $id","","section_id");
                        $course_content["syllabus"] = $syllabus;
                        
                        //get feedbacks
                        $feedbacks = $this->web_app_model->get_data_join("feedbacks", "users", '', "feedbacks.user_id = users.id", '', array('course_id' => $id), "feedbacks.id, feedbacks.user_id, feedbacks.course_id, feedbacks.rating, feedbacks.feedback, feedbacks.feedback, feedbacks.created_date, users.full_name", '', "created_date DESC");
    
                        //calculate feedback stats
                        $feedback_stats = new stdClass();
                        $feedback_stats->star_5 = 0;
                        $feedback_stats->star_4 = 0;
                        $feedback_stats->star_3 = 0;
                        $feedback_stats->star_2 = 0;
                        $feedback_stats->star_1 = 0;
                        $feedback_stats->average = 0;
    
                        if(count($feedbacks) > 0){
                            foreach ($feedbacks as $feedback) {
                                $rating = intval($feedback->rating);
                                $feedback_stats->average += $rating;
                                switch ($rating) {
                                    case 1:
                                        $feedback_stats->star_1  += 1;
                                        break;
                                    case 2:
                                        $feedback_stats->star_2  += 1;
                                        break;
                                    case 3:
                                        $feedback_stats->star_3  += 1;
                                        break;
                                    case 4:
                                        $feedback_stats->star_4  += 1;
                                        break;
                                    case 5:
                                        $feedback_stats->star_5  += 1;
                                        break;
                                }
                            }
                            $feedback_stats->average = $feedback_stats->average / count($feedbacks);
                        }
    
                        $data = array(
                            'detail' => $course_detail,
                            'feedback' => $feedbacks,
                            'feedback_stats' => $feedback_stats,
                            'content' => $course_content
                        );
    
                        $response->data = $data;
                        $response->status = true;
                        $response->error_code = 200;
                        $response->message = "Get data success";
    
                        log_api("API_COURSE_DETAIL",$response);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }else{
                        $package_objects = $this->web_app_model->get_data_join('packet_ocw_course as packet', 'ocw_course as course', '', 'packet.course_id = course.id', '', ['course.id' => $id], 'packet.id as packet_id, packet.*, course.*');

                        if(!empty($package_objects)){
                            $package_objects[0]->description = strip_tags($package_objects[0]->description);
                            $package_objects[0]->banner_image = base_url() . $package_objects[0]->banner_image;
                            $package_objects[0]->courses = $this->web_app_model->get_data_join('reference_multiple_course as reference', 'ocw_course_joined as course','','reference.course_target_id = course.id', '', ['reference.package_ocw_course_id' => $package_objects[0]->packet_id]);
                            $package_objects[0]->faq = $this->web_app_model->get_data('packet_ocw_course_faq', ['packet_ocw_course_id' => $package_objects[0]->packet_id]);

                            foreach($package_objects[0]->courses as &$course){
                                $course->banner_image = base_url() . $course->banner_image;
                                $course->description = strip_tags($course->description);
                            }

                            $response->data = $package_objects;
                            $response->status = true;
                            $response->error_code = 200;
                            $response->message = "Get data success";
        
                            log_api("API_COURSE_DETAIL",$response);
                            $this->response($response, REST_Controller::HTTP_OK);
                        }else{
                            $response->status = false;
                            $response->error_code = 400;
                            $response->message = "Data not found";

                            log_api("API_COURSE_DETAIL",$response);
                            $this->response($response, REST_Controller::HTTP_OK);
                        }
                    }
                } else {
                    $response->status = false;
                    $response->error_code = 400;
                    $response->message = "Data not found";

                    log_api("API_COURSE_DETAIL",$response);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "Id is required";
                $response->error_code = 400;

                log_api("API_COURSE_DETAIL",$response);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }
        
    }
}
