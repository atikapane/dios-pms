<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class MyTransaction extends BaseAPIController
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('log_api');
        $this->load->library('WkHtmlToPdf');
        $this->load->model('Web_app_model');
    }

    function index_get()
    {
        $this->credentials_check();
        $response = init_response();
        $search = $this->input->get("search");
        $date_range_start = $this->input->get("date_start");
        $date_range_end = $this->input->get("date_end");
        $status = $this->input->get("status");
        $start = $this->input->get("start");
        $limit = $this->input->get("limit");

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        if (!empty($token_user)) {
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $where = array("user_id" => $user_id);

                if (empty($search)) {
                    $search = "";
                }

                if (empty($start)) {
                    $start = 0;
                }

                if (empty($limit)) {
                    $limit = 10;
                }

                $transaction_objects = $this->web_app_model->get_data_list_with_date("transaction", $search, $user_id, $start, $limit, $date_range_start, $date_range_end, $status, "id", $where);
                $response->message = "Success get transaction";
                $response->status = true;
                foreach ($transaction_objects as &$transaction_object) {
                    $banner_image_objects = $this->web_app_model->get_data_join('transaction_item', 'ocw_course', '', 'transaction_item.course_id = ocw_course.id', '', ['transaction_item.invoice_number' => $transaction_object->invoice_number], 'ocw_course.banner_image');
                    $course_items = $this->web_app_model->get_data_join('transaction_item', 'ocw_course', '', 'transaction_item.course_id = ocw_course.id', '', ['transaction_item.invoice_number' => $transaction_object->invoice_number], 'ocw_course.ocw_name');
                    if (!empty($banner_image_objects)) {
                        $transaction_object->banner_image = base_url() . $banner_image_objects[0]->banner_image;
                    }

                    if  (!empty($course_items)){
                        foreach($course_items as &$course){
                            $course->banner_image = base_url() . $course->banner_image;
                            $course->lecturer_photo = base_url() . $course->lecturer_photo;
                        }
                        $transaction_object->transaction_items = $course_items;
                    }
                }
                $response->data = $transaction_objects;
                log_api("API_MY_TRANSACTION", $response);
                $this->response($response, REST_Controller::HTTP_OK);
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_MY_TRANSACTION_FAILED", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_MY_TRANSACTION_FAILED", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }

    function detail_get()
    {
        $this->credentials_check();
        $response = init_response();

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        $invoice_number = $this->input->get('invoice_number');

        if (!empty($token_user)) {
            $user_id = $token_user->id;
            if (!empty($user_id)) {

                $transaction_item_objects = $this->web_app_model->get_data_join('transaction_item', 'ocw_course', '', 'transaction_item.course_id = ocw_course.id', '', ['transaction_item.invoice_number' => $invoice_number]);
                foreach ($transaction_item_objects as &$transaction_item_object) {
                    $transaction_item_object->course_description = strip_tags($transaction_item_object->course_description);
                }
                $response->message = "Success get transaction";
                $response->status = true;
                $response->data = $transaction_item_objects;
                log_api("API_MY_TRANSACTION", $response);
                $this->response($response, REST_Controller::HTTP_OK);
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_MY_TRANSACTION_FAILED", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_MY_TRANSACTION_FAILED", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }

    public function generate_get()
    {

        $this->credentials_check();
        $response = init_response();

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        $id = $this->input->get('id');

        if (!empty($token_user)) {
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $transaction_object = $this->web_app_model->get_single_data('transaction', ['id' => $id]);
                $transaction_item_objects = $this->web_app_model->get_data('transaction_item', ['invoice_number' => $transaction_object->invoice_number]);
                $transaction_object->transaction_items = $transaction_item_objects;

                $data['transaction'] = $transaction_object;
                $html_page = $this->load->view('fe_v2/my_account/_download_transaction', $data, true);

                $filename = $this->wkhtmltopdf->single_page_pdf($html_page, str_replace('/', '_', $transaction_object->invoice_number));
                if ($filename) {
                    $response->code = 200;
                    $response->status = true;
                    $response->data = $filename;
                    log_api("API_MY_TRANSACTION", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->code = 200;
                    $response->status = false;
                    $response->message = 'Cannot Download Invoice At This Momment, Please Try Again Later';
                    log_api("API_MY_TRANSACTION", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
                echo json_encode($response);
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_MY_TRANSACTION_FAILED", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_MY_TRANSACTION_FAILED", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
        echo json_encode($response);
    }

    public function download_get()
    {
        $filename = $this->input->get('filename');
        if (!$filename) {
            echo 'An Error Has Occured, Please Try Again Later';
            return;
        }

        $filepath = $this->config->item('wkhtmltopdf_upload_path') . $filename;
        if (file_exists($filepath)) {
            $this->load->helper('download');
            force_download($filepath, NULL);
        } else {
            echo 'An Error Has Occured, Please Try Again Later';
        }
    }
}
