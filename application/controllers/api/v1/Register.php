<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class Register extends BaseAPIController {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Sync');
        $this->load->helper('sync_mooc');
        $this->load->helper('sync_igracias');
        $this->load->helper('log_api');
    }

	public function index_post()
	{
        $this->credentials_check();

        $response = init_response();

        $oauth_provider = $this->input->post("oauth_provider",true);
        $oauth_provider = trim($oauth_provider);
        $oauth_id = $this->input->post("oauth_id",true);
        $oauth_id = trim($oauth_id);
        $email = $this->input->post("email",true);
        $email = trim($email);
        $password = $this->input->post("password");
        $name = $this->input->post("fullname", true);
        $name = trim($name);
        $username = $this->input->post("username",true);
        $username = strtolower(trim($username));
        $cityId = $this->input->post("city",true);
        $cityId = trim($cityId);
        $gender = $this->input->post("gender",true);
        $gender = trim($gender);
        $date_birth = $this->input->post("dateBirth",true);
        $date_birth = trim($date_birth);
        $phone_number = $this->input->post('phone_number', true);
        $phone_number = trim($phone_number);

        $sync_data = [
            'oauth_provider' => $oauth_provider,
            'oauth_id' => $oauth_id,
            'email' => $email,
            'fullname' => $name,
            'username' => $username,
            'city_id' => $cityId,
            'gender' => $gender,
            'date_birth' => $date_birth,
            'phone_number' => $phone_number
        ];
        // $log_request = format_log_data($sync_data);
        // log_api('API_REGISTER', $response);

        $this->form_validation->set_rules('oauth_id', 'Oauth Id', '');
        $this->form_validation->set_rules('oauth_provider', 'Oauth Provider', '');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_password_check');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]|valid_email', ['is_unique'=>'{field} already exists']);
        $this->form_validation->set_rules('fullname', 'Full name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|alpha|min_length[6]|max_length[20]|is_unique[users.username]|regex_match[/^[a-z]*$/]', ['is_unique'=>'{field} already exists', 'regex_match' => '{field} must be lowercase']);
        $this->form_validation->set_rules('city', 'City', 'required|integer');
        $this->form_validation->set_rules('gender', 'Gender', 'integer', 'required|regex_match[/^([1,2]|99)$/]');
        $this->form_validation->set_rules('dateBirth', 'Date Birth', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 200;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
            log_api('API_REGISTER_FAILED', $response);

        }
        else
        {
            if (!$this->ion_auth->email_check($email))
            {
                // check user in mooc
                $sync_response = check_user_mooc($username);

                // check there is user or not
                if(!empty($sync_response['users'])){
                    $mooc_id = $sync_response['users'][0]['id'];

                    // update user mooc
                    $sync_data = new stdClass();
                    $sync_data->id = $mooc_id;
                    $sync_data->username = $username;
                    $sync_data->fullname = $name;
                    $sync_data->email = $email;
                    $sync_data->password = $password;
                    $sync_response = update_user_mooc($sync_data);
                    // var_dump($sync_response);
                    // die();

                    if(is_null($sync_response)){
                    }else{
                        $err_message = isset($sync_response['message']) ? strip_tags($sync_response['message']) : 'Failed To Update User In MOOC';

                        $response->code = 200;
                        $response->status = false;
                        $response->message = $sync_response['message'];

                        // $log_request['password'] = $password;
                        // $log_response = format_log_data($response);
                        // insert_log('API_REGISTER_FAILED', $log_request, $log_response);

                        log_api('API_REGISTER_FAILED', $response);
                        $this->response($response, REST_Controller::HTTP_OK);
                        return;
                    }
                }elseif(isset($sync_response['exception']) && !empty($sync_response['exception'])){
                    $err_message = isset($sync_response['debuginfo']) ? $sync_response['debuginfo'] : ((isset($sync_response['message'])) ? $sync_response['message'] : ' Failed To Enroll User');
                    
                    $response->code = 400;
                    $response->status = false;
                    $response->message = $err_message;

                    log_api('API_REGISTER_FAILED', $response);
                    $this->response($response, REST_Controller::HTTP_OK);
                    return;
                }

                $additional_data = array(
                    "oauth_provider"=>$oauth_provider,
                    "oauth_uid"=>$oauth_id,
                    "full_name"=>$name,
                    "mooc_id" => isset($mooc_id) ? $mooc_id : 0,
                    "country_id"=>1,
                    "city_id"=>$cityId,
                    "gender"=>$gender,
                    "dateBirth"=>$date_birth,
                    'phone_number' => $phone_number
                );

                $group = array('2');

                // check user igracias
                $sync_response = get_user_igracias($username, true);

                $city = $this->web_app_model->get_single_data('city', ['id'=> $additional_data['city_id']]);
                if($sync_response['status']){
                    // update user igracias
                    $sync_data = new stdClass();
                    $sync_data->username = $username;
                    $sync_data->phone_number = $phone_number;
                    $sync_data->city = $city->name;
                    $sync_data->email = $email;
                    $sync_data->fullname = $name;
                    $sync_data->birth_date = $date_birth;
                    $sync_data->gender = $gender;
                    $sync_response = update_user_igracias($sync_data, true);


                    if($sync_response['status']){
                    }else{
                        $response->code = 200;
                        $response->status = false;
                        $response->message = $sync_response['message'];

                        // $log_request['password'] = $password;
                        // $log_response = format_log_data($response);
                        // insert_log('API_REGISTER_FAILED', $log_request, $log_response);

                        log_api('API_REGISTER_FAILED', $response);
                        $this->response($response, REST_Controller::HTTP_OK);
                        return;
                    }
                }else{
                    // create user igracias
                    $sync_data = new stdClass();
                    $sync_data->username = $username;
                    $sync_data->password = $password;
                    $sync_data->phone_number = $phone_number;
                    $sync_data->city = $city->name;
                    $sync_data->email = $email;
                    $sync_data->fullname = $name;
                    $sync_data->birth_date = $date_birth;
                    $sync_data->gender = $gender;
                    $sync_response = create_user_igracias($sync_data, true);

                    if($sync_response['status']){
                        $additional_data['personid'] = $sync_response['personid'];
                    }else{
                        $response->code = 200;
                        $response->status = false;
                        $response->message = $sync_response['message'];

                        // $log_request['password'] = $password;
                        // $log_response = format_log_data($response);
                        // insert_log('API_REGISTER_FAILED', $log_request, $log_response);

                        log_api('API_REGISTER_FAILED', $response);
                        $this->response($response, REST_Controller::HTTP_OK);
                        return;
                    }
                }
            
                if ($this->ion_auth->register($username, $password, $email, $additional_data, $group)) {
                    $this->session->sess_destroy();

                    $response->message = "Register Successful";
                    $response->status = true;

                    // $log_response = format_log_data($response);
                    // insert_log('API_REGISTER_SUCCESS', $log_request, $log_response);

                    log_api('API_REGISTER_SUCCESS', $response);

                }else{
                    $response->message = strip_tags($this->ion_auth->errors());
                    $response->status = false;

                    // $log_request['password'] = $password;
                    // $log_response = format_log_data($response);
                    // insert_log('API_REGISTER_FAILED', $log_request, $log_response);
                    log_api('API_REGISTER_FAILED', $response);
                }
            }else{
                $response->message = "Account already exist";
                $response->status = false;

                // $log_request['password'] = $password;
                // $log_response = format_log_data($response);
                // insert_log('API_REGISTER_FAILED', $log_request, $log_response);
                
                log_api('API_REGISTER_FAILED', $response);
            }
            $response->error_code = 0;

        }

        $this->response($response, REST_Controller::HTTP_OK);
	}

	public function city_get(){
        $this->credentials_check();

        $response = init_response();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");

        if(empty($start)){
            $start = 0;
        }

        if(empty($limit)){
            $limit = 10;
        }

        $response->status = true;
        $response->message = "Success";
        $response->data = $this->web_app_model->get_data_list("city", "", "", $start, $limit, "id", "");

        $this->response($response, REST_Controller::HTTP_OK);

    }

    public function password_check($str){
        $username = $this->input->post('username', true);
        if(strpos($str, $username) === false) return true;
        $this->form_validation->set_message('password_check', '{field} cannot contain username');
        return false;
    }

}

/* End of file Controllername.php */