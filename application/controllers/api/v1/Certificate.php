<?php

use Endroid\QrCode\QrCode;

defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class Certificate extends BaseAPIController
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('log_api');
        $this->load->model('Web_app_model');
    }

    function index_get()
    {
        $this->credentials_check();
        $response = init_response();
        $search = $this->input->get("search");
        $start = $this->input->get("start");
        $limit = $this->input->get("limit");

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        if (!empty($token_user)) {
            $user_id = $token_user->id;

            if (!empty($user_id)) {
                $where = [
                    'user_id' => $user_id
                ];
                if (!empty($search)) $where['ocw_course.ocw_name LIKE'] = '%' . $search . '%';

                $grade_objects = $this->web_app_model->get_data_join_list('user_grade', 'ocw_course', '', 'user_grade.ocw_course_id = ocw_course.id', '', $where, $start, $limit, '*', '', 'created_date');
                foreach ($grade_objects as &$grade_object) {
                    $grade_object->course = $this->web_app_model->get_single_data('ocw_course_joined', ['id' => $grade_object->ocw_course_id]);
                }
                $count = $this->web_app_model->get_data_join('user_grade', 'ocw_course', '', 'user_grade.ocw_course_id = ocw_course.id', '', $where, 'COUNT(user_grade.id) as total_item');
                if (!empty($grade_objects)) {
                    $response->code = 200;
                    $response->status = true;
                    $response->data = $grade_objects;
                    $response->count = $count[0]->total_item;

                    log_api("API_MY_CERTIFICATE", $response);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->code = 200;
                    $response->status = false;
                    $response->count = 0;
                    $response->message = "You Don't Have Any Certificate";

                    log_api("API_MY_CERTIFICATE_FAILED", $response);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            }
        }
    }

    function create_certificate_get()
    {
        $this->credentials_check();
        $response = init_response();
        
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        
        if (!empty($token_user)) {
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $get_params = $this->input->get();
                if (empty($get_params)) { //set default param so the form validation triggered
                    $get_params = array("course_id" => "");
                }
                $this->form_validation->set_data($get_params);
                $this->form_validation->set_rules('course_id', 'Course Id', 'required');

                if ($this->form_validation->run() == FALSE) {
                    $response->code = 400;
                    $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
                } else {
                    $course_id = $this->input->get("course_id");
                    $student_object = $this->web_app_model->get_data_join('users', 'user_grade', '', 'users.id = user_grade.user_id', '', ['users.id' => $user_id], 'users.*, user_grade.id as grade_id, user_grade.grade, user_grade.certificate_number, user_grade.certificate_path');
                    $course_object = $this->web_app_model->get_data_join('ocw_course', 'certificate', '', 'ocw_course.template_id = certificate.id', '', ['ocw_course.id' => $course_id, 'ocw_course.is_multiple' => 0]);
                    $template_objects = json_decode($course_object[0]->data);

                    if (!empty($student_object) && !empty($course_object)) {
                        if (empty($student_object[0]->certificate_path)) {
                            $grade_id = $student_object[0]->grade_id;
                            $certificate_number = $course_object[0]->certificate_prefix;
                            $certificate_number = !empty($certificate_number) ? $certificate_number : 'cert_';
                            $update_data['certificate_number'] = empty($student_object[0]->certificate_number) ? uniqid($certificate_number) : $student_object[0]->certificate_number;

                            $this->load->library('image_lib');

                            $publish_path = FCPATH . $this->config->item('certificate_publish_path');
                            if (file_exists(FCPATH . $course_object[0]->template_image)) {
                                if(!file_exists($publish_path.$course_object[0]->slug.$course_object[0]->id)){
                                    mkdir($publish_path.$course_object[0]->slug.$course_object[0]->id,0777,true);
                                }
                                
                                $update_data['certificate_path'] = $this->config->item('certificate_publish_path').$course_object[0]->slug.$course_object[0]->id.'/'. $student_object[0]->username . '.jpeg';
                                $student_certificate = $publish_path.$course_object[0]->slug.$course_object[0]->id.'/'. $student_object[0]->username . '.jpeg';
                                copy(FCPATH.$course_object[0]->template_image, $student_certificate);
                            } else {
                                $response->status = false;
                                $response->error_code = 400;
                                $response->message = "There is no template image";

                                log_api("API_CREATE_MY_CERTIFICATE_FAILED", $response);
                                $this->response($response, REST_Controller::HTTP_OK);
                                return;
                            }

                            if (isset($student_certificate)) {
                                $x = 1;
                                foreach ($template_objects as $object) {
                                    $config = [];
                                    $config['image_library'] = 'GD2';
                                    $config['source_image'] = $student_certificate;
                                    $config['quality'] = '100%';

                                    $object = json_decode($object);
                                    if (property_exists($object, 'objType') && $object->objType == 'fixedElement') {
                                        if ($object->type == 'i-text') {
                                            if ($object->id == '(Course Name)') {
                                                $config['wm_text'] = $course_object[0]->ocw_name;
                                            } else if ($object->id == '(Lecturer Name)') {
                                                $config['wm_text'] =  $course_object[0]->lecturer_name;
                                            } else if ($object->id == '(Learning Date)') {
                                                $config['wm_text'] = $course_object[0]->start_date . ' - ' . $course_object[0]->end_date;
                                            } else if ($object->id == '(Student Name)') {
                                                $config['wm_text'] = $student_object[0]->full_name;
                                            } else if ($object->id == '(Grade)') {
                                                $config['wm_text'] = $student_object[0]->grade;
                                            } else if ($object->id == '(Certificate Number)'){
                                                $config['wm_text'] = $update_data['certificate_number'];
                                            }

                                            if ($object->underline) {
                                                $arr_text = str_split($config['wm_text']);
                                                $arr_text = array_map([$this, 'string_to_underline'], $arr_text);
                                                array_unshift($arr_text, '&#x0332;');
                                                $config['wm_text'] = implode('', $arr_text);
                                            }

                                            $is_bold = $object->fontWeight == 'bold';
                                            $is_italic = $object->fontStyle == 'italic';
                                            if ($object->fontFamily == 'Times New Roman') {
                                                $fontname = 'times';
                                                if ($is_bold && $is_italic) {
                                                    $fontname = 'timesbi';
                                                } else if ($is_bold) {
                                                    $fontname = 'timesbd';
                                                } else if ($is_italic) {
                                                    $fontname = 'timesi';
                                                }
                                            } else if ($object->fontFamily == 'Verdana') {
                                                $fontname = 'verdana';
                                                if ($is_bold && $is_italic) {
                                                    $fontname = 'verdanaz';
                                                } else if ($is_bold) {
                                                    $fontname = 'verdanab';
                                                } else if ($is_italic) {
                                                    $fontname = 'verdanai';
                                                }
                                            } else if ($object->fontFamily == 'Arial') {
                                                $fontname = 'arial';
                                                if ($is_bold && $is_italic) {
                                                    $fontname = 'arialbi';
                                                } else if ($is_bold) {
                                                    $fontname = 'arialbd';
                                                } else if ($is_italic) {
                                                    $fontname = 'ariali';
                                                }
                                            } else if ($object->fontFamily == 'Kunstler') {
                                                $fontname = 'kunstler';
                                                if ($is_bold) {
                                                    $fontname = 'kunstlerbold';
                                                }
                                            }

                                            $config['wm_type'] = 'text';
                                            $config['wm_font_path'] = FCPATH . $this->config->item('certificate_fonts_path') . $fontname . '.ttf';
                                            $config['wm_font_size'] = $object->fontSize * $object->scaleX;
                                            $config['wm_font_color'] = $object->fill;

                                            if($object->originX == 'right'){
                                                $temp = imagettfbbox($config['wm_font_size'], 0, $config['wm_font_path'], $config['wm_text']);
                                                $temp = $temp[2] - $temp[0];
                                                $fontwidth = $temp / strlen($config['wm_text']);
                                                $x = ($object->left + $object->width) - ($temp + ($fontwidth * 3));
                                            }else if($object->originX == 'center'){
                                                $temp = imagettfbbox($config['wm_font_size'], 0, $config['wm_font_path'], $config['wm_text']);
                                                $temp = $temp[2] - $temp[0];
                                                $x = ($object->left + ($object->width / 2)) - ($temp / 2);
                                            }else{
                                                $x = $object->left;
                                            }

                                            $config['wm_hor_alignment'] = 'left';
                                            $config['wm_hor_offset'] = $x;
                                            $config['wm_vrt_alignment'] = 'top';
                                            $config['wm_vrt_offset'] = $object->top;
                                        } else if ($object->type = 'image') {
                                            $config['wm_hor_alignment'] = 'left';
                                            $config['wm_hor_offset'] = $object->left;
                                            $config['wm_vrt_alignment'] = 'top';
                                            $config['wm_vrt_offset'] = $object->top;

                                            $md5 = md5($student_object[0]->grade_id);
                                            $update_data['md5_hash'] = $md5;
                                            $qrcode = new QrCode(base_url('certificate/view/') . $md5);
                                            $qrcode->setSize($object->width * $object->scaleX);

                                            // save qrcode to temp directory
                                            $tmpfname = tempnam(sys_get_temp_dir(), 'cert_');
                                            $base64_img = explode(',', $qrcode->writeDataUri());
                                            $image = base64_decode($base64_img[1]);
                                            file_put_contents($tmpfname, $image);

                                            $config['wm_type'] = 'overlay';
                                            $config['wm_overlay_path'] = $tmpfname;
                                            $config['wm_opacity'] = 100;
                                        }

                                        $this->image_lib->initialize($config);
                                        if (!$this->image_lib->watermark()) {
                                            echo $this->image_lib->display_errors();
                                        }
                                    }
                                }
                                $this->web_app_model->update_data("user_grade", $update_data, $grade_id, "id");

                                $response->status = true;
                                $response->error_code = 200;
                                $response->data = base_url() . $update_data['certificate_path'];
                                $response->message = "Successfully to generate the certificate";

                                log_api("API_CREATE_MY_CERTIFICATE", $response);
                                $this->response($response, REST_Controller::HTTP_OK);
                            } else {
                                $response->status = false;
                                $response->error_code = 400;
                                $response->message = "Failed to generate the certificate";

                                log_api("API_CREATE_MY_CERTIFICATE_FAILED", $response);
                                $this->response($response, REST_Controller::HTTP_OK);
                            }
                        } else {
                            $response->status = true;
                            $response->error_code = 200;
                            $response->data = base_url() . $student_object[0]->certificate_path;
                            $response->message = "Successfully to generate the certificate";

                            log_api("API_CREATE_MY_CERTIFICATE", $response);
                            $this->response($response, REST_Controller::HTTP_OK);
                        }
                    } else {
                        $response->status = false;
                        $response->error_code = 400;
                        $response->message = "Something wrong with the information that send";

                        log_api("API_CREATE_MY_CERTIFICATE_FAILED", $response);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                }
            } else {
                $response->status = false;
                $response->message = "Invalid token";
                $response->error_code = 403;

                log_api("API_MY_CERTIFICATE_FAILED", $response);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function download_get()
    {
        $filename = $this->input->get('filename');
        if (!$filename) {
            echo 'An Error Has Occured, Please Try Again Later';
            return;
        }

        if (file_exists($filename)) {
            $this->load->helper('download');
            force_download($filename, NULL);
        } else {
            echo 'An Error Has Occured, Please Try Again Later';
        }
    }

    private function string_to_underline($letter)
    {
        return $letter . '&#x0332;';
    }
}
