<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class PushNotifToken extends BaseAPIController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Push_Notif_Model');
        $this->load->helper('log_api');
        $this->load->model('Web_App_Model');
        $this->load->library('Push_notif');
    }

    public function index_post()
    {
        $this->credentials_check();

        $response = init_response();

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        $fcm_token = $this->input->post("token", true);

        if (!empty($token_user)) {
            $user_id = $token_user->id;

            if (!empty($user_id)) {

                $push_notif_token_data = array();
                $push_notif_token_data['user_id'] = $user_id;
                $push_notif_token_data['token'] = $fcm_token;

                $this->Push_Notif_Model->create($push_notif_token_data);

                $response->status = true;
                $response->data = $fcm_token;
                $response->message = "FCM token successfully created";

                log_api("API_PUSH_NOTIF_TOKEN_POST", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_PUSH_NOTIF_TOKEN_POST_FAILED", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_PUSH_NOTIF_TOKEN_POST_FAILED", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }

    public function logout_post()
    {
        $this->credentials_check();

        $response = init_response();

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        if (!empty($token_user)) {
            $user_id = $token_user->id;

            if (!empty($user_id)) {
                $push_notif_data = $this->Push_Notif_Model->get($user_id);
                $fcm_token = $this->input->post('token',true);
                if (!empty($push_notif_data)) {
                    $this->Push_Notif_Model->delete($fcm_token);

                    $response->status = true;
                    $response->data = "Token deleted";
                    $response->message = "FCM token successfully deleted";

                    log_api("API_DELETE_PUSH_NOTIF_TOKEN", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->status = false;
                    $response->message = "Push notif data not found";
                    $response->error_code = 400;

                    log_api("API_DELETE_PUSH_NOTIF_TOKEN_FAILED", $response, $token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_DELETE_PUSH_NOTIF_TOKEN_FAILED", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_DELETE_PUSH_NOTIF_TOKEN_FAILED", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }
}
