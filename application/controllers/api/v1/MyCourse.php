<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class MyCourse extends BaseAPIController
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('log_api');
    }

    function index_get()
    {
        $this->credentials_check();
        $response = init_response();
        $search = $this->input->get("search");
        $date_range_start = $this->input->get("date_start");
        $date_range_end = $this->input->get("date_end");
        $status = $this->input->get("status");
        $start = $this->input->get("start");
        $limit = $this->input->get("limit");

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        if (!empty($token_user)) {
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $where = array("user_id" => $user_id);
                $target = "ocw_course_joined.ocw_name";
                if (empty($search)) {
                    $search = "";
                }

                if (empty($start)) {
                    $start = 0;
                }

                if (empty($limit)) {
                    $limit = 10;
                }
                
                if(!empty($status)){
                    if($status == 'upcoming') {
                        $where['ocw_course_joined.start_date >'] = date('Y-m-d');
                    }else if($status == 'completed'){
                        $where['ocw_course_joined.end_date <'] = date('Y-m-d');
                    }else{
                        $where['ocw_course_joined.start_date <='] = date('Y-m-d');
                        $where['ocw_course_joined.end_date >='] = date('Y-m-d');
                    }
                }

                $courses = $this->web_app_model->get_data_join_list_with_date('ocw_course_enroll', 'ocw_course_joined', 'ocw_course_enroll.ocw_course_id = ocw_course_joined.id', $where, $target, $search, $status,$date_range_start, $date_range_end, $start, $limit, 'ocw_course_enroll.id as enroll_id, ocw_course_enroll.*, ocw_course_joined.*', '', 'ocw_course_enroll.created_date');
                foreach($courses as &$course){
                    $course->description = strip_tags($course->description);
        
                    if($course->is_multiple == 1){
                        $course->courses = $this->web_app_model->get_data_join('packet_ocw_course as packet', 'reference_multiple_course as reference', 'ocw_course_joined as course', 'packet.id = reference.package_ocw_course_id', 'reference.course_target_id = course.id', ['packet.course_id' => $course->id]);
        
                        foreach($course->courses as &$course){
                            $course->banner_image = base_url() . $course->banner_image;
                            $course->lecturer_photo = base_url() . $course->lecturer_photo;
                            $course->description = strip_tags($course->description);
                        }
                    }
                }

                $response->message = "Success get course";
                $response->status = true;
                $response->data = $courses;

                log_api("API_MY_COURSE", $response);
                $this->response($response, REST_Controller::HTTP_OK);
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_MY_COURSE_FAILED", $response, $token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_MY_COURSE_FAILED", $response, $token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }
}
