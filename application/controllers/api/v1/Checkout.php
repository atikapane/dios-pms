<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class Checkout extends BaseAPIController {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Transaction_Model');
        $this->load->helper('log_api');
    }

    public function transaction_get(){
        $this->credentials_check();
        $response = init_response();
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        if(!empty($token_user)){
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $transaction_data = $this->Transaction_Model->get_by_user($user_id,Transaction_Model::STATUS_WAITING);

                if (!empty($transaction_data)){
                    $response->data = $transaction_data;
                    $response->status = true;
                    $response->error_code = 0;
                    $response->message = "Get transaction data success";
    
                    log_api("API_TRANSACTION_GET",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->status = false;
                    $response->message = "Transaction data not found";
                    $response->error_code = 400;

                    log_api("API_TRANSACTION_GET_FAILED",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
                
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_TRANSACTION_GET_FAILED",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }

        } else{
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_TRANSACTION_GET_FAILED",$response,$token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }

    }

}