<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class Survey extends BaseAPIController{

	function __construct() 
	{
		parent::__construct();
        $this->load->helper('log_api');
        $this->load->model('Web_App_Model');
        
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, celoe-api-key");
        if ( "OPTIONS" === $_SERVER['REQUEST_METHOD'] ) {
            die();
        }    
    }
    
    public function today_survey_get(){
        $this->credentials_check();
        $response = get_ajax_response();
        
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('platform', 'Platform', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        }
        else
        {
            $user_id = $this->input->get('user_id', true);
            $platform = $this->input->get('platform', true);
            $classes = $this->input->get('classes', true);
    
            $now = new DateTime();
            $where = [
                'survey.start_date <=' => $now->format('Y-m-d'),
                'survey.end_date >=' => $now->format('Y-m-d'),
                'survey.platform' => $platform
            ];

            if($platform == 'LMS'){
                if(!empty($classes)){
                    $classes = explode(',', $classes);
                    $classStrArr = '(';
                    $x = 0;
                    foreach($classes as $class){
                        if($x) $classStrArr .= ',';
                        $classStrArr .= "'" . $class . "'";
                        $x++;
                    }
                    $classStrArr .= ')';
                    $classes = $classStrArr;
    
                    $where["(`course`.`course_id` IS NULL or `course`.`course_id` IN {$classes})"] = null;
                }else{
                    $where["`survey`.`subject_code` IS NULL"] = null;
                }

                $survey_objects = $this->web_app_model->get_data_join('survey', 'survey_class as class', 'sync_lms_course as course', 'survey.id = class.survey_id', 'class.class = course.class AND survey.semester = course.semester AND survey.subject_code = course.subject_code', $where, 'survey.*, class.class', 'survey.id', 'survey.end_date', 'left', 'left');
            }elseif ($platform == 'MOOC'){
                if(!empty($classes)){
                    $classes = explode(',', $classes);
                    $classStrArr = '(';
                    $x = 0;
                    foreach($classes as $class){
                        if($x) $classStrArr .= ',';
                        $classStrArr .= "'" . $class . "'";
                        $x++;
                    }
                    $classStrArr .= ')';
                    $classes = $classStrArr;

                    $where["(`survey`.`subject_code` IS NULL or `ocw_course_joined`.`mooc_id` IN {$classes})"] = null;
                }else{
                    $where["`survey`.`subject_code` IS NULL"] = null;
                }

                $survey_objects = $this->web_app_model->get_data_join('survey', 'ocw_course_joined', '', 'survey.subject_code = ocw_course_joined.subject_code', '', $where, 'survey.*', 'survey.id', 'survey.end_date', 'left');
            }else{
                $survey_objects = $this->web_app_model->get_data('survey', $where);
            }
            foreach($survey_objects as $key => $survey){
                $survey_not_show = $this->web_app_model->get_single_data('survey_not_show', ['user_id' => $user_id, 'survey_id' => $survey->id]);
                if(!empty($survey_not_show)){
                    unset($survey_objects[$key]);
                }
            }
    
            $survey = array_shift($survey_objects);
    
            if(!empty($survey)){
                $this->web_app_model->update_data("survey",['seen_count' => ++$survey->seen_count],$survey->id,"id");

                $response->status = true;
                $response->error_code = 200;
                $response->survey = $survey;
            }else{
                $response->status = false;
                $response->error_code = 400;
                $response->message = 'There is no survey';
            }
        }

        log_api('API_SURVEY_TODAY', $response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function survey_reject_post(){
        $this->credentials_check();
        $response = get_ajax_response();

        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('survey_id', 'Survey Id', 'required');
        $this->form_validation->set_rules('not_show', 'Not Show', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        }
        else
        {
            $user_id = $this->input->post('user_id', true);
            $survey_id = $this->input->post('survey_id', true);
            $not_show = $this->input->post('not_show', true);
    
            $survey_object = $this->web_app_model->get_single_data('survey', ['id' => $survey_id]);
            if(!empty($survey_object)){
                if($not_show == 'false'){
                    $this->web_app_model->update_data("survey",['reject_count' => ++$survey_object->reject_count],$survey_id,"id");
                }else{
                    $insert_data = [
                        'user_id' => $user_id,
                        'survey_id' => $survey_id,
                    ];
                    $this->Web_App_Model->insert_data('survey_not_show', $insert_data);
                    $this->web_app_model->update_data("survey",['reject_permanently_count' => ++$survey_object->reject_permanently_count],$survey_id,"id");
                }
    
                $response->error_code = 200;
                $response->status = true;
                $response->message = 'Survey rejected';
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = 'Survey not found';
            }
        }

        log_api('API_SURVEY_NOT_SHOW', $response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

	public function check_survey_post()
    {
        $response = get_ajax_response();
        $table = "survey";

        $name = $this->input->post("username",true);
        $start_day = $this->input->post("start_date[day]",true);
        $start_month = $this->input->post("start_date[month]",true);
        $start_year = $this->input->post("start_date[year]",true);
        
        $end_day = $this->input->post("end_date[day]",true);
        $end_month = $this->input->post("end_date[month]",true);
        $end_year = $this->input->post("end_date[year]",true);

        $start = "$start_year/$start_month/$start_day";
        $end = "$end_year/$end_month/$end_day";

        $type = $this->input->post("type",true);
        $type_data ="";
        if ($type=="0"){
            $type_data="COURSE";
        } else {
            $type_data="SERVICES";
        }

        $platform = $this->input->post("platform_course",true);
        if (empty($platform)){
            $platform = $this->input->post("platform_services",true);
        }

        $class = $this->input->post("class",true);

        
            $status = false;

            $where = array();
            $where_data = array();

            array_push($where,"platform = \"$platform\"");   
            $this->db->where("platform",$platform);
            // $data_platform = $this->Web_App_Model->get_count($table,$where_platform);

            if ($platform=="LMS"){
                if ($type_data=="COURSE"){
                    array_push($where,"class = \"$class\"");   
                    $this->db->where("class",$class);
                    // $data_class = $this->Web_App_Model->get_count($table,$where_class);
                    // if ($data_class>0){
                    //     $status = true;
                    // } else {
                    //     $status = false;
                    // }
                }
            }

            $start_date = new DateTime($start);
            $end_date = new DateTime($end);
            $this->db->select("*");
            $this->db->where("start_time >=",$start_date->format('Y-m-d H:i:s'));
            $this->db->where("end_time <=",$end_date->format('Y-m-d H:i:s'));
            // $where_date[] = "created_at BETWEEN '" . $start->format('Y-m-d H:i:s') . "' AND '" . $end->format('Y-m-d H:i:s') . "'";

            array_push($where,"start_time >='" . $start_date->format('Y-m-d H:i:s') . "' AND end_time<='" . $end_date->format('Y-m-d H:i:s') . "'");
            $this->db->from($table);
            $query = $this->db->get();
		    $survey_data =$query->row();

            $data = $this->Web_App_Model->get_count($table,$where);
            if ($data>0){
                $status = true;
            } else {
                $status = false;
            }
            // $response->status = true;
            // $response->data = $status;
            // $response->data_val = $survey_data->id;
            // $response->query = $this->db->last_query();
            // $response->error_code = 0;
            // $this->response($response, REST_Controller::HTTP_OK);

            if ($status){
                $url = "https://localhost/moodle/user/dialog_survey.php?name=$name&survey_id=$survey_data->id";
                $redirect = '<script type="text/javascript">
                window.location = "'.$url.'";
                </script>';
            } else {
                $url = "http://localhost/moodle/my";
                $redirect = '<script type="text/javascript">
                alert("Survey not available");
                window.location = "'.$url.'";
                 </script>';
            }
            log_api('API_SURVEY_CHECK', $redirect);
            echo $redirect;
    }

    public function class_get()
    {
        $table = "sync_lms_course";
        $group=array();
        $where=array();
        
        array_push($group,'class');

        $data = $this->Web_App_Model->get_data($table,$where,$group);
        $response = get_ajax_response();

        $response->data = $data;

        if(!empty($data)){
            $response->code = 200;
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->code = 200;
            $response->status = false;
            $response->message = "Data not found";
        }
        log_api('API_SURVEY_GET_CLASS', $response);
        echo json_encode($response);
    }
	
}
