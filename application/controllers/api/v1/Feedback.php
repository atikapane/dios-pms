<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class Feedback extends BaseAPIController
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Web_App_Model');
        $this->load->model('Feedback_Model');
        $this->load->helper('log_api');
    }

    public function index_get()
    {
        $response = get_ajax_response();

        $get_params = $this->input->get();

        $start = empty($get_params['start']) ? 0 : $get_params['start'];
        $limit = empty($get_params['limit']) ? 10 : $get_params['limit'];
        $course_id = empty($get_params['course_id']) ? "" : $get_params['course_id'];

        if (!empty($get_params)) {
            $this->form_validation->set_data($get_params);
            $this->form_validation->set_rules('course_id', 'Course Id', 'numeric');
            $this->form_validation->set_rules('start', 'Start', 'numeric');
            $this->form_validation->set_rules('limit', 'Limit', 'numeric');

            if ($this->form_validation->run() == FALSE) {
                $response->status = false;
                $response->error_code = 400;
                $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));

                $this->response($response, REST_Controller::HTTP_OK);
            }
        }

        $response->status = true;
        $response->error_code = 200;
        $response->data = $this->Feedback_Model->get_feedback($limit, $start, $course_id);

        log_api("API_FEEDBACK_LIST",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function detail_get()
    {
        $response = init_response();

        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $response->status = false;
            $response->error_code = 400;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));

            log_api("API_FEEDBACK_DETAIL",$response);
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $feedback_id = $this->input->get("id", true);
            if (!empty($feedback_id)) {
                $query_data['id'] = $feedback_id;
                $feedback_object = $this->Feedback_Model->get_detail($feedback_id);
                if (!empty($feedback_object)) {
                    $response->data = $feedback_object;
                    $response->status = true;
                    $response->error_code = 200;
                    $response->message = "Get data success";

                    log_api("API_FEEDBACK_DETAIL",$response);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->status = false;
                    $response->error_code = 400;
                    $response->message = "Data not found";

                    log_api("API_FEEDBACK_DETAIL",$response);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "Id is required";
                $response->error_code = 400;

                log_api("API_FEEDBACK_DETAIL",$response);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function create_post()
    {
        $this->credentials_check();

        $response = init_response();

        // $this->form_validation->set_rules('user_id', 'User Id', 'required');

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        $this->form_validation->set_rules('course_id', 'Course Id', 'required');
        $this->form_validation->set_rules('rating', 'Rating', 'required|numeric');
        // $this->form_validation->set_rules('feedback', 'Feedback', 'required');

        if(!empty($token_user)){
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                if ($this->form_validation->run() == FALSE) {
                    $response->status = false;
                    $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
                    $response->error_code = 400;
        
                    log_api("API_FEEDBACK_CREATE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $where = array(
                        'user_id' => $user_id,
                        'course_id' => $this->input->post('course_id', true)
                    );
                    $feedback = $this->Web_App_Model->get_data_and_where('feedbacks', $where);
                    if (!$feedback) {
                        $data = array(
                            'user_id' => $user_id,
                            'course_id' => $this->input->post('course_id', true),
                            'rating' => $this->input->post('rating', true),
                            'feedback' => $this->input->post('feedback', true)
                        );
                        $this->Web_App_Model->insert_data('feedbacks', $data);
        
                        $response->status = true;
                        $response->message = "Data submitted successfully";
                        $response->error_code = 200;
        
                        log_api("API_FEEDBACK_CREATE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    } else {
                        $response->status = false;
                        $response->message = "This user has submitted feedback in the course";
                        $response->error_code = 400;
        
                        log_api("API_FEEDBACK_CREATE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                }
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_FEEDBACK_CREATE",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
            
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_FEEDBACK_CREATE",$response,$token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
        
    }

    public function update_post()
    {
        $this->credentials_check();

        $response = init_response();

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        if(!empty($token_user)){
            $user_id = $token_user->id;
            if (!empty($user_id)) {

                $this->form_validation->set_rules('id', 'Id', 'required');
                $this->form_validation->set_rules('rating', 'Rating', 'required|numeric');
                // $this->form_validation->set_rules('feedback', 'Feedback', 'required');
        
                if ($this->form_validation->run() == FALSE) {
                    $response->status = false;
                    $response->message = "Message error: " . validation_errors();
                    $response->error_code = 400;
                    log_api("API_FEEDBACK_UPDATE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $id = $this->input->post("id", true);
                    $rating = $this->input->post("rating", true);
                    $feedback = $this->input->post("feedback", true);
        
                    $primary_key['id'] = $id;
        
                    $update_data['rating'] = $rating;
                    $update_data['feedback'] = $feedback;
                    $update_data['created_date'] = date("Y-m-d h:i:s");
        
                    $feedback_object = $this->web_app_model->get_single_data("feedbacks", $primary_key);
                    if (!empty($feedback_object)) {
                        $this->web_app_model->update_data("feedbacks", $update_data, $primary_key['id'], "id");
        
                        $response->status = true;
                        $response->message = "Data updated successfully";
                        $response->error_code = 200;
        
                        log_api("API_FEEDBACK_UPDATE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    } else {
                        $response->status = false;
                        $response->message = "Data not found";
                        $response->error_code = 400;
        
                        log_api("API_FEEDBACK_UPDATE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                }
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_FEEDBACK_UPDATE",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
            
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_FEEDBACK_UPDATE",$response,$token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }
    }

    public function delete_get()
    {
        $this->credentials_check();

        $response = init_response();

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);

        if(!empty($token_user)){
            $user_id = $token_user->id;
            if (!empty($user_id)) {
                $get_params = $this->input->get();
                if (empty($get_params)) { //set default param so the form validation triggered
                    $get_params = array("id" => "");
                }
                $this->form_validation->set_data($get_params);
                $this->form_validation->set_rules('id', 'Id', 'required');
        
                if ($this->form_validation->run() == FALSE) {
                    $response->status = false;
                    $response->message = "Message error: " . validation_errors();
                    $response->error_code = 400;
                    log_api("API_FEEDBACK_DELETE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $feedback_id = $this->input->get("id", true);
                    if (!empty($feedback_id)) {
                        $query_data['id'] = $feedback_id;
                        $feedback_object = $this->web_app_model->get_single_data("feedbacks", $query_data);
                        if (!empty($feedback_object)) {
                            $delete_data['id'] = $feedback_id;
                            $this->web_app_model->delete_data("feedbacks", $delete_data);
        
                            $response->status = true;
                            $response->message = "Data deleted successfully";
                            $response->error_code = 200;
        
                            log_api("API_FEEDBACK_DELETE",$response,$token_user);
                            $this->response($response, REST_Controller::HTTP_OK);
                        } else {
                            $response->status = false;
                            $response->message = "Data not found";
                            $response->error_code = 400;
        
                            log_api("API_FEEDBACK_DELETE",$response,$token_user);
                            $this->response($response, REST_Controller::HTTP_OK);
                        }
                    } else {
                        $response->status = false;
                        $response->message = "Id is required";
                        $response->error_code = 400;
        
                        log_api("API_FEEDBACK_DELETE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                }
            } else {
                $response->status = false;
                $response->message = "User Id is required";
                $response->error_code = 400;

                log_api("API_FEEDBACK_DELETE",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
            
        } else {
            $response->status = false;
            $response->message = "Invalid token";
            $response->error_code = 403;

            log_api("API_FEEDBACK_DELETE",$response,$token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        }

        
    }
}