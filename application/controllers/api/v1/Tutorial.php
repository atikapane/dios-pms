<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class Tutorial extends BaseAPIController{

	function __construct() 
	{
		parent::__construct();
        $this->load->helper('log_api');
	}

	public function index_get()
    {
        $response = get_ajax_response();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");

        if(empty($start)){
            $start = 0;
        }

        if(empty($limit)){
            $limit = 1000;
        }

        $response->status = true;
        $response->data = $this->web_app_model->get_data_list("landing_tutorial", "", "", $start, $limit, "id", "");
        foreach ($response->data as $value) {
            $value->file_path = !empty($value->file_path) ? ($value->type == "file" ? base_url().$value->file_path : $value->file_path) : "";
        }
     
        log_api("API_TUTORIAL_LIST",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }
	
}
