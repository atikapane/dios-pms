<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class ForgotPassword extends BaseAPIController {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('log_api');
    }
    
    function send_email_post()
    {
        $this->credentials_check();

        $response = init_response();

        $email = $this->input->post('email',true);
        $this->form_validation->set_rules('email', 'Email Address', 'required');

        if ($this->form_validation->run() == false) {
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
            $response->status = false;
        }
        else {
            $forgotten = $this->ion_auth->forgotten_password($email);
            if ($forgotten) {
                $response->message = strip_tags($this->ion_auth->messages());
                $response->status = true;
                $response->data = $forgotten;
            }
            else {
                $response->message = strip_tags($this->ion_auth->errors());
                $response->status = false;
            }
        }

        log_api("API_FORGOT_PASSWORD_SEND",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    function process_reset_password_post(){
        // $this->credentials_check();

        $response = init_response();

        $this->form_validation->set_rules('new_password', 'New Password', 'required');
        $this->form_validation->set_rules('token', 'Token', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        }
        else
        {
            $new_password = $this->input->post("new_password");
            $token = $this->input->post('token');

            $user = $this->ion_auth->forgotten_password_check($token);
            if ($user)
            {
                $id = $user->id;
                $data = array(
                    "oauth_provider"=>null,
                    "oauth_uid"=>null,
                    "password" => $new_password,
                    "forgotten_password_selector"=>null,
                    "forgotten_password_code"=>null,
                    "forgotten_password_time"=>null
                );
                if($this->ion_auth->update_user($id, $data)){
                    $response->message = strip_tags($this->ion_auth->messages());
                    $response->status = true;
                }else{
                    $response->message = strip_tags($this->ion_auth->errors());
                    $response->status = false;
                }
            }else{
                $response->message = strip_tags($this->ion_auth->errors());
                $response->status = false;
            }
        }

        log_api("API_FORGOT_PASSWORD_RESET",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

}

/* End of file Controllername.php */