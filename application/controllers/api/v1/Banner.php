<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class Banner extends BaseAPIController{

	function __construct() 
	{
		parent::__construct();
        $this->load->helper('log_api');
	}

	public function index_get()
    {
        $response = get_ajax_response();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");

        if(empty($start)){
            $start = 0;
        }

        if(empty($limit)){
            $limit = 10;
        }

        $response->status = true;
        $response->data = $this->web_app_model->get_data_list("landing_banner", "", "", $start, $limit, "id", "");
        foreach ($response->data as $value) {
            $value->img_path = base_url().$value->img_path;
        }
     
        log_api("API_BANNER_LIST",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }
	
}
