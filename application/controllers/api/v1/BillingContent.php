<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class BillingContent extends BaseAPIController{

	function __construct() 
	{
		parent::__construct();
        $this->load->model('Web_App_Model');
        $this->load->helper('log_api');
	}

	public function index_get()
    {
        $response = get_ajax_response();
        $response->data =  $this->Web_App_Model->get_data_all('landing_billing',"priority asc");
        $response->status =  true;
        $response->message =  "Get Billing Content Success";

        // log_api("API_BANNER_LIST",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function update_post()
    {
        $response = get_ajax_response();

        $get_params = $this->input->post();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        } else {
            $bilcont_id = $this->input->post('id', true);
            if (!empty($bilcont_id)) {
                $data = array(
                    'title' => $this->input->post('title', true),
                    'content' => $this->input->post('content', true)
                );
                
                $id = $this->Web_App_Model->update_data('landing_billing', $data, $bilcont_id, 'id');
                if (!empty($id)) {
                    $response->code = 200;
                    $response->data = $id;
                    $response->status = true;
                    $response->message = "Update Billing Content Successfully";
                } else {
                    $response->code = 200;
                    $response->data = $id;
                    $response->status = false;
                    $response->message = "Failed To Update Billing Content Data";
                }
            }
        }

        echo json_encode($response);
    }
    public function create_post()
    {
        $response = get_ajax_response();
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        }
        else
        {
            $count = $this->web_app_model->get_data_distinct("landing_billing", [], 'count(*) as count');
            $title = $this->input->post("title", true);
            $content = $this->input->post("content", true);
            $prio_pos = $count[0]->count + 1;
            
            $data = array(
                'title' => $title,
                'content' => $content,
                'priority' => $prio_pos
            );
            $id = $this->Web_App_Model->insert_data('landing_billing', $data);
            
            if (!empty($id)) {
                $response->code = 200;
                $response->status = true;
                $response->message = "Add Billing Content Successfully";
            } else {
                $response->code = 200;
                $response->status = false;
                $response->message = "Failed To Add Billing Content Data";
            }
        }

        echo json_encode($response);
    }

    public function remove_post()
	{
        $response = get_ajax_response();

        $get_params = $this->input->post();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        } else {
            $data['id'] = $this->input->post('id');
            $db=false;
            $db = $this->web_app_model->delete_data('landing_billing',$data);
            if($db=true){
                
                $response->code = 200;
                $response->status = true;
                $response->message = "Remove Billing Content Successfully";
            }else{
                $response->code = 200;
                $response->status = false;
                $response->message = "Failed To Remove Billing Content Data";
            }
        }
        
        echo json_encode($response);
	}
	
}
