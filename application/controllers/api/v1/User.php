<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class User extends BaseAPIController{

	function __construct() 
	{
        parent::__construct();
        $this->load->library('Sync');
        $this->load->helper('sync_mooc');
        $this->load->helper('sync_igracias');
        $this->load->helper('log_api');
	}

	public function update_post()
    {
        $this->credentials_check();

        $response = init_response();
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        // $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('old_password', 'Old Password', 'min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_old_password_check');
        $this->form_validation->set_rules('new_password', 'New Password', 'min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_password_check|callback_new_password_check');
        $this->form_validation->set_rules('email', 'Email', 'valid_email');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'numeric');
        $this->form_validation->set_rules('gender', 'Gender', 'regex_match[/^([1,2]|99)$/]');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 200;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        }
        else
        {
            if(!empty($token_user)){
                $user_id = $token_user->id;
                $primary_key['id'] = $user_id;

                // $user_object = $this->web_app_model->get_single_data("users",$primary_key);
                $user_object = $this->ion_auth->user($primary_key['id'])->row();
                if(!empty($user_object)){
                    $old_password = $this->input->post('old_password',true);
                    $password = $this->input->post('new_password',true);
                    $email = $this->input->post('email',true);
                    $email = trim($email);
                    $fullname = $this->input->post('fullname',true);
                    $fullname = trim($fullname);
                    $city_id = $this->input->post('city',true);
                    $city_id = trim($city_id);
                    $gender = $this->input->post('gender',true);
                    $gender = trim($gender);
                    $date_birth = $this->input->post('date_birth',true);
                    $date_birth = trim($date_birth);
                    $phone_number = $this->input->post('phone_number',true);
                    $phone_number = trim($phone_number);

                    if($password) $update_data['password'] = $password;
                    $update_data['ip_address'] = $this->input->ip_address();
                    if($email){
                        $update_data['email'] = $this->input->post('email',true);
                        $email_object = $this->web_app_model->get_single_data('users', ['email' => $update_data['email']]);
                    }
                    if($fullname) $update_data['full_name'] = $this->input->post('fullname',true);
                    $update_data['country_id'] = 1;
                    if($city_id){
                        $update_data['city_id'] = $this->input->post('city',true);
                        $city = $this->web_app_model->get_single_data('city', ['id'=> $update_data['city_id']]);
                    }
                    if($gender) $update_data['gender'] = $this->input->post('gender',true);
                    if($date_birth) $update_data['dateBirth'] = $this->input->post('date_birth',true);
                    if($phone_number) $update_data['phone_number'] = $this->input->post('phone_number',true);

                    $sync_data = [
                        'email' => $email,
                        'fullname' => $fullname,
                        'username' => $user_object->username,
                        'city_id' => $city_id,
                        'gender' => $gender,
                        'date_birth' => $date_birth,
                        'phone_number' => $phone_number
                    ];
                    // $log_request = format_log_data($sync_data);
                    // insert_log('API_USER_UPDATE', $log_request);

                    log_api("API_USER_UPDATE",$response);

                    if(empty($email_object) || $email_object->id == $primary_key['id']){
                        // check user in mooc
                        $sync_response = check_user_mooc($user_object->email);
                        
                        if(!empty($sync_response['users'])){
                            $update_data['mooc_id'] = $sync_response['users'][0]['id'];
                            // update user mooc
                            $sync_data = new stdClass();
                            $sync_data->id = $update_data['mooc_id'];
                            $sync_data->username = $user_object->username;
                            if($fullname) $sync_data->fullname = $fullname;
                            if($email) $sync_data->email = $email;
                            if($password) $sync_data->password = $password;
                            $sync_response = update_user_mooc($sync_data);

                            if(is_null($sync_response)){
                            }else{
                                $err_message = isset($sync_response['message']) ? strip_tags($sync_response['message']) : 'Failed To Update User In MOOC';

                                $response->code = 200;
                                $response->status = false;
                                $response->message = $err_message;

                                // $log_request['password'] = $password ?: 'Qwerty1@';
                                // $log_response = format_log_data($response);
                                // insert_log('API_USER_UPDATE_FAILED', $log_request, $log_response);

                                log_api("API_USER_UPDATE_FAILED",$response);

                                $this->response($response, REST_Controller::HTTP_OK);
                                return ;
                            }
                        }elseif(isset($sync_response['exception']) && !empty($sync_response['exception'])){
                            $err_message = isset($sync_response['debuginfo']) ? $sync_response['debuginfo'] : ((isset($sync_response['message'])) ? $sync_response['message'] : ' Failed To Enroll User');
                            
                            $response->code = 400;
                            $response->status = false;
                            $response->message = $err_message;
        
                            log_api('API_USER_UPDATE_FAILED', $response);
                            $this->response($response, REST_Controller::HTTP_OK);
                            return;
                        }

                        // check user igracias
                        $sync_response = get_user_igracias($user_object->username, true);

                        $city = $this->web_app_model->get_single_data('city', ['id'=> $update_data['city_id']]);
                        if($sync_response['status']){
                            // update user igracias
                            $sync_data = new stdClass();
                            $sync_data->username = $token_user->username;
                            if($phone_number) $sync_data->phone_number = $update_data['phone_number'];
                            if($city_id) $sync_data->city = $city->name;
                            if($email) $sync_data->email = $update_data['email'];
                            if($fullname) $sync_data->fullname = $update_data['full_name'];
                            if($date_birth) $sync_data->birth_date = $update_data['dateBirth'];
                            if($gender) $sync_data->gender = $update_data['gender'];
                            $sync_response = update_user_igracias($sync_data, true);

                            if($sync_response['status']){
                                // udpate password igracias
                                $old_password = $this->input->post('old_password', true);
                                if($password && $old_password){
                                    $sync_data = new stdClass();
                                    $sync_data->username = $user_object->username;
                                    $sync_data->old_password = $old_password;
                                    $sync_data->new_password = $password;
                                    $sync_response = update_password_igracias($sync_data, true);

                                    if($sync_response['status']){
                                    }else{
                                        $response->code = 200;
                                        $response->status = false;
                                        $response->message = $sync_response['message'];

                                        // $log_request['password'] = $password ?: 'Qwerty1@';
                                        // $log_response = format_log_data($response);
                                        // insert_log('API_USER_UPDATE_FAILED', $log_request, $log_response);

                                        log_api("API_USER_UPDATE_FAILED",$response);
                                        $this->response($response, REST_Controller::HTTP_OK);
                                        return;
                                    }
                                }
                            }else{
                                $response->code = 200;
                                $response->status = false;
                                $response->message = $sync_response['message'];

                                // $log_request['password'] = $password ?: 'Qwerty1@';
                                // $log_response = format_log_data($response);
                                // insert_log('API_USER_UPDATE_FAILED', $log_request, $log_response);

                                log_api("API_USER_UPDATE_FAILED",$response);
                                $this->response($response, REST_Controller::HTTP_OK);
                                return;
                            }
                        }else{
                            // create user igracias
                            $sync_data = new stdClass();
                            $sync_data->username = $user_object->username;
                            $sync_data->password = $password ?: 'Qwerty1@';
                            $sync_data->phone_number = $phone_number ?: $user_object->phone_number;
                            $sync_data->city = $city_id ? $city->name : $user_object->city_id;
                            $sync_data->email = $email ?: $user_object->email;
                            $sync_data->fullname = $fullname ?: $user_object->full_name;
                            $sync_data->birth_date = $date_birth ?: $user_object->dateBirth;
                            $sync_data->gender = $gender ?: $user_object->gender;
                            $sync_response = create_user_igracias($sync_data, true);

                            if($sync_response['status']){
                                $update_data['personid'] = $sync_response['personid'];
                            }else{
                                $response->code = 200;
                                $response->status = false;
                                $response->message = $sync_response['message'];

                                // $log_request['password'] = $password ?: 'Qwerty1@';
                                // $log_response = format_log_data($response);
                                // insert_log('API_USER_UPDATE_FAILED', $log_request, $log_response);

                                log_api("API_USER_UPDATE_FAILED",$response);
                                $this->response($response, REST_Controller::HTTP_OK);
                            }
                        }

                        if($this->ion_auth->update($primary_key['id'], $update_data)){

                            $response->message = "Data updated successfully";
                            $response->code = 200;
                            $response->status = true;
                            
                            log_api("API_USER_UPDATE_SUCCESS",$response);
                        }else{

                            $response->message = "Failed to update data"; 
                            $response->code = 200;
                            $response->status = false;
                            
                            log_api("API_USER_UPDATE_FAILED",$response);
                        }
                    }else {

                        $response->code = 200;
                        $response->status = false;
                        $response->message = "Email has been used";
                        
                        log_api("API_USER_UPDATE_FAILED",$response);
                    }
                }else{
                    
                    $response->code = 200;
                    $response->status = false;
                    $response->message = "Data not found";
                    
                    log_api("API_USER_UPDATE_FAILED",$response);
                }
            }else{

                $response->code = 200;
                $response->status = false;
                $response->message = "Invalid token";
                $response->error_code = 403;
    
                log_api("API_USER_UPDATE_FAILED",$response);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }
     
        $this->response($response, REST_Controller::HTTP_OK);
    }
    
    public function password_check($str){
        $username = $this->input->post('username', true);
        if(strpos($str, $username) === false) return true;
        $this->form_validation->set_message('password_check', '{field} cannot contain username');
        return false;
    }

    public function new_password_check($str){
        $new_password = $this->input->post('new_password', true);
        $old_password = $this->input->post('old_password', true);
        if($new_password == '' && $old_password != ''){
            $this->form_validation->set_message('new_password_check', '{field} cannot empty when update password');
            return false;
        }
        return true;
    }

    public function old_password_check($str){
        $new_password = $this->input->post('new_password', true);
        $old_password = $this->input->post('old_password', true);
        if($new_password != '' && $old_password == ''){
            $this->form_validation->set_message('old_password_check', '{field} cannot empty when update password');
            return false;
        }
        return true;
    }
}
