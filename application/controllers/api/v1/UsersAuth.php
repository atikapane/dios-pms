<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class UsersAuth extends BaseAPIController {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_Model');
        $this->load->helper('log_api');
    }

	public function index_post()
	{
	    $this->credentials_check();

        $response = init_response();

        $oauth_provider = $this->input->post('oauth_provider', true);
        $identity = $this->input->post('email',true);
        $identity = trim($identity);

        $this->form_validation->set_rules('email', 'Email', 'required');

        if(empty($oauth_provider)){
            $password = $this->input->post('password',true);
            $this->form_validation->set_rules('password', 'Password', 'required');
    
            if ($this->form_validation->run()==TRUE) {
                if($this->ion_auth->login($identity, $password)){
                    $user = $this->ion_auth->user()->row();
                    $user_groups = $this->ion_auth->get_users_groups($user->id)->row();
                    $new_token = $this->generate_token($user->id);
    
                    $response->status = true;
                    $response->message = strip_tags($this->ion_auth->messages());
                    $response->error_code = 0;
                    $response->data = array(
                        "profile"=>$user,
                        "token"=>$new_token
                    );
                    
                    log_api("API_LOGIN_SUCCESS",$response);
                }else{
                    $response->data = new stdClass;
                    $response->status = false;
                    $response->message = strip_tags("Message error: ".$this->ion_auth->errors());
                    $response->error_code = 400;
                    log_api("API_LOGIN_FAILED",$response);
                }
            }else{
                $response->data = new stdClass;
                $response->status = false;
                $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
                $response->error_code = 400;
                
                log_api("API_LOGIN_FAILED",$response);
            }
        }else{
            $oauth_id = $this->input->post('oauth_id', true);

            $this->form_validation->set_rules('oauth_provider', 'Provider', 'required');
            $this->form_validation->set_rules('oauth_id', 'Id', 'required');

            if($this->form_validation->run() == TRUE){
                $user_object = $this->web_app_model->get_single_data('users', ['email' => $identity]);
                if(!empty($user_object)){
                    if($user_object->oauth_provider == $oauth_provider && $user_object->oauth_uid == $oauth_id){
                        if($this->ion_auth->login_oauth($user_object->email)){
                            $new_token = $this->generate_token($user_object->id);

                            $response->status = true;
                            $response->message = strip_tags($this->ion_auth->messages());
                            $response->error_code = 0;
                            $response->data = array(
                                "profile"=>$user_object,
                                "token"=>$new_token
                            );
                            
                            log_api("API_LOGIN_SUCCESS",$response);
                        }else{
                            $response->data = new stdClass();
                            $response->status = false;
                            $response->message = strip_tags("Message error: ".$this->ion_auth->errors());
                            $response->error_code = 400;
                            log_api("API_LOGIN_FAILED",$response);
                        }
                    }else{
                        $response->data = new stdClass();
                        $response->status = false;
                        $response->message = 'this ' . $oauth_provider . ' account is not associated with CeLOE account, please try to login manually by using your email and password';
                        $response->error_code = 400;
                        log_api("API_LOGIN_FAILED",$response);
                    }
                }else{
                    $response->data = new stdClass();
                    $response->status = false;
                    $response->message = 'This account is not registered, please register first in the registration menu';
                    $response->error_code = 400;
                    log_api("API_LOGIN_FAILED",$response);
                }
            }else{
                $response->data = new stdClass();
                $response->status = false;
                $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
                $response->error_code = 400;
                
                log_api("API_LOGIN_FAILED",$response);
            }
        }


        $this->response($response, REST_Controller::HTTP_OK);
	}
}

/* End of file Controllername.php */