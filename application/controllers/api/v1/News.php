<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/api/BaseAPIController.php");

class News extends BaseAPIController{

	function __construct() 
	{
		parent::__construct();
        $this->load->helper('log_api');
	}

	public function index_get()
    {
        $response = get_ajax_response();

        $start = $this->input->get("start");
        $limit = $this->input->get("limit");
        $search = $this->input->get("search");

        if(empty($start)){
            $start = 0;
        }

        if(empty($limit)){
            $limit = 10;
        }

        $where = array("deleted_at"=>null);
        
        $response->status = true;
        $response->data = $this->web_app_model->get_data_list("news", $search, "title", $start, $limit, "id", $where);
        foreach ($response->data as $value) {
            $value->thumbnail = base_url().$value->thumbnail;
        }

     
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function detail_get()
    {
        $response = get_ajax_response();

        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("slug" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('slug', 'Slug', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        } else {
            $slug = $this->input->get("slug");
        
            $where = array("slug"=>$slug,"deleted_at"=>null);

            $response->status = true;
            $response->data = $this->web_app_model->get_data_list("news", "", "", 0, 1, "id", $where);
            foreach ($response->data as $value) {
                $value->thumbnail = base_url().$value->thumbnail;
            }

            if(count($response->data) > 0){
                $response->data = $response->data[0];
            }else{
                $response->data = null;
            }
        }
     
        log_api("API_NEWS_DETAIL",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }
	
}
