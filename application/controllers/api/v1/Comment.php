<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/api/BaseAPIController.php");

class Comment extends BaseAPIController
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Web_App_Model');
        $this->load->model('Comment_Model');
        $this->load->helper('log_api');
    }

    public function index_get()
    {
        $response = get_ajax_response();

        $get_params = $this->input->get();

        $start = empty($get_params['start']) ? 0 : $get_params['start'];
        $limit = empty($get_params['limit']) ? 10 : $get_params['limit'];
        $course_id = empty($get_params['course_id']) ? "" : $get_params['course_id'];

        if (!empty($get_params)) {
            $this->form_validation->set_data($get_params);
            $this->form_validation->set_rules('course_id', 'Course Id', 'numeric');
            $this->form_validation->set_rules('start', 'Start', 'numeric');
            $this->form_validation->set_rules('limit', 'Limit', 'numeric');

            if ($this->form_validation->run() == FALSE) {
                $response->status = false;
                $response->error_code = 400;
                $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));

                $this->response($response, REST_Controller::HTTP_OK);
            }
        }


        $response->status = true;
        $response->error_code = 200;
        $response->data = $this->Comment_Model->get_comment($limit, $start, $course_id);
        log_api("API_COMMENT_LIST",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function reply_get(){
        $response = get_ajax_response();

        $get_params = $this->input->get();

        $start = empty($get_params['start']) ? 0 : $get_params['start'];
        $limit = empty($get_params['limit']) ? 10 : $get_params['limit'];
        $parent_id = empty($get_params['parent_id']);

        if (!empty($get_params)) {
            $this->form_validation->set_data($get_params);
            $this->form_validation->set_rules('parent_id', 'Parent Id', 'reqiuired|numeric');
            $this->form_validation->set_rules('start', 'Start', 'numeric');
            $this->form_validation->set_rules('limit', 'Limit', 'numeric');

            if ($this->form_validation->run() == FALSE) {
                $response->status = false;
                $response->error_code = 400;
                $response->message = "Message error: " . validation_errors();

                $this->response($response, REST_Controller::HTTP_OK);
            }
        }


        $response->status = true;
        $response->error_code = 200;
        $response->data = $this->Comment_Model->get_comment($limit, $start, "". $parent_id);
        log_api("API_COMMENT_REPLY",$response);
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function detail_get()
    {
        $response = init_response();

        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $response->status = false;
            $response->error_code = 400;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));

            log_api("API_COMMENT_DETAIL",$response);
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $comment_id = $this->input->get("id", true);
            if (!empty($comment_id)) {
                $query_data['id'] = $comment_id;
                $comment_object = $this->Comment_Model->get_detail($comment_id);
                if (!empty($comment_object)) {
                    $response->data = $comment_object;
                    $response->status = true;
                    $response->error_code = 200;
                    $response->message = "Get data success";

                    log_api("API_COMMENT_DETAIL",$response);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->status = false;
                    $response->error_code = 400;
                    $response->message = "Data not found";

                    log_api("API_COMMENT_DETAIL",$response);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "Id is required";
                $response->error_code = 400;

                log_api("API_COMMENT_DETAIL",$response);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function create_post()
    {
        // $this->credentials_check();

        $response = init_response();

        $this->form_validation->set_rules('course_id', 'Course Id', 'required');
        $this->form_validation->set_rules('comment', 'Comment', 'required');

        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        
        if ($this->form_validation->run() == FALSE) {
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
            $response->error_code = 400;

            log_api("API_COMMENT_CREATE",$response,$token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            if(!empty($token_user)){
                $user_id = $token_user->id;
                if (!empty($user_id)) {
                    $data = array(
                        'user_id' => $user_id,
                        'course_id' => $this->input->post('course_id', true),
                        'comment' => $this->input->post('comment', true),
                        'parent_id' => $this->input->post('parent_id', true) ?: 0
                    );
                    $this->Web_App_Model->insert_data('comments', $data);
        
                    $response->status = true;
                    $response->message = "Data submitted successfully";
                    $response->error_code = 200;
        
                    log_api("API_COMMENT_CREATE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->status = false;
                    $response->message = "User Id is required";
                    $response->error_code = 400;
    
                    log_api("API_COMMENT_CREATE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            }else{
                $response->status = false;
                $response->message = "Invalid token";
                $response->error_code = 403;
    
                log_api("API_COMMENT_CREATE",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
        }

    }

    public function update_post()
    {
        $this->credentials_check();

        $response = init_response();
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('comment', 'Comment', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
            $response->error_code = 400;

            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            if(!empty($token_user)){
                $id = $this->input->post("id", true);
                $comment = $this->input->post("comment", true);
    
                $primary_key['id'] = $id;
    
                $update_data['comment'] = $comment;
    
                $comment_object = $this->web_app_model->get_single_data("comments", $primary_key);
                if (!empty($comment_object)) {
                    $this->web_app_model->update_data("comments", $update_data, $primary_key['id'], "id");
    
                    $response->status = true;
                    $response->message = "Data updated successfully";
                    $response->error_code = 200;
    
                    log_api("API_COMMENT_UPDATE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                } else {
                    $response->status = false;
                    $response->message = "Data not found";
                    $response->error_code = 400;
    
                    log_api("API_COMMENT_UPDATE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "Invalid token";
                $response->error_code = 403;
    
                log_api("API_COMMENT_UPDATE",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }

            log_api("API_COMMENT_UPDATE",$response,$token_user);
        }
    }

    public function delete_get()
    {
        $this->credentials_check();

        $response = init_response();
        $mobile_token = $this->get_mobile_token_request();
        $token_user = $this->validate_token($mobile_token);
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
            $response->error_code = 400;

            log_api("API_COMMENT_DELETE",$response,$token_user);
            $this->response($response, REST_Controller::HTTP_OK);
        } else {
            if(!empty($token_user)){
                $comment_id = $this->input->get("id", true);
                if (!empty($comment_id)) {
                    $query_data['id'] = $comment_id;
                    $comment_object = $this->web_app_model->get_single_data("comments", $query_data);
                    if (!empty($comment_object)) {
                        $delete_data['id'] = $comment_id;
                        $this->web_app_model->delete_data("comments", $delete_data);

                        $response->status = true;
                        $response->message = "Data deleted successfully";
                        $response->error_code = 200;

                        log_api("API_COMMENT_DELETE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    } else {
                        $response->status = false;
                        $response->message = "Data not found";
                        $response->error_code = 400;

                        log_api("API_COMMENT_DELETE",$response,$token_user);
                        $this->response($response, REST_Controller::HTTP_OK);
                    }
                } else {
                    $response->status = false;
                    $response->message = "Id is required";
                    $response->error_code = 400;

                    log_api("API_COMMENT_DELETE",$response,$token_user);
                    $this->response($response, REST_Controller::HTTP_OK);
                }
            } else {
                $response->status = false;
                $response->message = "Invalid token";
                $response->error_code = 403;
    
                log_api("API_COMMENT_DELETE",$response,$token_user);
                $this->response($response, REST_Controller::HTTP_OK);
            }
            
        }

    }
}