<?php
/**
 * Created by PhpStorm.
 * User: djakapermana
 * Date: 17/11/19
 * Time: 08.47
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('News_model');
        $this->load->library('session');
    }

    public function index()
    {
        // $header["title"] = $body["title"] = "News";
        // $header["sub_menu"] = false;
        // $header['active_page'] = "news";
        // $this->load->view('fe_v2/nav_bar',$header);
        // $this->load->view('fe_v2/news/index');
        // $this->load->view('fe_v2/footer');

        $data["sub_menu"] = false;
        $data["title"] = "News";
        $data['active_page'] = "news";
        $this->load->view('fe_v2/news/index', $data);
    }

    public function get()
    {
        $query = $this->input->get("q");
        $limit = $this->input->get("l");
        $offset = $this->input->get("o");

        $data = $this->web_app_model->get_data_list("news", $query, "title", $offset, $limit, "id", false);
        foreach ($data as $single_data) {
            $single_data->thumbnail = base_url().$single_data->thumbnail;
            $single_data->description = str_replace("&nbsp;","",strip_tags($single_data->description));
            $single_data->date = date("d M Y",strtotime($single_data->news_date));
            $single_data->url = base_url("News/view/".$single_data->slug);
        }
        echo json_encode($data);
    }

    public function load_news(){
        $response = init_response();
        $search = $this->input->post('search',true);
        $start = $this->input->post('start',true);
        $limit = $this->input->post('limit',true);

        $data_news = $this->News_model->get_data_news($search, $start, $limit);
        $count_data_news = $this->News_model->get_all_data_news();

        if($data_news){
            $response->status = true;
            $response->data = $data_news;
            $response->num_of_rows = count($count_data_news);
        }else{
            $response->status = false;
            $response->data = array();
            $response->num_of_rows = count($count_data_news);
        }
        json_response($response);
    }

    public function view($slug)
    {
        $data["title"] = "Article";
        $data["sub_menu"] = true;
        $data['active_page'] = "news";

        if(is_numeric($slug)){
            $detail = $this->load_news_detail('id', $slug);
        }else{
            $detail = $this->load_news_detail('slug', $slug);
        }

        if($detail->status){
            $data["news_detail"] = $detail;
            $this->load->view('fe/news_detail/index', $data);
        }else{
            redirect('news', 'auto');
        }
    }

    public function load_news_detail($param, $slug){
        $response = init_response();
        $response->data_related = array();
        $response->data_detail = array();
        $params[$param] = $slug;
        $data_news_detail = $this->web_app_model->get_single_data('news',$params);

        if($data_news_detail){
            $related = explode(',',$data_news_detail->related);

            foreach ($related as $item){
                $sql[] = "SELECT title, thumbnail, news_date, description, slug, user.fullname FROM news LEFT JOIN user on user.empnum = news.created_by WHERE FIND_IN_SET('".$this->db->escape_str($item)."',related) AND deleted_at is null";
            }

            $data_related = $this->db->query(implode(' union ',$sql))->result();


            $response->status = true;
            $response->data_detail = $data_news_detail;
            $response->data_related = $data_related;

        }else{
            $response->status = false;
        }

        return $response;
    }

}

/* End of file Controllername.php */