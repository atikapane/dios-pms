<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Exam_schedule extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Web_app_model');
        $this->load->library('Sync');
        $this->load->helper('log_user_frontend');
        $this->load->helper('sync_mooc');
        $this->load->helper('sync_igracias');
        $this->load->helper('check_and_copy_exam');
        $this->load->library('session');
    }

    public function index()
    {
        $data['coordinators'] = $this->web_app_model->get_data('user');
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('exam_schedule/index.php', $data);
    }

    public function datatable()
    {

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'lms_exam_schedule as exam';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "paralel.id_lms_exam_schedule = exam.id";
        $join->table = "lms_list_id_course_paralel as paralel";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "course.course_id = paralel.id_course_paralel";
        $join->table = "sync_lms_course as course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "mcourse.course_id = exam.id_course_master";
        $join->table = "sync_lms_course as mcourse";
        array_push($extra_join, $join);
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "exam.coordinator_lecturer = user.id";
        $join->table = "user";
        array_push($extra_join, $join);

        $group_by = "exam.id";

        $select_fields = array("exam.id", "exam.status", "exam.exam_type","exam.id_course_master", "CONCAT(mcourse.subject_code,' / ',mcourse.class) as course_master", "exam.total_exam_participant", "exam.period", "exam.exam_date", "exam.exam_start_time", "exam_end_time", "GROUP_CONCAT(CONCAT(course.subject_code,' / ',course.class )) as course_paralel", 'user.fullname', 'user.employeeid');
        $search_fields = array("exam_type", "period", "id_course_master", "exam_date", 'user.fullname', 'user.employeeid');

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields, $group_by);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $data[] = $result;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function get_coordinator(){
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $course_id = $this->input->get("id", true);
            if (!empty($course_id)) {
                $course_object = $this->web_app_model->get_single_data('sync_lms_course', ['course_id' => $course_id]);
                if(!empty($course_object)) $user_object = $this->web_app_model->get_single_data('user', ['employeeid' => $course_object->employeeid]);
            }

            $response->error_code = 200;
            $response->status = true;
            $response->coordinator = empty($user_object) ? null : $user_object->id;
        }

        echo json_encode($response);
    }

    public function udpate_status(){
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('id', 'Exam Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post('id', true);
            $exam_object = $this->web_app_model->get_single_data('lms_exam_schedule', ['id' => $id]);

            if(!empty($exam_object)){
                $this->web_app_model->update_data("lms_exam_schedule", ['status' => 1], $id, "id");

                $task_objects = $this->web_app_model->get_data('lms_exam_schedule_task', ['lms_exam_schedule_id' => $id, 'status' => 4]);
                foreach($task_objects as $task){
                    $this->web_app_model->update_data("lms_exam_schedule_task", ['status' => 1], $task->id, "id");   
                }

                $response->code = 200;
                $response->status = true;
                $response->message = "Data updated successfully";
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }

        echo json_encode($response);
    }

    public function get()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $where = [];
        $q = $this->input->get('q', true);
        if(!empty($q)) $where = ['class LIKE' => '%' . $q . '%', 'subject_code LIKE' => '%' . $q . '%'];
        $courses = $this->Web_app_model->get_data_or_where('sync_lms_course', $where);
        $arr = [];
        foreach($courses as $course){
            $data = new stdClass();
            $data->id = $course->course_id;
            $data->text = $course->subject_code . ' / ' . $course->class;
            $arr[] = $data;
        }
        

        $response->code = 200;
        $response->data = $arr;
        $response->status = true;
        $response->message = "Get data success";

        echo json_encode($response);
    }

    public function get_exam_schedule()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Exam ID', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->get("id", true);
            $exam_object = null;
            if (!empty($id)) {
                $query_data['lms_exam_schedule.id'] = $id;
                $select = "lms_exam_schedule.id, lms_exam_schedule.coordinator_lecturer, lms_exam_schedule.exam_type, lms_exam_schedule.id_course_master as id_course_master, lms_exam_schedule.total_exam_participant, lms_exam_schedule.period, lms_exam_schedule.exam_date, lms_exam_schedule.exam_start_time, lms_exam_schedule.exam_end_time, GROUP_CONCAT(CONCAT(sync_lms_course.subject_code, ' / ', sync_lms_course.class)) as pcourse,CONCAT(mcourse.subject_code,' / ',mcourse.class) as course_master,  GROUP_CONCAT(CONCAT(sync_lms_course.course_id)) as course_paralel_id";
                $exam_object = $this->web_app_model->get_singel_exam_schedule_data("lms_exam_schedule", "lms_list_id_course_paralel", "sync_lms_course", "lms_list_id_course_paralel.id_lms_exam_schedule = lms_exam_schedule.id", "sync_lms_course.course_id = lms_list_id_course_paralel.id_course_paralel", "mcourse.course_id = lms_exam_schedule.id_course_master", $select, "lms_exam_schedule.id", $query_data);
            }

            $response->code = 200;
            $response->data = $exam_object;

            if (!empty($exam_object)) {
                $response->status = true;
                $response->message = "Get data success";
            } else {
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function get_detail()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Exam ID', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->get("id", true);
            $exam_object = $this->web_app_model->get_data_join('lms_exam_schedule as exam', 'sync_lms_course as course', '', 'exam.id_course_master = course.course_id', '', ['exam.id' => $id]);
            if(!empty($exam_object)){
                $exam_object = $exam_object[0];
                $exam_object->task = $this->web_app_model->get_data_join('lms_exam_schedule_task as task', 'sync_lms_course as course', '', 'task.paralel_course_id = course.course_id', '', ['task.lms_exam_schedule_id' => $id]);
            }

            $response->code = 200;
            $response->data = $exam_object;

            if (!empty($exam_object)) {
                $response->status = true;
                $response->message = "Get data success";
            } else {
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function save()
    {
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('exam_type', 'Exam Type', 'required');
        $this->form_validation->set_rules('total_exam_participant', 'Total Exam Participant', 'required');
        $this->form_validation->set_rules('period', 'Period', 'required');
        $this->form_validation->set_rules('exam_date', 'Exam Date', 'required');
        $this->form_validation->set_rules('exam_start_time', 'Exam Start Time', 'required');
        $this->form_validation->set_rules('exam_end_time', 'Exam End Time', 'required');
        $this->form_validation->set_rules('id_course_master', 'ID Course Master', 'required');
        $this->form_validation->set_rules('coordinator_lecturer', 'Coordinator Lecturer', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $exam_type = $this->input->post("exam_type", true);
            $total_exam_participant = $this->input->post("total_exam_participant", true);
            $period = $this->input->post("period", true);
            $exam_date = $this->input->post("exam_date", true);
            $exam_start_time = $this->input->post("exam_start_time", true);
            $exam_end_time = $this->input->post("exam_end_time", true);
            $id_course_master = $this->input->post("id_course_master", true);
            $list_id_course_paralel = $this->input->post("list_id_course_paralel");
            $coordinator = $this->input->post('coordinator_lecturer', true);

            $success = check_section($id_course_master, $exam_type); // error check faultString
            if(isset($success->status)){
                if($success->status){
                    $insert_data['exam_type'] = $exam_type;
                    $insert_data['total_exam_participant'] = $total_exam_participant;
                    $insert_data['period'] = $period;
                    $insert_data['exam_date'] = $exam_date;
                    $insert_data['exam_start_time'] = $exam_start_time;
                    $insert_data['exam_end_time'] = $exam_end_time;
                    $insert_data['id_course_master'] = $id_course_master;
                    $insert_data['coordinator_lecturer'] = $coordinator;
                    // $insert_data['list_id_course_paralel'] = implode(',',$list_id_course_paralel);
        
                    $this->web_app_model->insert_data("lms_exam_schedule", $insert_data);
        
                    $last_exam_id = $this->web_app_model->get_last_id('lms_exam_schedule', 'id');
                    
                    $insert_task['lms_exam_schedule_id'] = $last_exam_id;
                    $insert_task['master_course_id'] = $id_course_master;
                    $insert_task['exam_type'] = $exam_type;

                    foreach ($list_id_course_paralel as $list_course) {
                        $insert_list['id_lms_exam_schedule'] = $last_exam_id;
                        $insert_list['id_course_paralel'] = $list_course;
                        $this->web_app_model->insert_data("lms_list_id_course_paralel", $insert_list);
                        
                        $insert_task['paralel_course_id'] = $list_course;
                        $this->web_app_model->insert_data("lms_exam_schedule_task", $insert_task);
                    }
        
                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data submitted successfully";
                }else{
                    $response->code = 400;
                    $response->message = $success->message;
                }
            }else{
                $response->code = 400;
                $response->message = $success->faultString;
            }
        }
        echo json_encode($response);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('exam_type_edit', 'Exam Type', 'required');
        $this->form_validation->set_rules('total_exam_participant_edit', 'Total Exam Participant', 'required');
        $this->form_validation->set_rules('period_edit', 'Period', 'required');
        $this->form_validation->set_rules('exam_date_edit', 'Exam Date', 'required');
        $this->form_validation->set_rules('exam_start_time_edit', 'Exam Start Time', 'required');
        $this->form_validation->set_rules('exam_end_time_edit', 'Exam End Time', 'required');
        $this->form_validation->set_rules('id_course_master_edit', 'ID Course Master', 'required');
        $this->form_validation->set_rules('coordinator_lecturer', 'Coordinator Lecturer', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $exam_type = $this->input->post("exam_type_edit", true);
            $total_exam_participant = $this->input->post("total_exam_participant_edit", true);
            $period = $this->input->post("period_edit", true);
            $exam_date = $this->input->post("exam_date_edit", true);
            $exam_start_time = $this->input->post("exam_start_time_edit", true);
            $exam_end_time = $this->input->post("exam_end_time_edit", true);
            $id_course_master = $this->input->post("id_course_master_edit", true);
            $list_id_course_paralel = $this->input->post("list_id_course_paralel_edit");
            $coordinator = $this->input->post("coordinator_lecturer", true);

            $success = check_section($id_course_master, $exam_type); // error check faultString
            if(isset($success->status)){
                if($success->status){
                    $update_data['exam_type'] = $exam_type;
                    $update_data['total_exam_participant'] = $total_exam_participant;
                    $update_data['period'] = $period;
                    $update_data['exam_date'] = $exam_date;
                    $update_data['exam_start_time'] = $exam_start_time;
                    $update_data['exam_end_time'] = $exam_end_time;
                    $update_data['id_course_master'] = $id_course_master;
                    $update_data['coordinator_lecturer'] = $coordinator;
                    $update_data['status'] = 1;
        
                    $primary_key['id'] = $this->input->post('edit_id', true);
        
                    $exam_object = $this->web_app_model->get_single_data("lms_exam_schedule", $primary_key);
                    if (!empty($exam_object)) {
                        $delete_item['id_lms_exam_schedule'] = $primary_key['id'];
                        $this->web_app_model->delete_data("lms_list_id_course_paralel", $delete_item);

                        $delete_task['lms_exam_schedule_id'] = $primary_key['id'];
                        $this->web_app_model->delete_data("lms_exam_schedule_task", $delete_task);

                        $this->web_app_model->update_data("lms_exam_schedule", $update_data, $primary_key['id'], "id");
                        
                        $insert_task['lms_exam_schedule_id'] = $primary_key['id'];
                        $insert_task['master_course_id'] = $id_course_master;
                        $insert_task['exam_type'] = $exam_type;
                        foreach ($list_id_course_paralel as $list_course) {
                            $insert_list['id_lms_exam_schedule'] = $primary_key['id'];
                            $insert_list['id_course_paralel'] = $list_course;
                            $this->web_app_model->insert_data("lms_list_id_course_paralel", $insert_list);

                            $insert_task['paralel_course_id'] = $list_course;
                            $this->web_app_model->insert_data("lms_exam_schedule_task", $insert_task);
                        }
        
                        $response->code = 200;
                        $response->status = true;
                        $response->message = "Data updated successfully";
                    } else {
                        $response->code = 400;
                        $response->status = false;
                        $response->message = "Data not found";
                    }
                }else{
                    $response->code = 400;
                    $response->message = $success->message;
                }
            }else{
                $response->code = 400;
                $response->message = $success->faultString;
            }
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if (!$this->can_delete()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $exam_id = $this->input->get("id", true);
            if (!empty($exam_id)) {
                $query_data['id'] = $exam_id;
                $exam_object = $this->web_app_model->get_single_data("lms_exam_schedule", $query_data);
                if (!empty($exam_object)) {
                    $delete_data['id'] = $exam_id;
                    $this->web_app_model->delete_data("lms_exam_schedule", $delete_data);

                    $delete_item['id_lms_exam_schedule'] = $exam_id;
                    $this->web_app_model->delete_data("lms_list_id_course_paralel", $delete_item);

                    $delete_task['lms_exam_schedule_id'] = $exam_id;
                    $this->web_app_model->delete_data("lms_exam_schedule_task", $delete_task);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                } else {
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }
        }
        echo json_encode($response);
    }

    public function export(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $query = $this->input->post("query",true);

        $table = 'lms_exam_schedule as exam';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "paralel.id_lms_exam_schedule = exam.id";
        $join->table = "lms_list_id_course_paralel as paralel";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "course.course_id = paralel.id_course_paralel";
        $join->table = "sync_lms_course as course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "mcourse.course_id = exam.id_course_master";
        $join->table = "sync_lms_course as mcourse";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "exam.coordinator_lecturer = user.id";
        $join->table = "user";
        array_push($extra_join, $join);

        $extra_where =false;
        $extra_where_or=false; 
        //$extra_join=false; 
        // $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $order = "exam.id";
        $group_by = "exam.id";
        $filter = $search;

        $select_fields = array("exam.id", "exam.exam_type","exam.id_course_master", "CONCAT(mcourse.subject_code,' / ',mcourse.class) as course_master", "exam.total_exam_participant", "exam.period", "exam.exam_date", "exam.exam_start_time", "exam_end_time", "GROUP_CONCAT(CONCAT(course.subject_code,' / ',course.class )) as course_paralel", 'user.fullname', 'user.employeeid');
        $search_fields = array("exam_type", "period", "id_course_master", "exam_date", 'user.fullname', 'user.employeeid');

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $select_fields, $order, $filter, $search_fields, $column_type,$start,$limit, $group_by);

        $arr = array();
        foreach ($result as $row) {
            $arr[] = array($row->course_master, $row->exam_type,$row->total_exam_participant,$row->period, $row->exam_date,$row->exam_start_time, $row->exam_end_time ,$row->course_paralel, $row->fullname . ' ( ' . $row->employeeid . ' )');
         }
         $filed = array('Course Mater','Exam Type','Total Participant','Period','Date', 'Start Time','End Time', 'Course Paralel', 'Coordinator');
         //do export
        export_csv($arr,$filed);         
    }

    public function print(){
         //get value
         $search = $this->input->post('search');   
         //load helper
         $this->load->model('Datatable_model');
         $this->load->helper('export');
 
         $query = $this->input->post("query",true);
 
         $table = 'lms_exam_schedule as exam';
         $extra_where = array();
         $extra_where_or = array();
         $extra_join = array();
 
         $join = new stdClass();
         $join->join_type = "INNER";
         $join->condition = "paralel.id_lms_exam_schedule = exam.id";
         $join->table = "lms_list_id_course_paralel as paralel";
         array_push($extra_join, $join);
 
         $join = new stdClass();
         $join->join_type = "INNER";
         $join->condition = "course.course_id = paralel.id_course_paralel";
         $join->table = "sync_lms_course as course";
         array_push($extra_join, $join);
 
         $join = new stdClass();
         $join->join_type = "INNER";
         $join->condition = "mcourse.course_id = exam.id_course_master";
         $join->table = "sync_lms_course as mcourse";
         array_push($extra_join, $join);

         $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "exam.coordinator_lecturer = user.id";
        $join->table = "user";
        array_push($extra_join, $join);
 
         $extra_where =false;
         $extra_where_or=false; 
         //$extra_join=false; 
         // $custom_select=false;       
         $column_type=false;
         $start=false;
         $limit=false;
         $order = "exam.id";
         $group_by = "exam.id";
         $filter = $search;
 
         $select_fields = array("exam.id", "exam.exam_type","exam.id_course_master", "CONCAT(mcourse.subject_code,' / ',mcourse.class) as course_master", "exam.total_exam_participant", "exam.period", "exam.exam_date", "exam.exam_start_time", "exam_end_time", "GROUP_CONCAT(CONCAT(course.subject_code,' / ',course.class )) as course_paralel", 'user.fullname', 'user.employeeid');
        $search_fields = array("exam_type", "period", "id_course_master", "exam_date", 'user.fullname', 'user.employeeid');
 
         $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $select_fields, $order, $filter, $search_fields, $column_type,$start,$limit, $group_by);
         $response = get_ajax_response();
         if ($result){
             $data = $this->load->view("exam_schedule/print",array("data"=>$result),true);
             $response->code = 200;
             $response->status = true;
             $response->message = "Load Data Successfully";
             $response->data = $data;
         } else {
             $response->code = 400;
             $response->status = true;
             $response->message = "Fail Load Data";
             $response->data = null;
         }
 
         echo json_encode($response);       
        
    }

    public function get_total_student(){
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $get_params = $this->input->get();
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('exam_date', 'Exam Date', 'required');
        $this->form_validation->set_rules('exam_start_time', 'Exam Start Time', 'required');
        $this->form_validation->set_rules('exam_end_time', 'Exam End Time', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $exam_date = date('Y-m-d', strtotime($this->input->get('exam_date', true)));
            $exam_start_time = date('H:i:s', strtotime($this->input->get('exam_start_time', true)));
            $exam_end_time = date('H:i:s', strtotime($this->input->get('exam_end_time', true)));
            $where = [
                "exam_date = '{$exam_date}'",
                "exam_start_time < '{$exam_end_time}'",
                "exam_end_time > '{$exam_start_time}'",
            ];
            $participant_sum = $this->web_app_model->get_sum('lms_exam_schedule', 'total_exam_participant', $where);
            
            $max_student = $this->config->item('max_student_exam');
            $response->status = true;
            $response->code = 200;
            if(!empty($participant_sum)){
                $response->total_student = $max_student - $participant_sum;
            }else{
                $response->total_student = $max_student;

            }
        }

        echo json_encode($response);
    }
}
