<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Exam_Topic extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    private function get_user_position_level(){
        $user_info = get_login_info();
        $position_list = $this->config->item('positionlist');
        $data['prodi_position'] = array_search(array("SEK.PRODI","KETUA PRODI"), $position_list);
        $user_position = 0;
        foreach($position_list as $key => $val){
            foreach($val as $position){
                if($position == $user_info['positionlevel']){
                    return $key;
                    break;
                }
            }
        }
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('exam_topic/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'lms_exam_schedule';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("lms_exam_schedule.id", "exam_type", 'period', 'exam_date', 'exam_start_time', 'exam_end_time', 'subject_code', 'class');
        $select_fields = array("lms_exam_schedule.id", "exam_type", 'total_exam_participant', 'period', 'exam_date', 'exam_start_time', 'exam_end_time', 'topic_activity', 'subject_code', 'class');
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.course_id = lms_exam_schedule.id_course_master";
        $join->table = "sync_lms_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.category_id = schoolyear.category_id";
        $join->table = "sync_category as schoolyear";
        array_push($extra_join, $join);
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "schoolyear.category_parent_id = studyprogram.category_id";
        $join->table = "sync_category as studyprogram";
        array_push($extra_join, $join);
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "studyprogram.category_parent_id = faculty.category_id";
        $join->table = "sync_category as faculty";
        array_push($extra_join, $join);

        array_push($extra_where, "lms_exam_schedule.status = 3");

        $user_position = $this->get_user_position_level();
        $position_list = $this->config->item('positionlist');
        $prodi = array_search(array("SEK.PRODI","KETUA PRODI"), $position_list);
        $dekan = array_search(array("WAKIL DEKAN","DEKAN FAKULTAS"), $position_list);

        if(!$this->is_super_admin){
            if($user_position == $prodi){
                array_push($extra_where, "studyprogram.category_id = '{$this->unit_id}'");
            }else if($user_position == $dekan){
                array_push($extra_where, "faculty.category_id = '{$this->directorate_id}'");
            }else{
                $user_logged_id = get_logged_in_id();
                array_push($extra_where, "lms_exam_schedule.coordinator_lecturer = '{$user_logged_id}'");
            }
        }

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $parallel = $this->web_app_model->get_data_join('lms_exam_schedule', 'lms_list_id_course_paralel', 'sync_lms_course', 'lms_exam_schedule.id = lms_list_id_course_paralel.id_lms_exam_schedule', 'sync_lms_course.course_id = lms_list_id_course_paralel.id_course_paralel', ['lms_list_id_course_paralel.id_lms_exam_schedule' => $result->id], 'subject_code, class');
                if(!empty($parallel)){
                    $parallel_arr = [];
                    foreach($parallel as $paralel){
                        $parallel_arr[] = $paralel->subject_code . ' / ' . $paralel->class;
                    }
                    $result->parallel = implode(', ', $parallel_arr);
                }else{
                    $result->parallel = '';
                }
                $data[] = $result;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);

            $primary_key['id'] = $id;

            $update_data['topic_activity'] = 1;

            $this->web_app_model->update_data("lms_exam_schedule", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data verified successfully";
        }

        echo json_encode($response);
    }

    public function export(){   

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $query = $this->input->post("query", true);

        $table = 'lms_exam_schedule';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("lms_exam_schedule.id", "exam_type", 'period', 'exam_date', 'exam_start_time', 'exam_end_time', 'subject_code', 'class');
        $select_fields = array("lms_exam_schedule.id", "exam_type", 'total_exam_participant', 'period', 'exam_date', 'exam_start_time', 'exam_end_time', 'topic_activity', 'subject_code', 'class');
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.course_id = lms_exam_schedule.id_course_master";
        $join->table = "sync_lms_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.course_id = lms_exam_schedule.id_course_master";
        $join->table = "sync_lms_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.category_id = schoolyear.category_id";
        $join->table = "sync_category as schoolyear";
        array_push($extra_join, $join);
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "schoolyear.category_parent_id = studyprogram.category_id";
        $join->table = "sync_category as studyprogram";
        array_push($extra_join, $join);
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "studyprogram.category_parent_id = faculty.category_id";
        $join->table = "sync_category as faculty";
        array_push($extra_join, $join);

        $user_position = $this->get_user_position_level();
        $position_list = $this->config->item('positionlist');
        $prodi = array_search(array("SEK.PRODI","KETUA PRODI"), $position_list);
        $dekan = array_search(array("WAKIL DEKAN","DEKAN FAKULTAS"), $position_list);

        if(!$this->is_super_admin){
            if($user_position == $prodi){
                array_push($extra_where, "studyprogram.category_id = '{$this->unit_id}'");
            }else if($user_position == $dekan){
                array_push($extra_where, "faculty.category_id = '{$this->directorate_id}'");
            }else{
                $user_logged_id = get_logged_in_id();
                array_push($extra_where, "lms_exam_schedule.coordinator_lecturer = '{$user_logged_id}'");
            }
        }

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;  

        $arr = array();
        foreach ($list as $row) {  
            $parallel = $this->web_app_model->get_data_join('lms_exam_schedule', 'lms_list_id_course_paralel', 'sync_lms_course', 'lms_exam_schedule.id = lms_list_id_course_paralel.id_lms_exam_schedule', 'sync_lms_course.course_id = lms_list_id_course_paralel.id_course_paralel', ['lms_list_id_course_paralel.id_lms_exam_schedule' => $row->id], 'subject_code, class');
            if(!empty($parallel)){
                $parallel_arr = [];
                foreach($parallel as $paralel){
                    $parallel_arr[] = $paralel->subject_code . ' / ' . $paralel->class;
                }
                $row->parallel = implode(', ', $parallel_arr);
            }else{
                $row->parallel = '';
            }
            $arr[] = array($row->id,$row->exam_type,$row->total_exam_participant,$row->period,$row->exam_date,$row->exam_start_time,$row->exam_end_time,$row->subject_code . ' / ' . $row->class,$row->parallel);
        }
        $field = array('Id','Exam Type', 'Total Exam Participant', 'Period', 'Exam Date', 'Exam Start Time', 'Exam End Time', 'Course Master', 'Course Parallel');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print(){   

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $query = $this->input->post("query", true);

        $table = 'lms_exam_schedule';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("lms_exam_schedule.id", "exam_type", 'period', 'exam_date', 'exam_start_time', 'exam_end_time', 'subject_code', 'class');
        $select_fields = array("lms_exam_schedule.id", "exam_type", 'total_exam_participant', 'period', 'exam_date', 'exam_start_time', 'exam_end_time', 'topic_activity', 'subject_code', 'class');
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.course_id = lms_exam_schedule.id_course_master";
        $join->table = "sync_lms_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.course_id = lms_exam_schedule.id_course_master";
        $join->table = "sync_lms_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.category_id = schoolyear.category_id";
        $join->table = "sync_category as schoolyear";
        array_push($extra_join, $join);
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "schoolyear.category_parent_id = studyprogram.category_id";
        $join->table = "sync_category as studyprogram";
        array_push($extra_join, $join);
        
        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "studyprogram.category_parent_id = faculty.category_id";
        $join->table = "sync_category as faculty";
        array_push($extra_join, $join);

        $user_position = $this->get_user_position_level();
        $position_list = $this->config->item('positionlist');
        $prodi = array_search(array("SEK.PRODI","KETUA PRODI"), $position_list);
        $dekan = array_search(array("WAKIL DEKAN","DEKAN FAKULTAS"), $position_list);

        if(!$this->is_super_admin){
            if($user_position == $prodi){
                array_push($extra_where, "studyprogram.category_id = '{$this->unit_id}'");
            }else if($user_position == $dekan){
                array_push($extra_where, "faculty.category_id = '{$this->directorate_id}'");
            }else{
                $user_logged_id = get_logged_in_id();
                array_push($extra_where, "lms_exam_schedule.coordinator_lecturer = '{$user_logged_id}'");
            }
        }

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;  

        $arr = array();
        foreach ($list as $row) {  
            $parallel = $this->web_app_model->get_data_join('lms_exam_schedule', 'lms_list_id_course_paralel', 'sync_lms_course', 'lms_exam_schedule.id = lms_list_id_course_paralel.id_lms_exam_schedule', 'sync_lms_course.course_id = lms_list_id_course_paralel.id_course_paralel', ['lms_list_id_course_paralel.id_lms_exam_schedule' => $row->id], 'subject_code, class');
            if(!empty($parallel)){
                $parallel_arr = [];
                foreach($parallel as $paralel){
                    $parallel_arr[] = $paralel->subject_code . ' / ' . $paralel->class;
                }
                $row->parallel = implode(', ', $parallel_arr);
            }else{
                $row->parallel = '';
            }
            $arr[] = $row;
        }

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("exam_topic/print",array("data"=>$arr),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);   
		     
    }
}
