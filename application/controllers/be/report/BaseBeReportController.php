<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class BaseBeReportController extends BaseBEController {
	
	function __construct() 
	{
		parent::__construct();	
        $this->load->model("Report_model");
	}

    public function get_my_faculty(){
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $filter = array('category_type'=>'FACULTY');
        $clearance_level = $this->clearance_level;
        if ($clearance_level < 4 && !$this->is_super_admin){
            $filter['category_id'] = $this->directorate_id;
        }
        $my_faculty = $this->web_app_model->get_data_and_where('sync_category',$filter,'sync_category.category_name');
        
        $response->code = 200;
        $response->data = $my_faculty;

        if(!empty($my_faculty)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }

        echo json_encode($response);
    }

	public function get_my_study_program(){
        $faculty_id = $this->input->get("faculty_id");
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $filter = array('category_type'=>'STUDYPROGRAM');
        if (!empty($faculty_id)){
            $filter["category_parent_id"] = $faculty_id;
        }
        $clearance_level = $this->clearance_level;
        if ($clearance_level < 4 && !$this->is_super_admin){
            $filter['category_parent_id'] = $this->directorate_id;
            if ($clearance_level <= 2){
                $filter['category_id'] = $this->unit_id;    
            }
        }
		
		$my_subjects = $this->web_app_model->get_data_and_where('sync_category',$filter,'sync_category.category_name');
        
    	$response->code = 200;
    	$response->data = $my_subjects;

    	if(!empty($my_subjects)){
        	$response->status = true;
        	$response->message = "Get data success";
    	}else{
        	$response->status = false;
        	$response->message = "Data not found";
    	}

        echo json_encode($response);
	}

	public function get_my_subject(){
        $study_program_id = $this->input->get("study_program_id");
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $filter = array();
        if (!empty($study_program_id)){
            $filter["studyprogram.category_id"] = $study_program_id;
        }
        $clearance_level = $this->clearance_level;
        if ($clearance_level < 2){
            $user_id = get_logged_in_id();
            $filter = array('user_id'=>$user_id,'type_id'=>2);
            $my_subjects = $this->web_app_model->get_data_join("course_development","sync_course","enrol_development","course_development.subject_id = sync_course.subject_id","course_development.id = enrol_development.course_id",$filter,"subject_code,subject_name","subject_code","subject_code");
        }
        else {
            if ($clearance_level < 4){
                $filter['studyprogram.category_parent_id'] = $this->directorate_id;
                if ($clearance_level <= 2){
                    $filter["studyprogram.category_id"] = $this->unit_id;
                }
            }
            $my_subjects = $this->web_app_model->get_data_join("course_development","sync_course","sync_category studyprogram","course_development.subject_id = sync_course.subject_id","sync_course.category_id = studyprogram.category_id",$filter,"sync_course.subject_id as subject_id,subject_code,subject_name","","subject_code");  
        }

    	$response->code = 200;
    	$response->data = $my_subjects;

    	if(!empty($my_subjects)){
        	$response->status = true;
        	$response->message = "Get data success";
    	}else{
        	$response->status = false;
        	$response->message = "Data not found";
    	}
        echo json_encode($response);
	}
    //Start Filter Course Management
    public function get_my_subject_development(){
        $year_id = $this->input->get("year_id");
        $quarter_id = $this->input->get("quarter_id");
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $user_id = get_logged_in_id();
        
        $filter = array('user_id'=>$user_id,'type_id'=>2);
        if (!empty($year_id)){
            $filter["year"] = $year_id;
        }
        if (!empty($quarter_id)){
            $filter["quarter"] = $quarter_id;
        }
        
        $my_subjects = $this->web_app_model->get_data_join("course_development","sync_course","enrol_development","course_development.subject_id = sync_course.subject_id","course_development.id = enrol_development.course_id",$filter,"subject_code,subject_name","subject_code","subject_code");

        $response->code = 200;
        $response->data = $my_subjects;

        if(!empty($my_subjects)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function get_my_subject_teaching(){
        $semester_id = $this->input->get("semester_id");
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $user_id = get_logged_in_id();
        
        $filter = array('user_id'=>$user_id);
        if (!empty($semester_id)){
            $filter["semester"] = $semester_id;
        }
        
        $my_subjects = $this->web_app_model->get_data_and_where('sync_lms_course',$filter,'','subject_code');

        $response->code = 200;
        $response->data = $my_subjects;

        if(!empty($my_subjects)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function get_my_class_teaching(){
        $semester_id = $this->input->get("semester_id");
        $subject_id = $this->input->get("subject_id");
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $user_id = get_logged_in_id();
        
        $filter = array('user_id'=>$user_id);
        if (!empty($semester_id)){
            $filter["semester"] = $semester_id;
        }
        if (!empty($subject_id)){
            $filter["subject_code"] = $subject_id;
        }
        
        $my_classes = $this->web_app_model->get_data_and_where('sync_lms_course',$filter,'','class');

        $response->code = 200;
        $response->data = $my_classes;

        if(!empty($my_classes)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function get_cource_change_subject(){
        $semester = $this->input->get("semester");
        $study_program_id = $this->input->get("study_program_id");
        $faculty_id = 0;
        
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        if($this->clearance_level <= 1){
            $user_id = get_logged_in_id();
        }else{
            if ($this->clearance_level < 4){
                $faculty_id = $this->directorate_id;
                if ($this->clearance_level < 3){
                    if ($study_program_id == 0){
                        $study_program_id = $this->unit_id;    
                    }
                }
            }
            $user_id = 0;
        }
        $my_subjects = $this->Report_model->get_lms_course($semester,$faculty_id,$study_program_id,$user_id);
        
        $response->code = 200;
        $response->data = $my_subjects;

        if(!empty($my_subjects)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    //End Filter Course Management

    public function get_my_subject_semester()
    {
        $semester_id = $this->input->get("semester_id");
        $study_program_id = $this->input->get("study_program_id");
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        if($this->clearance_level <= 1){
            $user_id = get_logged_in_id();
        }else{
            $user_id = 0;
        }
        $my_subjects = $this->Report_model->get_lms_course($semester,$study_program_id,$user_id);

        $response->code = 200;
        $response->data = $my_subjects;

        if(!empty($my_subjects)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    // Start Filter Open Course Management

    public function get_role()
    {
        $enrolType = $this->input->get("enrol_type");
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $filter = array('status'=>$enrolType);
        $role = $this->web_app_model->get_data_and_where('enrol_type',$filter,'enrol_type.enrol_name');

        $response->code = 200;
        $response->data = $role;

        if(!empty($role)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    // End Filter Open Course Management

}