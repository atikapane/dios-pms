<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Development_monitoring extends BaseBEController {
	function __construct() 
	{
		parent::__construct();							
	}


	public function index()
	{		
        $this->load->view('report/monitoring_course_development');
	}

	public function course_development_data(){
        $response = init_response();
        $response->status = true;
        $data = array();
        $data[] = array("no"=>1,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Informatika","status_review"=>"OK");
        $data[] = array("no"=>2,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Elektronika","status_review"=>"Tidak OK");
        $response->data = $data;
        json_response($response);
    }

    public function course_development_data_prodi(){
        $response = init_response();
        $response->status = true;
        $data = array();
        $data[] = array("no"=>1,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Elektronika","prodi"=>"Teknik Elektronika","status_review"=>"OK");
        $data[] = array("no"=>2,"years"=>2019,"triwulan"=>1,"faculty"=>"Fakultas Teknik Elektronika","prodi"=>"Teknik Telekomunikasi","status_review"=>"Tidak OK");
        $response->data = $data;
        json_response($response);
    }

    public function course_development_data_mk(){
        $response = array();
        $response['status'] = true;
        $response['recordsTotal'] = 5;
        $response['recordsFiltered'] = 5;
        $data = array();
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"jajang",8=>"rahmat",9=>"OK",10=>"OK",11=>"12 Oktober 2019");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Informatika",4=>"Teknik Informatika",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"herman",8=>"neneng",9=>"Tidak OK",10=>"OK",11=>"12 Oktober 2019");
        $data[] = array(0=>1,1=>2019,2=>1,3=>"Fakultas Teknik Elektronika",4=>"Teknik Telekomunikasi",5=>"CS1234",6=>"Pengantar Teknik Informatika",7=>"herman",8=>"neneng",9=>"Tidak OK",10=>"OK",11=>"12 Oktober 2019");
        $data[] = array(0=>2,1=>2019,2=>1,3=>"Fakultas Teknik Elektronika",4=>"Teknik Telekomunikasi",5=>"TT1234",6=>"Jaringan Komputer",7=>"made",8=>"siti",9=>"Tidak OK",10=>"OK",11=>"13 Oktober 2019");
        $data[] = array(0=>2,1=>2019,2=>1,3=>"Fakultas Teknik Elektronika",4=>"Teknik Telekomunikasi",5=>"TT1234",6=>"Jaringan Komputer",7=>"bayu",8=>"asep",9=>"Tidak OK",10=>"OK",11=>"13 Oktober 2019");
        $response['data'] = $data;
        json_response($response);
    }

	public function monitoring_course_development(){
		$this->load->view('report/monitoring_course_development');
	}

	public function course_change(){
		$this->load->view('report/course_change');
	}

	public function instructor_attendance(){
		$this->load->view('report/instructor_attendance');
	}


	public function student_attendance(){
		$this->load->view('report/student_attendance');
	}

}