<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Submenu extends BaseBEController {

    function __construct() 
    {
        parent::__construct();  
        $this->load->model('Menu_Model','m_menu');  
            
    }
    
    public function index()
    {       
        //TODO : CHECK PRIVILEGE        
        //TODO : CHECK LOGIN        
        $data['menus'] = $this->Web_App_Model->get_data_all('menu'); 
        $data['breadcrumb'] = $this->breadcrumb;    
        $data['controller_full_path'] = $this->full_path;  
        $data['privilege'] = $this->privilege;    
        $this->load->view('submenu/index.php',$data);
    }

    public function datatable()
    {       
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }             
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'submenu';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("menu.menu_name as menu_name",
                                "submenu.submenu_name as submenu_name","submenu.submenu_link as submenu_link",
                                "submenu.submenu_icon as icon","submenu.submenu_id as submenu_id", "submenu.priority"
                               );
        $search_fields = array("menu_name","submenu_name");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "menu.menu_id = submenu.menuid";
        $join->table = "menu";
        array_push($extra_join,$join);
       
        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
               
                $row->submenu_id = $result->submenu_id;
                $row->menu_name = $result->menu_name;
                $row->submenu_name = $result->submenu_name;
                $row->submenu_link = $result->submenu_link;
                $row->submenu_icon = $result->icon;
                $row->priority = $result->priority;
                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function export(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="submenu";
        $order = "submenu_id"; 
        $filter = $search;
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $column_search =  array("menu.menu_name as menu_name",
                                "submenu.submenu_name as submenu_name","submenu.submenu_link as submenu_link",
                                "submenu.submenu_icon as icon","submenu.submenu_id as submenu_id"
                               );
        $column_search = array("menu_name","submenu_name");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "menu.menu_id = submenu.menuid";
        $join->table = "menu";
        array_push($extra_join,$join);

        //$column_search = array("id", "type", "url");
        //$extra_where =false;
        //$extra_where_or=false; 
        //$extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        foreach ($result as $row) {
            $link = base_url().$row->submenu_link;
            $arr[] = array($row->submenu_id,$row->menu_name,$row->submenu_name,$link);
         }
        $filed = array('Id','Menu','Sub Menu','Link');
        //do export
        export_csv($arr,$filed);        
    }

    public function print_submenu(){
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="submenu";
        $order = "submenu_id"; 
        $filter = $search;
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $column_search =  array("menu.menu_name as menu_name",
                                "submenu.submenu_name as submenu_name","submenu.submenu_link as submenu_link",
                                "submenu.submenu_icon as icon","submenu.submenu_id as submenu_id"
                               );
        $column_search = array("menu_name","submenu_name");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "menu.menu_id = submenu.menuid";
        $join->table = "menu";
        array_push($extra_join,$join);

        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        
        $response = get_ajax_response();
        
        if ($result){
            $data = $this->load->view("submenu/print",array("data"=>$result),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response); 
    }

    public function get()
    {       
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Submenu ID', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $submenu_id = $this->input->get("id",true);
            $submenu_object = null;
            if(!empty($submenu_id)){
                $query_data['submenu_id'] = $submenu_id;
                $group_object = $this->web_app_model->get_single_data("submenu",$query_data);
            }

            $response->code = 200;
            $response->data = $group_object;

            if(!empty($group_object)){
                $response->status = true;
                $response->message = "Get data success";
            }else{
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function get_order(){
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  

        $menu_id = $this->input->get('menu', true);
        $order_object = $this->web_app_model->get_data("submenu", ['menuid' => $menu_id], '', 'priority');
        $response->code = 200;
        $response->data = $order_object;
        $response->status = true;
        $response->message = "Get data success";

        echo  json_encode($response);
    }

    public function save()
    {       
        $response = get_ajax_response();
        if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('menu_id', 'Menu Name', 'required');
        $this->form_validation->set_rules('submenu_name', 'Submenu Name', 'required');
        $this->form_validation->set_rules('submenu_link', 'Submenu Link', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $menu_id = $this->input->post("menu_id",true);
            $submenu_name = $this->input->post("submenu_name",true);
            $submenu_link = $this->input->post("submenu_link",true);
            $count = $this->web_app_model->get_data_distinct("submenu", ['menuid' => $menu_id], 'count(*) as count');
			$priority = $count[0]->count + 1;
    
            $insert_data['menuid'] = $menu_id;
            $insert_data['submenu_name'] = $submenu_name;
            $insert_data['submenu_link'] = $submenu_link;
            $insert_data['priority'] = $priority;

            $this->web_app_model->insert_data("submenu",$insert_data);

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }

    public function save_order(){
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $priorities = $this->input->post('order_array');
        if (empty($priorities)){
            $response->code = 400;
            $response->status = true;
            $response->message = "Data Empty";
        }else{
            $x = 1;
            foreach ($priorities as $priority){
                $primary_key['id'] = $priority;

                $update_data['priority'] = $x++;

                $this->web_app_model->update_data("submenu", $update_data, $primary_key['id'], "submenu_id");
            }

            $response->code = 200;
            $response->status = true;
            $response->priority = $priorities;
            $response->message = "Data updated successfully";
        }
        echo json_encode($response);
    }

   public function delete()
    {       
        $response = get_ajax_response();
        if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Previlege ID', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $submenu_id = $this->input->get("id",true);
            if(!empty($submenu_id)){
                $query_data['submenu_id'] = $submenu_id;
                $submenu_object = $this->web_app_model->get_single_data("submenu",$query_data);
                if(!empty($submenu_object)){
                    $delete_data['submenu_id'] = $submenu_id;
                    $this->web_app_model->delete_data("submenu",$delete_data);
                
                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Previlege_id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {       
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('menuid', 'Menu Name', 'required');
        $this->form_validation->set_rules('submenu_name', 'Submenu Name', 'required');
        $this->form_validation->set_rules('submenu_link', 'Submenu Link', 'required');
        
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $submenu_id = $this->input->post("submenu_id",true);    
            $menu_id = $this->input->post("menuid",true);
            $submenu_name = $this->input->post("submenu_name",true);    
            $submenu_link = $this->input->post("submenu_link",true);
            
            $primary_key['submenu_id'] = $submenu_id;

            $update_data['menuid'] = $menu_id;
            $update_data['submenu_name'] = $submenu_name;
            $update_data['submenu_link'] = $submenu_link;
            
            $submenu_object = $this->web_app_model->get_single_data("submenu",$primary_key);
          
            if(!empty($submenu_object)){
                $this->web_app_model->update_data("submenu",$update_data,$primary_key['submenu_id'],"submenu_id");

                $response->code = 200;
                $response->status = true;
                $response->message = "Data updated successfully";
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }
}
