<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Log extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('log/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'log_setting';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "log_setting.log_action_id = log_action.id";
        $join->table = "log_action";
        array_push($extra_join,$join);

        $select_fields = array('log_setting.id', 'log_action.key', 'log_setting.is_log');
        $search_fields = array("log_setting.id", "log_action.key");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->key = $result->key;
                $row->is_log = $result->is_log;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('is_log', 'Log', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);
            $is_log = $this->input->post("is_log", true);

            $primary_key['id'] = $id;

            $update_data['is_log'] = $is_log;

            $this->web_app_model->update_data("log_setting", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data updated successfully";
        }

        echo json_encode($response);
    }

    public function export(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        //filter vars
        $query = $this->input->post('query', true);

        $table = 'log_setting';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "log_setting.log_action_id = log_action.id";
        $join->table = "log_action";
        array_push($extra_join,$join);

        $select_fields = array('log_setting.id', 'log_action.key', 'log_setting.is_log');
        $search_fields = array("log_setting.id", "log_action.key");

        $result = $this->Datatable_model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;  

        $arr = array();
        if(!empty($list)){
            foreach ($list as $result) {
                $result->is_log = $result->is_log == 1 ? 'Yes' : 'No';
                $arr[] = array(
                    $result->key, 
                    $result->is_log,
                );
            }
        }
        $field = array('Log Action', 'Log');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print_log(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        //filter vars
        $query = $this->input->post('query', true);

        $table = 'log_setting';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "log_setting.log_action_id = log_action.id";
        $join->table = "log_action";
        array_push($extra_join,$join);

        $select_fields = array('log_setting.id', 'log_action.key', 'log_setting.is_log');
        $search_fields = array("log_setting.id", "log_action.key");

        $result = $this->Datatable_model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;

        $arr = array();
        if(!empty($list)){
            foreach ($list as $result) {
                $arr[] = array(
                    "action"=>$result->key,
                    "log"=>$result->is_log == 1 ? 'Yes' : 'No', 
                );
            }
        }

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("log/print",array("data"=>$arr),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);    
		     
    }
}
