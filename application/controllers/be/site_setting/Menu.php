<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Menu extends BaseBEController {

	function __construct() 
	{
		parent::__construct();	
		$this->load->model('Menu_Model','m_menu');	
			
	}
	
	public function index()
	{		
		//TODO : CHECK PRIVILEGE		
		//TODO : CHECK LOGIN	
		$data['icons']	= $this->m_menu->get_menuicons();
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;	
		$data['privilege'] = $this->privilege;   
		$this->load->view('menu/index.php',$data);
	}

	public function datatable()
	{		

		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }				
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'menu';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("menu.menu_id as menu_id","menu.menu_name as menu_name",
        						"menu.menu_link as menu_link","menu_icon as icon", "priority"
        	                   );
        $search_fields = array("menu_id","menu_name");
       
        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
               
                $row->menu_id = $result->menu_id;
                $row->menu_name = $result->menu_name;
                $row->menu_link = $result->menu_link;
                $row->menu_icon = $result->icon;
                $row->priority = $result->priority;
                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
	}

	public function export(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="menu";
        $order = "menu_id"; 
        $filter = $search;
		$extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $column_search =  array("menu.menu_id as menu_id","menu.menu_name as menu_name",
        						"menu.menu_link as menu_link","menu_icon as icon",
        	                   );
        $column_search = array("menu_id","menu_name");
        //$extra_where =false;
        //$extra_where_or=false; 
        //$extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        foreach ($result as $row) {			
            $arr[] = array($row->menu_id,$row->menu_name,$row->menu_icon);
         }
        $filed = array('Id','Menu','Icon');
        //do export
        export_csv($arr,$filed);        
	}
	
	public function print_menu(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="menu";
        $order = "menu_id"; 
        $filter = $search;
		$extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $column_search =  array("menu.menu_id as menu_id","menu.menu_name as menu_name",
        						"menu.menu_link as menu_link","menu_icon as icon",
        	                   );
        $column_search = array("menu_id","menu_name");
        //$extra_where =false;
        //$extra_where_or=false; 
        //$extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("menu/print",array("data"=>$result),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);      
    }

	public function get()
	{		
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Menu ID', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$menu_id = $this->input->get("id",true);
        	$group_object = null;
        	if(!empty($menu_id)){
        		$query_data['menu_id'] = $menu_id;
        		$group_object = $this->web_app_model->get_single_data("menu",$query_data);
        	}

        	$response->code = 200;
        	$response->data = $group_object;

        	if(!empty($group_object)){
	        	$response->status = true;
	        	$response->message = "Get data success";
        	}else{
	        	$response->status = false;
	        	$response->message = "Data not found";
        	}
        }
        echo json_encode($response);
	}

	public function get_order(){
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
		}  
		
        $order_object = $this->web_app_model->get_data("menu", '', '', 'priority');
        $response->code = 200;
        $response->data = $order_object;
        $response->status = true;
        $response->message = "Get data success";

        echo  json_encode($response);
    }

	public function save()
	{		
		$response = get_ajax_response();
		if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$this->form_validation->set_rules('menu_name', 'Menu Name', 'required');
		$this->form_validation->set_rules('menu_icon', 'Menu Icon', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$menu_name = $this->input->post("menu_name",true);
			$menu_icon = $this->input->post("menu_icon",true);
			$count = $this->web_app_model->get_data_distinct("menu", [], 'count(*) as count');
			$priority = $count[0]->count + 1;

        	//$upload_path = $this->config->item('group_upload_path');

        	$insert_data['menu_name'] = $menu_name;
        	$insert_data['menu_icon'] = $menu_icon;
        	$insert_data['priority'] = $priority;

        	$this->web_app_model->insert_data("menu",$insert_data);

        	$response->code = 200;
        	$response->status = true;
        	$response->message = "Data submitted successfully";
        }
        echo json_encode($response);
	}

	public function save_order(){
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $priorities = $this->input->post('order_array');
        if (empty($priorities)){
            $response->code = 400;
            $response->status = true;
            $response->message = "Order Empty";
        }else{
            $x = 1;
            foreach ($priorities as $priority){
                $primary_key['id'] = $priority;

                $update_data['priority'] = $x++;

                $this->web_app_model->update_data("menu", $update_data, $primary_key['id'], "menu_id");
            }

            $response->code = 200;
            $response->status = true;
            $response->priority = $priorities;
            $response->message = "Data updated successfully";
        }
        echo json_encode($response);
    }

	public function delete()
	{		
		$response = get_ajax_response();
		if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Menu ID', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$menu_id = $this->input->get("id",true);
        	if(!empty($menu_id)){
        		$menu['menu_id'] = $menu_id;
        		$menu_object = $this->web_app_model->get_single_data("menu",$menu);
        		if(!empty($menu_object)){
	        		$submenu['menuid'] = $menu_id;
                    $submenu_object = $this->web_app_model->get_single_data("submenu",$submenu);
                    $menu['menu_id'] = $menu_id;
                    $this->web_app_model->delete_data("menu",$menu);
	        		if(!empty($submenu_object)){
	        			$submenu_id['menuid'] = $menu_id;
	        			$this->web_app_model->delete_data("submenu",$submenu_id);
	        	
			        	$response->code = 200;
			        	$response->status = true;
			        	$response->message = "Data menu and submenu deleted successfully";
	        		}else{
	        			$response->code = 200;
			        	$response->status = true;
			        	$response->message = "Data menu deleted successfully";
	        		}
        		}else{
		        	$response->code = 400;
		        	$response->status = false;
		        	$response->message = "Data not found";
        		}
        	}else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Id is required";
        	}

        }
        echo json_encode($response);
	}

	public function edit()
	{		
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$this->form_validation->set_rules('menu_name', 'Menu Name', 'required');
		$this->form_validation->set_rules('menu_icon', 'Menu Icon', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$menu_id = $this->input->post("menu_id",true);	
        	$menu_name = $this->input->post("menu_name",true);	
        	$menu_icon = $this->input->post("menu_icon",true);	
        	
       		$primary_key['menu_id'] = $menu_id;

        	$update_data['menu_name'] = $menu_name;
        	$update_data['menu_icon'] = $menu_icon;

			$menu_object = $this->web_app_model->get_single_data("menu",$primary_key);

			if(!empty($menu_object)){
	        	$this->web_app_model->update_data("menu",$update_data,$primary_key['menu_id'],"menu_id");

	        	$response->code = 200;
	        	$response->status = true;
	        	$response->message = "Data updated successfully";
			}else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Data not found";
			}
        }
        echo json_encode($response);
	}
}
