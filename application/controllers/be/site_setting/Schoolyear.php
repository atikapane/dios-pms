<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Schoolyear extends BaseBEController {

	function __construct() 
	{
		parent::__construct();
	}
	
	public function index()
	{		
		//TODO : CHECK PRIVILEGE		
		//TODO : CHECK LOGIN	
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;	
		$data['privilege'] = $this->privilege;   
		$this->load->view('schoolyear/index.php',$data);
	}

	public function datatable()
	{		

		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }				
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);
        
        /*menukar hasil sorting desc menjadi asc dan sebalikanya karena pada database :
        semester : odd -> 1, even -> 2
        status : inactive -> 0, active -> 1*/
        if(!empty($sort) && is_array($sort) && array_key_exists("field", $sort) && array_key_exists("sort", $sort)){
            if($sort["field"] == "semester" || $sort["field"] == "status"){
                if($sort["sort"] == "desc"){
                    $sort["sort"] = "asc";
                } elseif ($sort["sort"] == "asc"){
                    $sort["sort"] = "desc";
                }
            }
        }

        $table = 'schoolyear';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("id","year","semester","description","status");
        $search_fields = array("year","semester","status");
       
        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
               
                $row->id = $result->id;
                $row->year = $result->year;
                $row->semester = ($result->semester == 1 ? "Odd" : "Even");
                $row->description = $result->description;
                $row->status = $result->status;
                
                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
	}

	public function export(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="schoolyear";
        $order = "id"; 
        $filter = $search;
		$extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("id","year","semester","description","status");
        $search_fields = array("year","semester","status");
        $column_search=false;
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        foreach ($result as $row) {
            $row->semester = $row->semester == 1 ? "Odd" : "Even";
            $row->status = $row->status == 1 ? "Active" : "Inactive";
            $arr[] = array($row->id,$row->year,$row->semester,$row->description,$row->status);
         }
        $filed = array('Id','Schoolyear','Semester','Description','Status');
        //do export
        export_csv($arr,$filed);        
	}
	
	public function print_schoolyear(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="schoolyear";
        $order = "id"; 
        $filter = $search;
		$extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("id","year","semester","description","status");
        $search_fields = array("year","semester","status");
        $column_search=false;
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("schoolyear/print",array("data"=>$result),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);      
    }

	public function get()
	{		
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){ 
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Id', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$id = $this->input->get("id",true);
        	$schoolyear_object = null;
        	if(!empty($id)){
        		$query_data['id'] = $id;
        		$schoolyear_object = $this->web_app_model->get_single_data("schoolyear",$query_data);
                $schoolyear_object->status = $schoolyear_object->status == 1 ? 1 : 2;
        	}

        	$response->code = 200;
        	$response->data = $schoolyear_object;

        	if(!empty($schoolyear_object)){
	        	$response->status = true;
	        	$response->message = "Get data success";
        	}else{
	        	$response->status = false;
	        	$response->message = "Data not found";
        	}
        }
        echo json_encode($response);
	}

    public function status_active()
    {
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $schoolyear_id = $this->input->get("id",true);
            if(!empty($schoolyear_id)){
                $query_data['id'] = $schoolyear_id;
                $tutorial_object = $this->web_app_model->get_single_data("schoolyear",$query_data);
                if(!empty($tutorial_object)){
                    $status = $tutorial_object->status;
                    $data["status"] = ($status == 1 ? 0 : 1);
                    $this->web_app_model->update_data("schoolyear",$data,$schoolyear_id,"id");
                    $response->message = "Status changed successfully";

                    $response->code = 200;
                    $response->status = true;
                    
                }
                else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

	public function save()
	{		
		$response = get_ajax_response();
		if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$this->form_validation->set_rules('year', 'Year', 'required');
		$this->form_validation->set_rules('semester', 'Semester', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$year = $this->input->post("year",true);
			$semester = $this->input->post("semester",true);
            $description = $this->input->post("description",true);
            $status = $this->input->post("status",true);

        	$insert_data['year'] = $year;
            $insert_data['semester'] = $semester;
            

            $schoolyear_object = $this->web_app_model->get_single_data("schoolyear",$insert_data);
            if (empty($schoolyear_object)){
                $insert_data['description'] = $description;
                $insert_data['status'] = $status == 1 ? 1 : 0;
                $this->web_app_model->insert_data("schoolyear",$insert_data);

                $response->code = 200;
                $response->status = true;
                $response->message = "Data submitted successfully";
            }
            else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Data ".$year."/".$semester." is already exist";
            }
        }
        echo json_encode($response);
	}

	public function delete()
	{		
		$response = get_ajax_response();
		if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Id', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$id = $this->input->get("id",true);
        	if(!empty($id)){
        		$schoolyear['id'] = $id;
        		$schoolyear_object = $this->web_app_model->get_single_data("schoolyear",$schoolyear);
        		if(!empty($schoolyear_object)){
                    $this->web_app_model->delete_data("schoolyear",$schoolyear);
                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
        		}
                else{
		        	$response->code = 400;
		        	$response->status = false;
		        	$response->message = "Data not found";
        		}
        	}
            else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Id is required";
        	}

        }
        echo json_encode($response);
	}

	public function edit()
	{		
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$this->form_validation->set_rules('year', 'Year', 'required');
		$this->form_validation->set_rules('semester', 'Semester', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$id = $this->input->post("id",true);	
        	$year = $this->input->post("year",true);	
        	$semester = $this->input->post("semester",true);
            $description = $this->input->post("description",true);
            $status = $this->input->post("status",true);
        	
       		$primary_key['id'] = $id;

        	$update_data['year'] = $year;
        	$update_data['semester'] = $semester;

			$schoolyear_object = $this->web_app_model->get_single_data("schoolyear",$primary_key);

			if(!empty($schoolyear_object)){
                $update_data['id!='] = $id;
                $schoolyear_duplicate = $this->web_app_model->get_single_data("schoolyear",$update_data);
                if (empty($schoolyear_duplicate)){
                    unset($update_data['id!=']);
                    $update_data['description'] = $description;
                    $update_data['status'] = $status == 1 ? 1 : 0;

    	        	$this->web_app_model->update_data("schoolyear",$update_data,$primary_key['id'],"id");

    	        	$response->code = 200;
    	        	$response->status = true;
    	        	$response->message = "Data updated successfully";
                }
                else {
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data ".$year."/".$semester." is already exist";
                }
			}else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Data not found";
			}
        }
        echo json_encode($response);
	}
}
