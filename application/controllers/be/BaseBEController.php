<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseBEController extends CI_Controller {
	protected $path_sub_folder = "";
	protected $path_controller = "";
	protected $full_path = "";
	protected $semester_active = "";

	protected $breadcrumb;
	protected $privilege = array("can_read"=>false,"can_delete"=>false,"can_update"=>false,"can_create"=>false);
	protected $clearance_level = 0;
	protected $directorate_id = 0;
	protected $unit_id = 0;
	protected $is_super_admin = 0;

	function __construct() 
	{
		parent::__construct();	
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->model("Menu_Model");
		$this->load->model("Web_App_Model");
		$this->load->model('Group_Model','m_group');
        $this->load->model('Auth_Model');
        $this->load->model('Users_Model');
        $this->load->model('Previlege_Model');
        $this->load->model('Role_Model','m_role');
		$this->load->helper(array('form', 'url'));

		$this->generate_breadcrumb();
        $this->load_privilege();
        $this->get_semester_active();

        if(!is_logged_in()){
        	redirect('/auth');
        }	
	}

	public function generate_breadcrumb(){
		$this->breadcrumb = new stdClass();
		$this->path_sub_folder = $this->uri->segment(2);
		$this->path_controller = $this->uri->segment(3);

		$this->full_path = "be/".$this->path_sub_folder."/".$this->path_controller;
		$menu_object = $this->Menu_Model->get_menu_by_submenu_link($this->full_path);
		
		if(!empty($menu_object)){
			$this->breadcrumb->parent_id = $menu_object->menu_id;
			$this->breadcrumb->parent = $menu_object->menu_name;
			$this->breadcrumb->child_id = $menu_object->submenu_id;
			$this->breadcrumb->child = $menu_object->submenu_name;
			$this->breadcrumb->icon = $menu_object->menu_icon;
		}else{
			$this->breadcrumb->parent_id = 0;
			$this->breadcrumb->parent = "";
			$this->breadcrumb->child_id = 0;
			$this->breadcrumb->child = "";
			$this->breadcrumb->icon = "";
		}
	}

	protected function load_privilege(){
		$user_id = get_logged_in_id();
		$this->clearance_level = $this->Previlege_Model->get_clearance_level($user_id);
		$privileges = $this->Previlege_Model->get_privilege($this->breadcrumb->child_id,$user_id);
		if ($this->clearance_level<4){
			$structure = $this->Previlege_Model->get_structure_attribute($user_id);
			if (!empty($structure)){
				$this->directorate_id = $structure->c_directorate_id;
				$this->unit_id = $structure->c_unit_id;
			}
		}
		
		$can_read = false;
		$can_update = false;
		$can_delete = false;
		$can_create = false;
		foreach ($privileges as $privilege) {
			$can_read = $can_read || $privilege->read == 1;
			$can_update = $can_update || $privilege->update == 1;
			$can_delete = $can_delete || $privilege->delete == 1;
			$can_create = $can_create || $privilege->create == 1;
		}
		
		$this->privilege["can_read"] = $can_read;
		$this->privilege["can_update"] = $can_update;
		$this->privilege["can_delete"] = $can_delete;
		$this->privilege["can_create"] = $can_read;

		//cast to object for ease of access
		$this->privilege = (object) $this->privilege;

		$super_admin_group_id = $this->config->item('super_admin_group_id');
		if(empty($super_admin_group_id) || !is_int($super_admin_group_id)){
			$super_admin_group_id = 0;
		}
		$this->is_super_admin = $this->Previlege_Model->is_super_admin($user_id,$super_admin_group_id);
	}

	protected function get_semester_active(){
		$data = $this->Web_App_Model->get_single_data("config_semester",array("status_active"=>1));
		if (!empty($data)){
			$this->semester_active = $data->semester;
		}
	}

	protected function get_graph_jwt(){
		$this->load->library('general');
		$this->load->library('graph');
		$datetime = $this->general->get_datetime();
		$account = array(
			"graph_url_oauth" => $this->config->item('graph_url_oauth'),
            "graph_client_id" => $this->config->item('graph_client_id'),
            "graph_client_secret" => $this->config->item('graph_client_secret'),
		);
		$data = $this->Web_App_Model->get_single_data("graph_jwt",array("id"=>1));
		if (!empty($data)){
			$expires_in = $data->expires_in;
			if (strtotime($datetime) >= strtotime($expires_in)){
				$token_data = $this->graph->oauth($account);
				if (!empty($token_data["access_token"])){
					unset($token_data["ext_expires_in"]);
					$token_data["expires_in"] = $this->general->get_datetime("plus",$token_data["expires_in"]);
					$this->Web_App_Model->update_data("graph_jwt",array("access_token"=>$token_data["access_token"],"expires_in"=>$token_data["expires_in"]),1,"id");
					return $token_data["access_token"];
				}
			}
			else {
				return $data->access_token;
			}
		}
		else {
			$token_data = $this->graph->oauth($account);
			if (!empty($token_data["access_token"])){
				unset($token_data["ext_expires_in"]);
				$token_data["id"] = 1;
				$token_data["expires_in"] = $this->general->get_datetime("plus",$token_data["expires_in"]);
				$this->Web_App_Model->insert_data("graph_jwt",$token_data);
				return $token_data["access_token"];
			}
		}
	}

	protected function can_read(){
		return $this->privilege->can_read;
	}
	
	protected function can_update(){
		return $this->privilege->can_update;
	}
	
	protected function can_create(){
		return $this->privilege->can_create;
	}
	
	protected function can_delete(){
		return $this->privilege->can_delete;
	}
}