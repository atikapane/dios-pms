<?php
/**
 * Created by PhpStorm.
 * User: djakapermana
 * Date: 19/11/19
 * Time: 08.08
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class News extends BaseBEController {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('News_model');
        $this->load->library('session');
    }

    public function index()
	{
        $user = $this->session->userdata("sess_auth");
        if(isset($user) || true){
            $data['breadcrumb'] = $this->breadcrumb;
            $data['controller_full_path'] = $this->full_path;
            $data['privilege'] = $this->privilege; 
            $this->load->view('news/index', $data);
        }else{
            redirect('auth', 'auto');
        }
	}

    public function datatable()
    {
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }           
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'news';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("title","description");
        array_push($extra_where, "(deleted_at is null)");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->title = $result->title;
                $row->news_date = $result->news_date;
                $row->updated_at = $result->updated_at;
                $data[] = $row;
            }
        }

        $output = array(
            "meta" => $meta,
            "data" => $data
        );

        json_response($output);
    }

    public function image_upload()
    {
        if(!$this->can_create() && !$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $config['upload_path'] = FCPATH.$this->config->item('news_upload_path');
        if(!file_exists($config['upload_path'])){
            mkdir($config['upload_path'],0777,true);
        }
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['encrypt_name'] = false;
        $config['encrypt_name'] = true;

//        $max_size_upload = $this->config->item('banner_max_upload');
//        if(!empty($max_size_upload)){
//            $config['max_size'] = $max_size_upload;
//        }

        $this->load->library('upload',$config);

        if($this->upload->do_upload('file')){
            $upload_data = $this->upload->data();
            echo $upload_data['file_name'];
        }else{
            header('HTTP/1.0 400 Bad Request');
            echo strip_tags($this->upload->display_errors());
        }
    }

    public function save(){
        $response = init_response();
        if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $user = $this->session->userdata("sess_auth");
        if(isset($user) || true){
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('image_name', 'Thumbnail', 'required');
            $this->form_validation->set_rules('news_date', 'News Date', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $response->code = 400;
                $response->message = implode("<br>", $this->form_validation->error_array());
            }
            else
            {
                $title = $this->security->xss_clean($this->input->post("title"));
                $description = $this->input->post("description");
                $image_name = $this->input->post("image_name",true);
                $news_date = $this->input->post("news_date",true);

                $upload_path = $this->config->item('news_upload_path');

                $image_name = $upload_path.$image_name;

                $tag_related = json_decode($this->input->post("tag_related_add"));
                $related_arr = array();
                $relates = "";
                if(!empty($tag_related)){
                    foreach ($tag_related as $item){
                        array_push($related_arr,$item->value);
                    }

                    $relates = implode(',',$related_arr);
                }

//                $insert_data['id'] = uuid();
                $insert_data['title'] = strip_tags($title);
                $insert_data['description'] = $description;
                $insert_data['thumbnail'] = $image_name;
                $insert_data["created_by"] = $user;
                $insert_data["slug"] = slug('slug',strip_tags($title),'news','id');
                $insert_data["related"] = $relates;
                $insert_data['news_date'] = $news_date;

                $this->web_app_model->insert_data("news",$insert_data);


                $response->code = 200;
                $response->status = true;
                $response->message = "Data submitted successfully";
            }
            json_response($response);
        }else{
            redirect('auth', 'auto');
        }
    }


    public function get()
    {
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $news_id = $this->input->get("id",true);
            $news_object = null;
            if(!empty($news_id)){
                $query_data['id'] = $news_id;
                $news_object = $this->web_app_model->get_single_data("news",$query_data);
                if(!empty($news_object)){
                    $news_object->img_path = base_url().$news_object->thumbnail;
                }
            }

            $response->code = 200;
            $response->data = $news_object;

            if(!empty($news_object)){
                $response->status = true;
                $response->message = "Get data success";
            }else{
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
       if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $news_id = $this->input->get("id",true);
            if(!empty($news_id)){
                $query_data['id'] = $news_id;
                $news_object = $this->web_app_model->get_single_data("news",$query_data);
                if(!empty($news_object)){
                    $delete_data['id'] = $news_id;
                    $this->web_app_model->delete_data("news",$delete_data);
                    unlink(FCPATH.$news_object->thumbnail);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }

        json_response($response);
    }

    public function edit()
    {
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $response = init_response();
        $user = $this->session->userdata("sess_auth");
        if(isset($user) || true){
            $this->form_validation->set_rules('id', 'Id', 'required');
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('image_name', 'Thumbnail', 'required');
            $this->form_validation->set_rules('news_date', 'News Date', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $response->code = 400;
                $response->message = implode("<br>", $this->form_validation->error_array());
            }
            else
            {
                $id = $this->input->post("id",true);
                $title = $this->input->post("title",true);
                $description = $this->input->post("description");
                $image_name = $this->input->post("image_name",true);


                $upload_path = $this->config->item('news_upload_path');
                $image_name = $upload_path.$image_name;

                $primary_key['id'] = $id;

                $banner_object = $this->web_app_model->get_single_data("news",$primary_key);

                $tag_related = json_decode($this->input->post("tag_related_edit"));
                $related_arr = array();
                if(count($tag_related)>0){
                    foreach ($tag_related as $item){
                        array_push($related_arr,$item->value);
                    }
                    $relates = implode(',',$related_arr);
                }else{
                    $relates = "";
                }

                $news_date = $this->input->post("news_date",true);


                $update_data['title'] = strip_tags($title);
                $update_data['description'] = $description;
                $update_data['thumbnail'] = $image_name;
                $update_data["created_by"] = $user;
                $update_data["related"] = $relates;
                $update_data['news_date'] = $news_date;

                if($update_data['title']!=$banner_object->title){
                    $update_data["slug"] = slug('slug',strip_tags($title),'news','id');
                }

                if(!empty($banner_object)){

                    $this->web_app_model->update_data("news",$update_data,$primary_key['id'],"id");

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data updated successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }

        }else{
            $response->code = 401;
            $response->status = false;
            $response->message = "User No Authorization";
        }

        json_response($response);
    }

    function slug($field,$slug,$incerement=0) {

        $this->db->select('ID');
        $this->db->where($field,$slug);
        $this->db->order_by($field,'asc');
        $news = $this->db->get('news')->row_array();

        $new_slug = $news[$field]+'-'+$count+1;
        if($count){
            $this->slug($field,$new_slug);
        } else {
            return $new_slug;
        }
    }

    public function export(){   

        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }           
        $this->load->model('Datatable_model');

        $query = $this->input->post("query",true);

        $table = 'news';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("title","description");
        array_push($extra_where, "(deleted_at is null)");

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $arr = array();
        foreach ($list as $row) {  
            $arr[] = array($row->id,$row->title,$row->updated_at);
        }
        $field = array('Id','Title','Updated Date');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print_news(){   

        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }           
        $this->load->model('Datatable_model');
        
        $query = $this->input->post("query",true);

        $table = 'news';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("title","description");
        array_push($extra_where, "(deleted_at is null)");

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("news/print",array("data"=>$list),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);   
		     
    }

}

/* End of file Controllername.php */