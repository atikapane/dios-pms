<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Comments extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('comment/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'comments';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields = array("comments.id as id", "users.full_name as full_name", "ocw_course.ocw_name as ocw_name", "sync_course.subject_name", "sync_course.subject_code", "comments.comment");
        $search_fields = array("comments.id", "ocw_course.ocw_name", "users.full_name", "sync_course.subject_name", "sync_course.subject_code");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "comments.user_id = users.id";
        $join->table = "users";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "comments.course_id = ocw_course.id";
        $join->table = "ocw_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "ocw_course.sync_course_id = sync_course.subject_id";
        $join->table = "sync_course";
        array_push($extra_join, $join);

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->full_name = $result->full_name;
                $row->ocw_name = $result->ocw_name;
                $row->subject_name = $result->subject_name;
                $row->subject_code = $result->subject_code;
                $row->comment = $result->comment;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function get()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $comment_id = $this->input->get("id", true);
            $comment_object = null;
            if (!empty($comment_id)) {
                $query_data['id'] = $comment_id;
                $comment_object = $this->web_app_model->get_single_data("comments", $query_data);
            }
            $response->code = 200;
            $response->data = $comment_object;

            if (!empty($comment_object)) {
                $response->status = true;
                $response->message = "Get data success";
            } else {
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if (!$this->can_delete()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id[]" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id[]', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $comment_id = $this->input->get("id[]", true);
            if (!empty($comment_id)) {
                $x = 0;
                foreach($comment_id as $id){
                    $query_data['id'] = $id;
                    $comment_object = $this->web_app_model->get_single_data("comments", $query_data);
                    if (!empty($comment_object)) {
                        $delete_data['id'] = $id;
                        if($this->web_app_model->delete_data("comments", $delete_data)){
                            $x++;
                        }
                    }
                }
                $response->code = 200;
                $response->status = true;
                $response->message = $x. " data deleted successfully";
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }
        }
        echo json_encode($response);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('comment', 'Comment', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);
            $comment = $this->input->post("comment", true);

            $primary_key['id'] = $id;

            $update_data['comment'] = $comment;

            $comment_object = $this->web_app_model->get_single_data("comments", $primary_key);
            if (!empty($comment_object)) {
                $this->web_app_model->update_data("comments", $update_data, $primary_key['id'], "id");

                $response->code = 200;
                $response->status = true;
                $response->message = "Data updated successfully";
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function export(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        //filter vars
        $query = $this->input->post('query', true);

        $table = 'comments';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "comments.user_id = users.id";
        $join->table = "users";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "comments.course_id = ocw_course.id";
        $join->table = "ocw_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "ocw_course.sync_course_id = sync_course.subject_id";
        $join->table = "sync_course";
        array_push($extra_join, $join);

        $search_fields = array("comments.id", "ocw_course.ocw_name", "users.full_name", "sync_course.subject_name", "sync_course.subject_code");
        $select_fields =  array("comments.id as id", "users.full_name as full_name", "ocw_course.ocw_name as ocw_name", "sync_course.subject_name", "sync_course.subject_code", "comments.comment");

        $result = $this->Datatable_model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;  

        $arr = array();
        if(!empty($list)){
            foreach ($list as $result) {
                $arr[] = array(
                    $result->id,
                    $result->ocw_name, 
                    $result->subject_name, 
                    $result->subject_code,
                    $result->full_name,
                    $result->comment,
                );
            }
        }
        $field = array('Id', 'OCW Name', 'Subject Name', 'Subject Code', 'User Fullname', 'Comment');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print_comments(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        //filter vars
        $query = $this->input->post('query', true);

        $table = 'comments';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "comments.user_id = users.id";
        $join->table = "users";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "comments.course_id = ocw_course.id";
        $join->table = "ocw_course";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "ocw_course.sync_course_id = sync_course.subject_id";
        $join->table = "sync_course";
        array_push($extra_join, $join);

        $search_fields = array("comments.id", "ocw_course.ocw_name", "users.full_name", "sync_course.subject_name", "sync_course.subject_code");
        $select_fields =  array("comments.id as id", "users.full_name as full_name", "ocw_course.ocw_name as ocw_name", "sync_course.subject_name", "sync_course.subject_code", "comments.comment");

        $result = $this->Datatable_model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;   

        $arr = array();
        if(!empty($list)){
            foreach ($list as $result) {
                $arr[] = array(
                    "id"=>$result->id,
                    "ocw_name"=>$result->ocw_name, 
                    "subject_name"=>$result->subject_name,
                    "subject_code"=>$result->subject_code,
                    "full_name"=>$result->full_name, 
                    "comment"=>$result->comment,
                );
            }
        }

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("comment/print",array("data"=>$arr),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);    
		     
    }
}
