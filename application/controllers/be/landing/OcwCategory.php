<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class OcwCategory extends BaseBEController {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('News_model');
        $this->load->library('session');
    }

    public function index()
    {
        $user = $this->session->userdata("sess_auth");
        if(isset($user) ){
            $data['breadcrumb'] = $this->breadcrumb;
            $data['controller_full_path'] = $this->full_path;
            $data['privilege'] = $this->privilege;
            $data['faculty_list'] = $this->web_app_model->get_data("sync_category",array("category_type"=>"FACULTY"));
            $this->load->view('ocwcategory/index', $data);
        }else{
            redirect('auth', 'auto');
        }
    }

    public function get_program_study()
    {
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("faculty_id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('faculty_id', 'Faculty', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $faculty_id = $this->input->get("faculty_id",true);
            $program_study_list = array();
            if(!empty($faculty_id)){
                $query_data['category_parent_id'] = $faculty_id;
                $query_data['category_type'] = "STUDYPROGRAM";
                $program_study_list = $this->web_app_model->get_data("sync_category",$query_data);
            }

            $response->code = 200;
            $response->data = $program_study_list;
            $response->status = true;
            $response->message = "Get data success";
        }
        echo json_encode($response);
    }

    public function datatable()
    {
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'vw_ocw_category';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("ocw_category_id","category_name");
        array_push($extra_where, "(deleted_at is null)");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->ocw_category_id = $result->ocw_category_id;
                $row->category_name = $result->category_name;
                $row->img_path = base_url().$result->img_path;
                $data[] = $row;
            }
        }

        $output = array(
            "meta" => $meta,
            "data" => $data
        );

        json_response($output);
    }

    public function image_upload()
    {
        if(!$this->can_create() && !$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $config['upload_path'] = FCPATH.$this->config->item('ocw_category_upload_path');
        if(!file_exists($config['upload_path'])){
            mkdir($config['upload_path'],0777,true);
        }
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['encrypt_name'] = false;
        $config['encrypt_name'] = true;

        $this->load->library('upload',$config);

        if($this->upload->do_upload('file')){
            $upload_data = $this->upload->data();
            echo $upload_data['file_name'];
        }else{
            header('HTTP/1.0 400 Bad Request');
            echo strip_tags($this->upload->display_errors());
        }
    }

    public function save(){
        $response = init_response();
        if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $user = $this->session->userdata("sess_auth");
        if(isset($user)){
            $this->form_validation->set_rules('faculty', 'Faculty', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('image_name', 'Image', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('study_program_name', 'Study Program Name', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $response->code = 400;
                $response->message = implode("<br>", $this->form_validation->error_array());
            }
            else
            {
                $faculty = $this->security->xss_clean($this->input->post("faculty"));
                $study_program_name = $this->input->post("study_program_name");
                $description = $this->input->post("description");
                $image_name = $this->input->post("image_name",true);

                $upload_path = $this->config->item('ocw_category_upload_path');

                $image_name = $upload_path.$image_name;

                $insert_data['faculty_id'] = $faculty;
                $insert_data['description'] = $description;
                $insert_data['img_path'] = $image_name;
                $insert_data["created_by"] = $user;
                $insert_data["study_program_name"] = $user;

                $this->web_app_model->insert_data("ocw_category",$insert_data);


                $response->code = 200;
                $response->status = true;
                $response->message = "Data submitted successfully";
            }
            json_response($response);
        }else{
            redirect('auth', 'auto');
        }
    }


    public function get()
    {
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){
            //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $news_id = $this->input->get("id",true);
            $news_object = null;
            if(!empty($news_id)){
                $query_data['ocw_category_id'] = $news_id;
                $news_object = $this->web_app_model->get_single_data("vw_ocw_category",$query_data);
                if(!empty($news_object)){
                    $news_object->img_path = base_url().$news_object->img_path;
                }
            }

            $response->code = 200;
            $response->data = $news_object;

            if(!empty($news_object)){
                $response->status = true;
                $response->message = "Get data success";
            }else{
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $category_id = $this->input->get("id",true);
            if(!empty($banner_id)){
                $query_data['ocw_category_id'] = $category_id;
                $banner_object = $this->web_app_model->get_single_data("vw_ocw_category",$query_data);
                if(!empty($banner_object)){
                    $delete_data['ocw_category_id'] = $category_id;
                    $this->web_app_model->delete_data("ocw_category",$delete_data);
                    unlink(FCPATH.$banner_object->img_path);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $response = init_response();
        $user = $this->session->userdata("sess_auth");
        if(isset($user) ){
            $this->form_validation->set_rules('ocw_category_id', 'Id', 'required');
            $this->form_validation->set_rules('faculty', 'Faculty', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('image_name', 'Image', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('study_program_name', 'Study Program Name', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $response->code = 400;
                $response->message = implode("<br>", $this->form_validation->error_array());
            }
            else
            {
                $id = $this->input->post("ocw_category_id",true);
                $faculty = $this->input->post("faculty",true);
                $description = $this->input->post("description");
                $image_name = $this->input->post("image_name",true);

                $upload_path = $this->config->item('ocw_category_upload_path');
                $image_name = $upload_path.$image_name;

                $primary_key['ocw_category_id'] = $id;

                $banner_object = $this->web_app_model->get_single_data("vw_ocw_category",$primary_key);

                $update_data['faculty_id'] = $faculty;
                $update_data['description'] = $description;
                $update_data['img_path'] = $image_name;
                $update_data["created_by"] = $user;

                if(!empty($banner_object)){

                    $this->web_app_model->update_data("ocw_category",$update_data,$primary_key['ocw_category_id'],"ocw_category_id");

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data updated successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }

        }else{
            $response->code = 401;
            $response->status = false;
            $response->message = "User No Authorization";
        }

        json_response($response);
    }

    function slug($field,$slug,$incerement=0) {

        $this->db->select('ID');
        $this->db->where($field,$slug);
        $this->db->order_by($field,'asc');
        $news = $this->db->get('news')->row_array();

        $new_slug = $news[$field].'-'.$count+1;
        if($count){
            $this->slug($field,$new_slug);
        } else {
            return $new_slug;
        }
    }


}

/* End of file Controllername.php */