<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/report/BaseBeReportController.php");

class Summary extends BaseBeReportController {
	function __construct() 
	{
		parent::__construct();							
	}

	public function index()
	{		
        $data['projects'] = $this->web_app_model->get_data('project');
        $data['users'] = $this->web_app_model->get_data('user');
        $data['privilege'] = $this->privilege;    
        $data['clearance_level'] = $this->clearance_level;    
        $data['breadcrumb'] = $this->breadcrumb;    
        $data['controller_full_path'] = $this->full_path;   
        $this->load->view('summary/summary',$data);
	}

    public function datatable_person()
    {       
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                   

        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        //filter vars
        $month = $this->input->post("month",true);
        $year = $this->input->post("year",true);
        $user = $this->input->post("user",true);
        $project = $this->input->post("project",true);

        $limit = 10;
        $offset = 0;
        if(array_key_exists("page", $pagination) && array_key_exists("perpage", $pagination)){
            $limit = $pagination["perpage"];
            $offset = (($pagination["page"] -1) * $limit) ;
        }
        
        if(!empty($sort)){
            if(array_key_exists("field", $sort) && array_key_exists("sort", $sort)){
                $column_order = array("value"=>$sort["field"],"direction"=>$sort["sort"]);
            }
        }

        $table = 'report';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "report.user_id = user.id";
        $join->table = "user";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "report.project = project.id";
        $join->table = "project";
        array_push($extra_join,$join);

        if(!empty($month)){
            array_push($extra_where,"MONTH(`date`) =" .$month);   
        }

        if(!empty($year)){
            array_push($extra_where,"YEAR(`date`) = $year");   
        }

        if(!empty($user)){
            array_push($extra_where, "report.user_id = $user");
        }

        if(!empty($project)){
            array_push($extra_where, "report.project = $project");
        }

        $select_fields =  array("fullname","SEC_TO_TIME(SUM(TIME_TO_SEC(`hours`))) as totaltime");
        $search_fields =  array("fullname");

        $group_by = "report.user_id";

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields, $group_by);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();
        
        if (!empty($list)) {
            foreach ($list as $result) {               
                $row = new stdClass();

                $row->fullname = $result->fullname;
                $row->hours = $result->totaltime;
                
                $data[] = $row;
            }
        }

        $output = array(
            "meta" => $meta,
            "data" => $data
        );

        echo json_encode($output);
    }

    public function statistic_person()
    {   
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->load->model('Summary_model');

        $month = $this->input->get("month",true);
        $year = $this->input->get("year",true);
        $user = $this->input->get("user",true);
        $project = $this->input->get("project",true);
        
        $statistic_object = $this->Summary_model->get_person_statistic($month,$year,$user, $project);
      
        $chartStatistics = new stdClass();
        
        $dataPie = array();
        $dataBar = array();

        foreach($statistic_object as $statistic){
            $rowPie = new stdClass();
            (!empty($user)) ? $rowPie->label = $statistic->nama_project : $rowPie->label = $statistic->fullname;  
            $rowPie->value = $statistic->totaltime;
            $dataPie[]= $rowPie;

            $rowBar = new stdClass();
            (!empty($user)) ? $rowBar->y = $statistic->nama_project : $rowBar->y = $statistic->fullname;  
            $rowBar->a = $statistic->totaltime;
            $dataBar[]= $rowBar;
        }
        
        $chartStatistics->pie = $dataPie;
        $chartStatistics->bar = $dataBar;

        $response->code = 200;
        $response->data = $chartStatistics;

        if(!empty($statistic_object)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function datatable_project()
    {       
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                   

        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        //filter vars
        $month = $this->input->post("month",true);
        $year = $this->input->post("year",true);
        $project = $this->input->post("project",true);
        $user = $this->input->post("user",true);

        $limit = 10;
        $offset = 0;
        if(array_key_exists("page", $pagination) && array_key_exists("perpage", $pagination)){
            $limit = $pagination["perpage"];
            $offset = (($pagination["page"] -1) * $limit) ;
        }
        
        $table = 'report';
        $select_fields = array('project.nama_project as project_name', 'SEC_TO_TIME(SUM(TIME_TO_SEC(hours))) as time');

        if(!empty($sort)){
            if(array_key_exists("field", $sort) && array_key_exists("sort", $sort)){
                $column_order = array("value"=>$sort["field"],"direction"=>$sort["sort"]);
            }
        }

        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "report.user_id = user.id";
        $join->table = "user";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "report.project = project.id";
        $join->table = "project";
        array_push($extra_join,$join);

        $search_fields = array();

        if(!empty($month)){
            array_push($extra_where,"MONTH(`date`) =" .$month);   
        }

        if(!empty($year)){
            array_push($extra_where,"YEAR(`date`) = $year");   
        }

        if(!empty($user)){
            array_push($extra_where, "report.user_id = $user");
        }

        if(!empty($project)){
           array_push($extra_where, "report.project = $project");
        }

        $group_by = "report.project";
        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields, $group_by);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();
        if (!empty($list)) {
            foreach ($list as $result) {               
                $row = new stdClass();

                $row->project_name = $result->project_name;
                $row->hours = $result->time;
                
                $data[] = $row;
            }
        }

        $output = array(
            "meta" => $meta,
            "data" => $data
        );

        echo json_encode($output);
    }

    public function statistic_project()
    {       
        
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->load->model('Summary_model');

        $month = $this->input->get("month",true);
        $year = $this->input->get("year",true);
        $project = $this->input->get("project",true);
        $user = $this->input->get("user",true);
        
        $project_filtered = false;
       
		if(!empty($project)){
            $project_filtered = true;
        }

        $statistic_object = $this->Summary_model->get_project_statistic($month,$year,$project,$user);
        
        $chartStatistics = new stdClass();

        $dataBar = array();
        $data = array();
        if($project_filtered){
            foreach($statistic_object as $statistic_result){
                $row = new stdClass();
                $rowbar = new stdClass();

                $row->label = $statistic_result->fullname;
                $row->value = $statistic_result->time;

                $rowbar->y = $statistic_result->fullname;
                $rowbar->a = $statistic_result->time;

                $data[]= $row;
                $dataBar[]= $rowbar;
            }
        } else {
            foreach($statistic_object as $statistic_result){
                $row = new stdClass();
                $rowbar = new stdClass();
                
                $row->label = $statistic_result->project_name;
                $row->value = $statistic_result->time;

                $rowbar->y = $statistic_result->project_name;
                $rowbar->a = $statistic_result->time;

                $data[]= $row;
                $dataBar[]= $rowbar;
            }
        }        
        $chartStatistics->bar = $dataBar;
        $chartStatistics->pie = $data;

        $response->code = 200;
        $response->data = $chartStatistics;

        if(!empty($statistic_object)){
            $response->status = true;
            $response->message = "Get data success";
        }else{
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function export(){

        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        
        $param = $this->input->post();
        foreach($param as $key => $value){
            if(!empty($key)){
                $param = json_decode($key,true);
            }
        }
        if(!empty($param)){
            $type = $param['type'];
            $list_type = array('project','person');
            if (in_array($type, $list_type)){
                $month = isset($param[$type."_month_filter"]) ? $param[$type."_month_filter"] : '';
                $year = isset($param[$type."_year_filter"]) ? $param[$type."_year_filter"] : '';
                $project = isset($param[$type."_project_filter"]) ? $param[$type."_project_filter"] : '';
                $person = isset($param[$type."_person_filter"]) ? $param[$type."_person_filter"] : '';

                error_reporting(0);
                //load helper
                $this->load->model('Datatable_model');
                $this->load->helper('export');

                $extra_where = array();
                $extra_where_or = array();
                $extra_join = array();

                $join = new stdClass();
                $join->join_type = "LEFT";
                $join->condition = "report.project = project.id";
                $join->table = "project";
                array_push($extra_join,$join);

                $join = new stdClass();
                $join->join_type = "LEFT";
                $join->condition = "report.user_id = user.id";
                $join->table = "user";
                array_push($extra_join,$join);

                $table = 'report';

                if(!empty($month)){
                    array_push($extra_where,"MONTH(`date`) =" .$month);   
                }

                if(!empty($year)){
                    array_push($extra_where,"YEAR(`date`) = $year");   
                }

                if($type == 'project'){
                    
                    if(!empty($person)){
                        array_push($extra_where, "report.user_id = $person");
                    }

                    $project_filtered = false;
                    if(!empty($project)){
                        array_push($extra_where, "report.project = $project");
                        array_push($extra_where, "true GROUP BY report.user_id, report.project");
                        $project_filtered = true;
                    }else{
                        array_push($extra_where, "true GROUP BY report.project");
                    }

                    if($project_filtered){
                        $select_fields = array('report.user_id as id', 'user.fullname as name', 'SEC_TO_TIME(SUM(TIME_TO_SEC(report.hours))) as time');
                        $field = array('Id','Employee Name','Hours');
                    }else {
                        $select_fields = array('report.project as id', 'project.nama_project as name', 'SEC_TO_TIME(SUM(TIME_TO_SEC(report.hours))) as time');
                        $field = array('Id','Project Name','Hours');
                    }
                }else if($type == 'person'){
                    $person_filtered = false;
                    
                    if(!empty($person)){
                        array_push($extra_where, "report.user_id = $person");
                        array_push($extra_where, "true GROUP BY report.project, report.user_id");
                        
                        $select_fields = array('report.user_id as id', 'project.nama_project as name', 'SEC_TO_TIME(SUM(TIME_TO_SEC(report.hours))) as time');
                        $field = array('Id','Project Name','Hours');
                        
                    }else{
                        array_push($extra_where, "true GROUP BY report.user_id");
                        $select_fields = array('report.project as id','user.fullname as name' , 'SEC_TO_TIME(SUM(TIME_TO_SEC(report.hours))) as time');
                        $field = array('Id','Employee Name','Hours');
                    }

                }

                $search_fields = [];
                    
                $result = $this->Datatable_model->get_data_export($table,'', '', $extra_where, $extra_where_or, $extra_join,$select_fields);
                $list = $result->data;       

                $arr = array();
                foreach ($list as $row) {
                    $row->time = $row->time;
                    $arr[] = array($row->id,$row->name,$row->time);
                }

                //do export
                export_csv($arr,$field);
            }
            else {
                $response->code = 403;
                $response->status = false;
                $response->message = "Invalid Parameter submitted";
                echo json_encode($response);
                return;
            }
        }
        else {
            $response->code = 403;
            $response->status = false;
            $response->message = "Empty Parameter submitted";
            echo json_encode($response);
            return;
        }
    }
}