<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Agreement extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('terms/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'landing_agreements';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id","name", "content");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        //untuk membandingkan string content yang sudah dihilangkan tag htmlnya
        function compare_content_asc($data_row1, $data_row2){
            $content1 = strip_tags($data_row1->content);
            $content2 = strip_tags($data_row2->content);
            return strcmp($content1, $content2);
        }

        function compare_content_desc($data_row1, $data_row2){
            $content1 = strip_tags($data_row1->content);
            $content2 = strip_tags($data_row2->content);
            return strcmp($content2, $content1);
        }

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->name = $result->name;
                $row->content = $result->content;
                $row->created_date = $result->created_date;

                $data[] = $row;
            }

            //untuk mengurutkan setiap item berdasarkan string deskripsi yang sudah tidak ada tag htmlnya
            if($sort["field"] == "content"){
                if($sort["sort"] == "asc"){
                    usort($data, 'compare_content_asc');
                } elseif($sort["sort"] == "desc") {
                    usort($data, 'compare_content_desc');
                }
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function get()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $term_id = $this->input->get("id", true);
            if (!empty($term_id)) {
                $query_data['id'] = $term_id;
                $term_object = $this->web_app_model->get_single_data("landing_agreements", $query_data);
            }
        }

        $response->code = 200;
        $response->data = $term_object;

        if (!empty($term_object)) {
            $response->status = true;
            $response->message = "Get data success";
        } else {
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);
            $content = $this->input->post("content");

            $primary_key['id'] = $id;

            $update_data['content'] = $content;

            $this->web_app_model->update_data("landing_agreements", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data updated successfully";
        }

        echo json_encode($response);
    }

    public function export(){   

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $query = $this->input->post("query", true);

        $table = 'landing_agreements';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "content");

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $arr = array();
        foreach ($list as $row) {  
            $arr[] = array($row->id,$row->name,strip_tags($row->content));
        }
        $field = array('Id', "Name",'Content');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print_agreement(){   

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $query = $this->input->post("query", true);

        $table = 'landing_agreements';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "content");

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("terms/print",array("data"=>$list),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);   
		     
    }
}
