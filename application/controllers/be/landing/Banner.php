<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Banner extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('banner/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'landing_banner';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "type", "url", 'description');

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->type = $result->type;
                $row->img_path = $result->img_path ? base_url() . $result->img_path : '';
                $row->url = $result->url;
                $row->priority = $result->priority;
                $row->description = $result->description;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function image_upload()
    {
        $response = get_ajax_response();
        if (!$this->can_create() && !$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $config['upload_path'] = FCPATH . $this->config->item('banner_upload_path');
        if (!file_exists($config['upload_path'])) {
            if (!mkdir($concurrentDirectory = $config['upload_path'], 0777, true) && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['encrypt_name'] = false;

        $max_size_upload = $this->config->item('banner_max_upload');
        if (!empty($max_size_upload)) {
            $config['max_size'] = $max_size_upload;
        }

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('file')) {
            $upload_data = $this->upload->data();
            echo $upload_data['file_name'];
        } else {
            header('HTTP/1.0 400 Bad Request');
            echo strip_tags($this->upload->display_errors());
        }
    }

    public function get()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $banner_id = $this->input->get("id", true);
            $banner_object = null;
            if (!empty($banner_id)) {
                $query_data['id'] = $banner_id;
                $banner_object = $this->web_app_model->get_single_data("landing_banner", $query_data);
                if (!empty($banner_object)) {
                    if ($banner_object->type == 'image')
                        $banner_object->img_path = base_url() . $banner_object->img_path;
                }
            }
        }

        $response->code = 200;
        $response->data = $banner_object;

        if (!empty($banner_object)) {
            $response->status = true;
            $response->message = "Get data success";
        } else {
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function save()
    {
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('url', 'Link', 'valid_url|callback_url_check');
        $this->form_validation->set_rules('priority', 'Priority', 'required|numeric');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $type = $this->input->post("type", true);
            $file_name = $this->input->post("image_name", true);
            if (!empty($file_name)) {
                $upload_path = $this->config->item('banner_upload_path');
                $file_name = $upload_path . $file_name;

                $insert_data['img_path'] = $file_name;
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Image is required";

                echo json_encode($response);
                return;
            }
            $url = $this->input->post("url", true);
            $priority = $this->input->post("priority", true);
            $description = $this->input->post("description", true);

            $insert_data['type'] = $type;
            $insert_data['url'] = $url;
            $insert_data['priority'] = $priority;
            $insert_data['description'] = $description;

            $this->web_app_model->insert_data("landing_banner", $insert_data);

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";

        }

        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if (!$this->can_delete()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $banner_id = $this->input->get("id", true);
            if (!empty($banner_id)) {
                $query_data['id'] = $banner_id;
                $banner_object = $this->web_app_model->get_single_data("landing_banner", $query_data);
                if (!empty($banner_object)) {
                    $delete_data['id'] = $banner_id;
                    $this->web_app_model->delete_data("landing_banner", $delete_data);
                    $banner_object->type === 'image' ? unlink(FCPATH . $banner_object->img_path) : '';

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                } else {
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('url', 'Link', 'valid_url|callback_url_check');
        $this->form_validation->set_rules('priority', 'Priority', 'required|numeric');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $type = $this->input->post("type", true);
            $file_name = $this->input->post("image_name", true);
            if (!empty($file_name)) {
                $upload_path = $this->config->item('banner_upload_path');
                $file_name = $upload_path . $file_name;

                $update_data['img_path'] = $file_name;
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Image is required";

                echo json_encode($response);
                return;
            }
            $id = $this->input->post("id", true);
            $url = $this->input->post("url", true);
            $priority = $this->input->post("priority", true);
            $description = $this->input->post("description", true);

            $primary_key['id'] = $id;

            $update_data['type'] = $type;
            $update_data['url'] = $url;
            $update_data['priority'] = $priority;
            $update_data['description'] = $description;

            $this->web_app_model->update_data("landing_banner", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";

        }

        echo json_encode($response);
    }

    public function print_banner(){
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="landing_banner";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id", "type", "url");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("banner/print",array("data"=>$result),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);
    }

    public function export(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="landing_banner";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id", "type", "url");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        foreach ($result as $row) {
            $image= "".base_url($row->img_path)."";
            $arr[] = array($row->id,$row->type,$image,$row->url,$row->priority);
         }
        $filed = array('Id','Type','Image','Url','Priority');
        //do export
        export_csv($arr,$filed);        
    }

    public function url_check($str){
        if($this->input->post('type') == 'video'){
            if(!$str){
                $this->form_validation->set_message('url_check', 'The {field} field is required when type is video');
                return FALSE;
            }else return true;
        } else return true;
    }

}
