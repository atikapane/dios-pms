<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Tutorial extends BaseBEController {

    function __construct() 
    {
        parent::__construct();      
    }
    
    public function index()
    {       
        //TODO : CHECK PRIVILEGE        
        //TODO : CHECK LOGIN        
        $data['breadcrumb'] = $this->breadcrumb;    
        $data['controller_full_path'] = $this->full_path;   
        $data['privilege'] = $this->privilege;   
        $data['max_upload'] = !empty($this->config->item('tutorial_max_upload')) ? $this->config->item('tutorial_max_upload') : 0;
        $this->load->view('tutorial/index.php',$data);
    }

    public function datatable()
    {       

        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                    
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'landing_tutorial';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id","title","type","description","file_status");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
                
                $row->id = $result->id;
                $row->title = $result->title;
                $row->type = $result->type == "file" ? "File" : "External URL";
                $row->file_path = !empty($result->file_path) ? ($row->type == "File" ? "<a href='".base_url().$result->file_path."' target='_blank'>View</a>" :  "<a href='".$result->file_path."' target='_blank'>View</a>") : "";
                $row->description = $result->description;
                $row->file_status = $result->file_status;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function file_upload()
    {
        if(!$this->can_create() && !$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $config['upload_path'] = FCPATH.$this->config->item('tutorial_upload_path');
        if(!file_exists($config['upload_path'])){
            mkdir($config['upload_path'],0777,true);
        }
        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
        $config['encrypt_name'] = false;
       
        $max_size_upload = $this->config->item('tutorial_max_upload');
        if(!empty($max_size_upload)){
            $config['max_size'] = $max_size_upload;
        }

        $this->load->library('upload',$config);

        if($this->upload->do_upload('file')){
            $upload_data = $this->upload->data(); 
            echo $upload_data['file_name'];
        }else{
            header('HTTP/1.0 400 Bad Request');
            echo strip_tags($this->upload->display_errors());
        }
    } 

    public function get()
    {       
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        } 
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $tutorial_id = $this->input->get("id",true);
            $tutorial_object = null;
            if(!empty($tutorial_id)){
                $query_data['id'] = $tutorial_id;
                $tutorial_object = $this->web_app_model->get_single_data("landing_tutorial",$query_data);
                if(!empty($tutorial_object)){
                    $tutorial_object->file_path = base_url().$tutorial_object->file_path;
                }
            }

            $response->code = 200;
            $response->data = $tutorial_object;

            if(!empty($tutorial_object)){
                $response->status = true;
                $response->message = "Get data success";
            }else{
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function save()
    {       
        $response = get_ajax_response();
         if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('file_status', 'Status File', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $type = $this->input->post("type",true);
            if ($type == "file"){
                $file_name = $this->input->post("file_name",true);
                if (!empty($file_name)){
                    $upload_path = $this->config->item('tutorial_upload_path');
                    $file_name = $upload_path.$file_name;    
                }
            }
            if ($type == "external"){
                $file_name = $this->input->post("url",true);
            }
            if (empty($file_name)){
                $response->code = 400;
                $response->message = "File or External URL is required";
            }
            else {
                $title = $this->input->post("title",true);
                $description = $this->input->post("description",true);
                $file_status = $this->input->post("file_status",true);

                $insert_data['title'] = $title;
                $insert_data['type'] = $type;
                $insert_data['file_path'] = $file_name;
                $insert_data['description'] = $description;
                $insert_data['file_status'] = $file_status;

                $this->web_app_model->insert_data("landing_tutorial",$insert_data);

                $response->code = 200;
                $response->status = true;
                $response->message = "Data submitted successfully";
            }
            
        }
        echo json_encode($response);
    }

    public function delete()
    {       
        $response = get_ajax_response();
         if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $tutorial_id = $this->input->get("id",true);
            if(!empty($tutorial_id)){
                $query_data['id'] = $tutorial_id;
                $tutorial_object = $this->web_app_model->get_single_data("landing_tutorial",$query_data);
                if(!empty($tutorial_object)){
                    $delete_data['id'] = $tutorial_id;
                    $this->web_app_model->delete_data("landing_tutorial",$delete_data);
                    if ($tutorial_object->type == "file"){
                        unlink(FCPATH.$tutorial_object->file_path);    
                    }

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {       
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('file_status', 'file_status', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $type = $this->input->post("type",true);
            if ($type == "file"){
                $file_name = $this->input->post("file_name",true);
                if (!empty($file_name)){
                    $upload_path = $this->config->item('tutorial_upload_path');
                    $file_name = $upload_path.$file_name;    
                }
            }
            if ($type == "external"){
                $file_name = $this->input->post("url",true);
            }
            if (empty($file_name)){
                $response->code = 400;
                $response->message = "File or External URL is required";
            }
            else {
                $id = $this->input->post("id",true);
                $title = $this->input->post("title",true);
                $description = $this->input->post("description",true);
                $file_status = $this->input->post("file_status",true);

                $primary_key['id'] = $id;

                $update_data['title'] = $title;
                $update_data['type'] = $type;
                $update_data['file_path'] = $file_name;
                $update_data['description'] = $description;
                $update_data['file_status'] = $file_status;

                $tutorial_object = $this->web_app_model->get_single_data("landing_tutorial",$primary_key);
                if(!empty($tutorial_object)){
                    $this->web_app_model->update_data("landing_tutorial",$update_data,$primary_key['id'],"id");

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data updated successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }
        }
        echo json_encode($response);
    }

    public function status_active()
    {
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $tutorial_id = $this->input->get("id",true);
            if(!empty($tutorial_id)){
                $query_data['id'] = $tutorial_id;
                $tutorial_object = $this->web_app_model->get_single_data("landing_tutorial",$query_data);
                if(!empty($tutorial_object)){
                    $file_status = $tutorial_object->file_status;
                    $data["file_status"] = ($file_status == "1" ? "2" : "1");
                    $this->web_app_model->update_data("landing_tutorial",$data,$tutorial_id,"id");
                    $response->message = "Status changed successfully";

                    $response->code = 200;
                    $response->status = true;
                    
                }
                else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function export(){ 
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                    
        $this->load->model('Datatable_model');
        $this->load->model('Tutorial_Model');

        $query = $this->input->post("query",true);

        $table = 'landing_tutorial';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id","title","type","description","file_status");

        $result = $this->Tutorial_Model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;
        $arr = array();
        foreach ($list as $row) {

            $type = $row->type == "file" ? "File" : "External URL";
            $path= "".base_url($row->file_path)."";
            $status ="";
            if($row->file_status==1){$status="Active";}else{$status="Inactive";}
            $arr[] = array($row->id,$row->title,$type,$path,$row->description,$status);
         }
         $filed = array('Id','title','Type','Link','Description','Status');
         //do export
        export_csv($arr,$filed);         
    }

    public function print_tutorial(){ 
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                    
        $this->load->model('Datatable_model');
        $this->load->model('Tutorial_Model');

        $query = $this->input->post("query",true);

        $table = 'landing_tutorial';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id","title","type","description","file_status");

        $result = $this->Tutorial_Model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;
        $arr = array();
        foreach ($list as $row) {

            $type = $row->type == "file" ? "File" : "External URL";
            $path= "".base_url($row->file_path)."";
            $status ="";
            if($row->file_status==1){$status="Active";}else{$status="Inactive";}
            $arr[] = array('id' => $row->id,'title' => $row->title,'type' => $type,'path' => $path,'description' => $row->description,'status' => $status);
         }
         $response = get_ajax_response();

         if ($result){
             $data = $this->load->view("tutorial/print",array("data"=>$arr),true);
             $response->code = 200;
             $response->status = true;
             $response->message = "Load Data Successfully";
             $response->data = $data;
         } else {
             $response->code = 400;
             $response->status = true;
             $response->message = "Fail Load Data";
             $response->data = null;
         }
 
         echo json_encode($response);          
    }

}
