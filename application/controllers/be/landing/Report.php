<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Report extends BaseBEController {

    function __construct() 
    {
        parent::__construct();      
    }
    
    public function index()
    {            
        $data['projects'] = $this->web_app_model->get_data('project');
        $data['breadcrumb'] = $this->breadcrumb;    
        $data['controller_full_path'] = $this->full_path;   
        $data['privilege'] = $this->privilege;   
        $this->load->view('report/index.php',$data);
    }

    public function datatable()
    {       

        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                    
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'report';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields = array("report.id as id", "task", "project.nama_project as nama_project", "type", "task_name", "date", 
                            "start_time", "end_time","hours", "pct_of_task_completed", "notes");
        $search_fields = array("project.nama_project", "type", "task_name", "date", "notes");

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "report.project = project.id";
        $join->table = "project";
        array_push($extra_join, $join);

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->task = $result->task;
                $row->nama_project = $result->nama_project;
                $row->type = $result->type;
                $row->task_name  = $result->task_name ;
                $row->date = $result->date;
                $row->start_time = $result->start_time;
                $row->end_time = $result->end_time;
                $row->hours = $result->hours;
                $row->pct_of_task_completed = $result->pct_of_task_completed;
                $row->notes = $result->notes;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function get()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $report_id = $this->input->get("id", true);
            if (!empty($report_id)) {
                $query_data['id'] = $report_id;
                $report_object = $this->web_app_model->get_single_data("report", $query_data);
            }
        }

        $response->code = 200;
        $response->data = $report_object;

        if (!empty($report_object)) {
            $response->status = true;
            $response->message = "Get data success";
        } else {
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function save()
    {
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('task', 'Task #', 'required');
        $this->form_validation->set_rules('project', 'Project', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('task_name', 'Task Name', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('start_time', 'Start Time', 'required');
        $this->form_validation->set_rules('end_time', 'End Time', 'required');
        $this->form_validation->set_rules('pct_of_task_completed', 'PCT of Task Completed', 'required');
        $this->form_validation->set_rules('content', 'Notes', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $task = $this->input->post("task", true);
            
            $identity = get_login_info();
            $user_id = $identity['user_id'];
            
            $project = $this->input->post("project", true);
            $type = $this->input->post("type");
            $task_name = $this->input->post("task_name", true);
            $date = $this->input->post("date", true);
            $start_time = $this->input->post("start_time", true);
            $end_time = $this->input->post("end_time", true);
            $pct_of_task_completed = $this->input->post("pct_of_task_completed", true);
            $notes = $this->input->post("content", true);

            $start_time24 = strtotime($start_time);
            $end_time24 = strtotime($end_time);
            $hours = gmdate("H:i:s", $end_time24 - $start_time24);

            $insert_data['task'] = $task;
            $insert_data['user_id'] = $user_id;
            $insert_data['project'] = $project;
            $insert_data['type'] = $type;
            $insert_data['task_name'] = $task_name;
            $insert_data['date'] = $date;
            $insert_data['start_time'] = date("H:i", strtotime($start_time));
            $insert_data['end_time'] = date("H:i", strtotime($end_time));
            $insert_data['hours'] = $hours;
            $insert_data['pct_of_task_completed'] = $pct_of_task_completed;
            $insert_data['notes'] = $notes;

            $this->web_app_model->insert_data("report", $insert_data);

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }


    public function delete()
    {       
        $response = get_ajax_response();
         if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $report_id = $this->input->get("id",true);
            if(!empty($report_id)){
                $query_data['id'] = $report_id;
                $report_object = $this->web_app_model->get_single_data("report",$query_data);
                if(!empty($report_object)){
                    $delete_data['id'] = $report_id;
                    $this->web_app_model->delete_data("report",$delete_data);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('task', 'Task #', 'required');
        $this->form_validation->set_rules('project', 'Project', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('task_name', 'Task Name', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('start_time', 'Start Time', 'required');
        $this->form_validation->set_rules('end_time', 'End Time', 'required');
        $this->form_validation->set_rules('pct_of_task_completed', 'PCT of Task Completed', 'required');
        $this->form_validation->set_rules('content', 'Notes', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);
            $user_id = $this->input->post("user_id", true);
            $task = $this->input->post("task", true);
            $project = $this->input->post("project", true);
            $type = $this->input->post("type");
            $task_name = $this->input->post("task_name", true);
            $date = $this->input->post("date", true);
            $start_time = $this->input->post("start_time", true);
            $end_time = $this->input->post("end_time", true);
            $pct_of_task_completed = $this->input->post("pct_of_task_completed", true);
            $notes = $this->input->post("content", true);

            $start_time24 = strtotime($start_time);
            $end_time24 = strtotime($end_time);
            $hours = gmdate("H:i:s", $end_time24 - $start_time24);

            $primary_key['id'] = $id;

            $update_data['task'] = $task;
            $update_data['user_id'] = $user_id;
            $update_data['project'] = $project;
            $update_data['type'] = $type;
            $update_data['task_name'] = $task_name;
            $update_data['date'] = $date;
            $update_data['start_time'] = date("H:i", strtotime($start_time));
            $update_data['end_time'] = date("H:i", strtotime($end_time));
            $update_data['hours'] = $hours;
            $update_data['pct_of_task_completed'] = $pct_of_task_completed;
            $update_data['notes'] = $notes;

            $this->web_app_model->update_data("report", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data updated successfully";
        }

        echo json_encode($response);
    }

    
    public function export(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="report";
        $order = "id"; 
        $filter = $search;
        $column_search = array("report.id as id", "task", "project.nama_project as nama_project", "type", "task_name", "date", 
        "start_time", "end_time","hours", "pct_of_task_completed", "notes");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "report.project = project.id";
        $join->table = "project";
        $extra_join[] = $join;

        $custom_select = array("report.id as id", "task", "project.nama_project as nama_project", "type", "task_name", "date", 
        "start_time", "end_time","hours", "pct_of_task_completed", "notes");
        

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $arr = array();
        foreach ($result as $row) {
            $arr[] = array( $row->task,
                            $row->nama_project,
                            $row->type,
                            $row->task_name ,
                            $row->date,
                            $row->start_time,
                            $row->end_time,
                            $row->hours,
                            $row->pct_of_task_completed,
                            $row->notes);
         }
         $filed = array( 'Task','Project','Type', 'Task name','Date','Start time','End time','Hours','Pct of task completed','Notes');
         //do export
         export_csv($arr,$filed);         
    }

    public function print_report(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');
        $table="report";
        $order = "id"; 
        $filter = $search;
        $column_search = array("report.id as id", "task", "nama_project", "type","task_name", "date","start_time",
                            "end_time","hours","pct_of_task_completed","notes");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "report.project = project.id";
        $join->table = "project";
        $extra_join[] = $join;

        $custom_select = array("report.id as id", "task", "nama_project", "type","task_name", "date","start_time",
                                    "end_time","hours","pct_of_task_completed","notes");

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $arr = array();
        foreach ($result as $row) {
            $arr[] = array("id"=>$row->id, "task"=>$row->task, "nama_project"=>$row->nama_project, "type"=>$row->type,
                            "task_name"=>$row->task_name, "date"=>$row->date,"start_time"=>$row->start_time,"end_time"=>$row->end_time,
                            "hours"=>$row->hours,"pct_of_task_completed"=>$row->pct_of_task_completed,"notes"=>$row->notes);
         }
         $response = get_ajax_response();

         if ($result){
             $data = $this->load->view("report/print",array("data"=>$arr),true);
             $response->code = 200;
             $response->status = true;
             $response->message = "Load Data Successfully";
             $response->data = $data;
         } else {
             $response->code = 400;
             $response->status = true;
             $response->message = "Fail Load Data";
             $response->data = null;
         }
 
         echo json_encode($response);        
    }

}
