<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Download extends BaseBEController {

    function __construct() 
    {
        parent::__construct();      
    }
    
    public function index()
    {       
        //TODO : CHECK PRIVILEGE        
        //TODO : CHECK LOGIN        
        $data['breadcrumb'] = $this->breadcrumb;    
        $data['controller_full_path'] = $this->full_path;   
        $data['privilege'] = $this->privilege;   
        $data['max_upload'] = !empty($this->config->item('download_max_upload')) ? $this->config->item('download_max_upload') : 0;
        $this->load->view('download/index.php',$data);
    }

    public function datatable()
    {       

        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                    
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'landing_download';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id","title","description","file_status");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
                
                $row->id = $result->id;
                $row->file_path = !empty($result->file_path) ? "<a href='".base_url().$result->file_path."' target='_blank'>View</a>" : "";
                $row->title = $result->title;
                $row->description = $result->description;
                $row->file_status = $result->file_status;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function file_upload()
    {
        if(!$this->can_create() && !$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $config['upload_path'] = FCPATH.$this->config->item('download_upload_path');
        if(!file_exists($config['upload_path'])){
            mkdir($config['upload_path'],0777,true);
        }
        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
        $config['encrypt_name'] = false;
       
        $max_size_upload = $this->config->item('download_max_upload');
        if(!empty($max_size_upload)){
            $config['max_size'] = $max_size_upload;
        }

        $this->load->library('upload',$config);

        if($this->upload->do_upload('file')){
            $upload_data = $this->upload->data(); 
            echo $upload_data['file_name'];
        }else{
            header('HTTP/1.0 400 Bad Request');
            echo strip_tags($this->upload->display_errors());
        }
    } 

    public function get()
    {       
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        } 
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $download_id = $this->input->get("id",true);
            $download_object = null;
            if(!empty($download_id)){
                $query_data['id'] = $download_id;
                $download_object = $this->web_app_model->get_single_data("landing_download",$query_data);
                if(!empty($download_object)){
                    $download_object->file_path = base_url().$download_object->file_path;
                }
            }

            $response->code = 200;
            $response->data = $download_object;

            if(!empty($download_object)){
                $response->status = true;
                $response->message = "Get data success";
            }else{
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function save()
    {       
        $response = get_ajax_response();
         if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('file_name', 'File', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('file_status', 'Status File', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $title = $this->input->post("title",true);
            $file_name = $this->input->post("file_name",true);
            $description = $this->input->post("description",true);
            $file_status = $this->input->post("file_status",true);

            $upload_path = $this->config->item('download_upload_path');

            $file_name = $upload_path.$file_name;

            $insert_data['title'] = $title;
            $insert_data['file_path'] = $file_name;
            $insert_data['description'] = $description;
            $insert_data['file_status'] = $file_status;

            $this->web_app_model->insert_data("landing_download",$insert_data);

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }

    public function delete()
    {       
        $response = get_ajax_response();
         if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $download_id = $this->input->get("id",true);
            if(!empty($download_id)){
                $query_data['id'] = $download_id;
                $download_object = $this->web_app_model->get_single_data("landing_download",$query_data);
                if(!empty($download_object)){
                    $delete_data['id'] = $download_id;
                    $this->web_app_model->delete_data("landing_download",$delete_data);
                    unlink(FCPATH.$download_object->file_path);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {       
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  
        $this->form_validation->set_rules('file_name', 'File', 'required');
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('file_status', 'file_status', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $id = $this->input->post("id",true);
            $title = $this->input->post("title",true);
            $file_name = $this->input->post("file_name",true);
            $description = $this->input->post("description",true);
            $file_status = $this->input->post("file_status",true);

            $upload_path = $this->config->item('download_upload_path');
            $file_name = $upload_path.$file_name;

            $primary_key['id'] = $id;

            $update_data['title'] = $title;
            $update_data['file_path'] = $file_name;
            $update_data['description'] = $description;
            $update_data['file_status'] = $file_status;

            $download_object = $this->web_app_model->get_single_data("landing_download",$primary_key);
            if(!empty($download_object)){
                $this->web_app_model->update_data("landing_download",$update_data,$primary_key['id'],"id");

                $response->code = 200;
                $response->status = true;
                $response->message = "Data updated successfully";
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function status_active()
    {
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $download_id = $this->input->get("id",true);
            if(!empty($download_id)){
                $query_data['id'] = $download_id;
                $download_object = $this->web_app_model->get_single_data("landing_download",$query_data);
                if(!empty($download_object)){
                    $file_status = $download_object->file_status;
                    $data["file_status"] = ($file_status == "1" ? "2" : "1");
                    $this->web_app_model->update_data("landing_download",$data,$download_id,"id");
                    $response->message = "Status changed successfully";

                    $response->code = 200;
                    $response->status = true;
                    
                }
                else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function export(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="landing_download";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id","title","description","file_status");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $arr = array();
        foreach ($result as $row) {
            $file= "".base_url($row->file_path)."";
            $status ="";
            if($row->file_status==1){$status="Active";}else{$status="Inactive";}
            $arr[] = array($row->id,$row->title, $file,$row->description,$status);
         }
         $filed = array('Id','Title','File','Description','Status');
         //do export
         export_csv($arr,$filed);         
    }

    public function print_download(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="landing_download";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id","title","description","file_status");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $arr = array();
        foreach ($result as $row) {
            $file= "".base_url($row->file_path)."";
            $status ="";
            if($row->file_status==1){$status="Active";}else{$status="Inactive";}
            $arr[] = array('id'=>$row->id,'title'=>$row->title, 'file'=>$file,'description'=>$row->description,'status'=>$status);
         }
         $response = get_ajax_response();

         if ($result){
             $data = $this->load->view("download/print",array("data"=>$arr),true);
             $response->code = 200;
             $response->status = true;
             $response->message = "Load Data Successfully";
             $response->data = $data;
         } else {
             $response->code = 400;
             $response->status = true;
             $response->message = "Fail Load Data";
             $response->data = null;
         }
 
         echo json_encode($response);        
    }

}
