<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Client extends BaseBEController {

	function __construct() 
	{
        parent::__construct();				
	}
	
	public function index()
	{	
        $data['consultants'] = $this->Web_App_Model->get_data('user');
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;	
		$this->load->view('client/index.php',$data);
	}

	public function datatable()
	{		

		if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'client';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields = array("client.id as id", "client.nama as nama", "client.instansi as instansi", 
                                "client.jabatan as jabatan", "client.no_kontak as no_kontak", "client.email as email",
                                "user.fullname as full_name");
        $search_fields = array("nama", "instansi", "jabatan", "no_kontak", "client.email", "username");

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "client.consultant = user.id";
        $join->table = "user";
        array_push($extra_join, $join);

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->nama = $result->nama;
                $row->instansi = $result->instansi;
                $row->jabatan = $result->jabatan;
                $row->no_kontak = $result->no_kontak;
                $row->email = $result->email;
                $row->full_name = $result->full_name;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }
    
    public function get(){
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $client_id = $this->input->get("id", true);
            $client_object = null;
            if (!empty($client_id)) {
                $query_data['id'] = $client_id;
                $client_object = $this->web_app_model->get_single_data("client", $query_data);
            }
        }

        $response->code = 200;
        $response->data = $client_object;

        if (!empty($client_object)) {
            $response->status = true;
            $response->message = "Get data success";
        } else {
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

	public function edit()
	{			
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('instansi', 'Instansi', 'required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('nomor_kontak', 'Nomor Kontak', 'required|numeric');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('consultant', 'Consultans', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);
            $nama = $this->input->post('nama', true);
            $nama = trim($nama);
            $instansi = $this->input->post('instansi', true);
            $instansi = trim($instansi);
            $jabatan = $this->input->post('jabatan', true);
            $jabatan = trim($jabatan);
            $nomor_kontak = $this->input->post('nomor_kontak', true);
            $nomor_kontak = trim($nomor_kontak);
            $email = $this->input->post('email', true);
            $email = trim($email);
            $consultant = $this->input->post('consultant', true);
            $consultant = trim($consultant);

            $primary_key['id'] = $id;

            $update_data['nama'] = $nama;
            $update_data['instansi'] = $instansi;
            $update_data['jabatan'] = $jabatan;
            $update_data['no_kontak'] = $nomor_kontak;
            $update_data['email'] = $email;
            $update_data['consultant'] = $consultant;

            $this->web_app_model->update_data("client", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
   		
    }

    public function save(){
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('instansi', 'Instansi', 'required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('nomor_kontak', 'Nomor Kontak', 'required|numeric');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('consultant', 'Consultans', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $nama = $this->input->post('nama', true);
            $nama = trim($nama);
            $instansi = $this->input->post('instansi', true);
            $instansi = trim($instansi);
            $jabatan = $this->input->post('jabatan', true);
            $jabatan = trim($jabatan);
            $nomor_kontak = $this->input->post('nomor_kontak', true);
            $nomor_kontak = trim($nomor_kontak);
            $email = $this->input->post('email', true);
            $email = trim($email);
            $consultant = $this->input->post('consultant', true);
            $consultant = trim($consultant);

            $insert_data['nama'] = $nama;
            $insert_data['instansi'] = $instansi;
            $insert_data['jabatan'] = $jabatan;
            $insert_data['no_kontak'] = $nomor_kontak;
            $insert_data['email'] = $email;
            $insert_data['consultant'] = $consultant;

            $this->web_app_model->insert_data("client", $insert_data);

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if (!$this->can_delete()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $client_id = $this->input->get("id", true);
            if (!empty($client_id)) {
                $query_data['id'] = $client_id;
                $client_object = $this->web_app_model->get_single_data("client", $query_data);
                if (!empty($client_object)) {
                    $delete_data['id'] = $client_id;
                    $this->web_app_model->delete_data("client", $delete_data);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                } else {
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function export(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');
        
        $table="client";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id", "nama", "instansi", "jabatan", "no_kontak", "email");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "client.consultant = user.id";
        $join->table = "user";
        $extra_join[] = $join;

        $custom_select = array("client.id as id", "client.nama as nama", "client.instansi as instansi", 
        "client.jabatan as jabatan", "client.no_kontak as no_kontak", "client.email as email",
        "user.fullname as full_name");

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        foreach ($result as $row) {
            $arr[] = array($row->id,$row->nama,$row->instansi,$row->jabatan,$row->no_kontak, $row->email, $row->full_name);
         }
        $filed = array("id", "nama", "instansi", "jabatan", "no_kontak", "email", "consultant");
        //do export
        export_csv($arr,$filed);           
    }

    public function print_client(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        // $table="sync_category as unit";
        $table="client";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id", "nama", "instansi", "jabatan", "no_kontak", "email");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "client.consultant = user.id";
        $join->table = "user";
        $extra_join[] = $join;

        $custom_select = array("client.id as id", "client.nama as nama", "client.instansi as instansi", 
        "client.jabatan as jabatan", "client.no_kontak as no_kontak", "client.email as email",
        "user.fullname as full_name");

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("client/print",array("data"=>$result),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);           
    }
}
