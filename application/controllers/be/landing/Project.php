<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Project extends BaseBEController {

    function __construct() 
    {
        parent::__construct();      
    }
    
    public function index()
    {       
        //TODO : CHECK PRIVILEGE        
        //TODO : CHECK LOGIN        
        $data['consultants'] = $this->Web_App_Model->get_data('user');
        $data['clients'] = $this->Web_App_Model->get_data('client');
        $data['breadcrumb'] = $this->breadcrumb;    
        $data['controller_full_path'] = $this->full_path;   
        $data['privilege'] = $this->privilege;   
        $data['max_upload'] = !empty($this->config->item('project_max_upload')) ? $this->config->item('project_max_upload') : 0;
        $this->load->view('project/index.php',$data);
    }

    public function datatable()
    {       

        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }                    
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'project';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields = array("project.id as id", "nama_project", "tanggal_mulai", "tanggal_selesai", 
                            "client.nama as nama_client", "user.fullname as nama_consultant","quotation", "spk");
        $search_fields = array("nama_project","tanggal_mulai","tanggal_selesai", "client.nama", "user.fullname");

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "project.client_id = client.id";
        $join->table = "client";
        array_push($extra_join, $join);

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "project.consultant = user.id";
        $join->table = "user";
        array_push($extra_join, $join);

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
                
                $row->id = $result->id;
                $row->nama_project = $result->nama_project;
                $row->tanggal_mulai = $result->tanggal_mulai;
                $row->tanggal_selesai = $result->tanggal_selesai;
                $row->nama_client = $result->nama_client;
                $row->nama_consultant = $result->nama_consultant;
                $row->quotation = !empty($result->quotation) ? "<a href='".base_url().$result->quotation."' target='_blank'>View</a>" : "";
                $row->spk = !empty($result->spk) ? "<a href='".base_url().$result->spk."' target='_blank'>View</a>" : "";
                
                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function file_upload()
    {
        if(!$this->can_create() && !$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $config['upload_path'] = FCPATH.$this->config->item('project_upload_path');
        if(!file_exists($config['upload_path'])){
            mkdir($config['upload_path'],0777,true);
        }
        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
        $config['encrypt_name'] = false;
       
        $max_size_upload = $this->config->item('project_max_upload');
        if(!empty($max_size_upload)){
            $config['max_size'] = $max_size_upload;
        }

        $this->load->library('upload',$config);

        if($this->upload->do_upload('file')){
            $upload_data = $this->upload->data(); 
            echo $upload_data['file_name'];
        }else{
            header('HTTP/1.0 400 Bad Request');
            echo strip_tags($this->upload->display_errors());
        }
    } 

    public function get()
    {       
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        } 
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $project_id = $this->input->get("id",true);
            $project_object = null;
            if(!empty($project_id)){
                $query_data['id'] = $project_id;
                $project_object = $this->web_app_model->get_single_data("project",$query_data);
                if(!empty($project_object)){
                    $project_object->quotation = base_url().$project_object->quotation;
                    $project_object->spk = base_url().$project_object->spk;
                }
            }

            $response->code = 200;
            $response->data = $project_object;

            if(!empty($project_object)){
                $response->status = true;
                $response->message = "Get data success";
            }else{
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function save()
    {       
        $response = get_ajax_response();
         if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('nama_project', 'Nama Project', 'required');
        $this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('tanggal_selesai', 'Tanggal_Selesai', 'required');
        $this->form_validation->set_rules('nama_client', 'Client', 'required');
        $this->form_validation->set_rules('nama_consultant', 'Consultant', 'required');
        $this->form_validation->set_rules('file_quotation', 'Quotation', 'required');
        $this->form_validation->set_rules('file_spk', 'SPK', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $nama_project = $this->input->post("nama_project",true);
            $tanggal_mulai = $this->input->post("tanggal_mulai",true);
            $tanggal_selesai = $this->input->post("tanggal_selesai",true);
            $nama_client = $this->input->post("nama_client",true);
            $nama_consultant = $this->input->post("nama_consultant",true);
            $file_quotation = $this->input->post("file_quotation",true);
            $file_spk = $this->input->post("file_spk",true);

            $upload_path = $this->config->item('project_upload_path');

            $file_quotation = $upload_path.$file_quotation;

            $upload_path = $this->config->item('project_upload_path');

            $file_spk = $upload_path.$file_spk;

            $insert_data['nama_project'] = $nama_project;
            $insert_data['tanggal_mulai'] = $tanggal_mulai;
            $insert_data['tanggal_selesai'] = $tanggal_selesai;
            $insert_data['client_id'] = $nama_client;
            $insert_data['consultant'] = $nama_consultant;
            $insert_data['quotation'] = $file_quotation;
            $insert_data['spk'] = $file_spk;

            $this->web_app_model->insert_data("project",$insert_data);

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }

    public function delete()
    {       
        $response = get_ajax_response();
         if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $project_id = $this->input->get("id",true);
            if(!empty($project_id)){
                $query_data['id'] = $project_id;
                $project_object = $this->web_app_model->get_single_data("project",$query_data);
                if(!empty($project_object)){
                    $delete_data['id'] = $project_id;
                    $this->web_app_model->delete_data("project",$delete_data);
                    unlink(FCPATH.$project_object->quotation);
                    unlink(FCPATH.$project_object->spk);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {       
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('nama_project', 'Nama Project', 'required');
        $this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'required');
        $this->form_validation->set_rules('nama_client', 'Client', 'required');
        $this->form_validation->set_rules('nama_consultant', 'Consultant', 'required');
        $this->form_validation->set_rules('file_quotation', 'Quotation', 'required');
        $this->form_validation->set_rules('file_spk', 'SPK', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $id = $this->input->post("id",true);
            $nama_project = $this->input->post("nama_project",true);
            $tanggal_mulai = $this->input->post("tanggal_mulai",true);
            $tanggal_selesai = $this->input->post("tanggal_selesai",true);
            $nama_client = $this->input->post("nama_client",true);
            $nama_consultant = $this->input->post("nama_consultant",true);
            $file_quotation = $this->input->post("file_quotation",true);
            $file_spk = $this->input->post("file_spk",true);

            $upload_path = $this->config->item('project_upload_path');
            $file_quotation = $upload_path.$file_quotation;

            $upload_path = $this->config->item('project_upload_path');

            $file_spk = $upload_path.$file_spk;

            $primary_key['id'] = $id;

            $update_data['nama_project'] = $nama_project;
            $update_data['tanggal_mulai'] = $tanggal_mulai;
            $update_data['tanggal_selesai'] = $tanggal_selesai;
            $update_data['client_id'] = $nama_client;
            $update_data['consultant'] = $nama_consultant;
            $update_data['quotation'] = $file_quotation;
            $update_data['spk'] = $file_spk;

            $project_object = $this->web_app_model->get_single_data("project",$primary_key);

            if(!empty($project_object)){
                $this->web_app_model->update_data("project",$update_data,$primary_key['id'],"id");

                $response->code = 200;
                $response->status = true;
                $response->message = "Data updated successfully";
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    public function export(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="project";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id","nama_project","tanggal_mulai","tanggal_selesai", "client_id", "consultant", "quotation", "spk");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "project.client_id = client.id";
        $join->table = "client";
        $extra_join[] = $join;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "project.consultant = user.id";
        $join->table = "user";
        $extra_join[] = $join;

        $custom_select = array("project.id as id", "nama_project", "tanggal_mulai", "tanggal_selesai", 
                            "client.nama as nama_client", "user.fullname as nama_consultant","quotation", "spk");

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $arr = array();
        foreach ($result as $row) {
            $file_quotation= "".base_url($row->quotation)."";
            $file_spk= "".base_url($row->spk)."";
            $arr[] = array($row->id,$row->nama_project, $row->tanggal_mulai, $row->tanggal_selesai, $row->nama_client, $row->nama_consultant, $file_quotation, $file_spk);
         }
         $filed = array('Id','Nama Project','Tanggal Mulai','Tanggal Selesai','Client', 'Consultant', 'Quotation', 'SPK');
         //do export
         export_csv($arr,$filed);         
    }

    public function print_project(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="project";
        $order = "id"; 
        $filter = $search;
        $column_search = array("id","nama_project","tanggal_mulai","tanggal_selesai", "client_id", "consultant", "quotation", "spk");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "project.client_id = client.id";
        $join->table = "client";
        $extra_join[] = $join;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "project.consultant = user.id";
        $join->table = "user";
        $extra_join[] = $join;

        $custom_select = array("project.id as id", "nama_project", "tanggal_mulai", "tanggal_selesai", 
                            "client.nama as nama_client", "user.fullname as nama_consultant","quotation", "spk");

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $arr = array();
        foreach ($result as $row) {
            $file_quotation= "".base_url($row->quotation)."";
            $file_spk= "".base_url($row->spk)."";
            $arr[] = array('id'=>$row->id, 'nama_project'=>$row->nama_project, 'tanggal_mulai'=>$row->tanggal_mulai, 
                        'tanggal_selesai'=>$row->tanggal_selesai, 'nama_client'=>$row->nama_client, 
                        'nama_consultant'=>$row->nama_consultant, 'quotation'=>$file_quotation, 'spk'=>$file_spk);
         }
         $response = get_ajax_response();

         if ($result){
             $data = $this->load->view("project/print",array("data"=>$arr),true);
             $response->code = 200;
             $response->status = true;
             $response->message = "Load Data Successfully";
             $response->data = $data;
         } else {
             $response->code = 400;
             $response->status = true;
             $response->message = "Fail Load Data";
             $response->data = null;
         }
 
         echo json_encode($response);        
    }

}
