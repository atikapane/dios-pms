<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class About extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['groups'] = $this->web_app_model->get_data('landing_about', '', 'group');
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('about/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'landing_about';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "title", "group", 'description', 'color');

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        //untuk membandingkan string description yang sudah dihilangkan tag htmlnya
        function compare_description_asc($data_row1, $data_row2){
            $desc1 = strip_tags($data_row1->description);
            $desc2 = strip_tags($data_row2->description);
            return strcmp($desc1, $desc2);
        }

        function compare_description_desc($data_row1, $data_row2){
            $desc1 = strip_tags($data_row1->description);
            $desc2 = strip_tags($data_row2->description);
            return strcmp($desc2, $desc1);
        }

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->title = $result->title;
                $row->group = $result->group;
                $row->description = $result->description;
                $row->color = $result->color;
                $row->priority = $result->priority;
                $row->created_date = $result->created_date;

                $data[] = $row;
            }

            //untuk mengurutkan setiap item berdasarkan string deskripsi yang sudah tidak ada tag htmlnya
            if($sort["field"] == "description"){
                if($sort["sort"] == "asc"){
                    usort($data, 'compare_description_asc');
                } elseif($sort["sort"] == "desc") {
                    usort($data, 'compare_description_desc');
                }
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function get()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $about_id = $this->input->get("id", true);
            if (!empty($about_id)) {
                $query_data['id'] = $about_id;
                $about_object = $this->web_app_model->get_single_data("landing_about", $query_data);
            }
        }

        $response->code = 200;
        $response->data = $about_object;

        if (!empty($about_object)) {
            $response->status = true;
            $response->message = "Get data success";
        } else {
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function get_order(){
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  

        $group = $this->input->get('group');
        $order_object = $this->web_app_model->get_data("landing_about", ['group' => $group], '', 'priority');
        $response->code = 200;
        $response->data = $order_object;
        $response->status = true;
        $response->message = "Get data success";

        echo  json_encode($response);
    }

    public function save()
    {
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('group', 'Group', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('color', 'Color', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $title = $this->input->post("title", true);
            $group = $this->input->post("group", true);
            $description = $this->input->post("description");
            $color = $this->input->post("color", true);
            $count = $this->web_app_model->get_data_distinct("landing_about", ['group' => $group], 'count(*) as count');
            $priority = $count[0]->count + 1;

            $insert_data['title'] = $title;
            $insert_data['group'] = $group;
            $insert_data['description'] = $description;
            $insert_data['color'] = $color;
            $insert_data['priority'] = $priority;

            $this->web_app_model->insert_data("landing_about", $insert_data);

            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }

    public function save_order(){
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $priorities = $this->input->post('order_array');
        if (empty($priorities)){
            $response->code = 400;
            $response->status = true;
            $response->message = "Order Empty";
        }else{
            $x = 1;
            foreach ($priorities as $priority){
                $primary_key['id'] = $priority;

                $update_data['priority'] = $x++;

                $this->web_app_model->update_data("landing_about", $update_data, $primary_key['id'], "id");
            }

            $response->code = 200;
            $response->status = true;
            $response->priority = $priorities;
            $response->message = "Data updated successfully";
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if (!$this->can_delete()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $about_id = $this->input->get("id", true);
            if (!empty($about_id)) {
                $query_data['id'] = $about_id;
                $about_object = $this->web_app_model->get_single_data("landing_about", $query_data);
                if (!empty($about_object)) {
                    $delete_data['id'] = $about_id;
                    $this->web_app_model->delete_data("landing_about", $delete_data);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                } else {
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('group', 'Group', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('color', 'Color', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);

            $primary_key['id'] = $id;

            $update_data['title'] = $this->input->post('title', true);
            $update_data['group'] = $this->input->post('group', true);
            $update_data['description'] = $this->input->post('description');
            $update_data['color'] = $this->input->post('color', true);

            $this->web_app_model->update_data("landing_about", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data updated successfully";
        }

        echo json_encode($response);
    }

    public function export(){   

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $query = $this->input->post("query", true);

        $table = 'landing_about';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "title", "short_title", 'description', 'color');

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $arr = array();
        foreach ($list as $row) {  
            $arr[] = array($row->id,$row->title,$row->group,strip_tags($row->description),$row->color,$row->priority,$row->created_date);
        }
        $field = array('Id','Title','Group','Description','Color','Priority','Created Date');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print_about_us(){   

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $query = $this->input->post("query", true);

        $table = 'landing_about';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "title", "short_title", 'description', 'color');

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("about/print",array("data"=>$list),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);     
		     
    }

}
