<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Content extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('content/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'landing_content';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "title", "priority");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->title = $result->title;
                $row->content = $result->content;
                $row->priority = $result->priority;
                $row->created_date = $result->created_date;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function get()
    {
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $content_id = $this->input->get("id", true);
            if (!empty($content_id)) {
                $query_data['id'] = $content_id;
                $content_object = $this->web_app_model->get_single_data("landing_content", $query_data);
            }
        }

        $response->code = 200;
        $response->data = $content_object;

        if (!empty($content_object)) {
            $response->status = true;
            $response->message = "Get data success";
        } else {
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

    public function get_order(){
        $response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }  

        $order_object = $this->web_app_model->get_data("landing_content", '', '', 'priority');
        $response->code = 200;
        $response->data = $order_object;
        $response->status = true;
        $response->message = "Get data success";

        echo  json_encode($response);
    }

    public function save()
    {
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $count = $this->web_app_model->get_data_distinct("landing_content", [], 'count(*) as count');
            $title = $this->input->post("title", true);
            $content = $this->input->post("content", true);
            $priority = $count[0]->count + 1;

            $insert_data['title'] = $title;
            $insert_data['content'] = $content;
            $insert_data['priority'] = $priority;

            $this->web_app_model->insert_data("landing_content", $insert_data);

            $response->code = 200;
            $response->status = true;
            $response->count = $count;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }

    public function save_order(){
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $priorities = $this->input->post('order_array');
        if (empty($priorities)){
            $response->code = 400;
            $response->status = true;
            $response->message = "Order Empty";
        }else{
            $x = 1;
            foreach ($priorities as $priority){
                $primary_key['id'] = $priority;

                $update_data['priority'] = $x++;

                $this->web_app_model->update_data("landing_content", $update_data, $primary_key['id'], "id");
            }

            $response->code = 200;
            $response->status = true;
            $response->priority = $priorities;
            $response->message = "Data updated successfully";
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if (!$this->can_delete()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $content_id = $this->input->get("id", true);
            if (!empty($content_id)) {
                $query_data['id'] = $content_id;
                $content_object = $this->web_app_model->get_single_data("landing_content", $query_data);
                if (!empty($content_object)) {
                    $delete_data['id'] = $content_id;
                    $this->web_app_model->delete_data("landing_content", $delete_data);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                } else {
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function edit()
    {
        $response = get_ajax_response();
        if (!$this->can_update()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("id", true);

            $primary_key['id'] = $id;

            $update_data['title'] = $this->input->post('title', true);
            $update_data['content'] = $this->input->post('content', true);

            $this->web_app_model->update_data("landing_content", $update_data, $primary_key['id'], "id");

            $response->code = 200;
            $response->status = true;
            $response->message = "Data updated successfully";
        }

        echo json_encode($response);
    }

    public function export(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        $query = $this->input->post("query", true);

        $table = 'landing_content';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "title", "priority");

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $arr = array();
        foreach ($list as $row) {  
            
            $arr[] = array($row->id,$row->title,$row->content,$row->priority,$row->created_date);
        }
        $field = array('Id','Title','Content','Priority','Created Date');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print_content(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        $query = $this->input->post("query", true);

        $table = 'landing_content';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("id", "title", "priority");

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;  

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("content/print",array("data"=>$list),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);   
		     
    }
}
