<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class User extends BaseBEController {

	function __construct() 
	{
		parent::__construct();
	}
	
	public function index()
	{			
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;	
        $data['privilege'] = $this->privilege;
        $data['education_levels'] = $this->web_app_model->get_data("education_level");
        $data['countries'] = $this->web_app_model->get_data("country");
        $data['cities'] = $this->web_app_model->get_data("city");
		$this->load->view('ocw_user/index.php',$data);
	}

	public function datatable()
	{		
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }			
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'users';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("email","full_name","username");
        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $data[] = $result;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
	}

	public function get()
	{		
		$response = get_ajax_response();
        if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Id', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$user_id = $this->input->get("id",true);
            $user_object = null;
        	if(!empty($user_id)){
        		$query_data['id'] = $user_id;
        		$user_object = $this->web_app_model->get_single_data("users",$query_data);
                if(!empty($user_object)){
                    $response->data = $user_object;
                    $response->status = true;
                    $response->message = "Get data success";
                }else{
                    $response->status = false;
                    $response->message = "Data not found";
                }
        	}

            $response->code = 200;
        }
        json_response($response);
	}

	public function save()
	{		
		$response = get_ajax_response();
        if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('add_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('add_full_name', 'Name', 'required');
        $this->form_validation->set_rules('add_username', 'Username', 'required');
        $this->form_validation->set_rules('add_password', 'Password', 'required');
//        $this->form_validation->set_rules('add_country', 'Country');
        $this->form_validation->set_rules('add_city', 'City');
        $this->form_validation->set_rules('add_gender', 'Gender');
        $this->form_validation->set_rules('add_date_birth', 'Birth Date');

		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$add_email = $this->input->post("add_email",true);
            $add_full_name = $this->input->post("add_full_name",true);
            $add_username = $this->input->post("add_username",true);
            $add_password = $this->input->post("add_password",true);
            $add_country = $this->input->post("add_country",true);
        	$add_city = $this->input->post("add_city",true);
            $add_gender = $this->input->post("add_gender",true);
            $add_date_birth = $this->input->post("add_date_birth",true);

            $additional_data = array(
                "username"=> $add_username,
                "full_name"=> $add_full_name,
                "country_id"=> 1,
                "city_id" => $add_city,
                "gender" => $add_gender,
                "dateBirth" => $add_date_birth
            );
            $group = array('2');

            if (!$this->ion_auth->email_check($add_email))
            {
                if ($this->ion_auth->register($add_email, $add_password, $add_email, $additional_data, $group)) {
                    $response->message = "Register Successful";
                    $response->status = true;
                }else{
                    $response->message = "Register Failed";
                    $response->status = false;
                }
            }else{
                $response->message = "Account already exist";
                $response->status = false;
            }

        	$response->code = 200;
        }
        json_response($response);
	}

	public function delete()
	{		
		$response = get_ajax_response();
        if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$get_params = $this->input->get();
		if(empty($get_params)){
		    //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Id User', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $id = $this->input->get("id",true);
            if($this->ion_auth->delete_user($id)){
                $response->status = true;
                $response->message = "Data deleted successfully";
            }else{
                $response->status = false;
                $response->message = $this->ion_auth->errors();
            }
            $response->code = 200;
        }
        json_response($response);
	}

	public function edit()
	{
        $response = get_ajax_response();
        if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('id', 'Id User', 'required');
        $this->form_validation->set_rules('edit_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('edit_full_name', 'Name', 'required');
        $this->form_validation->set_rules('edit_username', 'Username', 'required');
//        $this->form_validation->set_rules('edit_country', 'Country');
        $this->form_validation->set_rules('edit_city', 'City');
        $this->form_validation->set_rules('edit_gender', 'Gender');
        $this->form_validation->set_rules('edit_date_birth', 'Birth Date');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $id = $this->input->post("id",true);
            $edit_email = $this->input->post("edit_email",true);
            $edit_full_name = $this->input->post("edit_full_name",true);
            $edit_username = $this->input->post("edit_username",true);
            $edit_password = $this->input->post("edit_password",true);
//            $edit_country = $this->input->post("edit_country",true);
            $edit_city = $this->input->post("edit_city",true);
            $edit_gender = $this->input->post("edit_gender",true);
            $edit_date_birth = $this->input->post("edit_date_birth",true);

            $additional_data = array(
                "email"=> $edit_email,
                "username"=> $edit_username,
                "full_name"=> $edit_full_name,
                "country_id"=> 1,
                "city_id" => $edit_city,
                "gender" => $edit_gender,
                "dateBirth" => $edit_date_birth
            );

            if($edit_password !== null){
                array_push($additional_data, array("password"=>$edit_password));
            }

            $group = array('2');

            if ($this->ion_auth->update_user($id, $additional_data, $group))
            {
                $response->message = "Update Successful";
                $response->status = true;
            }else{
                $response->message = $this->ion_auth->errors();
                $response->status = false;
            }

            $response->code = 200;
        }
        json_response($response);
    }
    
    public function export(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        //filter vars
        $query = $this->input->post('query', true);

        $table = 'users';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("email","full_name","username");
        $select_fields =  array("users.id", "username", "email", "full_name", "country.name as country", "city.name as city", "gender", "dateBirth", 'phone_number', 'oauth_provider');

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.country_id = country.id";
        $join->table = "country";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.city_id = city.id";
        $join->table = "city";
        array_push($extra_join,$join);

        $result = $this->Datatable_model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;  

        $arr = array();
        if(!empty($list)){
            foreach ($list as $result) {
                $result->gender = $result->gender == 1 ? 'Male' : ($result->gender == 2 ? 'Female' : 'Prefer Not to Say');
                $arr[] = array(
                    $result->id,
                    $result->email, 
                    $result->full_name, 
                    $result->username,
                    $result->city,
                    $result->gender,
                    $result->dateBirth,
                    $result->oauth_provider
                );
            }
        }
        $field = array('Id', 'Email', 'Name', 'Username', 'City', 'Gender', 'Date of Birth', 'Oauth Provider');
        //do export
		export_csv($arr,$field);   
		     
    }

    public function print(){   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }

        //filter vars
        $query = $this->input->post('query', true);

        $table = 'users';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $search_fields = array("email","full_name","username");
        $select_fields =  array("users.id", "username", "email", "full_name", "country.name as country", "city.name as city", "gender", "dateBirth", 'phone_number', 'oauth_provider');

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.country_id = country.id";
        $join->table = "country";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.city_id = city.id";
        $join->table = "city";
        array_push($extra_join,$join);

        $result = $this->Datatable_model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;   

        $arr = array();
        if(!empty($list)){
            foreach ($list as $result) {
                $arr[] = array(
                    "id"=>$result->id,
                    "email"=>$result->email,
                    "full_name"=> $result->full_name, 
                    "username"=>$result->username,
                    "city"=>$result->city,
                    "gender"=>$result->gender == 1 ? 'Male' : ($result->gender == 2 ? 'Female' : 'Prefer Not to Say'),
                    "dateBirth"=>$result->dateBirth,
                    "oauth_provider"=>$result->oauth_provider
                );
            }
        }

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("ocw_user/print",array("data"=>$arr),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }
 
         echo json_encode($response);    
		     
    }
}
