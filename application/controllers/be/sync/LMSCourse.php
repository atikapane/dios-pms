<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class LMSCourse extends BaseBEController {

	function __construct() 
	{
		parent::__construct();		
		$this->load->model('Sync_LMS_Course_Model');
	}

	private function get_last_sync()
	{
		$last_sync = $this->Sync_LMS_Course_Model->get_last_sync();	
		if(empty($last_sync)){
			$last_sync = new stdClass();
			$last_sync->date = "N/A";
		}
		return $last_sync;
	}

	public function index()
	{	
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
		$data['privilege'] = $this->privilege;  
		$data['sync_category_types'] = $this->config->item('sync_category_types');
		$data['user_list']  = $this->web_app_model->get_data('user',array());
		$parent_types = array('category_type'=>'STUDYPROGRAM','sync_category.category_type'=>'FACULTY');
		$data['parent_categories']  = $this->web_app_model->get_data_or_where('sync_category',$parent_types);
		
		$data['last_sync'] = $this->get_last_sync();	
		$this->load->view('sync/lmscourse/index.php',$data);
	}

	public function datatable()
	{		

		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }					
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $limit = 10;
        $offset = 0;
        if(array_key_exists("page", $pagination) && array_key_exists("perpage", $pagination)){
        	$limit = $pagination["perpage"];
        	$offset = (($pagination["page"] -1) * $limit) ;
        }
        $table = 'sync_lms_course';
        $select_fields = array('faculty.category_name as faculty_name','study_program.category_name as study_program_name','semester','subject_code','subject_name','class','study_program.category_parent_id as faculty_id','subject_id');

        if(!empty($sort)){
	        if(array_key_exists("field", $sort) && array_key_exists("sort", $sort)){
	        	$column_order = array("value"=>$sort["field"],"direction"=>$sort["sort"]);
			}
		}

        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "sync_lms_course.category_id = school_year.category_id";
        $join->table = "sync_category school_year";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "school_year.category_parent_id = study_program.category_id";
        $join->table = "sync_category study_program";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "study_program.category_parent_id = faculty.category_id";
        $join->table = "sync_category faculty";
        array_push($extra_join,$join);

    	array_push($extra_where, "(study_program.category_type = 'STUDYPROGRAM')");

        $search_fields =   array('faculty.category_name','study_program.category_name','semester','subject_code','subject_name','class');

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join,$select_fields);
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $data[] = $result;
            }
        }

        $output = array(
        	"meta" => $meta,
            "data" => $data
        );

        echo json_encode($output);
	}

	public function datatable_history()
	{		

		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }				
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'sync_lms_course_history';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("sync_lms_course_history.id as id","user.username as username","date");
        $search_fields = array("sync_lms_course_history.id","user.username","date");

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "user.id = sync_lms_course_history.user_id";
        $join->table = "user";
        array_push($extra_join,$join);

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join,$select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
                
                $row->id = $result->id;
                $row->username = $result->username;
                $row->date = $result->date;

                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
	}

	//sync functions
	private function get_igracias_lms_course_data()
	{
		$this->load->helper("curl_helper");
		$igracias_url 		= $this->config->item('igracias_url_lms_course');
		$igracias_username 	= $this->config->item('igracias_api_username');
		$igracias_password 	= $this->config->item('igracias_api_password');

		$response = basic_auth_post($igracias_url,$igracias_username,$igracias_password,array());
		return json_decode($response);
	}
	
	public function sync_igracias()
	{
		$response = get_ajax_response();
		if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	

		$course_data = $this->get_igracias_lms_course_data();
		if($course_data == null){
			//igracias failed to respond
        	$response->code = 500;
        	$response->message = "Cannot reach iGracias at the moment, please try again later";
		}else if(!is_array($course_data)){
			//igracias responds with unknown data structure
        	$response->code = 500;
        	$response->message = "iGracias response is invalid, please contact administrator";
		}else{
			$course_rows = array();
			foreach ($course_data as $course) {
				$course_row = array();

				//clean up data, TODO : check which fields are mandatory
				if(property_exists($course, "course_id")){
					$course_row["course_id"] = $course->course_id;
				}else{
					$course_row["course_id"] = "";
				}

				if(property_exists($course, "category_id")){
					$course_row["category_id"] = $course->category_id;
				}else{
					$course_row["category_id"] = "";
				}
				
				if(property_exists($course, "semester")){
					$course_row["semester"] = $course->semester;
				}else{
					$course_row["semester"] = "";
				}
				
				if(property_exists($course, "class")){
					$course_row["class"] = $course->class;
				}else{
					$course_row["class"] = "";
				}
				
				if(property_exists($course, "subject_code")){
					$course_row["subject_code"] = $course->subject_code;
				}else{
					$course_row["subject_code"] = "";
				}
				
				if(property_exists($course, "subject_name")){
					$course_row["subject_name"] = $course->subject_name;
				}else{
					$course_row["subject_name"] = "";
				}
				
				if(property_exists($course, "sync_status")){
					$course_row["sync_status"] = $course->sync_status;
				}else{
					$course_row["sync_status"] = "";
				}
				
				if(property_exists($course, "sync_date")){
					$course_row["sync_date"] = $course->sync_date;
				}else{
					$course_row["sync_date"] = "";
				}
				
				if(property_exists($course, "flag_status")){
					$course_row["flag_status"] = $course->flag_status;
				}else{
					$course_row["flag_status"] = "";
				}
				
				if(property_exists($course, "table_owner")){
					$course_row["table_owner"] = $course->table_owner;
				}else{
					$course_row["table_owner"] = "";
				}
				
				if(property_exists($course, "table_id")){
					$course_row["table_id"] = $course->table_id;
				}else{
					$course_row["table_id"] = "";
				}
				
				if(property_exists($course, "subject_id")){
					$course_row["subject_id"] = $course->subject_id;
				}else{
					$course_row["subject_id"] = "";
				}

				if(property_exists($course, "user_id")){
					$course_row["user_id"] = $course->user_id;
				}else{
					$course_row["user_id"] = "";
				}

				if(property_exists($course, "user_username")){
					$course_row["user_username"] = $course->user_username;
				}else{
					$course_row["user_username"] = "";
				}

				if(property_exists($course, "employeeid")){
					$course_row["employeeid"] = $course->employeeid;
				}else{
					$course_row["employeeid"] = "";
				}

				if(property_exists($course, "lecturercode")){
					$course_row["lecturercode"] = $course->lecturercode;
				}else{
					$course_row["lecturercode"] = "";
				}

				if(property_exists($course, "employee_name")){
					$course_row["employee_name"] = $course->employee_name;
				}else{
					$course_row["employee_name"] = "";
				}

				$this->Sync_LMS_Course_Model->upsert($course_row);
				$course_rows[] = $course_row;
			}

			//log sync history
			$user_id = get_logged_in_id();
			$this->Sync_LMS_Course_Model->log_history($user_id);// TODO : get info from session

        	$response->code = 200;
            $response->status = true;
        	$response->message = "Sync Success";
        	$response->data = $this->get_last_sync();
		}

		echo json_encode($response);
	}
	
}
