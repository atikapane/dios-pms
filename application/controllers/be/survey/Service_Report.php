<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . "controllers/be/BaseBEController.php");

class Service_Report extends BaseBEController
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('survey/report/service/index.php', $data);
    }

    public function datatable()
    {
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);

        $table = 'survey';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();
        $extra_where[] = 'survey.type = 2';

        $search_fields = array("survey.id", "survey.name", "survey.platform", 'user.fullname');
        $select_fields =  array('survey.id', 'survey.name', 'survey.start_date', 'survey.end_date', 'survey.platform', 'user.fullname');

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "user.id = survey.created_by";
        $join->table = "user";
        array_push($extra_join,$join);

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $data[] = $result;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function view(){
        if (!$this->can_read()) {
            redirect(base_url($this->full_path));
            return;
        }

        $survey_id = $this->input->get("survey_id", true);
        if(empty($survey_id)) redirect(base_url($this->full_path));
        $survey_object = $this->web_app_model->get_single_data("survey", ['id' => $survey_id]);

        if(!empty($survey_object)){
            $question_objects = $this->web_app_model->get_data('survey_question', ['survey_id' => $survey_object->id]);
            foreach($question_objects as &$question){
                if($question->type == 'multiple_choice'){
                    $question->choices = $this->web_app_model->get_data('survey_question_option', ['survey_question_id' => $question->id]);
                }
            }
            $survey_object->questions = $question_objects;
    
            $created_by = $this->web_app_model->get_single_data('user', ['id' => $survey_object->created_by]);
            $survey_object->created_by = $created_by->fullname;
            $total_participant = $this->web_app_model->get_data('survey_respondent', ['survey_id' => $survey_id]);
            $count = count($total_participant);
        }


        $data['survey_id'] = $survey_id;
        $data['data'] = !empty($survey_object) ? $survey_object : '';
        $data['total_respondent'] = isset($count) ? $count : 0;
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('survey/report/service/view.php', $data);
    }

    public function get_question(){
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Survey Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
            $response->status = false;
        } else {
            $survey_id = $this->input->get("id", true);
            if(!empty($survey_id)){
                $question_objects = $this->web_app_model->get_data('survey_question', ['survey_id' => $survey_id]);
                foreach($question_objects as &$question){
                    if($question->type == 'multiple_choice'){
                        $question->choices = $this->web_app_model->get_data('survey_question_option', ['survey_question_id' => $question->id]);
                    }
                }

                $response->code = 200;
                $response->status = true;
                $response->data = $question_objects;
            }else{
                $response->code = 400;
                $response->message = "Survey not found";
                $response->status = false;
            }
        }
        echo json_encode($response);
    }

    public function answer(){
        if (!$this->can_read()) {
            redirect(base_url($this->full_path));
            return;
        }

        $question_id = $this->input->get('question_id', true);
        $question_object = $this->web_app_model->get_single_data('survey_question', ['id' => $question_id]);
        $data['survey_id'] = $this->input->get('survey_id', true);
        $data['question_id'] = $question_id;
        $data['question_type'] = $question_object->type;
        $data['breadcrumb'] = $this->breadcrumb;
        $data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $this->load->view('survey/report/service/answer.php', $data);
    }

    public function answer_datatable(){
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination", true);
        $sort = $this->input->post("sort", true);
        $query = $this->input->post("query", true);
        $survey_id = $this->input->post("survey_id", true);
        $question_id = $this->input->post("question_id", true);

        $question_objects = $this->web_app_model->get_data('survey_question', ['survey_id' => $survey_id]);
        foreach($question_objects as $key => $question){
            if($question->id == $question_id){
                $question_object = $question;
                $arrKey = $key;
                break;
            }
        }

        $table = 'survey_respondent as respondent';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();
        $extra_where[] = "respondent.survey_id = {$survey_id}";

        $search_fields = array("respondent.respondent_name");
        $select_fields =  array("respondent.*");

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $answer_objects = $this->web_app_model->get_data('survey_respondent_answer', ['survey_respondent_id' => $result->id]);
                if($question_object->type == 'freetext'){
                    $result->answer = $answer_objects[$arrKey]->answer_text;
                }else if($question_object->type == 'rating'){
                    $result->answer = $answer_objects[$arrKey]->answer_num;
                }else if($question_object->type == 'multiple_choice'){
                    $choice_objects = $this->web_app_model->get_data('survey_question_option', ['survey_question_id' => $question_id]);
                    $result->answer = $choice_objects[$answer_objects[$arrKey]->answer_num]->content;
                }
                $data[] = $result;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }

    public function get_statistic(){
        $response = get_ajax_response();
        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $this->load->model('Datatable_model');

        $query = $this->input->get("search", true);
        $survey_id = $this->input->get("survey_id", true);
        $question_id = $this->input->get("question_id", true);

        $question_objects = $this->web_app_model->get_data('survey_question', ['survey_id' => $survey_id]);
        foreach($question_objects as $key => $question){
            if($question->id == $question_id){
                $question_object = $question;
                $arrKey = $key;
                break;
            }
        }

        $table = 'survey_respondent as respondent';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();
        $extra_where[] = "respondent.survey_id = {$survey_id}";

        $search_fields = array("respondent.respondent_name");
        $select_fields =  array("respondent.id, respondent.respondent_name");

        $result = $this->Datatable_model->get_data_export($table,$query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $data = [];
        if (!empty($list)) {
            if($question_object->type == 'rating'){
                $data[1] = 0;
                $data[2] = 0;
                $data[3] = 0;
                $data[4] = 0;
                $data[5] = 0;

                foreach ($list as $result) {
                    $answer_objects = $this->web_app_model->get_data('survey_respondent_answer', ['survey_respondent_id' => $result->id]);
                    $answer = $answer_objects[$arrKey]->answer_num;
                    $data[$answer]++;
                }
            }else if($question_object->type == 'multiple_choice'){
                foreach ($list as $result) {
                    $answer_objects = $this->web_app_model->get_data('survey_respondent_answer', ['survey_respondent_id' => $result->id]);
                    $choice_objects = $this->web_app_model->get_data('survey_question_option', ['survey_question_id' => $question_id]);
                    $answer = $choice_objects[$answer_objects[$arrKey]->answer_num]->content;
                    if(isset($data[$answer])){
                        $data[$answer]++;
                    }else{
                        $data[$answer] = 1;
                    }
                }
            }
        }
        $response->code = 200;
        $response->status = true;
        $response->data = $data;
        $response->question = isset($question_object) ? $question_object : '';
        $response->total_participant = count($list);

        echo json_encode($response);
    }

    public function export(){   

        if (!$this->can_read()) {
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }
        $survey_id = $this->input->post('id', true);

        $field = ['Timestamp', 'Respondent Id', 'Respondent Username', 'Respondent Name', 'Respondent Email', 'Respondent Origin'];
        $question_objects = $this->web_app_model->get_data('survey_question', ['survey_id' => $survey_id]);
        foreach($question_objects as $question){
            $field[] = $question->question;
        }

        $arr = [];
        $respondent_objects = $this->web_app_model->get_data('survey_respondent', ['survey_id' => $survey_id]);
        foreach($respondent_objects as $respondent){
            $data = [$respondent->created_date, $respondent->respondent_id, $respondent->respondent_username, $respondent->respondent_name, $respondent->respondent_email, $respondent->respondent_origin];
            $answer_objects = $this->web_app_model->get_data('survey_respondent_answer', ['survey_respondent_id' => $respondent->id]);

            $x = 0;
            foreach($answer_objects as $answer){
                if(!empty($answer->answer_text)){
                    $data[] = $answer->answer_text;
                }else{
                    if($question_objects[$x]->type == 'rating'){
                        $data[] = $answer->answer_num;
                    }else{
                        $choice_objects = $this->web_app_model->get_data('survey_question_option', ['survey_question_id' => $question_objects[$x]->id]);
                        $data[] = $choice_objects[$answer->answer_num]->content;
                    }
                }
                $x++;
            }
            $arr[] = $data;
        }

        //do export
        $this->load->helper('export');
		export_csv($arr,$field);   
		     
    }
}
