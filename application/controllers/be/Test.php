<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Test extends BaseBEController {

	function __construct() 
	{
		parent::__construct();
		$this->load->library('graph');
	}

	public function index()
	{
		$token = $this->get_graph_jwt();
		$data = array(
			"subject_id" => 1234,
			"api_app_id" => "C2LtRh9T2nMjrjKf",
			"api_app_secret" => "NZVFw3hbsvbvGtcjZk9iPiczPthn8O5B",
			"cds_url_course_site_id" => $this->config->item("cds_url_course_site_id"),
			"graph_url_sharepoint" => $this->config->item("graph_url_sharepoint"),
			"list_item" => arraY(
				0 => array(
					"id" => 6737,
					"url" => 'https://telkomuniversityofficial.sharepoint.com/CELOE/mkc/Shared Documents/Focusing on Service Process and Quality 1-40275.pdf',
				),
				1 => array(
					"id" => 6738,
					"url" => 'https://telkomuniversityofficial.sharepoint.com/CELOE/mkc/Shared Documents/TU-logo-primer-horizontal-40275.jpg',
				),
				2 => array(
					"id" => 9514,
					"url" => 'https://telkomuniversityofficial.sharepoint.com/CELOE/mkc/Shared Documents/Video Testing 141119-Kode Dosen.mp4',
				),
				3 => array(
					"id" => 9666,
					"url" => 'https://telkomuniversityofficial.sharepoint.com/CELOE/mkc/Shared Documents/Video Testing H5P-40275.mp4',
				)
			)
		);
		$result = $this->graph->create_public_link($token,$data);
		var_dump($result);
	}
}