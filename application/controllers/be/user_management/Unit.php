<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Unit extends BaseBEController {

	function __construct() 
	{
        parent::__construct();				
        $this->load->model('Web_app_model');
	}
	
	public function index()
	{		
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;	
		$this->load->view('unit/index.php',$data);
	}

	public function datatable()
	{		

		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }   			
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);	
        $query = $this->input->post("query",true);

        $table = 'sync_category as unit';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();


        $select_fields =  array("unit.category_id as unit_id", "unit.category_name as unit_name", "directorate.category_id as directorate_id", "directorate.category_name as directorate_name");
        $search_fields =  array("unit.category_name", "unit.category_id");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "unit.category_parent_id = directorate.category_id";
        $join->table = "sync_category as directorate";
        array_push($extra_join,$join);

        $extra_where[] = 'unit.category_type = "STUDYPROGRAM"';

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $data[] = $result;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }
    
    public function get(){
        $response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $or_where = [
            'c_directorate_id' => 0,
            'c_unit_id' => 0,
        ];
        $users = $this->Web_app_model->get_data_or_where('user', $or_where);

        $response->code = 200;
        $response->data = $users;
        $response->status = true;
        $response->message = "Get data success";

        echo json_encode($response);
    }

	public function edit()
	{			
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        
        $this->form_validation->set_rules('user[]', 'User', 'required');
        $this->form_validation->set_rules('unit_id', 'Unit Id', 'required');
        $this->form_validation->set_rules('directorate_id', 'Directorate Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $id = $this->input->post("user",true);

            $update_data['c_directorate_id'] = $this->input->post('directorate_id', true);
            $update_data['c_unit_id'] = $this->input->post('unit_id', true);

            $where = [
                'unit.category_id' => $update_data['c_unit_id'],
                'unit.category_parent_id' => $update_data['c_directorate_id']
            ];
            $select = 'unit.category_id, unit.category_name as unit, unit.category_parent_id, directorate.category_name as directorate';
            $category_object = $this->web_app_model->get_data_join('sync_category as unit', 'sync_category as directorate', '', 'unit.category_parent_id = directorate.category_id', '', $where, $select);

            $update_data['directorate'] = $category_object[0]->directorate;
            $update_data['unit'] = $category_object[0]->unit;
            
            foreach($id as $user){
                $user_object = $this->web_app_model->get_single_data("user",['id' => $user]);
                if(!empty($user_object)){
                    $this->web_app_model->update_data("user",$update_data,$user,"id");        
                }
            }

            $response->code = 200;
            $response->status = true;
            $response->message = "Data updated successfully";
            
        }
        echo json_encode($response);
   		
    }
    
    public function upload(){
        if($this->input->post('import_submit')){
            // Form field validation rules
            $files = $_FILES;
            $this->form_validation->set_data($files);
            $this->form_validation->set_rules('import_csv', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
            if($this->form_validation->run() == true){

                // If file uploaded
                if(is_uploaded_file($_FILES['import_csv']['tmp_name'])){
                    $this->load->library('CSVReader');
                    $csv_data = $this->csvreader->parse_csv($_FILES['import_csv']['tmp_name']);

                    $new_data = [];
                    foreach($csv_data as $key => $row){
                        $new_row = new stdClass();
                        $new_row->employee_id = $row['Employee Id'];
                        $new_row->directorate_name = $row['Directorate Name'];
                        $new_row->unit_name = $row['Unit Name'];
                        $new_row->error = [];

                        $user_object = $this->Web_app_model->get_single_data('user', ['employeeid' => $new_row->employee_id]);
                        $unit_object = $this->Web_app_model->get_single_data('sync_category', ['category_name' => $new_row->unit_name]);
                        $directorate_object = $this->web_app_model->get_single_data('sync_category', ['category_name' => $new_row->directorate_name]);

                        if (empty($user_object)) $new_row->error['employee_id'][] = 'Employee not found';
                        if(empty($unit_object)) $new_row->error['unit_name'][] = 'Unit not found';
                        if(empty($directorate_object)) $new_row->error['directorate_name'][] = 'Directorate not found';
                        if(!empty($unit_object) && !empty($directorate_object)){
                            if($unit_object->category_parent_id != $directorate_object->category_id) $new_row->error['unit_name'][] = 'Unit not under the Directorate';
                        }

                        $new_data[] = $new_row;
                    }
                }
            }
        }

        $data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $data['table'] = $new_data;
		$this->load->view('unit/import.php',$data);
    }

    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['import_csv']['name']) && $_FILES['import_csv']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['import_csv']['name']);
            $file_arr = explode('.', $_FILES['import_csv']['name']);
            $ext = end($file_arr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }

    public function import(){
        $response = get_ajax_response();
        if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $data = $this->input->post('data');
        $data = json_decode($data);

        foreach($data as $user){
            $primary_key['id'] = $user->employee_id;
            $unit_object = $this->Web_app_model->get_single_data('sync_category', ['category_name' => $user->unit_name]);
            $directorate_object = $this->web_app_model->get_single_data('sync_category', ['category_name' => $user->directorate_name]);

            $update_data['c_directorate_id'] = $directorate_object->category_id;
            $update_data['directorate'] = $user->directorate_name;
            $update_data['c_unit_id'] = $unit_object->category_id;
            $update_data['unit'] = $user->unit_name;

            $this->web_app_model->update_data("user",$update_data,$primary_key['id'],"employeeid");
        }

        $response->code = 200;
        $response->status = true;
        $response->message = "Data updated successfully";

        echo json_encode($response);
    }

    public function export(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');
        $this->load->model('Unit_Mapping_Model');

        $query = $this->input->post("query",true);

        $table = 'sync_category as unit';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();


        $select_fields =  array("unit.category_id as unit_id", "unit.category_name as unit_name", "directorate.category_id as directorate_id", "directorate.category_name as directorate_name");
        $search_fields =  array("category_name");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "unit.category_parent_id = directorate.category_id";
        $join->table = "sync_category as directorate";
        array_push($extra_join,$join);

        $extra_where[] = 'unit.category_type = "STUDYPROGRAM"';

        $result = $this->Unit_Mapping_Model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $arr = array();
        foreach ($list as $row) {
            $arr[] = array($row->unit_id, $row->directorate_name,$row->unit_name);
         }
         $filed = array('Id','Directorate','Unit');
         //do export
        export_csv($arr,$filed);         
    }

    public function print_unit(){ 
        if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }   			
        $this->load->model('Datatable_model');
        $this->load->model('Unit_Mapping_Model');

        $query = $this->input->post("query",true);

        $table = 'sync_category as unit';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();


        $select_fields =  array("unit.category_id as unit_id", "unit.category_name as unit_name", "directorate.category_id as directorate_id", "directorate.category_name as directorate_name");
        $search_fields =  array("category_name");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "unit.category_parent_id = directorate.category_id";
        $join->table = "sync_category as directorate";
        array_push($extra_join,$join);

        $extra_where[] = 'unit.category_type = "STUDYPROGRAM"';

        $result = $this->Unit_Mapping_Model->get_data_export($table, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("unit/print",array("data"=>$list),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);        
    }
}
