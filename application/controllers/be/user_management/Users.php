<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Users extends BaseBEController {

	function __construct()
	{
		parent::__construct();				
	}
	
	public function index()
	{		
		//TODO : CHECK PRIVILEGE		
        //TODO : CHECK LOGIN	
        $import_error = $this->session->flashdata('import_error');
        if($import_error) $data['import_error'] = $import_error;
        $data['cities'] = $this->Web_App_Model->get_data('city', ['country_id' => 1]);
        $data['departemen'] = $this->Web_App_Model->get_data('departemen');	
        $data['max_upload'] = !empty($this->config->item('user_max_upload')) ? $this->config->item('user_max_upload') : 0;
        $data["groups"] = $this->web_app_model->get_data("group");
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;	
		$this->load->view('users/index.php',$data);
	}

	public function datatable()
	{		
		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }   			
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);	
        $query = $this->input->post("query",true);

        $table = 'user';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "departemen.departemen_id = user.departemen";
        $join->table = "departemen";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "city.id = user.tempat_lahir";
        $join->table = "city";
        array_push($extra_join,$join);

        $select_fields =  array("user.id","employeeid","fullname","status_pegawai","gender","name","tgl_lahir","usia","tgl_bergabung","departemen_name","posisi","npwp","no_ktp","no_bpjs_kesehatan","nama_faskes","no_bpjs_ketenagakerjaan","pendidikan_terakhir","nama_sekolah","jurusan","ipk","tahun_lulus","no_seri_ijazah","no_hp","no_tlp_rumah","email","alamat_ktp","kode_pos","alamat_domisili","no_darurat","nama_keluarga","hubungan_keluarga","keahlian_khusus","pengalaman_project","riwayat_training","account_no","owner","bank","bank_branch","agama","status_keluarga","tenaga_kerja","istri_suami","anak","copy_ijazah","copy_transkrip_nilai","copy_ktp","copy_npwp","cv");
        $search_fields =  array("fullname","username","employeeid");

        $group_by = "user.id";

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields, $group_by);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();

                $row->id = $result->id;
                $row->employeeid=$result->employeeid;
                $row->fullname=$result->fullname;
                if ($result->status_pegawai == "1") { $status = "Tetap"; } 
                elseif ($result->status_pegawai == "2") { $status = "Kontrak"; }
                elseif ($result->status_pegawai == "3") { $status = "Freelance"; }
                elseif ($result->status_pegawai == "99") { $status = "Keluar"; }
                else { $status = ""; }
                $row->status_pegawai = $status;
                $row->gender= ($result->gender = 1 )? "Pria" : "Wanita" ;
                $row->tempat_lahir=$result->name;
                $row->tgl_lahir=$result->tgl_lahir;
                $row->usia=$result->usia;
                $row->tgl_bergabung=$result->tgl_bergabung;
                $row->departemen_name=$result->departemen_name;
                $row->posisi=$result->posisi;
                $row->npwp=$result->npwp;
                $row->no_ktp=$result->no_ktp;
                $row->no_bpjs_kesehatan=$result->no_bpjs_kesehatan;
                $row->nama_faskes=$result->nama_faskes;
                $row->no_bpjs_ketenagakerjaan=$result->no_bpjs_ketenagakerjaan;
                $row->pendidikan_terakhir=$result->pendidikan_terakhir;
                $row->nama_sekolah=$result->nama_sekolah;
                $row->jurusan=$result->jurusan;
                $row->ipk=$result->ipk;
                $row->tahun_lulus=$result->tahun_lulus;
                $row->no_seri_ijazah=$result->no_seri_ijazah;
                $row->no_hp=$result->no_hp;
                $row->no_tlp_rumah=$result->no_tlp_rumah;
                $row->email=$result->email;
                $row->alamat_ktp=$result->alamat_ktp;
                $row->kode_pos=$result->kode_pos;
                $row->alamat_domisili=$result->alamat_domisili;
                $row->no_darurat=$result->no_darurat;
                $row->nama_keluarga=$result->nama_keluarga;
                $row->hubungan_keluarga=$result->hubungan_keluarga;
                $row->keahlian_khusus=$result->keahlian_khusus;
                $row->pengalaman_project=$result->pengalaman_project;
                $row->riwayat_training=$result->riwayat_training;
                $row->account_no=$result->account_no;
                $row->owner=$result->owner;
                $row->bank=$result->bank;
                $row->bank_branch=$result->bank_branch;
                $row->agama=$result->agama;
                $row->status_keluarga=$result->status_keluarga;
                $row->tenaga_kerja=$result->tenaga_kerja;
                $row->istri_suami=$result->istri_suami;
                $row->anak=$result->anak;
                $row->copy_ijazah=!empty($result->copy_ijazah) ? "<a href='".base_url().$result->copy_ijazah."' target='_blank'>View</a>" : "";
                $row->copy_transkrip_nilai=!empty($result->copy_transkrip_nilai) ? "<a href='".base_url().$result->copy_transkrip_nilai."' target='_blank'>View</a>" : "";
                $row->copy_ktp=!empty($result->copy_ktp) ? "<a href='".base_url().$result->copy_ktp."' target='_blank'>View</a>" : "";
                $row->copy_npwp=!empty($result->copy_npwp) ? "<a href='".base_url().$result->copy_npwp."' target='_blank'>View</a>" : "";
                $row->cv=!empty($result->cv) ? "<a href='".base_url().$result->cv."' target='_blank'>View</a>" : "";


                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
	}

	public function export(){   
        error_reporting(0);
        //get value
        $search = $this->input->post('search');   
        
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="user";
        $order = "id"; 
        $filter = $search;
        $extra_join = [];

        $column_search =  array("fullname","username","employeeid");
        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "departemen.departemen_id = user.departemen";
        $join->table = "departemen";
        array_push($extra_join,$join);
        
        $custom_select =  array("employeeid","fullname","status_pegawai","gender","tempat_lahir","tgl_lahir","usia","tgl_bergabung","departemen_name","posisi","npwp","no_ktp","no_bpjs_kesehatan","nama_faskes","no_bpjs_ketenagakerjaan","pendidikan_terakhir","nama_sekolah","jurusan","ipk","tahun_lulus","no_seri_ijazah","no_hp","no_tlp_rumah","email","alamat_ktp","kode_pos","alamat_domisili","no_darurat","nama_keluarga","hubungan_keluarga","keahlian_khusus","pengalaman_project","riwayat_training","account_no","owner","bank","bank_branch","agama","status_keluarga","tenaga_kerja","istri_suami","anak","copy_ijazah","copy_transkrip_nilai","copy_ktp","copy_npwp", "cv");

        $extra_where =false;
        $extra_where_or=false; 
        //$extra_join=false; 
        // $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $group_by = 'user.id';
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit, $group_by);

        $arr = array();
        foreach ($result as $row) {
            $copy_ijazah= "".base_url($row->copy_ijazah)."";
            $copy_transkrip_nilai= "".base_url($row->copy_transkrip_nilai)."";
            $copy_ktp= "".base_url($row->copy_ktp)."";
            $copy_npwp= "".base_url($row->copy_npwp)."";
            $cv = "".base_url($row->cv)."";
            //$image= "".base_url($row->img_path)."";
            $arr[] = array($row->employeeid,$row->fullname,$row->status_pegawai,$row->gender,$row->tempat_lahir,$row->tgl_lahir,$row->usia,$row->tgl_bergabung,$row->departemen,$row->posisi,$row->npwp,$row->no_ktp,$row->no_bpjs_kesehatan,$row->nama_faskes,$row->no_bpjs_ketenagakerjaan,$row->pendidikan_terakhir,$row->nama_sekolah,$row->jurusan,$row->ipk,$row->tahun_lulus,$row->no_seri_ijazah,$row->no_hp,$row->no_tlp_rumah,$row->email,$row->alamat_ktp,$row->kode_pos,$row->alamat_domisili,$row->no_darurat,$row->nama_keluarga,$row->hubungan_keluarga,$row->keahlian_khusus,$row->pengalaman_project,$row->riwayat_training,$row->account_no,$row->owner,$row->bank,$row->bank_branch,$row->agama,$row->status_keluarga,$row->tenaga_kerja,$row->istri_suami,$row->anak,$copy_ijazah,$copy_transkrip_nilai,$copy_ktp,$copy_npwp,$cv);
         }
        $filed = array("employeeid","fullname","status_pegawai","gender","tempat_lahir","tgl_lahir","usia","tgl_bergabung","departemen_name","posisi","npwp","no_ktp","no_bpjs_kesehatan","nama_faskes","no_bpjs_ketenagakerjaan","pendidikan_terakhir","nama_sekolah","jurusan","ipk","tahun_lulus","no_seri_ijazah","no_hp","no_tlp_rumah","email","alamat_ktp","kode_pos","alamat_domisili","no_darurat","nama_keluarga","hubungan_keluarga","keahlian_khusus","pengalaman_project","riwayat_training","account_no","owner","bank","bank_branch","agama","status_keluarga","tenaga_kerja","istri_suami","anak","copy_ijazah","copy_transkrip_nilai","copy_ktp","copy_npwp","cv");
        
        //do export
        export_csv($arr,$filed);        
    }

    public function print_users(){
        error_reporting(0);
        
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="user";
        $order = "id"; 
        $filter = $search;
        $extra_join = [];

        $column_search =  array("fullname","username","employeeid");
        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "departemen.departemen_id = user.departemen";
        $join->table = "departemen";
        array_push($extra_join,$join);
        
        $custom_select =  array("employeeid","fullname","status_pegawai","gender","tempat_lahir","tgl_lahir","usia","tgl_bergabung","departemen_name","posisi","npwp","no_ktp","no_bpjs_kesehatan","nama_faskes","no_bpjs_ketenagakerjaan","pendidikan_terakhir","nama_sekolah","jurusan","ipk","tahun_lulus","no_seri_ijazah","no_hp","no_tlp_rumah","email","alamat_ktp","kode_pos","alamat_domisili","no_darurat","nama_keluarga","hubungan_keluarga","keahlian_khusus","pengalaman_project","riwayat_training","account_no","owner","bank","bank_branch","agama","status_keluarga","tenaga_kerja","istri_suami","anak","copy_ijazah","copy_transkrip_nilai","copy_ktp","copy_npwp", "cv");

        $extra_where =false;
        $extra_where_or=false; 
        //$extra_join=false; 
        // $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $group_by = 'user.id';
        
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit, $group_by);
        
        $response = get_ajax_response();
        if ($result){
            $data = $this->load->view("users/print",array("data"=>$result),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);
        
    }


	public function get()
	{		
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'User ID', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $this->load->model('Users_model');
        	$id = $this->input->get("id",true);
        	$user_object = null;
        	if(!empty($id)){
        		$query_data['id'] = $id;
        		$user_object = $this->Users_model->get_user_byid($id);//$this->web_app_model->get_single_data("user",$query_data);
                $user_object->roles = $this->Users_model->get_user_roles($id);//$this->web_app_model->get_single_data("user",$query_data);

        	}

        	$response->code = 200;
        	$response->data = $user_object;

        	if(!empty($user_object)){
	        	$response->status = true;
	        	$response->message = "Get data success";
        	}else{
	        	$response->status = false;
	        	$response->message = "Data not found";
        	}
        }
        echo json_encode($response);
	}

	public function edit()
	{			
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        
    	$id = $this->input->post("id",true);	

   		$primary_key['id'] = $id;

    	$group  = $this->input->post('group');
    	$del['userid'] = $id;
		$roles_object = $this->web_app_model->delete_data('roles',$del);//insert to role
		for ($i=0; $i < count($group); $i++) { 
			$role['userid']  = $id;
			$role['groupid'] = $group[$i];
			$roles_object = $this->web_app_model->insert_data('roles',$role);
		}

		$user_object = $this->web_app_model->get_single_data("user",$primary_key);

		if(!empty($user_object)){
        	$response->code = 200;
        	$response->status = true;
        	$response->message = "Data updated successfully";
		}else{
        	$response->code = 400;
        	$response->status = false;
        	$response->message = "Data not found";
		}
        
        echo json_encode($response);
    }

    public function save(){
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]');
        $this->form_validation->set_rules('username', 'Username', 'required|alpha|min_length[6]|max_length[20]|is_unique[users.username]|regex_match[/^[a-z]*$/]', ['is_unique'=>'{field} already exists', 'regex_match' => '{field} must be lowercase']);
        $this->form_validation->set_rules('employee_id', 'NIP', 'required');
        $this->form_validation->set_rules('fullname', 'Nama Pegawai', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('status_pegawai','Status Pegawai', 'required');
        $this->form_validation->set_rules('gender','Gender', 'required');
        $this->form_validation->set_rules('tempat_lahir','Tempat lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir', 'required');
        $this->form_validation->set_rules('tgl_bergabung','Tanggal Bergabung', 'required');
        $this->form_validation->set_rules('departemen','Departemen', 'required');
        $this->form_validation->set_rules('posisi','Posisi', 'required');
        $this->form_validation->set_rules('no_ktp','No KTP', 'required');
        //$this->form_validation->set_rules('no_bpjs_ketenagakerjaan','No BPJS Ketenagakerjaan', 'required');
        $this->form_validation->set_rules('pendidikan_terakhir','Pendidikan Terakhir', 'required');
        $this->form_validation->set_rules('nama_sekolah','Nama Sekolah/Universitas', 'required');
        $this->form_validation->set_rules('jurusan','Jurusan', 'required');
        $this->form_validation->set_rules('tahun_lulus','Tahun Lulus', 'required');
        $this->form_validation->set_rules('no_seri_ijazah','No Seri ijazah', 'required');
        $this->form_validation->set_rules('no_hp','No Handphone', 'required');
        $this->form_validation->set_rules('alamat_ktp','Alamat KTP', 'required');
        $this->form_validation->set_rules('kode_pos','Kode POS', 'required');
        $this->form_validation->set_rules('alamat_domisili','Alamat domisili', 'required');
        $this->form_validation->set_rules('no_darurat','No Telepon Darurat', 'required');
        $this->form_validation->set_rules('nama_keluarga','Nama', 'required');
        $this->form_validation->set_rules('hubungan_keluarga','Hubungan keluarga', 'required');
        $this->form_validation->set_rules('account_no','Account No', 'required');
        $this->form_validation->set_rules('owner','Owner', 'required');
        $this->form_validation->set_rules('bank','Bank', 'required');
        $this->form_validation->set_rules('bank_branch','Cabang', 'required');
        $this->form_validation->set_rules('agama','Agama', 'required');
        $this->form_validation->set_rules('status_keluarga','Status Keluarga', 'required');
        $this->form_validation->set_rules('tenaga_kerja','Tenaga Kerja', 'required');
        $this->form_validation->set_rules('istri_suami','Istri/Suami', 'required');
        $this->form_validation->set_rules('anak','Anak', 'required');
        $this->form_validation->set_rules('copy_ijazah_name','Copy Ijazah', 'required');
        $this->form_validation->set_rules('copy_transkrip_nilai_name','Copy Transkrip Nilai', 'required');
        $this->form_validation->set_rules('copy_ktp_name','Copy KTP', 'required');
        $this->form_validation->set_rules('cv_name','CV', 'required');
        

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $insert_data['username'] = $this->input->post('username', true);
            $password = $this->input->post('password', true);
            $salt = $this->string_generator();
            $insert_data['password'] = md5($password.$salt);
            $insert_data['employeeid'] = $this->input->post('employee_id', true);
            $insert_data['fullname'] = $this->input->post('fullname', true);
            $insert_data['email'] = $this->input->post('email', true);
            $insert_data['status_pegawai'] =    $this->input->post('status_pegawai', true);
            $insert_data['gender'] =    $this->input->post('gender', true);
            $insert_data['tempat_lahir'] =  $this->input->post('tempat_lahir', true);
            $insert_data['tgl_lahir'] = $this->input->post('tgl_lahir', true);
            // $insert_data['usia'] =  $this->input->post('usia', true);
            $insert_data['tgl_bergabung'] = $this->input->post('tgl_bergabung', true);
            $insert_data['departemen'] =    $this->input->post('departemen', true);
            $insert_data['posisi'] =    $this->input->post('posisi', true);
            $insert_data['npwp'] =  $this->input->post('npwp', true);
            $insert_data['no_ktp'] =    $this->input->post('no_ktp', true);
            $insert_data['no_bpjs_kesehatan'] = $this->input->post('no_bpjs_kesehatan', true);
            $insert_data['nama_faskes'] =   $this->input->post('nama_faskes', true);
            $insert_data['no_bpjs_ketenagakerjaan'] =   $this->input->post('no_bpjs_ketenagakerjaan', true);
            $insert_data['pendidikan_terakhir'] =   $this->input->post('pendidikan_terakhir', true);
            $insert_data['nama_sekolah'] =  $this->input->post('nama_sekolah', true);
            $insert_data['jurusan'] =   $this->input->post('jurusan', true);
            $insert_data['ipk'] =   $this->input->post('ipk', true);
            $insert_data['tahun_lulus'] =   $this->input->post('tahun_lulus', true);
            $insert_data['no_seri_ijazah'] =    $this->input->post('no_seri_ijazah', true);
            $insert_data['no_hp'] = $this->input->post('no_hp', true);
            $insert_data['no_tlp_rumah'] =  $this->input->post('no_tlp_rumah', true);
            $insert_data['alamat_ktp'] =    $this->input->post('alamat_ktp', true);
            $insert_data['kode_pos'] =  $this->input->post('kode_pos', true);
            $insert_data['alamat_domisili'] =   $this->input->post('alamat_domisili', true);
            $insert_data['no_darurat'] =    $this->input->post('no_darurat', true);
            $insert_data['nama_keluarga'] = $this->input->post('nama_keluarga', true);
            $insert_data['hubungan_keluarga'] = $this->input->post('hubungan_keluarga', true);
            $insert_data['keahlian_khusus'] =   $this->input->post('keahlian_khusus', true);
            $insert_data['pengalaman_project'] =    $this->input->post('pengalaman_project', true);
            $insert_data['riwayat_training'] =  $this->input->post('riwayat_training', true);
            $insert_data['account_no'] =    $this->input->post('account_no', true);
            $insert_data['owner'] = $this->input->post('owner', true);
            $insert_data['bank'] =  $this->input->post('bank', true);
            $insert_data['bank_branch'] =   $this->input->post('bank_branch', true);
            $insert_data['agama'] = $this->input->post('agama', true);
            $insert_data['status_keluarga'] =   $this->input->post('status_keluarga', true);
            $insert_data['tenaga_kerja'] =  $this->input->post('tenaga_kerja', true);
            $insert_data['istri_suami'] =   $this->input->post('istri_suami', true);
            $insert_data['anak'] =  $this->input->post('anak', true);
            
            $upload_path = $this->config->item('user_upload_path');
            
            $ijazah_name = $this->input->post('copy_ijazah_name', true);
            $insert_data['copy_ijazah'] =  $upload_path.$ijazah_name;  
           
            $transkrip_name = $this->input->post('copy_transkrip_nilai_name', true);
            $insert_data['copy_transkrip_nilai'] =   $upload_path.$transkrip_name;

            $ktp_name = $this->input->post('copy_ktp_name', true);
            $insert_data['copy_ktp'] =   $upload_path.$ktp_name;
            
            $npwp_name = $this->input->post('copy_npwp_name', true);
            $insert_data['copy_npwp'] =   $upload_path.$npwp_name;
           
            $cv_name = $this->input->post('cv_name', true);
            $insert_data['cv'] = $upload_path.$cv_name;  
            $insert_data['is_manual_insert'] = 1;

            $insert = $this->web_app_model->insert_data("user", $insert_data);
            ;
            $response->code = 200;
            $response->status = true;
            $response->message = "Data submitted successfully";
        }
        echo json_encode($response);
    }

    public function delete()
    {       
        $response = get_ajax_response();
         if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if(empty($get_params)){ //set default param so the form validation triggered
            $get_params = array("id"=>"");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $user_id = $this->input->get("id",true);
            if(!empty($user_id)){
                $query_data['id'] = $user_id;
                $user_object = $this->web_app_model->get_single_data("user",$query_data);
                if(!empty($user_object)){
                    $delete_data['id'] = $user_id;
                    $this->web_app_model->delete_data("user",$delete_data);

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
                }else{
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }

    public function file_upload()
    {
        if(!$this->can_create() && !$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $config['upload_path'] = FCPATH.$this->config->item('user_upload_path');
        if(!file_exists($config['upload_path'])){
            mkdir($config['upload_path'],0777,true);
        }
        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
        $config['encrypt_name'] = false;
       
        $max_size_upload = $this->config->item('user_max_upload');
        if(!empty($max_size_upload)){
            $config['max_size'] = $max_size_upload;
        }

        $this->load->library('upload',$config);

        if($this->upload->do_upload('file')){
            $upload_data = $this->upload->data(); 
            echo $upload_data['file_name'];
        }else{
            header('HTTP/1.0 400 Bad Request');
            echo strip_tags($this->upload->display_errors());
        }
    }

    public function modify(){
        $response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|alpha|min_length[6]|max_length[20]|is_unique[users.username]|regex_match[/^[a-z]*$/]', ['is_unique'=>'{field} already exists', 'regex_match' => '{field} must be lowercase']);
        $this->form_validation->set_rules('employee_id', 'NIP', 'required');
        $this->form_validation->set_rules('fullname', 'Nama Pegawai', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('status_pegawai','Status Pegawai', 'required');
        $this->form_validation->set_rules('gender','Gender', 'required');
        $this->form_validation->set_rules('tempat_lahir','Tempat lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir', 'required');
        $this->form_validation->set_rules('tgl_bergabung','Tanggal Bergabung', 'required');
        $this->form_validation->set_rules('departemen','Departemen', 'required');
        $this->form_validation->set_rules('posisi','Posisi', 'required');
        $this->form_validation->set_rules('no_ktp','No KTP', 'required');
        //$this->form_validation->set_rules('no_bpjs_ketenagakerjaan','No BPJS Ketenagakerjaan', 'required');
        $this->form_validation->set_rules('pendidikan_terakhir','Pendidikan Terakhir', 'required');
        $this->form_validation->set_rules('nama_sekolah','Nama Sekolah/Universitas', 'required');
        $this->form_validation->set_rules('jurusan','Jurusan', 'required');
        $this->form_validation->set_rules('tahun_lulus','Tahun Lulus', 'required');
        $this->form_validation->set_rules('no_seri_ijazah','No Seri ijazah', 'required');
        $this->form_validation->set_rules('no_hp','No Handphone', 'required');
        $this->form_validation->set_rules('alamat_ktp','Alamat KTP', 'required');
        $this->form_validation->set_rules('kode_pos','Kode POS', 'required');
        $this->form_validation->set_rules('alamat_domisili','Alamat domisili', 'required');
        $this->form_validation->set_rules('no_darurat','No Telepon Darurat', 'required');
        $this->form_validation->set_rules('nama_keluarga','Nama', 'required');
        $this->form_validation->set_rules('hubungan_keluarga','Hubungan keluarga', 'required');
        $this->form_validation->set_rules('account_no','Account No', 'required');
        $this->form_validation->set_rules('owner','Owner', 'required');
        $this->form_validation->set_rules('bank','Bank', 'required');
        $this->form_validation->set_rules('bank_branch','Cabang', 'required');
        $this->form_validation->set_rules('agama','Agama', 'required');
        $this->form_validation->set_rules('status_keluarga','Status Keluarga', 'required');
        $this->form_validation->set_rules('tenaga_kerja','Tenaga Kerja', 'required');
        $this->form_validation->set_rules('istri_suami','Istri/Suami', 'required');
        $this->form_validation->set_rules('anak','Anak', 'required');
        $this->form_validation->set_rules('copy_ijazah_name','Copy Ijazah', 'required');
        $this->form_validation->set_rules('copy_transkrip_nilai_name','Copy Transkrip Nilai', 'required');
        $this->form_validation->set_rules('copy_ktp_name','Copy KTP', 'required');
        $this->form_validation->set_rules('cv_name','CV', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $primary_key['id'] = $this->input->post('id', true);

            $update_data['username'] = $this->input->post('username', true);
            
            $password = $this->input->post('password', true);
            if($password ){
                $salt = $this->string_generator();
                $password = md5($password.$salt);
                $update_data['password'] = $password;
                $update_data['salt'] = $salt;
            }
            $update_data['employeeid'] = $this->input->post('employee_id', true);
            $update_data['fullname'] = $this->input->post('fullname', true);
            $update_data['email'] = $this->input->post('email', true);
            $update_data['status_pegawai'] =    $this->input->post('status_pegawai', true);
            $update_data['gender'] =    $this->input->post('gender', true);
            $update_data['tempat_lahir'] =  $this->input->post('tempat_lahir', true);
            $update_data['tgl_lahir'] = $this->input->post('tgl_lahir', true);
            // $update_data['usia'] =  $this->input->post('usia', true);
            $update_data['tgl_bergabung'] = $this->input->post('tgl_bergabung', true);
            $update_data['departemen'] =    $this->input->post('departemen', true);
            $update_data['posisi'] =    $this->input->post('posisi', true);
            $update_data['npwp'] =  $this->input->post('npwp', true);
            $update_data['no_ktp'] =    $this->input->post('no_ktp', true);
            $update_data['no_bpjs_kesehatan'] = $this->input->post('no_bpjs_kesehatan', true);
            $update_data['nama_faskes'] =   $this->input->post('nama_faskes', true);
            $update_data['no_bpjs_ketenagakerjaan'] =   $this->input->post('no_bpjs_ketenagakerjaan', true);
            $update_data['pendidikan_terakhir'] =   $this->input->post('pendidikan_terakhir', true);
            $update_data['nama_sekolah'] =  $this->input->post('nama_sekolah', true);
            $update_data['jurusan'] =   $this->input->post('jurusan', true);
            $update_data['ipk'] =   $this->input->post('ipk', true);
            $update_data['tahun_lulus'] =   $this->input->post('tahun_lulus', true);
            $update_data['no_seri_ijazah'] =    $this->input->post('no_seri_ijazah', true);
            $update_data['no_hp'] = $this->input->post('no_hp', true);
            $update_data['no_tlp_rumah'] =  $this->input->post('no_tlp_rumah', true);
            $update_data['alamat_ktp'] =    $this->input->post('alamat_ktp', true);
            $update_data['kode_pos'] =  $this->input->post('kode_pos', true);
            $update_data['alamat_domisili'] =   $this->input->post('alamat_domisili', true);
            $update_data['no_darurat'] =    $this->input->post('no_darurat', true);
            $update_data['nama_keluarga'] = $this->input->post('nama_keluarga', true);
            $update_data['hubungan_keluarga'] = $this->input->post('hubungan_keluarga', true);
            $update_data['keahlian_khusus'] =   $this->input->post('keahlian_khusus', true);
            $update_data['pengalaman_project'] =    $this->input->post('pengalaman_project', true);
            $update_data['riwayat_training'] =  $this->input->post('riwayat_training', true);
            $update_data['account_no'] =    $this->input->post('account_no', true);
            $update_data['owner'] = $this->input->post('owner', true);
            $update_data['bank'] =  $this->input->post('bank', true);
            $update_data['bank_branch'] =   $this->input->post('bank_branch', true);
            $update_data['agama'] = $this->input->post('agama', true);
            $update_data['status_keluarga'] =   $this->input->post('status_keluarga', true);
            $update_data['tenaga_kerja'] =  $this->input->post('tenaga_kerja', true);
            $update_data['istri_suami'] =   $this->input->post('istri_suami', true);
            $update_data['anak'] =  $this->input->post('anak', true);
            
            $upload_path = $this->config->item('user_upload_path');
            
            $ijazah_name = $this->input->post('copy_ijazah_name', true);
            $update_data['copy_ijazah'] =  $upload_path.$ijazah_name;  
           
            $transkrip_name = $this->input->post('copy_transkrip_nilai_name', true);
            $update_data['copy_transkrip_nilai'] =   $upload_path.$transkrip_name;

            $ktp_name = $this->input->post('copy_ktp_name', true);
            $update_data['copy_ktp'] =   $upload_path.$ktp_name;
            
            $npwp_name = $this->input->post('copy_npwp_name', true);
            $update_data['copy_npwp'] =   $upload_path.$npwp_name;
           
            $cv_name = $this->input->post('cv_name', true);
            $update_data['cv'] = $upload_path.$cv_name;

            $user_object = $this->web_app_model->get_single_data("user", $primary_key);
            if (!empty($user_object)) {
                $this->web_app_model->update_data("user", $update_data, $primary_key['id'], "id");

                $response->code = 200;
                $response->status = true;
                $response->message = "Data updated successfully";
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
    }

    private function string_generator($length = 64){
        $string = '';
        // You can define your own characters here.
        $characters = "123456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters)-1)];
        }

        return $string;
    }
    
    public function get_unit(){
        $response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $id = $this->input->post("id",true);
        $where = [
            'category_parent_id' => $id,
            'category_type' => 'STUDYPROGRAM'
        ];
        $unit_object = $this->Web_App_Model->get_data_and_where('sync_category', $where);

        $response->code = 200;
        $response->status = true;
        $response->data = $unit_object;

        echo json_encode($response);
    }

    public function upload(){
        if($this->input->post('import_submit')){
            // Form field validation rules
            $files = $_FILES;
            $this->form_validation->set_data($files);
            $this->form_validation->set_rules('import_csv', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
            if($this->form_validation->run() == true){

                // If file uploaded
                if(is_uploaded_file($_FILES['import_csv']['tmp_name'])){
                    $this->load->library('CSVReader');
                    $csv_data = $this->csvreader->parse_csv($_FILES['import_csv']['tmp_name']);

                    $new_data = [];
                    foreach($csv_data as $key => $row){
                        $new_row = new stdClass();
                        
                        $new_row->id = $row['Id'];
                        $new_row->username = $row['Username'];
                        $new_row->password = $row['Password'];
                        $new_row->employeeid = $row['NIP'];
                        $new_row->fullname = $row['Nama Pegawai'];
                        $new_row->email = $row['Email'];
                        $new_row->status_pegawai = $row['Status Pegawai'];
                        $new_row->gender = $row['Gender'];
                        $new_row->tempat_lahir = $row['Tempat lahir ID'];
                        $new_row->tgl_lahir = $row['Tgl Lahir'];
                        $new_row->tgl_bergabung = $row['Tgl bergabung'];
                        $new_row->departemen = $row['Departemen ID'];
                        $new_row->posisi = $row['Posisi'];
                        $new_row->npwp = $row['NPWP'];
                        $new_row->no_ktp = $row['No KTP'];
                        $new_row->no_bpjs_kesehatan = $row['No BPJS Kesehatan'];
                        $new_row->nama_faskes = $row['Nama Faskes'];
                        $new_row->no_bpjs_ketenagakerjaan = $row['No BPJS Ketenagakerjaan'];
                        $new_row->pendidikan_terakhir = $row['Pendidikan terakhir'];
                        $new_row->nama_sekolah = $row['Nama Sekolah'];
                        $new_row->jurusan = $row['Jurusan'];
                        $new_row->ipk = $row['IPK'];
                        $new_row->tahun_lulus = $row['Tahun Lulus'];
                        $new_row->no_seri_ijazah = $row['No Seri ijazah'];
                        $new_row->no_hp = $row['No Handphone'];
                        $new_row->no_tlp_rumah = $row['No Tlp rumah'];
                        $new_row->alamat_ktp = $row['Alamat KTP'];
                        $new_row->kode_pos = $row['Kode Pos'];
                        $new_row->alamat_domisili = $row['Alamat domisili'];
                        $new_row->no_darurat = $row['No Darurat'];
                        $new_row->nama_keluarga = $row['Nama'];
                        $new_row->hubungan_keluarga = $row['Hubungan keluarga'];
                        $new_row->keahlian_khusus = $row['Keahlian khusus'];
                        $new_row->pengalaman_project = $row['Pengalaman Project'];
                        $new_row->riwayat_training = $row['Riwayat Training'];
                        $new_row->account_no = $row['Account No'];
                        $new_row->owner = $row['Owner'];
                        $new_row->bank = $row['Bank'];
                        $new_row->bank_branch = $row['Bank Branch'];
                        $new_row->agama = $row['Agama'];
                        $new_row->status_keluarga = $row['Status Keluarga'];
                        $new_row->tenaga_kerja = $row['Tenaga Kerja'];
                        $new_row->istri_suami = $row['Istri/Suami'];
                        $new_row->anak = $row['Anak'];
                        $new_row->error = [];

                        $departemen = $this->web_app_model->get_single_data('departemen', ['departemen_id' => $new_row->departemen]);

                        $city = $this->web_app_model->get_single_data('city', ['id' => $new_row->tempat_lahir]);
                        
                        if($new_row->id != ''){
                            if(!is_numeric($new_row->id)){
                                $new_row->error[] = 'id';
                            }else{
                                $user_object = $this->web_app_model->get_single_data('user', ['id' => $new_row->id]);
                                if(empty($user_object)){
                                    $new_row->error[] = 'id';
                                }
                            }
                        }else{
                            $new_row->password == '' ? $new_row->error[] = 'password' : '';
                        }

                        if($new_row->username == '' && $new_row->fullname != '') {
                            $lowercase_fullame = strtolower($new_row->fullname);
                            $stripped_fullname = str_replace(' ', '', $lowercase_fullame);

                            $new_row->username = substr($stripped_fullname,0,8);
                        }
                        if($new_row->employeeid == '') $new_row->error[] = 'employeeid';
                        if($new_row->fullname == '') $new_row->error[] = 'fullname';
                        if($new_row->email == '' || !filter_var($new_row->email, FILTER_VALIDATE_EMAIL)) $new_row->error[] = 'email';
                        if($new_row->status_pegawai == '') $new_row->error[] = 'status_pegawai';
                        if($new_row->gender == '') $new_row->error[] = 'gender';
                        if(empty($city)) $new_row->error[] = 'tempat_lahir';
                        if($new_row->tgl_lahir == '') $new_row->error[] = 'tgl_lahir';
                        // if($new_row->usia == '') $new_row->error[] = 'usia';
                        if($new_row->tgl_bergabung == '') $new_row->error[] = 'tgl_bergabung';
                        if($new_row->departemen == '') $new_row->error[] = 'departemen';
                        if(empty($departemen)) $new_row->error[] = 'departemen';;
                        if($new_row->posisi == '') $new_row->error[] = 'posisi';
                        // if($new_row->npwp == '') $new_row->error[] = 'npwp';
                        if($new_row->no_ktp == '') $new_row->error[] = 'no_ktp';
                        // if($new_row->no_bpjs_kesehatan == '') $new_row->error[] = 'no_bpjs_kesehatan';
                        // if($new_row->nama_faskes == '') $new_row->error[] = 'nama_faskes';
                        // if($new_row->no_bpjs_ketenagakerjaan == '') $new_row->error[] = 'no_bpjs_ketenagakerjaan';
                        if($new_row->pendidikan_terakhir == '') $new_row->error[] = 'pendidikan_terakhir';
                        if($new_row->nama_sekolah == '') $new_row->error[] = 'nama_sekolah';
                        if($new_row->jurusan == '') $new_row->error[] = 'jurusan';
                        // if($new_row->ipk == '') $new_row->error[] = 'ipk';
                        if($new_row->tahun_lulus == '') $new_row->error[] = 'tahun_lulus';
                        if($new_row->no_seri_ijazah == '') $new_row->error[] = 'no_seri_ijazah';
                        if($new_row->no_hp == '') $new_row->error[] = 'no_hp';
                        // if($new_row->no_tlp_rumah == '') $new_row->error[] = 'no_tlp_rumah';
                        if($new_row->alamat_ktp == '') $new_row->error[] = 'alamat_ktp';
                        if($new_row->kode_pos == '') $new_row->error[] = 'kode_pos';
                        if($new_row->alamat_domisili == '') $new_row->error[] = 'alamat_domisili';
                        if($new_row->no_darurat == '') $new_row->error[] = 'no_darurat';
                        if($new_row->nama_keluarga == '') $new_row->error[] = 'nama_keluarga';
                        if($new_row->hubungan_keluarga == '') $new_row->error[] = 'hubungan_keluarga';
                        // if($new_row->keahlian_khusus == '') $new_row->error[] = 'keahlian_khusus';
                        // if($new_row->pengalaman_project == '') $new_row->error[] = 'pengalaman_project';
                        // if($new_row->riwayat_training == '') $new_row->error[] = 'riwayat_training';
                        if($new_row->account_no == '') $new_row->error[] = 'account_no';
                        if($new_row->owner == '') $new_row->error[] = 'owner';
                        if($new_row->bank == '') $new_row->error[] = 'bank';
                        if($new_row->bank_branch == '') $new_row->error[] = 'bank_branch';
                        if($new_row->agama == '') $new_row->error[] = 'agama';
                        if($new_row->status_keluarga == '') $new_row->error[] = 'status_keluarga';
                        if($new_row->tenaga_kerja == '') $new_row->error[] = 'tenaga_kerja';
                        if($new_row->istri_suami == '') $new_row->error[] = 'istri_suami';
                        if($new_row->anak == '') $new_row->error[] = 'anak';

                        $new_data[] = $new_row;
                    }
                }
            }
        }

        $data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $data['table'] = $new_data;
		$this->load->view('users/import.php',$data);
    }

    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['import_csv']['name']) && $_FILES['import_csv']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['import_csv']['name']);
            $file_arr = explode('.', $_FILES['import_csv']['name']);
            $ext = end($file_arr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }

    public function import(){
        $response = get_ajax_response();
        if(!$this->can_update() || !$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $data = $this->input->post('data');
        $data = json_decode($data);

        $i = 0;
        $u = 0;
        $error = [];
        foreach($data as $user){
            $primary_key['id'] = $user->id;
            if($primary_key['id'] != ''){

                $update_data['username'] = $user->username;
                
                $password = $user->password;
                if($password){
                    $salt = $this->string_generator();
                    $password = md5($password.$salt);
                    $update_data['password'] = $password;
                    $update_data['salt'] = $salt;
                }
                
                $update_data['employeeid'] = $user->employeeid;
                $update_data['fullname'] = $user->fullname;
                $update_data['email'] = $user->email;
                $update_data['status_pegawai'] =    $user->status_pegawai;
                $update_data['gender'] =    $user->gender;
                $update_data['tempat_lahir'] =  $user->tempat_lahir;
                $update_data['tgl_lahir'] = $user->tgl_lahir;
                // $update_data['usia'] =  $user->usia;
                $update_data['tgl_bergabung'] = $user->tgl_bergabung;
                $update_data['departemen'] =    $user->departemen;
                $update_data['posisi'] =    $user->posisi;
                $update_data['npwp'] =  $user->npwp;
                $update_data['no_ktp'] =    $user->no_ktp;
                $update_data['no_bpjs_kesehatan'] = $user->no_bpjs_kesehatan;
                $update_data['nama_faskes'] =   $user->nama_faskes;
                $update_data['no_bpjs_ketenagakerjaan'] =   $user->no_bpjs_ketenagakerjaan;
                $update_data['pendidikan_terakhir'] =   $user->pendidikan_terakhir;
                $update_data['nama_sekolah'] =  $user->nama_sekolah;
                $update_data['jurusan'] =   $user->jurusan;
                $update_data['ipk'] =   $user->ipk;
                $update_data['tahun_lulus'] =   $user->tahun_lulus;
                $update_data['no_seri_ijazah'] =    $user->no_seri_ijazah;
                $update_data['no_hp'] = $user->no_hp;
                $update_data['no_tlp_rumah'] =  $user->no_tlp_rumah;
                $update_data['alamat_ktp'] =    $user->alamat_ktp;
                $update_data['kode_pos'] =  $user->kode_pos;
                $update_data['alamat_domisili'] =   $user->alamat_domisili;
                $update_data['no_darurat'] =    $user->no_darurat;
                $update_data['nama_keluarga'] = $user->nama_keluarga;
                $update_data['hubungan_keluarga'] = $user->hubungan_keluarga;
                $update_data['keahlian_khusus'] =   $user->keahlian_khusus;
                $update_data['pengalaman_project'] =    $user->pengalaman_project;
                $update_data['riwayat_training'] =  $user->riwayat_training;
                $update_data['account_no'] =    $user->account_no;
                $update_data['owner'] = $user->owner;
                $update_data['bank'] =  $user->bank;
                $update_data['bank_branch'] =   $user->bank_branch;
                $update_data['agama'] = $user->agama;
                $update_data['status_keluarga'] =   $user->status_keluarga;
                $update_data['tenaga_kerja'] =  $user->tenaga_kerja;
                $update_data['istri_suami'] =   $user->istri_suami;
                $update_data['anak'] =  $user->anak;
            
                $update = $this->web_app_model->update_data("user", $update_data, $primary_key['id'], "id");
                // $this->db->last_query();
                if($update > 0) $u++;
            }else{
              
                $insert_data['username'] = $user->username;
                $insert_data['password'] = $user->password;
                $insert_data['salt'] = $this->string_generator();
                $insert_data['password'] = md5($insert_data['password'].$insert_data['salt']);
                $insert_data['employeeid'] = $user->employeeid;
                $insert_data['fullname'] = $user->fullname;
                $insert_data['email'] = $user->email;
                $insert_data['status_pegawai'] =    $user->status_pegawai;
                $insert_data['gender'] =    $user->gender;
                $insert_data['tempat_lahir'] =  $user->tempat_lahir;
                $insert_data['tgl_lahir'] = $user->tgl_lahir;
                // $insert_data['usia'] =  $user->usia;
                $insert_data['tgl_bergabung'] = $user->tgl_bergabung;
                $insert_data['departemen'] =    $user->departemen;
                $insert_data['posisi'] =    $user->posisi;
                $insert_data['npwp'] =  $user->npwp;
                $insert_data['no_ktp'] =    $user->no_ktp;
                $insert_data['no_bpjs_kesehatan'] = $user->no_bpjs_kesehatan;
                $insert_data['nama_faskes'] =   $user->nama_faskes;
                $insert_data['no_bpjs_ketenagakerjaan'] =   $user->no_bpjs_ketenagakerjaan;
                $insert_data['pendidikan_terakhir'] =   $user->pendidikan_terakhir;
                $insert_data['nama_sekolah'] =  $user->nama_sekolah;
                $insert_data['jurusan'] =   $user->jurusan;
                $insert_data['ipk'] =   $user->ipk;
                $insert_data['tahun_lulus'] =   $user->tahun_lulus;
                $insert_data['no_seri_ijazah'] = $user->no_seri_ijazah;
                $insert_data['no_hp'] = $user->no_hp;
                $insert_data['no_tlp_rumah'] =  $user->no_tlp_rumah;
                $insert_data['alamat_ktp'] = $user->alamat_ktp;
                $insert_data['kode_pos'] =  $user->kode_pos;
                $insert_data['alamat_domisili'] = $user->alamat_domisili;
                $insert_data['no_darurat'] = $user->no_darurat;
                $insert_data['nama_keluarga'] = $user->nama_keluarga;
                $insert_data['hubungan_keluarga'] = $user->hubungan_keluarga;
                $insert_data['keahlian_khusus'] = $user->keahlian_khusus;
                $insert_data['pengalaman_project'] = $user->pengalaman_project;
                $insert_data['riwayat_training'] = $user->riwayat_training;
                $insert_data['account_no'] = $user->account_no;
                $insert_data['owner'] = $user->owner;
                $insert_data['bank'] =  $user->bank;
                $insert_data['bank_branch'] = $user->bank_branch;
                $insert_data['agama'] = $user->agama;
                $insert_data['status_keluarga'] = $user->status_keluarga;
                $insert_data['tenaga_kerja'] = $user->tenaga_kerja;
                $insert_data['istri_suami'] =   $user->istri_suami;
                $insert_data['anak'] =  $user->anak;
                $insert_data['is_manual_insert'] = 1;

                $insert = $this->web_app_model->insert_data("user", $insert_data);
                if($insert > 0) $i++;
            }
        }

        $this->session->set_flashdata('import_error', $i . ' inserted and ' . $u . ' updated');

        $response->code = 200;
        $response->status = true;
        $response->message = $i . ' inserted and ' . $u . ' updated';

        echo json_encode($response);
    }
}
