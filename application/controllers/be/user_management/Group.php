<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Group extends BaseBEController {

	function __construct() 
	{
		parent::__construct();	
			
	}
	
	public function index()
	{		
		//TODO : CHECK PRIVILEGE		
		//TODO : CHECK LOGIN	
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;	
        $data['privilege'] = $this->privilege;  
        $data['group_level'] = $this->config->item("group_level");  
		$this->load->view('group/index.php',$data);
	}

	public function datatable()
	{		

		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }			
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'group';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("group_id","group_name","group_level");
        $search_fields = array("group_id","group_name");
        
        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        //untuk membandingkan string group_level
        function compare_group_level_asc($data_row1, $data_row2){
            return strcmp($data_row1->group_level, $data_row2->group_level);
        }

        function compare_group_level_desc($data_row1, $data_row2){
            return strcmp($data_row2->group_level, $data_row1->group_level);
        }

        $group_level = $this->config->item("group_level");  
        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
               
                $row->group_id = $result->group_id;
                $row->group_name = $result->group_name;
                $row->group_level = $group_level[$result->group_level];
                $data[] = $row;
            }
            //untuk mengurutkan setiap item berdasarkan string group_level
            if($sort["field"] == "group_level"){
                if($sort["sort"] == "asc"){
                    usort($data, 'compare_group_level_asc');
                } elseif($sort["sort"] == "desc") {
                    usort($data, 'compare_group_level_desc');
                }
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }
    
    public function export(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="group";
        $order = "group_id"; 
        $filter = $search;
        $column_search =  array("group_id","group_name","group_level");
        $column_search = array("group_id","group_name");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        $group_level = $this->config->item("group_level");  

        foreach ($result as $rows) {
            $row = new stdClass();
               
            $row->group_id = $rows->group_id;
            $row->group_name = $rows->group_name;
            $row->group_level = $group_level[$rows->group_level];
            $arr[] = array($row->group_id,$row->group_name,$row->group_level);
         }
        $filed = array('Group Id','Group Name','Level');
        //do export
        export_csv($arr,$filed);        
    }

    public function print_group(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="group";
        $order = "group_id"; 
        $filter = $search;
        $column_search =  array("group_id","group_name","group_level");
        $column_search = array("group_id","group_name");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        $group_level = $this->config->item("group_level");  

        foreach ($result as $rows) {
            $row = new stdClass();
               
            $row->group_id = $rows->group_id;
            $row->group_name = $rows->group_name;
            $row->group_level = $group_level[$rows->group_level];
            $arr[] = array('group_id' => $row->group_id,'group_name' => $row->group_name,'group_level' => $row->group_level);
         }
        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("group/print",array("data"=>$arr),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);        
    }

	public function get()
	{		
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Group ID', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$group_id = $this->input->get("id",true);
        	$group_object = null;
        	if(!empty($group_id)){
        		$query_data['group_id'] = $group_id;
        		$group_object = $this->web_app_model->get_single_data("group",$query_data);
        	}

        	$response->code = 200;
        	$response->data = $group_object;

        	if(!empty($group_object)){
	        	$response->status = true;
	        	$response->message = "Get data success";
        	}else{
	        	$response->status = false;
	        	$response->message = "Data not found";
        	}
        }
        echo json_encode($response);
	}

	public function save()
	{		
		$response = get_ajax_response();
		if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$this->form_validation->set_rules('group_name', 'Group Name', 'required');
        $this->form_validation->set_rules('group_level', 'Level', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$group_name = $this->input->post("group_name",true);
            $group_level = $this->input->post("group_level",true);

        	$insert_data['group_name'] = $group_name;
            $insert_data['group_level'] = $group_level;

        	$this->web_app_model->insert_data("group",$insert_data);

        	$response->code = 200;
        	$response->status = true;
        	$response->message = "Data submitted successfully";
        }
        echo json_encode($response);
	}

	public function delete()
	{		
		$response = get_ajax_response();
		if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Id', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$user_id = get_logged_in_id(); // function get_logged_in_id loaded in BaseBEController
        	$group_id = $this->input->get("id",true);

        	if(!empty($group_id)){
        		$id['group_id'] = $group_id;
        		$group_object = $this->web_app_model->get_single_data("group",$id);

        		if(!empty($group_object)){
                    $this->web_app_model->delete_data("group",array("group_id" => $group_id));
                    $this->web_app_model->delete_data("previleges",array("groupid" => $group_id));
                    $this->web_app_model->delete_data("roles",array("groupid" => $group_id));

                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data deleted successfully";
        		}else{
		        	$response->code = 400;
		        	$response->status = false;
		        	$response->message = "Data not found";
        		}
        	}else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Id is required";
        	}

        }
        echo json_encode($response);
	}

	public function edit()
	{		
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$this->form_validation->set_rules('group_name', 'Group Name', 'required');
        $this->form_validation->set_rules('group_level', 'Level', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$group_id = $this->input->post("group_id",true);	
        	$group_name = $this->input->post("group_name",true);
            $group_level = $this->input->post("group_level",true);
        	
       		$primary_key['group_id'] = $group_id;

        	$update_data['group_name'] = $group_name;
            $update_data['group_level'] = $group_level;

			$banner_object = $this->web_app_model->get_single_data("group",$primary_key);

			if(!empty($banner_object)){
	        	$this->web_app_model->update_data("group",$update_data,$primary_key['group_id'],"group_id");

	        	$response->code = 200;
	        	$response->status = true;
	        	$response->message = "Data updated successfully";
			}else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Data not found";
			}
        }
        echo json_encode($response);
	}
}
