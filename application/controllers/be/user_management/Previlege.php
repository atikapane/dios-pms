<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Previlege extends BaseBEController {

	function __construct() 
	{
		parent::__construct();		
	}
	
	public function index()
	{		
		//TODO : CHECK PRIVILEGE		
		//TODO : CHECK LOGIN		
        $this->load->model("Users_Model");
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;	
		$data['submenus'] = $this->Users_Model->get_all_menus();
		$data['group'] = $this->Web_App_Model->get_data_all('group');
        $data['privilege'] = $this->privilege;   
		$this->load->view('previlege/index.php',$data);
	}

	public function datatable()
	{		
		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }   			
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);
        $query = $this->input->post("query",true);

        $table = 'previleges';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        $select_fields =  array("menu.menu_name as menu_name","submenu.submenu_name as submenu_name","group.group_name as group_name",
        	                    "previleges.create as create","previleges.update as update",
        	                    "previleges.delete as delete","previleges.read as read",
        	                    "previleges.previlege_id as previlegeid",
        	                   );
        $search_fields = array("menu_name", "submenu_name","group_name");

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "submenu.submenu_id = previleges.submenu_id";
        $join->table = "submenu";
		array_push($extra_join,$join);
		
		$join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "menu.menu_id = submenu.menuid";
        $join->table = "menu";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "group_id = previleges.groupid";
        $join->table = "group";
        array_push($extra_join,$join);

		$result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);
        
        $list = $result->data;
        $meta = $result->meta;
        $data = array();
      

        if (!empty($list)) {
            foreach ($list as $result) {
                $row = new stdClass();
                $row->previlege_id = $result->previlegeid;
                $row->submenu_name = $result->submenu_name;
                $row->menu_name = $result->menu_name;
                $row->group_name = $result->group_name;
                $row->create = $result->create;
                $row->update = $result->update;
                $row->delete = $result->delete;
          		$row->read = $result->read;
                $data[] = $row;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
	}

	public function export(){   
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="previleges";
        $order = "previlege_id"; 
        $filter = $search;
		$column_search =  array("submenu.submenu_name as submenu_name","group.group_name as group_name",
		"previleges.create as create","previleges.update as update",
		"previleges.delete as delete","previleges.read as read",
		"previleges.previlege_id as previlegeid",
	   );
		$column_search = array("submenu_name","group_name");
		$extra_where = array();
        $extra_where_or = array();
		$extra_join = array();
		
	    $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "submenu.submenu_id = previleges.submenu_id";
        $join->table = "submenu";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "group_id = previleges.groupid";
        $join->table = "group";
        array_push($extra_join,$join);
        //$extra_where =false;
        //$extra_where_or=false; 
        //$extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        foreach ($result as $row) {
			$create="";	$update="";	$delete="";	$read="";
			if($row->create == 1) {$create="Yes";}else{$create="No";}
			if($row->update == 1) {$update="Yes";}else{$update="No";}
			if($row->delete == 1) {$delete="Yes";}else{$delete="No";}
			if($row->read == 1) {$read="Yes";}else{$read="No";}
            $arr[] = array($row->previlege_id,$row->submenu_name,$row->group_name,$create,$update,$delete,$read);
         }
        $filed = array('Id','Submenu Memu','Group Name','Create','Update','Delete','Read');
        //do export
        export_csv($arr,$filed);        
	}
	
	public function print_privilege(){
		//get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        $table="previleges";
        $order = "previlege_id"; 
        $filter = $search;
		$column_search =  array("submenu.submenu_name as submenu_name","group.group_name as group_name",
		"previleges.create as create","previleges.update as update",
		"previleges.delete as delete","previleges.read as read",
		"previleges.previlege_id as previlegeid",
	   );
		$column_search = array("submenu_name","group_name");
		$extra_where = array();
        $extra_where_or = array();
		$extra_join = array();
		
	    $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "submenu.submenu_id = previleges.submenu_id";
        $join->table = "submenu";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "INNER";
        $join->condition = "group_id = previleges.groupid";
        $join->table = "group";
        array_push($extra_join,$join);
        //$extra_where =false;
        //$extra_where_or=false; 
        //$extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;
        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);
        $arr = array();
        foreach ($result as $row) {
			$create="";	$update="";	$delete="";	$read="";
			if($row->create == 1) {$create="Yes";}else{$create="No";}
			if($row->update == 1) {$update="Yes";}else{$update="No";}
			if($row->delete == 1) {$delete="Yes";}else{$delete="No";}
			if($row->read == 1) {$read="Yes";}else{$read="No";}
            $arr[] = array('previlege_id' => $row->previlege_id,'submenu_name' => $row->submenu_name,'group_name' => $row->group_name,'create' => $create,'update' => $update,'delete' => $delete,'read' => $read);
		 }
		//  $filed = array('Id','Submenu Memu','Group Name','Create','Update','Delete','Read');
		 $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("previlege/print",array("data"=>$arr),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);
	}

	public function get()
	{		
		$response = get_ajax_response();
		if(!$this->can_read()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }	
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}

		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Previlege ID', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$previlege_id = $this->input->get("id",true);
        	
        	$previlege_object = null;
        	if(!empty($previlege_id)){
        		$query_data['previlege_id'] = $previlege_id;
        		$previlege_object = $this->web_app_model->get_single_data("previleges",$query_data);
        	}

        	$response->code = 200;
        	$response->data = $previlege_object;

        	if(!empty($previlege_object)){
	        	$response->status = true;
	        	$response->message = "Get data success";
        	}else{
	        	$response->status = false;
	        	$response->message = "Data not found";
        	}
        }
        echo json_encode($response);
	}

	public function save()
	{		
		$response = get_ajax_response();
		if(!$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$this->form_validation->set_rules('submenu_id[]', 'Submenu Name', 'required');
		$this->form_validation->set_rules('group_id', 'Group Name', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$submenuid = $this->input->post("submenu_id",true);
        	$groupid= $this->input->post("group_id",true);
        	$create = $this->input->post("create",true);
        	if($create == "on"){
        		$create = 1;
        	}else{
        		$create = 0;
        	}
  			$update = $this->input->post("update",true);
  			if($update == "on"){
        		$update = 1;
        	}else{
        		$update = 0;
        	}
  			$delete = $this->input->post("delete",true);
  			if($delete == "on"){
        		$delete = 1;
        	}else{
        		$delete = 0;
        	}
  			$read   = $this->input->post("read",true);
  			if($read == "on"){
        		$read = 1;
        	}else{
        		$read = 0;
        	}

        	$upload_path = $this->config->item('previlege_upload_path');

        	$previlege_data['groupid'] = $groupid;
        	$previlege_data['create'] = $create;
        	$previlege_data['update'] = $update;
        	$previlege_data['delete'] = $delete;
        	$previlege_data['read'] = $read;

            for ($i=0; $i < count($submenuid); $i++) { 
                $previlege_data['submenu_id'] = $submenuid[$i];
                $check = $this->web_app_model->get_single_data("previleges",array("groupid"=>$groupid,"submenu_id"=>$submenuid[$i]));
                if (empty($check)){
                    $this->web_app_model->insert_data("previleges",$previlege_data);
                }
                else {
                    $this->web_app_model->update_data("previleges",$previlege_data,$check->previlege_id,"previlege_id");
                }
            }

        	$response->code = 200;
        	$response->status = true;
        	$response->message = "Data submitted successfully";
        }
        echo json_encode($response);
	}

	public function delete()
	{		
		$response = get_ajax_response();
		if(!$this->can_delete()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$get_params = $this->input->get();
		if(empty($get_params)){ //set default param so the form validation triggered
			$get_params = array("id"=>"");
		}
		$this->form_validation->set_data($get_params);
		$this->form_validation->set_rules('id', 'Previlege ID', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$previlege_id = $this->input->get("id",true);
        	if(!empty($previlege_id)){
        		$query_data['previlege_id'] = $previlege_id;
        		$previlege_object = $this->web_app_model->get_single_data("previleges",$query_data);
        		if(!empty($previlege_object)){
	        		$delete_data['previlege_id'] = $previlege_id;
	        		$this->web_app_model->delete_data("previleges",$delete_data);
	        	
		        	$response->code = 200;
		        	$response->status = true;
		        	$response->message = "Data deleted successfully";
        		}else{
		        	$response->code = 400;
		        	$response->status = false;
		        	$response->message = "Data not found";
        		}
        	}else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Previlege_id is required";
        	}

        }
        echo json_encode($response);
	}

	public function edit()
	{		
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
		$this->form_validation->set_rules('previlege_id', 'Previlege ID', 'required');
		$this->form_validation->set_rules('submenu_id', 'Submenu Name', 'required');
		$this->form_validation->set_rules('group_id', 'Group Name', 'required');
		
	
		if ($this->form_validation->run() == FALSE)
        {
        	$response->code = 400;
        	$response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
        	$previlegeid = $this->input->post("previlege_id",true);
        	$submenuid = $this->input->post("submenu_id",true);
        	$groupid= $this->input->post("group_id",true);
        	$create = $this->input->post("create",true);
            if($create == "on"){
                $create = 1;
            }else{
                $create = 0;
            }
  			$update = $this->input->post("update",true);
            if($update == "on"){
                $update = 1;
            }else{
                $update = 0;
            }
  			$delete = $this->input->post("delete",true);
            if($delete == "on"){
                $delete = 1;
            }else{
                $delete = 0;
            }
  			$read   = $this->input->post("read",true);
            if($read == "on"){
                $read = 1;
            }else{
                $read = 0;
            }
  		
        	$upload_path = $this->config->item('previlege_upload_path');

        	$primary_key['previlege_id'] = $previlegeid;

        	$update_data['submenu_id'] = $submenuid;
        	$update_data['groupid'] = $groupid;
        	$update_data['create'] = $create;
        	$update_data['update'] = $update;
        	$update_data['delete'] = $delete;
        	$update_data['read'] = $read; 
            
			$previlege_object = $this->web_app_model->get_single_data("previleges",$primary_key);
            
			if(!empty($previlege_object)){
	        	$this->web_app_model->update_data("previleges",$update_data,$primary_key['previlege_id'],"previlege_id");

	        	$response->code = 200;
	        	$response->status = true;
	        	$response->message = "Data updated successfully";
			}else{
	        	$response->code = 400;
	        	$response->status = false;
	        	$response->message = "Data not found";
			}
        }
        echo json_encode($response);
	}
}
