<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/be/BaseBEController.php");

class Frontend extends BaseBEController {

	function __construct() 
	{
        parent::__construct();				
        $this->load->model('Web_app_model');
        $this->load->library('Sync');
        $this->load->helper('log_user_frontend');
        $this->load->helper('sync_mooc');
        $this->load->helper('sync_igracias');
        $this->load->library('session');
	}
	
	public function index()
	{	
        $import_error = $this->session->flashdata('import_error');
        if($import_error) $data['import_error'] = implode('<br>', $import_error);
        $data['cities'] = $this->Web_App_Model->get_data('city', ['country_id' => 1]);
		$data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;	
		$this->load->view('user_frontend/index.php',$data);
	}

	public function datatable()
	{		

		if(!$this->can_read()){
            $output = get_datatables_response();
            echo json_encode($output);
            return;
        }   			
        $this->load->model('Datatable_model');

        $pagination = $this->input->post("pagination",true);
        $sort = $this->input->post("sort",true);	
        $query = $this->input->post("query",true);

        $table = 'users';
        $extra_where = array();
        $extra_where_or = array();
        $extra_join = array();

        /*Mengurutkan gender sesuai alfabet berdasarkan data pada database :
        Female -> 2, Male -> 1, Other/Prefer Not to Say -> 99 atau NULL
        */
        if(!empty($sort) && is_array($sort) && array_key_exists("field", $sort) && array_key_exists("sort", $sort)){
            if($sort["field"] == "gender" ){
                $sort["field"] = "";
                if($sort["sort"] == "asc"){
                    $order = "DESC";
                } elseif($sort["sort"] == "desc"){
                    $order = "ASC";
                }
                array_push($extra_where_or, "true ORDER BY FIELD (`gender`, 99, 1, 2) $order");
            }
        }


        $select_fields =  array("users.id", "username", "email", "full_name", "country.name as country", "city.name as city", "gender", "dateBirth", 'phone_number');
        $search_fields =  array("username", "email", "full_name", "country.name", "city.name");

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.country_id = country.id";
        $join->table = "country";
        array_push($extra_join,$join);

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.city_id = city.id";
        $join->table = "city";
        array_push($extra_join,$join);

        $result = $this->Datatable_model->get_datatables($table, $pagination, $sort, $query, $search_fields, $extra_where, $extra_where_or, $extra_join, $select_fields);

        $list = $result->data;
        $meta = $result->meta;
        $data = array();

        if (!empty($list)) {
            foreach ($list as $result) {
                $data[] = $result;
            }
        }

        $output = get_datatables_response();
        $output->meta = $meta;
        $output->data = $data;

        echo json_encode($output);
    }
    
    public function get(){
        $response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $user_id = $this->input->get("id", true);
            if (!empty($user_id)) {
                $query_data['id'] = $user_id;
                // $user_object = $this->web_app_model->get_single_data("users", $query_data);
                $user_object = $this->ion_auth->user($query_data['id'])->row();
            }
        }

        $response->code = 200;
        $response->data = $user_object;

        if (!empty($user_object)) {
            $response->status = true;
            $response->message = "Get data success";
        } else {
            $response->status = false;
            $response->message = "Data not found";
        }
        echo json_encode($response);
    }

	public function edit()
	{			
		$response = get_ajax_response();
		if(!$this->can_update()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        
        $this->form_validation->set_rules('id', 'Id', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|alpha');
        $this->form_validation->set_rules('new_password', 'New Password', 'min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_password_check|callback_new_password_check');
        $this->form_validation->set_rules('old_password', 'Old Password', 'min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_old_password_check');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('fullname', 'Fullname', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('date_birth', 'Date Birth', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $primary_key['id'] = $this->input->post("id",true);

            // $user_object = $this->web_app_model->get_single_data("users",$primary_key);
            $user_object = $this->ion_auth->user($primary_key['id'])->row();
            if(!empty($user_object)){
                $update_data['ip_address'] = $this->input->ip_address();
                $update_data['username'] = $this->input->post('username',true);
                $update_data['username'] = strtolower(trim($update_data['username']));
                $password = $this->input->post('new_password',true);
                if($password) $update_data['password'] = $password;
                $update_data['email'] = $this->input->post('email',true);
                $update_data['email'] = trim($update_data['email']);
                $update_data['full_name'] = $this->input->post('fullname',true);
                $update_data['full_name'] = trim($update_data['full_name']);
                $update_data['country_id'] = 1;
                $update_data['city_id'] = $this->input->post('city',true);
                $update_data['city_id'] = trim($update_data['city_id']);
                $update_data['gender'] = $this->input->post('gender',true);
                $update_data['gender'] = trim($update_data['gender']);
                $update_data['dateBirth'] = $this->input->post('date_birth',true);
                $update_data['dateBirth'] = trim($update_data['dateBirth']);
                $update_data['phone_number'] = $this->input->post('phone_number',true);
                $update_data['phone_number'] = trim($update_data['phone_number']);

                $input = $update_data;
                unset($input['password']);
                $log = format_log_data($input);
                insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());

                $email_object = $this->web_app_model->get_single_data('users', ['email' => $update_data['email']]);
                if(empty($email_object) || $email_object->id == $primary_key['id']){
                    // check user in mooc
                    $sync_response = check_user_mooc($user_object->username);
                    //$sync_response['users'][0]['id'] = 31847;
                    
                    $log = format_log_data(['email' => $user_object->username], 'Check User In MOOC');
                    insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());

                    if(!empty($sync_response['users'])){
                        $update_data['mooc_id'] = $sync_response['users'][0]['id'];
                        // update user mooc
                        $sync_data = new stdClass();
                        $sync_data->id = $update_data['mooc_id'];
                        $sync_data->username = $update_data['username'];
                        $sync_data->fullname = $update_data['full_name'];
                        $sync_data->email = $update_data['email'];
                        $sync_data->password = $password;
                        $sync_response = update_user_mooc($sync_data);
                        //var_dump($sync_response);
                        //die();

                        $input = $sync_data;
                        unset($input->password);
                        $log = format_log_data($input, 'Update User In MOOC');
                        insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());

                        if(is_null($sync_response)){
                            $log = format_log_data($input, 'User Updated In MOOC');
                            insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());
                        }else{
                            $err_message = isset($sync_response['message']) ? strip_tags($sync_response['message']) : 'Failed To Update User In MOOC';
                            $log = format_log_data($input, $err_message);
                            insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());

                            $response->code = 400;
                            $response->status = false;
                            $response->message = $err_message;

                            echo json_encode($response);
                            return false;
                        }
                    }elseif(isset($sync_response['exception']) && !empty($sync_response['exception'])){
                        $err_message = isset($sync_response['debuginfo']) ? $sync_response['debuginfo'] : ((isset($sync_response['message'])) ? $sync_response['message'] : ' Failed To Enroll User');
                        
                        $log = format_log_data(['email' => $user_object->username], 'User '. $user_object->username.' - '. $err_message);
                        insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());
                        
                        $response->code = 400;
                        $response->status = false;
                        $response->message = $err_message;

                        echo json_encode($response);
                        return;
                    }

                    // check user igracias
                    $sync_response = get_user_igracias($user_object->username);

                    $log = format_log_data(['username' => $user_object->username], 'Check User In Igracias');
                    insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());

                    $city = $this->web_app_model->get_single_data('city', ['id'=> $update_data['city_id']]);
                    if($sync_response['status']){
                        $log = format_log_data(['username' => $user_object->username], 'User Found In Igracias');
                        insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());
                        // update user igracias
                        $sync_data = new stdClass();
                        $sync_data->username = $update_data['username'];
                        $sync_data->phone_number = $update_data['phone_number'];
                        $sync_data->city = $city->name;
                        $sync_data->email = $update_data['email'];
                        $sync_data->fullname = $update_data['full_name'];
                        $sync_data->birth_date = $update_data['dateBirth'];
                        $sync_data->gender = $update_data['gender'];
                        $sync_response = update_user_igracias($sync_data);

                        $input = $sync_data;
                        unset($input->password);
                        $log = format_log_data($input, 'Update User In Igracias');
                        insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());

                        if($sync_response['status']){
                            $log = format_log_data($input, "User Updated In Igracias");
                            insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());

                            // udpate password igracias
                            $old_password = $this->input->post('old_password', true);
                            if($password && $old_password){
                                $sync_data = new stdClass();
                                $sync_data->username = $user_object->username;
                                $sync_data->old_password = $old_password;
                                $sync_data->new_password = $password;
                                $sync_response = update_password_igracias($sync_data);
                                
                                $log = format_log_data(['username' => $sync_data->username], 'Update Password User In Igracias');
                                insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());

                                if($sync_response['status']){
                                    $log = format_log_data(['username' => $sync_data->username], "User Password Updated In Igracias");
                                    insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());
                                }else{
                                    $log = format_log_data(['username' => $sync_data->username], $sync_response['message']);
                                    insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());

                                    $response->code = 400;
                                    $response->status = false;
                                    $response->message = $sync_response['message'];

                                    echo json_encode($response);
                                    return false;
                                }
                            }
                        }else{
                            $log = format_log_data($input, $sync_response['message']);
                            insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());
    
                            $response->code = 400;
                            $response->status = false;
                            $response->message = $sync_response['message'];

                            echo json_encode($response);
                            return false;
                        }
                    }else{
                        $log = format_log_data(['username' => $user_object->username], 'User Not Found In Igracias');
                        insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());
                        // create user igracias
                        $sync_data = new stdClass();
                        $sync_data->username = $update_data['username'];
                        $sync_data->password = $password ?: 'Qwerty1@';
                        $sync_data->phone_number = $update_data['phone_number'];
                        $sync_data->city = $city->name;
                        $sync_data->email = $update_data['email'];
                        $sync_data->fullname = $update_data['full_name'];
                        $sync_data->birth_date = $update_data['dateBirth'];
                        $sync_data->gender = $update_data['gender'];
                        $sync_response = create_user_igracias($sync_data);

                        $input = $sync_data;
                        unset($input->password);
                        $log = format_log_data($input, 'Create User In Igracias');
                        insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());

                        if($sync_response['status']){
                            $log = format_log_data($input, "User Created In Igracias");
                            insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());
                            $update_data['personid'] = $sync_response['personid'];
                        }else{
                            $log = format_log_data($input, $sync_response['message']);
                            insert_log('USER_FRONTEND_CREATE_FAILED', $log, get_logged_in_id());

                            $response->code = 400;
                            $response->status = false;
                            $response->message = $sync_response['message'];

                            echo json_encode($response);
                            return false;
                        }
                    }

                    // update user database
                    if($this->ion_auth->update($primary_key['id'], $update_data)) {
                        unset($update_data['password']);
                        $log = format_log_data($update_data, "User Updated In Database");
                        insert_log('USER_FRONTEND_UPDATE_SUCCESS', $log, get_logged_in_id());

                        $response->message = "Data updated successfully";
                        $response->code = 200;
                        $response->status = true;
                    }else{
                        unset($update_data['password']);
                        $log = format_log_data($update_data, "Failed Register User To Database");
                        insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());

                        $response->message = "Failed to update data"; 
                        $response->code = 400;
                        $response->status = false;
                    }
                }else{
                    $log = format_log_data(['email' => $email_object->email], 'Email has been used');
                    insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());

                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Email has been used";
                }
            }else{
                $log = format_log_data($primary_key, 'Id not found');
                insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());

                $response->code = 400;
                $response->status = false;
                $response->message = "Data not found";
            }
        }
        echo json_encode($response);
   		
    }

    public function save(){
        $response = get_ajax_response();
        if (!$this->can_create()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|max_length[12]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/]|callback_password_check');
        $this->form_validation->set_rules('username', 'Username', 'required|alpha|min_length[6]|max_length[20]|is_unique[users.username]|regex_match[/^[a-z]*$/]', ['is_unique'=>'{field} already exists', 'regex_match' => '{field} must be lowercase']);
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]|valid_email', ['is_unique'=>'{field} already exists']);
        $this->form_validation->set_rules('fullname', 'Fullname', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('date_birth', 'Date Birth', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $username = $this->input->post('username', true);
            $username = strtolower(trim($username));
            $password = $this->input->post('password', true);
            $email = $this->input->post('email', true);
            $email = trim($email);
            $fullname = $this->input->post('fullname', true);
            $fullname = trim($fullname);
            $city_id = $this->input->post('city', true);
            $city_id = trim($city_id);
            $gender = $this->input->post('gender', true);
            $gender = trim($gender);
            $dateBirth = $this->input->post('date_birth', true);
            $dateBirth = trim($dateBirth);
            $phone_number = $this->input->post('phone_number', true);
            $phone_number = trim($phone_number);

            if (!$this->ion_auth->email_check($email))
            {
                $input = [
                    'ip_address' => $this->input->ip_address(),
                    'username' => $username,
                    'email' => $email,
                    'full_name' => $fullname,
                    'country_id' => 1,
                    'city_id' => $city_id,
                    'gender' => $gender,
                    'dateBirth' => $dateBirth,
                    'phone_number' => $phone_number,
                ];
                $log = format_log_data($input);
                insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());

                // check user in mooc
                $sync_response = check_user_mooc($username);

                $log = format_log_data(['email' => $username], 'Check User In MOOC');
                insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());

                // check there is user or not
                if(!empty($sync_response['users'])){
                    $mooc_id = $sync_response['users'][0]['id'];

                    // update user mooc
                    $sync_data = new stdClass();
                    $sync_data->id = $mooc_id;
                    $sync_data->username = $username;
                    $sync_data->fullname = $fullname;
                    $sync_data->email = $email;
                    $sync_data->password = $password;
                    $sync_response = update_user_mooc($sync_data);
                    //var_dump($sync_response);
                    //die();

                    $input = $sync_data;
                    unset($input->password);
                    $log = format_log_data($input, 'Update User In MOOC');
                    insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());

                    if(is_null($sync_response)){
                        $log = format_log_data($input, 'User Updated In MOOC');
                        insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());
                    }else{
                        $err_message = isset($sync_response['message']) ? strip_tags($sync_response['message']) : 'Failed To Update User In MOOC';
                        $log = format_log_data($input, $err_message);
                        insert_log('USER_FRONTEND_CREATE_FAILED', $log, get_logged_in_id());

                        $response->code = 400;
                        $response->status = false;
                        $response->message = $err_message;

                        echo json_encode($response);
                        return false;
                    }
                }elseif(isset($sync_response['exception']) && !empty($sync_response['exception'])){
                    $err_message = isset($sync_response['debuginfo']) ? $sync_response['debuginfo'] : ((isset($sync_response['message'])) ? $sync_response['message'] : ' Failed To Enroll User');
                        
                    $log = format_log_data(['email' => $username], 'User '. $username.' - '. $err_message);
                    insert_log('USER_FRONTEND_CREATE_FAILED', $log, get_logged_in_id());
                    
                    $response->code = 400;
                    $response->status = false;
                    $response->message = $err_message;

                    echo json_encode($response);
                    return;
                }
                $additional_data = [
                    'ip_address' => $this->input->ip_address(),
                    'mooc_id' => isset($mooc_id) ? $mooc_id : 0,
                    'full_name' => $fullname,
                    'country_id' => 1,
                    'city_id' => $city_id,
                    'gender' => $gender,
                    'dateBirth' => $dateBirth,
                    'phone_number' => $phone_number,
                ];
                $group = ['2'];

                // check user igracias
                $sync_response = get_user_igracias($username);

                $log = format_log_data(['username' => $username], 'Check User In Igracias');
                insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());

                $city = $this->web_app_model->get_single_data('city', ['id'=> $additional_data['city_id']]);
                if($sync_response['status']){
                    $log = format_log_data(['username' => $username], 'User Found In Igracias');
                    insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());
                    // update user igracias
                    $sync_data = new stdClass();
                    $sync_data->username = $username;
                    $sync_data->phone_number = $additional_data['phone_number'];
                    $sync_data->city = $city->name;
                    $sync_data->email = $email;
                    $sync_data->fullname = $fullname;
                    $sync_data->birth_date = $additional_data['dateBirth'];
                    $sync_data->gender = $additional_data['gender'];
                    $sync_response = update_user_igracias($sync_data);

                    $input = $sync_data;
                    unset($input->password);
                    $log = format_log_data($input, 'Update User In Igracias');
                    insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());

                    if($sync_response['status']){
                        $log = format_log_data($input, "User Updated In Igracias");
                        insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());
                    }else{
                        $log = format_log_data($input, $sync_response['message']);
                        insert_log('USER_FRONTEND_CREATE_FAILED', $log, get_logged_in_id());

                        $response->code = 400;
                        $response->status = false;
                        $response->message = $sync_response['message'];

                        echo json_encode($response);
                        return false;
                    }
                }else{
                    // create user igracias
                    $sync_data = new stdClass();
                    $sync_data->username = $username;
                    $sync_data->password = $password;
                    $sync_data->phone_number = $additional_data['phone_number'];
                    $sync_data->city = $city->name;
                    $sync_data->email = $email;
                    $sync_data->fullname = $fullname;
                    $sync_data->birth_date = $additional_data['dateBirth'];
                    $sync_data->gender = $additional_data['gender'];
                    $sync_response = create_user_igracias($sync_data);

                    $input = $sync_data;
                    unset($input->password);
                    $log = format_log_data($input, 'Create User In Igracias');
                    insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());

                    if($sync_response['status']){
                        $log = format_log_data($input, "User Created In Igracias");
                        insert_log('USER_FRONTEND_CREATE', $log, get_logged_in_id());
                        $additional_data['personid'] = $sync_response['personid'];
                    }else{
                        $log = format_log_data($input, $sync_response['message']);
                        insert_log('USER_FRONTEND_CREATE_FAILED', $log, get_logged_in_id());
                        $response->code = 400;
                        $response->status = false;
                        $response->message = $sync_response['message'];

                        echo json_encode($response);
                        return false;
                    }
                }
                if ($this->ion_auth->register($username, $password, $email, $additional_data, $group)) {
                    $log = format_log_data($input);
                    insert_log('USER_FRONTEND_CREATE_SUCCESS', $log, get_logged_in_id());
            
                    $response->code = 200;
                    $response->status = true;
                    $response->message = "Data submitted successfully";
                }else{
                    $log = format_log_data($input, "Failed Register To Database");
                    insert_log('USER_FRONTEND_CREATE_FAILED', $log, get_logged_in_id());
                    $response->message = "Failed to submit data";
                    $response->status = false;
                }
            }else{
                $log = format_log_data(['email' => $email], "Account already exist");
                insert_log('USER_FRONTEND_CREATE_FAILED', $log);
                $response->message = "Account already exist";
                $response->status = false;
                $response->code = 400;
            }
        }
        echo json_encode($response);
    }

    public function delete()
    {
        $response = get_ajax_response();
        if (!$this->can_delete()) {
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }
        $get_params = $this->input->get();
        if (empty($get_params)) { //set default param so the form validation triggered
            $get_params = array("id" => "");
        }
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('id', 'Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        } else {
            $user_id = $this->input->get("id", true);
            $log = format_log_data(['id' => $user_id]);
            insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());
            if (!empty($user_id)) {
                $query_data['id'] = $user_id;
                // $user_object = $this->web_app_model->get_single_data("users", $query_data);
                $user_object = $this->ion_auth->user($query_data['id'])->row();

                $log = format_log_data(['id' => $user_id], 'Check user with id ' . $user_id);
                insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());
                if (!empty($user_object)) {
                    $log = format_log_data(['id' => $user_id], 'User found');
                    insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());

                    // check user in mooc
                    $sync_response = check_user_mooc($user_object->username);

                    $log = format_log_data(['email' => $user_object->username], 'Check User In MOOC');
                    insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());

                    // check there is user or not
                    if(!empty($sync_response['users'])){
                        $log = format_log_data(['mooc_id' => $user_object->mooc_id], 'Delete user from mooc');
                        insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());

                        // delete user mooc
                        $sync_response = delete_user_mooc($user_object->mooc_id);

                        if(!is_null($sync_response)){
                            $err_message = isset($sync_response['message']) ? $sync_response['message'] : 'Failed to communicate with mooc';
                            $log = format_log_data(['mooc_id' => $user_object->mooc_id], $err_message);
                            insert_log('USER_FRONTEND_DELETE_FAILED', $log, get_logged_in_id());
    
                            $response->code = 400;
                            $response->status = false;
                            $response->message = $err_message;

                            echo json_encode($response);
                            return false;
                        }

                        $log = format_log_data(['mooc_id' => $user_object->mooc_id], 'Success delete user from mooc');
                        insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());
                    }elseif(isset($sync_response['exception']) && !empty($sync_response['exception'])){
                        $err_message = isset($sync_response['debuginfo']) ? $sync_response['debuginfo'] : ((isset($sync_response['message'])) ? $sync_response['message'] : ' Failed To Enroll User');
                            
                        $log = format_log_data(['email' => $user_object->username], 'User '. $user_object->username.' - '. $err_message);
                        insert_log('USER_FRONTEND_DELETE_FAILED', $log, get_logged_in_id());
                        
                        $response->code = 400;
                        $response->status = false;
                        $response->message = $err_message;
    
                        echo json_encode($response);
                        return;
                    }

                    // check uuser igracias
                    $sync_response = get_user_igracias($user_object->username);

                    $log = format_log_data(['username' => $user_object->username], 'Check User In Igracias');
                    insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());

                    if($sync_response['status']){
                        $log = format_log_data(['username' => $user_object->username], 'User Found In Igracias');
                        insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());
                        // delete user igracias
                        $sync_response = delete_user_igracias($user_object->username);

                        $log = format_log_data(['username' => $user_object->username], 'Delete user from igracias');
                        insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());

                        if($sync_response['status']){
                            $log = format_log_data(['username' => $user_object->username], 'Success delete user from igracias');
                            insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());
                        }else{
                            $log = format_log_data(['username' => $user_object->username], $sync_response['message']);
                            insert_log('USER_FRONTEND_DELETE_FAILED', $log, get_logged_in_id());

                            $response->code = 400;
                            $response->status = false;
                            $response->message = $sync_response['message'];

                            echo json_encode($response);
                            return false;
                        }
                    }

                    $delete_data['id'] = $user_id;

                    $log = format_log_data(['id' => $user_id], 'Delete user from database');
                    insert_log('USER_FRONTEND_DELETE', $log, get_logged_in_id());
                    if($this->ion_auth->delete_user($delete_data['id'])){
                        $log = format_log_data(['id' => $user_id], 'Success delete user from database');
                        insert_log('USER_FRONTEND_DELETE_SUCCESS', $log, get_logged_in_id());

                        $response->message = "Data deleted successfully";
                    }else{
                        $log = format_log_data(['id' => $user_id], 'Failed delete user from database');
                        insert_log('USER_FRONTEND_DELETE_FAILED', $log, get_logged_in_id());

                        $response->message = "Failed to delete data";
                    }

                    $response->code = 200;
                    $response->status = true;
                } else {
                    $log = format_log_data(['id' => $user_id], 'User not found');
                    insert_log('USER_FRONTEND_DELETE_FAILED', $log, get_logged_in_id());
                    
                    $response->code = 400;
                    $response->status = false;
                    $response->message = "Data not found";
                }
            } else {
                $response->code = 400;
                $response->status = false;
                $response->message = "Id is required";
            }

        }
        echo json_encode($response);
    }
    
    public function upload(){
        if($this->input->post('import_submit')){
            // Form field validation rules
            $files = $_FILES;
            $this->form_validation->set_data($files);
            $this->form_validation->set_rules('import_csv', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
            if($this->form_validation->run() == true){

                // If file uploaded
                if(is_uploaded_file($_FILES['import_csv']['tmp_name'])){
                    $this->load->library('CSVReader');
                    $csv_data = $this->csvreader->parse_csv($_FILES['import_csv']['tmp_name']);

                    $new_data = [];
                    foreach($csv_data as $key => $row){
                        $new_row = new stdClass();
                        $new_row->id = $row['Id'];
                        $new_row->username = $row['Username'];
                        $new_row->password = $row['Password'];
                        $new_row->old_password = $row['Old Password'];
                        $new_row->email = $row['Email'];
                        $new_row->full_name = $row['Fullname'];
                        $new_row->gender = $row['Gender'];
                        $new_row->date_birth = $row['Date Birth'];
                        $new_row->phone_number = $row['Phone Number'];
                        $new_row->error = [];

                        $user_object = $this->Web_app_model->get_single_data('users', ['id' => $new_row->id]);
                        $email_object = $this->Web_app_model->get_single_data('users', ['email' => $new_row->email]);
                        $username_object = $this->Web_app_model->get_single_data('users', ['username' => $new_row->username]);

                        if($new_row->id != ''){
                            if(!is_numeric($new_row->id)){
                                $new_row->error[] = 'id';
                            }else{
                                // $user_object = $this->Web_app_model->get_single_data('users', ['id' => $new_row->id]);
                                $user_object = $this->ion_auth->user($new_row->id)->row();
                                if(!empty($user_object)){
                                    if($user_object->username != $new_row->username) $new_row->error[] = 'username';
                                }
                            }
                        }

                        if($new_row->username == '' || !ctype_alpha($new_row->username) || (!empty($username_object) && !($new_row->id == $username_object->id)) || !preg_match('/^[a-z]*$/', $new_row->username)){
                             $new_row->error[] = 'username';
                        }else{
                            $x = 0;
                            for ($i=0; $i < count($new_data); $i++) { 
                                if($new_data[$i]->username == $new_row->username && !in_array('username', $new_data[$i]->error)){
                                    if($x == 0){
                                        $new_row->error[] = 'username';
                                        $x++;
                                    }
                                }
                            }
                        }

                        if($new_row->password == ''){
                            if($new_row->id == ''){
                                $new_row->error[] = 'password';
                            }else if($new_row->old_password != ''){
                                $new_row->error[] = 'password';
                            }
                        }else{
                            if(!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/', $new_row->password) || (strlen($new_row->password) > 12 || strlen($new_row->password) < 8)) $new_row->error[] = 'password';
                        }

                        if (!empty($user_object)) {
                            if($new_row->old_password == ''){
                                if($new_row->password != ''){
                                    if($new_row->id != '') $new_row->error[] = 'old_password';
                                }
                            }else{
                                if(!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]*$/', $new_row->old_password) || (strlen($new_row->old_password) > 12 || strlen($new_row->old_password) < 8)) $new_row->error[] = 'old_password';
                            }
                        }

                        if(!filter_var($new_row->email, FILTER_VALIDATE_EMAIL) || $new_row->email == '' || (!empty($email_object) && !($new_row->id == $email_object->id))){
                            $new_row->error[] = 'email';
                        }else{
                            $x = 0;
                            for ($i=0; $i < count($new_data); $i++) { 
                                if($new_data[$i]->email == $new_row->email && !in_array('email', $new_data[$i]->error)){
                                    if($x == 0){
                                        $new_row->error[] = 'email';
                                        $x++;
                                    }
                                }
                            }
                        }

                        if($new_row->full_name == '') $new_row->error[] = 'full_name';

                        if($new_row->gender == '')
                            $new_row->error[] = 'gender';

                        if($new_row->date_birth == '' || !date_create($new_row->date_birth)) $new_row->error[] = 'date_birth';
                        if($new_row->phone_number == '' || !is_numeric($new_row->phone_number)) $new_row->error[] = 'phone_number';

                        $new_data[] = $new_row;
                    }
                }
            }
        }

        $data['breadcrumb'] = $this->breadcrumb;	
		$data['controller_full_path'] = $this->full_path;
        $data['privilege'] = $this->privilege;
        $data['table'] = $new_data;
		$this->load->view('user_frontend/import.php',$data);
    }

    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['import_csv']['name']) && $_FILES['import_csv']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['import_csv']['name']);
            $file_arr = explode('.', $_FILES['import_csv']['name']);
            $ext = end($file_arr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }

    public function password_check($str){
        $username = $this->input->post('username', true);
        if(strpos($str, $username) === false) return true;
        $this->form_validation->set_message('password_check', '{field} cannot contain username');
        return false;
    }

    public function new_password_check($str){
        $new_password = $this->input->post('new_password', true);
        $old_password = $this->input->post('old_password', true);
        if($new_password == '' && $old_password != ''){
            $this->form_validation->set_message('new_password_check', '{field} cannot empty when update password');
            return false;
        }
        return true;
    }

    public function old_password_check($str){
        $new_password = $this->input->post('new_password', true);
        $old_password = $this->input->post('old_password', true);
        if($new_password != '' && $old_password == ''){
            $this->form_validation->set_message('old_password_check', '{field} cannot empty when update password');
            return false;
        }
        return true;
    }

    public function import(){
        $response = get_ajax_response();
        if(!$this->can_update() || !$this->can_create()){
            $response->code = 403;
            $response->status = false;
            $response->message = "You are not authorized to do this action";
            echo json_encode($response);
            return;
        }

        $data = $this->input->post('data');
        $data = json_decode($data);

        $i = 0;
        $u = 0;
        $error = [];
        if (!empty($data)){
            foreach($data as $user){
                $primary_key['id'] = $user->id;
                if($user->id) 
                    $user_object = $this->web_app_model->get_single_data('users', ['id' => $user->id]);
                $date = date_create($user->date_birth);
                $user->gender = $user->gender == 'Male' || $user->gender == 'male' ? 1 : ($user->gender == 'Female' || $user->gender == 'female' ? 2 : 99);
                $user->date_birth = date_format($date, 'Y-m-d');
    
                $input = [
                    'username' => $user->username,
                    'email' => $user->email,
                    'full_name' => $user->full_name,
                    'country_id' => 1,
                    'city_id' => 3273,
                    'gender' => $user->gender,
                    'dateBirth' => $user->date_birth,
                    'phone_number' => $user->phone_number,
                ];
                
                $log = format_log_data($input);
                insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id()); 
                

                if($primary_key['id'] == '' || empty($user_object)){
                    // check user in mooc
                    $sync_response = check_user_mooc($user->username);
     
                    $log = format_log_data(['email' => $user->username], 'Check User In MOOC');
                    insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
    
                    // check there is user or not
                    if(!empty($sync_response['users'])){
                        $mooc_id = $sync_response['users'][0]['id'];
    
                        // update user mooc
                        $sync_data = new stdClass();
                        $sync_data->id = $mooc_id;
                        $sync_data->username = $user->username;
                        $sync_data->fullname = $user->full_name;
                        $sync_data->email = $user->email;
                        $sync_data->password = $user->password;
                        $sync_response = update_user_mooc($sync_data);
                        //var_dump($sync_response);
                        //die();
    
                        $input = $sync_data;
                        unset($input->password);

                        $log = format_log_data($input, 'Update User In MOOC');
                        insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                        
                        if(is_null($sync_response)){
                            
                            $log = format_log_data($input, 'User Updated In MOOC');
                            insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                    
                        }else{
                            $err_message = isset($sync_response['message']) ? strip_tags($sync_response['message']) : 'Failed To Update User In MOOC';
                            
                            $log = format_log_data($input, $err_message);
                            insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                            
                            array_push($error, $user->username . ' Error MOOC - ' . $err_message);
                            continue;
                        }
                    }
                    $additional_data = [
                        'mooc_id' => isset($mooc_id) ? $mooc_id : 0,
                        'full_name' => $user->full_name,
                        'country_id' => 1,
                        'city_id' => 3273,
                        'gender' => $user->gender,
                        'dateBirth' => $user->date_birth,
                        'phone_number' => $user->phone_number,
                    ];
                    $group = ['2'];
    
                    // check user igracias
                    $sync_response = get_user_igracias($user->username);
    
                    
                    $log = format_log_data(['username' => $user->username], 'Check User In Igracias');
                    insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                    
                    $city = $this->web_app_model->get_single_data('city', ['id'=> $additional_data['city_id']]);
                    
                    if($sync_response['status']){
                        $log = format_log_data(['username' => $user->username], 'User Found In Igracias');
                        insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                        
                        // update user igracias
                        $sync_data = new stdClass();
                        $sync_data->username = $user->username;
                        $sync_data->phone_number = $user->phone_number;
                        $sync_data->city = $city->name;
                        $sync_data->email = $user->email;
                        $sync_data->fullname = $user->full_name;
                        $sync_data->birth_date = $user->date_birth;
                        $sync_data->gender = $user->gender;
                        $sync_response = update_user_igracias($sync_data);
    
                        $input = $sync_data;
                        unset($input->password);
                        
                        $log = format_log_data($input, 'Update User In Igracias');
                        insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                        
                        if($sync_response['status']){
                            $log = format_log_data($input, "User Updated In Igracias");
                            insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                        }else{
                            $log = format_log_data($input, $sync_response['message']);
                            insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                            array_push($error, $user->username . ' Error Igracias - ' . $sync_response['message']);
                            continue;
                        }
                    }else{
                        // create user igracias
                        $sync_data = new stdClass();
                        $sync_data->username = $user->username;
                        $sync_data->password = $user->password;
                        $sync_data->phone_number = $user->phone_number;
                        $sync_data->city = $city->name;
                        $sync_data->email = $user->email;
                        $sync_data->fullname = $user->full_name;
                        $sync_data->birth_date = $user->date_birth;
                        $sync_data->gender = $user->gender;
                        $sync_response = create_user_igracias($sync_data);
    
                        $input = $sync_data;
                        unset($input->password);
                        $log = format_log_data($input, 'Create User In Igracias');
                        insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                        if($sync_response['status']){
                            $log = format_log_data($input, "User Created In Igracias");
                            insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                            $additional_data['personid'] = $sync_response['personid'];
                        }else{
                            $log = format_log_data($input, $sync_response['message']);
                            insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                            array_push($error, $user->username . ' Error Igracias - ' . $sync_response['message']);
                            continue;
                        }
                    }
                    
                    if ($this->ion_auth->register($user->username, $user->password, $user->email, $additional_data, $group)) {
                        
                        $log = format_log_data($input);
                        insert_log('USER_FRONTEND_IMPORT_SUCCESS', $log, get_logged_in_id());
                        
                        $i++;
                    }else{
                        
                        $log = format_log_data($input, "Failed Register To Database");
                        insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                        
                        array_push($error, $user->username . ' Error Database - Failed Register To Database');
                        continue;
                    }
                }else{
                    $update_data['username'] = $user->username;
                    $password = $user->password;
                    if($password) $update_data['password'] = $password;
                    $update_data['email'] = $user->email;
                    $update_data['full_name'] = $user->full_name;
                    $update_data['country_id'] = 1;
                    $update_data['city_id'] = 3273;
                    $update_data['gender'] = $user->gender;
                    $update_data['dateBirth'] = $user->date_birth;
                    $update_data['phone_number'] = $this->input->post('phone_number',true);
    
                    $input = $update_data;
                    unset($input['password']);
                    
                    $log = format_log_data($input);
                    insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                    
                    $email_object = $this->web_app_model->get_single_data('users', ['email' => $update_data['email']]);
                    if(empty($email_object) || $email_object->id == $primary_key['id']){
                        // check user in mooc
                        $sync_response = check_user_mooc($user->username);
                        //$sync_response['users'][0]['id'] = 31847;
                        
                        
                        $log = format_log_data(['email' => $user->username], 'Check User In MOOC');
                        insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                        
                        if(!empty($sync_response['users'])){
                            $update_data['mooc_id'] = $sync_response['users'][0]['id'];
                            // update user mooc
                            $sync_data = new stdClass();
                            $sync_data->id = $update_data['mooc_id'];
                            $sync_data->username = $update_data['username'];
                            $sync_data->fullname = $update_data['full_name'];
                            $sync_data->email = $update_data['email'];
                            $sync_data->password = $password;
                            $sync_response = update_user_mooc($sync_data);
                            //var_dump($sync_response);
                            //die();
    
                            $input = $sync_data;
                            unset($input->password);
                            
                            $log = format_log_data($input, 'Update User In MOOC');
                            insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                            
                            if(is_null($sync_response)){
                                
                                $log = format_log_data($input, 'User Updated In MOOC');
                                insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                                
                            }else{
                                $err_message = isset($sync_response['message']) ? strip_tags($sync_response['message']) : 'Failed To Update User In MOOC';
                                
                                $log = format_log_data($input, $err_message);
                                insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                                
                                array_push($error, $user->username . ' Error MOOC - ' . $err_message);
                                continue;
                            }
                        }
    
                        // check user igracias
                        $sync_response = get_user_igracias($user->username);
                        
                        $log = format_log_data(['username' => $user->username], 'Check User In Igracias');
                        insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                        
                        $city = $this->web_app_model->get_single_data('city', ['id'=> $update_data['city_id']]);
                        
                        if($sync_response['status']){
                            $log = format_log_data(['username' => $user->username], 'User Found In Igracias');
                            insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                            // update user igracias
                            $sync_data = new stdClass();
                            $sync_data->username = $update_data['username'];
                            $sync_data->phone_number = $update_data['phone_number'];
                            $sync_data->city = $city->name;
                            $sync_data->email = $update_data['email'];
                            $sync_data->fullname = $update_data['full_name'];
                            $sync_data->birth_date = $update_data['dateBirth'];
                            $sync_data->gender = $update_data['gender'];
                            $sync_response = update_user_igracias($sync_data);
    
                            $input = $sync_data;
                            unset($input->password);
                            $log = format_log_data($input, 'Update User In Igracias');
                            insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());
                            if($sync_response['status']){
                                $log = format_log_data($input, "User Updated In Igracias");
                                insert_log('USER_FRONTEND_UPDATE', $log, get_logged_in_id());
                                // udpate password igracias
                                $old_password = $user->old_password;
                                if($password && $old_password){
                                    $sync_data = new stdClass();
                                    $sync_data->username = $user->username;
                                    $sync_data->old_password = $old_password;
                                    $sync_data->new_password = $password;
                                    $sync_response = update_password_igracias($sync_data);

                                    $log = format_log_data(['username' => $user->username], 'Update Password User In Igracias');
                                    insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                                    if($sync_response['status']){
                                        $log = format_log_data(['username' => $user->username], "User Password Updated In Igracias");
                                        insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                                    }else{
                                        $log = format_log_data(['username' => $user->username], $sync_response['message']);
                                        insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                                        array_push($error, $user->username . ' Error Igracias - ' . $sync_response['message']);
                                        continue;
                                    }
                                }
                            }else{
                                $log = format_log_data($input, $sync_response['message']);
                                insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                                array_push($error, $user->username . ' Error Igracias - ' . $sync_response['message']);
                                continue;
                            }
                        }else{
                            $log = format_log_data(['username' => $user->username], 'User Not Found In Igracias');
                            insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                            // create user igracias
                            $sync_data = new stdClass();
                            $sync_data->username = $update_data['username'];
                            $sync_data->password = $password ?: 'Qwerty1@';
                            $sync_data->phone_number = $update_data['phone_number'];
                            $sync_data->city = $city->name;
                            $sync_data->email = $update_data['email'];
                            $sync_data->fullname = $update_data['full_name'];
                            $sync_data->birth_date = $update_data['dateBirth'];
                            $sync_data->gender = $update_data['gender'];
                            $sync_response = create_user_igracias($sync_data);
    
                            $input = $sync_data;
                            unset($input->password);
                            $log = format_log_data($input, 'Create User In Igracias');
                            insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                            if($sync_response['status']){
                                $log = format_log_data($input, "User Created In Igracias");
                                insert_log('USER_FRONTEND_IMPORT', $log, get_logged_in_id());
                                $update_data['personid'] = $sync_response['personid'];
                            }else{
                                $log = format_log_data($input, $sync_response['message']);
                                insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                                array_push($error, $user->username . ' Error Igracias - ' . $sync_response['message']);
                                continue;
                            }
                        }
                        
                        // update user database
                        if($this->ion_auth->update($primary_key['id'], $update_data)) {
                            unset($update_data['password']);
                            
                            $log = format_log_data($update_data, "User Updated In Database");
                            insert_log('USER_FRONTEND_IMPORT_SUCCESS', $log, get_logged_in_id());
                            
                            $u++;
                        }else{
                            unset($update_data['password']);
                            
                            $log = format_log_data($update_data, "Failed Register User To Database");
                            insert_log('USER_FRONTEND_UPDATE_FAILED', $log, get_logged_in_id());
                            
                            array_push($error, $user->username . ' Error Database - Failed Register User To Database');
                            continue;
                        }
                    }else{
                        
                        $log = format_log_data(['email' => $user->email], 'Email has been used');
                        insert_log('USER_FRONTEND_IMPORT_FAILED', $log, get_logged_in_id());
                        
                        array_push($error, $user->username . ' Error - Email has been used');
                        continue;
                    }
                }
            }
        }
        

        $this->session->set_flashdata('import_error', $error);

        $response->code = 200;
        $response->status = true;
        $response->message = $i . ' inserted and ' . $u . ' updated';

        echo json_encode($response);
    }

    public function export(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        // $table="sync_category as unit";
        $table="users";
        $order = "id"; 
        $filter = $search;
        $column_search = array("username", "email", "full_name", "country.name", "city.name");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.country_id = country.id";
        $join->table = "country";
        $extra_join[] = $join;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.city_id = city.id";
        $join->table = "city";
        $extra_join[] = $join;

        $custom_select = array("users.id", "username", "email", "full_name", "country.name as country", "city.name as city", "gender", "dateBirth", "phone_number");

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $arr = array();
        foreach ($result as $row) {
            $row->gender = $row->gender == 1 ? 'Male' : ($row->gender == 2 ? 'Female' : 'Other/Prefer Not to Say');
            $arr[] = array($row->id,$row->username, $row->email,$row->full_name, $row->country, $row->city, $row->gender, $row->dateBirth, $row->phone_number);
         }
         $filed = array('Id','Username','Email','Fullname', 'Country', 'City', 'Gender', 'Date Birth', 'Phone Number');
         //do export
        export_csv($arr,$filed);         
    }

    public function print_frontend(){ 
        //get value
        $search = $this->input->post('search');   
        //load helper
        $this->load->model('Datatable_model');
        $this->load->helper('export');

        // $table="sync_category as unit";
        $table="users";
        $order = "id"; 
        $filter = $search;
        $column_search = array("username", "email", "full_name", "country.name", "city.name");
        $extra_where =false;
        $extra_where_or=false; 
        $extra_join=false; 
        $custom_select=false;       
        $column_type=false;
        $start=false;
        $limit=false;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.country_id = country.id";
        $join->table = "country";
        $extra_join[] = $join;

        $join = new stdClass();
        $join->join_type = "LEFT";
        $join->condition = "users.city_id = city.id";
        $join->table = "city";
        $extra_join[] = $join;

        $custom_select = array("users.id", "username", "email", "full_name", "country.name as country", "city.name as city", "gender", "dateBirth", "phone_number");

        $result = $this->Datatable_model->get_data($table, $extra_where, $extra_where_or, $extra_join, $custom_select, $order, $filter, $column_search, $column_type,$start,$limit);

        $response = get_ajax_response();

        if ($result){
            $data = $this->load->view("user_frontend/print",array("data"=>$result),true);
            $response->code = 200;
            $response->status = true;
            $response->message = "Load Data Successfully";
            $response->data = $data;
        } else {
            $response->code = 400;
            $response->status = true;
            $response->message = "Fail Load Data";
            $response->data = null;
        }

        echo json_encode($response);           
    }
}
