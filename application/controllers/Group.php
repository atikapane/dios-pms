<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	function __construct() 
	{
		parent::__construct();		
		$this->load->model('Group_Model','m_group');						
	}
	
	public function index()
	{		
		$this->load->view('group/index.php');
	}

	public function save()
	{	
		$data['group_name'] = $this->input->post('group_name');
		$db=false;
		$db = $this->web_app_model->insert_data('group',$data);
		if($db=true){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function update()
	{	
		$groupid 			= $this->input->post('group_id');
		$data['group_name'] = $this->input->post('group_name');

		$db=false;
		$db = $this->web_app_model->update_data('group',$data,$groupid,'group_id');
		if($db=true){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function delete()
	{
		$data['group_id'] = $this->input->post('group_id');
		$db=false;
		$db = $this->web_app_model->delete_data('group',$data);
		if($db=true){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	//json data
	public function get_group()
	{
		$data = $this->m_group->get_group();
		echo json_encode($data);
	}

	public function get_group_byid($id)
	{
		$data = $this->m_group->get_group_byid($id);
		echo json_encode($data);
	}
	
}
