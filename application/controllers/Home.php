<?php
/**
 * Created by PhpStorm.
 * User: djakapermana
 * Date: 13/11/19
 * Time: 21.13
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
    	parent::__construct();

        $this->load->model('Web_App_Model');
    }

	public function index()
	{
        $oauth_provider = $this->session->flashdata('oauth_provider');
        $data["data_user"] = null;
        $data["redir"] = $this->input->get('redir');

        if(!empty($oauth_provider)){
            if($oauth_provider == 'google'){
                $this->google->setRedirectUri(base_url('UsersAuth'));
                if (isset($_GET['code'])) {
                    $token = $this->google->getAuthenticate($_GET['code']);
                    $this->google->setAccessToken($token['access_token']);
                    $gpInfo = $this->google->getUserInfo();

                    $userData["oauth_provider"] = $oauth_provider;
                    $userData["id"] = $gpInfo["id"];
                    $userData["email"] = $gpInfo["email"];
                    $userData["full_name"] = $gpInfo["name"];
                }
            } else if($oauth_provider == "facebook"){
                $this->facebook->set_redirect_uri('UsersAuth');
                if($this->facebook->is_authenticated()){
                    $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture,birthday');

                    $userData["oauth_provider"] = $oauth_provider;
                    $userData["id"] = $fbUser["id"];
                    $userData["email"] = $fbUser["email"];
                    $userData["full_name"] = $fbUser["first_name"]." ".$fbUser["last_name"];
                }
            }

            if(!empty($fbUser) || !empty($gpInfo)){
                if ($this->Users_Model->checkUserAuth($userData))
                {
                    $this->session->set_userdata('user_data',$userData);
                    redirect('','auto');
                }else{
                    $this->session->set_flashdata('message_error', 'Message error: An error occurred when signing you in to Celoe');
                }
            }
        }
        
        $data['user'] = $this->ion_auth->user()->row();
	    $data["title"] = "Home";
        $data["sub_menu"] = false;
        $data['active_page'] = "home";
        $data["banner"] = $this->load_banner();
        $data["news"] = $this->load_news();
        $data["category_list"] =  $this->load_category();
        $data["course_list"] = $this->load_course();
        $data["landing_content"] = $this->load_landing_content();
        $this->load->view('fe/home/index', $data);
    }

	public function load_news(){
        $query_news = $this->Web_App_Model->get_data_limit('news',$this->config->item('limit_news'),'updated_at');
        return $query_news;
    }

    public function load_banner(){
        $query_banner = $this->Web_App_Model->get_data_all('landing_banner',"priority asc");
        return $query_banner;
    }

    public function load_course(){
        $query_course = $this->web_app_model->get_data_list("ocw_course_joined","","",0,$this->config->item('limit_course'),"id","status=3");
        return $query_course;
    }

    public function load_landing_content(){
        $content = $this->web_app_model->get_data_list("landing_content","","",0,1000,"priority","","asc");
        return $content;
    }

    public function load_category(){
        $query_category = $this->web_app_model->get_data_all("vw_ocw_category");
        return $query_category;
    }

    public function search_course(){
        $response = init_response();

        $search_content = $this->input->post("search_content",true);

        if(strlen(trim($search_content))>0){
            $search_course = array(
                'course' => $search_content
            );
            $this->session->set_userdata($search_course);

            $response->status = true;
            $response->error_code = 0;
            $response->message = "success";
        }

        json_response($response);
    }

    public function today_survey(){
        $response = get_ajax_response();
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('user_id', 'User Id', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        }
        else
        {
            $user_id =  $this->input->get('user_id');
            $now = new DateTime();
            $where = [
                'survey.start_date <=' => $now->format('Y-m-d'),
                'survey.end_date >=' => $now->format('Y-m-d'),
                'survey.platform' => 'celoe'
            ];

            $survey_objects = $this->web_app_model->get_data('survey', $where);
            foreach($survey_objects as $key => $survey){
                $survey_not_show = $this->web_app_model->get_single_data('survey_not_show', ['user_id' => $user_id, 'survey_id' => $survey->id]);
                if(!empty($survey_not_show)){
                    unset($survey_objects[$key]);
                }
            }

            $survey = array_shift($survey_objects);

            if(!empty($survey)){
                $this->web_app_model->update_data("survey",['seen_count' => ++$survey->seen_count],$survey->id,"id");

                $response->status = true;
                $response->error_code = 200;
                $response->survey = $survey;
            }else{
                $response->status = false;
                $response->error_code = 400;
                $response->message = 'There is no survey';
            }
        }

        echo json_encode($response);
    }

    public function survey_reject(){
        $response = get_ajax_response();
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('survey_id', 'Survey Id', 'required');
        $this->form_validation->set_rules('not_show', 'Not Show', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->code = 400;
            $response->status = false;
            $response->message = strip_tags(implode(", ", $this->form_validation->error_array()));
        }
        else
        {
            $user_id = $this->input->post('user_id', true);
            $survey_id = $this->input->post('survey_id', true);
            $not_show = $this->input->post('not_show', true);
    
            $survey_object = $this->web_app_model->get_single_data('survey', ['id' => $survey_id]);
            if(!empty($survey_object)){
                if($not_show == 'false'){
                    $this->web_app_model->update_data("survey",['reject_count' => ++$survey_object->reject_count],$survey_id,"id");
                }else{
                    $insert_data = [
                        'user_id' => $user_id,
                        'survey_id' => $survey_id,
                    ];
                    $this->Web_App_Model->insert_data('survey_not_show', $insert_data);
                    $this->web_app_model->update_data("survey",['reject_permanently_count' => ++$survey_object->reject_permanently_count],$survey_id,"id");
                }
    
                $response->error_code = 200;
                $response->status = true;
                $response->message = 'Survey rejected';
            }else{
                $response->code = 400;
                $response->status = false;
                $response->message = 'Survey not found';
            }
        }

        echo json_encode($response);
    }
}

/* End of file Controllername.php */