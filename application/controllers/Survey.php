<?php
/**
 * Created by PhpStorm.
 * User: djakapermana
 * Date: 13/11/19
 * Time: 21.13
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends CI_Controller {
    protected $full_path = "";

    function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->model('web_app_model');
    }

    public function index()
    {
        $get_params = $this->input->get();
        $this->form_validation->set_data($get_params);
        $this->form_validation->set_rules('survey_id', 'Survey Id', 'required');
        $this->form_validation->set_rules('id', 'Respondent Id', 'required');
        $this->form_validation->set_rules('username', 'Respondent Username', 'required');
        $this->form_validation->set_rules('name', 'Respondent Name', 'required');
        $this->form_validation->set_rules('email', 'Respondent Email', 'required');
        $this->form_validation->set_rules('origin', 'Respondent Origin', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            redirect(base_url());
        }

        $survey_id = $this->input->get('survey_id', true);
        $id = $this->input->get('id', true);
        $username = $this->input->get('username', true);
        $name = $this->input->get('name', true);
        $email = $this->input->get('email', true);
        $origin = $this->input->get('origin', true);
        
        $survey_object = $this->web_app_model->get_single_data('survey', ['id' => $survey_id]);
        if(!empty($survey_object)){
            $respondent_object = $this->web_app_model->get_single_data('survey_respondent', ['survey_id' => $survey_id, 'respondent_id' => $id]);
            if(strtotime($survey_object->end_date) < strtotime(now())){
                $data['error'] = '<p>Survey Expired</p>';
            }else if(!empty($respondent_object)){
                $data['error'] = '<p>You have participate in this survey</p>';
            }else{
                $this->web_app_model->update_data("survey",['view_count' => ++$survey_object->view_count],$survey_id,"id");
            }
        }else{
            $data['error'] = '<p>Wrong survey id</p>';
        }

        $data['respondent'] = [
            'id' => $id,
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'origin' => $origin
        ];

        $data['survey'] = json_encode($survey_object);
        $data["sub_menu"] = false;
        $data["title"] = "Survey";
        $data["sub_title"] = "";
        $data['active_page'] = "";
        $this->load->view('fe_v2/survey/index', $data);
    }

    public function get_question(){
        $response = get_ajax_response();

        $id = $this->input->get('id', true);
        $question_objects = $this->web_app_model->get_data('survey_question', ['survey_id' => $id]);
        if(!empty($question_objects)){
            foreach($question_objects as &$question){
                if($question->type == 'multiple_choice'){
                    $question->choices = $this->web_app_model->get_data('survey_question_option', ['survey_question_id' => $question->id]);
                }
            }

            $response->status = true;
            $response->error_code = 200;
            $response->data = $question_objects;
        }else{
            $response->status = false;
            $response->error_code = 400;
            $response->message = '<p>There is no question</p>';
        }
        
        echo json_encode($response);
    }

    public function save(){
        $response = get_ajax_response();

        $survey_id = $this->input->post('survey_id', true);
        $question_objects = $this->web_app_model->get_data('survey_question', ['survey_id' => $survey_id]);

        for($i = 1; $i <= count($question_objects); $i++){
            $this->form_validation->set_rules('answer'.$i, 'Answer '.$i, 'required');
        }
        $this->form_validation->set_rules('respondent', 'Respondent Infromation', 'required');
        $this->form_validation->set_rules('survey_id', 'Survey Id', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $response->status = false;
            $response->code = 400;
            $response->message = implode("<br>", $this->form_validation->error_array());
        }
        else
        {
            $respondent = $this->input->post('respondent', true);
            $respondent = json_decode($respondent);

            $insert_data['survey_id'] = $survey_id;
            $insert_data['respondent_id'] = $respondent->id;
            $insert_data['respondent_username'] = $respondent->username;
            $insert_data['respondent_name'] = $respondent->name;
            $insert_data['respondent_email'] = $respondent->email;
            $insert_data['respondent_origin'] = $respondent->origin;

            $respondent_id = $this->web_app_model->insert_data("survey_respondent", $insert_data);

            $x = 1;
            foreach($question_objects as $question){
                $answer = $this->input->post('answer'.$x, true);

                $insert_answer = [];
                $insert_answer['survey_respondent_id'] = $respondent_id;
                if($question->type == 'freetext'){
                    $insert_answer['answer_text'] = $answer;
                }else if($question->type == 'rating'){
                    $insert_answer['answer_num'] = $answer;
                }else if($question->type == 'multiple_choice'){
                    $choices = $this->web_app_model->get_data('survey_question_option', ['survey_question_id' => $question->id]);
                    foreach($choices as $key => $choice){
                        if($choice->content == $answer){
                            $insert_answer['answer_num'] = $key;
                        }
                    }
                }

                $this->web_app_model->insert_data("survey_respondent_answer", $insert_answer);
                $x++;
            }

            $insert_data = [
                'user_id' => $respondent->id,
                'survey_id' => $survey_id,
            ];
            $this->web_app_model->insert_data('survey_not_show', $insert_data);

            $response->status = true;
            $response->code = 200;
        }

        echo json_encode($response);
    }
}