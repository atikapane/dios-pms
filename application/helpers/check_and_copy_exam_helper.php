<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('check_section'))
{
    function check_section($courseidnumber, $sectionname){
        $CI =& get_instance();
        $CI->load->library('Curl');
        /// SETUP - NEED TO BE CHANGED
        $token = 'a849736860557d104b36dbc5ef6d2ece';
        $url = $CI->config->item('exam_check_and_copy');

        /// FUNCTION NAME
        $functionname = 'local_celoecoursehelper_check_course_section';

        ///// XML-RPC CALL
        header('Content-Type: text/plain');
        $serverurl = $url . '?wstoken=' . $token;
        $post = xmlrpc_encode_request($functionname, array($courseidnumber, $sectionname));
        $resp = xmlrpc_decode($CI->curl->post($serverurl, $post));
        
        return is_array($resp) ? (object) $resp : json_decode($resp);
    }
}

if ( ! function_exists('copy_exam'))
{
    function copy_exam($courseidnumber, $sectionname, $courseidnumberlist){
        $CI =& get_instance();
        $CI->load->library('Curl');
        /// SETUP - NEED TO BE CHANGED
        $token = 'a849736860557d104b36dbc5ef6d2ece';
        $url = $CI->config->item('exam_check_and_copy');

        /// FUNCTION NAME
        $functionname = 'local_celoecoursehelper_duplicate_section_content';

        ///// XML-RPC CALL
        header('Content-Type: text/plain');
        $serverurl = $url . '?wstoken=' . $token;
        $post = xmlrpc_encode_request($functionname, array($courseidnumber, $sectionname, $courseidnumberlist));
        $resp = xmlrpc_decode($CI->curl->post($serverurl, $post));

        return is_array($resp) ? (object) $resp : json_decode($resp);
    }
}