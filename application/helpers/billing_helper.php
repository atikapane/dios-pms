<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('generate_invoice_number')){
    function generate_invoice_number($user_id)
    {
        $year = date('y');
        $month = date('m');
        $roman_year = roman_number(20);
        $roman_month = roman_number($month);
    
        //$invoice_number = $this->Transaction_Model->get_max_id() + 1;//uncomment this to use max id instead of timestamp
        $invoice_number = time();
    
        return "INV/$roman_year/$roman_month/$user_id/$invoice_number";
    }
}

if (! function_exists('roman_number')){
    function roman_number($number) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
}