<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('update_user_igracias')){
    function update_user_igracias($data, $mobile = false){
        $CI =& get_instance();
        $CI->load->library('Sync');

        $url = $CI->config->item('igracias_update_user');
        $sync_data = [
            'username' => $data->username,
            'country_code' => 'INA',
            'country' => 'indonesia',
            'identity_card_number' => 111222333,
            'usertypeid' => 1,
            'created_by' => 'celoe'
        ];

        if(isset($data->phone_number)) $sync_data['phonenumber'] = $data->phone_number;
        if(isset($data->city)) $sync_data['city'] = $data->city;
        if(isset($data->email)) $sync_data['email'] = $data->email;
        if(isset($data->fullname)) $sync_data['fullname'] = $data->fullname;
        if(isset($data->birth_date)) $sync_data['datebirthday'] = $data->birth_date;
        if(isset($data->gender)){
            $sync_data['gender'] = $data->gender == '1' ? 'M' : ($data->gender == '2' ? 'F' : 'N');
        }
        $sync_data = array("url"=>$url,"user_data"=>$sync_data);
        return $CI->sync->user_frontend('edit', $sync_data, $mobile);
    }

}
if (! function_exists('create_user_igracias')){
    function create_user_igracias($sync_data, $mobile = false){
        $CI =& get_instance();
        $CI->load->library('Sync');

        $url = $CI->config->item('igracias_create_user');
        $sync_data = [
            'username' => $sync_data->username,
            'password' => $sync_data->password,
            'phonenumber' => $sync_data->phone_number,
            'country_code' => 'INA',
            'country' => 'indonesia',
            'city' => $sync_data->city,
            'email' => $sync_data->email,
            'fullname' => $sync_data->fullname,
            'datebirthday' => $sync_data->birth_date,
            'gender' => $sync_data->gender == '1' ? 'M' : ($sync_data->gender == '2' ? 'F' : 'N'),
            'identity_card_number' => 111222333,
            'usertypeid' => 1,
            'created_by' => 'celoe',
            'code_program_type' => '03'
        ];
        $sync_data = ["url"=>$url, "user_data"=>$sync_data];
        return $CI->sync->user_frontend('add', $sync_data, $mobile);
    }
}

if (! function_exists('update_password_igracias')){
    function update_password_igracias($sync_data, $mobile = false){
        $CI =& get_instance();
        $CI->load->library('Sync');

        $url = $CI->config->item('igracias_update_password_user');
        $sync_data = [
            'username' => $sync_data->username,
            'oldpassword' => $sync_data->old_password,
            'newpassword' => $sync_data->new_password,
        ];
        $sync_data = ["url"=>$url, "user_data"=>$sync_data];
        return $CI->sync->user_frontend('update_password', $sync_data, $mobile);
    }
}

if (! function_exists('delete_user_igracias')){
    function delete_user_igracias($username, $mobile = false){
        $CI =& get_instance();
        $CI->load->library('Sync');

        $url = $CI->config->item('igracias_delete_user');
        $sync_data = array("url"=>$url,"user_data"=>$username);
        return $CI->sync->user_frontend('delete', $sync_data, $mobile);
    }
}

if (! function_exists('get_user_igracias')){
    function get_user_igracias($username, $mobile = false){
        $CI =& get_instance();
        $CI->load->library('Sync');

        $url = $CI->config->item('igracias_get_user');
        $sync_data = array("url"=>$url,"user_data"=>$username);
        return $CI->sync->user_frontend('get', $sync_data, $mobile);
    }
}