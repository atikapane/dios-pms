<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('check_user_mooc')){
    function check_user_mooc($username){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        // check user in mooc
        $sync_data = [
            'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
            'wsfunction' => 'core_user_get_users',
            'moodlewsrestformat' => 'json',
            'criteria[0][key]' => 'username',
            'criteria[0][value]' => $username . '@365.telkomuniversity.ac.id'
        ];
        $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
        return $CI->sync->sync_mooc('get', $sync_data);
    }
}

if (! function_exists('delete_user_mooc')){
    function delete_user_mooc($mooc_id){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        // check user in mooc
        $sync_data = [
            'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
            'wsfunction' => 'core_user_delete_users',
            'moodlewsrestformat' => 'json',
            'userids[0]' => $mooc_id
        ];
        $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
        return $CI->sync->sync_mooc('delete', $sync_data);
    }
}

if (! function_exists('create_user_mooc')){
    function create_user_mooc($sync_data){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        $last_name = explode(' ', $sync_data->fullname);
        $first_name = array_shift($last_name);
        $last_name = implode(' ', $last_name);
        $last_name = $last_name ? $last_name : $first_name;
        $sync_data = [
            'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
            'wsfunction' => 'core_user_create_users',
            'moodlewsrestformat' => 'json',
            'users[0][username]' => $sync_data->username . '@365.telkomuniversity.ac.id',
            'users[0][firstname]' => $first_name,
            'users[0][lastname]' => $last_name,
            'users[0][email]' => $sync_data->email,
            'users[0][idnumber]' => 111222333,
            'users[0][auth]' => 'oidc',
        ];
        $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
        return $CI->sync->sync_mooc('create', $sync_data);
    }
}

if (! function_exists('update_user_mooc')){
    function update_user_mooc($data){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        if(isset($data->fullaname)){
            $last_name = explode(' ', $data->fullname);
            $first_name = array_shift($last_name);
            $last_name = implode(' ', $last_name);
            $last_name = $last_name ? $last_name : $first_name;
        }

        $sync_data = [
            'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
            'wsfunction' => 'core_user_update_users',
            'moodlewsrestformat' => 'json',
            'users[0][id]' => $data->id,
            'users[0][idnumber]' => 111222333,
            'users[0][auth]' => 'oidc',
        ];
        if(isset($data->username)) $sync_data['users[0][username]'] = $data->username;
        if(isset($first_name)) $sync_data['users[0][firstname]'] = $first_name;
        if(isset($last_name)) $sync_data['users[0][lastname]'] = $last_name;
        if(isset($data->email)) $sync_data['users[0][email]'] = $data->email;
        if(isset($data->password)) $sync_data['users[0][password]'] = $data->password;

        $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
        return $CI->sync->sync_mooc('update', $sync_data);
    }
}

if (! function_exists('enroll_user_mooc')){
    function enroll_user_mooc($sync_data){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        $prefix = $CI->config->item('prefix_course_shortname');
        $padding = $CI->config->item('padding_course_shortname');
        // enroll user in mooc
        $sync_data = [
            'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
            'wsfunction' => 'local_mooc_api_enrol_users',
            'moodlewsrestformat' => 'json',
            'enrolments[0][userid]' => $sync_data->username . '@365.telkomuniversity.ac.id',
            // 'enrolments[0][courseid]' => 'BAH1A3',
            'enrolments[0][courseid]' => $sync_data->ocw_id,
            'enrolments[0][roleid]' => $sync_data->enroll_type_id,
            'enrolments[0][timestart]' => $sync_data->start_date,
            'enrolments[0][timeend]' => $sync_data->end_date
        ];
        $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
        return $CI->sync->sync_mooc('create', $sync_data);
    }
}

if (! function_exists('unenroll_user_mooc')){
    function unenroll_user_mooc($sync_data){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        $prefix = $CI->config->item('prefix_course_shortname');
        $padding = $CI->config->item('padding_course_shortname');
        // enroll user in mooc
        $sync_data = [
            'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
            'wsfunction' => 'local_mooc_api_unenrol_users',
            'moodlewsrestformat' => 'json',
            'enrolments[0][userid]' => $sync_data->username . '@365.telkomuniversity.ac.id',
            // 'enrolments[0][courseid]' => 'BAH1A3',
            'enrolments[0][courseid]' => $sync_data->ocw_id,
        ];
        $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
        return $CI->sync->sync_mooc('delete', $sync_data);
    }
}

if (! function_exists('create_course_mooc')){
    function create_course_mooc($sync_data){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        if(!empty($sync_data)){
            $prefix = $CI->config->item('prefix_course_shortname');
            $padding = $CI->config->item('padding_course_shortname');
            // create course in mooc
            $sync_data = [
                'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
                'wsfunction' => 'local_mooc_api_create_courses',
                'moodlewsrestformat' => 'json',
                'courses[0][fullname]' => $sync_data->fullname,
                'courses[0][shortname]' => $prefix . '-' . $sync_data->subject_code . '-' . str_pad($sync_data->ocw_id, $padding, '0', STR_PAD_LEFT),
                'courses[0][categoryid]' => $sync_data->faculty_store_name,
                'courses[0][summary]' => $sync_data->description,
                'courses[0][startdate]' => $sync_data->start_date,
                'courses[0][enddate]' => $sync_data->end_date,
                'courses[0][image]' => $sync_data->banner_image,
                'courses[0][idnumber]' => $sync_data->ocw_id,
                'courses[0][restore]' => 1,
                'courses[0][cds_shortname]' => $sync_data->subject_code,
                'courses[0][update]' => 0
            ];
            $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
            return $CI->sync->call_mooc('create', $sync_data);
        }
    }
}

if (! function_exists('delete_course_mooc')){
    function delete_course_mooc($ocw_id){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        if(!empty($ocw_id)){
            // delete course in mooc
            $sync_data = [
                'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
                'wsfunction' => 'local_mooc_api_delete_courses',
                'moodlewsrestformat' => 'json',
                'courseids[0]' => $ocw_id,
            ];
            $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
            return $CI->sync->sync_mooc('delete', $sync_data);
        }
    }
}

if (! function_exists('create_category_mooc')){
    function create_category_mooc($sync_data){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        if(!empty($sync_data)){
            // create category in mooc
            $sync_data = [
                'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
                'wsfunction' => 'local_mooc_api_create_categories',
                'moodlewsrestformat' => 'json',
                'categories[0][name]' => $sync_data->name,
                // 'categories[0][parent]' => $sync_data->parent,
                'categories[0][idnumber]' => $sync_data->idnumber,
                'categories[0][description]' => $sync_data->description
            ];
            $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
            return $CI->sync->sync_mooc('create', $sync_data);
        }
    }
}

if (! function_exists('update_category_mooc')){
    function update_category_mooc($sync_data){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        if(!empty($sync_data)){
            // update category in mooc
            $sync_data = [
                'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
                'wsfunction' => 'local_mooc_api_update_categories',
                'moodlewsrestformat' => 'json',
                'categories[0][name]' => $sync_data->name,
                // 'categories[0][parent]' => $sync_data->parent,
                'categories[0][idnumber]' => $sync_data->idnumber,
                'categories[0][description]' => $sync_data->description
            ];
            $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
            return $CI->sync->sync_mooc('update', $sync_data);
        }
    }
}

if (! function_exists('delete_category_mooc')){
    function delete_category_mooc($idnumber){
        $CI =& get_instance();
        $CI->load->library('Sync');
        $url = $CI->config->item('mooc_api');

        if(!empty($idnumber)){
            // update category in mooc
            $sync_data = [
                'wstoken' => '3661e1f30a7776b4a79e070ae8f5f7f4',
                'wsfunction' => 'local_mooc_api_delete_categories',
                'moodlewsrestformat' => 'json',
                'categories[0][idnumber]' => $idnumber,
                'categories[0][recursive]' => 1
            ];
            $sync_data = array("url"=>$url,"sync_data"=>$sync_data);
            return $CI->sync->sync_mooc('delete', $sync_data);
        }
    }
}