<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('insert_log')){
    function insert_log($key, $request, $response = '', $user_id = ''){
        $CI =& get_instance();
        $CI->load->model('Web_app_model');
        $CI->load->helper('url');

        $check_key = str_replace('_SUCCESS', '', $key);
        $check_key = str_replace('_FAILED', '', $check_key);
        $can_log = $CI->Web_app_model->get_data_join('log_action','log_setting','', 'log_action.id = log_setting.log_action_id', '', ['log_action.key' => $check_key], 'log_setting.is_log');
        if(!empty($can_log)){
            if($can_log[0]->is_log == 1){
                $action = $CI->Web_app_model->get_single_data('log_action', ['key'=>$key]);
                if (!empty($action)){
                    $insert_data['log_action_id'] = $action->id;
                    $insert_data['user_id'] = $user_id ?: null;
                    $insert_data['request'] = $request;
                    $insert_data['response'] = $response ?: null;
                    $insert_data['url'] = current_url();
                    $insert_data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    $insert_data['ip_address'] = $CI->input->ip_address();
        
                    $CI->Web_app_model->insert_data('log_api', $insert_data);
        
                    $message['error'] = false;
                    $message['message'] = "Success Saving Log " . $action->key;
        
                    return $message;
                }
                $message['error'] = true;
                $message['message'] = "Action " . $key . " Not Found";
        
                return $message;
            }
        }
    }
}

if (! function_exists('log_api')){
    function log_api($key,  $response = '', $user_data = '',$request = false){
        if($request == false){
            $request = get_api_log_request();
        }
        if($response != ""){
            //encode json response if not encoded
            $is_valid_json = false;
            if(is_string($response)){
                json_decode($response);
                $is_valid_json =  (json_last_error() == JSON_ERROR_NONE);
            }

            if(!$is_valid_json){
                $response = json_encode($response);
            }
        }
        if($user_data != ""){
            if(property_exists($user_data,"id")){
                $user_data = $user_data->id;
            }else{
                $user_data = "";
            }
        }
        insert_log($key, $request, $response, $user_data);
    }
}

if (!function_exists('format_log_data')){
    function format_log_data($data = [], $message = ''){
        if (!empty($data)){
            $data_object = new stdClass();
            foreach ($data as $key => $value){
                $data_object->$key = $value;
            }
        }

        $log  = new stdClass();
        if ($message) $log->message = $message;
        if (!empty($data_object)) $log->data = $data_object;

        return json_encode($log);
    }
}

if (!function_exists('get_api_log_request')){
    function get_api_log_request(){
        $CI =& get_instance();
        $http_method = $CI->input->method();
        $input_data = array();
        if($http_method == "get"){
            $input_data = $CI->input->get();
        }else{
            $input_data = $CI->input->post();
        }
        return json_encode($input_data);
    }
}