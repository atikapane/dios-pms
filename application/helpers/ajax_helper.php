<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_ajax_response'))
{
    function get_ajax_response()
    {
        $response = new stdClass();
        $response->status = false;
        $response->error_code = 0;
        $response->message = "";
        $response->data = false;
        $response->server_date=date("Y-m-d");
        $response->server_time=date("Y-m-d H:i:s");
        $response->csrf_token = "";
        return $response;
    }   
}

if ( ! function_exists('get_datatables_response'))
{
    function get_datatables_response()
    {
        $response = new stdClass();
        $response->meta = null;
        $response->data = array();
        return $response;
    }   
}