<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('escape_allow_symbol')){
    
    function escape_allow_symbol($str){
        $like_escape_chr = '!';

        if (is_array($str))
        {
            foreach ($str as $key => $val)
            {
                $str[$key] = $this->escape_allow_symbol($val);
            }

            return $str;
        }

        $str = remove_invisible_characters($str, FALSE);
        $str = str_replace("'", "\'", $str);
        $str = str_replace("%", "\%", $str);
        
        return $str;
    }
}