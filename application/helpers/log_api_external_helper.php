<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('insert_log_external')){
    function insert_log_external($key, $url, $request, $response = '', $is_success = false, $user_id = ''){
        $CI =& get_instance();
        $CI->load->model('Web_app_model');

        $check_key = str_replace('_SUCCESS', '', $key);
        $check_key = str_replace('_FAILED', '', $check_key);
        $can_log = $CI->Web_app_model->get_data_join('log_action','log_setting','', 'log_action.id = log_setting.log_action_id', '', ['log_action.key' => $check_key], 'log_setting.is_log');

        if(!empty($can_log)){
            if($can_log[0]->is_log == 1){
                $action = $CI->Web_app_model->get_single_data('log_action', ['key'=>$key]);
                if (!empty($action)){
                    $insert_data['log_action_id'] = $action->id;
                    $insert_data['user_id'] = $user_id ?: null;
                    $insert_data['request'] = $request;
                    $insert_data['response'] = $response ?: 'null';
                    $insert_data['status'] = $is_success;
                    $insert_data['url'] = $url;
                    $insert_data['user_agent'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "php";
                    $insert_data['ip_address'] = $CI->input->ip_address();
        
                    $CI->Web_app_model->insert_data('log_api_external', $insert_data);
        
                    $message['error'] = false;
                    $message['message'] = "Success Saving Log " . $action->key;
        
                    return $message;
                }
                $message['error'] = true;
                $message['message'] = "Action " . $key . " Not Found";
        
                return $message;
            }
        }
    }
}

if (!function_exists('format_log_data')){
    function format_log_data($data = [], $message = ''){
        if (!empty($data)){
            $data_object = new stdClass();
            foreach ($data as $key => $value){
                $data_object->$key = $value;
            }
        }

        $log  = new stdClass();
        if ($message) $log->message = $message;
        if (!empty($data_object)) $log->data = $data_object;

        return json_encode($log);
    }
}