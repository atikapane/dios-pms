<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('export_csv')){
    function export_csv($arr, $field){
         //output headers so that the file is downloaded rather than displayed
         header('Content-Type: text/csv; charset=utf-8');
         header('Content-Disposition: attachment; filename=export_data.csv');          
         // create a file pointer connected to the output stream
         $output = fopen('php://output', 'w');          
         // output the column headings
         fputcsv($output, $field);          
         //Loop through the array and add to the csv
         foreach ($arr as $row) {
             fputcsv($output, $row);
         }
         return $output;
    }
}