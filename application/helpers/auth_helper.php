<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('is_logged_in'))
{
    function is_logged_in()
    {
        $CI =& get_instance();
        $user_session = $CI->session->userdata("identity");
        return !empty($user_session) && is_array($user_session) && array_key_exists("user_id", $user_session) && !empty($user_session['user_id']);
    }   
}

if ( ! function_exists('get_looged_in_id'))
{
    function get_logged_in_id()
    {
    	if(is_logged_in()){
	        $CI =& get_instance();
	        $user_session = $CI->session->userdata("identity");
	        return $user_session['user_id'];
    	}else{
    		return false;
    	}
    }   
}

if ( ! function_exists('get_login_info'))
{
    function get_login_info()
    {
        if(is_logged_in()){
            $CI =& get_instance();
            $user_session = $CI->session->userdata("identity");
            return $user_session;
        }else{
            return false;
        }
    }   
}

if ( ! function_exists('get_token_info'))
{
    function get_token_info()
    {
        if(is_logged_in()){
            $CI =& get_instance();
            $token_data = $CI->session->userdata("token_data");
            return $token_data;
        }else{
            return false;
        }
    }   
}
