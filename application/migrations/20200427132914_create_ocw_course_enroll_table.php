<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ocw_course_enroll_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `ocw_course_enroll` (
            `id` int NOT NULL AUTO_INCREMENT,
            `ocw_course_id` int NOT NULL,
            `user_id` int NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `ocw_course_enroll`";
        $this->db->query($sql);
    }
}