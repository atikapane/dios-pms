<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Revise_course_change_report_view extends CI_Migration {

    public function up()
    {
		$sql = "DROP VIEW IF EXISTS course_change_report;";
    	$this->db->query($sql);
		$sql = "CREATE view course_change_report AS SELECT lms_course.subject_code, lms_course.subject_name, lms_course.class,lms_course.semester, lms_course.user_id, lms_course.employee_name,school_year.category_name AS year, study_program.category_id AS study_program_id, study_program.category_name AS study_program_name, faculty.category_id AS faculty_id, faculty.category_name AS faculty_name, log.forum, log.assignment,log.hvp, log.label,log.page,log.quiz,log.resource,log.url FROM sync_lms_course lms_course JOIN sync_category school_year ON school_year.category_id = lms_course.category_id JOIN sync_category study_program ON study_program.category_id = school_year.category_parent_id JOIN sync_category faculty ON faculty.category_id = study_program.category_parent_id JOIN course_change_log log ON log.course_id = lms_course.course_id WHERE lms_course.employee_name IS NOT NULL";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}