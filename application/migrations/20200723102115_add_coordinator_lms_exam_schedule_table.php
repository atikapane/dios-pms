<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_coordinator_lms_exam_schedule_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `lms_exam_schedule` ADD `coordinator_lecturer` int NOT NULL AFTER `id_course_master`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `lms_exam_schedule` DROP  COLUMN `coordinator_lecturer`";
        $this->db->query($sql);
    }
}