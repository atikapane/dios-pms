<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_grade_user_grade_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `user_grade` CHANGE `grade` `grade` int NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user_grade` CHANGE `grade` `grade` int NOT NULL";
        $this->db->query($sql);
    }
}