<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_survey2 extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `survey`
        DROP `respondent`";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}