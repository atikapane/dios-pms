<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_tools_menu extends CI_Migration {

    public function up()
    {
		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (9, 'Tools', NULL, 'fa-wrench');";
    	$this->db->query($sql);
		

    	$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (92, '9', 'Import Course Review', 'be/tools/course_review_importer', NULL);";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}