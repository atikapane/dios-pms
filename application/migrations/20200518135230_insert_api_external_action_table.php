<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_external_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Create User', 'API_EXTERNAL_MOOC_CREATE_USER', 'event log api external mooc create user')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Update User', 'API_EXTERNAL_MOOC_UPDATE_USER', 'event log api external mooc update user')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Delete User', 'API_EXTERNAL_MOOC_DELETE_USER', 'event log api external mooc delete user')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Check User', 'API_EXTERNAL_MOOC_CHECK_USER', 'event log api external mooc check user')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Check User', 'API_EXTERNAL_MOOC_ENROLL_USER', 'event log api external mooc enroll user')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Create User', 'API_EXTERNAL_IGRACIAS_CREATE_USER', 'event log api external igracias create user')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Update User', 'API_EXTERNAL_IGRACIAS_UPDATE_USER', 'event log api external igracias update user')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Delete User', 'API_EXTERNAL_IGRACIAS_DELETE_USER', 'event log api external igracias delete user')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_MOOC_UPDATE_USER' OR `key` = 'API_EXTERNAL_MOOC_DELETE_USER' OR `key` = 'API_EXTERNAL_MOOC_CHECK_USER' OR `key` = 'API_EXTERNAL_MOOC_ENROLL_USER' OR `key` = 'API_EXTERNAL_MOOC_CREATE_USER'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_IGRACIAS_UPDATE_USER' OR `key` = 'API_EXTERNAL_IGRACIAS_DELETE_USER' OR `key` = 'API_EXTERNAL_IGRACIAS_CREATE_USER'";
        $this->db->query($sql);
    }
}