<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_sub_category_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `log_action` ADD `sub_category` VARCHAR(255) NOT NULL AFTER `category`";
        $this->db->query($sql);

        $sql = "UPDATE `log_action` SET `sub_category`='Login' WHERE `key` IN ('USER_LOGIN', 'USER_LOGIN_SUCCESS', 'USER_LOGIN_FAILED')";
        $this->db->query($sql);

        $sql = "UPDATE `log_action` SET `sub_category`='Logout' WHERE `key` IN ('USER_LOGOUT')";
        $this->db->query($sql);

        $sql = "UPDATE `log_action` SET `sub_category`='Token Sync' WHERE `key` IN ('USER_TOKEN_SYNC', 'USER_TOKEN_SYNC_SUCCESS', 'USER_TOKEN_SYNC_FAILED')";
        $this->db->query($sql);

        $sql = "UPDATE `log_action` SET `sub_category`='Login Sync' WHERE `key` IN ('USER_LOGIN_SYNC', 'USER_LOGIN_SYNC_SUCCESS', 'USER_LOGIN_SYNC_FAILED')";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}