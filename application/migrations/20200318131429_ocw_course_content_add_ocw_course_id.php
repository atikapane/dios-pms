<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Ocw_course_content_add_ocw_course_id extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_course_content` ADD `ocw_course_id` INT NOT NULL DEFAULT '0' AFTER `public_url`;";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}