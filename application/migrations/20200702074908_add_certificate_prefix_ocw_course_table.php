<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_certificate_prefix_ocw_course_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `ocw_course` ADD `certificate_prefix` VARCHAR(50) NULL AFTER `template_id`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `ocw_course` DROP  COLUMN `certificate_prefix`";
        $this->db->query($sql);
    }
}