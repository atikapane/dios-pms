<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_dummy_about_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `landing_about` (`title`, `short_title`, `description`, `priority`) VALUES ('CELOE LANDING PAGE', 'CLP', 'Description Landing Page', 1)";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `landing_about` WHERE `title` = 'CELOE LANDING PAGE'";
        $this->db->query($sql);
    }
}