<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Drop_old_survey_table extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS survey, survey_answer, survey_question, survey_question_answer, survey_question_options";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}