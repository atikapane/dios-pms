<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_external_action_part_two_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('LMS', 'Course Sync', 'API_EXTERNAL_LMS_COURSE_SYNC', 'event log api external lms course sync')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Course Sync', 'API_EXTERNAL_CDS_COURSE_SYNC', 'event log api external cds course sync')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Approve Course', 'API_EXTERNAL_CDS_APPROVE_COURSE', 'event log api external cds approve course')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Create Subject', 'API_EXTERNAL_IGRACIAS_CREATE_SUBJECT', 'event log api external igracias create subject')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Update Subject', 'API_EXTERNAL_IGRACIAS_UPDATE_SUBJECT', 'event log api external igracias update subject')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Delete Subject', 'API_EXTERNAL_IGRACIAS_DELETE_SUBJECT', 'event log api external igracias delete subject')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Enroll Course', 'API_EXTERNAL_IGRACIAS_ENROLL_COURSE', 'event log api external igracias enroll course')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Unenroll Course', 'API_EXTERNAL_IGRACIAS_UNENROLL_COURSE', 'event log api external igracias unenroll course')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('DEV', 'Token', 'API_EXTERNAL_DEV_TOKEN', 'event log api external dev token')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('DEV', 'Auth', 'API_EXTERNAL_DEV_AUTH', 'event log api external dev auth')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('DEV', 'Get User', 'API_EXTERNAL_DEV_GET_USER', 'event log api external dev get user')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('OCW', 'CONTENT', 'API_EXTERNAL_OCW_CONTENT', 'event log api external ocw get content')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Course Development', 'API_EXTERNAL_CDS_COURSE_DEVELOPMENT', 'event log api external cds course development')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('LMS', 'Course Change', 'API_EXTERNAL_LMS_COURSE_CHANGE', 'event log api external lms course change')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('LMS', 'Course Attendance', 'API_EXTERNAL_LMS_COURSE_ATTENDANCE', 'event log api external lms course attendance')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('GRAPH', 'Oauth', 'API_EXTERNAL_GRAPH_OAUTH', 'event log api external graph oauth')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('GRAPH', 'Sharepoint', 'API_EXTERNAL_GRAPH_SHAREPOINT', 'event log api external graph sharepoint')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Course Site Id', 'API_EXTERNAL_CDS_COURSE_SITE_ID', 'event log api external cds course site id')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_LMS_COURSE_SYNC' OR `key` = 'API_EXTERNAL_CDS_COURSE_SYNC' OR `key` = 'API_EXTERNAL_CDS_APPROVE_COURSE'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_IGRACIAS_CREATE_SUBJECT' OR `key` = 'API_EXTERNAL_IGRACIAS_UPDATE_SUBJECT' OR `key` = 'API_EXTERNAL_IGRACIAS_DELETE_SUBJECT'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_IGRACIAS_ENROLL_COURSE' OR `key` = 'API_EXTERNAL_IGRACIAS_UNENROLL_COURSE'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_DEV_TOKEN' OR `key` = 'API_EXTERNAL_DEV_AUTH' OR `key` = 'API_EXTERNAL_DEV_GET_USER'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_OCW_CONTENT'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_COURSE_DEVELOPMENT'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_GRAPH_OAUTH' OR `key` = 'API_EXTERNAL_GRAPH_SHAREPOINT'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_COURSE_SITE_ID'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_LMS_COURSE_CHANGE' OR `key` = 'API_EXTERNAL_LMS_COURSE_ATTENDANCE'";
        $this->db->query($sql);
    }
}