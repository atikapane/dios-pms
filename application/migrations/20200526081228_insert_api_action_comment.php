<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_action_comment extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COMMENT', 'list', 'API_COMMENT_LIST', 'get list comment');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COMMENT', 'reply', 'API_COMMENT_REPLY', 'get list reply');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COMMENT', 'detail', 'API_COMMENT_DETAIL', 'get comment detail');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COMMENT', 'create', 'API_COMMENT_CREATE', 'create comment');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COMMENT', 'update', 'API_COMMENT_UPDATE', 'update comment');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COMMENT', 'delete', 'API_COMMENT_DELETE', 'delete comment');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '61', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '62', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '63', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '64', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '65', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '66', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_COMMENT_LIST' OR `key` = 'API_COMMENT_REPLY' OR `key` = 'API_COMMENT_DETAIL' OR `key` = 'API_COMMENT_CREATE' OR `key` = 'API_COMMENT_UPDATE' OR `key` = 'API_COMMENT_DELETE'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 61 OR `log_action_id` = 62 OR `log_action_id` = 63 OR `log_action_id` = 64 OR `log_action_id` = 65 OR `log_action_id` = 66";
        $this->db->query($sql);
    }
}