<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Course_attendance_sync extends CI_Migration {

    public function up()
    {
    	$sql = "DROP TABLE IF EXISTS `course_attendance_sync`;";
    	$this->db->query($sql);
    	
		$sql = "CREATE TABLE `course_attendance_sync` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `date` date NOT NULL,
		  `num_course` int(11) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`date`,`num_course`) USING BTREE
		)";
    	$this->db->query($sql);

    	$sql = "INSERT INTO `course_attendance_sync` VALUES ('1', '2020-02-02', '1');";
    	$this->db->query($sql);

    	$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (30, '7', 'Course Teaching Attendance', 'be/report/Course_attendance', NULL);";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}