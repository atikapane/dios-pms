<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_survey_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Survey', 'Survey Today', 'API_SURVEY_TODAY', 'get today survey');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Survey', 'Survey Not Show', 'API_SURVEY_NOT_SHOW', 'insert survey that not show again');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Survey', 'Survey Check', 'API_SURVEY_CHECK', 'check survey');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Survey', 'Get Class', 'API_SURVEY_GET_CLASS', 'get class for survey');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_SURVEY_TODAY' OR `key` = 'API_SURVEY_NOT_SHOW' OR `key` = 'API_SURVEY_CHECK' OR `key` = 'API_SURVEY_GET_CLASS'";
        $this->db->query($sql);
    }
}