<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_push_notif_token extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS push_notif_token";
    	$this->db->query($sql);

        $sql = "CREATE TABLE `push_notif_token` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `token` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->db->query($sql);;
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS push_notif_token";
        $this->db->query($sql);
    }
}