<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_partner_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `partner` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` VARCHAR(255) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE
		)";
        $this->db->query($sql);

        $sql = "INSERT INTO `partner` (`name`) VALUES ('BNI');";
        $this->db->query($sql);
        $sql = "INSERT INTO `partner` (`name`) VALUES ('OJK');";
        $this->db->query($sql);
        $sql = "INSERT INTO `partner` (`name`) VALUES ('BI');";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}