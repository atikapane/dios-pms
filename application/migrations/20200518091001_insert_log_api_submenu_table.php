<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_log_api_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (11, 'Celoe Api Log', 'be/log/api')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Celoe Api Log'";
        $this->db->query($sql);
    }
}