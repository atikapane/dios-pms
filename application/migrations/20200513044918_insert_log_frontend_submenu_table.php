<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_log_frontend_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (11, 'User Frontend Log', 'be/log/frontend')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'User Frontend Log'";
        $this->db->query($sql);
    }
}