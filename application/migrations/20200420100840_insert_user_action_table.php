<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_user_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_LOGIN_SUCCESS', 'event log user backend success')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_LOGIN_FAILED', 'event log user backend failed')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_TOKEN_SYNC', 'event log user token sync')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_TOKEN_SYNC_SUCCESS', 'event log user token sync success')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_TOKEN_SYNC_FAILED', 'event log user token sync failed')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_LOGIN_SYNC', 'event log user login sync')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_LOGIN_SYNC_SUCCESS', 'event log user login sync success')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_LOGIN_SYNC_FAILED', 'event log user login sync failed')";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}