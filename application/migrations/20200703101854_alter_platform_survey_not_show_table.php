<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_platform_survey_not_show_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `survey_not_show` DROP  COLUMN `platform`;";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = 'ALTER TABLE `survey_not_show` ADD `platform` VARCHAR(15) NOT NULL AFTER `survey_id`';
        $this->db->query($sql);
    }
}