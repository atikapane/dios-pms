<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_feedbacks extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
        $sql = "SELECT menuid FROM submenu WHERE submenu_link = 'be/landing/Feedbacks'";
        $menu_id = $this->db->query($sql)->row()->menuid;

        $sql = "DELETE FROM submenu WHERE submenu_link = 'be/landing/Feedbacks'";
        $this->db->query($sql);
    }
}