<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_dummy_terms_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `landing_terms` (`content`) VALUES ('terms')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `landing_terms`";
        $this->db->query($sql);
    }
}