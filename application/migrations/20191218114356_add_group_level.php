<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_group_level extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE `groups` ADD `group_level` INT NOT NULL AFTER `group_name`;";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}