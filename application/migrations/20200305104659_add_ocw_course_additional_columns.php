<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_ocw_course_additional_columns extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE `sync_category` ADD `name` VARCHAR(150) NOT NULL AFTER `category_name`, ADD `study_program_id` INT NOT NULL AFTER `name`;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}