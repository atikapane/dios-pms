<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_my_certificate_log extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Certificate', 'Certificate exist', 'API_MY_CERTIFICATE', 'event log if certificate exist');";
        $this->db->query($sql);

        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, ". $insert_id ." , '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Certificate', 'Certificate not exist', 'API_MY_CERTIFICATE_FAILED', 'event log if certificate not exist');";
        $this->db->query($sql);

        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, ". $insert_id .", '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Certificate', 'Certificate created', 'API_CREATE_MY_CERTIFICATE', 'event log if certificate created');";
        $this->db->query($sql);

        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, ". $insert_id .", '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Certificate', 'Certificate not created', 'API_CREATE_MY_CERTIFICATE_FAILED', 'event log if certificate not created');";
        $this->db->query($sql);

        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, ". $insert_id .", '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_MY_CERTIFICATE' or `key` = 'API_MY_CERTIFICATE_FAILED' or `key` = 'API_CREATE_MY_CERTIFICATE' or `key` = 'API_CREATE_MY_CERTIFICATE_FAILED'" ;
        $this->db->query($sql);
    }
}