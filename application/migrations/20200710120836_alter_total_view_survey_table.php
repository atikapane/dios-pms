<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_total_view_survey_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `survey` ADD `view_count` int NOT NULL DEFAULT 0 AFTER `seen_count`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `survey` DROP  COLUMN `view_count`";
        $this->db->query($sql);
    }
}