<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_survey_class_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `survey_class` (
            `id` int NOT NULL AUTO_INCREMENT,
            `survey_id` int NOT NULL,
            `class` VARCHAR(255) NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS survey_class";
        $this->db->query($sql);
    }
}