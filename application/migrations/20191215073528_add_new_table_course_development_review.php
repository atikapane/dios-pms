<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_new_table_course_development_review extends CI_Migration {

	public function up()
    {
    	$sql = "CREATE TABLE `criteria_review` (
		`id`  int NOT NULL AUTO_INCREMENT ,
		`title`  varchar(100) NOT NULL ,
		`rubric`  text NULL ,
		`criteria_status`  tinyint(3) NOT NULL DEFAULT 1 ,
		PRIMARY KEY (`id`),
		UNIQUE INDEX `unique` (`id`) USING BTREE ,
		INDEX `index` (`title`) USING BTREE );";
        $this->db->query($sql);

        $sql = "INSERT INTO `menu` VALUES ('8', 'Course Management', null, 'fa-swatchbook');";
        $this->db->query($sql);

        $sql = "INSERT INTO `submenu` VALUES ('16', '8', 'Course Development', 'be/course_management/course_development', null);";
        $this->db->query($sql);

        $sql = "INSERT INTO `submenu` VALUES ('17', '8', 'Course Teaching', 'be/course_management/course_teaching', null);";
        $this->db->query($sql);

        $sql = "INSERT INTO `submenu` VALUES ('18', '8', 'Course Review', 'be/course_management/course_review', null);";
        $this->db->query($sql);

        $sql = "INSERT INTO `submenu` VALUES ('19', '8', 'Criteria Review', 'be/course_management/criteria_review', null);";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}