<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_need_grade_certificate_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `certificate` ADD `need_grade` BOOLEAN NOT NULL AFTER `template_image`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `certificate` DROP  COLUMN `need_grade`";
        $this->db->query($sql);
    }
}