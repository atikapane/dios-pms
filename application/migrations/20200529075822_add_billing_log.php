<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_billing_log extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Billing', 'Transaction exist', 'API_PENDING_TX_EXISTS', 'event log if pending tx exist');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '90', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Billing', 'Transaction create', 'API_TX_CREATE', 'event log if tx created');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '91', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Billing', 'Transaction failed', 'API_TX_CREATE_FAILED', 'event log if tx create  failed');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '92', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_PENDING_TX_EXISTS' or `key` = 'API_TX_CREATE' or `key` = 'API_TX_CREATE_FAILED' " ;
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 90 or `log_action_id` = 91 or `log_action_id` = 92";
        $this->db->query($sql);
    }
}