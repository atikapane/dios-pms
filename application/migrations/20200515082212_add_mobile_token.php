<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_mobile_token extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS mobile_token";
        $this->db->query($sql);
        
        $sql = "CREATE TABLE mobile_token (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `token` varchar (100),
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            `user_id` int(8) NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
          $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `mobile_token`";
        $this->db->query($sql);
    }
}