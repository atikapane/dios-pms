<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_data_certificate_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `certificate` CHANGE `data` `data` LONGTEXT NOT NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `certificate` CHANGE `data` `data` TEXT NOT NULL";
        $this->db->query($sql);
    }
}