<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Ocw_category_add_study_name extends CI_Migration {

    public function up()
    {
	    $sql = "ALTER TABLE `ocw_category` ADD `study_program_name` VARCHAR(150) NOT NULL DEFAULT '' AFTER `name`;";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}