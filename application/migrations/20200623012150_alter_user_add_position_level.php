<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_user_add_position_level extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `user` DROP COLUMN IF EXISTS `positionlevel`";
        $this->db->query($sql);

        $sql = "ALTER TABLE `user` ADD COLUMN `positionlevel`  varchar(40) NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user` DROP COLUMN IF EXISTS `positionlevel`";
        $this->db->query($sql);
        
    }
}