<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_course_lms_cds extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `sync_lms_course`
				ADD COLUMN `user_id`  int NULL AFTER `subject_id`,
				ADD COLUMN `user_username`  varchar(40) NULL AFTER `user_id`,
				ADD COLUMN `employeeid`  char(20) NULL AFTER `user_username`,
				ADD COLUMN `lecturercode`  char(3) NULL AFTER `employeeid`,
				ADD COLUMN `employee_name`  varchar(100) NULL AFTER `lecturercode`,
				ADD UNIQUE INDEX `unique` (`course_id`) USING BTREE ,
				ADD INDEX `index` (`category_id`, `semester`, `class`, `subject_code`, `employeeid`) USING BTREE ;";
		$this->db->query($sql);

		$sql = "ALTER TABLE `sync_course`
				ADD UNIQUE INDEX `unique` (`subject_id`) USING BTREE ,
				ADD INDEX `index` (`subject_code`, `subject_type`, `curriculum_year`, `category_id`, `studyprogramid`, `approve_status`, `is_manual_insert`) USING BTREE ;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}