<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_type_course_enroll_internal_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_course_enroll_internal` ADD `enroll_type_id` int NOT NULL AFTER `user_id`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `ocw_course_enroll_internal` DROP COLUMN `enroll_type_id`";
        $this->db->query($sql);
    }
}