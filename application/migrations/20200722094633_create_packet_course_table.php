<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_packet_course_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `packet_ocw_course` (
            `id` int NOT NULL AUTO_INCREMENT,
            `course_id` int NOT NULL,
            `about` TEXT NOT NULL,
            `how_it_works` TEXT NOT NULL,
            `enrollment_options` TEXT NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS packet_ocw_course";
        $this->db->query($sql);
    }
}