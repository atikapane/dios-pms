<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_mooc_id_user_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `user` ADD `mooc_id` int NOT NULL AFTER `c_unit_id`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user` DROP COLUMN `mooc_id`";
        $this->db->query($sql);
    }
}