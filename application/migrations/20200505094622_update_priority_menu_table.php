<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_priority_menu_table extends CI_Migration {

    public function up()
    {
        $sql = "UPDATE `menu` SET `priority` = 1 WHERE `menu_name` = 'User Management'";
        $this->db->query($sql);
        
        $sql = "UPDATE `menu` SET `priority` = 2 WHERE `menu_name` = 'Site Setting'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 3 WHERE `menu_name` = 'Sync Master Data'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 4 WHERE `menu_name` = 'CDS'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 5 WHERE `menu_name` = 'LMS'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 6 WHERE `menu_name` = 'Home Page'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 7 WHERE `menu_name` = 'Report'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 8 WHERE `menu_name` = 'Course Management'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 9 WHERE `menu_name` = 'Tools'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 10 WHERE `menu_name` = 'Open Course Ware'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 11 WHERE `menu_name` = 'Log Management'";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'User Management'";
        $this->db->query($sql);
        
        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Site Setting'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Sync Master Data'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'CDS'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'LMS'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Home Page'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Report'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Course Management'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Tools'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Open Course Ware'";
        $this->db->query($sql);

        $sql = "UPDATE `menu` SET `priority` = 0 WHERE `menu_name` = 'Log Management'";
        $this->db->query($sql);
    }
}