<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_survey_respondent_answer_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `survey_respondent_answer` (
            `id` int NOT NULL AUTO_INCREMENT,
            `survey_question_id` int NOT NULL,
            `answer_text` TEXT NOT NULL,
            `answer_num` int NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS survey_respondent_answer";
        $this->db->query($sql);
    }
}