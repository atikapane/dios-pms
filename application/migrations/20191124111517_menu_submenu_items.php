<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Menu_submenu_items extends CI_Migration {

    public function up()
    {








		$sql = "TRUNCATE TABLE menu;";
    	$this->db->query($sql);
		$sql = "TRUNCATE TABLE submenu;";
    	$this->db->query($sql);
		$sql = "ALTER TABLE menu AUTO_INCREMENT = 1;";
    	$this->db->query($sql);
		$sql = "ALTER TABLE submenu AUTO_INCREMENT = 1;";
    	$this->db->query($sql);
		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (1, 'User Management', NULL, 'fa-user');";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (1, '1', 'User List', 'users', NULL);";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (2, '1', 'Group List', 'group', NULL);";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (3, '1', 'Privilege List', 'previlege', NULL);";
    	$this->db->query($sql);

		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (2, 'Site Setting', '', 'fa-cogs');";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (4, '2', 'Menu List', 'menu', NULL);";
    	$this->db->query($sql);

		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (3, 'Sync Master Data', '', 'fa-database');";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (5, '3', 'User', 'sync/user', NULL);";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (6, '3', 'Category', 'sync/category', NULL);";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (7, '3', 'Course', 'sync/course', NULL);";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (8, '3', 'Class', 'sync/classlearning', NULL);";
    	$this->db->query($sql);


		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (4, 'Sync CDS', '', 'fa-book');";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (9, '4', 'Course', 'sync/cdscourse', NULL);";
    	$this->db->query($sql);

		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (5, 'Sync LMS', '', 'fa-chalkboard-teacher');";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (10, '5', 'Course', 'sync/lmscourse', NULL);";
    	$this->db->query($sql);

		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (6, 'OCW', '', 'fa-home');";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (11, '6', 'Banner', 'be/landing/banner', NULL);";
    	$this->db->query($sql);
        $sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (12, '6', 'News', 'be/landing/News', NULL);";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}