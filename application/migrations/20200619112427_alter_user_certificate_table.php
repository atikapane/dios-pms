<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_user_certificate_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `certificate` ADD `user_id` int NOT NULL AFTER `id`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `certificate` DROP  COLUMN `user_id`;";
        $this->db->query($sql);
    }
}