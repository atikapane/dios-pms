<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_User_sync_history extends CI_Migration {

    public function up()
    {
    	$sql = "CREATE TABLE `sync_user_history` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `user_id` int(11) NOT NULL,
		  `date` datetime NOT NULL DEFAULT current_timestamp(),
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}