<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Category_ocw extends CI_Migration
{

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (26, '6', 'Category', 'be/ocw/Category', NULL)";
        $this->db->query($sql);

        $sql = "INSERT INTO `previleges` (`previlege_id`, `submenu_id`, `groupid`, `create`, `update`, `delete`, `read`, `upload`, `download`) VALUES ('23', '26', '1', '1', '1', '1', '1', '1', '1');";
        $this->db->query($sql);

        $sql = "DROP TABLE IF EXISTS ocw_category";
        $this->db->query($sql);

        $sql = "CREATE TABLE `ocw_category` (
                `ocw_category_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `faculty_id` int(11) NOT NULL,
                `description` text DEFAULT NULL,
                `img_path` varchar(255) DEFAULT NULL,
                `created_by` varchar(20) DEFAULT NULL,
                `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
                `deleted_at` datetime DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->db->query($sql);

        $sql = "CREATE VIEW vw_ocw_category AS SELECT ocw_category.ocw_category_id, sync_category.category_name, ocw_category.faculty_id, ocw_category.description, ocw_category.img_path, ocw_category.created_by, ocw_category.updated_at, ocw_category.deleted_at FROM ocw_category JOIN sync_category ON ocw_category.faculty_id = sync_category.category_id WHERE sync_category.category_type = 'FACULTY'";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}