<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_log_setting_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (1),(2),(5),(8),(11),(14),(17),(20),(23),(26),(29),(32),(35),(36),(37),(38),(39),(40),(41),(42)";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` IN (1,2,5,8,11,14,17,20,23,26,29,32,35,36,37,38,39,40,41,42)";
        $this->db->query($sql);
    }
}