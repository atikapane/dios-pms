<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_content_landing_agreements_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `landing_agreements` CHANGE `content` `content` TEXT NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        
    }
}