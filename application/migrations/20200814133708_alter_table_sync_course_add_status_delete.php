<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_sync_course_add_status_delete extends CI_Migration {

    public function up()
    {
    	$sql = 'ALTER TABLE `sync_course` ADD COLUMN `is_deleted` tinyint(4) NOT NULL DEFAULT 0 AFTER `is_manual_insert`;';
    	$this->db->query($sql);
    }

    public function down()
    {
    }
}