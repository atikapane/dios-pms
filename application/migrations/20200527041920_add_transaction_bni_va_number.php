<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_transaction_bni_va_number extends CI_Migration {
    public function up()
    {
        $sql = "ALTER TABLE `transaction` ADD `bni_va_number` VARCHAR(20) NOT NULL AFTER `price`;";
        $this->db->query($sql);
    }

    public function down()
    {
		$sql = 'ALTER TABLE `transaction` DROP COLUMN `bni_va_number`;';
        $this->db->query($sql);
    }
}