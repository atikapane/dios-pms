<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Table_ocw_content extends CI_Migration {

    public function up()
    {
    	$sql = 'CREATE TABLE `ocw_course_content` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) DEFAULT NULL,
		  `section_id` int(11) DEFAULT NULL,
		  `content_id` int(11) DEFAULT NULL,
		  `content_name` varchar(100) DEFAULT NULL,
		  `content_desc` text DEFAULT NULL,
		  `content_url` varchar(100) DEFAULT NULL,
		  `content_json` text DEFAULT NULL,
		  `content_type` varchar(100) DEFAULT NULL,
		  `content_status` tinyint(3) DEFAULT 0,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`section_id`,`content_id`,`content_type`,`content_status`) USING BTREE
		)';
		$this->db->query($sql);
    }

    public function down()
    {

    }
}