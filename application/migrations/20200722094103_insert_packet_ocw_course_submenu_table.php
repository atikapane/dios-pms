<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_packet_ocw_course_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` ( `menuid`, `submenu_name`, `submenu_link`, `priority`) VALUES ( '10', 'Packet Course', 'be/ocw/packet', '1')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/ocw/packet'";
        $this->db->query($sql);
    }
}