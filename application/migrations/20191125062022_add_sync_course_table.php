<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_sync_course_table extends CI_Migration {

    public function up()
    {
		$sql = "DROP TABLE IF EXISTS sync_course";
    	$this->db->query($sql);
    	$sql = "CREATE TABLE `sync_course` (
			  `subject_id` int(11) NOT NULL,
			  `subject_code` varchar(255) DEFAULT NULL,
			  `subject_name` varchar(255) DEFAULT NULL,
			  `subject_type` varchar(255) DEFAULT NULL,
			  `subject_ppdu` varchar(50) DEFAULT NULL,
			  `credit` varchar(50) DEFAULT NULL,
			  `curriculum_year` varchar(4) DEFAULT NULL,
			  `sync_by` int(11) DEFAULT NULL,
			  `sync_status` varchar(50) DEFAULT NULL,
			  `sync_date` datetime DEFAULT NULL,
			  `flag_status` varchar(50) DEFAULT NULL,
			  `table_owner` varchar(50) DEFAULT NULL,
			  `table_id` int(11) DEFAULT NULL,
			  `category_id` int(11) DEFAULT NULL,
			  `studyprogramid` int(11) DEFAULT NULL,
			  `approve_status` int(11) DEFAULT NULL,
			  `approve_date` datetime DEFAULT NULL,
			  `notes` text DEFAULT NULL,
			  `approve_by` int(11) DEFAULT NULL,
			  `input_by` int(11) DEFAULT NULL,
			  `input_date` datetime DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
			";
		$this->db->query($sql);

		$sql = "DROP TABLE IF EXISTS sync_course_history";
    	$this->db->query($sql);

    	$sql = "CREATE TABLE `sync_course_history` (
				  `id` int(11) NOT NULL,
				  `user_id` int(11) NOT NULL,
				  `date` datetime NOT NULL DEFAULT current_timestamp()
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		$this->db->query($sql);


		$sql = "ALTER TABLE `sync_course` ADD PRIMARY KEY (`subject_id`);";
    	$this->db->query($sql);

		$sql = "ALTER TABLE `sync_course_history` ADD PRIMARY KEY (`id`);";
    	$this->db->query($sql);


		$sql = "ALTER TABLE `sync_course_history` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
    	$this->db->query($sql);

		$sql = "ALTER TABLE `sync_course` ADD `is_manual_insert` TINYINT NOT NULL DEFAULT '0' AFTER `input_date`;";
    	$this->db->query($sql);



    }

    public function down()
    {

    }
}