<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_submenu_exam_schedule extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` ( `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`, `priority`) VALUES ( '5', 'Exam Schedule', 'be/lms/exam_schedule', NULL, '1')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/lms/exam_schedule'";
        $this->db->query($sql);

    }
}