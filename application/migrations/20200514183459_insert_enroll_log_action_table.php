<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_enroll_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Course Enroll', 'Create', 'USER_CREATE_COURSE_ENROLL', 'event log user create course enroll')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Course Enroll', 'Create', 'USER_CREATE_COURSE_ENROLL_SUCCESS', 'event log user create course enroll success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Course Enroll', 'Create', 'USER_CREATE_COURSE_ENROLL_FAILED', 'event log user create course enroll failed')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_CREATE_COURSE_ENROLL' OR `key` = 'USER_CREATE_COURSE_ENROLL_SUCCESS' OR `key` = 'USER_CREATE_COURSE_ENROLL_FAILED'";
        $this->db->query($sql);
    }
}