<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_log_setting_part_two_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(57),(58),(59),(60)";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` IN (43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60)";
        $this->db->query($sql);
    }
}