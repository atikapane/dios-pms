<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_landing_content_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `landing_content` (`id`,`title`, `priority`) VALUES (1,'Banner', 1)";
        $this->db->query($sql);

        $sql = "INSERT INTO `landing_content` (`id`,`title`, `priority`) VALUES (2,'Category Course', 2)";
        $this->db->query($sql);

        $sql = "INSERT INTO `landing_content` (`id`,`title`, `priority`) VALUES (3,'New Course', 3)";
        $this->db->query($sql);

        $sql = "INSERT INTO `landing_content` (`id`,`title`, `priority`) VALUES (4,'Recent Event', 4)";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}