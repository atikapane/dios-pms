<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_table_survey_question_options extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS survey_question_options";
        $sql = "CREATE TABLE `survey_question_options` ( `id` INT(11) NOT NULL AUTO_INCREMENT , 
        `question_id` INT(11) NOT NULL , 
        `question_answer` VARCHAR(100) NOT NULL , 
        `options` VARCHAR(255) NOT NULL , 
        PRIMARY KEY (`id`)) ENGINE = InnoDB";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}