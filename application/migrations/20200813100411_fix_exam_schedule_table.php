<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Fix_exam_schedule_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `lms_exam_schedule` CHANGE `exam_type` `exam_type` VARCHAR(255) NOT NULL";
        $this->db->query($sql);

        $sql = "ALTER TABLE `lms_exam_schedule` CHANGE `period` `period` VARCHAR(25) NOT NULL";
        $this->db->query($sql);

        $sql = "ALTER TABLE `lms_exam_schedule` CHANGE `exam_date` `exam_date` DATE NOT NULL";
        $this->db->query($sql);

        $sql = "ALTER TABLE `lms_exam_schedule` DROP  COLUMN `list_id_course_paralel`;";
        $this->db->query($sql);

        $sql = 'ALTER TABLE `lms_exam_schedule` ADD `status` int NOT NULL DEFAULT 1 COMMENT "1: queue, 2: on progress, 3: success, 4: failed" AFTER `coordinator_lecturer`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `lms_exam_schedule` DROP  COLUMN `status`;";
        $this->db->query($sql);

        $sql = 'ALTER TABLE `lms_exam_schedule` ADD `list_id_course_paralel` VARCHAR(50) NOT NULL AFTER `coordinator_lecturer`';
        $this->db->query($sql);

        $sql = "ALTER TABLE `lms_exam_schedule` CHANGE `exam_type` `exam_type` VARCHAR(25) NOT NULL";
        $this->db->query($sql);
        
        $sql = "ALTER TABLE `lms_exam_schedule` CHANGE `period` `period` int NOT NULL";
        $this->db->query($sql);

        $sql = "ALTER TABLE `lms_exam_schedule` CHANGE `exam_date` `exam_date` DATETIME NOT NULL";
        $this->db->query($sql);
    }
}