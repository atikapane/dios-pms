<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_new_api_external_log_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Get User', 'API_EXTERNAL_IGRACIAS_GET_USER', 'get spesific user');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Update Password User', 'API_EXTERNAL_IGRACIAS_UPDATE_PASSWORD_USER', 'update password user');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Unenroll User', 'API_EXTERNAL_MOOC_UNENROLL_USER', 'unenroll user from course');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Create Courses', 'API_EXTERNAL_MOOC_CREATE_COURSES', 'create course');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Delete Courses', 'API_EXTERNAL_MOOC_DELETE_COURSES', 'delete course');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Create Categories', 'API_EXTERNAL_MOOC_CREATE_CATEGORIES', 'create categories');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Update Categories', 'API_EXTERNAL_MOOC_UPDATE_CATEGORIES', 'update categories');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Delete Categories', 'API_EXTERNAL_MOOC_DELETE_CATEGORIES', 'delete categories');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_IGRACIAS_GET_USER' OR `key` = 'API_EXTERNAL_IGRACIAS_UPDATE_PASSWORD_USER' OR `key` = 'API_EXTERNAL_MOOC_UNENROLL_USER' OR `key` = 'API_EXTERNAL_MOOC_CREATE_COURSES' OR `key` = 'API_EXTERNAL_MOOC_DELETE_COURSES' OR `key` = 'API_EXTERNAL_MOOC_CREATE_CATEGORIES' OR `key` = 'API_EXTERNAL_MOOC_UPDATE_CATEGORIES' OR `key` = 'API_EXTERNAL_MOOC_DELETE_CATEGORIES'";
        $this->db->query($sql);

        // $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 77";
        // $this->db->query($sql);
    }
}