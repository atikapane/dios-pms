<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_course_change_log extends CI_Migration {

    public function up()
    {
		$sql = "DROP TABLE IF EXISTS course_change_log;";
    	$this->db->query($sql);


		$sql = "CREATE TABLE `course_change_log` ( `id` int(11) NOT NULL, `name` varchar(255) NOT NULL,`course_id` varchar(50) NOT NULL, `quiz` int(11) NOT NULL DEFAULT 0, `resource` int(11) NOT NULL DEFAULT 0, `hvp` int(11) NOT NULL DEFAULT 0, `label` int(11) DEFAULT 0, `page` int(11) NOT NULL DEFAULT 0, `url` int(11) NOT NULL DEFAULT 0, `forum` int(11) NOT NULL DEFAULT 0,`assignment` int(11) NOT NULL DEFAULT 0, `pull_date` datetime NOT NULL DEFAULT current_timestamp()) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
    	$this->db->query($sql);
    	
		$sql = "ALTER TABLE `course_change_log` ADD PRIMARY KEY (`id`);";
    	$this->db->query($sql);



    }

    public function down()
    {

    }
}