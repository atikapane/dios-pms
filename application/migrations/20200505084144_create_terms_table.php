<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_terms_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `landing_terms` (
            `id` int NOT NULL AUTO_INCREMENT,
            `content` VARCHAR(255) NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `landing_terms`";
        $this->db->query($sql);
    }
}