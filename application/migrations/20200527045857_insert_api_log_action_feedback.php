<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_log_action_feedback extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'FEEDBACK', 'list', 'API_FEEDBACK_LIST', 'get list feedback');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'FEEDBACK', 'detail', 'API_FEEDBACK_DETAIL', 'get feedback detail');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'FEEDBACK', 'create', 'API_FEEDBACK_CREATE', 'create feedback');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'FEEDBACK', 'update', 'API_FEEDBACK_UPDATE', 'update feedback');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'FEEDBACK', 'delete', 'API_FEEDBACK_DELETE', 'delete feedback');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '67', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '68', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '69', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '70', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '71', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_FEEDBACK_LIST' OR `key` = 'API_FEEDBACK_DETAIL' OR `key` = 'API_FEEDBACK_CREATE' OR `key` = 'API_FEEDBACK_UPDATE' OR `key` = 'API_FEEDBACK_DELETE'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 67 OR `log_action_id` = 68 OR `log_action_id` = 69 OR `log_action_id` = 70 OR `log_action_id` = 71 ";
        $this->db->query($sql);
    }
}