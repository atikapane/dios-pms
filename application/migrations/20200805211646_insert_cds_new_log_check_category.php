<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_cds_new_log_check_category extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Check Category', 'API_EXTERNAL_CDS_CHECK_CATEGORY', 'check category cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Check Course', 'API_EXTERNAL_CDS_CHECK_COURSE', 'check course cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Create Course', 'API_EXTERNAL_CDS_CREATE_COURSE', 'create course cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Update Course', 'API_EXTERNAL_CDS_UPDATE_COURSE', 'update course cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Delete Course', 'API_EXTERNAL_CDS_DELETE_COURSE', 'delete course cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_CHECK_CATEGORY'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_CHECK_COURSE'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_CREATE_COURSE'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_UPDATE_COURSE'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_DELETE_COURSE'";
        $this->db->query($sql);
    }
}