<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_ocw_course extends CI_Migration {

    public function up()
    {
    	$sql = "CREATE TABLE `ocw_course` ( `id` INT NOT NULL AUTO_INCREMENT , `ocw_name` VARCHAR(255) NOT NULL , `start_date` DATE NOT NULL , `end_date` DATE NOT NULL , `lecturer_photo` VARCHAR(255) NOT NULL , `lecturer_name` VARCHAR(255) NOT NULL , `lecturer_profile` TEXT NOT NULL ,`description` TEXT NOT NULL , `sync_course_id` INT NOT NULL , `created_by` INT NOT NULL , `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`)) ENGINE = InnoDB;";
    	$this->db->query($sql);

    }

    public function down()
    {

    }
}