<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_push_notif_token_log extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Push Notif', 'Save push notif token success', 'API_PUSH_NOTIF_TOKEN_POST', 'event log if saving push notif success');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '95', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Push Notif', 'Save push notif token failed', 'API_PUSH_NOTIF_TOKEN_POST_FAILED', 'event log if saving push notif failed');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '96', '1');";

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Push Notif', 'Delete push notif token success', 'API_DELETE_PUSH_NOTIF_TOKEN', 'event log if delete push notif success');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '97', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Push Notif', 'Delete push notif token failed', 'API_DELETE_PUSH_NOTIF_TOKEN_FAILED', 'event log if delete push notif failed');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '98', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_PUSH_NOTIF_TOKEN_POST' or `key` = 'API_PUSH_NOTIF_TOKEN_POST_FAILED' or `key` = 'API_DELETE_PUSH_NOTIF_TOKEN' or `key` = 'API_DELETE_PUSH_NOTIF_TOKEN_FAILED'" ;
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 95 or `log_action_id` = 96 or `log_action_id` = 97 or `log_action_id` = 98";
        $this->db->query($sql);
    }
}