<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_log_setting_subbmenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (2, 'Log Setting', 'be/site_setting/log')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Log Setting'";
        $this->db->query($sql);
    }
}