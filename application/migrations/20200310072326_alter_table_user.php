<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_user extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `user`
			MODIFY COLUMN `directorate`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' AFTER `email`,
			MODIFY COLUMN `unit`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' AFTER `directorate`,
			MODIFY COLUMN `structural_functional_position`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' AFTER `lecturercode`,
			MODIFY COLUMN `structural_academic_position`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' AFTER `structural_functional_position`,
			ADD COLUMN `c_directorate_id`  int(20) NULL DEFAULT 0 AFTER `structural_academic_position`,
			ADD COLUMN `c_unit_id`  int(20) NULL DEFAULT 0 AFTER `c_directorate_id`;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}