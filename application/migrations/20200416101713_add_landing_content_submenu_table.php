<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_landing_content_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (6, 'Content', 'be/landing/content')";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}