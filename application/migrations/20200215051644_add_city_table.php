<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_city_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `city` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `country_id` int(11) NOT NULL,
		  `name` VARCHAR(255) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE
		)";
        $this->db->query($sql);

        $sql = "INSERT INTO `city` (`country_id`,`name`) VALUES (1, 'Bandung');";
        $this->db->query($sql);
        $sql = "INSERT INTO `city` (`country_id`,`name`) VALUES (1, 'Jakarta Selatan');";
        $this->db->query($sql);
        $sql = "INSERT INTO `city` (`country_id`,`name`) VALUES (1, 'Jakarta Timur');";
        $this->db->query($sql);
        $sql = "INSERT INTO `city` (`country_id`,`name`) VALUES (1, 'Jakarta Barat');";
        $this->db->query($sql);
        $sql = "INSERT INTO `city` (`country_id`,`name`) VALUES (1, 'Jakarta Utara');";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}