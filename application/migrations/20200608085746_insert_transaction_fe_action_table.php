<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_transaction_fe_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Add Cart', 'USER_FRONTEND_ADD_CART', 'Add course to cart');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Add Cart', 'USER_FRONTEND_ADD_CART_SUCCESS', 'Add course to cart success');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Add Cart', 'USER_FRONTEND_ADD_CART_FAILED', 'Add course to cart failed');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Remove Cart', 'USER_FRONTEND_REMOVE_CART', 'remove course from cart');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Remove Cart', 'USER_FRONTEND_REMOVE_CART_SUCCESS', 'remove course from cart success');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Remove Cart', 'USER_FRONTEND_REMOVE_CART_FAILED', 'remove course from cart failed');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Make Tx', 'USER_FRONTEND_MAKE_TX', 'create transaction');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Make Tx', 'USER_FRONTEND_MAKE_TX_SUCCESS', 'create transaction success');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Make Tx', 'USER_FRONTEND_MAKE_TX_FAILED', 'create transaction failed');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Payment', 'USER_FRONTEND_PAYMENT', 'payment process');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Payment', 'USER_FRONTEND_PAYMENT_SUCCESS', 'payment process success');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Payment', 'USER_FRONTEND_PAYMENT_FAILED', 'payment process failed');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES ('97');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES ('100');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES ('103');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES ('106');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_ADD_CART' OR `key` = 'USER_FRONTEND_ADD_CART_SUCCESS' OR `key` = 'USER_FRONTEND_ADD_CART_FAILED'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_REMOVE_CART' OR `key` = 'USER_FRONTEND_REMOVE_CART_SUCCESS' OR `key` = 'USER_FRONTEND_REMOVE_CART_FAILED'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_MAKE_TX' OR `key` = 'USER_FRONTEND_MAKE_TX_SUCCESS' OR `key` = 'USER_FRONTEND_MAKE_TX_FAILED'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_PAYMENT' OR `key` = 'USER_FRONTEND_PAYMENT_SUCCESS' OR `key` = 'USER_FRONTEND_PAYMENT_FAILED'";
        $this->db->query($sql);
        
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 97 OR `log_action_id` = 100 OR `log_action_id` = 103 OR `log_action_id` = 106";
        $this->db->query($sql);
    }
}