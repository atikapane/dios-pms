<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_log_criteria extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `sync_course_development_history`
			ADD COLUMN `log`  text NULL AFTER `date`;";
		$this->db->query($sql);

		$sql = "ALTER TABLE `criteria_review`
			ADD COLUMN `weight`  int(11) NOT NULL DEFAULT 0 AFTER `rubric`;";
		$this->db->query($sql);

		$sql = "ALTER TABLE `course_review`
			MODIFY COLUMN `total_score`  double(11,1) NOT NULL AFTER `reviewer_id`;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}