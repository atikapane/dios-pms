<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_program_owner_id_ocw_course_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_course` DROP  COLUMN `program_owner_id`;";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = 'ALTER TABLE `ocw_course` ADD `program_owner_id` int NOT NULL AFTER `banner_image`';
        $this->db->query($sql);
    }
}