<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_view_ocw_course_joined extends CI_Migration {

    public function up()
    {
    	
    	$sql = "CREATE VIEW ocw_course_joined AS SELECT ocw_course.*,course.subject_name,course.subject_code, study_program.category_id as study_program_id, study_program.category_name as study_program_name, faculty.category_id as faculty_id, faculty.category_name as faculty_name,user.fullname,user.employeeid FROM `ocw_course` JOIN sync_course course ON course.subject_id = ocw_course.sync_course_id JOIN sync_category study_program ON study_program.category_id = course.category_id JOIN sync_category faculty ON faculty.category_id = study_program.category_parent_id JOIN user ON user.id = ocw_course.created_by WHERE 1";
    	$this->db->query($sql);

    }

    public function down()
    {

    }
}