<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_user_lecturer extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `user` CHANGE COLUMN `lecturecode` `lecturercode`  char(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' AFTER `unit`;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}