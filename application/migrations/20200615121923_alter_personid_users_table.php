<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_personid_users_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `users` ADD `personid` VARCHAR(25) NOT NULL AFTER `oauth_uid`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `users` DROP  COLUMN `personid`;";
        $this->db->query($sql);
    }
}