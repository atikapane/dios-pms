<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Ocw_course_content_add_public_url extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_course_content` ADD `public_url` TEXT NULL DEFAULT NULL AFTER `content_status`";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}