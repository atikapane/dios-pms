<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_lms_exam_schedule_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `lms_exam_schedule` ( 
            `id` INT(11) NOT NULL AUTO_INCREMENT ,
            `exam_type` VARCHAR(25) NOT NULL , 
            `total_exam_participant` INT(11) NOT NULL ,
            `period` VARCHAR(25) NOT NULL , 
            `exam_date` DATE NOT NULL ,
            `exam_start_time` TIME NOT NULL , 
            `exam_end_time` TIME NOT NULL , 
            `id_course_master` VARCHAR(25) NOT NULL , 
            PRIMARY KEY (`id`)
            ) ENGINE = InnoDB;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `lms_list_id_course_paralel` ( 
            `id` INT(25) NOT NULL AUTO_INCREMENT , 
            `id_lms_exam_schedule` INT(25) NOT NULL , 
            `id_course_paralel` INT(25) NOT NULL , 
            PRIMARY KEY (`id`)
            ) ENGINE = InnoDB;";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS lms_exam_schedule";
        $this->db->query($sql);

        $sql = "DROP TABLE IF EXISTS lms_list_id_course_paralel";
        $this->db->query($sql);
    }
}