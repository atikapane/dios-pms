<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Revise_menu_submenu extends CI_Migration {

    public function up()
    {
        // change menu OCW to Home Page
        $sql = "UPDATE `menu` SET `menu_name`='Home Page' WHERE `menu_id`=6";
        $this->db->query($sql);

        // add Open Course Ware menu
        $sql = "INSERT INTO `menu` (`menu_name`, `menu_icon`) VALUES ('Open Course Ware', 'fa-book')";
        $this->db->query($sql);

        // move submenu Showcase Course, Category, Showcase Publish to Open Course Ware menu
        $sql = "UPDATE `submenu` SET `menuid`=10 WHERE `submenu_id` IN (25, 26, 27)";
        $this->db->query($sql);

        // rename submenu
        $sql = "UPDATE `submenu` SET `submenu_name`='Course' WHERE `submenu_id`=25";
        $this->db->query($sql);
        
        $sql = "UPDATE `submenu` SET `submenu_name`='Course Category' WHERE `submenu_id`=26";
        $this->db->query($sql);

        $sql = "UPDATE `submenu` SET `submenu_name`='Course Moderation' WHERE `submenu_id`=27";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}