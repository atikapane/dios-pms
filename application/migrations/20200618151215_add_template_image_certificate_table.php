<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_template_image_certificate_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `certificate` ADD `template_image` VARCHAR(255) NOT NULL AFTER `data`;';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = 'ALTER TABLE `transaction` DROP `template_image`;';
        $this->db->query($sql);
    }
}