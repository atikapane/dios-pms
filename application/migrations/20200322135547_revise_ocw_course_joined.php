<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Revise_ocw_course_joined2 extends CI_Migration {

    public function up()
    {
 		$sql = "DROP  VIEW  ocw_course_joined;";
        $this->db->query($sql);

        $sql = "CREATE VIEW ocw_course_joined AS   SELECT ocw_course.*,course.subject_name,course.subject_code, study_program.category_id as study_program_id, study_program.category_name as study_program_name, faculty.category_id as faculty_id, faculty.category_name as faculty_name,user.fullname,user.employeeid,ocw_category.ocw_category_id,ocw_category.study_program_name as study_program_store_name,ocw_category.name as faculty_store_name FROM `ocw_course` 
			 JOIN sync_course course ON course.subject_id = ocw_course.sync_course_id 
			 JOIN sync_category study_program ON study_program.category_id = course.category_id 
			 JOIN sync_category faculty ON faculty.category_id = study_program.category_parent_id 
			 JOIN ocw_category ON ocw_category.faculty_id = faculty.category_id AND ocw_category.program_id = study_program.category_id
			 JOIN user ON user.id = ocw_course.created_by WHERE 1";

        $this->db->query($sql);
    }

    public function down()
    {

    }
}