<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_log_action_login extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Authentication', 'Login', 'API_LOGIN', 'event log api login');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Authentication', 'Login', 'API_LOGIN_SUCCESS', 'event log api login success');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Authentication', 'Login', 'API_LOGIN_FAILED', 'event log api login failed');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '86', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '87', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '88', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_LOGIN' OR `key` = 'API_LOGIN_SUCCESS'OR `key` = 'API_LOGIN_FAILED'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 86 OR `log_action_id` = 87 OR `log_action_id` = 88";
        $this->db->query($sql);
    }
}