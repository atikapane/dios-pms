<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_school_year extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
        $sql = "SELECT menuid FROM submenu WHERE submenu_link = 'be/site_setting/schoolyear'";
        $menu_id = $this->db->query($sql)->row()->menuid;

        $sql = "DELETE FROM submenu WHERE submenu_link = 'be/site_setting/schoolyear'";
        $this->db->query($sql);
    }
}