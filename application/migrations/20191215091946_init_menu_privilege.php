<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Init_menu_privilege extends CI_Migration {

    public function up()
    {
    	$sql = "UPDATE submenu SET submenu_name = 'User' WHERE submenu_id=1";
        $this->db->query($sql);
    	$sql = "UPDATE submenu SET submenu_name = 'Group' WHERE submenu_id=2";
        $this->db->query($sql);
    	$sql = "UPDATE submenu SET submenu_name = 'Privilege' WHERE submenu_id=3";
        $this->db->query($sql);

    	$sql = "TRUNCATE `groups`;";
        $this->db->query($sql);
    	$sql = "TRUNCATE `previleges`;";
        $this->db->query($sql);
    	$sql = "TRUNCATE `roles`;";
        $this->db->query($sql);

    	$sql = "INSERT INTO `groups` (`group_id`, `group_name`) VALUES ('1', 'Super Administrator');";
        $this->db->query($sql);
    	$sql = "INSERT INTO `roles` (`roles_id`, `groupid`, `userid`) VALUES ('1', '1', '1');";
        $this->db->query($sql);
        for ($i=1; $i < 19 ; $i++) { 
    		$sql = "INSERT INTO `previleges` (`previlege_id`, `submenu_id`, `groupid`, `create`, `update`, `delete`, `read`, `upload`, `download`) VALUES ('$i', '$i', '1', '1', '1', '1', '1', '1', '1');";
       	 	$this->db->query($sql);
        }
    }

    public function down()
    {

    }
}