<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_report_menu extends CI_Migration {

    public function up()
    {

		$sql = "INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_link`, `menu_icon`) VALUES (7, 'Report', '', 'fa-chart-line');";
    	$this->db->query($sql);
		$sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (14, '7', 'Beban Pengembangan Course', 'be/report/Course_development', NULL);";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}