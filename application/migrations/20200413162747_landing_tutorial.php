<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Landing_tutorial extends CI_Migration {

    public function up()
    {
    	$sql = "INSERT INTO `submenu` (`submenu_id`,`menuid`, `submenu_name`, `submenu_link`) VALUES (96, 6, 'Tutorial', 'be/landing/Tutorial')";
    	$this->db->query($sql);

    	$sql = "INSERT INTO `previleges` (`submenu_id`, `groupid`, `create`, `update`, `delete`, `read`, `upload`, `download`) VALUES (96, 1, 1, 1, 1, 1, 1, 1)";
    	$this->db->query($sql);

    	$sql = "CREATE TABLE `landing_tutorial` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(100) NOT NULL,
		  `type` varchar(40) NOT NULL,
		  `file_path` text NOT NULL,
		  `description` text DEFAULT NULL,
		  `file_status` tinyint(3) NOT NULL DEFAULT 1,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`type`,`file_status`) USING BTREE
		)";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}