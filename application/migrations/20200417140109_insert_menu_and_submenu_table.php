<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_menu_and_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `menu` (`menu_name`, `menu_icon`) VALUES ('Log Management', 'fa-clipboard-list')";
        $this->db->query($sql);

        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (11, 'User Log', 'be/log/user')";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}