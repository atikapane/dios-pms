<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_sync_course_teaching_history extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `sync_lms_course_teaching_history`
    		MODIFY COLUMN `date`  datetime NULL DEFAULT NULL AFTER `course_id`,
			ADD COLUMN `log`  text DEFAULT NULL AFTER `date`;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}