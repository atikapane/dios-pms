<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_my_course_log extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'My Course', 'Course exist', 'API_MY_COURSE', 'event log if filtered course exist');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '101', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'My Course not found', 'Course not found', 'API_MY_COURSE_FAILED', 'event log if filtered course not found');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '102', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_MY_COURSE' or `key` = 'API_MY_COURSE_FAILED' " ;
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 101 or `log_action_id` = 102";
        $this->db->query($sql);
    }
}