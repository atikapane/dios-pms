<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_certificat_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (10, 'Certificate', 'be/ocw/certificate')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/ocw/certificate'";
        $this->db->query($sql);
    }
}