<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_mdfive_hash_user_grade_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `user_grade` ADD `md5_hash` VARCHAR(255) NULL AFTER `certificate_path`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user_grade` DROP  COLUMN `md5_hash`";
        $this->db->query($sql);
    }
}