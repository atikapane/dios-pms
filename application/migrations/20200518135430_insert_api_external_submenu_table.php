<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_external_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (11, 'External Api Log', 'be/log/external')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'External Api Log'";
        $this->db->query($sql);
    }
}