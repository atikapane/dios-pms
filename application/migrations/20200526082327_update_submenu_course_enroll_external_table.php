<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_submenu_course_enroll_external_table extends CI_Migration {

    public function up()
    {
        $sql = "UPDATE `submenu` SET `submenu_name` = 'Course Enroll External' WHERE `submenu_name` = 'Course Enroll'";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "UPDATE `submenu` SET `submenu_name` = 'Course Enroll' WHERE `submenu_name` = 'Course Enroll External'";
        $this->db->query($sql);
    }
}