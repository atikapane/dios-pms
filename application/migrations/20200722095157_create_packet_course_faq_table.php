<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_packet_course_faq_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `packet_ocw_course_faq` (
            `id` int NOT NULL AUTO_INCREMENT,
            `packet_ocw_course_id` int NOT NULL,
            `question` VARCHAR(255) NOT NULL,
            `answer` TEXT NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS packet_ocw_course_faq";
        $this->db->query($sql);
    }
}