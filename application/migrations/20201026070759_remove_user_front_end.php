<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_user_front_end extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
        $sql = "SELECT menuid FROM submenu WHERE submenu_link = 'be/user_management/frontend'";
        $menu_id = $this->db->query($sql)->row()->menuid;

        $sql = "DELETE FROM submenu WHERE submenu_link = 'be/user_management/frontend'";
        $this->db->query($sql);
    }
}