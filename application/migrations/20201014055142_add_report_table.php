<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_report_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `report` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `task` int NOT NULL ,
            `project` int NOT NULL,
            `type` varchar(10) NOT NULL,
            `task_name`   varchar(100) NOT NULL,
            `date`  date NOT NULL,
            `start_time`  time NOT NULL,
            `end_time`  time NOT NULL,
            `hours` int NOT NULL,
            `pct_of_task_completed` int NOT NULL,
            `notes` varchar(225) NOT NULL,
            PRIMARY KEY (`id`),
            FOREIGN KEY (project) REFERENCES Project(`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS report";
        $this->db->query($sql);
    }
}