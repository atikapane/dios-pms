<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Download_menu extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (13, '6', 'Download', 'be/landing/Download', NULL);";
        $this->db->query($sql);
        $sql = "CREATE TABLE `landing_download` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(100) NOT NULL,
		  `file_path` varchar(100) NOT NULL,
		  `description` text DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}