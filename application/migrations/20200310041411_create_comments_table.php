<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Migration_Create_comments_table extends CI_Migration
{

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS comments";
        $this->db->query($sql);
        $sql = "CREATE TABLE `comments` (
            `id` int NOT NULL AUTO_INCREMENT,
            `user_id` int NOT NULL,
            `course_id` int NOT NULL,
            `comment` varchar(255) NOT NULL,
            `parent_id` int NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
		    UNIQUE KEY `unique` (`id`) USING BTREE
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
          $this->db->query($sql);
    }

    public function down()
    {
    }
}
