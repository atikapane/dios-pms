<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Rename_table_groups extends CI_Migration {

    public function up()
    {
        $sql = "RENAME TABLE `groups` TO `group`";
        $this->db->query($sql);

        $sql = "DROP TABLE `users`";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}