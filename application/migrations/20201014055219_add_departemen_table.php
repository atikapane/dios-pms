<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_departemen_table extends CI_Migration {

    public function up()
    {
        $sql = 'CREATE TABLE departemen (
            departemen_id INT NOT NULL AUTO_INCREMENT,
            departemen_name varchar(100) NOT null,
            constraint pk_departemen primary key (departemen_id)
            );';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = 'DROP TABLE `departemen`';
        $this->db->query($sql);
    }
}