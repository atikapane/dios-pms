<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_template_ocw_course_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `ocw_course` ADD `template_id` int NOT NULL AFTER `certification`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `ocw_course` DROP  COLUMN `template_id`;";
        $this->db->query($sql);
    }
}