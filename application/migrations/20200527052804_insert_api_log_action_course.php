<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_log_action_course extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COURSE', 'list', 'API_COURSE_LIST', 'get list course');"; 
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COURSE', 'lms', 'API_COURSE_LMS', 'get lms course');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'COURSE', 'detail', 'API_COURSE_DETAIL', 'get detail course');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '77', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '78', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '79', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_COURSE_LIST' OR `key` = 'API_COURSE_LMS' OR `key` = 'API_COURSE_DETAIL'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 77 OR `log_action_id` = 78 OR `log_action_id` = 79";
        $this->db->query($sql);
    }
}