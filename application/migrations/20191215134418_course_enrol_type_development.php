<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Course_enrol_type_development extends CI_Migration {

    public function up()
    {
    	$sql = "CREATE TABLE `course_development` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `year` int(11) NOT NULL,
		  `quarter` tinyint(3) NOT NULL,
		  `subject_id` int(11) NOT NULL,
		  `created_date` datetime NOT NULL,
		  `created_by` char(20) NOT NULL,
		  `updated_date` datetime DEFAULT NULL,
		  `updated_by` char(20) NOT NULL,
		  `status_review` tinyint(3) NOT NULL DEFAULT 0,
		  `status_approve` tinyint(3) NOT NULL DEFAULT 0,
		  `date_approve` date DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`year`,`quarter`,`subject_id`,`status_review`,`status_approve`) USING BTREE
		)";
		$this->db->query($sql);

		$sql = "CREATE TABLE `enrol_development` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) NOT NULL,
		  `employeeid` char(20) NOT NULL,
		  `type_id` int(11) NOT NULL,
		  `is_leader` tinyint(3) NOT NULL DEFAULT 0,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`employeeid`,`type_id`) USING BTREE
		)";
		$this->db->query($sql);

		$sql = "CREATE TABLE `enrol_type` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `enrol_id` int(11) NOT NULL,
		  `enrol_name` varchar(40) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`enrol_id`) USING BTREE
		)";
		$this->db->query($sql);

		$sql = "INSERT INTO `enrol_type` VALUES ('1', '1', 'Student')";
		$this->db->query($sql);
		$sql = "INSERT INTO `enrol_type` VALUES ('2', '2', 'Editing Teacher')";
		$this->db->query($sql);
		$sql = "INSERT INTO `enrol_type` VALUES ('3', '3', 'Non Editing Teacher')";
		$this->db->query($sql);
		$sql = "INSERT INTO `enrol_type` VALUES ('4', '9', 'Reviewer')";
		$this->db->query($sql);

		$sql = 'ALTER TABLE `user`
			DROP INDEX `index` ,
			ADD INDEX `index` (`institution`, `empnum`, `employeeid`, `email`, `lecturercode`, `fullname`) USING BTREE ;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `sync_lms_course_teaching_history` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `employeeid` char(20) NOT NULL,
		  `course_id` int(11) NOT NULL,
		  `date` datetime NOT NULL DEFAULT current_timestamp(),
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`employeeid`,`course_id`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = 'ALTER TABLE `sync_lms_course` ADD COLUMN `last_sync`  datetime NULL AFTER `employee_name`;';
		$this->db->query($sql);
    }

    public function down()
    {

    }
}