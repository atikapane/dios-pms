<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_log_setting_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `log_setting` (
            `id` int NOT NULL AUTO_INCREMENT,
            `log_action_id` int NOT NULL,
            `is_log` boolean NOT NULL DEFAULT TRUE,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
          $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `log_setting`";
        $this->db->query($sql);
    }
}