<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_billing_sales extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
        $sql = "SELECT menuid FROM submenu WHERE submenu_link = 'be/billing/sales'";
        $menu_id = $this->db->query($sql)->row()->menuid;

        $sql = "DELETE FROM submenu WHERE submenu_link = 'be/billing/sales'";
        $this->db->query($sql);

        $sql = "DELETE FROM menu WHERE menu_id = " . $menu_id;
        $this->db->query($sql);
    }
}