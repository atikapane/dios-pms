<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_about_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (6, 'About Us', 'be/landing/about')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'About Us'";
        $this->db->query($sql);
    }
}