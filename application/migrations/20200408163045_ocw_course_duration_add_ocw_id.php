<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Ocw_course_duration_add_ocw_id extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE `ocw_course_duration` ADD `ocw_course_id` INT NOT NULL AFTER `id`;";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}