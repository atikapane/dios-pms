<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_table_survey_question extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS survey_question";
        $sql = "CREATE TABLE `survey_question` ( `id` INT(11) NOT NULL AUTO_INCREMENT , 
        `survey_id` INT(11) NOT NULL , 
        `question` VARCHAR(250) NOT NULL , 
        `type` INT(1) NOT NULL , 
        PRIMARY KEY (`id`)) ENGINE = InnoDB";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}