<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_table_curriculum extends CI_Migration {

    public function up()
    {
    	$sql = "DROP TABLE IF EXISTS `curriculum`;";
        $this->db->query($sql);

        $sql = 'CREATE TABLE `curriculum` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `year` char(4) NOT NULL,
		  `description` varchar(200) DEFAULT NULL,
		  `status` tinyint(3) NOT NULL DEFAULT 1,
		  PRIMARY KEY (`id`)
		)';
        $this->db->query($sql);

        $sql = "INSERT INTO `curriculum` VALUES ('1', '2016', 'Kurikulum Tahun 2016', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `curriculum` VALUES ('2', '2020', 'Kurikulum Tahun 2020', '1');";
        $this->db->query($sql);

        $this->db->select("menu_id");
        $this->db->from("menu");
        $this->db->where("menu_name = 'Site Setting'");
		$this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if (!empty($row)){
        	$parent_id = $row["menu_id"];
        	$sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (" . $parent_id . ", 'Curriculum', 'be/site_setting/curriculum');";
        	$this->db->query($sql);
        }
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `curriculum`;";
        $this->db->query($sql);

        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/site_setting/curriculum'";
        $this->db->query($sql);
    }
}