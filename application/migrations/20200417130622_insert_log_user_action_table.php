<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_log_user_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_LOGIN', 'event log user backend')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`key`, `description`) VALUES ('USER_LOGOUT', 'event logout user backend')";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}