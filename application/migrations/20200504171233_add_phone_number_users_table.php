<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_phone_number_users_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `users` ADD `phone_number` VARCHAR(255) NOT NULL AFTER `dateBirth`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `users` DROP COLUMN `phone_number`";
        $this->db->query($sql);
    }
}