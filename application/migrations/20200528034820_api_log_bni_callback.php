<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Api_log_bni_callback extends CI_Migration {
 	public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Billing', 'BNI Callback', 'API_BNI_CALLBACK', 'event log api BNI callback');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '89', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_BNI_CALLBACK'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 89";
        $this->db->query($sql);
    }
}