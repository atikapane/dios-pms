<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_lms_exam_schedule_task_table extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS `lms_exam_schedule_task`;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `lms_exam_schedule_task` (
            `id` int NOT NULL AUTO_INCREMENT,
            `lms_exam_schedule_id` int NOT NULL,
            `master_course_id` int NOT NULL,
            `paralel_course_id` int NOT NULL,
            `exam_type` VARCHAR(255) NOT NULL,
            `status` int NOT NULL DEFAULT 1 COMMENT '1: queue, 2: on progress, 3: success, 4: failed',
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS lms_exam_schedule_task";
        $this->db->query($sql);
    }
}