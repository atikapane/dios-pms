<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_transaction_price extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `transaction` ADD `price` BIGINT NOT NULL AFTER `user_phone`;";
        $this->db->query($sql);
    }

    public function down()
    {
		$sql = 'ALTER TABLE `transaction` DROP COLUMN `price`;';
        $this->db->query($sql);
    }
}