<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_start_end_date_trasaction_item_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `transaction_item` ADD `start_date` DATE NOT NULL AFTER `course_description`";
        $this->db->query($sql);
        $sql = "ALTER TABLE `transaction_item` ADD `end_date` DATE NOT NULL AFTER `start_date`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `transaction_item` DROP COLUMN `end_date`";
        $this->db->query($sql);
        $sql = "ALTER TABLE `transaction_item` DROP COLUMN `start_date`";
        $this->db->query($sql);
    }
}