<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_exam_verification_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` ( `menuid`, `submenu_name`, `submenu_link`, `priority`) VALUES ( '5', 'Exam Topic & Activity Verification', 'be/lms/exam_topic', '1')";
        $this->db->query($sql);
        $sql = "INSERT INTO `submenu` ( `menuid`, `submenu_name`, `submenu_link`, `priority`) VALUES ( '5', 'Exam Readiness Verification', 'be/lms/exam_readiness', '1')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/lms/exam_readiness'";
        $this->db->query($sql);
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/lms/exam_topic'";
        $this->db->query($sql);
    }
}