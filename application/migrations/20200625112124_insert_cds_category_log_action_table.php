<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_cds_category_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Create Category', 'API_EXTERNAL_IGRACIAS_CREATE_CATEGORY', 'create category in igracias');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Update Category', 'API_EXTERNAL_IGRACIAS_UPDATE_CATEGORY', 'udpate category in igracias');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('IGRACIAS', 'Delete Category', 'API_EXTERNAL_IGRACIAS_DELETE_CATEGORY', 'delete category in igracias');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_IGRACIAS_CREATE_CATEGORY' OR `key` = 'API_EXTERNAL_IGRACIAS_UPDATE_CATEGORY' OR `key` = 'API_EXTERNAL_IGRACIAS_DELETE_CATEGORY'";
        $this->db->query($sql);
    }
}