<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_news extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE news MODIFY COLUMN id int(11) NOT NULL auto_increment;";
        $this->db->query($sql);
    
        $sql = "ALTER TABLE news MODIFY COLUMN title varchar(255) NOT NULL;";
        $this->db->query($sql);

        $sql = "ALTER TABLE news ADD slug VARCHAR(255) AFTER thumbnail;";
        $this->db->query($sql);

        $sql = "ALTER TABLE news ADD related VARCHAR(255) AFTER slug;";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}