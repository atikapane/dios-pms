<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_log_action_password extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'FORGOT PASSWORD', 'send', 'API_FORGOT_PASSWORD_SEND', 'send email forgot password');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'FORGOT PASSWORD', 'reset', 'API_FORGOT_PASSWORD_RESET', 'reset forgot password');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '81', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '82', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_FORGOT_PASSWORD_SEND' OR `key` = 'API_FORGOT_PASSWORD_RESET'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 81 OR `log_action_id` = 82 ";
        $this->db->query($sql);
    }
}