<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_priority_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `submenu` ADD `priority` int NOT NULL AFTER `submenu_icon`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `submenu` DROP COLUMN `priority`";
        $this->db->query($sql);
    }
}