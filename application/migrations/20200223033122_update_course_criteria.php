<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_course_criteria extends CI_Migration {

    public function up()
    {
    	$sql = 'ALTER TABLE `course_review`
		MODIFY COLUMN `total_score`  double(11,2) NOT NULL AFTER `reviewer_id`;';
		$this->db->query($sql);

		$sql = 'DROP TABLE IF EXISTS `criteria_review`;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `criteria_review` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(100) NOT NULL,
		  `rubric` text DEFAULT NULL,
		  `weight` double(11,2) NOT NULL DEFAULT 0.00,
		  `option` int(11) NOT NULL DEFAULT 1,
		  `criteria_status` tinyint(3) NOT NULL DEFAULT 1,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`title`,`weight`,`option`,`criteria_status`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('1', 'Jumlah Profil Mata Kuliah', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('2', 'Kuantitas Deskripsi &  Profil Mata kuliah', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('3', 'Kuantitas Pokok bahasan', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('4', 'Kuantitas Panduan Pembelajaran', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('5', 'Kuantitas lecture Notes', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('6', 'Kuantitas Tugas', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('7', 'kuantitas konten forum diskusi', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('8', 'Kuantitas Quiz Review', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('9', 'Kuantitas Soal Per Quiz', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('10', 'Kuantitas Link eksternal', '<ul><li>0 = Tidak Sesuai dengan Proposal</li><li>1 = Sesuai dengan Proposal</li></ul>', '6.25', '1', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('11', 'Pokok bahasan akurat, tidak bias, dan bebas dari kesalahan', '<ul><li>1 = Sangat bias, tidak akurat,masih banyak kesalahan</li><li>2 = Masih bias, tidak akurat, sedikit kesalahan</li><li>3 = Tidak bias, kurang akurat, masih ada kesalahan</li><li>4 = Tidak bias, cukup akurat</li><li>5 = Tidak bias, sangat akurat, bebas dari kesalahan</li></ul>', '6.25', '5', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('12', 'Struktur materi jelas, pokok bahasan dan sub pokok bahasannya jelas, masing-masing ada pengantar, pe', '<ul><li>1 = Struktur materi tidak jelas, pokok bahasan dan sub pokok bahasan tidak jelas, tidak ada pengantar, tidak ada penjelasan &amp; tidak ada ringkasan</li><li>2 = Struktur materi cukup jelas, pokok bahasan dan sub pokok bahasan cukup jelas, tidak ada pengantar, tidak ada penjelasan &amp; tidak ada ringkasan</li><li>3 = Struktur materi cukup jelas, pokok bahasan dan sub pokok bahasan tidak jelas, &nbsp;pengantar, penjelasan &amp; &nbsp;ringkasan tidak lengkap</li><li>4 = Struktur materi jelas, pokok bahasan dan sub pokok bahasan kurang &nbsp;jelas, pengantar, penjelasan &amp; &nbsp;ringkasan tidak lengkap</li><li>5 = Struktur materi jelas, pokok bahasan dan sub pokok bahasan jelas, pengantar, penjelasan &amp; ringkasan lengkap</li></ul>', '6.25', '5', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('13', 'Tersedia pemicu atau pemantik diskusi terkait topik yang dapat memunculkan diskusi dan/atau menumbuh', '<ul><li>1 = Tidak ada forum diskusi</li><li>2 = Ada forum diskusi, berupa kosongan</li><li>3 = Ada forum diskusi, Tapi tidak sesuai jumlah pokok bahasan</li><li>4 = Ada forum diskusi, sesuai jumlah pokok bahasan, tapi tidak ada pemicu diskusi</li><li>5 = Tersedia pemicu atau pemantik diskusi terkait topik yang dapat memunculkan diskusi dan/atau menumbuhkan gagasan baru</li></ul>', '6.25', '5', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('14', 'Penyajian konten menggunakan bahasa yang komunikatif', '<ul><li>1 = Tidak adanya teks dalam penyajian konten</li><li>2 = Ada teks, namun penyampaian tidak jelas</li><li>3 = Ada teks, penyampaian cukup jelas</li><li>4 = Ada teks, penyampaian jelas namun kurang komunikatif</li><li>5 = Ada teks dan penyampaian menggunakan bahasa yang komunikatif</li></ul>', '6.25', '5', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('15', 'Tercantum semua referensi yang digunakan, khusus untuk referensi daring disediakan tautan untuk memu', '<ul><li>1 = Tidak ada referensi dan tautan</li><li>2 = ada tautan tapi link mati</li><li>3 = ada referensi dan tautan tapi tidak sesuai jumlah pokok bahasan</li><li>4 = ada referensi tapi tidak ada tautan</li><li>5 = ada referensi dan ada tautan</li></ul>', '6.25', '5', '1');";
		$this->db->query($sql);

		$sql = "INSERT INTO `criteria_review` VALUES ('16', 'Tampilan visual jelas, teks mudah dibaca, grafik dan chart diberi label memadai dan bebas gangguan v', '<ul><li>1 = Tampilan visual tidak ada, tidak ada teks</li><li>2 = Tampilan visual ada namun belum terlalu jelas, tidak ada teks</li><li>3 = Tampilan visual ada, grafik dan chart ada, tidak ada teks</li><li>4 = Tampilan visual ada, tidak ada grafik dan chart, ada teks</li><li>5 = Tampilan visual jelas, teks mudah dibaca, grafik dan chart diberi label memadai dan bebas gangguan visual</li></ul>', '6.25', '5', '1');";
		$this->db->query($sql);

		$sql = 'CREATE TABLE `graph_jwt` (
		  `id` int(11) NOT NULL,
		  `token_type` varchar(100) NOT NULL,
		  `access_token` text NOT NULL,
		  `expires_in` datetime NOT NULL,
		  PRIMARY KEY (`id`)
		)';
		$this->db->query($sql);
    }

    public function down()
    {

    }
}