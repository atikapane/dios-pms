<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_thumbnail_ocw_course_content_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_course_content` ADD `thumbnail` VARCHAR(255) NULL AFTER `content_status`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `ocw_course_content` DROP COLUMN `thumbnail`";
        $this->db->query($sql);
    }
}