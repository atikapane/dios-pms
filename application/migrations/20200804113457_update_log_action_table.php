<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "UPDATE `log_action` SET `key` = 'API_EXTERNAL_CDS_CREATE_CATEGORY', `category` = 'CDS' WHERE `key` = 'API_EXTERNAL_IGRACIAS_CREATE_CATEGORY'";
        $this->db->query($sql);
        $sql = "UPDATE `log_action` SET `key` = 'API_EXTERNAL_CDS_UPDATE_CATEGORY', `category` = 'CDS' WHERE `key` = 'API_EXTERNAL_IGRACIAS_UPDATE_CATEGORY'";
        $this->db->query($sql);
        $sql = "UPDATE `log_action` SET `key` = 'API_EXTERNAL_CDS_DELETE_CATEGORY', `category` = 'CDS' WHERE `key` = 'API_EXTERNAL_IGRACIAS_DELETE_CATEGORY'";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "UPDATE `log_action` SET `key` = 'API_EXTERNAL_IGRACIAS_CREATE_CATEGORY', `category` = 'IGRACIAS' WHERE `key` = 'API_EXTERNAL_CDS_CREATE_CATEGORY'";
        $this->db->query($sql);
        $sql = "UPDATE `log_action` SET `key` = 'API_EXTERNAL_IGRACIAS_UPDATE_CATEGORY', `category` = 'IGRACIAS' WHERE `key` = 'API_EXTERNAL_CDS_UPDATE_CATEGORY'";
        $this->db->query($sql);
        $sql = "UPDATE `log_action` SET `key` = 'API_EXTERNAL_IGRACIAS_DELETE_CATEGORY', `category` = 'IGRACIAS' WHERE `key` = 'API_EXTERNAL_CDS_DELETE_CATEGORY'";
        $this->db->query($sql);
    }
}