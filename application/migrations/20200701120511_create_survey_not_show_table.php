<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_survey_not_show_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `survey_not_show` (
            `id` int NOT NULL AUTO_INCREMENT,
            `user_id` int NOT NULL,
            `survey_id` int NOT NULL,
            `platform` VARCHAR(15) NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS survey_not_show";
        $this->db->query($sql);
    }
}