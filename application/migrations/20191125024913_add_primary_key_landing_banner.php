<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_primary_key_landing_banner extends CI_Migration {

    public function up()
    {
    	$sql = "TRUNCATE TABLE landing_banner;";
    	$this->db->query($sql);

    	$sql = "ALTER TABLE `landing_banner` ADD PRIMARY KEY( `id`);";
    	$this->db->query($sql);
    	
    	$sql = "ALTER TABLE `landing_banner` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;";
    	$this->db->query($sql);

    }

    public function down()
    {

    }
}