<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Recreate_table_course_management extends CI_Migration {

    public function up()
    {
    	$sql = 'DROP TABLE IF EXISTS `course_development`;';
    	$this->db->query($sql);

    	$sql = 'CREATE TABLE `course_development` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `year` int(11) NOT NULL,
		  `quarter` tinyint(3) NOT NULL,
		  `subject_id` int(11) NOT NULL,
		  `creator_list` text DEFAULT NULL,
		  `reviewer_list` text DEFAULT NULL,
		  `created_date` datetime NOT NULL,
		  `created_by` int(11) NOT NULL,
		  `updated_date` datetime DEFAULT NULL,
		  `updated_by` int(11) DEFAULT NULL,
		  `deleted_date` datetime DEFAULT NULL,
		  `deleted_by` int(11) DEFAULT NULL,
		  `status_course` tinyint(3) NOT NULL DEFAULT 1,
		  `status_approve` tinyint(3) NOT NULL DEFAULT 0,
		  `date_approve` date DEFAULT NULL,
		  `user_approve` int(11) DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`year`,`quarter`,`subject_id`,`status_approve`,`status_course`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = 'DROP TABLE IF EXISTS `course_review`;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `course_review` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) NOT NULL,
		  `reviewer_id` int(11) NOT NULL,
		  `total_score` int(11) NOT NULL,
		  `date_review` date NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`reviewer_id`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = 'DROP TABLE IF EXISTS `course_review_message`;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `course_review_message` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) NOT NULL,
		  `reviewer_id` int(11) NOT NULL,
		  `message_text` text NOT NULL,
		  `message_file` varchar(200) DEFAULT NULL,
		  `message_date` datetime NOT NULL,
		  `empnum` int(11) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`reviewer_id`,`empnum`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = 'DROP TABLE IF EXISTS `course_review_sheet`;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `course_review_sheet` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `review_id` int(11) NOT NULL,
		  `rubric_id` int(11) NOT NULL,
		  `review_score` int(11) NOT NULL,
		  `review_note` text DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`review_id`,`rubric_id`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = 'DROP TABLE IF EXISTS `criteria_review`;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `criteria_review` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(100) NOT NULL,
		  `rubric` text DEFAULT NULL,
		  `criteria_status` tinyint(3) NOT NULL DEFAULT 1,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`title`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = 'DROP TABLE IF EXISTS `enrol_development`;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `enrol_development` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) NOT NULL,
		  `empnum` int(11) NOT NULL,
		  `type_id` int(11) NOT NULL,
		  `is_leader` tinyint(3) NOT NULL DEFAULT 0,
		  `enr_subject_id` int(11) DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`empnum`,`type_id`,`enr_subject_id`) USING BTREE
		)';
		$this->db->query($sql);

		$sql = 'DROP TABLE IF EXISTS `sync_lms_course_teaching_history`;';
		$this->db->query($sql);

		$sql = 'CREATE TABLE `sync_lms_course_teaching_history` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `empnum` int(11) NOT NULL,
		  `course_id` int(11) NOT NULL,
		  `date` datetime NOT NULL DEFAULT current_timestamp(),
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`empnum`,`course_id`) USING BTREE
		)';
		$this->db->query($sql);
    }

    public function down()
    {

    }
}