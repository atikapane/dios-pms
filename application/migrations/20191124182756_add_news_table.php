<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_news_table extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS news";
        $this->db->query($sql);
        $sql = "CREATE TABLE `news` (
                `id` varchar(50) NOT NULL PRIMARY KEY,
                `title` varchar(30) NOT NULL,
                `description` text DEFAULT NULL,
                `created_by` varchar(20) DEFAULT NULL,
                `thumbnail` varchar(255) DEFAULT NULL,
                `news_date` datetime DEFAULT NULL,
                `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
                `deleted_at` datetime DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}