<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_survey_id_respondent_answer_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `survey_respondent_answer` CHANGE `survey_question_id` `survey_respondent_id` int NOT NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `survey_respondent_answer` CHANGE `survey_respondent_id` `survey_question_id` int NOT NULL";
        $this->db->query($sql);
    }
}