<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_program_owner_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `program_owner` (
            `id` int NOT NULL AUTO_INCREMENT,
            `user_id` int NOT NULL,
            `directorate_id` int NOT NULL,
            `unit_id` int NOT NULL,
            `is_group_leader` BOOLEAN NOT NULL DEFAULT 0,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS program_owner";
        $this->db->query($sql);
    }
}