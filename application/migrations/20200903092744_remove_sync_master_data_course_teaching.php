<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_sync_master_data_course_teaching extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
        $sql = "SELECT menuid FROM submenu WHERE submenu_link = 'be/sync/course_lms'";
        $menu_id = $this->db->query($sql)->row()->menuid;

        $sql = "DELETE FROM submenu WHERE submenu_link = 'be/sync/course_lms'";
        $this->db->query($sql);

        $sql = "DELETE FROM menu WHERE menu_id = " . $menu_id;
        $this->db->query($sql);
    }
}