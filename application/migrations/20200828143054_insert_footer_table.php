<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_footer_table extends CI_Migration {

    public function up()
    {
        $sql = 'INSERT INTO `landing_footer` (`title`,`content`,`priority`) VALUES (\'ABOUT\',\'<img src="https://celoe.telkomuniversity.ac.id/assets/media/img/logo-celoe-white-no-desc.png" style="width:70%;height:auto;min-height: unset"><p class="mt-2">Membangun Manusia dan Mengembangkan Teknologi. Dari Telkom University untuk Semua.</p><p>Harmony, Excellence, Integrity</p>\',1)';
        $this->db->query($sql);
        $sql = 'INSERT INTO `landing_footer` (`title`,`content`,`priority`) VALUES (\'CONTACT\',\'<p>Gedung Bangkit Telkom University Jl. Telekomunikasi Terusan Buah Batu Indonesia 40257 , Bandung , Indonesia<br><i class="fas fa-phone"></i> +62 821-1666-3563<br><i class="fas fa-envelope"></i> infoceloe@telkomuniversity.ac.id</p>\',2)';
        $this->db->query($sql);
        $sql = 'INSERT INTO `landing_footer` (`title`,`content`,`priority`) VALUES (\'CELOE APPLICATION\',\'<p><a href="https://celoe.telkomuniversity.ac.id/auth" target="_blank">CeLOE Dashboard </a><br/><a href="https://cds.telkomuniversity.ac.id/" target="_blank">CeLOE CDS</a><br/><a href="https://lms.telkomuniversity.ac.id/" target="_blank">CeLOE LMS</a><br/><a href="https://onlinelearning.telkomuniversity.ac.id/" target="_blank">CeLOE MOOC </a></p>\',3);';
        $this->db->query($sql);
        $sql = 'INSERT INTO `landing_footer` (`title`,`content`,`priority`) VALUES (\'OTHER LINKS\',\'<p><a href="https://celoe.telkomuniversity.ac.id/code" target="_blank">CeLOE Development (CODE) </a><br/><a href="https://igracias.telkomuniversity.ac.id/" target="_blank">iGracias</a><br/><a href="https://openlibrary.telkomuniversity.ac.id/" target="_blank">Open Library</a><br/><a href="https://cdc.telkomuniversity.ac.id/" target="_blank">Career, Alumni, Endowment (CAE)</a><br/></p>\',4);';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `landing_footer` WHERE `title` = 'ABOUT' OR `title` = 'CONTACT' OR `title` = 'CELOE APPLICATION' OR `title` = 'OTHER LINKS'";
        $this->db->query($sql);
    }
}