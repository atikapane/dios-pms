<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_ocw_user_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `ocw_user` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `email` VARCHAR(255) NOT NULL,
		  `full_name` VARCHAR(255) NOT NULL,
		  `username` VARCHAR(255) NOT NULL,
		  `password` VARCHAR(255) NOT NULL,
		  `country_id` int(11) NOT NULL,
		  `city_id` int(11) NOT NULL,
		  `gender` VARCHAR(255) NOT NULL,
		  `year_of_birth` VARCHAR(5) NOT NULL,
		  `education_level_id` int(11) NOT NULL,
		  `partner_id` int(11) NOT NULL,
		  `unique_code` VARCHAR(255) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE
		)";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}