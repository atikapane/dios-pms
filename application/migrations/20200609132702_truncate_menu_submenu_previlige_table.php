<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Truncate_menu_submenu_previlige_table extends CI_Migration {

    public function up()
    {
        $sql = "TRUNCATE TABLE menu";
        $this->db->query($sql);
        $sql = "TRUNCATE TABLE submenu";
        $this->db->query($sql);
        $sql = "TRUNCATE TABLE previleges";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}