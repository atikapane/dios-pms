<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_mooc_id_users_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `users` ADD `mooc_id` int NULL AFTER `id`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `users` DROP COLUMN `mooc_id`";
        $this->db->query($sql);
    }
}