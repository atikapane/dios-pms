<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_status_external_log_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `log_api_external` ADD `status` boolean NOT NULL AFTER `user_agent`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `log_api_external` DROP  COLUMN `status`";
        $this->db->query($sql);
    }
}