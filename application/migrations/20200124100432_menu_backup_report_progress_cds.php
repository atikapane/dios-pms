<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Menu_backup_report_progress_cds extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (90, '4', 'Backup Course', 'be/sync/Cds_backup', NULL);";
        $this->db->query($sql);

        $sql = "INSERT INTO `submenu` (`submenu_id`, `menuid`, `submenu_name`, `submenu_link`, `submenu_icon`) VALUES (21, '7', 'Capaian Pengembangan Course', 'be/report/Course_progress', NULL);";
        $this->db->query($sql);

        $sql = "INSERT INTO `previleges` (`previlege_id`, `submenu_id`, `groupid`, `create`, `update`, `delete`, `read`, `upload`, `download`) VALUES ('20', '20', '1', '1', '1', '1', '1', '1', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `previleges` (`previlege_id`, `submenu_id`, `groupid`, `create`, `update`, `delete`, `read`, `upload`, `download`) VALUES ('21', '21', '1', '1', '1', '1', '1', '1', '1');";
        $this->db->query($sql);

        $sql = "CREATE TABLE `sync_course_backup_history` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `user_id` int(11) NOT NULL,
          `subject_id` int(11) NOT NULL,
          `date` datetime NOT NULL DEFAULT current_timestamp(),
          `log` text DEFAULT NULL,
          `status` tinyint(3) NOT NULL DEFAULT 0,
          PRIMARY KEY (`id`),
          UNIQUE KEY `unique` (`id`) USING BTREE,
          KEY `index` (`user_id`,`subject_id`,`status`) USING BTREE
        )";
		$this->db->query($sql);

        $sql = "ALTER TABLE `sync_lms_course_teaching_history`
            ADD COLUMN `status`  tinyint(3) NOT NULL DEFAULT 0 AFTER `log`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `sync_course`
            ADD COLUMN `last_backup`  datetime NULL AFTER `input_date`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `sync_lms_course_teaching_history`
            DROP INDEX `index` ,
            ADD INDEX `index` (`user_id`, `course_id`, `status`) USING BTREE ;";
        $this->db->query($sql);

        $sql = "DROP TABLE IF EXISTS `course_development_progress_report`;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `course_development_progress_report` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `subject_id` int(11) NOT NULL,
            `number_topic` int(11) NOT NULL,
            `list_topic` text NOT NULL,
            `percent_profile` decimal(11,2) NOT NULL,
            `percent_topic` decimal(11,2) NOT NULL,
            `number_file` int(11) NOT NULL,
            `number_assignment` int(11) NOT NULL,
            `number_video` int(11) NOT NULL,
            `number_quiz` int(11) NOT NULL,
            `number_link` int(11) NOT NULL,
            `number_forum` int(11) NOT NULL,
            `percent_progress` decimal(11,2) NOT NULL,
            PRIMARY KEY (`id`,`subject_id`),
            UNIQUE KEY `unique` (`id`,`subject_id`) USING BTREE,
            KEY `index` (`percent_profile`,`percent_topic`,`percent_progress`) USING BTREE
          )";
        $this->db->query($sql);

        $sql = "CREATE TABLE `course_development_progress_statistic` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `statistic_date` date NOT NULL,
            `subject_id` int(11) NOT NULL,
            `number_topic` int(11) NOT NULL,
            `list_topic` text NOT NULL,
            `percent_profile` decimal(11,2) NOT NULL,
            `percent_topic` decimal(11,2) NOT NULL,
            `number_file` int(11) NOT NULL,
            `number_assignment` int(11) NOT NULL,
            `number_video` int(11) NOT NULL,
            `number_quiz` int(11) NOT NULL,
            `number_link` int(11) NOT NULL,
            `number_forum` int(11) NOT NULL,
            `percent_progress` decimal(11,2) NOT NULL,
            PRIMARY KEY (`id`,`statistic_date`,`subject_id`),
            UNIQUE KEY `unique` (`id`,`subject_id`,`statistic_date`) USING BTREE,
            KEY `index` (`percent_profile`,`percent_topic`,`percent_progress`) USING BTREE
          )";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}