<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_internal_enrol_type_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `enrol_type` (`enrol_id`, `enrol_name`, `status`) VALUES (1, 'Student', 'internal')";
        $this->db->query($sql);
        $sql = "INSERT INTO `enrol_type` (`enrol_id`, `enrol_name`, `status`) VALUES (2, 'Trainer', 'internal')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `enrol_type` WHERE (`enrol_name` = 'Student' OR `enrol_name` = 'Trainer') AND `status` = 'internal'";
        $this->db->query($sql);
    }
}