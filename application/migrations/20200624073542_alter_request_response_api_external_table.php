<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_request_response_api_external_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `log_api_external` CHANGE `request` `request` LONGTEXT NOT NULL, CHANGE `response` `response` LONGTEXT NOT NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `log_api_external` CHANGE `request` `request` VARCHAR(255) NOT NULL, CHANGE `response` `response` VARCHAR(255) NOT NULL";
        $this->db->query($sql);
    }
}