<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_log_action_banner extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'BANNER', 'list', 'API_BANNER_LIST', 'get list banner');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '72', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_BANNER_LIST'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 72";
        $this->db->query($sql);
    }
}