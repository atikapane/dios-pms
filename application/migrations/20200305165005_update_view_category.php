<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_view_category extends CI_Migration {

    public function up()
    {
        $sql = "DROP VIEW vw_ocw_category;";
        $this->db->query($sql);

        $sql = "CREATE VIEW vw_ocw_category AS SELECT
            `ocw_category`.`ocw_category_id` AS `ocw_category_id`,
            `sync_category`.`category_name` AS `faculty_name`,
            vw_ocw_program.program_name AS program_name,
            `ocw_category`.`faculty_id` AS `faculty_id`,
            `ocw_category`.`program_id` AS `program_id`,
            `ocw_category`.`name` AS `name`,
            `ocw_category`.`description` AS `description`,
            `ocw_category`.`img_path` AS `img_path`,
            `ocw_category`.`created_by` AS `created_by`,
            `ocw_category`.`updated_at` AS `updated_at`,
            `ocw_category`.`deleted_at` AS `deleted_at` 
            FROM
            `ocw_category`
            JOIN `sync_category` ON `ocw_category`.`faculty_id` = `sync_category`.`category_id`
            JOIN vw_ocw_program ON ocw_category.program_id = vw_ocw_program.program_id;";

        $this->db->query($sql);
    }

    public function down()
    {

    }
}