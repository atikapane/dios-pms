<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_footer_table extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS `landing_footer`;";
        $this->db->query($sql);

        $sql = "CREATE TABLE `landing_footer` (
            `id` int NOT NULL AUTO_INCREMENT,
            `title` VARCHAR(255) NOT NULL,
            `content` TEXT NOT NULL,
            `priority` int NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS landing_footer";
        $this->db->query($sql);
    }
}