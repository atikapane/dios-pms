<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_publish_grade_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (10, 'Publish Grade', 'be/ocw/grade')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/ocw/grade'";
        $this->db->query($sql);
    }
}