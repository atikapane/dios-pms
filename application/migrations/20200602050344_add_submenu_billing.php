<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_submenu_billing extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (7, 'Sales', 'be/report/Sales')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Sales'";
        $this->db->query($sql);
    }
}