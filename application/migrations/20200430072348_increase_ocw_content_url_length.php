<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Increase_ocw_content_url_length extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE `ocw_course_content` CHANGE `content_url` `content_url` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;";
    	$this->db->query($sql);
    }

    public function down()
    {
		$sql = "ALTER TABLE `ocw_course_content` CHANGE `content_url` `content_url` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;";
    	$this->db->query($sql);
    }
}