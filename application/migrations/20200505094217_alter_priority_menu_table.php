<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_priority_menu_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `menu` ADD `priority` int NOT NULL AFTER `menu_icon`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `menu` DROP COLUMN `priority`";
        $this->db->query($sql);
    }
}