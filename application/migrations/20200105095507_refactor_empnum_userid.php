<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Refactor_empnum_userid extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `enrol_development`
			CHANGE COLUMN `empnum` `user_id`  int(11) NOT NULL AFTER `course_id`;";
		$this->db->query($sql);

		$sql = "ALTER TABLE `course_review_message`
			CHANGE COLUMN `empnum` `user_id`  int(11) NOT NULL AFTER `message_date`;";
		$this->db->query($sql);

		$sql = "CREATE TABLE `course_development_progress_report` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `year` int(11) NOT NULL,
		  `quarter` tinyint(3) NOT NULL,
		  `subject_id` int(11) NOT NULL,
		  `number_topic` int(11) NOT NULL,
		  `list_topic` text NOT NULL,
		  `percent_profile` int(11) NOT NULL,
		  `percent_topic` int(11) NOT NULL,
		  `number_file` int(11) NOT NULL,
		  `number_assignment` int(11) NOT NULL,
		  `number_video` int(11) NOT NULL,
		  `number_quiz` int(11) NOT NULL,
		  `number_link` int(11) NOT NULL,
		  `number_forum` int(11) NOT NULL,
		  `percent_progress` int(11) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`year`,`quarter`,`subject_id`,`percent_progress`) USING BTREE
		)";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}