<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Ocw_course_adjustment extends CI_Migration {

    public function up()
    {

		$sql = "ALTER TABLE `ocw_course` ADD `course_profile` TEXT NOT NULL AFTER `lecturer_profile`;";
    	$this->db->query($sql);
		
		$sql = "ALTER TABLE `ocw_course_content` ADD `section_name` VARCHAR(100) NOT NULL AFTER `section_id`;";
    	$this->db->query($sql);
		
		$sql = "ALTER TABLE `ocw_course` ADD `type` INT NOT NULL DEFAULT '1' AFTER `ocw_name`;";
    	$this->db->query($sql);
		//status 1 : not set, 2 : setup done , 3 published
        $sql = "ALTER TABLE `ocw_course` ADD `status` INT NOT NULL DEFAULT '1' AFTER `sync_course_id`;";
        $this->db->query($sql);
        
        
		$sql = "DROP VIEW IF EXISTS ocw_course_joined;";
    	$this->db->query($sql);

		$sql = "CREATE VIEW ocw_course_joined AS SELECT ocw_course.*,course.subject_name,course.subject_code, study_program.category_id as study_program_id, study_program.category_name as study_program_name, faculty.category_id as faculty_id, faculty.category_name as faculty_name,user.fullname,user.employeeid FROM `ocw_course` JOIN sync_course course ON course.subject_id = ocw_course.sync_course_id JOIN sync_category study_program ON study_program.category_id = course.category_id JOIN sync_category faculty ON faculty.category_id = study_program.category_parent_id JOIN user ON user.id = ocw_course.created_by WHERE 1";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}