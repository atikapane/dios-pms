<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_landing_about_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `landing_about` (
            `id` int NOT NULL AUTO_INCREMENT,
            `title` VARCHAR(255) NOT NULL,
            `short_title` VARCHAR(255) NOT NULL,
            `description` TEXT NOT NULL,
            `color` VARCHAR(255) NULL,
            `priority` int NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down(){
        $sql = "DROP TABLE IF EXISTS `landing_about`";
        $this->db->query($sql);
    }
}