<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_certificate_number_user_grade_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `user_grade` ADD `certificate_number` VARCHAR(25) NULL AFTER `grade`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user_grade` DROP  COLUMN `certificate_number`";
        $this->db->query($sql);
    }
}