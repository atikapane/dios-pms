<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_mooc_id_ocw_course_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_course` ADD `mooc_id` int NULL AFTER `sync_course_id`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `ocw_course` DROP COLUMN `mooc_id`";
        $this->db->query($sql);
    }
}