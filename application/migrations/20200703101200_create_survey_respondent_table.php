<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_survey_respondent_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `survey_respondent` (
            `id` int NOT NULL AUTO_INCREMENT,
            `survey_id` int NOT NULL,
            `respondent_id` int NOT NULL,
            `respondent_username` VARCHAR(255) NOT NULL,
            `respondent_name` VARCHAR(255) NOT NULL,
            `respondent_email` VARCHAR(255) NOT NULL,
            `respondent_origin` VARCHAR(255) NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS survey_respondent";
        $this->db->query($sql);
    }
}