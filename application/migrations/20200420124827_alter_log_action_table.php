<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `log_action` ADD `category` VARCHAR(255) NOT NULL AFTER `id`";
        $this->db->query($sql);

        $sql = "UPDATE `log_action` SET `category`='Authentication'";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}