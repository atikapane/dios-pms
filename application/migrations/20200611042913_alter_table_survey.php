<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_survey3 extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `survey` ADD `user` VARCHAR(25) NOT NULL AFTER `type`";
        
        $this->db->query($sql);
    }

    public function down()
    {

    }
}