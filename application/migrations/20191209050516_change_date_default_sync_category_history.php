<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Change_date_default_sync_category_history extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE sync_category_history CHANGE `date` `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}