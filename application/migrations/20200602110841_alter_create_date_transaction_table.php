<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_create_date_transaction_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `transaction` ADD `created_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `total_price`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `transaction` DROP  COLUMN `created_date`;";
        $this->db->query($sql);
    }
}