<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_group_landing_about_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `landing_about` CHANGE `short_title` `group` VARCHAR(255) NOT NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `landing_about` CHANGE `group` `short_title` VARCHAR(255) NOT NULL";
        $this->db->query($sql);
    }
}