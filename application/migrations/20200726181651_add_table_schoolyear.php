<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_table_schoolyear extends CI_Migration {

    public function up()
    {
    	$sql = "DROP TABLE IF EXISTS `schoolyear`;";
        $this->db->query($sql);

        $sql = 'CREATE TABLE `schoolyear` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `year` char(4) NOT NULL,
		  `semester` tinyint(3) NOT NULL,
		  `description` varchar(200) DEFAULT NULL,
		  `status` tinyint(3) NOT NULL DEFAULT 1,
		  PRIMARY KEY (`id`)
		)';
        $this->db->query($sql);

        $sql = "INSERT INTO `schoolyear` VALUES ('1', '1920', '1', 'Semester Ganjil Tahun Ajaran 2019-2020', '0');";
        $this->db->query($sql);
        $sql = "INSERT INTO `schoolyear` VALUES ('2', '1920', '2', 'Semester Genap Tahun Ajaran 2019-2020', '1');";
        $this->db->query($sql);
        $sql = "INSERT INTO `schoolyear` VALUES ('3', '2021', '1', 'Semester Ganjil Tahun Ajaran 2020-2021', '0');";
        $this->db->query($sql);

        $this->db->select("menu_id");
        $this->db->from("menu");
        $this->db->where("menu_name = 'Site Setting'");
		$this->db->limit(1);
        $query = $this->db->get();
        $row = $query->row_array();
        if (!empty($row)){
        	$parent_id = $row["menu_id"];
        	$sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (" . $parent_id . ", 'School Year', 'be/site_setting/schoolyear');";
        	$this->db->query($sql);
        }
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `schoolyear`;";
        $this->db->query($sql);

        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/site_setting/schoolyear'";
        $this->db->query($sql);
    }
}