<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_terms_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (6, 'Terms', 'be/landing/terms')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Terms'";
        $this->db->query($sql);
    }
}