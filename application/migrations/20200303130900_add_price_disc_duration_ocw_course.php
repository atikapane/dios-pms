<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_price_disc_duration_ocw_course extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `ocw_course` ADD `price` INT NOT NULL DEFAULT '0' AFTER `end_date`, ADD `discount` INT NOT NULL DEFAULT '0' AFTER `price`;";
    	$this->db->query($sql);
    	$sql = "CREATE TABLE `ocw_course_duration` ( `id` INT NOT NULL AUTO_INCREMENT , `section_id` INT NOT NULL , `section_name` VARCHAR(150) NOT NULL , `duration` INT NOT NULL DEFAULT '0' , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
		$this->db->query($sql);

    }

    public function down()
    {

    }
}