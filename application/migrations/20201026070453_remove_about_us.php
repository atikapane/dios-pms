<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_about_us extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
    	$sql = "SELECT menuid FROM submenu WHERE submenu_link = 'be/landing/about'";
        $menu_id = $this->db->query($sql)->row()->menuid;

        $sql = "DELETE FROM submenu WHERE submenu_link = 'be/landing/about'";
        $this->db->query($sql);

    }
}