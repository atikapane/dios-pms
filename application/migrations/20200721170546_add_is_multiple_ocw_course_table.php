<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_is_multiple_ocw_course_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `ocw_course` ADD `is_multiple` BOOLEAN NOT NULL DEFAULT 0 AFTER `status`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `ocw_course` DROP  COLUMN `is_multiple`";
        $this->db->query($sql);
    }
}