<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Truncate_log_setting_table extends CI_Migration {

    public function up()
    {
        $sql = "TRUNCATE TABLE log_setting";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}