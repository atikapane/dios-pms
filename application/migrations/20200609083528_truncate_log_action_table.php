<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Truncate_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "TRUNCATE TABLE log_action";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}