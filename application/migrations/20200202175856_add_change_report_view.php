<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_change_report_view extends CI_Migration {

    public function up()
    {
    	$sql = "DROP TABLE IF EXISTS course_change_log;";
    	$this->db->query($sql);


		$sql = "CREATE TABLE `course_change_log` ( `id` int(11) NOT NULL, `name` varchar(255) NOT NULL,`course_id` varchar(50) NOT NULL, `quiz` int(11) NOT NULL DEFAULT 0, `resource` int(11) NOT NULL DEFAULT 0, `hvp` int(11) NOT NULL DEFAULT 0, `label` int(11) DEFAULT 0, `page` int(11) NOT NULL DEFAULT 0, `url` int(11) NOT NULL DEFAULT 0, `forum` int(11) NOT NULL DEFAULT 0,`assignment` int(11) NOT NULL DEFAULT 0, `pull_date` datetime NOT NULL DEFAULT current_timestamp()) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
    	$this->db->query($sql);
    	
		$sql = "ALTER TABLE `course_change_log` ADD PRIMARY KEY (`id`);";
    	$this->db->query($sql);
    	
		$sql = "create view `course_change_report`  AS select `schoolyear`.`category_name` AS `school_year`,`lms_course`.`semester` AS `semester`,`lms_course`.`subject_code` AS `subject_code`,`lms_course`.`subject_name` AS `subject_name`,`lms_course`.`class` AS `class`,`lms_course`.`employee_name` AS `employee_name`,`lms_course`.`user_id` AS `user_id`,`study_program`.`category_name` AS `study_program_name`,`faculty`.`category_name` AS `faculty_name`,`log`.`forum` AS `forum`,`log`.`assignment` AS `assignment`,`log`.`hvp` AS `hvp`,`log`.`label` AS `label`,`log`.`page` AS `page`,`log`.`quiz` AS `quiz`,`log`.`resource` AS `resource`,`log`.`url` AS `url` from ((((`sync_lms_course` `lms_course`  join `sync_category` `schoolyear` on(`schoolyear`.`category_id` = `lms_course`.`category_id`)) join `sync_category` `study_program` on(`study_program`.`category_id` = `schoolyear`.`category_parent_id`))  join `sync_category` `faculty` on(`faculty`.`category_id` = `study_program`.`category_parent_id`)) join `course_change_log` `log` on(`log`.`course_id` = `lms_course`.`course_id`)) where `lms_course`.`employee_name` is not null ;";
		$this->db->query($sql);

    }

    public function down()
    {

    }
}