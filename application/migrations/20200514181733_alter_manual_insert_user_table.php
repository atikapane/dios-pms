<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_manual_insert_user_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `user` ADD `is_manual_insert` int NOT NULL AFTER `last_sync`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user` DROP COLUMN `is_manual_insert`";
        $this->db->query($sql);
    }
}