<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_survey_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `survey` (
            `id` int NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NOT NULL,
            `introduction` text NOT NULL,
            `start_date` DATE NOT NULL,
            `end_date` DATE NOT NULL,
            `platform` VARCHAR(20) NOT NULL,
            `semester` VARCHAR(10) NULL,
            `subject_code` VARCHAR(50) NULL,
            `subject_name` VARCHAR(255) NULL,
            `seen_count` int NOT NULL DEFAULT 0,
            `reject_count` int NOT NULL DEFAULT 0,
            `reject_permanently_count` int NOT NULL DEFAULT 0,
            `type` int NOT NULL COMMENT '1=lecture, 2=service',
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            `created_by` int NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS survey";
        $this->db->query($sql);
    }
}