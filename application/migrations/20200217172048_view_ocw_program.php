<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_View_ocw_program extends CI_Migration {

    public function up()
    {
        $sql = "CREATE VIEW vw_ocw_program AS SELECT sync_category.category_id AS program_id, sync_category.category_name AS program_name, sync_category.category_parent_id AS faculty_id FROM sync_category WHERE sync_category.category_type = 'STUDYPROGRAM';";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}