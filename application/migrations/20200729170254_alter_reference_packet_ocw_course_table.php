<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_reference_packet_ocw_course_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `reference_multiple_course` CHANGE `course_reference_id` `package_ocw_course_id` int NOT NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `reference_multiple_course` CHANGE `package_ocw_course_id` `course_reference_id` int NOT NULL";
        $this->db->query($sql);
    }
}