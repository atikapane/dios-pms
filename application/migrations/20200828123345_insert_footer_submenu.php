<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_footer_submenu extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES ( 6, 'Footer', 'be/landing/footer');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/landing/footer'";
        $this->db->query($sql);
    }
}