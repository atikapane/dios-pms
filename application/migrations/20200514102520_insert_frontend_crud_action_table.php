<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_frontend_crud_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Create', 'USER_FRONTEND_CREATE', 'event log create user frontend')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Create', 'USER_FRONTEND_CREATE_SUCCESS', 'event log create user frontend success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Create', 'USER_FRONTEND_CREATE_FAILED', 'event log create user frontend failed')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Update', 'USER_FRONTEND_UPDATE', 'event log update user frontend')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Update', 'USER_FRONTEND_UPDATE_SUCCESS', 'event log update user frontend success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Update', 'USER_FRONTEND_UPDATE_FAILED', 'event log update user frontend failed')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Delete', 'USER_FRONTEND_DELETE', 'event log delete user frontend')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Delete', 'USER_FRONTEND_DELETE_SUCCESS', 'event log delete user frontend success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Delete', 'USER_FRONTEND_DELETE_FAILED', 'event log delete user frontend failed')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_CREATE' OR `key` = 'USER_FRONTEND_CREATE_SUCCESS' OR `key` = 'USER_FRONTEND_CREATE_FAILED'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_UPDATE' OR `key` = 'USER_FRONTEND_UPDATE_SUCCESS' OR `key` = 'USER_FRONTEND_UPDATE_FAILED'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_DELETE' OR `key` = 'USER_FRONTEND_DELETE_SUCCESS' OR `key` = 'USER_FRONTEND_DELETE_FAILED'";
        $this->db->query($sql);
    }
}