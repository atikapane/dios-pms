<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_dashboard_landing_about_table extends CI_Migration {

    public function up()
    {
        $sql = "UPDATE `landing_about` SET `group` = 'Dashboard' WHERE `title` = 'CELOE LANDING PAGE'";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "UPDATE `landing_about` SET `group` = 'CLP' WHERE `title` = 'CELOE LANDING PAGE'";
        $this->db->query($sql);
    }
}