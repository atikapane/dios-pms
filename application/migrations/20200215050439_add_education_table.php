<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_education_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `education_level` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` VARCHAR(255) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE
		)";
        $this->db->query($sql);

        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Doctorate');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Master of professional degree');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Bachelor degree');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Associate degree');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Secondary/high school');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Junior secondary/junior high/middle school');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Elementary/primary school');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('No Formal Education');";
        $this->db->query($sql);
        $sql = "INSERT INTO `education_level` (`name`) VALUES ('Other Education');";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}