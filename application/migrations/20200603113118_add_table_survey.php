<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_table_survey extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS survey";
        $this->db->query($sql);
        $sql = "CREATE TABLE `survey` ( `id` INT(11) NOT NULL AUTO_INCREMENT , 
        `respondent` VARCHAR(50) NOT NULL , 
        `type` VARCHAR(25) NOT NULL , 
        `course` VARCHAR(50) NOT NULL , 
        `course_id` INT(11) NOT NULL , 
        `start_time` DATETIME NOT NULL , 
        `end_time` DATETIME NOT NULL , 
        `platform` INT(11) NOT NULL , 
        `semester` VARCHAR(15) NULL , 
        `class` VARCHAR(20) NULL , 
        `created_at` TIMESTAMP NOT NULL , 
        PRIMARY KEY (`id`)) ENGINE = InnoDB";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}