<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_sales_submmenu_table extends CI_Migration {

    public function up()
    {
        $sql = "UPDATE `submenu` SET `menuid` = 13, `submenu_link` = 'be/billing/sales', `priority` = 2 WHERE `submenu_name` = 'Sales'";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "UPDATE `submenu` SET `menuid` = 7, `submenu_link` = 'be/report/sales', `priority` = 0 WHERE `submenu_name` = 'Sales'";
        $this->db->query($sql);
    }
}