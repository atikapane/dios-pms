<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_manager_enroll_type_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `enrol_type` (`enrol_id`, `enrol_name`) VALUES (4, 'Manager')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `enrol_type` WHERE `enrol_id` = 4";
        $this->db->query($sql);
    }
}