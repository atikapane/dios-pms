<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_survey extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `survey` CHANGE `platform` `platform` VARCHAR(25) NOT NULL, CHANGE `course_id` `course_id` INT(11) NULL, CHANGE `course` `course` VARCHAR(50) NULL";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}