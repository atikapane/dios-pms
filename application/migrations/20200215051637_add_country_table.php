<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_country_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `country` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` VARCHAR(255) NOT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE
		)";
        $this->db->query($sql);

        $sql = "INSERT INTO `country` (`name`) VALUES ('Indonesia');";
        $this->db->query($sql);
        $sql = "INSERT INTO `country` (`name`) VALUES ('Singapore');";
        $this->db->query($sql);
        $sql = "INSERT INTO `country` (`name`) VALUES ('Malaysia');";
        $this->db->query($sql);
        $sql = "INSERT INTO `country` (`name`) VALUES ('Vietnam');";
        $this->db->query($sql);
        $sql = "INSERT INTO `country` (`name`) VALUES ('Thailand');";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}