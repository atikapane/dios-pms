<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_sync_lms_course extends CI_Migration {

    public function up()
    {

    	$sql = "CREATE TABLE `sync_lms_course` ( `course_id` INT NOT NULL , `category_id` INT  NULL , `semester` VARCHAR(6)  NULL , `class` VARCHAR(50) NULL , `subject_code` VARCHAR(50) NULL , `subject_name` VARCHAR(255) NULL , `sync_status` VARCHAR(100) NULL , `sync_date` DATETIME NULL , `flag_status` VARCHAR(50) NULL , `table_owner` VARCHAR(50) NULL , `table_id` INT NULL , `subject_id` INT NULL , PRIMARY KEY (`course_id`)) ENGINE = InnoDB;";
        $this->db->query($sql);
    	$sql = "CREATE TABLE `sync_lms_course_history` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}