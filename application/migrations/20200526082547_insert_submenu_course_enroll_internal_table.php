<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_submenu_course_enroll_internal_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (10, 'Course Enroll Internal', 'be/course_management/enroll_internal')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Course Enroll Internal'";
        $this->db->query($sql);
    }
}