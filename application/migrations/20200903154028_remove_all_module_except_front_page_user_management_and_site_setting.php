<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_all_module_except_front_page_user_management_and_site_setting extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS certificate,
            course_attendance_report,
            course_development,
            course_development_progress_report,
            course_review,
            course_review_message,
            course_review_sheet,
            criteria_review,
            curriculum,
            enrol_development,
            enrol_type,
            landing_billing,
            ocw_category,
            ocw_course_content,
            ocw_course_duration,
            ocw_course_enroll,
            ocw_course_enroll_internal,
            packet_ocw_course,
            packet_ocw_course_faq,
            program_owner,
            reference_multiple_course,
            schoolyear,
            survey,
            survey_class,
            survey_question,
            survey_question_option,
            sync_category_history,
            sync_course_backup_history,
            sync_course_development_history,
            sync_course_history,
            sync_lms_course,
            sync_lms_course_history,
            sync_lms_course_teaching_history,
            transaction,
            transaction_item,
            user_grade";
        $this->db->query($sql);

        $sql = "DROP VIEW IF EXISTS course_change_report,
            ocw_course_joined,
            vw_ocw_category";
        $this->db->query($sql);
    }
}