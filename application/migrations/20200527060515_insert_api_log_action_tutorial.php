<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_log_action_tutorial extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'TUTORIAL', 'list', 'API_TUTORIAL_LIST', 'get list tutorial');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '85', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_TUTORIAL_LIST'";
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 85";
        $this->db->query($sql);
    }
}