<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_frontend_import_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Import', 'USER_FRONTEND_IMPORT', 'event log user import user frontend')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Import', 'USER_FRONTEND_IMPORT_SUCCESS', 'event log user import user frontend success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CRUD', 'Import', 'USER_FRONTEND_IMPORT_FAILED', 'event log user import user frontend failed')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_IMPORT' OR `key` = 'USER_FRONTEND_IMPORT_SUCCESS' OR `key` = 'USER_FRONTEND_IMPORT_FAILED'";
        $this->db->query($sql);
    }
}