<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_download extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `landing_download` ADD COLUMN `file_status`  tinyint(3) NOT NULL DEFAULT 1 AFTER `description`;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}