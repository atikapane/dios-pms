<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_status_enrol_type_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `enrol_type` ADD `status` VARCHAR(8) NOT NULL DEFAULT 'external' AFTER `enrol_name`";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `enrol_type` DROP COLUMN `status`";
        $this->db->query($sql);
    }
}