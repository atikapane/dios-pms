<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_front_page_module extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
    	$sql = "DROP TABLE IF EXISTS 
            landing_download,
    		landing_content,
    		landing_about,
    		landing_banner,
    		news,
    		landing_tutorial,
    		landing_agreements
    		";
        $this->db->query($sql);

    }
}