<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_user_frontend_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (1, 'User Frontend', 'be/user_management/frontend')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'User Frontend'";
        $this->db->query($sql);
    }
}