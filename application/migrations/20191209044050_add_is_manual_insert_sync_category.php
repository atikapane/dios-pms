<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_is_manual_insert_sync_category extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE `sync_category` ADD `is_manual_insert` TINYINT NOT NULL DEFAULT '0' AFTER `updated_id`;";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}