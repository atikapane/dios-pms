<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_certificate_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `certificate` (
            `id` int NOT NULL AUTO_INCREMENT,
            `template_name` VARCHAR(255) NOT NULL,
            `data` TEXT NOT NULL,
            `is_shared` BOOLEAN NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS certificate";
        $this->db->query($sql);
    }
}