<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_certificate_path_user_grade_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `user_grade` ADD `certificate_path` VARCHAR(255) NULL AFTER `certificate_number`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `user_grade` DROP  COLUMN `certificate_path`";
        $this->db->query($sql);
    }
}