<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_user_cart extends CI_Migration {

    public function up()
    {
    	$sql = "DROP TABLE IF EXISTS user_cart";
    	$this->db->query($sql);
    	$sql = "CREATE TABLE `user_cart` ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `course_id` INT NOT NULL , `updated_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
    	$this->db->query($sql);
    }

    public function down()
    {
		$sql = "DROP TABLE IF EXISTS user_cart";
    	$this->db->query($sql);
    }
}