<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_agreement_terms_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `landing_terms` RENAME TO `landing_agreements`";
        $this->db->query($sql);

        $sql = "UPDATE `submenu` SET `submenu_name` = 'Agreement', `submenu_link` = 'be/landing/Agreement' WHERE `submenu_link` = 'be/landing/terms'";
        $this->db->query($sql);

        $sql = 'ALTER TABLE `landing_agreements` ADD `name` VARCHAR(255) NOT NULL AFTER `id`';
        $this->db->query($sql);

        $sql = "UPDATE `landing_agreements` SET `name` = 'tnc'";
        $this->db->query($sql);

        $sql = "INSERT INTO `landing_agreements` (`name`, `content`) VALUES ('privacy', 'This is privacy policy');"; 
        $this->db->query($sql);

        $sql = "INSERT INTO `landing_agreements` (`name`, `content`) VALUES ('create_course', 'This is ocw create course aggrement');"; 
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `landing_agreements` WHERE `name` = 'privacy' OR `name` = 'create_course'";
        $this->db->query($sql);

        $sql = "ALTER TABLE `landing_agreements` DROP  COLUMN `name`";
        $this->db->query($sql);

        $sql = "UPDATE `submenu` SET `submenu_name` = 'Terms', `submenu_link` = 'be/landing/terms' WHERE `submenu_link` = 'be/landing/Agreement'";
        $this->db->query($sql);

        $sql = "ALTER TABLE `landing_agreements` RENAME TO `landing_terms`";
        $this->db->query($sql);
    }
}