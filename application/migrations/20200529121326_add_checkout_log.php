<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_checkout_log extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Checkout', 'Transaction retrieve success', 'API_TRANSACTION_GET', 'event log if retrieve transaction success');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '93', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'Checkout', 'Transaction retrieve failed', 'API_TRANSACTION_GET_FAILED', 'event log if retrieve transaction failed');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '94', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_TRANSACTION_GET' or `key` = 'API_TRANSACTION_GET_FAILED'" ;
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 93 or `log_action_id` = 94";
        $this->db->query($sql);
    }
}