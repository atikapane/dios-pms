<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_api_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Authentication', 'Register', 'API_REGISTER', 'event log api register')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Authentication', 'Register', 'API_REGISTER_SUCCESS', 'event log api register success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Authentication', 'Register', 'API_REGISTER_FAILED', 'event log api register failed')";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('User', 'Update', 'API_USER_UPDATE', 'event log api user update')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('User', 'Update', 'API_USER_UPDATE_SUCCESS', 'event log api user update success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('User', 'Update', 'API_USER_UPDATE_FAILED', 'event log api user update failed')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_REGISTER' OR `key` = 'API_REGISTER_SUCCESS' OR `key` = 'API_REGISTER_FAILED'";
        $this->db->query($sql);

        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_USER_UPDATE' OR `key` = 'API_USER_UPDATE_SUCCESS' OR `key` = 'API_USER_UPDATE_FAILED'";
        $this->db->query($sql);
    }
}