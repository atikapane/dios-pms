<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_enroll_to_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (10, 'Course Enroll', 'be/course_management/enroll')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Course Enroll'";
        $this->db->query($sql);
    }
}