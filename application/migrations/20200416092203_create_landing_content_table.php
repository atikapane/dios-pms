<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_landing_content_table extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS landing_content";
        $this->db->query($sql);
        $sql = "CREATE TABLE `landing_content` (
            `id` int NOT NULL AUTO_INCREMENT,
            `title` VARCHAR(40) NOT NULL,
            `content` TEXT NULL,
            `priority` int NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}