<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_id_ocw_course_category_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_category` MODIFY COLUMN `ocw_category_id` int(11) NOT NULL";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `ocw_category` MODIFY COLUMN `ocw_category_id` int(11) NOT NULL AUTO_INCREMENT";
        $this->db->query($sql);
    }
}