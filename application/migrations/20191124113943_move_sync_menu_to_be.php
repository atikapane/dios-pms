<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Move_sync_menu_to_be extends CI_Migration {

    public function up()
    {
		$sql = "UPDATE `submenu` SET `submenu_link` = 'be/site_setting/menu' WHERE `submenu`.`submenu_id` = 4;";
    	$this->db->query($sql);
		$sql = "UPDATE `submenu` SET `submenu_link` = 'be/sync/user' WHERE `submenu`.`submenu_id` = 5;";
    	$this->db->query($sql);
		$sql = "UPDATE `submenu` SET `submenu_link` = 'be/sync/category' WHERE `submenu`.`submenu_id` = 6;";
    	$this->db->query($sql);
		$sql = "UPDATE `submenu` SET `submenu_link` = 'be/sync/course' WHERE `submenu`.`submenu_id` = 7;";
    	$this->db->query($sql);
		$sql = "UPDATE `submenu` SET `submenu_link` = 'be/sync/classlearning' WHERE `submenu`.`submenu_id` = 8;";
    	$this->db->query($sql);
		$sql = "UPDATE `submenu` SET `submenu_link` = 'be/sync/cdscourse' WHERE `submenu`.`submenu_id` = 9;";
    	$this->db->query($sql);
		$sql = "UPDATE `submenu` SET `submenu_link` = 'be/sync/lmscourse' WHERE `submenu`.`submenu_id` = 10;";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}