<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_transaction_be_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Create', 'USER_TRANSACTION_CREATE', 'user create transaction');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Create', 'USER_TRANSACTION_CREATE_SUCCESS', 'user create transaction success');";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Transaction', 'Create', 'USER_TRANSACTION_CREATE_FAILED', 'user create transaction failed');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES ('109');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_TRANSACTION_CREATE' OR `key` = 'USER_TRANSACTION_CREATE_SUCCESS' OR `key` = 'USER_TRANSACTION_CREATE_FAILED'";
        $this->db->query($sql);
        
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 109";
        $this->db->query($sql);
    }
}