<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_topic_readiness_exam_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `lms_exam_schedule` ADD `topic_activity` BOOLEAN NOT NULL DEFAULT 0';
        $this->db->query($sql);
        $sql = 'ALTER TABLE `lms_exam_schedule` ADD `readiness` BOOLEAN NOT NULL DEFAULT 0';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `lms_exam_schedule` DROP  COLUMN `topic_activity`";
        $this->db->query($sql);
        $sql = "ALTER TABLE `lms_exam_schedule` DROP  COLUMN `readiness`";
        $this->db->query($sql);
    }
}