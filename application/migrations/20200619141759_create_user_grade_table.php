<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_user_grade_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `user_grade` (
            `id` int NOT NULL AUTO_INCREMENT,
            `ocw_course_id` int NOT NULL,
            `user_id` int NOT NULL,
            `grade` int NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS user_grade";
        $this->db->query($sql);
    }
}