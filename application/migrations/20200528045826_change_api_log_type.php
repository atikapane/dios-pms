<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Change_api_log_type extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `log_api` CHANGE `request` `request` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `log_api` CHANGE `response` `response` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;";
        $this->db->query($sql);

    }

    public function down()
    {
		$sql = "ALTER TABLE `log_api` CHANGE `request` `request` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `log_api` CHANGE `response` `response` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;";
        $this->db->query($sql);

    }
}