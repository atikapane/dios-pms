<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_landing_banner_table extends CI_Migration {

    public function up()
    {
		$sql = "DROP TABLE IF EXISTS landing_banner";
    	$this->db->query($sql);
    	$sql = "CREATE TABLE `landing_banner` (
				  `id` int(11) NOT NULL,
				  `img_path` varchar(255) NOT NULL,
				  `url` varchar(255) NOT NULL,
				  `priority` int(11) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;
				";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}