<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_thumbnail_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `thumbnails` (
                `thumb_id` varchar(50) NOT NULL PRIMARY KEY,
                `thumb_name` varchar(100) NOT NULL,
                `created_at` timestamp NOT NULL DEFAULT current_timestamp()
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}