<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_client_table extends CI_Migration {

    public function up()
    {   
        $sql = "CREATE TABLE `client` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `nama` varchar(100) CHARACTER SET latin1 NOT NULL,
            `instansi` varchar(100) CHARACTER SET latin1 NOT NULL,
            `jabatan` varchar(100) CHARACTER SET latin1 NOT NULL,
            `no_kontak` varchar(13) CHARACTER SET latin1 NOT NULL,
            `email` varchar(100) CHARACTER SET latin1 NOT NULL,
            `consultant` int(11) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `consultant` (`consultant`)
          ) ENGINE=InnoDB;";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS client";
        $this->db->query($sql);
    }
}