<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_api_log_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `log_api` (
            `id` int NOT NULL AUTO_INCREMENT,
            `log_action_id` int NOT NULL,
            `user_id` int NULL,
            `url` VARCHAR(255) NOT NULL,
            `request` VARCHAR(255) NOT NULL,
            `response` VARCHAR(255) NULL,
            `user_agent` VARCHAR(255) NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            `ip_address` VARCHAR(20) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `log_api`";
        $this->db->query($sql);
    }
}