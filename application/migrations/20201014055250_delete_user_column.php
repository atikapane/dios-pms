<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Delete_user_column extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `user`
                  DROP `empnum`,
                  DROP `unit`, 
                  DROP `lecturercode`, 
                  DROP `structural_functional_position`,
                  DROP `structural_academic_position`,
                  DROP `c_directorate_id` ,
                  DROP `c_unit_id`,
                  DROP `mooc_id`,
                  DROP `positionlevel`;';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = 'ALTER TABLE `user`
                  ADD `institution` varchar(100) NOT NULL,
                  ADD `empnum` char(10) NOT NULL,
                  ADD `directorate` varchar(100) DEFAULT NULL ,
                  ADD `unit` varchar(100) DEFAULT NULL,
                  ADD `lecturercode` char(3) DEFAULT NULL,
                  ADD `structural_functional_position` varchar(100) DEFAULT NULL,
                  ADD `structural_academic_position` varchar(100) DEFAULT NULL,
                  ADD `c_directorate_id` int(20) DEFAULT 0,
                  ADD `c_unit_id` int(20) DEFAULT 0,
                  ADD `mooc_id` int(11) NOT NULL,
                  ADD `positionlevel` varchar(40) DEFAULT NULL;';
        $this->db->query($sql);
    }
}