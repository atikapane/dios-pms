<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_ion_auth extends CI_Migration {
    private $tables;

    public function __construct() {
        parent::__construct();
        $this->load->dbforge();

        $this->load->config('ion_auth', TRUE);
        $this->tables = $this->config->item('tables', 'ion_auth');
    }

    public function up() {
        // Drop table 'groups' if it exists
        $this->dbforge->drop_table($this->tables['groups'], TRUE);

        // Table structure for table 'groups'
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'MEDIUMINT',
                'constraint'     => '8',
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '20',
            ],
            'description' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tables['groups']);

        // Dumping data for table 'groups'
        $data = [
            [
                'name'        => 'admin',
                'description' => 'Administrator'
            ],
            [
                'name'        => 'members',
                'description' => 'General User'
            ]
        ];
        $this->db->insert_batch($this->tables['groups'], $data);

        // Drop table 'users' if it exists
        $this->dbforge->drop_table($this->tables['users'], TRUE);

        // Table structure for table 'users'
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'MEDIUMINT',
                'constraint'     => '8',
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'ip_address' => [
                'type'       => 'VARCHAR',
                'constraint' => '45'
            ],
            'oauth_provider' => [
                'type'       => 'varchar',
                'constraint' => '15',
                'unsigned'   => TRUE,
                'null'       => TRUE
            ],
            'oauth_uid' => [
                'type'       => 'varchar',
                'constraint' => '50',
                'unsigned'   => TRUE,
                'null'       => TRUE
            ],
            'username' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'password' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'email' => [
                'type'       => 'VARCHAR',
                'constraint' => '254',
                'unique' => TRUE
            ],
            'full_name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'       => TRUE
            ],
            'country_id' => [
                'type'       => 'int',
                'constraint' => '11',
                'null'       => TRUE
            ],
            'city_id' => [
                'type'       => 'int',
                'constraint' => '11',
                'null'       => TRUE
            ],
            'gender' => [
                'type'       => 'int',
                'constraint' => '11',
                'null'       => TRUE
            ],
            'dateBirth' => [
                'type'       => 'date',
                'null'       => TRUE
            ],
            'activation_selector' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => TRUE,
                'unique' => TRUE
            ],
            'activation_code' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => TRUE
            ],
            'forgotten_password_selector' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => TRUE,
                'unique' => TRUE
            ],
            'forgotten_password_code' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => TRUE
            ],
            'forgotten_password_time' => [
                'type'       => 'INT',
                'constraint' => '11',
                'unsigned'   => TRUE,
                'null'       => TRUE
            ],
            'remember_selector' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => TRUE,
                'unique' => TRUE
            ],
            'remember_code' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
                'null'       => TRUE
            ],
            'last_login' => [
                'type'       => 'INT',
                'constraint' => '11',
                'unsigned'   => TRUE,
                'null'       => TRUE
            ],
            'active' => [
                'type'       => 'TINYINT',
                'constraint' => '1',
                'unsigned'   => TRUE,
                'null'       => TRUE
            ],
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tables['users']);

        // Dumping data for table 'users'
        $data = [
            'ip_address'              => '127.0.0.1',
            'username'                => 'administrator',
            'password'                => '$2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa',
            'email'                   => 'admin@admin.com',
            'activation_code'         => '',
            'forgotten_password_code' => NULL,
            'last_login'              => '1268889823',
            'active'                  => '1',
            'full_name'              => 'Administrator'
        ];
        $this->db->insert($this->tables['users'], $data);


        // Drop table 'users_groups' if it exists
        $this->dbforge->drop_table($this->tables['users_groups'], TRUE);

        // Table structure for table 'users_groups'
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'MEDIUMINT',
                'constraint'     => '8',
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'user_id' => [
                'type'       => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned'   => TRUE
            ],
            'group_id' => [
                'type'       => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned'   => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tables['users_groups']);

        // Dumping data for table 'users_groups'
        $data = [
            [
                'user_id'  => '1',
                'group_id' => '1',
            ],
            [
                'user_id'  => '1',
                'group_id' => '2',
            ]
        ];
        $this->db->insert_batch($this->tables['users_groups'], $data);


        // Drop table 'login_attempts' if it exists
        $this->dbforge->drop_table($this->tables['login_attempts'], TRUE);

        // Table structure for table 'login_attempts'
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'MEDIUMINT',
                'constraint'     => '8',
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'ip_address' => [
                'type'       => 'VARCHAR',
                'constraint' => '45'
            ],
            'login' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'       => TRUE
            ],
            'time' => [
                'type'       => 'INT',
                'constraint' => '11',
                'unsigned'   => TRUE,
                'null'       => TRUE
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tables['login_attempts']);

        $sql = "ALTER TABLE users
                ADD COLUMN `created_on` DATETIME DEFAULT CURRENT_TIMESTAMP,
                ADD COLUMN `updated_on` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";

        $this->db->query($sql);

    }

    public function down() {
        $this->dbforge->drop_table($this->tables['users'], TRUE);
        $this->dbforge->drop_table($this->tables['groups'], TRUE);
        $this->dbforge->drop_table($this->tables['users_groups'], TRUE);
        $this->dbforge->drop_table($this->tables['login_attempts'], TRUE);
    }
}
