<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_project_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `project` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `nama_project` varchar(100) CHARACTER SET latin1 NOT NULL,
            `tanggal_mulai` date NOT NULL,
            `tanggal_selesai` date NOT NULL,
            `client_id` int(11) NOT NULL,
            `consultant` int(11) NOT NULL,
            `quotation` text CHARACTER SET latin1 NOT NULL,
            `spk` text CHARACTER SET latin1 NOT NULL,
            PRIMARY KEY (`id`),
            KEY `consultant` (`consultant`),
            KEY `client_id` (`client_id`),
            CONSTRAINT `project_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
          ) ENGINE=InnoDB;";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS project";
        $this->db->query($sql);
    }
}