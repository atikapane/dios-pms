<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_is_manual_transaction_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `transaction` ADD `is_manual` BOOLEAN NOT NULL DEFAULT FALSE AFTER `total_price`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `transaction` DROP  COLUMN `is_manual`;";
        $this->db->query($sql);
    }
}