<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_log_user_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `log_user` MODIFY `user_id` int DEFAULT NULL";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}