<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_new_column_ocw_category extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `ocw_category` ADD `name` VARCHAR(150) NOT NULL AFTER `ocw_category_id`, ADD `program_id` INT NOT NULL AFTER `name`;";
        $this->db->query($sql);

        $sql = "ALTER TABLE `sync_category` DROP  COLUMN `name`, DROP COLUMN `study_program_id`;";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}