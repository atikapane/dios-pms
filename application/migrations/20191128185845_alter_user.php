<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_user extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `user` MODIFY COLUMN `email`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '' AFTER `fullname`;";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}