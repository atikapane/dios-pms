<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_log_api_external_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `log_api_external` (
            `id` int NOT NULL AUTO_INCREMENT,
            `log_action_id` int NOT NULL,
            `user_id` int NULL,
            `url` VARCHAR(255) NOT NULL,
            `request` VARCHAR(255) NOT NULL,
            `response` VARCHAR(255) NULL,
            `user_agent` VARCHAR(255) NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            `ip_address` VARCHAR(20) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
          $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `log_api_external`";
        $this->db->query($sql);
    }
}