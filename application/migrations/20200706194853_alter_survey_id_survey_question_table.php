<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_survey_id_survey_question_table extends CI_Migration {

    public function up()
    {
        $sql = 'ALTER TABLE `survey_question` ADD `survey_id` int NOT NULL AFTER `id`';
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `survey_question` DROP  COLUMN `survey_id`";
        $this->db->query($sql);
    }
}