<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_ocw_course_ceritification_column extends CI_Migration {

    public function up()
    {
    	$sql = "ALTER TABLE `ocw_course` ADD `certification` TEXT NOT NULL AFTER `description`;";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}