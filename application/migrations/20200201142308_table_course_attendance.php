<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Table_course_attendance extends CI_Migration {

    public function up()
    {
    	$sql = "CREATE TABLE `config_semester` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `semester` varchar(6) NOT NULL,
		  `date_start` date NOT NULL,
		  `date_end` date NOT NULL,
		  `status_active` tinyint(3) NOT NULL DEFAULT 0,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`semester`,`date_start`,`date_end`,`status_active`) USING BTREE
		)";
		$this->db->query($sql);

		$sql = "INSERT INTO `config_semester` VALUES ('1', '1920/1', '2019-07-01', '2019-12-31', '0');";
		$this->db->query($sql);
		
		$sql = "INSERT INTO `config_semester` VALUES ('2', '1920/2', '2020-01-01', '2020-06-30', '1');";
		$this->db->query($sql);

		$sql = "CREATE TABLE `course_attendance_report` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) NOT NULL,
		  `section_id` int(11) NOT NULL,
		  `section_name` varchar(255) NOT NULL,
		  `date_start` date NOT NULL,
		  `date_end` date NOT NULL,
		  `num_login` int(11) NOT NULL DEFAULT 0,
		  `num_forum` int(11) NOT NULL DEFAULT 0,
		  `num_grading` int(11) NOT NULL DEFAULT 0,
		  `attendance` tinyint(3) NOT NULL DEFAULT 0,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`section_id`,`date_start`,`date_end`,`attendance`) USING BTREE,
		  KEY `statistic` (`num_login`,`num_forum`,`num_grading`) USING BTREE
		)";
		$this->db->query($sql);

		$sql = "CREATE TABLE `course_attendance_statistic` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) NOT NULL,
		  `section_id` int(11) NOT NULL,
		  `date` date NOT NULL,
		  `num_login` int(11) NOT NULL DEFAULT 0,
		  `num_forum` int(11) NOT NULL DEFAULT 0,
		  `num_grading` int(11) NOT NULL DEFAULT 0,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`section_id`,`date`) USING BTREE,
		  KEY `statistic` (`num_login`,`num_forum`,`num_grading`) USING BTREE
		)";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}