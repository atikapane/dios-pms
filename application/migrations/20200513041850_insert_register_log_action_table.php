<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_register_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Authentication', 'Register', 'USER_FRONTEND_REGISTER', 'event log user frontend register')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Authentication', 'Register', 'USER_FRONTEND_REGISTER_SUCSESS', 'event log user frontend register success')";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('Authentication', 'Register', 'USER_FRONTEND_REGISTER_FAILED', 'event log user frontend register failed')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'USER_FRONTEND_REGISTER' OR `key` = 'USER_FRONTEND_REGISTER_SUCCESS' OR `key` = 'USER_FRONTEND_REGISTER_FAILED'";
        $this->db->query($sql);
    }
}