<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_data_landing_billing extends CI_Migration {

    public function up()
    {

        $sql = "INSERT INTO `landing_billing` (`id`, `title`, `content`, `priority`) VALUES (NULL, 'BNI Mobile', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `landing_billing` (`id`, `title`, `content`, `priority`) VALUES (NULL, 'Internet Banking BNI', 'Viverra nam libero justo laoreet sit amet cursus sit amet. Velit scelerisque in dictum non consectetur a erat nam. Dictumst quisque sagittis purus sit amet. Vivamus at augue eget arcu dictum varius duis at consectetur. Eu ultrices vitae auctor eu augue ut lectus arcu. Neque convallis a cras semper auctor. Tellus id interdum velit laoreet id. Neque egestas congue quisque egestas diam in arcu cursus euismod.', '2');";
        $this->db->query($sql);

        $sql = "INSERT INTO `landing_billing` (`id`, `title`, `content`, `priority`) VALUES (NULL, 'Kantor Cabang BNI', 'Fames ac turpis egestas maecenas pharetra convallis posuere. Pharetra sit amet aliquam id diam maecenas ultricies. Et pharetra pharetra massa massa ultricies mi quis. At elementum eu facilisis sed odio morbi quis commodo odio. Pellentesque adipiscing commodo elit at imperdiet dui accumsan sit amet. Feugiat in ante metus dictum at tempor. Pretium vulputate sapien nec sagittis aliquam malesuada bibendum arcu vitae.', '3');";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}