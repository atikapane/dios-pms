<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_log_user_action_table extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS log_action";
        $this->db->query($sql);

        $sql = "CREATE TABLE `log_action` (
            `id` int NOT NULL AUTO_INCREMENT,
            `key` VARCHAR(255) NOT NULL,
            `description` VARCHAR(255) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}