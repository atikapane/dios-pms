<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_transaction_item_table extends CI_Migration {

    public function up()
    {
    	$sql = "DROP TABLE IF EXISTS transaction_item";
    	$this->db->query($sql);

        $sql = "CREATE TABLE `transaction_item` ( `id` INT NOT NULL AUTO_INCREMENT , `course_id` INT NOT NULL , `course_title` VARCHAR(255) NOT NULL , `course_description` TEXT NOT NULL , `price` BIGINT NOT NULL , `invoice_number` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->db->query($sql);

		$sql = 'ALTER TABLE `transaction` DROP `course_id`, DROP `course_title`, DROP `course_description`, DROP `price`;';
        $this->db->query($sql);

		$sql = 'ALTER TABLE `transaction` ADD `total_price` BIGINT NOT NULL AFTER `bni_va_number`;';
        $this->db->query($sql);

    }

    public function down()
    {
    	$sql = "DROP TABLE IF EXISTS transaction_item";
    	$this->db->query($sql);

        $sql = "ALTER TABLE `transaction` ADD `course_id` INT NOT NULL;";
    	$this->db->query($sql);

        $sql = "ALTER TABLE `transaction` ADD `course_title` VARCHAR(255) NOT NULL;";
    	$this->db->query($sql);

        $sql = "ALTER TABLE `transaction` ADD `course_description` TEXT NOT NULL;";
    	$this->db->query($sql);

        $sql = "ALTER TABLE `transaction` ADD `price` BIGINT  NOT NULL;";
    	$this->db->query($sql);


		$sql = 'ALTER TABLE `transaction` DROP `total_price`;';
        $this->db->query($sql);
    }
}