<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_survey_menu_submenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `menu` (`menu_name`, `menu_icon`) VALUES ('Survey', 'fa-poll');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (" . $insert_id . ", 'Lecture Survey', 'be/survey/lecture');";
        $this->db->query($sql);
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (" . $insert_id . ", 'Service Survey', 'be/survey/service');";
        $this->db->query($sql);
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (" . $insert_id . ", 'Lecture Survey Report', 'be/survey/lecture_report');";
        $this->db->query($sql);
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (" . $insert_id . ", 'Service Survey Report', 'be/survey/service_report');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `menu` WHERE `menu_name` = 'Survey'";
        $this->db->query($sql);
        $sql = "DELETE FROM `submenu` WHERE `submenu_link` = 'be/survey/lecture' OR `submenu_link` = 'be/survey/service' OR  `submenu_link` = 'be/survey/report/lecture' OR `submenu_link` = 'be/survey/report/service'";
        $this->db->query($sql);
    }
}