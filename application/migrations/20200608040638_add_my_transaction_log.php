<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_my_transaction_log extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'My Transaction', 'Transaction exist', 'API_MY_TRANSACTION', 'event log if filtered tx exist');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '99', '1');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_action` (`id`, `category`, `sub_category`, `key`, `description`) VALUES (NULL, 'My Transaction not found', 'Transaction not found', 'API_MY_TRANSACTION_FAILED', 'event log if filtered tx not found');";
        $this->db->query($sql);

        $sql = "INSERT INTO `log_setting` (`id`, `log_action_id`, `is_log`) VALUES (NULL, '100', '1');";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_MY_TRANSACTION' or `key` = 'API_MY_TRANSACTION_FAILED' " ;
        $this->db->query($sql);
        $sql = "DELETE FROM `log_setting` WHERE `log_action_id` = 99 or `log_action_id` = 100";
        $this->db->query($sql);
    }
}