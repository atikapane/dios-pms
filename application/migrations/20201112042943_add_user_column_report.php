<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_report_user_column extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `report` ADD `user_id` int NOT NULL AFTER `project`";
        $this->db->query($sql);

        $sql = "ALTER TABLE report
                ADD CONSTRAINT FK_UserReport
                FOREIGN KEY (user_id) REFERENCES User(id);";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `report` DROP COLUMN `user_id`";
        $this->db->query($sql);

        $sql = "ALTER TABLE report
                DROP FOREIGN KEY FK_UserReport;";
        $this->db->query($sql);
    }
}