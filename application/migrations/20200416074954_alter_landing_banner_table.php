<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_landing_banner_table extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `landing_banner` ADD `type` VARCHAR(40) NOT NULL AFTER `id`";
        $this->db->query($sql);

        $sql = "ALTER TABLE `landing_banner` ADD `description` VARCHAR(255) NOT NULL AFTER `priority`";
        $this->db->query($sql);

        $sql = "ALTER TABLE `landing_banner` MODIFY `img_path` VARCHAR(255) DEFAULT NULL";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}