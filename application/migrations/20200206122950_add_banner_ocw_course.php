<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_banner_ocw_course extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE `ocw_course` ADD `banner_image` VARCHAR(255) NOT NULL AFTER `end_date`";
    	$this->db->query($sql);
    }

    public function down()
    {

    }
}