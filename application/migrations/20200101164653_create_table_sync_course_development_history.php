<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_table_sync_course_development_history extends CI_Migration {

    public function up()
    {
		$sql = "ALTER TABLE `sync_lms_course_teaching_history`
		  CHANGE COLUMN `empnum` `user_id`  int(11) NOT NULL AFTER `id`;";
		$this->db->query($sql);

		$sql = "CREATE TABLE `sync_course_development_history` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `user_id` int(11) NOT NULL,
		  `subject_id` int(11) NOT NULL,
		  `date` datetime NOT NULL DEFAULT current_timestamp(),
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`user_id`,`subject_id`) USING BTREE
		);";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}