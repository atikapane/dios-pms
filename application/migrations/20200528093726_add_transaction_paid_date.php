<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_transaction_paid_date extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE `transaction` ADD `paid_date` DATETIME NOT NULL AFTER `status`;";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "ALTER TABLE `transaction` DROP  COLUMN `paid_date`;";
        $this->db->query($sql);
    }
}