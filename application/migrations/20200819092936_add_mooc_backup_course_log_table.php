<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_mooc_backup_course_log_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('MOOC', 'Backup Course', 'API_EXTERNAL_MOOC_BACKUP_COURSE', 'api to backup course mooc');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_MOOC_BACKUP_COURSE'";
        $this->db->query($sql);
    }
}