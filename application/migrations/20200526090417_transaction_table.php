<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Transaction_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `transaction` ( `id` INT NOT NULL AUTO_INCREMENT , `invoice_number` VARCHAR(100) NOT NULL , `expired_at` DATETIME NOT NULL , `course_id` INT NOT NULL , `course_title` VARCHAR(255) NOT NULL , `course_description` TEXT NOT NULL , `status` INT NOT NULL COMMENT '0 = waiting, 1 = expired, 2 = paid, 3 = cancelled' , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->db->query($sql);
        $sql = "ALTER TABLE `transaction` ADD `user_id` INT NOT NULL AFTER `status`, ADD `username` VARCHAR(100) NOT NULL AFTER `user_id`, ADD `user_full_name` VARCHAR(100) NOT NULL AFTER `username`, ADD `user_email` VARCHAR(254) NOT NULL AFTER `user_full_name`, ADD `user_country` VARCHAR(255) NOT NULL AFTER `user_email`, ADD `user_city` VARCHAR(255) NOT NULL AFTER `user_country`, ADD `user_gender` INT NOT NULL AFTER `user_city`, ADD `user_birth_date` DATE NOT NULL AFTER `user_gender`, ADD `user_phone` VARCHAR(20) NOT NULL AFTER `user_birth_date`;";
        $this->db->query($sql);

    }

    public function down()
    {
    	$sql = "DROP TABLE IF EXISTS transaction";
    	$this->db->query($sql);
    }
}