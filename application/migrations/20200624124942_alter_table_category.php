<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_category extends CI_Migration {

    public function up()
    {

    	$sql = "ALTER TABLE `sync_category` DROP COLUMN IF EXISTS  `initial_studyprogram` ";
        $this->db->query($sql);

    	$sql = "ALTER TABLE `sync_category` ADD COLUMN `initial_studyprogram`  varchar(10) NULL AFTER `category_type`;";
        $this->db->query($sql);
    }

    public function down()
    {

    	$sql = "ALTER TABLE `sync_category` DROP COLUMN IF EXISTS  `initial_studyprogram` ";
        $this->db->query($sql);
    }
}