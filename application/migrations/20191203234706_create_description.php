<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_description extends CI_Migration {

    public function up()
    {
        $sql = "ALTER TABLE news MODIFY COLUMN description longtext;";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}