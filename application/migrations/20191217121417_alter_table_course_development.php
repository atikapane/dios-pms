<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_table_course_development extends CI_Migration {

    public function up()
    {
    	$sql = 'ALTER TABLE `course_development`
    		ADD COLUMN `creator_list`  text NULL AFTER `subject_id`,
			ADD COLUMN `reviewer_list`  text NULL AFTER `creator_list`,
			ADD COLUMN `status_course`  tinyint(3) NOT NULL DEFAULT 1 AFTER `updated_by`,
			ADD COLUMN `user_approve`  char(20) NULL AFTER `date_approve`,
			ADD COLUMN `deleted_date`  datetime NULL AFTER `updated_by`,
			ADD COLUMN `deleted_by`  char(20) NULL AFTER `deleted_date`,
			DROP INDEX `index` ,
			ADD INDEX `index` (`year`, `quarter`, `subject_id`, `status_approve`, `status_course`, `status_review`) USING BTREE ;';
		$this->db->query($sql);

		$sql = 'ALTER TABLE `enrol_development`
			ADD COLUMN `enr_subject_id`  int(11) NULL AFTER `is_leader`,
			DROP INDEX `index` ,
			ADD INDEX `index` (`course_id`, `employeeid`, `type_id`, `enr_subject_id`) USING BTREE ;';
		$this->db->query($sql);	

		$sql = 'ALTER TABLE `course_development`
			DROP COLUMN `status_review`;';
		$this->db->query($sql);	

		$sql = 'CREATE TABLE `course_review` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `course_id` int(11) NOT NULL,
		  `employeeid` char(20) NOT NULL,
		  `total_score` int(11) NOT NULL,
		  `date_review` date NOT NULL,
		  `status_review` tinyint(3) NOT NULL DEFAULT 0,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`course_id`,`employeeid`,`status_review`) USING BTREE
		)';
		$this->db->query($sql);	

		$sql = 'CREATE TABLE `course_review_sheet` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `review_id` int(11) NOT NULL,
		  `rubric_id` int(11) NOT NULL,
		  `review_score` int(11) NOT NULL,
		  `review_note` text DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  UNIQUE KEY `unique` (`id`) USING BTREE,
		  KEY `index` (`review_id`,`rubric_id`) USING BTREE
		)';
		$this->db->query($sql);	

		$sql = 'TRUNCATE TABLE `menu`';
		$this->db->query($sql);	

		$sql = "INSERT INTO `menu` VALUES ('1', 'User Management', null, 'fa-user');";
		$this->db->query($sql);
		$sql = "INSERT INTO `menu` VALUES ('2', 'Site Setting', '', 'fa-cogs');";
		$this->db->query($sql);
		$sql = "INSERT INTO `menu` VALUES ('3', 'Sync Master Data', '', 'fa-database');";
		$this->db->query($sql);
		$sql = "INSERT INTO `menu` VALUES ('4', 'CDS', null, 'fa-book');";
		$this->db->query($sql);
		$sql = "INSERT INTO `menu` VALUES ('5', 'LMS', null, 'fa-chalkboard-teacher');";
		$this->db->query($sql);
		$sql = "INSERT INTO `menu` VALUES ('6', 'OCW', '', 'fa-home');";
		$this->db->query($sql);
		$sql = "INSERT INTO `menu` VALUES ('7', 'Report', '', 'fa-chart-line');";
		$this->db->query($sql);
		$sql = "INSERT INTO `menu` VALUES ('8', 'Course Management', null, 'fa-swatchbook');";
		$this->db->query($sql);

		$sql = 'TRUNCATE TABLE `submenu`';
		$this->db->query($sql);	

		$sql = "INSERT INTO `submenu` VALUES ('1', '1', 'User List', 'be/user_management/Users', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('2', '1', 'Group List', 'be/user_management/Group', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('3', '1', 'Privilege List', 'be/user_management/Previlege', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('4', '2', 'Menu List', 'be/site_setting/Menu', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('5', '3', 'User', 'be/sync/User', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('6', '3', 'Category', 'be/sync/Category', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('7', '3', 'Course Development', 'be/sync/Course_cds', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('8', '3', 'Course Teaching', 'be/sync/Course_lms', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('9', '4', 'Sync to CDS', 'be/sync/Cds_sync', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('10', '5', 'Sync to LMS', 'be/sync/Lms_sync', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('11', '6', 'Banner', 'be/landing/Banner', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('12', '6', 'News', 'be/landing/News', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('13', '6', 'Download', 'be/landing/Download', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('14', '7', 'Beban Pengembangan Course', 'be/report/Course_development', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('15', '2', 'Submenu', 'be/site_setting/Submenu', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('16', '8', 'Course Development', 'be/course_management/Course_development', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('17', '8', 'Course Teaching', 'be/course_management/Course_teaching', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('18', '8', 'Course Review', 'be/course_management/Course_review', null);";
		$this->db->query($sql);
		$sql = "INSERT INTO `submenu` VALUES ('19', '8', 'Criteria Review', 'be/course_management/Criteria_review', null);";
		$this->db->query($sql);
    }

    public function down()
    {

    }
}