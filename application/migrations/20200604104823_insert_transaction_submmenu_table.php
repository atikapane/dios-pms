<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_transaction_submmenu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (13, 'Transaction', 'be/billing/transaction')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Transaction'";
        $this->db->query($sql);
    }
}