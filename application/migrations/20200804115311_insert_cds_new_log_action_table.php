<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_cds_new_log_action_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Check User', 'API_EXTERNAL_CDS_CHECK_USER', 'check user cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Enroll User', 'API_EXTERNAL_CDS_ENROLL_USER', 'enroll user to cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Unenroll User', 'API_EXTERNAL_CDS_UNENROLL_USER', 'unenroll user from cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
        $sql = "INSERT INTO `log_action` (`category`, `sub_category`, `key`, `description`) VALUES ('CDS', 'Create User', 'API_EXTERNAL_CDS_CREATE_USER', 'create user to cds');"; 
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();
        $sql = "INSERT INTO `log_setting` (`log_action_id`) VALUES (" . $insert_id . ");";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `log_action` WHERE `key` = 'API_EXTERNAL_CDS_CHECK_USER' OR `key` = 'API_EXTERNAL_CDS_ENROLL_USER' OR `key` = 'API_EXTERNAL_CDS_UNENROLL_USER' OR `key` = 'API_EXTERNAL_CDS_CREATE_USER'";
        $this->db->query($sql);
    }
}