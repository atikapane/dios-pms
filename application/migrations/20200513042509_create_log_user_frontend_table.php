<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_log_user_frontend_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `log_user_frontend` (
            `id` int NOT NULL AUTO_INCREMENT,
            `log_action_id` int NOT NULL,
            `user_id` int NULL,
            `data` TEXT NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            `ip_address` VARCHAR(20) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS `log_user_frontend`";
        $this->db->query($sql);
    }
}