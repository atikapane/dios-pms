<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Alter_enroll_user_log_table extends CI_Migration {

    public function up()
    {  
        $sql = "UPDATE `log_action` SET `sub_category` = 'Enroll User' WHERE `key` = 'API_EXTERNAL_MOOC_ENROLL_USER'";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}