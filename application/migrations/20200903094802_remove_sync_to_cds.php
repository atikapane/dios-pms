<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Remove_sync_to_cds extends CI_Migration {

    public function up()
    {

    }

    public function down()
    {
        $sql = "SELECT menuid FROM submenu WHERE submenu_link = 'be/sync/cds_sync'";
        $menu_id = $this->db->query($sql)->row()->menuid;

        $sql = "DELETE FROM submenu WHERE submenu_link = 'be/sync/cds_sync'";
        $this->db->query($sql);

        $sql = "DELETE FROM menu WHERE menu_id = " . $menu_id;
        $this->db->query($sql);
    }
}