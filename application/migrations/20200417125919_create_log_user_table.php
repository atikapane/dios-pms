<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_log_user_table extends CI_Migration {

    public function up()
    {
        $sql = "DROP TABLE IF EXISTS log_user";
        $this->db->query($sql);

        $sql = "CREATE TABLE `log_user` (
            `id` int NOT NULL AUTO_INCREMENT,
            `log_action_id` int NOT NULL,
            `user_id` int NOT NULL,
            `data` TEXT NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            `ip_address` VARCHAR(20) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->db->query($sql);
    }

    public function down()
    {

    }
}