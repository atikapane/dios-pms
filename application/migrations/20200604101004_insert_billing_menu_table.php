<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Insert_billing_menu_table extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `menu` (`menu_name`, `menu_icon`, `priority`) VALUES ('Billing', 'fa-file-invoice-dollar', 1)";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `menu` WHERE `menu_name` = 'Billing'";
        $this->db->query($sql);
    }
}