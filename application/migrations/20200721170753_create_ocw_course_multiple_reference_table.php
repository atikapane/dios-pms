<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ocw_course_multiple_reference_table extends CI_Migration {

    public function up()
    {
        $sql = "CREATE TABLE `reference_multiple_course` (
            `id` int NOT NULL AUTO_INCREMENT,
            `course_reference_id` INT NOT NULL,
            `course_target_id` INT NOT NULL,
            `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DROP TABLE IF EXISTS reference_multiple_course";
        $this->db->query($sql);
    }
}