<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_submenu_sales_report extends CI_Migration {

    public function up()
    {
        $sql = "INSERT INTO `submenu` (`menuid`, `submenu_name`, `submenu_link`) VALUES (6, 'Billing Content', 'be/landing/Billing')";
        $this->db->query($sql);
    }

    public function down()
    {
        $sql = "DELETE FROM `submenu` WHERE `submenu_name` = 'Billing Content'";
        $this->db->query($sql);
    }
}