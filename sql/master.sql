/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.4.6-MariaDB : Database - celeodb
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `groups` */

insert  into `groups`(`group_id`,`group_name`) values (1,'Administrator'),(2,'Manager'),(3,'Course Manager'),(4,'Teacher'),(5,'Student');

/*Table structure for table `icons` */

DROP TABLE IF EXISTS `icons`;

CREATE TABLE `icons` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=973 DEFAULT CHARSET=latin1;

/*Data for the table `icons` */

insert  into `icons`(`Id`,`Name`) values (1,'fa-ad'),(2,'fa-adjust'),(3,'fa-anchor'),(4,'fa-angry'),(5,'fa-ankh'),(6,'fa-archive'),(7,'fa-archway'),(8,'fa-at'),(9,'fa-atlas'),(10,'fa-atom'),(11,'fa-award'),(12,'fa-baby'),(13,'fa-bacon'),(14,'fa-ban'),(15,'fa-barcode'),(16,'fa-bars'),(17,'fa-bath'),(18,'fa-bed'),(19,'fa-beer'),(20,'fa-bell'),(21,'fa-bible'),(22,'fa-bicycle'),(23,'fa-biking'),(24,'fa-blender'),(25,'fa-blind'),(26,'fa-blog'),(27,'fa-bold'),(28,'fa-bolt'),(29,'fa-bomb'),(30,'fa-bone'),(31,'fa-bong'),(32,'fa-book'),(33,'fa-box'),(34,'fa-boxes'),(35,'fa-braille'),(36,'fa-brain'),(37,'fa-broom'),(38,'fa-brush'),(39,'fa-buffer'),(40,'fa-bug'),(41,'fa-burn'),(42,'fa-bus'),(43,'fa-bus-alt'),(44,'fa-camera'),(45,'fa-car'),(46,'fa-car-alt'),(47,'fa-carrot'),(48,'fa-cat'),(49,'fa-chair'),(50,'fa-check'),(51,'fa-cheese'),(52,'fa-chess'),(53,'fa-child'),(54,'fa-church'),(55,'fa-circle'),(56,'fa-city'),(57,'fa-clock'),(58,'fa-clone'),(59,'fa-cloud'),(60,'fa-code'),(61,'fa-coffee'),(62,'fa-cog'),(63,'fa-cogs'),(64,'fa-coins'),(65,'fa-columns'),(66,'fa-comment'),(67,'fa-compass'),(68,'fa-cookie'),(69,'fa-copy'),(70,'fa-couch'),(71,'fa-crop'),(72,'fa-cross'),(73,'fa-crow'),(74,'fa-crown'),(75,'fa-crutch'),(76,'fa-cube'),(77,'fa-cubes'),(78,'fa-cut'),(79,'fa-deaf'),(80,'fa-desktop'),(81,'fa-dice'),(82,'fa-dice-d6'),(83,'fa-divide'),(84,'fa-dizzy'),(85,'fa-dna'),(86,'fa-dog'),(87,'fa-dolly'),(88,'fa-donate'),(89,'fa-dove'),(90,'fa-dran'),(91,'fa-drum'),(92,'fa-dungeon'),(93,'fa-edit'),(94,'fa-egg'),(95,'fa-eject'),(96,'fa-equals'),(97,'fa-eraser'),(98,'fa-expand'),(99,'fa-eye'),(100,'fa-fan'),(101,'fa-fax'),(102,'fa-feather'),(103,'fa-female'),(104,'fa-file'),(105,'fa-fill'),(106,'fa-film'),(107,'fa-filter'),(108,'fa-fire'),(109,'fa-fish'),(110,'fa-flag'),(111,'fa-flask'),(112,'fa-flushed'),(113,'fa-folder'),(114,'fa-font'),(115,'fa-forward'),(116,'fa-frog'),(117,'fa-frown'),(118,'fa-futbol'),(119,'fa-gamepad'),(120,'fa-gavel'),(121,'fa-gem'),(122,'fa-ghost'),(123,'fa-gift'),(124,'fa-gifts'),(125,'fa-git-alt'),(126,'fa-glasses'),(127,'fa-globe'),(128,'fa-puram'),(129,'fa-grimace'),(130,'fa-grin'),(131,'fa-guitar'),(132,'fa-hammer'),(133,'fa-hamsa'),(134,'fa-hands'),(135,'fa-hashtag'),(136,'fa-haykal'),(137,'fa-hdd'),(138,'fa-heading'),(139,'fa-headset'),(140,'fa-heart'),(141,'fa-hiking'),(142,'fa-hippo'),(143,'fa-history'),(144,'fa-home'),(145,'fa-horse'),(146,'fa-hot-tub'),(147,'fa-hotdog'),(148,'fa-hotel'),(149,'fa-hryvnia'),(150,'fa-icicles'),(151,'fa-icons'),(152,'fa-id-card'),(153,'fa-igloo'),(154,'fa-image'),(155,'fa-images'),(156,'fa-inbox'),(157,'fa-indent'),(158,'fa-info'),(159,'fa-italic'),(160,'fa-jedi'),(161,'fa-joint'),(162,'fa-kaaba'),(163,'fa-key'),(164,'fa-khanda'),(165,'fa-kiss'),(166,'fa-laptop'),(167,'fa-laugh'),(168,'fa-leaf'),(169,'fa-lemon'),(170,'fa-link'),(171,'fa-list'),(172,'fa-list-ol'),(173,'fa-list-ul'),(174,'fa-lock'),(175,'fa-magic'),(176,'fa-magnet'),(177,'fa-male'),(178,'fa-map'),(179,'fa-map-pin'),(180,'fa-marker'),(181,'fa-mars'),(182,'fa-mask'),(183,'fa-mdb'),(184,'fa-medal'),(185,'fa-medkit'),(186,'fa-meh'),(187,'fa-memory'),(188,'fa-menorah'),(189,'fa-mercury'),(190,'fa-meteor'),(191,'fa-minus'),(194,'fa-clock'),(195,'fa-clone'),(197,'fa-cloud'),(208,'fa-code'),(210,'fa-coffee'),(211,'fa-cog'),(212,'fa-cogs'),(213,'fa-coins'),(214,'fa-columns'),(215,'fa-comment'),(224,'fa-compass'),(228,'fa-cookie'),(230,'fa-copy'),(233,'fa-couch'),(235,'fa-crop'),(237,'fa-cross'),(239,'fa-crow'),(240,'fa-crown'),(241,'fa-crutch'),(242,'fa-cube'),(243,'fa-cubes'),(244,'fa-cut'),(246,'fa-deaf'),(248,'fa-desktop'),(251,'fa-dice'),(253,'fa-dice-d6'),(262,'fa-divide'),(263,'fa-dizzy'),(264,'fa-dna'),(265,'fa-dog'),(267,'fa-dolly'),(269,'fa-donate'),(273,'fa-dove'),(276,'fa-dran'),(278,'fa-drum'),(284,'fa-dungeon'),(285,'fa-edit'),(286,'fa-egg'),(287,'fa-eject'),(294,'fa-equals'),(295,'fa-eraser'),(302,'fa-expand'),(306,'fa-eye'),(309,'fa-fan'),(312,'fa-fax'),(313,'fa-feather'),(315,'fa-female'),(317,'fa-file'),(340,'fa-fill'),(342,'fa-film'),(343,'fa-filter'),(345,'fa-fire'),(349,'fa-fish'),(351,'fa-flag'),(354,'fa-flask'),(355,'fa-flushed'),(356,'fa-folder'),(360,'fa-font'),(362,'fa-forward'),(363,'fa-frog'),(364,'fa-frown'),(367,'fa-futbol'),(368,'fa-gamepad'),(370,'fa-gavel'),(371,'fa-gem'),(373,'fa-ghost'),(374,'fa-gift'),(375,'fa-gifts'),(376,'fa-git-alt'),(381,'fa-glasses'),(382,'fa-globe'),(388,'fa-puram'),(392,'fa-grimace'),(393,'fa-grin'),(410,'fa-guitar'),(413,'fa-hammer'),(414,'fa-hamsa'),(430,'fa-hands'),(435,'fa-hashtag'),(439,'fa-haykal'),(440,'fa-hdd'),(441,'fa-heading'),(444,'fa-headset'),(445,'fa-heart'),(450,'fa-hiking'),(451,'fa-hippo'),(452,'fa-history'),(455,'fa-home'),(456,'fa-horse'),(461,'fa-hot-tub'),(462,'fa-hotdog'),(463,'fa-hotel'),(469,'fa-hryvnia'),(472,'fa-icicles'),(473,'fa-icons'),(475,'fa-id-card'),(477,'fa-igloo'),(478,'fa-image'),(479,'fa-images'),(480,'fa-inbox'),(481,'fa-indent'),(484,'fa-info'),(486,'fa-italic'),(487,'fa-jedi'),(488,'fa-joint'),(490,'fa-kaaba'),(491,'fa-key'),(493,'fa-khanda'),(494,'fa-kiss'),(500,'fa-laptop'),(503,'fa-laugh'),(508,'fa-leaf'),(509,'fa-lemon'),(516,'fa-link'),(518,'fa-list'),(520,'fa-list-ol'),(521,'fa-list-ul'),(523,'fa-lock'),(531,'fa-magic'),(532,'fa-magnet'),(534,'fa-male'),(535,'fa-map'),(540,'fa-map-pin'),(542,'fa-marker'),(543,'fa-mars'),(548,'fa-mask'),(549,'fa-mdb'),(550,'fa-medal'),(551,'fa-medkit'),(552,'fa-meh'),(555,'fa-memory'),(556,'fa-menorah'),(557,'fa-mercury'),(558,'fa-meteor'),(565,'fa-minus'),(568,'fa-mitten'),(569,'fa-mobile'),(578,'fa-moon'),(580,'fa-mosque'),(583,'fa-mouse'),(585,'fa-mug-hot'),(586,'fa-music'),(588,'fa-neuter'),(594,'fa-oil-can'),(595,'fa-om'),(596,'fa-orcid'),(597,'fa-otter'),(598,'fa-outdent'),(599,'fa-pager'),(602,'fa-palette'),(603,'fa-pallet'),(608,'fa-parking'),(611,'fa-paste'),(612,'fa-pause'),(614,'fa-paw'),(615,'fa-peace'),(616,'fa-pen'),(617,'fa-pen-alt'),(619,'fa-pen-nib'),(626,'fa-percent'),(629,'fa-phone'),(637,'fa-pills'),(640,'fa-plane'),(643,'fa-play'),(645,'fa-plug'),(646,'fa-plus'),(649,'fa-podcast'),(650,'fa-poll'),(651,'fa-poll-h'),(652,'fa-poo'),(654,'fa-poop'),(658,'fa-pray'),(663,'fa-print'),(667,'fa-qrcode'),(673,'fa-quran'),(676,'fa-rainbow'),(677,'fa-random'),(678,'fa-receipt'),(680,'fa-recycle'),(681,'fa-redo'),(685,'fa-reply'),(689,'fa-retweet'),(690,'fa-ribbon'),(691,'fa-ring'),(692,'fa-road'),(693,'fa-robot'),(694,'fa-rocket'),(695,'fa-route'),(696,'fa-rss'),(699,'fa-ruler'),(703,'fa-running'),(705,'fa-sad-cry'),(709,'fa-save'),(710,'fa-school'),(712,'fa-scroll'),(713,'fa-sd-card'),(714,'fa-search'),(720,'fa-server'),(721,'fa-shapes'),(722,'fa-share'),(728,'fa-ship'),(734,'fa-shower'),(736,'fa-sign'),(740,'fa-signal'),(743,'fa-sitemap'),(744,'fa-skating'),(745,'fa-skiing'),(747,'fa-skull'),(749,'fa-slash'),(750,'fa-sleigh'),(752,'fa-smile'),(755,'fa-smog'),(756,'fa-smoking'),(758,'fa-sms'),(761,'fa-snowman'),(763,'fa-socks'),(765,'fa-sort'),(779,'fa-sort-up'),(780,'fa-spa'),(783,'fa-spider'),(784,'fa-spinner'),(785,'fa-splotch'),(787,'fa-square'),(791,'fa-stamp'),(792,'fa-star'),(802,'fa-stop'),(805,'fa-store'),(807,'fa-stream'),(812,'fa-subway'),(815,'fa-sun'),(818,'fa-suse'),(820,'fa-swift'),(821,'fa-swimmer'),(824,'fa-sync'),(826,'fa-syringe'),(827,'fa-table'),(829,'fa-tablet'),(831,'fa-tablets'),(833,'fa-tag'),(834,'fa-tags'),(835,'fa-tape'),(836,'fa-tasks'),(837,'fa-taxi'),(838,'fa-teeth'),(842,'fa-tenge'),(846,'fa-th'),(848,'fa-th-list'),(860,'fa-times'),(862,'fa-tint'),(864,'fa-tired'),(867,'fa-toilet'),(869,'fa-toolbox'),(870,'fa-tools'),(871,'fa-tooth'),(872,'fa-torah'),(874,'fa-tractor'),(877,'fa-train'),(878,'fa-tram'),(881,'fa-trash'),(885,'fa-tree'),(886,'fa-trophy'),(887,'fa-truck'),(892,'fa-tshirt'),(893,'fa-tty'),(894,'fa-tv'),(895,'fa-umbraco'),(899,'fa-undo'),(903,'fa-unlink'),(904,'fa-unlock'),(906,'fa-upload'),(907,'fa-user'),(920,'fa-user-md'),(931,'fa-users'),(936,'fa-venus'),(939,'fa-vial'),(940,'fa-vials'),(941,'fa-video'),(943,'fa-vihara'),(952,'fa-walking'),(953,'fa-wallet'),(955,'fa-water'),(957,'fa-weight'),(960,'fa-wifi'),(961,'fa-wind'),(970,'fa-wrench'),(971,'fa-x-ray'),(972,'fa-genderless');

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(120) DEFAULT NULL,
  `menu_link` varchar(120) DEFAULT NULL,
  `menu_icon` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;


/*Table structure for table `previleges` */

DROP TABLE IF EXISTS `previleges`;

CREATE TABLE `previleges` (
  `previlege_id` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` int(11) DEFAULT NULL,
  `groupid` varchar(25) DEFAULT NULL,
  `create` int(11) DEFAULT NULL,
  `update` int(11) DEFAULT NULL,
  `delete` int(11) DEFAULT NULL,
  `read` int(11) DEFAULT NULL,
  `upload` int(11) DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  PRIMARY KEY (`previlege_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Data for the table `previleges` */

insert  into `previleges`(`previlege_id`,`menuid`,`groupid`,`create`,`update`,`delete`,`read`,`upload`,`download`) values (48,8,'1',0,0,0,1,NULL,NULL),(49,9,'1',0,0,0,1,NULL,NULL),(50,10,'1',0,0,0,1,NULL,NULL),(51,11,'1',0,0,0,1,NULL,NULL),(52,12,'1',0,0,0,1,NULL,NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `roles_id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) DEFAULT NULL,
  `userid` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`roles_id`,`groupid`,`userid`) values (2,1,'101923095731'),(4,1,'101923100708');

/*Table structure for table `submenu` */

DROP TABLE IF EXISTS `submenu`;

CREATE TABLE `submenu` (
  `submenu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` int(11) DEFAULT NULL,
  `submenu_name` varchar(120) DEFAULT NULL,
  `submenu_link` varchar(120) DEFAULT NULL,
  `submenu_icon` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`submenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Table structure for table `sync_category` */

DROP TABLE IF EXISTS `sync_category`;

CREATE TABLE `sync_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_type` varchar(150) DEFAULT NULL,
  `category_parent_id` int(11) DEFAULT NULL,
  `group_leader` varchar(150) DEFAULT NULL,
  `sync_date` datetime DEFAULT NULL,
  `sync_status` varchar(80) DEFAULT NULL,
  `flag_status` varchar(80) DEFAULT NULL,
  `table_owner` varchar(80) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sync_category` */

insert  into `sync_category`(`category_id`,`category_name`,`category_type`,`category_parent_id`,`group_leader`,`sync_date`,`sync_status`,`flag_status`,`table_owner`,`table_id`,`updated_date`,`updated_id`) values (100,'TESTING',NULL,NULL,'15890042-1','2019-07-04 14:45:32',NULL,'CREATE','MANUAL',NULL,NULL,NULL),(305,'EKONOMI DAN BISNIS','FACULTY',NULL,'93670009-1','2019-08-05 08:45:29',NULL,'CREATE','FACULTIES',8,NULL,NULL),(306,'ILMU TERAPAN','FACULTY',NULL,'14580008-1','2019-08-05 08:45:29',NULL,'CREATE','FACULTIES',3,NULL,NULL),(307,'INFORMATIKA','FACULTY',NULL,'99750010-1','2019-08-05 08:45:29',NULL,'CREATE','FACULTIES',7,NULL,NULL),(308,'INDUSTRI KREATIF','FACULTY',NULL,'12570001-1','2019-08-05 08:45:29',NULL,'CREATE','FACULTIES',4,NULL,NULL),(309,'KOMUNIKASI DAN BISNIS','FACULTY',NULL,'91660037-1','2019-08-05 08:45:29',NULL,'CREATE','FACULTIES',9,NULL,NULL),(310,'TEKNIK ELEKTRO','FACULTY',NULL,'99760035-1','2019-08-05 08:45:29',NULL,'CREATE','FACULTIES',5,NULL,NULL),(311,'REKAYASA INDUSTRI','FACULTY',NULL,'91670026-1','2019-08-05 08:45:29',NULL,'CREATE','FACULTIES',6,NULL,NULL),(312,'S1 Teknik Fisika','STUDYPROGRAM',310,'00760004-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',62,'2019-08-05 08:45:29','Y'),(313,'D3 Perhotelan','STUDYPROGRAM',306,'15800016-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',54,'2019-08-05 08:45:29','Y'),(314,'D3 Sistem Informasi','STUDYPROGRAM',306,'14740031-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',72,'2019-08-05 08:45:29','Y'),(315,'S1 Informatika','STUDYPROGRAM',307,'00750052-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',31,'2019-08-05 08:45:29','Y'),(316,'S2 Informatika','STUDYPROGRAM',307,'17770073-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',77,'2019-08-05 08:45:29','Y'),(317,'D3 Manajemen Pemasaran','STUDYPROGRAM',306,'08790045-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',51,'2019-08-05 08:45:29','Y'),(318,'S1 Administrasi Bisnis','STUDYPROGRAM',309,'14790007-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',44,'2019-08-05 08:45:29','Y'),(319,'S2 Teknik Industri','STUDYPROGRAM',311,'14590006-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',80,'2019-08-05 08:45:29','Y'),(320,'D3 Rekayasa Perangkat Lunak Aplikasi','STUDYPROGRAM',306,'17720068-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',32,'2019-08-05 08:45:29','Y'),(321,'S1 Teknologi Informasi','STUDYPROGRAM',307,'04660014-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',38,'2019-08-05 08:45:29','Y'),(322,'D3 Teknologi Komputer','STUDYPROGRAM',306,'15780038-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',71,'2019-08-05 08:45:29','Y'),(323,'S1 Desain Komunikasi Visual','STUDYPROGRAM',308,'12690031-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',91,'2019-08-05 08:45:29','Y'),(324,'S1 Rekayasa Perangkat Lunak','STUDYPROGRAM',307,'99750013-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',61,'2019-08-05 08:45:29','Y'),(325,'S1 Sistem Informasi','STUDYPROGRAM',311,'14790008-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',22,'2019-08-05 08:45:29','Y'),(326,'S1 Teknik Telekomunikasi','STUDYPROGRAM',310,'14780033-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',11,'2019-08-05 08:45:29','Y'),(327,'S2 Teknik Elektro','STUDYPROGRAM',310,'07760017-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',78,'2019-08-05 08:45:29','Y'),(328,'D3 Sistem Informasi Akuntansi','STUDYPROGRAM',306,'11710039-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',73,'2019-08-05 08:45:29','Y'),(329,'D4 Teknologi Rekayasa Multimedia','STUDYPROGRAM',306,'14870017-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',33,'2019-08-05 08:45:29','Y'),(330,'S1 Ilmu Komunikasi','STUDYPROGRAM',309,'14760015-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',43,'2019-08-05 08:45:29','Y'),(331,'D3 Teknologi Telekomunikasi','STUDYPROGRAM',306,'95680027-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',14,'2019-08-05 08:45:29','Y'),(332,'S1 Desain Produk','STUDYPROGRAM',308,'15860042-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',94,'2019-08-05 08:45:29','Y'),(333,'S1 Teknik Komputer','STUDYPROGRAM',310,'10750046-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',13,'2019-08-05 08:45:29','Y'),(334,'S1 Manajemen (Manajemen Bisnis Telekomunikasi & Informatika)','STUDYPROGRAM',305,'10740051-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',41,'2019-08-05 08:45:29','Y'),(335,'S2 Manajemen','STUDYPROGRAM',305,'08780059-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',79,'2019-08-05 08:45:29','Y'),(336,'S1 Kriya','STUDYPROGRAM',308,'15880043-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',92,'2019-08-05 08:45:29','Y'),(337,'S1 Akuntansi','STUDYPROGRAM',305,'08790035-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',42,'2019-08-05 08:45:29','Y'),(338,'S1 Seni Rupa','STUDYPROGRAM',308,'10840063-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',95,'2019-08-05 08:45:29','Y'),(339,'S1 Teknik Industri','STUDYPROGRAM',311,'14780007-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',21,'2019-08-05 08:45:29','Y'),(340,'S1 Desain Interior','STUDYPROGRAM',308,'14780067-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',93,'2019-08-05 08:45:29','Y'),(341,'S1 Teknik Elektro','STUDYPROGRAM',310,'99740009-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',12,'2019-08-05 08:45:29','Y'),(342,'S1 Digital Public Relations','STUDYPROGRAM',309,'15830012-1','2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',47,'2019-08-05 08:45:29','Y'),(343,'S1 Administrasi Bisnis - Pindahan','STUDYPROGRAM',309,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',27,'2019-08-05 08:45:29','Y'),(344,'S1 Teknik Elektro  (International Class)','STUDYPROGRAM',310,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',96,'2019-08-05 08:45:29','Y'),(345,'S1 Teknik Komputer - Pindahan','STUDYPROGRAM',310,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',17,'2019-08-05 08:45:29','Y'),(346,'S1 Informatika - Pindahan','STUDYPROGRAM',307,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',19,'2019-08-05 08:45:29','Y'),(347,'S1 International ICT Business','STUDYPROGRAM',305,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',40,'2019-08-05 08:45:29','Y'),(348,'S1 Sistem Informasi (International Class)','STUDYPROGRAM',311,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',24,'2019-08-05 08:45:29','Y'),(349,'S1 Informatika (International Class)','STUDYPROGRAM',307,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',25,'2019-08-05 08:45:29','Y'),(350,'S1 Desain Komunikasi Visual  (International Class)','STUDYPROGRAM',308,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',97,'2019-08-05 08:45:29','Y'),(351,'S1 Teknik Telekomunikasi (International Class)','STUDYPROGRAM',310,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',26,'2019-08-05 08:45:29','Y'),(352,'S1 Sistem Informasi - Pindahan','STUDYPROGRAM',311,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',20,'2019-08-05 08:45:29','Y'),(353,'S1 Ilmu Komunikasi (International Class)','STUDYPROGRAM',309,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',48,'2019-08-05 08:45:29','Y'),(354,'S1 Teknik Logistik','STUDYPROGRAM',311,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',60,'2019-08-05 08:45:29','Y'),(355,'S1 Akuntansi - Pindahan','STUDYPROGRAM',305,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',28,'2019-08-05 08:45:29','Y'),(356,'S1 Teknik Industri (Internasional Class)','STUDYPROGRAM',311,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',23,'2019-08-05 08:45:29','Y'),(357,'S1 Administrasi Bisnis (International Class)','STUDYPROGRAM',309,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',46,'2019-08-05 08:45:29','Y'),(358,'S1 Teknik Telekomunikasi - Pindahan','STUDYPROGRAM',310,NULL,'2019-08-05 08:45:29',NULL,'CREATE','STUDYPROGRAM',18,'2019-08-05 08:45:29','Y'),(359,'1920/1','SCHOOLYEAR',326,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',1,'2019-08-05 08:45:30','Y'),(360,'1920/1','SCHOOLYEAR',341,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',2,'2019-08-05 08:45:30','Y'),(361,'1920/1','SCHOOLYEAR',333,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',3,'2019-08-05 08:45:30','Y'),(362,'1920/1','SCHOOLYEAR',331,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',4,'2019-08-05 08:45:30','Y'),(363,'1920/1','SCHOOLYEAR',345,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',5,'2019-08-05 08:45:30','Y'),(364,'1920/1','SCHOOLYEAR',358,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',6,'2019-08-05 08:45:30','Y'),(365,'1920/1','SCHOOLYEAR',346,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',7,'2019-08-05 08:45:30','Y'),(366,'1920/1','SCHOOLYEAR',352,NULL,'2019-08-05 08:45:29',NULL,'CREATE','SCHOOLYEAR',8,'2019-08-05 08:45:30','Y'),(367,'1920/1','SCHOOLYEAR',339,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',9,'2019-08-05 08:45:30','Y'),(368,'1920/1','SCHOOLYEAR',325,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',10,'2019-08-05 08:45:30','Y'),(369,'1920/1','SCHOOLYEAR',356,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',11,'2019-08-05 08:45:30','Y'),(370,'1920/1','SCHOOLYEAR',348,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',12,'2019-08-05 08:45:30','Y'),(371,'1920/1','SCHOOLYEAR',349,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',13,'2019-08-05 08:45:30','Y'),(372,'1920/1','SCHOOLYEAR',351,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',14,'2019-08-05 08:45:30','Y'),(373,'1920/1','SCHOOLYEAR',343,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',15,'2019-08-05 08:45:30','Y'),(374,'1920/1','SCHOOLYEAR',355,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',16,'2019-08-05 08:45:30','Y'),(375,'1920/1','SCHOOLYEAR',315,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',17,'2019-08-05 08:45:30','Y'),(376,'1920/1','SCHOOLYEAR',320,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',18,'2019-08-05 08:45:30','Y'),(377,'1920/1','SCHOOLYEAR',329,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',19,'2019-08-05 08:45:30','Y'),(378,'1920/1','SCHOOLYEAR',321,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',20,'2019-08-05 08:45:30','Y'),(379,'1920/1','SCHOOLYEAR',347,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',21,'2019-08-05 08:45:30','Y'),(380,'1920/1','SCHOOLYEAR',334,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',22,'2019-08-05 08:45:30','Y'),(381,'1920/1','SCHOOLYEAR',337,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',23,'2019-08-05 08:45:30','Y'),(382,'1920/1','SCHOOLYEAR',330,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',24,'2019-08-05 08:45:30','Y'),(383,'1920/1','SCHOOLYEAR',318,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',25,'2019-08-05 08:45:30','Y'),(384,'1920/1','SCHOOLYEAR',357,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',26,'2019-08-05 08:45:30','Y'),(385,'1920/1','SCHOOLYEAR',342,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',27,'2019-08-05 08:45:30','Y'),(386,'1920/1','SCHOOLYEAR',353,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',28,'2019-08-05 08:45:30','Y'),(387,'1920/1','SCHOOLYEAR',317,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',29,'2019-08-05 08:45:30','Y'),(388,'1920/1','SCHOOLYEAR',313,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',30,'2019-08-05 08:45:30','Y'),(389,'1920/1','SCHOOLYEAR',354,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',31,'2019-08-05 08:45:30','Y'),(390,'1920/1','SCHOOLYEAR',324,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',32,'2019-08-05 08:45:30','Y'),(391,'1920/1','SCHOOLYEAR',312,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',33,'2019-08-05 08:45:30','Y'),(392,'1920/1','SCHOOLYEAR',322,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',34,'2019-08-05 08:45:30','Y'),(393,'1920/1','SCHOOLYEAR',314,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',35,'2019-08-05 08:45:30','Y'),(394,'1920/1','SCHOOLYEAR',328,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',36,'2019-08-05 08:45:30','Y'),(395,'1920/1','SCHOOLYEAR',316,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',37,'2019-08-05 08:45:30','Y'),(396,'1920/1','SCHOOLYEAR',327,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',38,'2019-08-05 08:45:30','Y'),(397,'1920/1','SCHOOLYEAR',335,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',39,'2019-08-05 08:45:30','Y'),(398,'1920/1','SCHOOLYEAR',319,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',40,'2019-08-05 08:45:30','Y'),(399,'1920/1','SCHOOLYEAR',323,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',41,'2019-08-05 08:45:30','Y'),(400,'1920/1','SCHOOLYEAR',336,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',42,'2019-08-05 08:45:30','Y'),(401,'1920/1','SCHOOLYEAR',340,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',43,'2019-08-05 08:45:30','Y'),(402,'1920/1','SCHOOLYEAR',332,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',44,'2019-08-05 08:45:30','Y'),(403,'1920/1','SCHOOLYEAR',338,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',45,'2019-08-05 08:45:30','Y'),(404,'1920/1','SCHOOLYEAR',344,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',46,'2019-08-05 08:45:30','Y'),(405,'1920/1','SCHOOLYEAR',350,NULL,'2019-08-05 08:45:30',NULL,'CREATE','SCHOOLYEAR',47,'2019-08-05 08:45:30','Y'),(406,'S2 Sistem Informasi','STUDYPROGRAM',311,NULL,'2019-10-28 15:37:26',NULL,'CREATE','STUDYPROGRAM',10,'2019-10-28 15:37:26','Y'),(407,'S1 Data Sains','STUDYPROGRAM',307,NULL,'2019-10-28 15:37:26',NULL,'CREATE','STUDYPROGRAM',30,'2019-10-28 15:37:26','Y'),(408,'S3 Informatika','STUDYPROGRAM',307,NULL,'2019-10-28 15:37:26',NULL,'CREATE','STUDYPROGRAM',52,'2019-10-28 15:37:26','Y'),(409,'S1 Teknik Biomedis','STUDYPROGRAM',310,NULL,'2019-10-28 15:37:26',NULL,'CREATE','STUDYPROGRAM',29,'2019-10-28 15:37:26','Y'),(410,'1819/1','SCHOOLYEAR',332,NULL,'2019-10-28 15:37:26',NULL,'CREATE','SCHOOLYEAR',48,'2019-10-28 15:37:26','Y'),(411,'1819/1','SCHOOLYEAR',338,NULL,'2019-10-28 15:37:26',NULL,'CREATE','SCHOOLYEAR',49,'2019-10-28 15:37:26','Y'),(412,'1819/1','SCHOOLYEAR',344,NULL,'2019-10-28 15:37:26',NULL,'CREATE','SCHOOLYEAR',50,'2019-10-28 15:37:26','Y'),(413,'1819/1','SCHOOLYEAR',350,NULL,'2019-10-28 15:37:26',NULL,'CREATE','SCHOOLYEAR',51,'2019-10-28 15:37:26','Y');

/*Table structure for table `sync_category_history` */

DROP TABLE IF EXISTS `sync_category_history`;

CREATE TABLE `sync_category_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sync_category_id` int(11) DEFAULT NULL,
  `user_id` varchar(25) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `sync_category_history` */

insert  into `sync_category_history`(`id`,`sync_category_id`,`user_id`,`date`) values (1,236,'101923100708','2019-11-08 07:44:19'),(2,NULL,'101923100708','2019-11-12 00:00:00'),(3,NULL,'101923100708','2019-11-12 10:52:28'),(4,NULL,'101923100708','2019-11-12 10:57:17'),(5,NULL,'101923100708','2019-11-12 13:16:04');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` varchar(25) NOT NULL,
  `fullname` varchar(150) DEFAULT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `telp` varchar(25) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`fullname`,`username`,`password`,`email`,`telp`,`address`) values ('101923100708','Benny Burhan','','','bburhan@gmail.com','0816700800','Jln.Pasar minggu no.8,Jakarta Selatan'),('101923134045','Tukirno','','','tukirno@gmail.com','0816700800','Jalan Kawung No.33,Kiaracondong,Bandung City'),('101924183652','Ibnu Haikal M','','','ihaikal@gmail.com','08167008001','Jalan Jakarta No.110,Antapani,Bandung City'),('111912014913','Hendro Siswanto','','','hsiswanto@gmail.com1','0816700800','Jalan Kembar III No.10,Bandung City'),('111912014932','M. Iqbal Tanjung','','','miqbol@ptwii.com','0816700800','Jalan Kawung No.33,Kiaracondong,Bandung City');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100408
Source Host           : localhost:3306
Source Database       : celeodb

Target Server Type    : MYSQL
Target Server Version : 100408
File Encoding         : 65001

Date: 2019-11-14 20:50:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user_logging
-- ----------------------------
DROP TABLE IF EXISTS `user_logging`;
CREATE TABLE `user_logging` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` char(10) NOT NULL,
  `log_type` int(20) NOT NULL,
  `log_value` text NOT NULL,
  `log_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`id`) USING BTREE,
  KEY `index` (`user_id`,`log_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_logging
-- ----------------------------
INSERT INTO `user_logging` VALUES ('1', '19871', '0', 'Sync', '2019-11-14 20:48:18');
INSERT INTO `user_logging` VALUES ('2', '19871', '1', 'Login', '2019-11-14 20:48:35');
INSERT INTO `user_logging` VALUES ('3', '19871', '1', 'Login', '2019-11-14 20:49:27');
INSERT INTO `user_logging` VALUES ('4', '19871', '1', 'Login', '2019-11-14 20:49:33');
INSERT INTO `user_logging` VALUES ('5', '19871', '2', 'Logout', '2019-11-14 20:49:56');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `institution` varchar(100) NOT NULL,
  `empnum` char(10) NOT NULL,
  `employeeid` char(20) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `directorate` varchar(40) DEFAULT NULL,
  `unit` varchar(40) DEFAULT NULL,
  `lecturecode` char(3) DEFAULT NULL,
  `structural_functional_position` varchar(100) DEFAULT '',
  `structural_academic_position` varchar(100) DEFAULT NULL,
  `last_sync` datetime NOT NULL,
  `token` text NOT NULL,
  `expired` datetime NOT NULL,
  PRIMARY KEY (`id`,`username`),
  UNIQUE KEY `unique` (`id`,`username`) USING BTREE,
  KEY `index` (`institution`,`empnum`,`employeeid`,`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'hidayanto', '3524bd49713a969d00817a806d59e52c', '4697c7439a5df4703b9404bbabc58c0fde230f53f81b1be7ec8a0aadff51dbcf', 'UNIVERSITAS TELKOM', '19871', '15900033-1', 'MUHAMMAD BAMBANG HIDAYANTO', 'hidayanto@telkomuniversity.ac.id', 'DIREKTORAT PASCA SARJANA & ADVANCE LEARN', 'URUSAN PENGELOLAAN MATERI, UJIAN & PERAN', null, null, null, '2019-11-14 20:48:18', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMTBiMjcxZDY1N2VlY2RhYmIyOGQwNzM4NDU5YjI4MjA3MmRhMTA3NTE3NDNhODAwNTA5NTc0ZGFhYjRjZTA3Yzc4ZTVjNGEyZGNiMTQ3ZDMiLCJpYXQiOjE1NzM3MzkyOTgsIm5iZiI6MTU3MzczOTI5OCwiZXhwIjoxNTczODI1Njk4LCJzdWIiOiJoaWRheWFudG8iLCJzY29wZXMiOlsiY2Vsb2UtZGFzaGJvYXJkIiwibmV3LXNzbyIsIm9sZC1wZWdhd2FpIl19.f_xotOJ-9Wf7MneYbFkuhaTWKtTmBIeZRDeUFQXmsJ2LBVf8TQ_1IsQZSfXjk15UxQJQmKd2ZZz-YB6XYl6jZvtHmhWWajqY0NOvm-L3JmaWG6T9nloiRNV3eFXZwh6PIsNcEjAqf0EpjLzUQoHB59Mho-_DeqOJJTA9ufS_ZI2cKPHrBiScZgumQeb5qlf_rXTA84vt_GUAOA-jTvKyd1XpwFNI1BwK68cqL4-1yKD83ekMJrNP3ROCJv0AyhFQWKZHAuuIrtHRPvuQEZje7yaqu8H-69Ph3BMLUyoBhuJgvPMkJXie9nddvsoaeNpTIP3djk_u1bb4Smtg0YHW8zWYlaB0pVwGpkelcIhbqQPdZIoGBpLoeRA7Sn7eS-mR9hpalhiVdjJqFY5iMuKOYdZDOaHFkw2BHkRPyZtleqFNkrwvw9Di6WPZFIhPYc4w1O-uPwRi5IojRjrv6rtrsmZtg-AN5OzsgW06XD0cbtWFBJb7cPgdzLHi1cwEVFWhhRq7W-nheWXRGj4YK-IFvPizIWCRIL1tq_u91HLz6UkgceCYDOPpc4dnAOko5hiMX_Hj75K_LMBGFtKuUksigjnCgchIA_A9pPoK7O866DUwrlsmKrV5cGL3p24sTw30256vORpQ6JVSoh4mu9fhHMqENSGt1Aw4Hbx0BJQK3s4', '2019-11-15 20:48:18');
SET FOREIGN_KEY_CHECKS=1;

/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100408
Source Host           : localhost:3306
Source Database       : celeodb

Target Server Type    : MYSQL
Target Server Version : 100408
File Encoding         : 65001

Date: 2019-11-13 00:11:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `auth_dev` varchar(100) NOT NULL,
  `sync_dev` varchar(100) NOT NULL,
  `auth_prod` varchar(100) NOT NULL,
  `sync_prod` varchar(100) NOT NULL,
  `status_active` char(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`id`) USING BTREE,
  KEY `index` (`auth_dev`,`sync_dev`,`auth_prod`,`sync_prod`,`status_active`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of auth
-- ----------------------------
INSERT INTO `auth` VALUES ('1', 'https://dev-gateway.telkomuniversity.ac.id/issueauth', 'https://dev-gateway.telkomuniversity.ac.id/5c584e9bb69c6a39275dd8d7d94a1b07', '', '', 'dev');
SET FOREIGN_KEY_CHECKS=1;

