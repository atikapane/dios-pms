$(document).ready(function () {
    $('.slider-category').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        focusOnSelect: false,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: false,
            }

        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                focusOnSelect: false,
            }

        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                infinite: true,
                focusOnSelect: false,
            }
        }]
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        autoplay: true
    });
    
    $('.slider-for').on('beforeChange', function(event, slick, currentSlide, nextSlide){
      var caption = $(slick.$slides.get(nextSlide)).data("caption");
      $("#banner_desc").fadeOut(400, function() {
        $(this).html(caption).fadeIn(400);
      });
    });

    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: true,
        centerMode: false,
        focusOnSelect: true
    });

    $('.fade-anchor').click(function (e) {
        e.preventDefault();

        $('.fade-content').css('max-height', 'none');
        $('.fade-anchor').remove();
    });

    var $star_rating = $('.star-rating .fas');

    var SetRatingStar = function () {
        return $star_rating.each(function () {
            if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fas-star').addClass('checked');
            } else {
                return $(this).removeClass('checked');
            }
        });
    };

    $star_rating.on('click', function () {
        $star_rating.siblings('input.rating-value').val($(this).data('rating')).trigger('change');
        return SetRatingStar();
    });

    var CheckRatingFeedback = () => {
        feedback = $('#rating_feedback').val()
        star = $('.rating-value').val()

        feedback == '' || star == '' ? $('#btn_rating_submit').attr('disabled', 'disabled') : $('#btn_rating_submit').removeAttr('disabled')
    }

    $('#rating_feedback').on('keyup', () => {
        CheckRatingFeedback()
    })

    $('.rating-value').on('change', () => {
        CheckRatingFeedback()
    })

    SetRatingStar();

    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".fa-plus").removeClass("fas fa-plus").addClass("fas fa-minus");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".fa-minus").removeClass("fas fa-minus").addClass("fas fa-plus");
    });
    $(".owl-carousel").owlCarousel({
        stagePadding: 50,
        loop: false,
        margin: 10,
        nav: true,
        navText : ["<i class='fas fa-chevron-left' style='color:#fff;'></i>","<i class='fas fa-chevron-right' style='color:#fff;'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        },
    });

    $('.open-login-modal').on('click',function(e) {
        e.preventDefault()
        $('#login-modal').fadeIn(400);
    });

    $('#close-login-modal').on('click',function(e) {
        e.preventDefault()
        $('#login-modal').fadeOut(400);
    });
});