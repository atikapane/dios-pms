$(document).ready(function(){
	var currentCatId = catId;
	var currentProgId = 0
	var offset = 0;
	var perPage = 10;
	var endReached = false;
	var isFetching = false;

	if(catName != "" ){
		$("#filter_title").html(catName + " Course")
	}

	$(window).scroll(function () {
	    if (($(document).height() - $('footer').height()) - $(this).height() <= $(this).scrollTop()) {
	    	loadMore()
	    }
	}); 

    function blockUI(){
        $("#loading_overlay").show()
    }
    function unblockUI(){
        $("#loading_overlay").hide()
    }
    $("#program_course_filter").change(function(){
    	currentProgId = $(this).val()
		clear()
		loadMore()
    })
	$("#search_btn").off('click').on('click',function(){
		clear()
		loadMore()
	})

	$('#search_query').keypress(function (e) {
	  if (e.which == 13) {
		clear()
		loadMore()
	  }
	});

	$(".category_card").on('click',function(e){
		e.preventDefault()
		e.stopPropagation()
		var catId = $(this).data('id')
		if(catId == currentCatId) return;
		var title = "All Course"
		if(catId != 0){
			title = $(this).find('.card-title').html() + " Course"
		}
		currentCatId = catId
    	currentProgId = 0
    	$('#search_query').val("")
		$("#filter_title").html(title)
		clear()
		loadMore()
		getProgram(currentCatId)
	})

	function loadMore(){
		if(!endReached && !isFetching){
			blockUI()
			var query = $("#search_query").val();
			isFetching = true
	        $.ajax({
	            type:"POST",
	            url:getCourseURL,
	            data:{
	                faculty_id:currentCatId,
	                program_id:currentProgId,
	                search:query,
	                limit:perPage,
	                start:offset
	            },
	            cache:false,
	            dataType: "json",
	            success: function (response, status) {
	               	endReached = response.data.length < perPage
	               	for (var i = 0; i < response.data.length; i++) {
	               		var item = response.data[i]
	               		var template =  $(".template").clone()
	               		template.find('#course_image').attr("src",baseURL+item.banner_image)
	               		template.find('#course_date').html(item.start_date)
	               		template.find('#course_title').text(item.ocw_name)
	               		if(item.type == 2){
		               		if(item.price!=='0'){

		               			template.find('#course_price').text( parseInt(item.price).toLocaleString('id-ID', { style: 'currency', currency: 'IDR',minimumFractionDigits: 0, maximumFractionDigits: 0 }));
		               		}else{
		               			template.find('#course_price').text("FREE")
		               		}
		               		if(item.discount!=='0'){
		               			template.find('#course_real_price').text( parseInt(100*item.price/(100-item.discount)).toLocaleString('id-ID', { style: 'currency', currency: 'IDR',minimumFractionDigits: 0, maximumFractionDigits: 0 }));
		               		}else{
		               			template.find('#course_real_price').text(" ")
		               		}
		               	}else{
		               		template.find(".secret").css("visibility","hidden")
						}
						
						if(item.is_multiple == 0){
							template.data('url',viewURL+item.slug)
						}else{
							template.data('url',packageURL+item.slug)
						}
	               		template.find('#course_desc').html(item.description.replace(/(<([^>]+)>)/ig,""))

	               		template.removeClass("template")
	               		template.css("display","initial")
	               		template.css("cursor","pointer")
	               		template.off('click').on('click',function(){
				    		window.location.href = $(this).data("url")
						})
	               		$("#main_container").append(template)
	               	}
	               	if(response.data.length == 0){
	               		$("#main_container").append($("<h5>No course with this criteria at the moment, please come back later!</h5>"))
	               	}
	       			offset += perPage
	       			isFetching = false
					unblockUI()
	            },
	            error: function (jqXHR, textStatus, errorThrown) {
	                isFetching = false
					unblockUI()
	            }
	        });
       }
	}

	function getProgram(categoryId){
        var ajax = $.ajax({
            url:getProgramURL,
            type:"POST",
            data:{
                faculty_id:categoryId
            },
            cache:false
        });

        ajax.done(function (res) {
            populateProgram(res.data);
        });

        ajax.always(function(res){
        	// loadDataCourse()
        })
	}

	function populateProgram(program){
        $("#program_course_filter").empty()
        $("#program_course_filter").append('<option value="0">All Program</option>');
        $.each(program, function (e, program) {
            $("#program_course_filter").append('<option value="'+program.program_id+'">'+program.study_program_name+'</option>');
        });
    }

	function clear(){
   		$("#main_container").empty()
		offset = 0;
		perPage = 10;
		endReached = false;
		isFetching = false;
	}
	loadMore();
})