$(document).ready(function(){
	function blockUI(){
        $("#loading_overlay").show()
    }
    function unblockUI(){
        $("#loading_overlay").hide()
    }
	var offset = 0;
	var perPage = 10;
	var endReached = false;
	var isFetching = false;
	$(window).scroll(function () {
	    if (($(document).height() - $('footer').height()) - $(this).height() <= $(this).scrollTop()) {
	    	loadMore()
	    }
	}); 

	$("#search_btn").off('click').on('click',function(){
		clear()
		loadMore()
	})
	$('#search_query').keypress(function (e) {
	  if (e.which == 13) {
		clear()
		loadMore()
	  }
	});
	
	function loadMore(){
		if(!endReached && !isFetching){
			blockUI()
			var query = $("#search_query").val();
			isFetching = true
	        $.ajax({
	            type: 'GET',
	            url: downloadURL,
	            data: {o:offset, l:perPage, q:query},
	            dataType: "json",
	            success: function (response, status) {
	               	endReached = response.length < perPage
	               	for (var i = 0; i < response.length; i++) {
	               		var item = response[i]
	               		var downloadTemplate =  $(".download_template").clone()
	               		downloadTemplate.find('.title').text(item.title)
	               		downloadTemplate.find('.description').text(item.description)
	               		downloadTemplate.find('.download_btn').data("url",item.file_path)
	               		downloadTemplate.css("display","initial")
	               		downloadTemplate.removeClass("download_template")
	               		if(item.type == "external"){
	               			downloadTemplate.find('.download_icon').removeClass("fa-download").addClass("fa-external-link-alt")
	               			downloadTemplate.find('.download_text').html("Open Link")
	               		}
	               		$("#main_container").append(downloadTemplate)
	               	}
	               	initEvent()
	       			offset += perPage
	       			isFetching = false
					unblockUI()
	            },
	            error: function (jqXHR, textStatus, errorThrown) {
	                isFetching = false
					unblockUI()
	            }
	        });
       }
	}
	function initEvent(){
		$(".download_btn").off('click').on('click',function(){
			var url = $(this).data("url")
    		window.open(url, '_blank');
		})
	}

	function clear(){
   		$("#main_container").empty()
		offset = 0;
		perPage = 10;
		endReached = false;
		isFetching = false;
	}
	loadMore()
})