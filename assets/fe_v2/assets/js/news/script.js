$(document).ready(function(){
	function blockUI(){
        $("#loading_overlay").show()
    }
    function unblockUI(){
        $("#loading_overlay").hide()
    }
	var offset = 0;
	var perPage = 10;
	var endReached = false;
	var isFetching = false;
	$(window).scroll(function () {
	    if (($(document).height() - $('footer').height()) - $(this).height() <= $(this).scrollTop()) {
	    	loadMore()
	    }
	}); 

	$("#search_btn").off('click').on('click',function(){
		clear()
		loadMore()
	})
	$('#search_query').keypress(function (e) {
	  if (e.which == 13) {
		clear()
		loadMore()
	  }
	});
	
	function loadMore(){
		if(!endReached && !isFetching){
			blockUI()
			var query = $("#search_query").val();
			isFetching = true
	        $.ajax({
	            type: 'GET',
	            url: getURL,
	            data: {o:offset, l:perPage, q:query},
	            dataType: "json",
	            success: function (response, status) {
	               	endReached = response.length < perPage
	               	for (var i = 0; i < response.length; i++) {
	               		var item = response[i]
						var template =  $(".template").clone()
						template.addClass('news_container')
	               		template.find('.news_title').text(item.title)
	               		template.find('.news_content').text(item.description)
	               		template.find('.news_date').text(item.date)
	               		template.find('.news_thumbnail').css('background', 'url("' + item.thumbnail + '")');
	               		template.find('.news_thumbnail').css('background-repeat', 'no-repeat');
	               		template.find('.news_thumbnail').css('background-size', 'cover');
	               		template.find('.news_thumbnail').css('background-position', 'center');
	               		var tags = item.related.split(",")
	               		for (var j = 0; j < tags.length; j++) {
	               			var tag = tags[j]
	               			var tagTemplate =  $(".tag_template").clone()
	               			tagTemplate.css("display","block")
	               			tagTemplate.html("#"+tag)
	               			tagTemplate.removeClass("tag_template")

	               			template.find('.news_tag').append(tagTemplate)
	               		}
	               		template.data("url",item.url)
	               		template.removeClass("template")
	               		template.css("display","initial")
	               		template.find('.news_title').css("cursor","pointer")
	               		template.off('click').find('.news_title').on('click',function(){
				    		window.location.href = $(this).parents('.news_container').data("url")
						})
	               		$("#main_container").append(template)
	               	}
	               	initEvent()
	       			offset += perPage
	       			isFetching = false
					unblockUI()
	            },
	            error: function (jqXHR, textStatus, errorThrown) {
	                isFetching = false
					unblockUI()
	            }
	        });
       }
	}
	function initEvent(){
		$(".download_btn").off('click').on('click',function(){
			var url = $(this).data("url")
    		window.open(url, '_blank');
		})
	}

	function clear(){
   		$("#main_container").empty()
		offset = 0;
		perPage = 10;
		endReached = false;
		isFetching = false;
	}
	loadMore()
})