$(document).ready(function(){
    $('.fade-anchor').click(function (e) {
        e.preventDefault();

        $('.fade-content').css('max-height', 'none');
        $('.fade-anchor').remove();
    });

    var $star_rating = $('.star-rating .fas');

    var SetRatingStar = function () {
        return $star_rating.each(function () {
            if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fas-star').addClass('checked');
            } else {
                return $(this).removeClass('checked');
            }
        });
    };

    $star_rating.on('click', function () {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
    });

    SetRatingStar();

    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".fa-plus").removeClass("fas fa-plus").addClass("fas fa-minus");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".fa-minus").removeClass("fas fa-minus").addClass("fas fa-plus");
    });
    $(".owl-carousel").owlCarousel({
        stagePadding: 50,
        loop: false,
        margin: 10,
        nav: true,
        navText : ["<i class='fas fa-chevron-left' style='color:#fff;'></i>","<i class='fas fa-chevron-right' style='color:#fff;'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        },
    });
    function blockUI(){
        $("#loading_overlay").show()
    }
    function unblockUI(){
        $("#loading_overlay").hide()
    }
})